package com.netizen.netiworld.utils

import android.util.Base64
import android.util.Log
import java.io.UnsupportedEncodingException

object JWTUtils {

    @Throws(Exception::class)
    fun decoded(JWTEncoded: String) {
        try {
            val split = JWTEncoded.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            val body=getJson(split[1])
            Log.d("JWT_DECODED", "Header: " + getJson(split[0]))
            Log.d("JWT_DECODED", "Body: " + getJson(split[1]))
        } catch (e: UnsupportedEncodingException) {
            Log.d("Not Support", "Not Encode")
        }

    }

    @Throws(UnsupportedEncodingException::class)
    fun getJson(strEncoded: String): String {
        val decodedBytes = Base64.decode(strEncoded, Base64.URL_SAFE)
        return String(decodedBytes)
    }
}