package com.netizen.netiworld.model.UserPoint

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class InventoryDetails {

    @SerializedName("message")
    @Expose
    private var message: String? = null

    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null

    @SerializedName("item")
    @Expose
    private var item: List<Item>? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<Item?>? {
        return item
    }

    fun setItem(item: List<Item>?) {
        this.item = item
    }

    class Item {
        @SerializedName("date")
        @Expose
        private var date: String? = null

        @SerializedName("category")
        @Expose
        private var category: String? = null

        @SerializedName("item")
        @Expose
        private var item: String? = null

        @SerializedName("unitPrice")
        @Expose
        private var unitPrice: Float? = null

        @SerializedName("quantity")
        @Expose
        private var quantity: Float? = null

        @SerializedName("totalPrice")
        @Expose
        private var totalPrice: Float? = null

        fun getDate(): String? {
            return date
        }

        fun setDate(date: String?) {
            this.date = date
        }

        fun getCategory(): String? {
            return category
        }

        fun setCategory(category: String?) {
            this.category = category
        }

        fun getItem(): String? {
            return item
        }

        fun setItem(item: String?) {
            this.item = item
        }

        fun getUnitPrice(): Float? {
            return unitPrice
        }

        fun setUnitPrice(unitPrice: Float?) {
            this.unitPrice = unitPrice
        }

        fun getQuantity(): Float? {
            return quantity
        }

        fun setQuantity(quantity: Float?) {
            this.quantity = quantity
        }

        fun getTotalPrice(): Float? {
            return totalPrice
        }

        fun setTotalPrice(totalPrice: Float?) {
            this.totalPrice = totalPrice
        }

    }

}