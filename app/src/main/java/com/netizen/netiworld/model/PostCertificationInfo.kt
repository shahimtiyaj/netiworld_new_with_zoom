package com.netizen.netiworld.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class PostCertificationInfo {

    @SerializedName("countryInfoDTO")
    @Expose
    private var countryInfoDTO: CountryInfoDTO? = null

    @SerializedName("certificationName")
    @Expose
    private var certificationName: String? = null

    @SerializedName("instituteName")
    @Expose
    private var instituteName: String? = null

    @SerializedName("instituteLocation")
    @Expose
    private var instituteLocation: String? = null

    @SerializedName("achieveDate")
    @Expose
    private var achieveDate: String? = null

    @SerializedName("courseDuration")
    @Expose
    private var courseDuration: String? = null

    fun getCountryInfoDTO(): CountryInfoDTO? {
        return countryInfoDTO
    }

    fun setCountryInfoDTO(countryInfoDTO: CountryInfoDTO?) {
        this.countryInfoDTO = countryInfoDTO
    }

    fun getCertificationName(): String? {
        return certificationName
    }

    fun setCertificationName(certificationName: String?) {
        this.certificationName = certificationName
    }

    fun getInstituteName(): String? {
        return instituteName
    }

    fun setInstituteName(instituteName: String?) {
        this.instituteName = instituteName
    }

    fun getInstituteLocation(): String? {
        return instituteLocation
    }

    fun setInstituteLocation(instituteLocation: String?) {
        this.instituteLocation = instituteLocation
    }

    fun getAchieveDate(): String? {
        return achieveDate
    }

    fun setAchieveDate(achieveDate: String?) {
        this.achieveDate = achieveDate
    }

    fun getCourseDuration(): String? {
        return courseDuration
    }

    fun setCourseDuration(courseDuration: String?) {
        this.courseDuration = courseDuration
    }

    class CountryInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: String? = null

        fun getCoreCategoryID(): String? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: String?) {
            this.coreCategoryID = coreCategoryID
        }
    }
}