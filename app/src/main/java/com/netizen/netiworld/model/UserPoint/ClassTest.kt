package com.netizen.netiworld.model.UserPoint

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class ClassTest {
    @SerializedName("message")
    @Expose
    private var message: Any? = null

    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null

    @SerializedName("item")
    @Expose
    private var item: List<Item?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<Item?>? {
        return item
    }

    fun setItem(item: List<Item?>?) {
        this.item = item
    }

    class Item {
        @SerializedName("classTestConfigID")
        @Expose
        private var classTestConfigID: Int? = null

        @SerializedName("instituteID")
        @Expose
        private var instituteID: String? = null

        @SerializedName("className")
        @Expose
        private var className: String? = null

        @SerializedName("testName")
        @Expose
        private var testName: String? = null

        fun getClassTestConfigID(): Int? {
            return classTestConfigID
        }

        fun setClassTestConfigID(classTestConfigID: Int?) {
            this.classTestConfigID = classTestConfigID
        }

        fun getInstituteID(): String? {
            return instituteID
        }

        fun setInstituteID(instituteID: String?) {
            this.instituteID = instituteID
        }

        fun getClassName(): String? {
            return className
        }

        fun setClassName(className: String?) {
            this.className = className
        }

        fun getTestName(): String? {
            return testName
        }

        fun setTestName(testName: String?) {
            this.testName = testName
        }

    }
}