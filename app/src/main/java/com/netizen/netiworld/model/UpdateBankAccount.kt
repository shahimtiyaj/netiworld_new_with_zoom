package com.netizen.netiworld.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class UpdateBankAccount {

    @SerializedName("taggingTypeDefaultCode")
    @Expose
    private var taggingTypeDefaultCode: String? = null

    @SerializedName("userBankAccountTaggingDTO")
    @Expose
    private var userBankAccountTaggingDTO: UserBankAccountTaggingDTO? = null

    @SerializedName("userBasicInfoDTO")
    @Expose
    private var userBasicInfoDTO: UserBasicInfoDTO? = null

    fun getTaggingTypeDefaultCode(): String? {
        return taggingTypeDefaultCode
    }

    fun setTaggingTypeDefaultCode(taggingTypeDefaultCode: String?) {
        this.taggingTypeDefaultCode = taggingTypeDefaultCode
    }

    fun getUserBankAccountTaggingDTO(): UserBankAccountTaggingDTO? {
        return userBankAccountTaggingDTO
    }

    fun setUserBankAccountTaggingDTO(userBankAccountTaggingDTO: UserBankAccountTaggingDTO?) {
        this.userBankAccountTaggingDTO = userBankAccountTaggingDTO
    }

    fun getUserBasicInfoDTO(): UserBasicInfoDTO? {
        return userBasicInfoDTO
    }

    fun setUserBasicInfoDTO(userBasicInfoDTO: UserBasicInfoDTO?) {
        this.userBasicInfoDTO = userBasicInfoDTO
    }

    class UserBankAccountTaggingDTO {

        @SerializedName("taggingTypeCoreCategoryInfoDTO")
        @Expose
        private var taggingTypeCoreCategoryInfoDTO: TaggingTypeCoreCategoryInfoDTO? = null

        @SerializedName("userBankAccountInfoDTO")
        @Expose
        private var userBankAccountInfoDTO: UserBankAccountInfoDTO? = null

        fun getTaggingTypeCoreCategoryInfoDTO(): TaggingTypeCoreCategoryInfoDTO? {
            return taggingTypeCoreCategoryInfoDTO
        }

        fun setTaggingTypeCoreCategoryInfoDTO(taggingTypeCoreCategoryInfoDTO: TaggingTypeCoreCategoryInfoDTO?) {
            this.taggingTypeCoreCategoryInfoDTO = taggingTypeCoreCategoryInfoDTO
        }

        fun getUserBankAccountInfoDTO(): UserBankAccountInfoDTO? {
            return userBankAccountInfoDTO
        }

        fun setUserBankAccountInfoDTO(userBankAccountInfoDTO: UserBankAccountInfoDTO?) {
            this.userBankAccountInfoDTO = userBankAccountInfoDTO
        }

        class TaggingTypeCoreCategoryInfoDTO {

            @SerializedName("coreCategoryID")
            @Expose
            private var coreCategoryID: Int? = null

            fun getCoreCategoryID(): Int? {
                return coreCategoryID
            }

            fun setCoreCategoryID(coreCategoryID: Int?) {
                this.coreCategoryID = coreCategoryID
            }
        }

        class UserBankAccountInfoDTO {

            @SerializedName("userBankAccId")
            @Expose
            private var userBankAccId: Int? = null

            @SerializedName("coreBankBranchInfoDTO")
            @Expose
            private var coreBankBranchInfoDTO: CoreBankBranchInfoDTO? = null

            fun getUserBankAccId(): Int? {
                return userBankAccId
            }

            fun setUserBankAccId(userBankAccId: Int?) {
                this.userBankAccId = userBankAccId
            }

            fun getCoreBankBranchInfoDTO(): CoreBankBranchInfoDTO? {
                return coreBankBranchInfoDTO
            }

            fun setCoreBankBranchInfoDTO(coreBankBranchInfoDTO: CoreBankBranchInfoDTO?) {
                this.coreBankBranchInfoDTO = coreBankBranchInfoDTO
            }

            class CoreBankBranchInfoDTO {

                @SerializedName("branchID")
                @Expose
                private var branchID: Int? = null

                fun getBranchID(): Int? {
                    return branchID
                }

                fun setBranchID(branchID: Int?) {
                    this.branchID = branchID
                }
            }
        }
    }

    class UserBasicInfoDTO {

        @SerializedName("netiID")
        @Expose
        private var netiID: Int? = null

        fun getNetiID(): Int? {
            return netiID
        }

        fun setNetiID(netiID: Int?) {
            this.netiID = netiID
        }
    }
}