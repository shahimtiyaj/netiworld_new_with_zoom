package com.netizen.netiworld.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RegistrationPost {

    @SerializedName("fullName")
    @Expose
    private var fullName: String? = null

    @SerializedName("gender")
    @Expose
    private var gender: String? = null

    @SerializedName("religion")
    @Expose
    private var religion: String? = null

    @SerializedName("dateOfBirth")
    @Expose
    private var dateOfBirth: String? = null

    @SerializedName("basicMobile")
    @Expose
    private var basicMobile: String? = null

    @SerializedName("basicEmail")
    @Expose
    private var basicEmail: String? = null

    @SerializedName("userName")
    @Expose
    private var userName: String? = null

    @SerializedName("userPassword")
    @Expose
    private var userPassword: String? = null

    @SerializedName("globalAreaInfoDTO")
    @Expose
    private var globalAreaInfoDTO: GlobalAreaInfoDTO? = null

    constructor() {}

    constructor(
        fullName: String?,
        gender: String?,
        religion: String?,
        dateOfBirth: String?,
        basicMobile: String?,
        basicEmail: String?,
        userName: String?,
        userPassword: String?,
        globalAreaInfoDTO: GlobalAreaInfoDTO?
    ) {
        this.fullName = fullName
        this.gender = gender
        this.religion = religion
        this.dateOfBirth = dateOfBirth
        this.basicMobile = basicMobile
        this.basicEmail = basicEmail
        this.userName = userName
        this.userPassword = userPassword
        this.globalAreaInfoDTO = globalAreaInfoDTO
    }


    class GlobalAreaInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null


        constructor(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }


        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }
    }

    class ProductRoleAssignDTO {

        @SerializedName("productRoleAssignID")
        @Expose
        private var productRoleAssignID: Int? = null

        fun getProductRoleAssignID(): Int? {
            return productRoleAssignID
        }

        fun setProductRoleAssignID(productRoleAssignID: Int?) {
            this.productRoleAssignID = productRoleAssignID
        }
    }
}