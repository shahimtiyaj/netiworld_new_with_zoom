package com.netizen.netiworld.model.UserPoint

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class UnpaidFees {
    @SerializedName("message")
    @Expose
    private var message: String? = null

    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null

    @SerializedName("item")
    @Expose
    private var item: List<Item>? = null

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<Item?>? {
        return item
    }

    fun setItem(item: List<Item>?) {
        this.item = item
    }


    class Item {
        @SerializedName("invoiceId")
        @Expose
        private var invoiceId: String? = null

        @SerializedName("studentName")
        @Expose
        private var studentName: String? = null

        @SerializedName("studentRoll")
        @Expose
        private var studentRoll: Int? = null

        @SerializedName("studentCustomId")
        @Expose
        private var studentCustomId: String? = null

        @SerializedName("studentId")
        @Expose
        private var studentId: String? = null

        @SerializedName("mobileNo")
        @Expose
        private var mobileNo: String? = null

        @SerializedName("feeHeadsName")
        @Expose
        private var feeHeadsName: String? = null

        @SerializedName("feeSubHeadsName")
        @Expose
        private var feeSubHeadsName: String? = null

        @SerializedName("classConfigName")
        @Expose
        private var classConfigName: Any? = null

        @SerializedName("ledgerName")
        @Expose
        private var ledgerName: Any? = null

        @SerializedName("payableAmt")
        @Expose
        private var payableAmt: Float? = null

        @SerializedName("paidAmt")
        @Expose
        private var paidAmt: Float? = null

        @SerializedName("dueAmt")
        @Expose
        private var dueAmt: Float? = null

        @SerializedName("date")
        @Expose
        private var date: String? = null

        fun getInvoiceId(): String? {
            return invoiceId
        }

        fun setInvoiceId(invoiceId: String?) {
            this.invoiceId = invoiceId
        }

        fun getStudentName(): String? {
            return studentName
        }

        fun setStudentName(studentName: String?) {
            this.studentName = studentName
        }

        fun getStudentRoll(): Int? {
            return studentRoll
        }

        fun setStudentRoll(studentRoll: Int?) {
            this.studentRoll = studentRoll
        }

        fun getStudentCustomId(): String? {
            return studentCustomId
        }

        fun setStudentCustomId(studentCustomId: String?) {
            this.studentCustomId = studentCustomId
        }

        fun getStudentId(): String? {
            return studentId
        }

        fun setStudentId(studentId: String?) {
            this.studentId = studentId
        }

        fun getMobileNo(): String? {
            return mobileNo
        }

        fun setMobileNo(mobileNo: String?) {
            this.mobileNo = mobileNo
        }

        fun getFeeHeadsName(): String? {
            return feeHeadsName
        }

        fun setFeeHeadsName(feeHeadsName: String?) {
            this.feeHeadsName = feeHeadsName
        }

        fun getFeeSubHeadsName(): String? {
            return feeSubHeadsName
        }

        fun setFeeSubHeadsName(feeSubHeadsName: String?) {
            this.feeSubHeadsName = feeSubHeadsName
        }

        fun getClassConfigName(): Any? {
            return classConfigName
        }

        fun setClassConfigName(classConfigName: Any?) {
            this.classConfigName = classConfigName
        }

        fun getLedgerName(): Any? {
            return ledgerName
        }

        fun setLedgerName(ledgerName: Any?) {
            this.ledgerName = ledgerName
        }

        fun getPayableAmt(): Float? {
            return payableAmt
        }

        fun setPayableAmt(payableAmt: Float?) {
            this.payableAmt = payableAmt
        }

        fun getPaidAmt(): Float? {
            return paidAmt
        }

        fun setPaidAmt(paidAmt: Float?) {
            this.paidAmt = paidAmt
        }

        fun getDueAmt(): Float? {
            return dueAmt
        }

        fun setDueAmt(dueAmt: Float?) {
            this.dueAmt = dueAmt
        }

        fun getDate(): String? {
            return date
        }

        fun setDate(date: String?) {
            this.date = date
        }

    }

}