package com.netizen.netiworld.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class ReferenceInfo : Serializable {

    @SerializedName("referenceId")
    @Expose
    private var referenceId: Int? = null

    @SerializedName("referenceSerial")
    @Expose
    private var referenceSerial: Any? = null

    @SerializedName("referenceName")
    @Expose
    private var referenceName: String? = null

    @SerializedName("referenceDesignation")
    @Expose
    private var referenceDesignation: String? = null

    @SerializedName("orgainizationName")
    @Expose
    private var orgainizationName: String? = null

    @SerializedName("referenceMobile")
    @Expose
    private var referenceMobile: String? = null

    @SerializedName("referenceEmail")
    @Expose
    private var referenceEmail: String? = null

    @SerializedName("referenceAddress")
    @Expose
    private var referenceAddress: String? = null

    @SerializedName("relationInfoDTO")
    @Expose
    private var relationInfoDTO: RelationInfoDTO? = null

    @SerializedName("userBasicInfoDTO")
    @Expose
    private var userBasicInfoDTO: Any? = null

    fun getReferenceId(): Int? {
        return referenceId
    }

    fun setReferenceId(referenceId: Int?) {
        this.referenceId = referenceId
    }

    fun getReferenceSerial(): Any? {
        return referenceSerial
    }

    fun setReferenceSerial(referenceSerial: Any?) {
        this.referenceSerial = referenceSerial
    }

    fun getReferenceName(): String? {
        return referenceName
    }

    fun setReferenceName(referenceName: String?) {
        this.referenceName = referenceName
    }

    fun getReferenceDesignation(): String? {
        return referenceDesignation
    }

    fun setReferenceDesignation(referenceDesignation: String?) {
        this.referenceDesignation = referenceDesignation
    }

    fun getOrgainizationName(): String? {
        return orgainizationName
    }

    fun setOrgainizationName(orgainizationName: String?) {
        this.orgainizationName = orgainizationName
    }

    fun getReferenceMobile(): String? {
        return referenceMobile
    }

    fun setReferenceMobile(referenceMobile: String?) {
        this.referenceMobile = referenceMobile
    }

    fun getReferenceEmail(): String? {
        return referenceEmail
    }

    fun setReferenceEmail(referenceEmail: String?) {
        this.referenceEmail = referenceEmail
    }

    fun getReferenceAddress(): String? {
        return referenceAddress
    }

    fun setReferenceAddress(referenceAddress: String?) {
        this.referenceAddress = referenceAddress
    }

    fun getRelationInfoDTO(): RelationInfoDTO? {
        return relationInfoDTO
    }

    fun setRelationInfoDTO(relationInfoDTO: RelationInfoDTO?) {
        this.relationInfoDTO = relationInfoDTO
    }

    fun getUserBasicInfoDTO(): Any? {
        return userBasicInfoDTO
    }

    fun setUserBasicInfoDTO(userBasicInfoDTO: Any?) {
        this.userBasicInfoDTO = userBasicInfoDTO
    }

    class RelationInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }

    }
}