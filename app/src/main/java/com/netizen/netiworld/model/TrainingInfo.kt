package com.netizen.netiworld.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class TrainingInfo : Serializable {

    @SerializedName("trainingInfoId")
    @Expose
    private var trainingInfoId: Int? = null

    @SerializedName("trainingSerial")
    @Expose
    private var trainingSerial: Any? = null

    @SerializedName("trainingTitle")
    @Expose
    private var trainingTitle: String? = null

    @SerializedName("topicCoveredDetails")
    @Expose
    private var topicCoveredDetails: String? = null

    @SerializedName("instituteName")
    @Expose
    private var instituteName: String? = null

    @SerializedName("instituteLocation")
    @Expose
    private var instituteLocation: String? = null

    @SerializedName("trainingDuration")
    @Expose
    private var trainingDuration: String? = null

    @SerializedName("countryInfoDTO")
    @Expose
    private var countryInfoDTO: CountryInfoDTO? = null

    @SerializedName("trainingYearInfoDTO")
    @Expose
    private var trainingYearInfoDTO: TrainingYearInfoDTO? = null

    fun getTrainingInfoId(): Int? {
        return trainingInfoId
    }

    fun setTrainingInfoId(trainingInfoId: Int?) {
        this.trainingInfoId = trainingInfoId
    }

    fun getTrainingSerial(): Any? {
        return trainingSerial
    }

    fun setTrainingSerial(trainingSerial: Any?) {
        this.trainingSerial = trainingSerial
    }

    fun getTrainingTitle(): String? {
        return trainingTitle
    }

    fun setTrainingTitle(trainingTitle: String?) {
        this.trainingTitle = trainingTitle
    }

    fun getTopicCoveredDetails(): String? {
        return topicCoveredDetails
    }

    fun setTopicCoveredDetails(topicCoveredDetails: String?) {
        this.topicCoveredDetails = topicCoveredDetails
    }

    fun getInstituteName(): String? {
        return instituteName
    }

    fun setInstituteName(instituteName: String?) {
        this.instituteName = instituteName
    }

    fun getInstituteLocation(): String? {
        return instituteLocation
    }

    fun setInstituteLocation(instituteLocation: String?) {
        this.instituteLocation = instituteLocation
    }

    fun getTrainingDuration(): String? {
        return trainingDuration
    }

    fun setTrainingDuration(trainingDuration: String?) {
        this.trainingDuration = trainingDuration
    }

    fun getCountryInfoDTO(): CountryInfoDTO? {
        return countryInfoDTO
    }

    fun setCountryInfoDTO(countryInfoDTO: CountryInfoDTO?) {
        this.countryInfoDTO = countryInfoDTO
    }

    fun getTrainingYearInfoDTO(): TrainingYearInfoDTO? {
        return trainingYearInfoDTO
    }

    fun setTrainingYearInfoDTO(trainingYearInfoDTO: TrainingYearInfoDTO?) {
        this.trainingYearInfoDTO = trainingYearInfoDTO
    }

    class CountryInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }
    }

    class TrainingYearInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }
    }
}