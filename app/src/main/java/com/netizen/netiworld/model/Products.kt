package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
class Products {

    @SerializedName("productRoleAssignID")
    @Expose
    private var productRoleAssignID: Int? = null
    @SerializedName("assignEnableStatus")
    @Expose
    private var assignEnableStatus: Int? = null
    @SerializedName("productInfoDTO")
    @Expose
    private var productInfoDTO: ProductInfoDTO? = null

    fun getProductRoleAssignID(): Int? {
        return productRoleAssignID
    }

    fun setProductRoleAssignID(productRoleAssignID: Int?) {
        this.productRoleAssignID = productRoleAssignID
    }

    fun getAssignEnableStatus(): Int? {
        return assignEnableStatus
    }

    fun setAssignEnableStatus(assignEnableStatus: Int?) {
        this.assignEnableStatus = assignEnableStatus
    }

    fun getProductInfoDTO(): ProductInfoDTO? {
        return productInfoDTO
    }

    fun setProductInfoDTO(productInfoDTO: ProductInfoDTO?) {
        this.productInfoDTO = productInfoDTO
    }

    class ProductInfoDTO {

        @SerializedName("productID")
        @Expose
        private var productID: Int? = null

        @SerializedName("productName")
        @Expose
        private var productName: String? = null

        @SerializedName("salesPrice")
        @Expose
        private var salesPrice: Double? = null

        @SerializedName("percentVat")
        @Expose
        private var percentVat: Double? = null

        fun getProductID(): Int? {
            return productID
        }

        fun setProductID(productID: Int?) {
            this.productID = productID
        }

        fun getProductName(): String? {
            return productName
        }

        fun setProductName(productName: String) {
            this.productName = productName
        }

        fun getSalesPrice(): Double? {
            return salesPrice
        }

        fun setSalesPrice(salesPrice: Double?) {
            this.salesPrice = salesPrice
        }

        fun getPercentVat(): Double? {
            return percentVat
        }

        fun setPercentVat(percentVat: Double?) {
            this.percentVat = percentVat
        }
    }
}
