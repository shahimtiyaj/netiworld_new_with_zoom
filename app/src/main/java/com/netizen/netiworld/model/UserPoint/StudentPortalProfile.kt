package com.netizen.netiworld.model.UserPoint

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class StudentPortalProfile {

    @SerializedName("logoName")
    @Expose
    private var logoName: String? = null

    @SerializedName("fatherName")
    @Expose
    private var fatherName: String? = null

    @SerializedName("country")
    @Expose
    private var country: String? = null

    @SerializedName("occupation")
    @Expose
    private var occupation: String? = null

    @SerializedName("lastEducation")
    @Expose
    private var lastEducation: String? = null

    @SerializedName("imagePath")
    @Expose
    private var imagePath: String? = null

    @SerializedName("postalCode")
    @Expose
    private var postalCode: String? = null

    @SerializedName("studentDOB")
    @Expose
    private var studentDOB: String? = null

    @SerializedName("birthCertificateNo")
    @Expose
    private var birthCertificateNo: Any? = null

    @SerializedName("studentRoll")
    @Expose
    private var studentRoll: Int? = null

    @SerializedName("mothersNID")
    @Expose
    private var mothersNID: String? = null

    @SerializedName("relation")
    @Expose
    private var relation: String? = null

    @SerializedName("division")
    @Expose
    private var division: String? = null

    @SerializedName("shiftName")
    @Expose
    private var shiftName: String? = null

    @SerializedName("bloodGroup")
    @Expose
    private var bloodGroup: String? = null

    @SerializedName("academicYear")
    @Expose
    private var academicYear: String? = null

    @SerializedName("admissionYear")
    @Expose
    private var admissionYear: String? = null

    @SerializedName("houseNo")
    @Expose
    private var houseNo: String? = null

    @SerializedName("previousInstituteCode")
    @Expose
    private var previousInstituteCode: String? = null

    @SerializedName("instituteMobile")
    @Expose
    private var instituteMobile: String? = null

    @SerializedName("customStudentId")
    @Expose
    private var customStudentId: String? = null

    @SerializedName("identificationId")
    @Expose
    private var identificationId: Int? = null

    @SerializedName("postOffice")
    @Expose
    private var postOffice: String? = null

    @SerializedName("height")
    @Expose
    private var height: Any? = null

    @SerializedName("presentPostalCode")
    @Expose
    private var presentPostalCode: String? = null

    @SerializedName("admissionDate")
    @Expose
    private var admissionDate: String? = null

    @SerializedName("imageName")
    @Expose
    private var imageName: String? = null

    @SerializedName("tcNo")
    @Expose
    private var tcNo: Any? = null

    @SerializedName("addressType")
    @Expose
    private var addressType: String? = null

    @SerializedName("logoPath")
    @Expose
    private var logoPath: String? = null

    @SerializedName("examName")
    @Expose
    private var examName: String? = null

    @SerializedName("motherName")
    @Expose
    private var motherName: String? = null

    @SerializedName("presentHouseNo")
    @Expose
    private var presentHouseNo: String? = null

    @SerializedName("weight")
    @Expose
    private var weight: Any? = null

    @SerializedName("presentThana")
    @Expose
    private var presentThana: String? = null

    @SerializedName("exPoint")
    @Expose
    private var exPoint: String? = null

    @SerializedName("presentDivision")
    @Expose
    private var presentDivision: String? = null

    @SerializedName("passingYear")
    @Expose
    private var passingYear: String? = null

    @SerializedName("groupName")
    @Expose
    private var groupName: String? = null

    @SerializedName("nationality")
    @Expose
    private var nationality: String? = null

    @SerializedName("studentName")
    @Expose
    private var studentName: String? = null

    @SerializedName("district")
    @Expose
    private var district: String? = null

    @SerializedName("presentVillage")
    @Expose
    private var presentVillage: String? = null

    @SerializedName("timePeriod")
    @Expose
    private var timePeriod: String? = null

    @SerializedName("instituteId")
    @Expose
    private var instituteId: String? = null

    @SerializedName("regId")
    @Expose
    private var regId: String? = null

    @SerializedName("guardianEmail")
    @Expose
    private var guardianEmail: String? = null

    @SerializedName("presentPostOffice")
    @Expose
    private var presentPostOffice: String? = null

    @SerializedName("presentDistrict")
    @Expose
    private var presentDistrict: String? = null

    @SerializedName("className")
    @Expose
    private var className: String? = null

    @SerializedName("instituteAddress")
    @Expose
    private var instituteAddress: String? = null

    @SerializedName("instituteName")
    @Expose
    private var instituteName: String? = null

    @SerializedName("sectionName")
    @Expose
    private var sectionName: String? = null

    @SerializedName("studentId")
    @Expose
    private var studentId: String? = null

    @SerializedName("studentEmail")
    @Expose
    private var studentEmail: String? = null

    @SerializedName("studentGender")
    @Expose
    private var studentGender: String? = null

    @SerializedName("classConfigId")
    @Expose
    private var classConfigId: String? = null

    @SerializedName("roadNo")
    @Expose
    private var roadNo: String? = null

    @SerializedName("guardianName")
    @Expose
    private var guardianName: String? = null

    @SerializedName("studentReligion")
    @Expose
    private var studentReligion: String? = null

    @SerializedName("exgroup")
    @Expose
    private var exgroup: String? = null

    @SerializedName("institutePhone")
    @Expose
    private var institutePhone: String? = null

    @SerializedName("village")
    @Expose
    private var village: String? = null

    @SerializedName("exGrade")
    @Expose
    private var exGrade: String? = null

    @SerializedName("guardianEducation")
    @Expose
    private var guardianEducation: String? = null

    @SerializedName("monthlyIncome")
    @Expose
    private var monthlyIncome: String? = null

    @SerializedName("previousStdRoll")
    @Expose
    private var previousStdRoll: String? = null

    @SerializedName("specialDisease")
    @Expose
    private var specialDisease: Any? = null

    @SerializedName("fathersNID")
    @Expose
    private var fathersNID: String? = null

    @SerializedName("guardianMobile")
    @Expose
    private var guardianMobile: String? = null

    @SerializedName("admissionCategory")
    @Expose
    private var admissionCategory: String? = null

    @SerializedName("studentMobile")
    @Expose
    private var studentMobile: String? = null

    @SerializedName("previousInstituteAddress")
    @Expose
    private var previousInstituteAddress: String? = null

    @SerializedName("presentRoadNo")
    @Expose
    private var presentRoadNo: String? = null

    @SerializedName("instituteEmail")
    @Expose
    private var instituteEmail: String? = null

    @SerializedName("thana")
    @Expose
    private var thana: String? = null

    @SerializedName("exSession")
    @Expose
    private var exSession: String? = null

    @SerializedName("board")
    @Expose
    private var board: String? = null

    @SerializedName("previousInstituteName")
    @Expose
    private var previousInstituteName: String? = null

    fun getLogoName(): String? {
        return logoName
    }

    fun setLogoName(logoName: String?) {
        this.logoName = logoName
    }

    fun getFatherName(): String? {
        return fatherName
    }

    fun setFatherName(fatherName: String?) {
        this.fatherName = fatherName
    }

    fun getCountry(): String? {
        return country
    }

    fun setCountry(country: String?) {
        this.country = country
    }

    fun getOccupation(): String? {
        return occupation
    }

    fun setOccupation(occupation: String?) {
        this.occupation = occupation
    }

    fun getLastEducation(): String? {
        return lastEducation
    }

    fun setLastEducation(lastEducation: String?) {
        this.lastEducation = lastEducation
    }

    fun getImagePath(): String? {
        return imagePath
    }

    fun setImagePath(imagePath: String?) {
        this.imagePath = imagePath
    }

    fun getPostalCode(): String? {
        return postalCode
    }

    fun setPostalCode(postalCode: String?) {
        this.postalCode = postalCode
    }

    fun getStudentDOB(): String? {
        return studentDOB
    }

    fun setStudentDOB(studentDOB: String?) {
        this.studentDOB = studentDOB
    }

    fun getBirthCertificateNo(): Any? {
        return birthCertificateNo
    }

    fun setBirthCertificateNo(birthCertificateNo: Any?) {
        this.birthCertificateNo = birthCertificateNo
    }

    fun getStudentRoll(): Int? {
        return studentRoll
    }

    fun setStudentRoll(studentRoll: Int?) {
        this.studentRoll = studentRoll
    }

    fun getMothersNID(): String? {
        return mothersNID
    }

    fun setMothersNID(mothersNID: String?) {
        this.mothersNID = mothersNID
    }

    fun getRelation(): String? {
        return relation
    }

    fun setRelation(relation: String?) {
        this.relation = relation
    }

    fun getDivision(): String? {
        return division
    }

    fun setDivision(division: String?) {
        this.division = division
    }

    fun getShiftName(): String? {
        return shiftName
    }

    fun setShiftName(shiftName: String?) {
        this.shiftName = shiftName
    }

    fun getBloodGroup(): String? {
        return bloodGroup
    }

    fun setBloodGroup(bloodGroup: String?) {
        this.bloodGroup = bloodGroup
    }

    fun getAcademicYear(): String? {
        return academicYear
    }

    fun setAcademicYear(academicYear: String?) {
        this.academicYear = academicYear
    }

    fun getAdmissionYear(): String? {
        return admissionYear
    }

    fun setAdmissionYear(admissionYear: String?) {
        this.admissionYear = admissionYear
    }

    fun getHouseNo(): String? {
        return houseNo
    }

    fun setHouseNo(houseNo: String?) {
        this.houseNo = houseNo
    }

    fun getPreviousInstituteCode(): String? {
        return previousInstituteCode
    }

    fun setPreviousInstituteCode(previousInstituteCode: String?) {
        this.previousInstituteCode = previousInstituteCode
    }

    fun getInstituteMobile(): String? {
        return instituteMobile
    }

    fun setInstituteMobile(instituteMobile: String?) {
        this.instituteMobile = instituteMobile
    }

    fun getCustomStudentId(): String? {
        return customStudentId
    }

    fun setCustomStudentId(customStudentId: String?) {
        this.customStudentId = customStudentId
    }

    fun getIdentificationId(): Int? {
        return identificationId
    }

    fun setIdentificationId(identificationId: Int?) {
        this.identificationId = identificationId
    }

    fun getPostOffice(): String? {
        return postOffice
    }

    fun setPostOffice(postOffice: String?) {
        this.postOffice = postOffice
    }

    fun getHeight(): Any? {
        return height
    }

    fun setHeight(height: Any?) {
        this.height = height
    }

    fun getPresentPostalCode(): String? {
        return presentPostalCode
    }

    fun setPresentPostalCode(presentPostalCode: String?) {
        this.presentPostalCode = presentPostalCode
    }

    fun getAdmissionDate(): String? {
        return admissionDate
    }

    fun setAdmissionDate(admissionDate: String?) {
        this.admissionDate = admissionDate
    }

    fun getImageName(): String? {
        return imageName
    }

    fun setImageName(imageName: String?) {
        this.imageName = imageName
    }

    fun getTcNo(): Any? {
        return tcNo
    }

    fun setTcNo(tcNo: Any?) {
        this.tcNo = tcNo
    }

    fun getAddressType(): String? {
        return addressType
    }

    fun setAddressType(addressType: String?) {
        this.addressType = addressType
    }

    fun getLogoPath(): String? {
        return logoPath
    }

    fun setLogoPath(logoPath: String?) {
        this.logoPath = logoPath
    }

    fun getExamName(): String? {
        return examName
    }

    fun setExamName(examName: String?) {
        this.examName = examName
    }

    fun getMotherName(): String? {
        return motherName
    }

    fun setMotherName(motherName: String?) {
        this.motherName = motherName
    }

    fun getPresentHouseNo(): String? {
        return presentHouseNo
    }

    fun setPresentHouseNo(presentHouseNo: String?) {
        this.presentHouseNo = presentHouseNo
    }

    fun getWeight(): Any? {
        return weight
    }

    fun setWeight(weight: Any?) {
        this.weight = weight
    }

    fun getPresentThana(): String? {
        return presentThana
    }

    fun setPresentThana(presentThana: String?) {
        this.presentThana = presentThana
    }

    fun getExPoint(): String? {
        return exPoint
    }

    fun setExPoint(exPoint: String?) {
        this.exPoint = exPoint
    }

    fun getPresentDivision(): String? {
        return presentDivision
    }

    fun setPresentDivision(presentDivision: String?) {
        this.presentDivision = presentDivision
    }

    fun getPassingYear(): String? {
        return passingYear
    }

    fun setPassingYear(passingYear: String?) {
        this.passingYear = passingYear
    }

    fun getGroupName(): String? {
        return groupName
    }

    fun setGroupName(groupName: String?) {
        this.groupName = groupName
    }

    fun getNationality(): String? {
        return nationality
    }

    fun setNationality(nationality: String?) {
        this.nationality = nationality
    }

    fun getStudentName(): String? {
        return studentName
    }

    fun setStudentName(studentName: String?) {
        this.studentName = studentName
    }

    fun getDistrict(): String? {
        return district
    }

    fun setDistrict(district: String?) {
        this.district = district
    }

    fun getPresentVillage(): String? {
        return presentVillage
    }

    fun setPresentVillage(presentVillage: String?) {
        this.presentVillage = presentVillage
    }

    fun getTimePeriod(): String? {
        return timePeriod
    }

    fun setTimePeriod(timePeriod: String?) {
        this.timePeriod = timePeriod
    }

    fun getInstituteId(): String? {
        return instituteId
    }

    fun setInstituteId(instituteId: String?) {
        this.instituteId = instituteId
    }

    fun getRegId(): String? {
        return regId
    }

    fun setRegId(regId: String?) {
        this.regId = regId
    }

    fun getGuardianEmail(): String? {
        return guardianEmail
    }

    fun setGuardianEmail(guardianEmail: String?) {
        this.guardianEmail = guardianEmail
    }

    fun getPresentPostOffice(): String? {
        return presentPostOffice
    }

    fun setPresentPostOffice(presentPostOffice: String?) {
        this.presentPostOffice = presentPostOffice
    }

    fun getPresentDistrict(): String? {
        return presentDistrict
    }

    fun setPresentDistrict(presentDistrict: String?) {
        this.presentDistrict = presentDistrict
    }

    fun getClassName(): String? {
        return className
    }

    fun setClassName(className: String?) {
        this.className = className
    }

    fun getInstituteAddress(): String? {
        return instituteAddress
    }

    fun setInstituteAddress(instituteAddress: String?) {
        this.instituteAddress = instituteAddress
    }

    fun getInstituteName(): String? {
        return instituteName
    }

    fun setInstituteName(instituteName: String?) {
        this.instituteName = instituteName
    }

    fun getSectionName(): String? {
        return sectionName
    }

    fun setSectionName(sectionName: String?) {
        this.sectionName = sectionName
    }

    fun getStudentId(): String? {
        return studentId
    }

    fun setStudentId(studentId: String?) {
        this.studentId = studentId
    }

    fun getStudentEmail(): String? {
        return studentEmail
    }

    fun setStudentEmail(studentEmail: String?) {
        this.studentEmail = studentEmail
    }

    fun getStudentGender(): String? {
        return studentGender
    }

    fun setStudentGender(studentGender: String?) {
        this.studentGender = studentGender
    }

    fun getClassConfigId(): String? {
        return classConfigId
    }

    fun setClassConfigId(classConfigId: String?) {
        this.classConfigId = classConfigId
    }

    fun getRoadNo(): String? {
        return roadNo
    }

    fun setRoadNo(roadNo: String?) {
        this.roadNo = roadNo
    }

    fun getGuardianName(): String? {
        return guardianName
    }

    fun setGuardianName(guardianName: String?) {
        this.guardianName = guardianName
    }

    fun getStudentReligion(): String? {
        return studentReligion
    }

    fun setStudentReligion(studentReligion: String?) {
        this.studentReligion = studentReligion
    }

    fun getExgroup(): String? {
        return exgroup
    }

    fun setExgroup(exgroup: String?) {
        this.exgroup = exgroup
    }

    fun getInstitutePhone(): String? {
        return institutePhone
    }

    fun setInstitutePhone(institutePhone: String?) {
        this.institutePhone = institutePhone
    }

    fun getVillage(): String? {
        return village
    }

    fun setVillage(village: String?) {
        this.village = village
    }

    fun getExGrade(): String? {
        return exGrade
    }

    fun setExGrade(exGrade: String?) {
        this.exGrade = exGrade
    }

    fun getGuardianEducation(): String? {
        return guardianEducation
    }

    fun setGuardianEducation(guardianEducation: String?) {
        this.guardianEducation = guardianEducation
    }

    fun getMonthlyIncome(): String? {
        return monthlyIncome
    }

    fun setMonthlyIncome(monthlyIncome: String?) {
        this.monthlyIncome = monthlyIncome
    }

    fun getPreviousStdRoll(): String? {
        return previousStdRoll
    }

    fun setPreviousStdRoll(previousStdRoll: String?) {
        this.previousStdRoll = previousStdRoll
    }

    fun getSpecialDisease(): Any? {
        return specialDisease
    }

    fun setSpecialDisease(specialDisease: Any?) {
        this.specialDisease = specialDisease
    }

    fun getFathersNID(): String? {
        return fathersNID
    }

    fun setFathersNID(fathersNID: String?) {
        this.fathersNID = fathersNID
    }

    fun getGuardianMobile(): String? {
        return guardianMobile
    }

    fun setGuardianMobile(guardianMobile: String?) {
        this.guardianMobile = guardianMobile
    }

    fun getAdmissionCategory(): String? {
        return admissionCategory
    }

    fun setAdmissionCategory(admissionCategory: String?) {
        this.admissionCategory = admissionCategory
    }

    fun getStudentMobile(): String? {
        return studentMobile
    }

    fun setStudentMobile(studentMobile: String?) {
        this.studentMobile = studentMobile
    }

    fun getPreviousInstituteAddress(): String? {
        return previousInstituteAddress
    }

    fun setPreviousInstituteAddress(previousInstituteAddress: String?) {
        this.previousInstituteAddress = previousInstituteAddress
    }

    fun getPresentRoadNo(): String? {
        return presentRoadNo
    }

    fun setPresentRoadNo(presentRoadNo: String?) {
        this.presentRoadNo = presentRoadNo
    }

    fun getInstituteEmail(): String? {
        return instituteEmail
    }

    fun setInstituteEmail(instituteEmail: String?) {
        this.instituteEmail = instituteEmail
    }

    fun getThana(): String? {
        return thana
    }

    fun setThana(thana: String?) {
        this.thana = thana
    }

    fun getExSession(): String? {
        return exSession
    }

    fun setExSession(exSession: String?) {
        this.exSession = exSession
    }

    fun getBoard(): String? {
        return board
    }

    fun setBoard(board: String?) {
        this.board = board
    }

    fun getPreviousInstituteName(): String? {
        return previousInstituteName
    }

    fun setPreviousInstituteName(previousInstituteName: String?) {
        this.previousInstituteName = previousInstituteName
    }
}