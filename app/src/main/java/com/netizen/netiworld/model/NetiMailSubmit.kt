package com.netizen.netiworld.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class NetiMailSubmit {
    @SerializedName("receiverBasicInfoDTO")
    @Expose
    private var receiverBasicInfoDTO: ReceiverBasicInfoDTO? = null

    @SerializedName("chatboxBody")
    @Expose
    private var chatboxBody: String? = null

    @SerializedName("attachContent")
    @Expose
    private var attachContent: String? = null

    @SerializedName("attachSaveOrEditable")
    @Expose
    private var attachSaveOrEditable: Boolean? = null

    @SerializedName("chatboxAttachmentName")
    @Expose
    private var chatboxAttachmentName: String? = null

  constructor(){

    }

    constructor(
        receiverBasicInfoDTO: ReceiverBasicInfoDTO?,
        chatboxBody: String?,
        attachContent: String?,
        attachSaveOrEditable: Boolean?,
        chatboxAttachmentName: String?
    ) {
        this.receiverBasicInfoDTO = receiverBasicInfoDTO
        this.chatboxBody = chatboxBody
        this.attachContent = attachContent
        this.attachSaveOrEditable = attachSaveOrEditable
        this.chatboxAttachmentName = chatboxAttachmentName
    }

    fun getReceiverBasicInfoDTO(): ReceiverBasicInfoDTO? {
        return receiverBasicInfoDTO
    }

    fun setReceiverBasicInfoDTO(receiverBasicInfoDTO: ReceiverBasicInfoDTO?) {
        this.receiverBasicInfoDTO = receiverBasicInfoDTO
    }

    fun getChatboxBody(): String? {
        return chatboxBody
    }

    fun setChatboxBody(chatboxBody: String?) {
        this.chatboxBody = chatboxBody
    }

    fun getAttachContent(): String? {
        return attachContent
    }

    fun setAttachContent(attachContent: String?) {
        this.attachContent = attachContent
    }

    fun getAttachSaveOrEditable(): Boolean? {
        return attachSaveOrEditable
    }

    fun setAttachSaveOrEditable(attachSaveOrEditable: Boolean?) {
        this.attachSaveOrEditable = attachSaveOrEditable
    }

    fun getChatboxAttachmentName(): String? {
        return chatboxAttachmentName
    }

    fun setChatboxAttachmentName(chatboxAttachmentName: String?) {
        this.chatboxAttachmentName = chatboxAttachmentName
    }


    class ReceiverBasicInfoDTO {
        @SerializedName("netiID")
        @Expose
        private var netiID: Int? = null

        constructor(
        ) {
        }

        constructor(
            netiID: Int?
        ) {
            this.netiID = netiID
        }

        fun getNetiID(): Int? {
            return netiID
        }

        fun setNetiID(netiID: Int?) {
            this.netiID = netiID
        }

    }

}