package com.netizen.netiworld.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class TagGetData : Serializable {

    @SerializedName("taggingTypeCoreCategoryInfoDTO")
    @Expose
    private var taggingTypeCoreCategoryInfoDTO: TaggingTypeCoreCategoryInfoDTO? = null

    @SerializedName("userBankAccountInfoDTO")
    @Expose
    private var userBankAccountInfoDTO: UserBankAccountInfoDTO? = null

    fun getTaggingTypeCoreCategoryInfoDTO(): TaggingTypeCoreCategoryInfoDTO? {
        return taggingTypeCoreCategoryInfoDTO
    }

    fun setTaggingTypeCoreCategoryInfoDTO(taggingTypeCoreCategoryInfoDTO: TaggingTypeCoreCategoryInfoDTO?) {
        this.taggingTypeCoreCategoryInfoDTO = taggingTypeCoreCategoryInfoDTO
    }

    fun getUserBankAccountInfoDTO(): UserBankAccountInfoDTO? {
        return userBankAccountInfoDTO
    }

    fun setUserBankAccountInfoDTO(userBankAccountInfoDTO: UserBankAccountInfoDTO?) {
        this.userBankAccountInfoDTO = userBankAccountInfoDTO
    }

    class TaggingTypeCoreCategoryInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }
    }

    class UserBankAccountInfoDTO {

        @SerializedName("userBankAccId")
        @Expose
        private var userBankAccId: Int? = null

        @SerializedName("bankAccNumber")
        @Expose
        private var bankAccNumber: String? = null

        @SerializedName("bankAccHolderName")
        @Expose
        private var bankAccHolderName: String? = null

        @SerializedName("coreBankBranchInfoDTO")
        @Expose
        private var coreBankBranchInfoDTO: CoreBankBranchInfoDTO? = null

        fun getUserBankAccId(): Int? {
            return userBankAccId
        }

        fun setUserBankAccId(userBankAccId: Int?) {
            this.userBankAccId = userBankAccId
        }

        fun getBankAccNumber(): String? {
            return bankAccNumber
        }

        fun setBankAccNumber(bankAccNumber: String?) {
            this.bankAccNumber = bankAccNumber
        }

        fun getBankAccHolderName(): String? {
            return bankAccHolderName
        }

        fun setBankAccHolderName(bankAccHolderName: String?) {
            this.bankAccHolderName = bankAccHolderName
        }

        fun getCoreBankBranchInfoDTO(): CoreBankBranchInfoDTO? {
            return coreBankBranchInfoDTO
        }

        fun setCoreBankBranchInfoDTO(coreBankBranchInfoDTO: CoreBankBranchInfoDTO?) {
            this.coreBankBranchInfoDTO = coreBankBranchInfoDTO
        }


        class CoreBankBranchInfoDTO {

            @SerializedName("branchID")
            @Expose
            private var branchID: Int? = null

            @SerializedName("branchName")
            @Expose
            private var branchName: String? = null

            @SerializedName("routingNumber")
            @Expose
            private var routingNumber: String? = null

            @SerializedName("coreBankInfoDTO")
            @Expose
            private var coreBankInfoDTO: CoreBankInfoDTO? = null

            @SerializedName("coreDistrictInfoDTO")
            @Expose
            private var coreDistrictInfoDTO: Any? = null

            fun getBranchID(): Int? {
                return branchID
            }

            fun setBranchID(branchID: Int?) {
                this.branchID = branchID
            }

            fun getBranchName(): String? {
                return branchName
            }

            fun setBranchName(branchName: String?) {
                this.branchName = branchName
            }

            fun getRoutingNumber(): String? {
                return routingNumber
            }

            fun setRoutingNumber(routingNumber: String?) {
                this.routingNumber = routingNumber
            }

            fun getCoreBankInfoDTO(): CoreBankInfoDTO? {
                return coreBankInfoDTO
            }

            fun setCoreBankInfoDTO(coreBankInfoDTO: CoreBankInfoDTO?) {
                this.coreBankInfoDTO = coreBankInfoDTO
            }

            fun getCoreDistrictInfoDTO(): Any? {
                return coreDistrictInfoDTO
            }

            fun setCoreDistrictInfoDTO(coreDistrictInfoDTO: Any?) {
                this.coreDistrictInfoDTO = coreDistrictInfoDTO
            }

            class CoreBankInfoDTO {

                @SerializedName("coreCategoryID")
                @Expose
                private var coreCategoryID: Int? = null

                @SerializedName("categoryDefaultCode")
                @Expose
                private var categoryDefaultCode: String? = null

                @SerializedName("categoryName")
                @Expose
                private var categoryName: String? = null

                fun getCoreCategoryID(): Int? {
                    return coreCategoryID
                }

                fun setCoreCategoryID(coreCategoryID: Int?) {
                    this.coreCategoryID = coreCategoryID
                }

                fun getCategoryDefaultCode(): String? {
                    return categoryDefaultCode
                }

                fun setCategoryDefaultCode(categoryDefaultCode: String?) {
                    this.categoryDefaultCode = categoryDefaultCode
                }

                fun getCategoryName(): String? {
                    return categoryName
                }

                fun setCategoryName(categoryName: String?) {
                    this.categoryName = categoryName
                }
            }
        }
    }
}