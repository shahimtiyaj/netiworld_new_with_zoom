package com.netizen.netiworld.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class PostReferenceInfo {

    @SerializedName("relationInfoDTO")
    @Expose
    private var relationInfoDTO: RelationInfoDTO? = null

    @SerializedName("referenceName")
    @Expose
    private var referenceName: String? = null

    @SerializedName("referenceDesignation")
    @Expose
    private var referenceDesignation: String? = null

    @SerializedName("orgainizationName")
    @Expose
    private var orgainizationName: String? = null

    @SerializedName("referenceMobile")
    @Expose
    private var referenceMobile: String? = null

    @SerializedName("referenceEmail")
    @Expose
    private var referenceEmail: String? = null

    @SerializedName("referenceAddress")
    @Expose
    private var referenceAddress: String? = null

    fun getRelationInfoDTO(): RelationInfoDTO? {
        return relationInfoDTO
    }

    fun setRelationInfoDTO(relationInfoDTO: RelationInfoDTO?) {
        this.relationInfoDTO = relationInfoDTO
    }

    fun getReferenceName(): String? {
        return referenceName
    }

    fun setReferenceName(referenceName: String?) {
        this.referenceName = referenceName
    }

    fun getReferenceDesignation(): String? {
        return referenceDesignation
    }

    fun setReferenceDesignation(referenceDesignation: String?) {
        this.referenceDesignation = referenceDesignation
    }

    fun getOrgainizationName(): String? {
        return orgainizationName
    }

    fun setOrgainizationName(orgainizationName: String?) {
        this.orgainizationName = orgainizationName
    }

    fun getReferenceMobile(): String? {
        return referenceMobile
    }

    fun setReferenceMobile(referenceMobile: String?) {
        this.referenceMobile = referenceMobile
    }

    fun getReferenceEmail(): String? {
        return referenceEmail
    }

    fun setReferenceEmail(referenceEmail: String?) {
        this.referenceEmail = referenceEmail
    }

    fun getReferenceAddress(): String? {
        return referenceAddress
    }

    fun setReferenceAddress(referenceAddress: String?) {
        this.referenceAddress = referenceAddress
    }

    class RelationInfoDTO {
        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: String? = null

        fun getCoreCategoryID(): String? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: String?) {
            this.coreCategoryID = coreCategoryID
        }
    }
}


