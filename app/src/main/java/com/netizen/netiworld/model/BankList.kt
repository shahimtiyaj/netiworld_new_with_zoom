package com.netizen.netiworld.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class BankList {

    @SerializedName("bank")
    @Expose
    private var bank: String? = null

    @SerializedName("bankID")
    @Expose
    private var bankID: Int? = null

    @SerializedName("accNumber")
    @Expose
    private var accNumber: String? = null

    @SerializedName("holderName")
    @Expose
    private var holderName: String? = null

    @SerializedName("accountDetail")
    @Expose
    private var accountDetail: String? = null

    @SerializedName("branchID")
    @Expose
    private var branchID: Int? = null

    @SerializedName("branchName")
    @Expose
    private var branchName: String? = null

    @SerializedName("districtID")
    @Expose
    private var districtID: Int? = null

    @SerializedName("districtName")
    @Expose
    private var districtName: String? = null

    @SerializedName("routingNumber")
    @Expose
    private var routingNumber: String? = null

    @SerializedName("chequeSlipPath")
    @Expose
    private var chequeSlipPath: String? = null

    @SerializedName("chequeSlipName")
    @Expose
    private var chequeSlipName: String? = null

    @SerializedName("othersAttachmentName")
    @Expose
    private var othersAttachmentName: String? = null

    @SerializedName("othersAttachmentPath")
    @Expose
    private var othersAttachmentPath: String? = null

    @SerializedName("enableStatus")
    @Expose
    private var enableStatus: Int? = null

    @SerializedName("taggingID")
    @Expose
    private var taggingID: Any? = null

    @SerializedName("taggingDate")
    @Expose
    private var taggingDate: Any? = null

    @SerializedName("userBankAccountID")
    @Expose
    private var userBankAccountID: Int? = null

    @SerializedName("netiID")
    @Expose
    private var netiID: Int? = null

    @SerializedName("taggingTypeDefaultCodes")
    @Expose
    private var taggingTypeDefaultCodes: Any? = null

    fun getBank(): String? {
        return bank
    }

    fun setBank(bank: String?) {
        this.bank = bank
    }

    fun getBankID(): Int? {
        return bankID
    }

    fun setBankID(bankID: Int?) {
        this.bankID = bankID
    }

    fun getAccNumber(): String? {
        return accNumber
    }

    fun setAccNumber(accNumber: String?) {
        this.accNumber = accNumber
    }

    fun getHolderName(): String? {
        return holderName
    }

    fun setHolderName(holderName: String?) {
        this.holderName = holderName
    }

    fun getAccountDetail(): String? {
        return accountDetail
    }

    fun setAccountDetail(accountDetail: String?) {
        this.accountDetail = accountDetail
    }

    fun getBranchID(): Int? {
        return branchID
    }

    fun setBranchID(branchID: Int?) {
        this.branchID = branchID
    }

    fun getBranchName(): String? {
        return branchName
    }

    fun setBranchName(branchName: String?) {
        this.branchName = branchName
    }

    fun getDistrictID(): Int? {
        return districtID
    }

    fun setDistrictID(districtID: Int?) {
        this.districtID = districtID
    }

    fun getDistrictName(): String? {
        return districtName
    }

    fun setDistrictName(districtName: String?) {
        this.districtName = districtName
    }

    fun getRoutingNumber(): String? {
        return routingNumber
    }

    fun setRoutingNumber(routingNumber: String?) {
        this.routingNumber = routingNumber
    }

    fun getChequeSlipPath(): String? {
        return chequeSlipPath
    }

    fun setChequeSlipPath(chequeSlipPath: String?) {
        this.chequeSlipPath = chequeSlipPath
    }

    fun getChequeSlipName(): String? {
        return chequeSlipName
    }

    fun setChequeSlipName(chequeSlipName: String?) {
        this.chequeSlipName = chequeSlipName
    }

    fun getOthersAttachmentName(): String? {
        return othersAttachmentName
    }

    fun setOthersAttachmentName(othersAttachmentName: String?) {
        this.othersAttachmentName = othersAttachmentName
    }

    fun getOthersAttachmentPath(): String? {
        return othersAttachmentPath
    }

    fun setOthersAttachmentPath(othersAttachmentPath: String?) {
        this.othersAttachmentPath = othersAttachmentPath
    }

    fun getEnableStatus(): Int? {
        return enableStatus
    }

    fun setEnableStatus(enableStatus: Int?) {
        this.enableStatus = enableStatus
    }

    fun getTaggingID(): Any? {
        return taggingID
    }

    fun setTaggingID(taggingID: Any?) {
        this.taggingID = taggingID
    }

    fun getTaggingDate(): Any? {
        return taggingDate
    }

    fun setTaggingDate(taggingDate: Any?) {
        this.taggingDate = taggingDate
    }

    fun getUserBankAccountID(): Int? {
        return userBankAccountID
    }

    fun setUserBankAccountID(userBankAccountID: Int?) {
        this.userBankAccountID = userBankAccountID
    }

    fun getNetiID(): Int? {
        return netiID
    }

    fun setNetiID(netiID: Int?) {
        this.netiID = netiID
    }

    fun getTaggingTypeDefaultCodes(): Any? {
        return taggingTypeDefaultCodes
    }

    fun setTaggingTypeDefaultCodes(taggingTypeDefaultCodes: Any?) {
        this.taggingTypeDefaultCodes = taggingTypeDefaultCodes
    }
}