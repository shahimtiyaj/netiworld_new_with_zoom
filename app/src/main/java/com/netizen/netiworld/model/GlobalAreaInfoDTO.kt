package com.netizen.netiworld.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GlobalAreaInfoDTO {

    @SerializedName("coreCategoryID")
    @Expose
    private var coreCategoryID: Int? = null

    @SerializedName("categoryDefaultCode")
    @Expose
    private var categoryDefaultCode: String? = null

    @SerializedName("categoryName")
    @Expose
    private var categoryName: String? = null

    @SerializedName("categoryNote")
    @Expose
    private var categoryNote: Any? = null

    @SerializedName("categoryEnableStatus")
    @Expose
    private var categoryEnableStatus: Int? = null

    @SerializedName("categorySerial")
    @Expose
    private var categorySerial: Int? = null

    @SerializedName("typeStatus")
    @Expose
    private var typeStatus: Int? = null

    @SerializedName("parentStatus")
    @Expose
    private var parentStatus: Int? = null

    @SerializedName("parentTypeInfoDTO")
    @Expose
    private var parentTypeInfoDTO: Any? = null

    @SerializedName("parentCoreCategoryInfoDTO")
    @Expose
    private var parentCoreCategoryInfoDTO: Any? = null

    @SerializedName("lastUserExecuted")
    @Expose
    private var lastUserExecuted: String? = null

    @SerializedName("lastIpExecuted")
    @Expose
    private var lastIpExecuted: String? = null

    @SerializedName("lastDateExecuted")
    @Expose
    private var lastDateExecuted: Int? = null

    fun getCoreCategoryID(): Int? {
        return coreCategoryID
    }

    fun setCoreCategoryID(coreCategoryID: Int?) {
        this.coreCategoryID = coreCategoryID
    }

    fun getCategoryDefaultCode(): String? {
        return categoryDefaultCode
    }

    fun setCategoryDefaultCode(categoryDefaultCode: String) {
        this.categoryDefaultCode = categoryDefaultCode
    }

    fun getCategoryName(): String? {
        return categoryName
    }

    fun setCategoryName(categoryName: String) {
        this.categoryName = categoryName
    }

    fun getCategoryNote(): Any? {
        return categoryNote
    }

    fun setCategoryNote(categoryNote: Any) {
        this.categoryNote = categoryNote
    }

    fun getCategoryEnableStatus(): Int? {
        return categoryEnableStatus
    }

    fun setCategoryEnableStatus(categoryEnableStatus: Int?) {
        this.categoryEnableStatus = categoryEnableStatus
    }

    fun getCategorySerial(): Int? {
        return categorySerial
    }

    fun setCategorySerial(categorySerial: Int?) {
        this.categorySerial = categorySerial
    }

    fun getTypeStatus(): Int? {
        return typeStatus
    }

    fun setTypeStatus(typeStatus: Int?) {
        this.typeStatus = typeStatus
    }

    fun getParentStatus(): Int? {
        return parentStatus
    }

    fun setParentStatus(parentStatus: Int?) {
        this.parentStatus = parentStatus
    }

    fun getParentTypeInfoDTO(): Any? {
        return parentTypeInfoDTO
    }

    fun setParentTypeInfoDTO(parentTypeInfoDTO: Any) {
        this.parentTypeInfoDTO = parentTypeInfoDTO
    }

    fun getParentCoreCategoryInfoDTO(): Any? {
        return parentCoreCategoryInfoDTO
    }

    fun setParentCoreCategoryInfoDTO(parentCoreCategoryInfoDTO: Any) {
        this.parentCoreCategoryInfoDTO = parentCoreCategoryInfoDTO
    }

    fun getLastUserExecuted(): String? {
        return lastUserExecuted
    }

    fun setLastUserExecuted(lastUserExecuted: String) {
        this.lastUserExecuted = lastUserExecuted
    }

    fun getLastIpExecuted(): String? {
        return lastIpExecuted
    }

    fun setLastIpExecuted(lastIpExecuted: String) {
        this.lastIpExecuted = lastIpExecuted
    }

    fun getLastDateExecuted(): Int? {
        return lastDateExecuted
    }

    fun setLastDateExecuted(lastDateExecuted: Int?) {
        this.lastDateExecuted = lastDateExecuted
    }


    class Info {

        @SerializedName("netiID")
        @Expose
        private var netiID: Int? = null
        @SerializedName("customNetiID")
        @Expose
        private var customNetiID: Int? = null
        @SerializedName("fullName")
        @Expose
        private var fullName: String? = null
        @SerializedName("gender")
        @Expose
        private var gender: String? = null
        @SerializedName("religion")
        @Expose
        private var religion: String? = null
        @SerializedName("dateOfBirth")
        @Expose
        private var dateOfBirth: String? = null
        @SerializedName("basicMobile")
        @Expose
        private var basicMobile: String? = null
        @SerializedName("basicEmail")
        @Expose
        private var basicEmail: String? = null
        @SerializedName("imagePath")
        @Expose
        private var imagePath: String? = null
        @SerializedName("imageName")
        @Expose
        private var imageName: String? = null
        @SerializedName("userWalletBalance")
        @Expose
        private var userWalletBalance: Float? = null
        @SerializedName("userReserveBalance")
        @Expose
        private var userReserveBalance: Float? = null
        @SerializedName("smsBalance")
        @Expose
        private var smsBalance: Float? = null
        @SerializedName("voiceBalance")
        @Expose
        private var voiceBalance: Float? = null
        @SerializedName("emailBalance")
        @Expose
        private var emailBalance: Float? = null
        @SerializedName("validationStatus")
        @Expose
        private var validationStatus: Int? = null
        @SerializedName("userEnableStatus")
        @Expose
        private var userEnableStatus: Int? = null
        @SerializedName("registrationDate")
        @Expose
        private var registrationDate: Int? = null
        @SerializedName("userName")
        @Expose
        private var userName: Any? = null
        @SerializedName("userPassword")
        @Expose
        private var userPassword: Any? = null
        @SerializedName("imageContent")
        @Expose
        private var imageContent: Any? = null
        @SerializedName("imageSaveOrEditable")
        @Expose
        private var imageSaveOrEditable: Boolean? = null
        @SerializedName("globalAreaInfoDTO")
        @Expose
        private var globalAreaInfoDTO: GlobalAreaInfoDTO? = null
        @SerializedName("lastUserExecuted")
        @Expose
        private var lastUserExecuted: String? = null
        @SerializedName("lastIpExecuted")
        @Expose
        private var lastIpExecuted: String? = null
        @SerializedName("lastDateExecuted")
        @Expose
        private var lastDateExecuted: Int? = null

        fun getNetiID(): Int? {
            return netiID
        }

        fun setNetiID(netiID: Int?) {
            this.netiID = netiID
        }

        fun getCustomNetiID(): Int? {
            return customNetiID
        }

        fun setCustomNetiID(customNetiID: Int?) {
            this.customNetiID = customNetiID
        }

        fun getFullName(): String? {
            return fullName
        }

        fun setFullName(fullName: String) {
            this.fullName = fullName
        }

        fun getGender(): String? {
            return gender
        }

        fun setGender(gender: String) {
            this.gender = gender
        }

        fun getReligion(): String? {
            return religion
        }

        fun setReligion(religion: String) {
            this.religion = religion
        }

        fun getDateOfBirth(): String? {
            return dateOfBirth
        }

        fun setDateOfBirth(dateOfBirth: String) {
            this.dateOfBirth = dateOfBirth
        }

        fun getBasicMobile(): String? {
            return basicMobile
        }

        fun setBasicMobile(basicMobile: String) {
            this.basicMobile = basicMobile
        }

        fun getBasicEmail(): String? {
            return basicEmail
        }

        fun setBasicEmail(basicEmail: String) {
            this.basicEmail = basicEmail
        }

        fun getImagePath(): String? {
            return imagePath
        }

        fun setImagePath(imagePath: String) {
            this.imagePath = imagePath
        }

        fun getImageName(): String? {
            return imageName
        }

        fun setImageName(imageName: String) {
            this.imageName = imageName
        }

        fun getUserWalletBalance(): Float? {
            return userWalletBalance
        }

        fun setUserWalletBalance(userWalletBalance: Float?) {
            this.userWalletBalance = userWalletBalance
        }

        fun getUserReserveBalance(): Float? {
            return userReserveBalance
        }

        fun setUserReserveBalance(userReserveBalance: Float?) {
            this.userReserveBalance = userReserveBalance
        }

        fun getSmsBalance(): Float? {
            return smsBalance
        }

        fun setSmsBalance(smsBalance: Float?) {
            this.smsBalance = smsBalance
        }

        fun getVoiceBalance(): Float? {
            return voiceBalance
        }

        fun setVoiceBalance(voiceBalance: Float?) {
            this.voiceBalance = voiceBalance
        }

        fun getEmailBalance(): Float? {
            return emailBalance
        }

        fun setEmailBalance(emailBalance: Float?) {
            this.emailBalance = emailBalance
        }

        fun getValidationStatus(): Int? {
            return validationStatus
        }

        fun setValidationStatus(validationStatus: Int?) {
            this.validationStatus = validationStatus
        }

        fun getUserEnableStatus(): Int? {
            return userEnableStatus
        }

        fun setUserEnableStatus(userEnableStatus: Int?) {
            this.userEnableStatus = userEnableStatus
        }

        fun getRegistrationDate(): Int? {
            return registrationDate
        }

        fun setRegistrationDate(registrationDate: Int?) {
            this.registrationDate = registrationDate
        }

        fun getUserName(): Any? {
            return userName
        }

        fun setUserName(userName: Any) {
            this.userName = userName
        }

        fun getUserPassword(): Any? {
            return userPassword
        }

        fun setUserPassword(userPassword: Any) {
            this.userPassword = userPassword
        }

        fun getImageContent(): Any? {
            return imageContent
        }

        fun setImageContent(imageContent: Any) {
            this.imageContent = imageContent
        }

        fun getImageSaveOrEditable(): Boolean? {
            return imageSaveOrEditable
        }

        fun setImageSaveOrEditable(imageSaveOrEditable: Boolean?) {
            this.imageSaveOrEditable = imageSaveOrEditable
        }

        fun getGlobalAreaInfoDTO(): GlobalAreaInfoDTO? {
            return globalAreaInfoDTO
        }

        fun setGlobalAreaInfoDTO(globalAreaInfoDTO: GlobalAreaInfoDTO) {
            this.globalAreaInfoDTO = globalAreaInfoDTO
        }

        fun getLastUserExecuted(): String? {
            return lastUserExecuted
        }

        fun setLastUserExecuted(lastUserExecuted: String) {
            this.lastUserExecuted = lastUserExecuted
        }

        fun getLastIpExecuted(): String? {
            return lastIpExecuted
        }

        fun setLastIpExecuted(lastIpExecuted: String) {
            this.lastIpExecuted = lastIpExecuted
        }

        fun getLastDateExecuted(): Int? {
            return lastDateExecuted
        }

        fun setLastDateExecuted(lastDateExecuted: Int?) {
            this.lastDateExecuted = lastDateExecuted
        }

    }


}
