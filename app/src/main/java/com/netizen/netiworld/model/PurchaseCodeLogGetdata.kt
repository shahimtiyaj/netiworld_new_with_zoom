package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
class PurchaseCodeLogGetdata {

    @SerializedName("purchaseDate")
    private var purchaseDate: String? = null

    @SerializedName("coreRoleNote") // purchase point
    private var coreRoleNote: String? = null

    @SerializedName("purchaseCode") //Product Quantity
    private var purchaseCode: String? = null

    @SerializedName("usedDate") // Product Used Date
    private var usedDate: String? = null

    @SerializedName("productPurchaseLogDTO")
    @Expose
    var productPurchaseLogDTO: ProductPurchaseLogDTO? = null



    constructor() {

    }

    fun getPurchaseDate(): String? {
        return purchaseDate
    }

    fun setPurchaseDate(purchaseDate: String) {
        this.purchaseDate = purchaseDate
    }

    fun getCoreRoleNote(): String? {
        return coreRoleNote
    }

    fun setCoreRoleNote(coreRoleNote: String) {
        this.coreRoleNote = coreRoleNote
    }

    fun getPurchaseCode(): String? {
        return purchaseCode
    }

    fun setPurchaseCode(purchaseCode: String) {
        this.purchaseCode = purchaseCode
    }

    fun getProductUseDate(): String? {
        return usedDate
    }

    fun setProductUseDate(usedDate: String) {
        this.usedDate = usedDate
    }

    class ProductInfoDTO {
        @SerializedName("productName")
        @Expose
        private var productName: String? = null

        @SerializedName("productTypeInfoDTO")
        @Expose
        var productTypeInfoDTO: ProductTypeInfoDTO? = null

        fun getProductName(): String? {
            return productName
        }

        fun setProductName(productName: String) {
            this.productName = productName
        }
    }

    class ProductTypeInfoDTO {

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        fun getProductType(): String? {
            return categoryName
        }

        fun setProductType(categoryName: String) {
            this.categoryName = categoryName
        }
    }

    class ProductPurchaseLogDTO {

        @SerializedName("productInfoDTO")
        @Expose
        var productInfoDTO: ProductInfoDTO? = null

    }

}

