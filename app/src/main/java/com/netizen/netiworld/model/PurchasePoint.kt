package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
class PurchasePoint {

    @SerializedName("userRoleAssignID")
    @Expose
    private var userRoleAssignID: Int? = null

    @SerializedName("coreRoleID")
    @Expose
    private var coreRoleID: Int? = null

    @SerializedName("coreRoleName")
    @Expose
    private var coreRoleName: String? = null

    constructor() {
    }

    constructor(coreRoleName: String?) {
        this.coreRoleName = coreRoleName
    }

    constructor(userRoleAssignID: Int?, coreRoleID: Int?, coreRoleName: String?) {
        this.userRoleAssignID = userRoleAssignID
        this.coreRoleID = coreRoleID
        this.coreRoleName = coreRoleName
    }

    fun getUserRoleAssignID(): Long? {
        return userRoleAssignID?.toLong()
    }

    fun setUserRoleAssignID(userRoleAssignID: Long?) {
        this.userRoleAssignID = userRoleAssignID!!.toInt()
    }

    fun getCoreRoleID(): Long? {
        return coreRoleID?.toLong()
    }

    fun setCoreRoleID(coreRoleID: Long?) {
        this.coreRoleID = coreRoleID!!.toInt()
    }

    fun getCoreRoleName(): String? {
        return coreRoleName
    }

    fun setCoreRoleName(coreRoleName: String) {
        this.coreRoleName = coreRoleName
    }

}
