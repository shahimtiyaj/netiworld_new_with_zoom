package com.netizen.netiworld.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BankAccountPostData {

    @SerializedName("coreCategoryInfoDTO")
    @Expose
    private var coreCategoryInfoDTO: CoreCategoryInfoDTO? = null

    @SerializedName("bankAccNumber")
    @Expose
    private var bankAccNumber: String? = null

    @SerializedName("bankAccHolderName")
    @Expose
    private var bankAccHolderName: String? = null

    @SerializedName("bankNote")
    @Expose
    private var bankNote: String? = null

    @SerializedName("chequeSlipEditable")
    @Expose
    private var chequeSlipEditable: Boolean? = null

    @SerializedName("chequeSlipName")
    @Expose
    private var chequeSlipName: String? = null

    @SerializedName("chequeSlipContent")
    @Expose
    private var chequeSlipContent: String? = null

    @SerializedName("othersAttachmentEditable")
    @Expose
    private var othersAttachmentEditable: Boolean? = null

    @SerializedName("othersAttachmentName")
    @Expose
    private var othersAttachmentName: String? = null

    @SerializedName("othersAttachmentContent")
    @Expose
    private var othersAttachmentContent: String? = null

    @SerializedName("coreBankBranchInfoDTO")
    @Expose
    private var coreBankBranchInfoDTO: CoreBankBranchInfoDTO? = null

    fun getCoreCategoryInfoDTO(): CoreCategoryInfoDTO? {
        return coreCategoryInfoDTO
    }

    fun setCoreCategoryInfoDTO(coreCategoryInfoDTO: CoreCategoryInfoDTO?) {
        this.coreCategoryInfoDTO = coreCategoryInfoDTO
    }

    fun getBankAccNumber(): String? {
        return bankAccNumber
    }

    fun setBankAccNumber(bankAccNumber: String?) {
        this.bankAccNumber = bankAccNumber
    }

    fun getBankAccHolderName(): String? {
        return bankAccHolderName
    }

    fun setBankAccHolderName(bankAccHolderName: String?) {
        this.bankAccHolderName = bankAccHolderName
    }

    fun getBankNote(): String? {
        return bankNote
    }

    fun setBankNote(bankNote: String?) {
        this.bankNote = bankNote
    }

    fun getChequeSlipEditable(): Boolean? {
        return chequeSlipEditable
    }

    fun setChequeSlipEditable(chequeSlipEditable: Boolean?) {
        this.chequeSlipEditable = chequeSlipEditable
    }

    fun getChequeSlipName(): String? {
        return chequeSlipName
    }

    fun setChequeSlipName(chequeSlipName: String?) {
        this.chequeSlipName = chequeSlipName
    }

    fun getChequeSlipContent(): String? {
        return chequeSlipContent
    }

    fun setChequeSlipContent(chequeSlipContent: String?) {
        this.chequeSlipContent = chequeSlipContent
    }

    fun getOthersAttachmentEditable(): Boolean? {
        return othersAttachmentEditable
    }

    fun setOthersAttachmentEditable(othersAttachmentEditable: Boolean?) {
        this.othersAttachmentEditable = othersAttachmentEditable
    }

    fun getOthersAttachmentName(): String? {
        return othersAttachmentName
    }

    fun setOthersAttachmentName(othersAttachmentName: String?) {
        this.othersAttachmentName = othersAttachmentName
    }

    fun getOthersAttachmentContent(): String? {
        return othersAttachmentContent
    }

    fun setOthersAttachmentContent(othersAttachmentContent: String?) {
        this.othersAttachmentContent = othersAttachmentContent
    }

    fun getCoreBankBranchInfoDTO(): CoreBankBranchInfoDTO? {
        return coreBankBranchInfoDTO
    }

    fun setCoreBankBranchInfoDTO(coreBankBranchInfoDTO: CoreBankBranchInfoDTO?) {
        this.coreBankBranchInfoDTO = coreBankBranchInfoDTO
    }

    class CoreBankBranchInfoDTO {

        @SerializedName("branchID")
        @Expose
        var branchID: Int? = null

        fun getBranchId(): Int? {
            return branchID
        }

        fun setBranchId(branchId: Int?) {
            this.branchID = branchId
        }
    }

    class CoreCategoryInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        var coreCategoryID: Int? = null

        fun getCoreCategoryId(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryId(coreCategoryId: Int?) {
            this.coreCategoryID = coreCategoryId
        }
    }
}