package com.netizen.netiworld.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class EducationInfo : Serializable {

    @SerializedName("educationInfoId")
    @Expose
    private var educationInfoId: Int? = null

    @SerializedName("examTitle")
    @Expose
    private var examTitle: String? = null

    @SerializedName("eduInfoSerial")
    @Expose
    private var eduInfoSerial: Int? = null

    @SerializedName("instituteName")
    @Expose
    private var instituteName: String? = null

    @SerializedName("instituteForeginStatus")
    @Expose
    private var instituteForeginStatus: Int? = null

    @SerializedName("achievementDetails")
    @Expose
    private var achievementDetails: String? = null

    @SerializedName("eduDuration")
    @Expose
    private var eduDuration: String? = null

    @SerializedName("degreeInfoDTO")
    @Expose
    private var degreeInfoDTO: DegreeInfoDTO? = null

    @SerializedName("subjectInfoDTO")
    @Expose
    private var subjectInfoDTO: SubjectInfoDTO? = null

    @SerializedName("gradeInfoDTO")
    @Expose
    private var gradeInfoDTO: GradeInfoDTO? = null

    @SerializedName("passingYearInfoDTO")
    @Expose
    private var passingYearInfoDTO: PassingYearInfoDTO? = null

    @SerializedName("boardInfoDTO")
    @Expose
    private var boardInfoDTO: BoardInfoDTO? = null

    fun getEducationInfoId(): Int? {
        return educationInfoId
    }

    fun setEducationInfoId(educationInfoId: Int?) {
        this.educationInfoId = educationInfoId
    }

    fun getExamTitle(): String? {
        return examTitle
    }

    fun setExamTitle(examTitle: String?) {
        this.examTitle = examTitle
    }

    fun getEduInfoSerial(): Int? {
        return eduInfoSerial
    }

    fun setEduInfoSerial(eduInfoSerial: Int?) {
        this.eduInfoSerial = eduInfoSerial
    }

    fun getInstituteName(): String? {
        return instituteName
    }

    fun setInstituteName(instituteName: String?) {
        this.instituteName = instituteName
    }

    fun getInstituteForeginStatus(): Int? {
        return instituteForeginStatus
    }

    fun setInstituteForeginStatus(instituteForeginStatus: Int?) {
        this.instituteForeginStatus = instituteForeginStatus
    }

    fun getAchievementDetails(): String? {
        return achievementDetails
    }

    fun setAchievementDetails(achievementDetails: String?) {
        this.achievementDetails = achievementDetails
    }

    fun getEduDuration(): String? {
        return eduDuration
    }

    fun setEduDuration(eduDuration: String?) {
        this.eduDuration = eduDuration
    }

    fun getDegreeInfoDTO(): DegreeInfoDTO? {
        return degreeInfoDTO
    }

    fun setDegreeInfoDTO(degreeInfoDTO: DegreeInfoDTO?) {
        this.degreeInfoDTO = degreeInfoDTO
    }

    fun getSubjectInfoDTO(): SubjectInfoDTO? {
        return subjectInfoDTO
    }

    fun setSubjectInfoDTO(subjectInfoDTO: SubjectInfoDTO?) {
        this.subjectInfoDTO = subjectInfoDTO
    }

    fun getGradeInfoDTO(): GradeInfoDTO? {
        return gradeInfoDTO
    }

    fun setGradeInfoDTO(gradeInfoDTO: GradeInfoDTO?) {
        this.gradeInfoDTO = gradeInfoDTO
    }

    fun getPassingYearInfoDTO(): PassingYearInfoDTO? {
        return passingYearInfoDTO
    }

    fun setPassingYearInfoDTO(passingYearInfoDTO: PassingYearInfoDTO?) {
        this.passingYearInfoDTO = passingYearInfoDTO
    }

    fun getBoardInfoDTO(): BoardInfoDTO? {
        return boardInfoDTO
    }

    fun setBoardInfoDTO(boardInfoDTO: BoardInfoDTO?) {
        this.boardInfoDTO = boardInfoDTO
    }

    class BoardInfoDTO {
        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }

    }

    class DegreeInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }
    }

    class GradeInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }
    }

    class PassingYearInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }
    }

    class SubjectInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }

    }
}