package com.netizen.netiworld.model

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose




class OfferProduct {

    @SerializedName("productOfferID")
    @Expose
    private var productOfferID: Long? = null
    @SerializedName("offerCode")
    @Expose
    private var offerCode: String? = null
    @SerializedName("actualPrice")
    @Expose
    private var actualPrice: Float? = null
    @SerializedName("discountPercent")
    @Expose
    private var discountPercent: Float? = null
    @SerializedName("offerStartDate")
    @Expose
    private var offerStartDate: Long? = null
    @SerializedName("offerQuantity")
    @Expose
    private var offerQuantity: Long? = null
    @SerializedName("offerUseableTime")
    @Expose
    private var offerUseableTime: Long? = null
    @SerializedName("offerNote")
    @Expose
    private var offerNote: String? = null
    @SerializedName("offerUsed")
    @Expose
    private var offerUsed: Long? = null
    @SerializedName("productName")
    @Expose
    private var productName: String? = null



    fun getProductOfferID(): Long? {
        return productOfferID
    }

    fun setProductOfferID(productOfferID: Long?) {
        this.productOfferID = productOfferID
    }

    fun getOfferCode(): String? {
        return offerCode
    }

    fun setOfferCode(offerCode: String) {
        this.offerCode = offerCode
    }

    fun getActualPrice(): Float? {
        return actualPrice
    }

    fun setActualPrice(actualPrice: Float?) {
        this.actualPrice = actualPrice
    }

    fun getDiscountPercent(): Float? {
        return discountPercent
    }

    fun setDiscountPercent(discountPercent: Float?) {
        this.discountPercent = discountPercent
    }

    fun getOfferStartDate(): Long? {
        return offerStartDate
    }

    fun setOfferStartDate(offerStartDate: Long?) {
        this.offerStartDate = offerStartDate
    }

    fun getOfferQuantity(): Long? {
        return offerQuantity
    }

    fun setOfferQuantity(offerQuantity: Long?) {
        this.offerQuantity = offerQuantity
    }

    fun getOfferUseableTime(): Long? {
        return offerUseableTime
    }

    fun setOfferUseableTime(offerUseableTime: Long?) {
        this.offerUseableTime = offerUseableTime
    }

    fun getOfferNote(): String? {
        return offerNote
    }

    fun setOfferNote(offerNote: String) {
        this.offerNote = offerNote
    }

    fun getOfferUsed(): Long? {
        return offerUsed
    }

    fun setOfferUsed(offerUsed: Long?) {
        this.offerUsed = offerUsed
    }

    fun getProductName(): String? {
        return productName
    }

    fun setProductName(productName: String) {
        this.productName = productName
    }

}



