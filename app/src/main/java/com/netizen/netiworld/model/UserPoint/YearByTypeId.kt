package com.netizen.netiworld.model.UserPoint

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@JsonIgnoreProperties(ignoreUnknown = true)
class YearByTypeId {
    @SerializedName("message")
    @Expose
    private var message: String? = null

    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null

    @SerializedName("item")
    @Expose
    private var item: List<Item?>? = null

    /*@SerializedName("item")
    @Expose
    private var item: Item? = null*/

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    /*fun getItem(): Item? {
         return item
     }

     fun setItem(item: Item?) {
         this.item = item
     }*/

    fun getItem(): List<Item?>? {
        return item
    }

    fun setItem(item: List<Item?>?) {
        this.item = item
    }

    class Item {
        @SerializedName("id")
        @Expose
        private var id: Int? = null

        @SerializedName("name")
        @Expose
        private var name: String? = null

        @SerializedName("typeId")
        @Expose
        private var typeId: Int? = null

        @SerializedName("defaultId")
        @Expose
        private var defaultId: String? = null

        fun getId(): Int? {
            return id
        }

        fun setId(id: Int?) {
            this.id = id
        }

        fun getName(): String? {
            return name
        }

        fun setName(name: String?) {
            this.name = name
        }

        fun getTypeId(): Int? {
            return typeId
        }

        fun setTypeId(typeId: Int?) {
            this.typeId = typeId
        }

        fun getDefaultId(): String? {
            return defaultId
        }

        fun setDefaultId(defaultId: String?) {
            this.defaultId = defaultId
        }

    }

}