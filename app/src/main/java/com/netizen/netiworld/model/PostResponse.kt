package com.netizen.netiworld.model

import com.google.gson.annotations.SerializedName

class PostResponse {
    @SerializedName("access_token")
    var access_token: String? = ""

    @SerializedName("token_type")
    var token_type: String? = ""

    @SerializedName("refresh_token")
    var refresh_token: String? = ""

    @SerializedName("expires_in")
    var expires_in: String? = ""

    @SerializedName("scope")
    var scope: String? = ""

    @SerializedName("jti")
    var jti: String? = ""

    fun getToken(): String? {
        return access_token
    }

    fun setToken(access_token: String?) {
        this.access_token = access_token
    }

}