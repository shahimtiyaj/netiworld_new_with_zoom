package com.netizen.netiworld.model.UserPoint

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class ClassTestDetails {
    @SerializedName("message")
    @Expose
    private var message: Any? = null

    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null

    @SerializedName("item")
    @Expose
    private var item: Item? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): Item? {
        return item
    }

    fun setItem(item: Item?) {
        this.item = item
    }

    class Item {
        @SerializedName("customStudentId")
        @Expose
        private var customStudentId: String? = null

        @SerializedName("studentId")
        @Expose
        private var studentId: String? = null

        @SerializedName("studentRoll")
        @Expose
        private var studentRoll: Int? = null

        @SerializedName("sectionName")
        @Expose
        private var sectionName: String? = null

        @SerializedName("classTestName")
        @Expose
        private var classTestName: String? = null

        @SerializedName("studentName")
        @Expose
        private var studentName: String? = null

        @SerializedName("mobileNo")
        @Expose
        private var mobileNo: String? = null

        @SerializedName("obtainedMarks")
        @Expose
        private var obtainedMarks: Float? = null

        @SerializedName("totalMarks")
        @Expose
        private var totalMarks: Float? = null

        @SerializedName("stdCtExamMarks")
        @Expose
        private var stdCtExamMarks: List<StdCtExamMark>? = null

        fun getCustomStudentId(): String? {
            return customStudentId
        }

        fun setCustomStudentId(customStudentId: String?) {
            this.customStudentId = customStudentId
        }

        fun getStudentId(): String? {
            return studentId
        }

        fun setStudentId(studentId: String?) {
            this.studentId = studentId
        }

        fun getStudentRoll(): Int? {
            return studentRoll
        }

        fun setStudentRoll(studentRoll: Int?) {
            this.studentRoll = studentRoll
        }

        fun getSectionName(): String? {
            return sectionName
        }

        fun setSectionName(sectionName: String?) {
            this.sectionName = sectionName
        }

        fun getClassTestName(): String? {
            return classTestName
        }

        fun setClassTestName(classTestName: String?) {
            this.classTestName = classTestName
        }

        fun getStudentName(): String? {
            return studentName
        }

        fun setStudentName(studentName: String?) {
            this.studentName = studentName
        }

        fun getMobileNo(): String? {
            return mobileNo
        }

        fun setMobileNo(mobileNo: String?) {
            this.mobileNo = mobileNo
        }

        fun getObtainedMarks(): Float? {
            return obtainedMarks
        }

        fun setObtainedMarks(obtainedMarks: Float?) {
            this.obtainedMarks = obtainedMarks
        }

        fun getTotalMarks(): Float? {
            return totalMarks
        }

        fun setTotalMarks(totalMarks: Float?) {
            this.totalMarks = totalMarks
        }

        fun getStdCtExamMarks(): List<StdCtExamMark?>? {
            return stdCtExamMarks
        }

        fun setStdCtExamMarks(stdCtExamMarks: List<StdCtExamMark>?) {
            this.stdCtExamMarks = stdCtExamMarks
        }

        class StdCtExamMark {
            @SerializedName("viewSerial")
            @Expose
            private var viewSerial: Int? = null

            @SerializedName("subjectName")
            @Expose
            private var subjectName: String? = null

            @SerializedName("subjectFullMark")
            @Expose
            private var subjectFullMark: Float? = null

            @SerializedName("obtainedMark")
            @Expose
            private var obtainedMark: Float? = null

            @SerializedName("highestMarks")
            @Expose
            private var highestMarks: Float? = null

            fun getViewSerial(): Int? {
                return viewSerial
            }

            fun setViewSerial(viewSerial: Int?) {
                this.viewSerial = viewSerial
            }

            fun getSubjectName(): String? {
                return subjectName
            }

            fun setSubjectName(subjectName: String?) {
                this.subjectName = subjectName
            }

            fun getSubjectFullMark(): Float? {
                return subjectFullMark
            }

            fun setSubjectFullMark(subjectFullMark: Float?) {
                this.subjectFullMark = subjectFullMark
            }

            fun getObtainedMark(): Float? {
                return obtainedMark
            }

            fun setObtainedMark(obtainedMark: Float?) {
                this.obtainedMark = obtainedMark
            }

            fun getHighestMarks(): Float? {
                return highestMarks
            }

            fun setHighestMarks(highestMarks: Float?) {
                this.highestMarks = highestMarks
            }

        }

    }
}