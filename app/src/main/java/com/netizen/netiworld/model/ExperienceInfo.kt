package com.netizen.netiworld.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class ExperienceInfo : Serializable {

    @SerializedName("experienceId")
    @Expose
    private var experienceId: Int? = null

    @SerializedName("experienceSerial")
    @Expose
    private var experienceSerial: Any? = null

    @SerializedName("companyName")
    @Expose
    private var companyName: String? = null

    @SerializedName("companyBusiness")
    @Expose
    private var companyBusiness: String? = null

    @SerializedName("designationName")
    @Expose
    private var designationName: String? = null

    @SerializedName("workingDepartment")
    @Expose
    private var workingDepartment: String? = null

    @SerializedName("companyLocation")
    @Expose
    private var companyLocation: String? = null

    @SerializedName("employmentStart")
    @Expose
    private var employmentStart: Long? = null

    @SerializedName("employmentEnd")
    @Expose
    private var employmentEnd: Long? = null

    @SerializedName("responsibilityDetails")
    @Expose
    private var responsibilityDetails: String? = null

    @SerializedName("userBasicInfoDTO")
    @Expose
    private var userBasicInfoDTO: Any? = null

    @SerializedName("countryInfoDTO")
    @Expose
    private var countryInfoDTO: CountryInfoDTO? = null

    fun getExperienceId(): Int? {
        return experienceId
    }

    fun setExperienceId(experienceId: Int?) {
        this.experienceId = experienceId
    }

    fun getExperienceSerial(): Any? {
        return experienceSerial
    }

    fun setExperienceSerial(experienceSerial: Any?) {
        this.experienceSerial = experienceSerial
    }

    fun getCompanyName(): String? {
        return companyName
    }

    fun setCompanyName(companyName: String?) {
        this.companyName = companyName
    }

    fun getCompanyBusiness(): String? {
        return companyBusiness
    }

    fun setCompanyBusiness(companyBusiness: String?) {
        this.companyBusiness = companyBusiness
    }

    fun getDesignationName(): String? {
        return designationName
    }

    fun setDesignationName(designationName: String?) {
        this.designationName = designationName
    }

    fun getWorkingDepartment(): String? {
        return workingDepartment
    }

    fun setWorkingDepartment(workingDepartment: String?) {
        this.workingDepartment = workingDepartment
    }

    fun getCompanyLocation(): String? {
        return companyLocation
    }

    fun setCompanyLocation(companyLocation: String?) {
        this.companyLocation = companyLocation
    }

    fun getEmploymentStart(): Long? {
        return employmentStart
    }

    fun setEmploymentStart(employmentStart: Long?) {
        this.employmentStart = employmentStart
    }

    fun getEmploymentEnd(): Long? {
        return employmentEnd
    }

    fun setEmploymentEnd(employmentEnd: Long?) {
        this.employmentEnd = employmentEnd
    }

    fun getResponsibilityDetails(): String? {
        return responsibilityDetails
    }

    fun setResponsibilityDetails(responsibilityDetails: String?) {
        this.responsibilityDetails = responsibilityDetails
    }

    fun getUserBasicInfoDTO(): Any? {
        return userBasicInfoDTO
    }

    fun setUserBasicInfoDTO(userBasicInfoDTO: Any?) {
        this.userBasicInfoDTO = userBasicInfoDTO
    }

    fun getCountryInfoDTO(): CountryInfoDTO? {
        return countryInfoDTO
    }

    fun setCountryInfoDTO(countryInfoDTO: CountryInfoDTO?) {
        this.countryInfoDTO = countryInfoDTO
    }

    class CountryInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }
    }
}