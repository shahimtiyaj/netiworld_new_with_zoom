package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


@JsonIgnoreProperties(ignoreUnknown = true)
class MobileBankAcNo {

    @SerializedName("coreBankAccId")
    @Expose
    private var coreBankAccId: Int? = null

    @SerializedName("accShortName")
    @Expose
    private var accShortName: String? = null

    constructor() {

    }

    constructor(accShortName : String) {
        this.accShortName = accShortName
    }

    constructor(coreBankAccId : Int, accShortName : String) {
        this.coreBankAccId = coreBankAccId
        this.accShortName = accShortName
    }

    fun getCoreBankAccountId(): Int? {
        return coreBankAccId
    }

    fun setCoreBankAccountId(coreBankAccId: Int?) {
        this.coreBankAccId = coreBankAccId
    }

    fun getAccShortName(): String?{
        return accShortName
    }

    fun setAccShortName(accShortName: String) {
        this.accShortName = accShortName
    }

}