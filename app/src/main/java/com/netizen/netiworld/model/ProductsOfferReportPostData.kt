package com.netizen.netiworld.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class ProductsOfferReportPostData {

    @SerializedName("startDate")
    @Expose
    private var startDate: String? = null
    @SerializedName("endDate")
    @Expose
    private var endDate: String? = null
    @SerializedName("pageLimit")
    @Expose
    private var pageLimit: Int? = null
    @SerializedName("pageNo")
    @Expose
    private var pageNo: Int? = null

    fun getStartDate(): String? {
        return startDate
    }

    fun setStartDate(startDate: String?) {
        this.startDate = startDate
    }

    fun getEndDate(): String? {
        return endDate
    }

    fun setEndDate(endDate: String?) {
        this.endDate = endDate
    }

    fun getPageLimit(): Int? {
        return pageLimit
    }

    fun setPageLimit(pageLimit: Int?) {
        this.pageLimit = pageLimit
    }

    fun getPageNo(): Int? {
        return pageNo
    }

    fun setPageNo(pageNo: Int?) {
        this.pageNo = pageNo
    }
}

