package com.netizen.netiworld.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class HomePageInfo {

    @SerializedName("tokenSummary")
    @Expose
    private var tokenSummary: TokenSummary? = null
    @SerializedName("basicInfo")
    @Expose
    private var basicInfo: BasicInfo? = null

    fun getTokenSummary(): TokenSummary? {
        return tokenSummary
    }

    fun setTokenSummary(tokenSummary: TokenSummary?) {
        this.tokenSummary = tokenSummary
    }

    fun getBasicInfo(): BasicInfo? {
        return basicInfo
    }

    fun setBasicInfo(basicInfo: BasicInfo?) {
        this.basicInfo = basicInfo
    }

    class BasicInfo {

        @SerializedName("basicEmail")
        @Expose
        private var basicEmail: String? = null
        @SerializedName("basicMobile")
        @Expose
        private var basicMobile: String? = null
        @SerializedName("parentName")
        @Expose
        private var parentName: String? = null
        @SerializedName("userEnableStatus")
        @Expose
        private var userEnableStatus: Int? = null
        @SerializedName("smsBalance")
        @Expose
        private var smsBalance: Double? = null
        @SerializedName("customNetiID")
        @Expose
        private var customNetiID: Int? = null
        @SerializedName("userWalletBalance")
        @Expose
        private var userWalletBalance: Double? = null
        @SerializedName("parentCustomNetiID")
        @Expose
        private var parentCustomNetiID: Int? = null
        @SerializedName("validationStatus")
        @Expose
        private var validationStatus: Int? = null

        fun getBasicEmail(): String? {
            return basicEmail
        }

        fun setBasicEmail(basicEmail: String?) {
            this.basicEmail = basicEmail
        }

        fun getBasicMobile(): String? {
            return basicMobile
        }

        fun setBasicMobile(basicMobile: String?) {
            this.basicMobile = basicMobile
        }

        fun getParentName(): String? {
            return parentName
        }

        fun setParentName(parentName: String?) {
            this.parentName = parentName
        }

        fun getUserEnableStatus(): Int? {
            return userEnableStatus
        }

        fun setUserEnableStatus(userEnableStatus: Int?) {
            this.userEnableStatus = userEnableStatus
        }

        fun getSmsBalance(): Double? {
            return smsBalance
        }

        fun setSmsBalance(smsBalance: Double?) {
            this.smsBalance = smsBalance
        }

        fun getCustomNetiID(): Int? {
            return customNetiID
        }

        fun setCustomNetiID(customNetiID: Int?) {
            this.customNetiID = customNetiID
        }

        fun getUserWalletBalance(): Double? {
            return userWalletBalance
        }

        fun setUserWalletBalance(userWalletBalance: Double?) {
            this.userWalletBalance = userWalletBalance
        }

        fun getParentCustomNetiID(): Int? {
            return parentCustomNetiID
        }

        fun setParentCustomNetiID(parentCustomNetiID: Int?) {
            this.parentCustomNetiID = parentCustomNetiID
        }

        fun getValidationStatus(): Int? {
            return validationStatus
        }

        fun setValidationStatus(validationStatus: Int?) {
            this.validationStatus = validationStatus
        }
    }

    class TokenSummary {

        @SerializedName("rejected")
        @Expose
        private var rejected: Int? = null
        @SerializedName("pending")
        @Expose
        private var pending: Int? = null
        @SerializedName("solved")
        @Expose
        private var solved: Int? = null

        fun getRejected(): Int? {
            return rejected
        }

        fun setRejected(rejected: Int?) {
            this.rejected = rejected
        }

        fun getPending(): Int? {
            return pending
        }

        fun setPending(pending: Int?) {
            this.pending = pending
        }

        fun getSolved(): Int? {
            return solved
        }

        fun setSolved(solved: Int?) {
            this.solved = solved
        }
    }
}