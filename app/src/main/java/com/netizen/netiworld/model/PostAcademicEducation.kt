package com.netizen.netiworld.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PostAcademicEducation {
    @SerializedName("educationInfoId")
    @Expose
    private var educationInfoId: Int? = null

    @SerializedName("degreeInfoDTO")
    @Expose
    private var degreeInfoDTO: DegreeInfoDTO? = null

    @SerializedName("subjectInfoDTO")
    @Expose
    private var subjectInfoDTO: SubjectInfoDTO? = null

    @SerializedName("gradeInfoDTO")
    @Expose
    private var gradeInfoDTO: GradeInfoDTO? = null

    @SerializedName("passingYearInfoDTO")
    @Expose
    private var passingYearInfoDTO: PassingYearInfoDTO? = null

    @SerializedName("boardInfoDTO")
    @Expose
    private var boardInfoDTO: BoardInfoDTO? = null

    @SerializedName("eduInfoSerial")
    @Expose
    private var eduInfoSerial: String? = null

    @SerializedName("examTitle")
    @Expose
    private var examTitle: String? = null

    @SerializedName("eduDuration")
    @Expose
    private var eduDuration: String? = null

    @SerializedName("instituteName")
    @Expose
    private var instituteName: String? = null

    @SerializedName("instituteForeginStatus")
    @Expose
    private var instituteForeginStatus: Int? = null

    @SerializedName("achievementDetails")
    @Expose
    private var achievementDetails: String? = null

    fun getEducationInfoId(): Int? {
        return educationInfoId
    }

    fun setEducationInfoId(educationInfoId: Int?) {
        this.educationInfoId = educationInfoId
    }

    fun getDegreeInfoDTO(): DegreeInfoDTO? {
        return degreeInfoDTO
    }

    fun setDegreeInfoDTO(degreeInfoDTO: DegreeInfoDTO?) {
        this.degreeInfoDTO = degreeInfoDTO
    }

    fun getSubjectInfoDTO(): SubjectInfoDTO? {
        return subjectInfoDTO
    }

    fun setSubjectInfoDTO(subjectInfoDTO: SubjectInfoDTO?) {
        this.subjectInfoDTO = subjectInfoDTO
    }

    fun getGradeInfoDTO(): GradeInfoDTO? {
        return gradeInfoDTO
    }

    fun setGradeInfoDTO(gradeInfoDTO: GradeInfoDTO?) {
        this.gradeInfoDTO = gradeInfoDTO
    }

    fun getPassingYearInfoDTO(): PassingYearInfoDTO? {
        return passingYearInfoDTO
    }

    fun setPassingYearInfoDTO(passingYearInfoDTO: PassingYearInfoDTO?) {
        this.passingYearInfoDTO = passingYearInfoDTO
    }

    fun getBoardInfoDTO(): BoardInfoDTO? {
        return boardInfoDTO
    }

    fun setBoardInfoDTO(boardInfoDTO: BoardInfoDTO?) {
        this.boardInfoDTO = boardInfoDTO
    }

    fun getEduInfoSerial(): String? {
        return eduInfoSerial
    }

    fun setEduInfoSerial(eduInfoSerial: String?) {
        this.eduInfoSerial = eduInfoSerial
    }

    fun getExamTitle(): String? {
        return examTitle
    }

    fun setExamTitle(examTitle: String?) {
        this.examTitle = examTitle
    }

    fun getEduDuration(): String? {
        return eduDuration
    }

    fun setEduDuration(eduDuration: String?) {
        this.eduDuration = eduDuration
    }

    fun getInstituteName(): String? {
        return instituteName
    }

    fun setInstituteName(instituteName: String?) {
        this.instituteName = instituteName
    }

    fun getInstituteForeginStatus(): Int? {
        return instituteForeginStatus
    }

    fun setInstituteForeginStatus(instituteForeginStatus: Int?) {
        this.instituteForeginStatus = instituteForeginStatus
    }

    fun getAchievementDetails(): String? {
        return achievementDetails
    }

    fun setAchievementDetails(achievementDetails: String?) {
        this.achievementDetails = achievementDetails
    }

    class GradeInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: String? = null

        fun getCoreCategoryID(): String? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: String?) {
            this.coreCategoryID = coreCategoryID
        }
    }

    class PassingYearInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: String? = null

        fun getCoreCategoryID(): String? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: String?) {
            this.coreCategoryID = coreCategoryID
        }
    }

    class SubjectInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: String? = null

        fun getCoreCategoryID(): String? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: String?) {
            this.coreCategoryID = coreCategoryID
        }
    }

    class DegreeInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: String? = null

        fun getCoreCategoryID(): String? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: String?) {
            this.coreCategoryID = coreCategoryID
        }
    }

    class BoardInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: String? = null

        fun getCoreCategoryID(): String? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: String?) {
            this.coreCategoryID = coreCategoryID
        }
    }
}