package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


@JsonIgnoreProperties(ignoreUnknown = true)
class GeneralProductGetData {

    @SerializedName("purchaseDate")
    private var purchaseDate: String? = null

    @SerializedName("coreRoleNote") //Purchase Point
    private var coreRoleNote: String? = null

    @SerializedName("categoryName") //Product Type
    private var categoryName: String? = null

    @SerializedName("productName")
    private var productName: String? = null

    @SerializedName("purchaseQuantity") //Product Quantity
    private var purchaseQuantity: Long? = null

    @SerializedName("totalAmount") //Payable Amount
    private var totalAmount: Double? = null

    @SerializedName("productPurchaseID") //Payable Amount
    private var productPurchaseID: Int? = null

    @SerializedName("genCodeStatus") //Info view enable or disable
    private var genCodeStatus: Int? = null

    @SerializedName("productInfoDTO")
    private var productInfoDTO: ProductInfoDTO? = null

    fun getPurchaseDate(): String? {
        return purchaseDate
    }

    fun setPurchaseDate(purchaseDate: String) {
        this.purchaseDate = purchaseDate
    }

    fun getCoreRoleNote(): String? {
        return coreRoleNote
    }

    fun setCoreRoleNote(coreRoleNote: String) {
        this.coreRoleNote = coreRoleNote
    }

    fun getCategoryName(): String? {
        return categoryName
    }

    fun setCategoryName(categoryName: String) {
        this.categoryName = categoryName
    }

    fun getProductName(): String? {
        return productName
    }

    fun setProductName(productName: String) {
        this.productName = productName
    }

    fun getPurchaseQuantity(): Long? {
        return purchaseQuantity
    }

    fun setPurchaseQuantity(purchaseQuantity: Long) {
        this.purchaseQuantity = purchaseQuantity
    }

    fun getTotalAmount(): Double? {
        return totalAmount
    }

    fun setTotalAmount(totalAmount: Double) {
        this.totalAmount = totalAmount
    }

    fun getProductPurchaseID(): Int? {
        return productPurchaseID
    }

    fun setProductPurchaseID(productPurchaseID: Int) {
        this.productPurchaseID = productPurchaseID
    }

    fun getGenCodeStatus(): Int? {
        return genCodeStatus
    }

    fun setGenCodeStatus(genCodeStatus: Int) {
        this.genCodeStatus = genCodeStatus
    }

    fun getProductInfoDTO(): ProductInfoDTO? {
        return productInfoDTO
    }

    fun setProductInfoDTO(productInfoDTO: ProductInfoDTO?) {
        this.productInfoDTO = productInfoDTO
    }

    class ProductInfoDTO {

        @SerializedName("productName")
        @Expose
        private var productName: String? = null

        fun getProductName(): String? {
            return productName
        }

        fun setProductName(productName: String?) {
            this.productName = productName
        }
    }
}

