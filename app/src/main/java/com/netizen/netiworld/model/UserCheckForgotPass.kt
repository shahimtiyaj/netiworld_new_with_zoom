package com.netizen.netiworld.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class UserCheckForgotPass {
    @SerializedName("basicMobile")
    @Expose
    var basicMobile: String? = null
    @SerializedName("basicEmail")
    @Expose
    var basicEmail: String? = null
    @SerializedName("fullName")
    @Expose
    var fullName: String? = null
    @SerializedName("customNetiID")
    @Expose
    var customNetiID: Int? = null
    @SerializedName("dateOfBirth")
    @Expose
    var dateOfBirth: String? = null
    @SerializedName("userName")
    @Expose
    var userName: String? = null
    @SerializedName("netiID")
    @Expose
    var netiID: Int? = null
    constructor(

    ) {
    }

    constructor(
        basicMobile: String?,
        basicEmail: String?,
        fullName: String?,
        customNetiID: Int?,
        dateOfBirth: String?,
        userName: String?,
        netiID: Int?
    ) {
        this.basicMobile = basicMobile
        this.basicEmail = basicEmail
        this.fullName = fullName
        this.customNetiID = customNetiID
        this.dateOfBirth = dateOfBirth
        this.userName = userName
        this.netiID = netiID
    }

}