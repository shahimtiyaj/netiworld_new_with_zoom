package com.netizen.netiworld.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class WithDrawInfo {

    @SerializedName("taggingID")
    @Expose
    private var taggingID: Any? = null
    @SerializedName("taggingDate")
    @Expose
    private var taggingDate: Any? = null
    @SerializedName("taggingTypeCoreCategoryInfoDTO")
    @Expose
    private var taggingTypeCoreCategoryInfoDTO: Any? = null
    @SerializedName("userBankAccountInfoDTO")
    @Expose
    private var userBankAccountInfoDTO: UserBankAccountInfoDTO? = null

    fun getTaggingID(): Any? {
        return taggingID
    }

    fun setTaggingID(taggingID: Any?) {
        this.taggingID = taggingID
    }

    fun getTaggingDate(): Any? {
        return taggingDate
    }

    fun setTaggingDate(taggingDate: Any?) {
        this.taggingDate = taggingDate
    }

    fun getTaggingTypeCoreCategoryInfoDTO(): Any? {
        return taggingTypeCoreCategoryInfoDTO
    }

    fun setTaggingTypeCoreCategoryInfoDTO(taggingTypeCoreCategoryInfoDTO: Any?) {
        this.taggingTypeCoreCategoryInfoDTO = taggingTypeCoreCategoryInfoDTO
    }

    fun getUserBankAccountInfoDTO(): UserBankAccountInfoDTO? {
        return userBankAccountInfoDTO
    }

    fun setUserBankAccountInfoDTO(userBankAccountInfoDTO: UserBankAccountInfoDTO?) {
        this.userBankAccountInfoDTO = userBankAccountInfoDTO
    }

    class UserBankAccountInfoDTO {

        @SerializedName("userBankAccId")
        @Expose
        private var userBankAccId: Int? = null
        @SerializedName("bankAccNumber")
        @Expose
        private var bankAccNumber: String? = null
        @SerializedName("bankAccHolderName")
        @Expose
        private var bankAccHolderName: String? = null
        @SerializedName("bankNote")
        @Expose
        private var bankNote: String? = null
        @SerializedName("accEnableStatus")
        @Expose
        private var accEnableStatus: Int? = null
        @SerializedName("chequeSlipName")
        @Expose
        private var chequeSlipName: String? = null
        @SerializedName("chequeSlipPath")
        @Expose
        private var chequeSlipPath: String? = null
        @SerializedName("othersAttachmentName")
        @Expose
        private var othersAttachmentName: Any? = null
        @SerializedName("othersAttachmentPath")
        @Expose
        private var othersAttachmentPath: Any? = null
        @SerializedName("chequeSlipEditable")
        @Expose
        private var chequeSlipEditable: Boolean? = null
        @SerializedName("othersAttachmentEditable")
        @Expose
        private var othersAttachmentEditable: Boolean? = null
        @SerializedName("chequeSlipContent")
        @Expose
        private var chequeSlipContent: Any? = null
        @SerializedName("othersAttachmentContent")
        @Expose
        private var othersAttachmentContent: Any? = null
        @SerializedName("coreCategoryInfoDTO")
        @Expose
        private var coreCategoryInfoDTO: Any? = null
        @SerializedName("userBasicInfoDTO")
        @Expose
        private var userBasicInfoDTO: Any? = null
        @SerializedName("coreBankBranchInfoDTO")
        @Expose
        private var coreBankBranchInfoDTO: CoreBankBranchInfoDTO? = null
        @SerializedName("lastUserExecuted")
        @Expose
        private var lastUserExecuted: String? = null
        @SerializedName("lastIpExecuted")
        @Expose
        private var lastIpExecuted: String? = null
        @SerializedName("lastDateExecuted")
        @Expose
        private var lastDateExecuted: String? = null

        fun getUserBankAccId(): Int? {
            return userBankAccId
        }

        fun setUserBankAccId(userBankAccId: Int?) {
            this.userBankAccId = userBankAccId
        }

        fun getBankAccNumber(): String? {
            return bankAccNumber
        }

        fun setBankAccNumber(bankAccNumber: String?) {
            this.bankAccNumber = bankAccNumber
        }

        fun getBankAccHolderName(): String? {
            return bankAccHolderName
        }

        fun setBankAccHolderName(bankAccHolderName: String?) {
            this.bankAccHolderName = bankAccHolderName
        }

        fun getBankNote(): String? {
            return bankNote
        }

        fun setBankNote(bankNote: String?) {
            this.bankNote = bankNote
        }

        fun getAccEnableStatus(): Int? {
            return accEnableStatus
        }

        fun setAccEnableStatus(accEnableStatus: Int?) {
            this.accEnableStatus = accEnableStatus
        }

        fun getChequeSlipName(): String? {
            return chequeSlipName
        }

        fun setChequeSlipName(chequeSlipName: String?) {
            this.chequeSlipName = chequeSlipName
        }

        fun getChequeSlipPath(): String? {
            return chequeSlipPath
        }

        fun setChequeSlipPath(chequeSlipPath: String?) {
            this.chequeSlipPath = chequeSlipPath
        }

        fun getOthersAttachmentName(): Any? {
            return othersAttachmentName
        }

        fun setOthersAttachmentName(othersAttachmentName: Any?) {
            this.othersAttachmentName = othersAttachmentName
        }

        fun getOthersAttachmentPath(): Any? {
            return othersAttachmentPath
        }

        fun setOthersAttachmentPath(othersAttachmentPath: Any?) {
            this.othersAttachmentPath = othersAttachmentPath
        }

        fun getChequeSlipEditable(): Boolean? {
            return chequeSlipEditable
        }

        fun setChequeSlipEditable(chequeSlipEditable: Boolean?) {
            this.chequeSlipEditable = chequeSlipEditable
        }

        fun getOthersAttachmentEditable(): Boolean? {
            return othersAttachmentEditable
        }

        fun setOthersAttachmentEditable(othersAttachmentEditable: Boolean?) {
            this.othersAttachmentEditable = othersAttachmentEditable
        }

        fun getChequeSlipContent(): Any? {
            return chequeSlipContent
        }

        fun setChequeSlipContent(chequeSlipContent: Any?) {
            this.chequeSlipContent = chequeSlipContent
        }

        fun getOthersAttachmentContent(): Any? {
            return othersAttachmentContent
        }

        fun setOthersAttachmentContent(othersAttachmentContent: Any?) {
            this.othersAttachmentContent = othersAttachmentContent
        }

        fun getCoreCategoryInfoDTO(): Any? {
            return coreCategoryInfoDTO
        }

        fun setCoreCategoryInfoDTO(coreCategoryInfoDTO: Any?) {
            this.coreCategoryInfoDTO = coreCategoryInfoDTO
        }

        fun getUserBasicInfoDTO(): Any? {
            return userBasicInfoDTO
        }

        fun setUserBasicInfoDTO(userBasicInfoDTO: Any?) {
            this.userBasicInfoDTO = userBasicInfoDTO
        }

        fun getCoreBankBranchInfoDTO(): CoreBankBranchInfoDTO? {
            return coreBankBranchInfoDTO
        }

        fun setCoreBankBranchInfoDTO(coreBankBranchInfoDTO: CoreBankBranchInfoDTO?) {
            this.coreBankBranchInfoDTO = coreBankBranchInfoDTO
        }

        fun getLastUserExecuted(): String? {
            return lastUserExecuted
        }

        fun setLastUserExecuted(lastUserExecuted: String?) {
            this.lastUserExecuted = lastUserExecuted
        }

        fun getLastIpExecuted(): String? {
            return lastIpExecuted
        }

        fun setLastIpExecuted(lastIpExecuted: String?) {
            this.lastIpExecuted = lastIpExecuted
        }

        fun getLastDateExecuted(): String? {
            return lastDateExecuted
        }

        fun setLastDateExecuted(lastDateExecuted: String?) {
            this.lastDateExecuted = lastDateExecuted
        }

        class CoreBankBranchInfoDTO {

            @SerializedName("branchID")
            @Expose
            private var branchID: Int? = null
            @SerializedName("branchName")
            @Expose
            private var branchName: String? = null
            @SerializedName("routingNumber")
            @Expose
            private var routingNumber: String? = null
            @SerializedName("detailsNote")
            @Expose
            private var detailsNote: String? = null
            @SerializedName("enableStatus")
            @Expose
            private var enableStatus: Int? = null
            @SerializedName("coreBankInfoDTO")
            @Expose
            private var coreBankInfoDTO: CoreBankInfoDTO? = null
            @SerializedName("coreDistrictInfoDTO")
            @Expose
            private var coreDistrictInfoDTO: Any? = null
            @SerializedName("lastUserExecuted")
            @Expose
            private var lastUserExecuted: String? = null
            @SerializedName("lastIpExecuted")
            @Expose
            private var lastIpExecuted: String? = null
            @SerializedName("lastDateExecuted")
            @Expose
            private var lastDateExecuted: String? = null

            fun getBranchID(): Int? {
                return branchID
            }

            fun setBranchID(branchID: Int?) {
                this.branchID = branchID
            }

            fun getBranchName(): String? {
                return branchName
            }

            fun setBranchName(branchName: String?) {
                this.branchName = branchName
            }

            fun getRoutingNumber(): String? {
                return routingNumber
            }

            fun setRoutingNumber(routingNumber: String?) {
                this.routingNumber = routingNumber
            }

            fun getDetailsNote(): String? {
                return detailsNote
            }

            fun setDetailsNote(detailsNote: String?) {
                this.detailsNote = detailsNote
            }

            fun getEnableStatus(): Int? {
                return enableStatus
            }

            fun setEnableStatus(enableStatus: Int?) {
                this.enableStatus = enableStatus
            }

            fun getCoreBankInfoDTO(): CoreBankInfoDTO? {
                return coreBankInfoDTO
            }

            fun setCoreBankInfoDTO(coreBankInfoDTO: CoreBankInfoDTO?) {
                this.coreBankInfoDTO = coreBankInfoDTO
            }

            fun getCoreDistrictInfoDTO(): Any? {
                return coreDistrictInfoDTO
            }

            fun setCoreDistrictInfoDTO(coreDistrictInfoDTO: Any?) {
                this.coreDistrictInfoDTO = coreDistrictInfoDTO
            }

            fun getLastUserExecuted(): String? {
                return lastUserExecuted
            }

            fun setLastUserExecuted(lastUserExecuted: String?) {
                this.lastUserExecuted = lastUserExecuted
            }

            fun getLastIpExecuted(): String? {
                return lastIpExecuted
            }

            fun setLastIpExecuted(lastIpExecuted: String?) {
                this.lastIpExecuted = lastIpExecuted
            }

            fun getLastDateExecuted(): String? {
                return lastDateExecuted
            }

            fun setLastDateExecuted(lastDateExecuted: String?) {
                this.lastDateExecuted = lastDateExecuted
            }

            class CoreBankInfoDTO  {

                @SerializedName("coreCategoryID")
                @Expose
                private var coreCategoryID: Int? = null
                @SerializedName("categoryDefaultCode")
                @Expose
                private var categoryDefaultCode: String? = null
                @SerializedName("categoryName")
                @Expose
                private var categoryName: String? = null
                @SerializedName("categoryNote")
                @Expose
                private var categoryNote: String? = null
                @SerializedName("categoryEnableStatus")
                @Expose
                private var categoryEnableStatus: Int? = null
                @SerializedName("categorySerial")
                @Expose
                private var categorySerial: Int? = null
                @SerializedName("typeStatus")
                @Expose
                private var typeStatus: Int? = null
                @SerializedName("parentStatus")
                @Expose
                private var parentStatus: Int? = null
                @SerializedName("parentTypeInfoDTO")
                @Expose
                private var parentTypeInfoDTO: Any? = null
                @SerializedName("parentCoreCategoryInfoDTO")
                @Expose
                private var parentCoreCategoryInfoDTO: Any? = null
                @SerializedName("lastUserExecuted")
                @Expose
                private var lastUserExecuted: String? = null
                @SerializedName("lastIpExecuted")
                @Expose
                private var lastIpExecuted: String? = null
                @SerializedName("lastDateExecuted")
                @Expose
                private var lastDateExecuted: String? = null

                fun getCoreCategoryID(): Int? {
                    return coreCategoryID
                }

                fun setCoreCategoryID(coreCategoryID: Int?) {
                    this.coreCategoryID = coreCategoryID
                }

                fun getCategoryDefaultCode(): String? {
                    return categoryDefaultCode
                }

                fun setCategoryDefaultCode(categoryDefaultCode: String?) {
                    this.categoryDefaultCode = categoryDefaultCode
                }

                fun getCategoryName(): String? {
                    return categoryName
                }

                fun setCategoryName(categoryName: String?) {
                    this.categoryName = categoryName
                }

                fun getCategoryNote(): String? {
                    return categoryNote
                }

                fun setCategoryNote(categoryNote: String?) {
                    this.categoryNote = categoryNote
                }

                fun getCategoryEnableStatus(): Int? {
                    return categoryEnableStatus
                }

                fun setCategoryEnableStatus(categoryEnableStatus: Int?) {
                    this.categoryEnableStatus = categoryEnableStatus
                }

                fun getCategorySerial(): Int? {
                    return categorySerial
                }

                fun setCategorySerial(categorySerial: Int?) {
                    this.categorySerial = categorySerial
                }

                fun getTypeStatus(): Int? {
                    return typeStatus
                }

                fun setTypeStatus(typeStatus: Int?) {
                    this.typeStatus = typeStatus
                }

                fun getParentStatus(): Int? {
                    return parentStatus
                }

                fun setParentStatus(parentStatus: Int?) {
                    this.parentStatus = parentStatus
                }

                fun getParentTypeInfoDTO(): Any? {
                    return parentTypeInfoDTO
                }

                fun setParentTypeInfoDTO(parentTypeInfoDTO: Any?) {
                    this.parentTypeInfoDTO = parentTypeInfoDTO
                }

                fun getParentCoreCategoryInfoDTO(): Any? {
                    return parentCoreCategoryInfoDTO
                }

                fun setParentCoreCategoryInfoDTO(parentCoreCategoryInfoDTO: Any?) {
                    this.parentCoreCategoryInfoDTO = parentCoreCategoryInfoDTO
                }

                fun getLastUserExecuted(): String? {
                    return lastUserExecuted
                }

                fun setLastUserExecuted(lastUserExecuted: String?) {
                    this.lastUserExecuted = lastUserExecuted
                }

                fun getLastIpExecuted(): String? {
                    return lastIpExecuted
                }

                fun setLastIpExecuted(lastIpExecuted: String?) {
                    this.lastIpExecuted = lastIpExecuted
                }

                fun getLastDateExecuted(): String? {
                    return lastDateExecuted
                }

                fun setLastDateExecuted(lastDateExecuted: String?) {
                    this.lastDateExecuted = lastDateExecuted
                }
            }
        }
    }
}