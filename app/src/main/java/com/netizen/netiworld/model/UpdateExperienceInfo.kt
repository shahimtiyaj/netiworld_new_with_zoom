package com.netizen.netiworld.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class UpdateExperienceInfo {

    @SerializedName("experienceId")
    @Expose
    private var experienceId: Int? = null

    @SerializedName("experienceSerial")
    @Expose
    private var experienceSerial: Any? = null

    @SerializedName("companyName")
    @Expose
    private var companyName: String? = null

    @SerializedName("companyBusiness")
    @Expose
    private var companyBusiness: String? = null

    @SerializedName("designationName")
    @Expose
    private var designationName: String? = null

    @SerializedName("workingDepartment")
    @Expose
    private var workingDepartment: String? = null

    @SerializedName("companyLocation")
    @Expose
    private var companyLocation: String? = null

    @SerializedName("employmentStart")
    @Expose
    private var employmentStart: String? = null

    @SerializedName("employmentEnd")
    @Expose
    private var employmentEnd: String? = null

    @SerializedName("responsibilityDetails")
    @Expose
    private var responsibilityDetails: String? = null

    @SerializedName("userBasicInfoDTO")
    @Expose
    private var userBasicInfoDTO: Any? = null

    @SerializedName("countryInfoDTO")
    @Expose
    private var countryInfoDTO: CountryInfoDTO? = null

    @SerializedName("employmentStartView")
    @Expose
    private var employmentStartView: String? = null

    @SerializedName("employmentStartEdit")
    @Expose
    private var employmentStartEdit: String? = null

    @SerializedName("employmentEndView")
    @Expose
    private var employmentEndView: String? = null

    @SerializedName("employmentEndEdit")
    @Expose
    private var employmentEndEdit: String? = null

    fun getExperienceId(): Int? {
        return experienceId
    }

    fun setExperienceId(experienceId: Int?) {
        this.experienceId = experienceId
    }

    fun getExperienceSerial(): Any? {
        return experienceSerial
    }

    fun setExperienceSerial(experienceSerial: Any?) {
        this.experienceSerial = experienceSerial
    }

    fun getCompanyName(): String? {
        return companyName
    }

    fun setCompanyName(companyName: String?) {
        this.companyName = companyName
    }

    fun getCompanyBusiness(): String? {
        return companyBusiness
    }

    fun setCompanyBusiness(companyBusiness: String?) {
        this.companyBusiness = companyBusiness
    }

    fun getDesignationName(): String? {
        return designationName
    }

    fun setDesignationName(designationName: String?) {
        this.designationName = designationName
    }

    fun getWorkingDepartment(): String? {
        return workingDepartment
    }

    fun setWorkingDepartment(workingDepartment: String?) {
        this.workingDepartment = workingDepartment
    }

    fun getCompanyLocation(): String? {
        return companyLocation
    }

    fun setCompanyLocation(companyLocation: String?) {
        this.companyLocation = companyLocation
    }

    fun getEmploymentStart(): String? {
        return employmentStart
    }

    fun setEmploymentStart(employmentStart: String?) {
        this.employmentStart = employmentStart
    }

    fun getEmploymentEnd(): String? {
        return employmentEnd
    }

    fun setEmploymentEnd(employmentEnd: String?) {
        this.employmentEnd = employmentEnd
    }

    fun getResponsibilityDetails(): String? {
        return responsibilityDetails
    }

    fun setResponsibilityDetails(responsibilityDetails: String?) {
        this.responsibilityDetails = responsibilityDetails
    }

    fun getUserBasicInfoDTO(): Any? {
        return userBasicInfoDTO
    }

    fun setUserBasicInfoDTO(userBasicInfoDTO: Any?) {
        this.userBasicInfoDTO = userBasicInfoDTO
    }

    fun getCountryInfoDTO(): CountryInfoDTO? {
        return countryInfoDTO
    }

    fun setCountryInfoDTO(countryInfoDTO: CountryInfoDTO?) {
        this.countryInfoDTO = countryInfoDTO
    }

    fun getEmploymentStartView(): String? {
        return employmentStartView
    }

    fun setEmploymentStartView(employmentStartView: String?) {
        this.employmentStartView = employmentStartView
    }

    fun getEmploymentStartEdit(): String? {
        return employmentStartEdit
    }

    fun setEmploymentStartEdit(employmentStartEdit: String?) {
        this.employmentStartEdit = employmentStartEdit
    }

    fun getEmploymentEndView(): String? {
        return employmentEndView
    }

    fun setEmploymentEndView(employmentEndView: String?) {
        this.employmentEndView = employmentEndView
    }

    fun getEmploymentEndEdit(): String? {
        return employmentEndEdit
    }

    fun setEmploymentEndEdit(employmentEndEdit: String?) {
        this.employmentEndEdit = employmentEndEdit
    }

    class CountryInfoDTO {
        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        @SerializedName("categoryNote")
        @Expose
        private var categoryNote: String? = null

        @SerializedName("categoryEnableStatus")
        @Expose
        private var categoryEnableStatus: Int? = null

        @SerializedName("categorySerial")
        @Expose
        private var categorySerial: Int? = null

        @SerializedName("typeStatus")
        @Expose
        private var typeStatus: Int? = null

        @SerializedName("parentStatus")
        @Expose
        private var parentStatus: Int? = null

        @SerializedName("parentTypeInfoDTO")
        @Expose
        private var parentTypeInfoDTO: Any? = null

        @SerializedName("parentCoreCategoryInfoDTO")
        @Expose
        private var parentCoreCategoryInfoDTO: Any? = null

        @SerializedName("lastUserExecuted")
        @Expose
        private var lastUserExecuted: String? = null

        @SerializedName("lastIpExecuted")
        @Expose
        private var lastIpExecuted: String? = null

        @SerializedName("lastDateExecuted")
        @Expose
        private var lastDateExecuted: Int? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }

        fun getCategoryNote(): String? {
            return categoryNote
        }

        fun setCategoryNote(categoryNote: String?) {
            this.categoryNote = categoryNote
        }

        fun getCategoryEnableStatus(): Int? {
            return categoryEnableStatus
        }

        fun setCategoryEnableStatus(categoryEnableStatus: Int?) {
            this.categoryEnableStatus = categoryEnableStatus
        }

        fun getCategorySerial(): Int? {
            return categorySerial
        }

        fun setCategorySerial(categorySerial: Int?) {
            this.categorySerial = categorySerial
        }

        fun getTypeStatus(): Int? {
            return typeStatus
        }

        fun setTypeStatus(typeStatus: Int?) {
            this.typeStatus = typeStatus
        }

        fun getParentStatus(): Int? {
            return parentStatus
        }

        fun setParentStatus(parentStatus: Int?) {
            this.parentStatus = parentStatus
        }

        fun getParentTypeInfoDTO(): Any? {
            return parentTypeInfoDTO
        }

        fun setParentTypeInfoDTO(parentTypeInfoDTO: Any?) {
            this.parentTypeInfoDTO = parentTypeInfoDTO
        }

        fun getParentCoreCategoryInfoDTO(): Any? {
            return parentCoreCategoryInfoDTO
        }

        fun setParentCoreCategoryInfoDTO(parentCoreCategoryInfoDTO: Any?) {
            this.parentCoreCategoryInfoDTO = parentCoreCategoryInfoDTO
        }

        fun getLastUserExecuted(): String? {
            return lastUserExecuted
        }

        fun setLastUserExecuted(lastUserExecuted: String?) {
            this.lastUserExecuted = lastUserExecuted
        }

        fun getLastIpExecuted(): String? {
            return lastIpExecuted
        }

        fun setLastIpExecuted(lastIpExecuted: String?) {
            this.lastIpExecuted = lastIpExecuted
        }

        fun getLastDateExecuted(): Int? {
            return lastDateExecuted
        }

        fun setLastDateExecuted(lastDateExecuted: Int?) {
            this.lastDateExecuted = lastDateExecuted
        }

    }

}