package com.netizen.netiworld.model.UserPoint

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class StudentPortalPeriod : Serializable {

    @SerializedName("message")
    @Expose
    private var message: Any? = null

    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null

    /*@SerializedName("item")
    @Expose
    private var item: Item? = null*/

    @SerializedName("item")
    @Expose
    private var item: List<Item?>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    /* fun getItem(): Item? {
         return item
     }

     fun setItem(item: Item?) {
         this.item = item
     }*/

    fun getItem(): List<Item?>? {
        return item
    }

    fun setItem(item: List<Item?>?) {
        this.item = item
    }

    class Item {
        @SerializedName("id")
        @Expose
        private var id: Int? = null

        @SerializedName("name")
        @Expose
        private var name: String? = null

        @SerializedName("typeId")
        @Expose
        private var typeId: Int? = null

        @SerializedName("typeName")
        @Expose
        private var typeName: String? = null

        @SerializedName("defaultId")
        @Expose
        private var defaultId: String? = null

        @SerializedName("viewStatus")
        @Expose
        private var viewStatus: Int? = null

        @SerializedName("viewSerial")
        @Expose
        private var viewSerial: Int? = null

        fun getId(): Int? {
            return id
        }

        fun setId(id: Int?) {
            this.id = id
        }

        fun getName(): String? {
            return name
        }

        fun setName(name: String?) {
            this.name = name
        }

        fun getTypeId(): Int? {
            return typeId
        }

        fun setTypeId(typeId: Int?) {
            this.typeId = typeId
        }

        fun getTypeName(): String? {
            return typeName
        }

        fun setTypeName(typeName: String?) {
            this.typeName = typeName
        }

        fun getDefaultId(): String? {
            return defaultId
        }

        fun setDefaultId(defaultId: String?) {
            this.defaultId = defaultId
        }

        fun getViewStatus(): Int? {
            return viewStatus
        }

        fun setViewStatus(viewStatus: Int?) {
            this.viewStatus = viewStatus
        }

        fun getViewSerial(): Int? {
            return viewSerial
        }

        fun setViewSerial(viewSerial: Int?) {
            this.viewSerial = viewSerial
        }

    }
}