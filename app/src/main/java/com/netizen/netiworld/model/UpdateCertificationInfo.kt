package com.netizen.netiworld.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class UpdateCertificationInfo {

    @SerializedName("certificateInfoId")
    @Expose
    private var certificateInfoId: Int? = null

    @SerializedName("certificateSerial")
    @Expose
    private var certificateSerial: Any? = null

    @SerializedName("certificationName")
    @Expose
    private var certificationName: String? = null

    @SerializedName("instituteName")
    @Expose
    private var instituteName: String? = null

    @SerializedName("instituteLocation")
    @Expose
    private var instituteLocation: String? = null

    @SerializedName("achieveDate")
    @Expose
    private var achieveDate: String? = null

    @SerializedName("courseDuration")
    @Expose
    private var courseDuration: String? = null

    @SerializedName("userBasicInfoDTO")
    @Expose
    private var userBasicInfoDTO: Any? = null

    @SerializedName("countryInfoDTO")
    @Expose
    private var countryInfoDTO: CountryInfoDTO? = null

    @SerializedName("achieveDateView")
    @Expose
    private var achieveDateView: String? = null

    @SerializedName("achieveDateEdit")
    @Expose
    private var achieveDateEdit: String? = null

    fun getCertificateInfoId(): Int? {
        return certificateInfoId
    }

    fun setCertificateInfoId(certificateInfoId: Int?) {
        this.certificateInfoId = certificateInfoId
    }

    fun getCertificateSerial(): Any? {
        return certificateSerial
    }

    fun setCertificateSerial(certificateSerial: Any?) {
        this.certificateSerial = certificateSerial
    }

    fun getCertificationName(): String? {
        return certificationName
    }

    fun setCertificationName(certificationName: String?) {
        this.certificationName = certificationName
    }

    fun getInstituteName(): String? {
        return instituteName
    }

    fun setInstituteName(instituteName: String?) {
        this.instituteName = instituteName
    }

    fun getInstituteLocation(): String? {
        return instituteLocation
    }

    fun setInstituteLocation(instituteLocation: String?) {
        this.instituteLocation = instituteLocation
    }

    fun getAchieveDate(): String? {
        return achieveDate
    }

    fun setAchieveDate(achieveDate: String?) {
        this.achieveDate = achieveDate
    }

    fun getCourseDuration(): String? {
        return courseDuration
    }

    fun setCourseDuration(courseDuration: String?) {
        this.courseDuration = courseDuration
    }

    fun getUserBasicInfoDTO(): Any? {
        return userBasicInfoDTO
    }

    fun setUserBasicInfoDTO(userBasicInfoDTO: Any?) {
        this.userBasicInfoDTO = userBasicInfoDTO
    }

    fun getCountryInfoDTO(): CountryInfoDTO? {
        return countryInfoDTO
    }

    fun setCountryInfoDTO(countryInfoDTO: CountryInfoDTO?) {
        this.countryInfoDTO = countryInfoDTO
    }

    fun getAchieveDateView(): String? {
        return achieveDateView
    }

    fun setAchieveDateView(achieveDateView: String?) {
        this.achieveDateView = achieveDateView
    }

    fun getAchieveDateEdit(): String? {
        return achieveDateEdit
    }

    fun setAchieveDateEdit(achieveDateEdit: String?) {
        this.achieveDateEdit = achieveDateEdit
    }


    class CountryInfoDTO {
        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        @SerializedName("categoryNote")
        @Expose
        private var categoryNote: String? = null

        @SerializedName("categoryEnableStatus")
        @Expose
        private var categoryEnableStatus: Int? = null

        @SerializedName("categorySerial")
        @Expose
        private var categorySerial: Int? = null

        @SerializedName("typeStatus")
        @Expose
        private var typeStatus: Int? = null

        @SerializedName("parentStatus")
        @Expose
        private var parentStatus: Int? = null

        @SerializedName("parentTypeInfoDTO")
        @Expose
        private var parentTypeInfoDTO: Any? = null

        @SerializedName("parentCoreCategoryInfoDTO")
        @Expose
        private var parentCoreCategoryInfoDTO: Any? = null

        @SerializedName("lastUserExecuted")
        @Expose
        private var lastUserExecuted: String? = null

        @SerializedName("lastIpExecuted")
        @Expose
        private var lastIpExecuted: String? = null

        @SerializedName("lastDateExecuted")
        @Expose
        private var lastDateExecuted: Int? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }

        fun getCategoryNote(): String? {
            return categoryNote
        }

        fun setCategoryNote(categoryNote: String?) {
            this.categoryNote = categoryNote
        }

        fun getCategoryEnableStatus(): Int? {
            return categoryEnableStatus
        }

        fun setCategoryEnableStatus(categoryEnableStatus: Int?) {
            this.categoryEnableStatus = categoryEnableStatus
        }

        fun getCategorySerial(): Int? {
            return categorySerial
        }

        fun setCategorySerial(categorySerial: Int?) {
            this.categorySerial = categorySerial
        }

        fun getTypeStatus(): Int? {
            return typeStatus
        }

        fun setTypeStatus(typeStatus: Int?) {
            this.typeStatus = typeStatus
        }

        fun getParentStatus(): Int? {
            return parentStatus
        }

        fun setParentStatus(parentStatus: Int?) {
            this.parentStatus = parentStatus
        }

        fun getParentTypeInfoDTO(): Any? {
            return parentTypeInfoDTO
        }

        fun setParentTypeInfoDTO(parentTypeInfoDTO: Any?) {
            this.parentTypeInfoDTO = parentTypeInfoDTO
        }

        fun getParentCoreCategoryInfoDTO(): Any? {
            return parentCoreCategoryInfoDTO
        }

        fun setParentCoreCategoryInfoDTO(parentCoreCategoryInfoDTO: Any?) {
            this.parentCoreCategoryInfoDTO = parentCoreCategoryInfoDTO
        }

        fun getLastUserExecuted(): String? {
            return lastUserExecuted
        }

        fun setLastUserExecuted(lastUserExecuted: String?) {
            this.lastUserExecuted = lastUserExecuted
        }

        fun getLastIpExecuted(): String? {
            return lastIpExecuted
        }

        fun setLastIpExecuted(lastIpExecuted: String?) {
            this.lastIpExecuted = lastIpExecuted
        }

        fun getLastDateExecuted(): Int? {
            return lastDateExecuted
        }

        fun setLastDateExecuted(lastDateExecuted: Int?) {
            this.lastDateExecuted = lastDateExecuted
        }
    }
}