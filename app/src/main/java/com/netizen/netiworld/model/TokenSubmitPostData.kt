package com.netizen.netiworld.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class TokenSubmitPostData {

    @SerializedName("attachSaveOrEditable")
    @Expose
    private var attachSaveOrEditable: Boolean? = null

    @SerializedName("tokenSource")
    @Expose
    private var tokenSource: String? = null

    @SerializedName("tokenDetails")
    @Expose
    private var tokenDetails: String? = null

    @SerializedName("tokenContact")
    @Expose
    private var tokenContact: String? = null

    @SerializedName("tokenTypeInfoDTO")
    @Expose
    private var tokenTypeInfoDTO: TokenTypeInfoDTO? = null

    @SerializedName("attachContent")
    @Expose
    private var attachContent: String? = null

    @SerializedName("attachmentName")
    @Expose
    private var attachmentName: Any? = null

    fun getAttachSaveOrEditable(): Boolean? {
        return attachSaveOrEditable
    }

    fun setAttachSaveOrEditable(attachSaveOrEditable: Boolean?) {
        this.attachSaveOrEditable = attachSaveOrEditable
    }

    fun getTokenSource(): String? {
        return tokenSource
    }

    fun setTokenSource(tokenSource: String?) {
        this.tokenSource = tokenSource
    }

    fun getTokenDetails(): String? {
        return tokenDetails
    }

    fun setTokenDetails(tokenDetails: String?) {
        this.tokenDetails = tokenDetails
    }

    fun getTokenContact(): String? {
        return tokenContact
    }

    fun setTokenContact(tokenContact: String?) {
        this.tokenContact = tokenContact
    }

    fun getTokenTypeInfoDTO(): TokenTypeInfoDTO? {
        return tokenTypeInfoDTO
    }

    fun setTokenTypeInfoDTO(tokenTypeInfoDTO: TokenTypeInfoDTO?) {
        this.tokenTypeInfoDTO = tokenTypeInfoDTO
    }

    fun getAttachContent(): String? {
        return attachContent
    }

    fun setAttachContent(attachContent: String?) {
        this.attachContent = attachContent
    }

    fun getAttachmentName(): Any? {
        return attachmentName
    }

    fun setAttachmentName(attachmentName: Any?) {
        this.attachmentName = attachmentName
    }

    class TokenTypeInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }
    }
}