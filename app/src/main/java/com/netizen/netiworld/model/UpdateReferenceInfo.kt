package com.netizen.netiworld.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class UpdateReferenceInfo {
    @SerializedName("referenceId")
    @Expose
    private var referenceId: Int? = null

    @SerializedName("referenceSerial")
    @Expose
    private var referenceSerial: Any? = null

    @SerializedName("referenceName")
    @Expose
    private var referenceName: String? = null

    @SerializedName("referenceDesignation")
    @Expose
    private var referenceDesignation: String? = null

    @SerializedName("orgainizationName")
    @Expose
    private var orgainizationName: String? = null

    @SerializedName("referenceMobile")
    @Expose
    private var referenceMobile: String? = null

    @SerializedName("referenceEmail")
    @Expose
    private var referenceEmail: String? = null

    @SerializedName("referenceAddress")
    @Expose
    private var referenceAddress: String? = null

    @SerializedName("relationInfoDTO")
    @Expose
    private var relationInfoDTO: RelationInfoDTO? = null

    @SerializedName("userBasicInfoDTO")
    @Expose
    private var userBasicInfoDTO: Any? = null

    fun getReferenceId(): Int? {
        return referenceId
    }

    fun setReferenceId(referenceId: Int?) {
        this.referenceId = referenceId
    }

    fun getReferenceSerial(): Any? {
        return referenceSerial
    }

    fun setReferenceSerial(referenceSerial: Any?) {
        this.referenceSerial = referenceSerial
    }

    fun getReferenceName(): String? {
        return referenceName
    }

    fun setReferenceName(referenceName: String?) {
        this.referenceName = referenceName
    }

    fun getReferenceDesignation(): String? {
        return referenceDesignation
    }

    fun setReferenceDesignation(referenceDesignation: String?) {
        this.referenceDesignation = referenceDesignation
    }

    fun getOrgainizationName(): String? {
        return orgainizationName
    }

    fun setOrgainizationName(orgainizationName: String?) {
        this.orgainizationName = orgainizationName
    }

    fun getReferenceMobile(): String? {
        return referenceMobile
    }

    fun setReferenceMobile(referenceMobile: String?) {
        this.referenceMobile = referenceMobile
    }

    fun getReferenceEmail(): String? {
        return referenceEmail
    }

    fun setReferenceEmail(referenceEmail: String?) {
        this.referenceEmail = referenceEmail
    }

    fun getReferenceAddress(): String? {
        return referenceAddress
    }

    fun setReferenceAddress(referenceAddress: String?) {
        this.referenceAddress = referenceAddress
    }

    fun getRelationInfoDTO(): RelationInfoDTO? {
        return relationInfoDTO
    }

    fun setRelationInfoDTO(relationInfoDTO: RelationInfoDTO?) {
        this.relationInfoDTO = relationInfoDTO
    }

    fun getUserBasicInfoDTO(): Any? {
        return userBasicInfoDTO
    }

    fun setUserBasicInfoDTO(userBasicInfoDTO: Any?) {
        this.userBasicInfoDTO = userBasicInfoDTO
    }

    class RelationInfoDTO {
        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        @SerializedName("categoryNote")
        @Expose
        private var categoryNote: String? = null

        @SerializedName("categoryEnableStatus")
        @Expose
        private var categoryEnableStatus: Int? = null

        @SerializedName("categorySerial")
        @Expose
        private var categorySerial: Int? = null

        @SerializedName("typeStatus")
        @Expose
        private var typeStatus: Int? = null

        @SerializedName("parentStatus")
        @Expose
        private var parentStatus: Int? = null

        @SerializedName("parentTypeInfoDTO")
        @Expose
        private var parentTypeInfoDTO: Any? = null

        @SerializedName("parentCoreCategoryInfoDTO")
        @Expose
        private var parentCoreCategoryInfoDTO: Any? = null

        @SerializedName("lastUserExecuted")
        @Expose
        private var lastUserExecuted: String? = null

        @SerializedName("lastIpExecuted")
        @Expose
        private var lastIpExecuted: String? = null

        @SerializedName("lastDateExecuted")
        @Expose
        private var lastDateExecuted: Int? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }

        fun getCategoryNote(): String? {
            return categoryNote
        }

        fun setCategoryNote(categoryNote: String?) {
            this.categoryNote = categoryNote
        }

        fun getCategoryEnableStatus(): Int? {
            return categoryEnableStatus
        }

        fun setCategoryEnableStatus(categoryEnableStatus: Int?) {
            this.categoryEnableStatus = categoryEnableStatus
        }

        fun getCategorySerial(): Int? {
            return categorySerial
        }

        fun setCategorySerial(categorySerial: Int?) {
            this.categorySerial = categorySerial
        }

        fun getTypeStatus(): Int? {
            return typeStatus
        }

        fun setTypeStatus(typeStatus: Int?) {
            this.typeStatus = typeStatus
        }

        fun getParentStatus(): Int? {
            return parentStatus
        }

        fun setParentStatus(parentStatus: Int?) {
            this.parentStatus = parentStatus
        }

        fun getParentTypeInfoDTO(): Any? {
            return parentTypeInfoDTO
        }

        fun setParentTypeInfoDTO(parentTypeInfoDTO: Any?) {
            this.parentTypeInfoDTO = parentTypeInfoDTO
        }

        fun getParentCoreCategoryInfoDTO(): Any? {
            return parentCoreCategoryInfoDTO
        }

        fun setParentCoreCategoryInfoDTO(parentCoreCategoryInfoDTO: Any?) {
            this.parentCoreCategoryInfoDTO = parentCoreCategoryInfoDTO
        }

        fun getLastUserExecuted(): String? {
            return lastUserExecuted
        }

        fun setLastUserExecuted(lastUserExecuted: String?) {
            this.lastUserExecuted = lastUserExecuted
        }

        fun getLastIpExecuted(): String? {
            return lastIpExecuted
        }

        fun setLastIpExecuted(lastIpExecuted: String?) {
            this.lastIpExecuted = lastIpExecuted
        }

        fun getLastDateExecuted(): Int? {
            return lastDateExecuted
        }

        fun setLastDateExecuted(lastDateExecuted: Int?) {
            this.lastDateExecuted = lastDateExecuted
        }

    }
}