package com.netizen.netiworld.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class CertificationInfo : Serializable {

    @SerializedName("certificateInfoId")
    @Expose
    private var certificateInfoId: Int? = null

    @SerializedName("certificateSerial")
    @Expose
    private var certificateSerial: Any? = null

    @SerializedName("certificationName")
    @Expose
    private var certificationName: String? = null

    @SerializedName("instituteName")
    @Expose
    private var instituteName: String? = null

    @SerializedName("instituteLocation")
    @Expose
    private var instituteLocation: String? = null

    @SerializedName("achieveDate")
    @Expose
    private var achieveDate: Long? = null

    @SerializedName("courseDuration")
    @Expose
    private var courseDuration: String? = null

    @SerializedName("countryInfoDTO")
    @Expose
    private var countryInfoDTO: CountryInfoDTO? = null

    fun getCertificateInfoId(): Int? {
        return certificateInfoId
    }

    fun setCertificateInfoId(certificateInfoId: Int?) {
        this.certificateInfoId = certificateInfoId
    }

    fun getCertificateSerial(): Any? {
        return certificateSerial
    }

    fun setCertificateSerial(certificateSerial: Any?) {
        this.certificateSerial = certificateSerial
    }

    fun getCertificationName(): String? {
        return certificationName
    }

    fun setCertificationName(certificationName: String?) {
        this.certificationName = certificationName
    }

    fun getInstituteName(): String? {
        return instituteName
    }

    fun setInstituteName(instituteName: String?) {
        this.instituteName = instituteName
    }

    fun getInstituteLocation(): String? {
        return instituteLocation
    }

    fun setInstituteLocation(instituteLocation: String?) {
        this.instituteLocation = instituteLocation
    }

    fun getAchieveDate(): Long? {
        return achieveDate
    }

    fun setAchieveDate(achieveDate: Long?) {
        this.achieveDate = achieveDate
    }

    fun getCourseDuration(): String? {
        return courseDuration
    }

    fun setCourseDuration(courseDuration: String?) {
        this.courseDuration = courseDuration
    }

    fun getCountryInfoDTO(): CountryInfoDTO? {
        return countryInfoDTO
    }

    fun setCountryInfoDTO(countryInfoDTO: CountryInfoDTO?) {
        this.countryInfoDTO = countryInfoDTO
    }

    class CountryInfoDTO {

        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: Int? = null

        @SerializedName("categoryDefaultCode")
        @Expose
        private var categoryDefaultCode: String? = null

        @SerializedName("categoryName")
        @Expose
        private var categoryName: String? = null

        fun getCoreCategoryID(): Int? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: Int?) {
            this.coreCategoryID = coreCategoryID
        }

        fun getCategoryDefaultCode(): String? {
            return categoryDefaultCode
        }

        fun setCategoryDefaultCode(categoryDefaultCode: String?) {
            this.categoryDefaultCode = categoryDefaultCode
        }

        fun getCategoryName(): String? {
            return categoryName
        }

        fun setCategoryName(categoryName: String?) {
            this.categoryName = categoryName
        }
    }
}