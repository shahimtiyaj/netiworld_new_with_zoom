package com.netizen.netiworld.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class PostExperienceInfo {

    @SerializedName("countryInfoDTO")
    @Expose
    private var countryInfoDTO: CountryInfoDTO? = null

    @SerializedName("companyName")
    @Expose
    private var companyName: String? = null

    @SerializedName("companyBusiness")
    @Expose
    private var companyBusiness: String? = null

    @SerializedName("designationName")
    @Expose
    private var designationName: String? = null

    @SerializedName("workingDepartment")
    @Expose
    private var workingDepartment: String? = null

    @SerializedName("companyLocation")
    @Expose
    private var companyLocation: String? = null

    @SerializedName("employmentStart")
    @Expose
    private var employmentStart: String? = null

    @SerializedName("employmentEnd")
    @Expose
    private var employmentEnd: String? = null

    @SerializedName("responsibilityDetails")
    @Expose
    private var responsibilityDetails: String? = null

    fun getCountryInfoDTO(): CountryInfoDTO? {
        return countryInfoDTO
    }

    fun setCountryInfoDTO(countryInfoDTO: CountryInfoDTO?) {
        this.countryInfoDTO = countryInfoDTO
    }

    fun getCompanyName(): String? {
        return companyName
    }

    fun setCompanyName(companyName: String?) {
        this.companyName = companyName
    }

    fun getCompanyBusiness(): String? {
        return companyBusiness
    }

    fun setCompanyBusiness(companyBusiness: String?) {
        this.companyBusiness = companyBusiness
    }

    fun getDesignationName(): String? {
        return designationName
    }

    fun setDesignationName(designationName: String?) {
        this.designationName = designationName
    }

    fun getWorkingDepartment(): String? {
        return workingDepartment
    }

    fun setWorkingDepartment(workingDepartment: String?) {
        this.workingDepartment = workingDepartment
    }

    fun getCompanyLocation(): String? {
        return companyLocation
    }

    fun setCompanyLocation(companyLocation: String?) {
        this.companyLocation = companyLocation
    }

    fun getEmploymentStart(): String? {
        return employmentStart
    }

    fun setEmploymentStart(employmentStart: String?) {
        this.employmentStart = employmentStart
    }

    fun getEmploymentEnd(): String? {
        return employmentEnd
    }

    fun setEmploymentEnd(employmentEnd: String?) {
        this.employmentEnd = employmentEnd
    }

    fun getResponsibilityDetails(): String? {
        return responsibilityDetails
    }

    fun setResponsibilityDetails(responsibilityDetails: String?) {
        this.responsibilityDetails = responsibilityDetails
    }

    class CountryInfoDTO {
        @SerializedName("coreCategoryID")
        @Expose
        private var coreCategoryID: String? = null

        fun getCoreCategoryID(): String? {
            return coreCategoryID
        }

        fun setCoreCategoryID(coreCategoryID: String?) {
            this.coreCategoryID = coreCategoryID
        }
    }
}