package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


@JsonIgnoreProperties(ignoreUnknown = true)
class ProfileInformation : Serializable {

    @SerializedName("netiID")
    @Expose
    private var netiID: Int? = null

    @SerializedName("customNetiID")
    @Expose
    private var customNetiID: String? = null

    @SerializedName("fullName")
    @Expose
    private var fullName: String? = null

    @SerializedName("hobby")
    @Expose
    private var hobby: Any? = null

    @SerializedName("gender")
    @Expose
    private var gender: String? = null

    @SerializedName("religion")
    @Expose
    private var religion: String? = null

    @SerializedName("dateOfBirth")
    @Expose
    private var dateOfBirth: String? = null

    @SerializedName("bloodGroup")
    @Expose
    private var bloodGroup: String? = null

    @SerializedName("age")
    @Expose
    private var age: String? = null

    @SerializedName("basicMobile")
    @Expose
    private var basicMobile: String? = null

    @SerializedName("basicEmail")
    @Expose
    private var basicEmail: String? = null

    @SerializedName("validationStatus")
    @Expose
    private var validationStatus: Any? = null

    @SerializedName("registrationDate")
    @Expose
    private var registrationDate: String? = null

    @SerializedName("userWalletBalance")
    @Expose
    private var userWalletBalance: Double? = null

    @SerializedName("smsBalance")
    @Expose
    private var smsBalance: Double? = null

    @SerializedName("imageName")
    @Expose
    private var imageName: String? = null

    @SerializedName("imagePath")
    @Expose
    private var imagePath: String? = null

    @SerializedName("leadName")
    @Expose
    private var leadName: Any? = null

    @SerializedName("leadCustomNetiID")
    @Expose
    private var leadCustomNetiID: Any? = null

    @SerializedName("area")
    @Expose
    private var area: String? = null

    @SerializedName("upazilla")
    @Expose
    private var upazilla: String? = null

    @SerializedName("district")
    @Expose
    private var district: String? = null

    @SerializedName("division")
    @Expose
    private var division: String? = null

    @SerializedName("userDetailsInfoResponseDTO")
    @Expose
    private var userDetailsInfoResponseDTO: UserDetailsInfoResponseDTO? = null

    @SerializedName("username")
    @Expose
    private var username: String? = null

    @SerializedName("userStatus")
    @Expose
    private var userStatus: String? = null

    @SerializedName("password")
    @Expose
    private var password: Any? = null

    @SerializedName("numOfPoint")
    @Expose
    private var numOfPoint: Int? = null

    @SerializedName("registerFrom")
    @Expose
    private var registerFrom: String? = null

    fun getNetiID(): Int? {
        return netiID
    }

    fun setNetiID(netiID: Int?) {
        this.netiID = netiID
    }

    fun getCustomNetiID(): String? {
        return customNetiID
    }

    fun setCustomNetiID(customNetiID: String?) {
        this.customNetiID = customNetiID
    }

    fun getFullName(): String? {
        return fullName
    }

    fun setFullName(fullName: String?) {
        this.fullName = fullName
    }

    fun getHobby(): Any? {
        return hobby
    }

    fun setHobby(hobby: Any?) {
        this.hobby = hobby
    }

    fun getGender(): String? {
        return gender
    }

    fun setGender(gender: String?) {
        this.gender = gender
    }

    fun getReligion(): String? {
        return religion
    }

    fun setReligion(religion: String?) {
        this.religion = religion
    }

    fun getDateOfBirth(): String? {
        return dateOfBirth
    }

    fun setDateOfBirth(dateOfBirth: String?) {
        this.dateOfBirth = dateOfBirth
    }

    fun getBloodGroup(): String? {
        return bloodGroup
    }

    fun setBloodGroup(bloodGroup: String?) {
        this.bloodGroup = bloodGroup
    }

    fun getAge(): String? {
        return age
    }

    fun setAge(age: String?) {
        this.age = age
    }

    fun getBasicMobile(): String? {
        return basicMobile
    }

    fun setBasicMobile(basicMobile: String?) {
        this.basicMobile = basicMobile
    }

    fun getBasicEmail(): String? {
        return basicEmail
    }

    fun setBasicEmail(basicEmail: String?) {
        this.basicEmail = basicEmail
    }

    fun getValidationStatus(): Any? {
        return validationStatus
    }

    fun setValidationStatus(validationStatus: Any?) {
        this.validationStatus = validationStatus
    }

    fun getRegistrationDate(): String? {
        return registrationDate
    }

    fun setRegistrationDate(registrationDate: String?) {
        this.registrationDate = registrationDate
    }

    fun getUserWalletBalance(): Double? {
        return userWalletBalance
    }

    fun setUserWalletBalance(userWalletBalance: Double?) {
        this.userWalletBalance = userWalletBalance
    }

    fun getSmsBalance(): Double? {
        return smsBalance
    }

    fun setSmsBalance(smsBalance: Double?) {
        this.smsBalance = smsBalance
    }

    fun getImageName(): String? {
        return imageName
    }

    fun setImageName(imageName: String?) {
        this.imageName = imageName
    }

    fun getImagePath(): String? {
        return imagePath
    }

    fun setImagePath(imagePath: String?) {
        this.imagePath = imagePath
    }

    fun getLeadName(): Any? {
        return leadName
    }

    fun setLeadName(leadName: Any?) {
        this.leadName = leadName
    }

    fun getLeadCustomNetiID(): Any? {
        return leadCustomNetiID
    }

    fun setLeadCustomNetiID(leadCustomNetiID: Any?) {
        this.leadCustomNetiID = leadCustomNetiID
    }

    fun getArea(): String? {
        return area
    }

    fun setArea(area: String?) {
        this.area = area
    }

    fun getUpazilla(): String? {
        return upazilla
    }

    fun setUpazilla(upazilla: String?) {
        this.upazilla = upazilla
    }

    fun getDistrict(): String? {
        return district
    }

    fun setDistrict(district: String?) {
        this.district = district
    }

    fun getDivision(): String? {
        return division
    }

    fun setDivision(division: String?) {
        this.division = division
    }

    fun getUserDetailsInfoResponseDTO(): UserDetailsInfoResponseDTO? {
        return userDetailsInfoResponseDTO
    }

    fun setUserDetailsInfoResponseDTO(userDetailsInfoResponseDTO: UserDetailsInfoResponseDTO?) {
        this.userDetailsInfoResponseDTO = userDetailsInfoResponseDTO
    }

    fun getUsername(): String? {
        return username
    }

    fun setUsername(username: String?) {
        this.username = username
    }

    fun getUserStatus(): String? {
        return userStatus
    }

    fun setUserStatus(userStatus: String?) {
        this.userStatus = userStatus
    }

    fun getPassword(): Any? {
        return password
    }

    fun setPassword(password: Any?) {
        this.password = password
    }

    fun getNumOfPoint(): Int? {
        return numOfPoint
    }

    fun setNumOfPoint(numOfPoint: Int?) {
        this.numOfPoint = numOfPoint
    }

    fun getRegisterFrom(): String? {
        return registerFrom
    }

    fun setRegisterFrom(registerFrom: String?) {
        this.registerFrom = registerFrom
    }

    class UserDetailsInfoResponseDTO {

        @SerializedName("userDetailsID")
        @Expose
        private var userDetailsID: Int? = null

        @SerializedName("nationalID")
        @Expose
        private var nationalID: String? = null

        @SerializedName("fatherName")
        @Expose
        private var fatherName: String? = null

        @SerializedName("motherName")
        @Expose
        private var motherName: String? = null

        @SerializedName("maritalStatus")
        @Expose
        private var maritalStatus: String? = null

        @SerializedName("numberOfChild")
        @Expose
        private var numberOfChild: String? = null

        @SerializedName("nationality")
        @Expose
        private var nationality: String? = null

        @SerializedName("address")
        @Expose
        private var address: Any? = null

        @SerializedName("latitude")
        @Expose
        private var latitude: String? = null

        @SerializedName("longitude")
        @Expose
        private var longitude: String? = null

        @SerializedName("occupation")
        @Expose
        private var occupation: Any? = null

        @SerializedName("passportNo")
        @Expose
        private var passportNo: String? = null

        @SerializedName("birthCertificateNo")
        @Expose
        private var birthCertificateNo: String? = null

        @SerializedName("addressDetails")
        @Expose
        private var addressDetails: String? = null

        @SerializedName("typeName")
        @Expose
        private var typeName: Any? = null

        @SerializedName("columnValue")
        @Expose
        private var columnValue: Any? = null

        fun getUserDetailsID(): Int? {
            return userDetailsID
        }

        fun setUserDetailsID(userDetailsID: Int?) {
            this.userDetailsID = userDetailsID
        }

        fun getNationalID(): String? {
            return nationalID
        }

        fun setNationalID(nationalID: String?) {
            this.nationalID = nationalID
        }

        fun getFatherName(): String? {
            return fatherName
        }

        fun setFatherName(fatherName: String?) {
            this.fatherName = fatherName
        }

        fun getMotherName(): String? {
            return motherName
        }

        fun setMotherName(motherName: String?) {
            this.motherName = motherName
        }

        fun getMaritalStatus(): String? {
            return maritalStatus
        }

        fun setMaritalStatus(maritalStatus: String?) {
            this.maritalStatus = maritalStatus
        }

        fun getNumberOfChild(): String? {
            return numberOfChild
        }

        fun setNumberOfChild(numberOfChild: String?) {
            this.numberOfChild = numberOfChild
        }

        fun getNationality(): String? {
            return nationality
        }

        fun setNationality(nationality: String?) {
            this.nationality = nationality
        }

        fun getAddress(): Any? {
            return address
        }

        fun setAddress(address: Any?) {
            this.address = address
        }

        fun getLatitude(): String? {
            return latitude
        }

        fun setLatitude(latitude: String?) {
            this.latitude = latitude
        }

        fun getLongitude(): String? {
            return longitude
        }

        fun setLongitude(longitude: String?) {
            this.longitude = longitude
        }

        fun getOccupation(): Any? {
            return occupation
        }

        fun setOccupation(occupation: Any?) {
            this.occupation = occupation
        }

        fun getPassportNo(): String? {
            return passportNo
        }

        fun setPassportNo(passportNo: String?) {
            this.passportNo = passportNo
        }

        fun getBirthCertificateNo(): String? {
            return birthCertificateNo
        }

        fun setBirthCertificateNo(birthCertificateNo: String?) {
            this.birthCertificateNo = birthCertificateNo
        }

        fun getAddressDetails(): String? {
            return addressDetails
        }

        fun setAddressDetails(addressDetails: String?) {
            this.addressDetails = addressDetails
        }

        fun getTypeName(): Any? {
            return typeName
        }

        fun setTypeName(typeName: Any?) {
            this.typeName = typeName
        }

        fun getColumnValue(): Any? {
            return columnValue
        }

        fun setColumnValue(columnValue: Any?) {
            this.columnValue = columnValue
        }
    }
}
