package com.netizen.netiworld.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class RevenueLogGetData {

    @SerializedName("date")
    @Expose
    private var date: String? = null
    @SerializedName("amount")
    @Expose
    private var amount: String? = null
    @SerializedName("grade")
    @Expose
    private var grade: String? = null
    @SerializedName("purchase_id")
    @Expose
    private var purchaseId: Int? = null
    @SerializedName("revenue_type")
    @Expose
    private var revenueType: String? = null
    @SerializedName("grade_percent")
    @Expose
    private var gradePercent: String? = null
    @SerializedName("productName")
    @Expose
    private var productName: String? = null

    fun getDate(): String? {
        return date
    }

    fun setDate(date: String?) {
        this.date = date
    }

    fun getAmount(): String? {
        return amount
    }

    fun setAmount(amount: String?) {
        this.amount = amount
    }

    fun getGrade(): String? {
        return grade
    }

    fun setGrade(grade: String?) {
        this.grade = grade
    }

    fun getPurchaseId(): Int? {
        return purchaseId
    }

    fun setPurchaseId(purchaseId: Int?) {
        this.purchaseId = purchaseId
    }

    fun getRevenueType(): String? {
        return revenueType
    }

    fun setRevenueType(revenueType: String?) {
        this.revenueType = revenueType
    }

    fun getGradePercent(): String? {
        return gradePercent
    }

    fun setGradePercent(gradePercent: String?) {
        this.gradePercent = gradePercent
    }

    fun getProductName(): String? {
        return productName
    }

    fun setProductName(productName: String?) {
        this.productName = productName
    }
}