package com.netizen.netiworld.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AreaModel {
    @SerializedName("coreCategoryID")
    @Expose
    private var coreCategoryID: String? = null

    @SerializedName("categoryName")
    @Expose
    private var categoryName: String? = null

    fun getCoreCategoryID(): String? {
        return coreCategoryID
    }

    fun setCoreCategoryID(coreCategoryID: String) {
        this.coreCategoryID = coreCategoryID
    }

    fun getCategoryName(): String? {
        return categoryName
    }

    fun setCategoryName(categoryName: String) {
        this.categoryName = categoryName
    }
}