package com.netizen.netiworld.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class BalanceDepositAccountInfo {

    @SerializedName("coreBankAccId")
    @Expose
    private var coreBankAccId: Int? = null
    @SerializedName("accDefaultCode")
    @Expose
    private var accDefaultCode: String? = null
    @SerializedName("accInfo")
    @Expose
    private var accInfo: String? = null
    @SerializedName("accEnableStatus")
    @Expose
    private var accEnableStatus: Int? = null
    @SerializedName("accSerial")
    @Expose
    private var accSerial: Int? = null
    @SerializedName("accNote")
    @Expose
    private var accNote: String? = null
    @SerializedName("maxRecharge")
    @Expose
    private var maxRecharge: Double? = null
    @SerializedName("minRecharge")
    @Expose
    private var minRecharge: Double? = null
    @SerializedName("accShortName")
    @Expose
    private var accShortName: String? = null
    @SerializedName("lastUserExecuted")
    @Expose
    private var lastUserExecuted: String? = null
    @SerializedName("lastIpExecuted")
    @Expose
    private var lastIpExecuted: String? = null
    @SerializedName("lastDateExecuted")
    @Expose
    private var lastDateExecuted: String? = null
    @Expose
    private var coreCategoryInfoDTO: CoreCategoryInfoDTO? = null

    fun getCoreBankAccId(): Int? {
        return coreBankAccId
    }

    fun setCoreBankAccId(coreBankAccId: Int?) {
        this.coreBankAccId = coreBankAccId
    }

    fun getAccDefaultCode(): String? {
        return accDefaultCode
    }

    fun setAccDefaultCode(accDefaultCode: String?) {
        this.accDefaultCode = accDefaultCode
    }

    fun getAccInfo(): String? {
        return accInfo
    }

    fun setAccInfo(accInfo: String?) {
        this.accInfo = accInfo
    }

    fun getAccEnableStatus(): Int? {
        return accEnableStatus
    }

    fun setAccEnableStatus(accEnableStatus: Int?) {
        this.accEnableStatus = accEnableStatus
    }

    fun getAccSerial(): Int? {
        return accSerial
    }

    fun setAccSerial(accSerial: Int?) {
        this.accSerial = accSerial
    }

    fun getAccNote(): String? {
        return accNote
    }

    fun setAccNote(accNote: String?) {
        this.accNote = accNote
    }

    fun getMaxRecharge(): Double? {
        return maxRecharge
    }

    fun setMaxRecharge(maxRecharge: Double?) {
        this.maxRecharge = maxRecharge
    }

    fun getMinRecharge(): Double? {
        return minRecharge
    }

    fun setMinRecharge(minRecharge: Double?) {
        this.minRecharge = minRecharge
    }

    fun getAccShortName(): String? {
        return accShortName
    }

    fun setAccShortName(accShortName: String?) {
        this.accShortName = accShortName
    }

    fun getLastUserExecuted(): String? {
        return lastUserExecuted
    }

    fun setLastUserExecuted(lastUserExecuted: String?) {
        this.lastUserExecuted = lastUserExecuted
    }

    fun getLastIpExecuted(): String? {
        return lastIpExecuted
    }

    fun setLastIpExecuted(lastIpExecuted: String?) {
        this.lastIpExecuted = lastIpExecuted
    }

    fun getLastDateExecuted(): String? {
        return lastDateExecuted
    }

    fun setLastDateExecuted(lastDateExecuted: String?) {
        this.lastDateExecuted = lastDateExecuted
    }

    fun getCoreCategoryInfoDTO(): CoreCategoryInfoDTO? {
        return coreCategoryInfoDTO
    }

    fun setCoreCategoryInfoDTO(coreCategoryInfoDTO: CoreCategoryInfoDTO?) {
        this.coreCategoryInfoDTO = coreCategoryInfoDTO
    }

    class CoreCategoryInfoDTO {

        @SerializedName("parentTypeInfoDTO")
        @Expose
        private var parentTypeInfoDTO: ParentTypeInfoDTO? = null

        fun getParentTypeInfoDTO(): ParentTypeInfoDTO? {
            return parentTypeInfoDTO
        }

        fun setParentTypeInfoDTO(parentTypeInfoDTO: ParentTypeInfoDTO?) {
            this.parentTypeInfoDTO = parentTypeInfoDTO
        }

        class ParentTypeInfoDTO {

            @SerializedName("categoryName")
            @Expose
            private var categoryName: String? = null

            fun getCategoryName(): String? {
                return categoryName
            }

            fun setCategoryName(categoryName: String?) {
                this.categoryName = categoryName
            }
        }
    }
}

