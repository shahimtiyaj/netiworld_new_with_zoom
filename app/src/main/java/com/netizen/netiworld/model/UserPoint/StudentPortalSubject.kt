package com.netizen.netiworld.model.UserPoint

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class StudentPortalSubject {
    @SerializedName("message")
    @Expose
    private var message: Any? = null

    @SerializedName("msgType")
    @Expose
    private var msgType: Int? = null

    @SerializedName("item")
    @Expose
    private var item: List<Item>? = null

    fun getMessage(): Any? {
        return message
    }

    fun setMessage(message: Any?) {
        this.message = message
    }

    fun getMsgType(): Int? {
        return msgType
    }

    fun setMsgType(msgType: Int?) {
        this.msgType = msgType
    }

    fun getItem(): List<Item?>? {
        return item
    }

    fun setItem(item: List<Item>?) {
        this.item = item
    }

    class Item {
        @SerializedName("subjectId")
        @Expose
        private var subjectId: Int? = null

        @SerializedName("subjectName")
        @Expose
        private var subjectName: String? = null

        @SerializedName("subjectMergeId")
        @Expose
        private var subjectMergeId: Int? = null

        @SerializedName("subjectSerial")
        @Expose
        private var subjectSerial: Int? = null

        @SerializedName("subjectType")
        @Expose
        private var subjectType: String? = null

        fun getSubjectId(): Int? {
            return subjectId
        }

        fun setSubjectId(subjectId: Int?) {
            this.subjectId = subjectId
        }

        fun getSubjectName(): String? {
            return subjectName
        }

        fun setSubjectName(subjectName: String?) {
            this.subjectName = subjectName
        }

        fun getSubjectMergeId(): Int? {
            return subjectMergeId
        }

        fun setSubjectMergeId(subjectMergeId: Int?) {
            this.subjectMergeId = subjectMergeId
        }

        fun getSubjectSerial(): Int? {
            return subjectSerial
        }

        fun setSubjectSerial(subjectSerial: Int?) {
            this.subjectSerial = subjectSerial
        }

        fun getSubjectType(): String? {
            return subjectType
        }

        fun setSubjectType(subjectType: String?) {
            this.subjectType = subjectType
        }

    }
}