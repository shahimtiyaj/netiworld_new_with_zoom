package com.netizen.netiworld.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


@JsonIgnoreProperties(ignoreUnknown = true)
class BalanceStatementGetData {

    @SerializedName("transactionDate")
    @Expose
    private var transactionDate: Long? = null

    @SerializedName("netiID")
    @Expose
    private var netiID: Any? = null

    @SerializedName("customID")
    @Expose
    private var customID: Any? = null

    @SerializedName("fullName")
    @Expose
    private var fullName: Any? = null

    @SerializedName("basicMobile")
    @Expose
    private var basicMobile: Any? = null

    @SerializedName("note")
    @Expose
    private var note: Any? = null

    @SerializedName("amount")
    @Expose
    private var amount: Double? = null

    @SerializedName("transactionFor")
    @Expose
    private var transactionFor: String? = null

    @SerializedName("income")
    @Expose
    private var income: Double? = null

    @SerializedName("expense")
    @Expose
    private var expense: Double? = null

    @SerializedName("trxAmount")
    @Expose
    private var trxAmount: String? = null

    fun getTransactionDate(): Long? {
        return transactionDate
    }

    fun setTransactionDate(transactionDate: Long?) {
        this.transactionDate = transactionDate
    }

    fun getNetiID(): Any? {
        return netiID
    }

    fun setNetiID(netiID: Any?) {
        this.netiID = netiID
    }

    fun getCustomID(): Any? {
        return customID
    }

    fun setCustomID(customID: Any?) {
        this.customID = customID
    }

    fun getFullName(): Any? {
        return fullName
    }

    fun setFullName(fullName: Any?) {
        this.fullName = fullName
    }

    fun getBasicMobile(): Any? {
        return basicMobile
    }

    fun setBasicMobile(basicMobile: Any?) {
        this.basicMobile = basicMobile
    }

    fun getNote(): Any? {
        return note
    }

    fun setNote(note: Any?) {
        this.note = note
    }

    fun getAmount(): Double? {
        return amount
    }

    fun setAmount(amount: Double?) {
        this.amount = amount
    }

    fun getTransactionFor(): String? {
        return transactionFor
    }

    fun setTransactionFor(transactionFor: String?) {
        this.transactionFor = transactionFor
    }

    fun getIncome(): Double? {
        return income
    }

    fun setIncome(income: Double?) {
        this.income = income
    }

    fun getExpense(): Double? {
        return expense
    }

    fun setExpense(expense: Double?) {
        this.expense = expense
    }

    fun getTrxAmount(): String? {
        return trxAmount
    }

    fun setTrxAmount(trxAmount: String?) {
        this.trxAmount = trxAmount
    }
}

