package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleReferenceInfoLayoutBinding
import com.netizen.netiworld.model.ReferenceInfo

class ReferenceInfoAdapter(
    private val context: Context,
    private val referenceInfoList: List<ReferenceInfo>,
    private val listener: OnEditClickListener
) : RecyclerView.Adapter<ReferenceInfoAdapter.ViewHolder>() {

    interface OnEditClickListener {
        fun onEditClick(referenceInfo: ReferenceInfo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_reference_info_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return referenceInfoList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(referenceInfoList[position], position + 1)

        holder.itemBinding.imageViewReferenceInfoEdit.setOnClickListener {
            listener.onEditClick(referenceInfoList[position])
        }
    }

    class ViewHolder(val itemBinding: SingleReferenceInfoLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(referenceInfo: ReferenceInfo, position: Int) {
            itemBinding.textViewReferenceInfoNo.text = "Reference - $position"
            itemBinding.textViewName.text = referenceInfo.getReferenceName()
            itemBinding.textViewDesignation.text = referenceInfo.getReferenceDesignation()
            itemBinding.textViewOrganization.text = referenceInfo.getOrgainizationName()
            itemBinding.textViewMobileNo.text = referenceInfo.getReferenceMobile()
            itemBinding.textViewEmail.text = referenceInfo.getReferenceEmail()
            itemBinding.textViewAddress.text = referenceInfo.getReferenceAddress()
            itemBinding.textViewRelation.text = referenceInfo.getRelationInfoDTO()?.getCategoryName()
        }
    }
}