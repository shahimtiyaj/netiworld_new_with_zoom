package com.netizen.netiworld.adapter.userPoint

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.netizen.netiworld.view.fragment.userPoint.ClassTestFragment
import com.netizen.netiworld.view.fragment.userPoint.SemesterExamFragment

class ExamInfoPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return ClassTestFragment()
            1 -> return SemesterExamFragment()
        }
        return ClassTestFragment()
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return "Class Test"
            1 -> return " Semester Exam"
        }
        return null
    }
}