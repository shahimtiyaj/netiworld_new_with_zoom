package com.netizen.netiworld.adapter.userPoint

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SubjectListItemBinding
import com.netizen.netiworld.model.UserPoint.StudentPortalSubject

class SubjectInfoAdapter(
    private val context: Context,
    private val subjectList: List<StudentPortalSubject.Item>
) : RecyclerView.Adapter<SubjectInfoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.subject_list_item,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return subjectList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(subjectList[position])
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class ViewHolder(private val itemBinding: SubjectListItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(subject: StudentPortalSubject.Item) {
            itemBinding.textViewSubjectName.text = subject.getSubjectName()
        }
    }

    companion object {
        private val TAG = SubjectInfoAdapter::class.java.simpleName
    }
}