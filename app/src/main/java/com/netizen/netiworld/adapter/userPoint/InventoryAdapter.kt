package com.netizen.netiworld.adapter.userPoint

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.InventoryListItemBinding
import com.netizen.netiworld.model.UserPoint.InventoryDetails

class InventoryAdapter(
    private val context: Context,
    private val inventoryDetailsList: List<InventoryDetails.Item>
) : RecyclerView.Adapter<InventoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.inventory_list_item,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return inventoryDetailsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(inventoryDetailsList[position])
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class ViewHolder(private val itemBinding: InventoryListItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(inventoryDetails: InventoryDetails.Item) {
            itemBinding.textViewPurchaseDate.text = inventoryDetails.getDate()
            itemBinding.textViewCategory.text = inventoryDetails.getCategory()
            itemBinding.textViewGradeItem.text = inventoryDetails.getItem()
            itemBinding.textViewUnitPrice.text = inventoryDetails.getUnitPrice().toString()
            itemBinding.textViewQuantity.text = inventoryDetails.getQuantity().toString()
            itemBinding.textViewTotalPrice.text = inventoryDetails.getTotalPrice().toString()
        }
    }

    companion object {
        private val TAG = InventoryAdapter::class.java.simpleName
    }
}