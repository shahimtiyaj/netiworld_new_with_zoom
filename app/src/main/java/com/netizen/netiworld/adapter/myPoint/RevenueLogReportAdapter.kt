package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleRevenueReportLayoutBinding
import com.netizen.netiworld.model.RevenueLogGetData

class RevenueLogReportAdapter(private val context: Context,
                              private val revenueLogList: List<RevenueLogGetData?>?
) : RecyclerView.Adapter<RevenueLogReportAdapter.ViewHolder>() {

    class ViewHolder(private val itemBinding: SingleRevenueReportLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(revenueLogGetData: RevenueLogGetData?) {
            itemBinding.textViewDate.text = revenueLogGetData?.getDate()
            itemBinding.textViewRevenueType.text = revenueLogGetData?.getRevenueType()
            itemBinding.textViewProductName.text = revenueLogGetData?.getProductName()
            itemBinding.textViewAmount.text = revenueLogGetData?.getAmount()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_revenue_report_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return revenueLogList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(revenueLogList?.get(position))
    }
}