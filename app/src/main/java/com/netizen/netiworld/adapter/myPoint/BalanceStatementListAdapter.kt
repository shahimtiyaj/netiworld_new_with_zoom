package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.ReportsBalanceStatementRowBinding
import com.netizen.netiworld.model.BalanceStatementGetData
import com.netizen.netiworld.utils.AppUtilsClass

class BalanceStatementListAdapter(
    private val context: Context,
    private val balanceStatementReportsList: List<BalanceStatementGetData?>?
) : RecyclerView.Adapter<BalanceStatementListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.reports_balance_statement_row,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return balanceStatementReportsList?.size!!
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(balanceStatementReportsList?.get(position))
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class ViewHolder(private val itemBinding: ReportsBalanceStatementRowBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(balanceStatementReport: BalanceStatementGetData?) {
//            if (!balanceStatementReport?.getTrxAmount().isNullOrEmpty()) {
//                val amount = balanceStatementReport?.getTrxAmount()?.split("(")
//                itemBinding.textViewAmount.text = String.format(
//                    "%s",
//                    AppUtilsClass.getDecimalFormattedValue(amount?.get(0)!!.toDouble()) + " (" + amount[1]
//                )
//            }

            itemBinding.textViewDate.text = AppUtilsClass.getDate(balanceStatementReport?.getTransactionDate())
            itemBinding.textViewTransaction.text = balanceStatementReport?.getTransactionFor()
            itemBinding.textViewCashIn.text = String.format("%,.2f", balanceStatementReport?.getIncome())
            itemBinding.textViewCashOut.text = String.format("%,.2f", balanceStatementReport?.getExpense())
        }
    }

    companion object {
        private val TAG = BalanceStatementListAdapter::class.java.simpleName
    }
}