package com.netizen.netiworld.adapter.userPoint

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.AttendanceListItemBinding
import com.netizen.netiworld.model.UserPoint.StudentPortalSubject


class AttendanceInfoAdapter(
    private val context: Context,
    private val attendanceList: List<StudentPortalSubject.Item>
) : RecyclerView.Adapter<AttendanceInfoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.attendance_list_item,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return attendanceList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(attendanceList[position])
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class ViewHolder(private val itemBinding: AttendanceListItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(subject: StudentPortalSubject.Item) {
            itemBinding.textViewDate.text = subject.getSubjectName()
            itemBinding.textViewDay.text = subject.getSubjectName()
            itemBinding.textViewStatus.text = subject.getSubjectName()
        }
    }

    companion object {
        private val TAG = AttendanceInfoAdapter::class.java.simpleName
    }
}