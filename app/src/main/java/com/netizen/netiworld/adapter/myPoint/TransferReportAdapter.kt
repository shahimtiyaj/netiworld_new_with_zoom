package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleCommonReportLayoutBinding
import com.netizen.netiworld.model.BalanceTransferGetData
import com.netizen.netiworld.utils.AppUtilsClass

class TransferReportAdapter(private val context: Context,
                            private val balanceTransferReportList: List<BalanceTransferGetData?>?
) : RecyclerView.Adapter<TransferReportAdapter.ViewHolder>() {

    class ViewHolder(private val itemBinding: SingleCommonReportLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(balanceTransferGetData: BalanceTransferGetData?) {
            itemBinding.textViewDate.text = AppUtilsClass.getDate(balanceTransferGetData?.transactionDate?.toLong())
            itemBinding.textViewType.text = balanceTransferGetData?.transactionFor
            itemBinding.textViewAmount.text = balanceTransferGetData?.amount.toString() + " Taka"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_common_report_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return balanceTransferReportList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(balanceTransferReportList?.get(position))
    }
}