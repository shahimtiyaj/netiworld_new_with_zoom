package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleMessageLogReportLayoutBinding
import com.netizen.netiworld.model.BalanceMessageGetData
import com.netizen.netiworld.utils.AppUtilsClass

class BalanceMessageListAdapter (
    private val context: Context,
    private val messageRechargeReportsList: List<BalanceMessageGetData?>
) : RecyclerView.Adapter<BalanceMessageListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_message_log_report_layout,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return messageRechargeReportsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(messageRechargeReportsList[position])
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }


    class ViewHolder(private val itemBinding: SingleMessageLogReportLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(messageReport: BalanceMessageGetData?) {

            itemBinding.textViewDate.text = messageReport?.getTrxDate()?.toLong()?.let { AppUtilsClass.getDate(it) }
            itemBinding.textViewMessageType.text = messageReport?.productInfoDTO?.getProductName().toString()
            itemBinding.textViewQuantity.text = messageReport?.getQuantity().toString()
            itemBinding.textViewAmount.text = String.format("%,.2f", messageReport?.productPurchaseLogDTO?.getPayableAmount())
        }
    }

    companion object {
        private val TAG = BalanceMessageListAdapter::class.java.simpleName
    }
}