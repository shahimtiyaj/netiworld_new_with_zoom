package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleBankInfoLayoutBinding
import com.netizen.netiworld.model.TagGetData

class BankInfoAdapter(
    private val context: Context,
    private val tagList: List<TagGetData>,
    private val listener: OnEditClickListener
) : RecyclerView.Adapter<BankInfoAdapter.ViewHolder>() {

    interface OnEditClickListener {
        fun onEditClick(tagGetData: TagGetData)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_bank_info_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return tagList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(tagList[position], position + 1)

        holder.itemBinding.imageViewUserInfoEdit.setOnClickListener {
            listener.onEditClick(tagList[position])
        }
    }

    class ViewHolder(val itemBinding: SingleBankInfoLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(tagGetData: TagGetData, position: Int) {
            itemBinding.textViewBankNumber.text = "Bank - $position"
            itemBinding.textViewUsingPurpose.text = tagGetData.getTaggingTypeCoreCategoryInfoDTO()?.getCategoryName()
            itemBinding.textViewBankName.text = tagGetData.getUserBankAccountInfoDTO()?.getCoreBankBranchInfoDTO()?.getCoreBankInfoDTO()?.getCategoryName()
            itemBinding.textViewBranch.text = tagGetData.getUserBankAccountInfoDTO()?.getCoreBankBranchInfoDTO()?.getBranchName()
            itemBinding.textViewDistrict.text = "---"
            itemBinding.textViewRoutingNumber.text = tagGetData.getUserBankAccountInfoDTO()?.getCoreBankBranchInfoDTO()?.getRoutingNumber().toString()
            itemBinding.textViewAccountHolder.text = tagGetData.getUserBankAccountInfoDTO()?.getBankAccHolderName()
            itemBinding.textViewAccountNo.text = tagGetData.getUserBankAccountInfoDTO()?.getBankAccNumber().toString()
            itemBinding.textViewAccountDetails.text = "None"
        }
    }
}