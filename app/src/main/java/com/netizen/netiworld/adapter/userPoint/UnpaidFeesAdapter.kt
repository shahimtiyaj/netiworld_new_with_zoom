package com.netizen.netiworld.adapter.userPoint

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FeesInfoPaidItemBinding
import com.netizen.netiworld.model.UserPoint.UnpaidFees

class UnpaidFeesAdapter(
    private val context: Context,
    private val unpaidFessList: List<UnpaidFees.Item>
) : RecyclerView.Adapter<UnpaidFeesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.fees_info_paid_item,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return unpaidFessList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(unpaidFessList[position])
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class ViewHolder(private val itemBinding: FeesInfoPaidItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(unpaidFees: UnpaidFees.Item) {
            itemBinding.textViewInvoiceId.text = unpaidFees.getInvoiceId()
            itemBinding.textViewPaymentDate.text = unpaidFees.getDate()
            itemBinding.textViewFeeHead.text = unpaidFees.getFeeHeadsName()
            itemBinding.textViewFeeSubHead.text = unpaidFees.getFeeSubHeadsName()
            itemBinding.textViewPayableAmt.text = unpaidFees.getPayableAmt().toString()
            itemBinding.textViewPaidAmt.text = unpaidFees.getPaidAmt().toString()
            itemBinding.textViewDueAmt.text = unpaidFees.getDueAmt().toString()
        }
    }

    companion object {
        private val TAG = AttendanceInfoAdapter::class.java.simpleName
    }
}