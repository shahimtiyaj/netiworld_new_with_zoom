package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.ReportsPurchaseUsedCodeRowBinding
import com.netizen.netiworld.model.PurchaseCodeLogGetdata
import com.netizen.netiworld.utils.AppUtilsClass

class PurchaseUsedCodeListAdapter (
    private val context: Context,
    private val usedCodeReportsList: List<PurchaseCodeLogGetdata?>
) : RecyclerView.Adapter<PurchaseUsedCodeListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.reports_purchase_used_code_row,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return usedCodeReportsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(usedCodeReportsList[position])
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class ViewHolder(private val itemBinding: ReportsPurchaseUsedCodeRowBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(unusedCodeReport: PurchaseCodeLogGetdata?) {
            itemBinding.requestDate.text = unusedCodeReport?.getPurchaseDate()?.toLong()?.let { AppUtilsClass.getDate(it) }
            itemBinding.purchasePointVal.text = unusedCodeReport?.getCoreRoleNote()
           // itemBinding.productTypeVal.text = unusedCodeReport?.getProductType()
           // itemBinding.productNameVal.text = unusedCodeReport?.getProductName()
            itemBinding.purchaseCodeVal.text = unusedCodeReport?.getPurchaseCode()
        }
    }

    companion object {
        private val TAG = ProductsOfferListAdapter::class.java.simpleName
    }
}