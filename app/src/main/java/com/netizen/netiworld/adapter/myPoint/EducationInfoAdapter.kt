package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleEducationInfoLayoutBinding
import com.netizen.netiworld.model.EducationInfo

class EducationInfoAdapter(
    private val context: Context,
    private val educationInfoList: List<EducationInfo>,
    private val listener: OnEditClickListener
) : RecyclerView.Adapter<EducationInfoAdapter.ViewHolder>() {

    interface OnEditClickListener {
        fun onEditClick(educationInfo: EducationInfo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_education_info_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return educationInfoList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(educationInfoList[position], position + 1)
        holder.itemBinding.imageViewAcademicInfoEdit.setOnClickListener {
            listener.onEditClick(educationInfoList[position])
        }
    }

    class ViewHolder(val itemBinding: SingleEducationInfoLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(educationInfo: EducationInfo, position: Int) {
            itemBinding.textViewEducationNo.text = "Academic Education - $position"
            itemBinding.textViewLevelOfEducation.text = educationInfo.getDegreeInfoDTO()?.getCategoryName()
            itemBinding.textViewExamTitle.text = educationInfo.getExamTitle()
            itemBinding.textViewMajor.text = educationInfo.getSubjectInfoDTO()?.getCategoryName()
            itemBinding.textViewResult.text = educationInfo.getGradeInfoDTO()?.getCategoryName()
            itemBinding.textViewPassingYear.text = educationInfo.getPassingYearInfoDTO()?.getCategoryName()
            itemBinding.textViewDuration.text = educationInfo.getEduDuration()
            itemBinding.textViewBoard.text = educationInfo.getBoardInfoDTO()?.getCategoryName()
            itemBinding.textViewInstituteName.text = educationInfo.getInstituteName()
            itemBinding.textViewAchievement.text = educationInfo.getAchievementDetails()

            if (educationInfo.getInstituteForeginStatus() == 1) {
                itemBinding.textViewForeignInstitute.text = "Yes"
            } else {
                itemBinding.textViewForeignInstitute.text = "No"
            }
        }
    }
}