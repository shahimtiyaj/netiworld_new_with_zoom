package com.netizen.netiworld.adapter.userPoint

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.netizen.netiworld.view.fragment.userPoint.ClassRoutineFragment
import com.netizen.netiworld.view.fragment.userPoint.ExamRoutineFragment

class RoutineInfoPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return ClassRoutineFragment()
            1 -> return ExamRoutineFragment()
        }
        return ClassRoutineFragment()
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position) {
            0 -> return "Class Routine"
            1 -> return "Exam Routine"
        }
        return null
    }
}