package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleTokenLayoutBinding
import com.netizen.netiworld.model.TokenList
import com.netizen.netiworld.utils.AppUtilsClass

class TokenListAdapter(private val context: Context,
                       private val tokenList: List<TokenList>,
                       private val listener: OnItemClick
) : RecyclerView.Adapter<TokenListAdapter.ViewHolder>() {

    interface OnItemClick {
        fun onItemClick(tokenList: TokenList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_token_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return tokenList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(context, tokenList[position])

        holder.itemBinding.root.setOnClickListener {
            listener.onItemClick(tokenList[position])
        }
    }

    class ViewHolder(val itemBinding: SingleTokenLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(context: Context, tokenList: TokenList) {
            itemBinding.textViewTokenId.text = tokenList.getCustomTokenID()
            itemBinding.textViewProblemType.text = tokenList.getTokenTypeInfoDTO()?.getCategoryName()
            itemBinding.textViewCreateDate.text = AppUtilsClass.getDate(tokenList.getCreateDate()!!.toLong())

            when (tokenList.getTokenStatus()) {
                0 -> {
                    itemBinding.textViewStatus.text = "Pending"
                    itemBinding.textViewStatus.setTextColor(context.resources.getColor(android.R.color.holo_red_dark))
                    itemBinding.ratingBar.visibility = View.GONE
                }
                10 -> {
                    itemBinding.textViewStatus.text = "Solved"
                    itemBinding.textViewStatus.setTextColor(context.resources.getColor(R.color.green))
                }
                else -> {
                    itemBinding.textViewStatus.text = "N/A"
                }
            }
        }
    }
}