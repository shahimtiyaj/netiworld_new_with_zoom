package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleCommonReportLayoutBinding
import com.netizen.netiworld.model.PurchaseCodeLogGetdata

class PurchaseUnusedCodeListAdapter(
    private val context: Context,
    private val unusedCodeReportsList: List<PurchaseCodeLogGetdata?>
) : RecyclerView.Adapter<PurchaseUnusedCodeListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_common_report_layout,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return unusedCodeReportsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(unusedCodeReportsList[position])
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class ViewHolder(private val itemBinding: SingleCommonReportLayoutBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(unusedCodeReport: PurchaseCodeLogGetdata?) {
           // itemBinding.textViewDate.text = unusedCodeReport?.getPurchaseDate()?.toLong()?.let { MyUtilsClass.getDate(it) }
           // itemBinding.purchasePointVal.text = unusedCodeReport?.getCoreRoleNote()
            itemBinding.textViewType.text = unusedCodeReport?.productPurchaseLogDTO?.productInfoDTO?.productTypeInfoDTO?.getProductType()
             itemBinding.textViewDate.text = unusedCodeReport?.productPurchaseLogDTO?.productInfoDTO?.getProductName()
            itemBinding.textViewAmount.text = unusedCodeReport?.getPurchaseCode()
        }
    }

    companion object {
        private val TAG = ProductsOfferListAdapter::class.java.simpleName
    }
}