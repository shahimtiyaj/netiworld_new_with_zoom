package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleCertificationLayoutBinding
import com.netizen.netiworld.model.CertificationInfo
import com.netizen.netiworld.utils.AppUtilsClass

class CertificationAdapter(
    private val context: Context,
    private val certificationInfoList: List<CertificationInfo>,
    private val listener: OnEditClickListener
) : RecyclerView.Adapter<CertificationAdapter.ViewHolder>() {

    interface OnEditClickListener {
        fun onEditClick(certificationInfo: CertificationInfo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_certification_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return certificationInfoList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(certificationInfoList[position], position + 1)
        holder.itemBinding.imageViewQualificationEdit.setOnClickListener {
            listener.onEditClick(certificationInfoList[position])
        }
    }

    class ViewHolder(val itemBinding: SingleCertificationLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(certificationInfo: CertificationInfo, position: Int) {
            itemBinding.textViewQualificationNo.text = "Personal Qualification - $position"
            itemBinding.textViewCertificationName.text = certificationInfo.getCertificationName()
            itemBinding.textViewInstitute.text = certificationInfo.getInstituteName()
            itemBinding.textViewLocation.text = certificationInfo.getInstituteLocation() + ", " + certificationInfo.getCountryInfoDTO()?.getCategoryName()
            itemBinding.textViewDate.text = AppUtilsClass.getDate(certificationInfo.getAchieveDate())
            itemBinding.textViewDuration.text = certificationInfo.getCourseDuration().toString()
        }
    }
}