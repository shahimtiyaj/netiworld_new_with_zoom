package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleCommonReportLayoutBinding
import com.netizen.netiworld.model.GeneralProductGetData
import com.netizen.netiworld.utils.AppUtilsClass

class GeneralProductReportAdapter(private val context: Context,
                                  private val generalProductReportList: List<GeneralProductGetData?>?
) : RecyclerView.Adapter<GeneralProductReportAdapter.ViewHolder>() {

    class ViewHolder(private val itemBinding: SingleCommonReportLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(generalProductGetData: GeneralProductGetData?) {
            itemBinding.textViewDate.text = AppUtilsClass.getDate(generalProductGetData?.getPurchaseDate()?.toLong())
            itemBinding.textViewType.text = generalProductGetData?.getProductInfoDTO()?.getProductName().toString()
            itemBinding.textViewAmount.text = generalProductGetData?.getTotalAmount().toString() + " Taka"
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_common_report_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return generalProductReportList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(generalProductReportList?.get(position))
    }
}