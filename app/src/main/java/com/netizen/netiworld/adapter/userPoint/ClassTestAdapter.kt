package com.netizen.netiworld.adapter.userPoint

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.ClassTestListItemBinding
import com.netizen.netiworld.model.UserPoint.ClassTestDetails

class ClassTestAdapter(
    private val context: Context,
    private val classTestDetailsList: List<ClassTestDetails.Item.StdCtExamMark>
) : RecyclerView.Adapter<ClassTestAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.class_test_list_item,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return classTestDetailsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(classTestDetailsList[position])
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class ViewHolder(private val itemBinding: ClassTestListItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(classTestDetails: ClassTestDetails.Item.StdCtExamMark) {
            itemBinding.textViewSubjectName.text = classTestDetails.getSubjectName()
            itemBinding.textViewFullMark.text = classTestDetails.getSubjectFullMark().toString()
            itemBinding.textViewHighestMark.text = classTestDetails.getHighestMarks().toString()
            itemBinding.textViewObtainedMark.text = classTestDetails.getObtainedMark().toString()
            itemBinding.textViewLetterGrade.visibility = View.GONE
            itemBinding.textViewGradePoint.visibility = View.GONE
            itemBinding.layoutLinearLetterGradeId.visibility = View.GONE
            itemBinding.layoutLinearLetterGradePointId.visibility = View.GONE
        }
    }

    companion object {
        private val TAG = InventoryAdapter::class.java.simpleName
    }
}