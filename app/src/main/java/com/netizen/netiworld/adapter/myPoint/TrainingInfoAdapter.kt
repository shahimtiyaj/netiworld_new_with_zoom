package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleTrainingInfoLayoutBinding
import com.netizen.netiworld.model.TrainingInfo

class TrainingInfoAdapter(
    private val context: Context,
    private val trainingInfoList: List<TrainingInfo>,
    private val listener: OnEditClickListener
) : RecyclerView.Adapter<TrainingInfoAdapter.ViewHolder>() {

    interface OnEditClickListener {
        fun onEditClick(trainingInfo: TrainingInfo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_training_info_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return trainingInfoList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(trainingInfoList[position], position + 1)
        holder.itemBinding.imageViewTrainingInfoEdit.setOnClickListener {
            listener.onEditClick(trainingInfoList[position])
        }
    }

    class ViewHolder(val itemBinding: SingleTrainingInfoLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(trainingInfo: TrainingInfo, position: Int) {
            itemBinding.textViewTrainingNo.text = "Training - $position"
            itemBinding.textViewTrainingTitle.text = trainingInfo.getTrainingTitle()
            itemBinding.textViewTopicCovered.text = trainingInfo.getTopicCoveredDetails()
            itemBinding.textViewInstitute.text = trainingInfo.getInstituteName()
            itemBinding.textViewLocation.text = trainingInfo.getInstituteLocation()
            itemBinding.textViewCountry.text = trainingInfo.getCountryInfoDTO()?.getCategoryName()
            itemBinding.textViewTrainingYear.text = trainingInfo.getTrainingYearInfoDTO()?.getCategoryName()
            itemBinding.textViewDuration.text = trainingInfo.getTrainingDuration().toString()
        }
    }
}