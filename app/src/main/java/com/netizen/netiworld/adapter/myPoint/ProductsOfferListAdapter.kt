package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.ReportsPurchaseProductOfferRowBinding
import com.netizen.netiworld.model.ProductsOfferGetData
import com.netizen.netiworld.utils.AppUtilsClass

class ProductsOfferListAdapter (
    private val context: Context,
    private val productOfferReportsList: List<ProductsOfferGetData?>
) : RecyclerView.Adapter<ProductsOfferListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.reports_purchase_product_offer_row,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return productOfferReportsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
            holder.bind(productOfferReportsList[position])
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
    }

    class ViewHolder(private val itemBinding: ReportsPurchaseProductOfferRowBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(offerReport: ProductsOfferGetData?) {
            itemBinding.requestDate.text = offerReport?.getOfferUseDate()?.toLong()?.let { AppUtilsClass.getDate(it) }
            itemBinding.productTypeVal.text = offerReport?.getProductType()
            itemBinding.productNameVal.text = offerReport?.getProductName()
            itemBinding.offerCodeVal.text = offerReport?.getOfferCode()
            itemBinding.productQuantityVal.text = offerReport?.getProductQuantity().toString()
            itemBinding.totalAmountVal.text = String.format("%, .2f", offerReport?.getTotalPrice())
            itemBinding.discountAmountVal.text = String.format("%, .2f", offerReport?.getTotalDiscount())
            itemBinding.payableAmountVal.text = String.format("%, .2f", offerReport?.getPayableAmount())
        }
}

    companion object {
        private val TAG = ProductsOfferListAdapter::class.java.simpleName
    }
}