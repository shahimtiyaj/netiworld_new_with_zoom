package com.netizen.netiworld.adapter.myPoint

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.SingleExperienceDetailsLayoutBinding
import com.netizen.netiworld.model.ExperienceInfo
import com.netizen.netiworld.utils.AppUtilsClass

class ExperienceInfoAdapter(
    private val context: Context,
    private val experienceInfoList: List<ExperienceInfo>,
    private val listener: OnEditClickListener
) : RecyclerView.Adapter<ExperienceInfoAdapter.ViewHolder>() {

    interface OnEditClickListener {
        fun onEditClick(experienceInfo: ExperienceInfo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.single_experience_details_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return experienceInfoList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(experienceInfoList[position], position + 1)
        holder.itemBinding.imageViewExperienceInfoEdit.setOnClickListener {
            listener.onEditClick(experienceInfoList[position])
        }
    }

    class ViewHolder(val itemBinding: SingleExperienceDetailsLayoutBinding) : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(experienceInfo: ExperienceInfo, position: Int) {
            itemBinding.textViewExperienceNo.text = "Experience Details - $position"
            itemBinding.textViewCompanyName.text = experienceInfo.getCompanyName()
            itemBinding.textViewCompanyBusiness.text = experienceInfo.getCompanyBusiness()
            itemBinding.textViewDesignation.text = experienceInfo.getDesignationName()
            itemBinding.textViewDepartment.text = experienceInfo.getWorkingDepartment()
            itemBinding.textViewCompanyLocation.text = experienceInfo.getCompanyLocation() + ", " + experienceInfo.getCountryInfoDTO()?.getCategoryName()
            itemBinding.textViewStartDate.text = AppUtilsClass.getDate(experienceInfo.getEmploymentStart())
            itemBinding.textViewEndDate.text = AppUtilsClass.getDate(experienceInfo.getEmploymentEnd())
            itemBinding.textViewResponsibilities.text = experienceInfo.getCompanyName()
        }
    }
}