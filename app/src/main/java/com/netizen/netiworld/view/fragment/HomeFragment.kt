package com.netizen.netiworld.view.fragment

import android.app.Application
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentHomeBinding
import com.netizen.netiworld.model.HomePageInfo
import com.netizen.netiworld.utils.AppUtilsClass
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.activity.MainActivity
import com.netizen.netiworld.view.fragment.myPoint.MyPointFragment
import com.netizen.netiworld.view.fragment.userPoint.UserPointFragment
import com.netizen.netiworld.viewModel.HomeViewModel
import com.netizen.netiworld.viewModel.ProfileViewModel
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.all_points_layout.view.*

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var profileViewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home,
            container,
            false
        )

        MainActivity.showBottomNavigation()

        initViews()
        initObservers()
        homeViewModel.getHomePageInfo()
        profileViewModel.getProfileInfo()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel = ViewModelProvider(activity!!,
            HomeViewModel.HomeViewModelFactory(context!!.applicationContext as Application))
            .get(HomeViewModel::class.java)

        profileViewModel = ViewModelProvider(activity!!,
            ProfileViewModel.ProfileViewModelFactory(context!!.applicationContext as Application))
            .get(ProfileViewModel::class.java)
    }

    private fun initViews() {
        binding.layoutAllPoints.layout_my_point.setOnClickListener {
//            findNavController().navigate(R.id.action_homeFragment_to_myPointFragment)
            MyPointFragment().show(activity!!.supportFragmentManager, null)
        }

        binding.layoutAllPoints.layout_user_point.setOnClickListener {
            UserPointFragment().show(activity!!.supportFragmentManager, null)
        }

        binding.layoutAllPoints.layout_neti_academy.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_initAuthSDKActivity)
        }

        binding.imageViewSetting.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_fragmentSetting)
        }

        binding.navigationId?.setOnClickListener {
            MyPointFragment().show(activity!!.supportFragmentManager, null)
        }
    }

    private fun initObservers() {

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        homeViewModel.homePageInfoLiveData.observe(viewLifecycleOwner, Observer {homePageInfo ->
//            Log.e("INFO", homePageInfo.getTokenSummary()?.getSolved().toString())
            setHomePageInfo(homePageInfo)
        })

        homeViewModel.isMyProfileClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_myProfileFragment)
                homeViewModel.isMyProfileClicked.value = false
            }
        })

        homeViewModel.isNetiMailClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_netiMailFragmentOne)
                homeViewModel.isNetiMailClicked.value = false
            }
        })

        homeViewModel.isSupportTokenClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_submitTokenFragment)
                homeViewModel.isSupportTokenClicked.value = false
            }
        })

        homeViewModel.isAddPointClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_addPointFragment)
                homeViewModel.isAddPointClicked.value = false
            }
        })

        homeViewModel.isWalletClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_walletFragment)
                homeViewModel.isWalletClicked.value = false
            }
        })

//        homeViewModel.isMessageClicked.observe(viewLifecycleOwner, Observer {
//            if (it) {
//
//            }
//        })

        homeViewModel.isGeneralProductClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_purchaseGeneralProductFragment)
                homeViewModel.isGeneralProductClicked.value = false
            }
        })

        homeViewModel.isOfferProductClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_fragmentOfferProduct)
                homeViewModel.isOfferProductClicked.value = false
            }
        })

        homeViewModel.isMessageLogClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_messageReportSelectionFragment)
                homeViewModel.isMessageLogClicked.value = false
            }
        })

        homeViewModel.isBankClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_bankFragment)
                homeViewModel.isBankClicked.value = false
            }
        })

        homeViewModel.isStatementReportClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_statementReportSelectionFragment)
                homeViewModel.isStatementReportClicked.value = false
            }
        })

        homeViewModel.isRevenueReportClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_revenueReportSelectionFragment)
                homeViewModel.isRevenueReportClicked.value = false
            }
        })

        homeViewModel.isGeneralProductReportClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_generalProductReportSelectionFragment)
                homeViewModel.isGeneralProductReportClicked.value = false
            }
        })

        homeViewModel.isPurchaseCodeReportClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_tabUsedUnusedCodeFragment)
                homeViewModel.isPurchaseCodeReportClicked.value = false
            }
        })

        homeViewModel.isOfferProductReportClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_offerProductReportSelectionFragment)
                homeViewModel.isOfferProductReportClicked.value = false
            }
        })

        homeViewModel.isWalletLogReportClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_walletReportFragment)
                homeViewModel.isWalletLogReportClicked.value = false
            }
        })

        profileViewModel.profileInformation.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                binding.textViewUserName.text = it.getFullName().toString()
                profileViewModel.getProfileImage(it.getImagePath().toString())
            }
        })

        profileViewModel.photoFileContent.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                val byteArray = Base64.decode(it, Base64.DEFAULT)
                binding.imageViewUser.setImageBitmap(convertToBitmap(byteArray))
            }
        })

        //---------------------------------User Point Click Event ----------------------------------
        homeViewModel.isAddPortalClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                // findNavController().navigate(R.id.action_homeFragment_to_walletReportFragment)
                homeViewModel.isAddPortalClicked.value = false
            }
        })

        homeViewModel.isInstituteProfileClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                Toast.makeText(context, "Coming Soon...", Toast.LENGTH_LONG).show()
                homeViewModel.isInstituteProfileClicked.value = false
            }
        })


        homeViewModel.isStudentProfileClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_studentProfileFragment)
                homeViewModel.isStudentProfileClicked.value = false
            }
        })

        homeViewModel.isStudentAttendanceClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_studentAttendanceFragment)
                homeViewModel.isStudentAttendanceClicked.value = false
            }
        })

        homeViewModel.isSubjectClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_subjectListFragment)
                homeViewModel.isSubjectClicked.value = false
            }
        })

        homeViewModel.isFeesClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_feesInfoFragment)
                homeViewModel.isFeesClicked.value = false
            }
        })

        homeViewModel.isInventoryClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_inventoryInfoFragment)
                homeViewModel.isInventoryClicked.value = false
            }
        })

        homeViewModel.isRoutineInfoClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_classRoutineFragment)
                homeViewModel.isRoutineInfoClicked.value = false
            }
        })

        homeViewModel.isExamInfoClicked.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_homeFragment_to_classTestFragment)
                homeViewModel.isExamInfoClicked.value = false
            }
        })
    }

    private fun setHomePageInfo(homePageInfo: HomePageInfo) {
        if (homePageInfo.getBasicInfo()?.getUserEnableStatus() == 1) {
//            binding.textViewActiveStatus.text = "Active"
        } else {
//            binding.textViewActiveStatus.text = "Inactive"
        }

//      binding.textViewUserName.text = homePageInfo.getBasicInfo()?.getBasicEmail()
        binding.textViewNetiId.text = homePageInfo.getBasicInfo()?.getCustomNetiID().toString()
        binding.textViewMobileNo.text = homePageInfo.getBasicInfo()?.getBasicMobile().toString()
        binding.textViewEmail.text = homePageInfo.getBasicInfo()?.getBasicEmail().toString()
        binding.textViewWalletBalance.text = AppUtilsClass.getDecimalFormattedValue(
            homePageInfo.getBasicInfo()?.getUserWalletBalance()!!
        )
        binding.textViewMessageBalance.text = AppUtilsClass.getDecimalFormattedValue(homePageInfo.getBasicInfo()?.getSmsBalance()!!)
//        binding.textViewPending.text = homePageInfo.getTokenSummary()?.getPending().toString()
//        binding.textViewRejected.text = homePageInfo.getTokenSummary()?.getRejected().toString()
//        binding.textViewSolved.text = homePageInfo.getTokenSummary()?.getSolved().toString()
//        binding.textViewTotal.text = homePageInfo.getTokenSummary()?.getSolved()
//            ?.plus(homePageInfo.getTokenSummary()?.getPending()!!)
//            ?.plus(homePageInfo.getTokenSummary()?.getRejected()!!)
//            .toString()
    }

    private fun convertToBitmap(b: ByteArray): Bitmap {
        return BitmapFactory.decodeByteArray(b, 0, b.size)
    }
}
