package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile

import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentAddTrainingInfoBinding
import com.netizen.netiworld.model.TagGetData
import com.netizen.netiworld.model.TrainingInfo
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.SearchSpinnerDialogFragment
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.ProfileViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class AddTrainingInfoFragment : Fragment() {

    private lateinit var binding: FragmentAddTrainingInfoBinding
    private lateinit var profileViewModel: ProfileViewModel

    private var trainingInfoId = 0
    private var title = "Add Training Info"
    private var isUpdate = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_add_training_info,
            container,
            false
        )

        if (!arguments?.getString("title").isNullOrEmpty()) {
            isUpdate = true
            title = arguments?.getString("title").toString()
            setData(arguments?.get("training_info") as TrainingInfo)
        }

        initViews()
        initObservers()
        profileViewModel.getCountry()
        profileViewModel.getPassingYears()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        profileViewModel = ViewModelProvider(
            activity!!,
            ProfileViewModel.ProfileViewModelFactory(context!!.applicationContext as Application)
        ).get(ProfileViewModel::class.java)
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", title)

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.editTextCountry.setOnClickListener {
            showSearchSpinnerDialog("profile", "country", 1)
        }

        binding.editTextYear.setOnClickListener {
            showSearchSpinnerDialog("profile", "training year", 4)
        }

        binding.buttonSave.setOnClickListener {
            if (isUpdate) {
                profileViewModel.checkUpdateTrainingInfo(
                    trainingInfoId,
                    binding.editTextTitle.text.toString(),
                    binding.editTextTopicCovered.text.toString(),
                    binding.editTextInstitute.text.toString(),
                    binding.editTextLocation.text.toString(),
                    binding.editTextDuration.text.toString()
                )
            } else {
                profileViewModel.checkTrainingInfo(
                    binding.editTextTitle.text.toString(),
                    binding.editTextTopicCovered.text.toString(),
                    binding.editTextInstitute.text.toString(),
                    binding.editTextLocation.text.toString(),
                    binding.editTextDuration.text.toString()
                )
            }
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                showToastyError(it)
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
                clearFields()
            }
        })

        Loaders.searchValue1.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                profileViewModel.getCountryId(it)
                binding.editTextCountry.setText(it)
            }
        })

        Loaders.searchValue4.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                profileViewModel.getPassingYearId(it)
                binding.editTextYear.setText(it)
            }
        })

        profileViewModel.isTrainingTitleEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Training title is empty!")
                profileViewModel.isTrainingTitleEmpty.value = false
            }
        })

        profileViewModel.isTopicEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Topic is empty!")
                profileViewModel.isTopicEmpty.value = false
            }
        })

        profileViewModel.isInstituteEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Institute name is empty!")
                profileViewModel.isInstituteEmpty.value = false
            }
        })

        profileViewModel.isLocationEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Location is empty!")
                profileViewModel.isLocationEmpty.value = false
            }
        })

        profileViewModel.isCountryEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Country is empty!")
                profileViewModel.isCountryEmpty.value = false
            }
        })

        profileViewModel.isPassingYearEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Training year is empty!")
                profileViewModel.isPassingYearEmpty.value = false
            }
        })

        profileViewModel.isDurationEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Duration is empty!")
                profileViewModel.isDurationEmpty.value = false
            }
        })
    }

    private fun showSearchSpinnerDialog(dataType: String, title: String, spinnerType: Int) {
        val bundle = Bundle()
        bundle.putString("dataType", dataType)
        bundle.putString("title", title)
        bundle.putInt("spinnerType", spinnerType)

        val searchSpinnerFragment = SearchSpinnerDialogFragment()
        searchSpinnerFragment.arguments = bundle
        searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
    }

    private fun setData(trainingInfo: TrainingInfo) {
        trainingInfoId = trainingInfo.getTrainingInfoId()!!

        binding.editTextTitle.setText(trainingInfo.getTrainingTitle())
        binding.editTextTopicCovered.setText(trainingInfo.getTopicCoveredDetails())
        binding.editTextInstitute.setText(trainingInfo.getInstituteName())
        binding.editTextLocation.setText(trainingInfo.getInstituteLocation())
        binding.editTextCountry.setText(trainingInfo.getCountryInfoDTO()?.getCategoryName())
        binding.editTextYear.setText(trainingInfo.getTrainingYearInfoDTO()?.getCategoryName())
        binding.editTextDuration.setText(trainingInfo.getTrainingDuration().toString())

        profileViewModel.getCountryId(trainingInfo.getCountryInfoDTO()?.getCategoryName())
        profileViewModel.getPassingYearId(trainingInfo.getTrainingYearInfoDTO()?.getCategoryName())
    }

    private fun clearFields() {
        binding.editTextTitle.text = null
        binding.editTextTopicCovered.text = null
        binding.editTextInstitute.text = null
        binding.editTextLocation.text = null
        binding.editTextCountry.text = null
        binding.editTextYear.text = null
        binding.editTextDuration.text = null
    }

    private fun showToastyError(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_LONG).show()
    }
}
