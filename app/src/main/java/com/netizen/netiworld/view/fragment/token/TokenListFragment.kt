package com.netizen.netiworld.view.fragment.token

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.myPoint.TokenListAdapter
import com.netizen.netiworld.databinding.FragmentTokenListBinding
import com.netizen.netiworld.model.TokenList
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.TokenViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class TokenListFragment : Fragment(), TokenListAdapter.OnItemClick {

    private lateinit var binding: FragmentTokenListBinding
    private lateinit var tokenViewModel: TokenViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_token_list,
            container,
            false
        )

        initViews()
        initObservers()
        tokenViewModel.getTokenList()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tokenViewModel = ViewModelProvider(
            activity!!,
            TokenViewModel.TokenViewModelFactory(context!!.applicationContext as Application)
        ).get(TokenViewModel::class.java)
    }

    override fun onItemClick(tokenList: TokenList) {
        tokenViewModel.token.value = tokenList
        TokenDetailsFragment().show(activity!!.supportFragmentManager, null)
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Token List")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                showErrorToasty(it)
                Loaders.apiError.value = null
            }
        })

        tokenViewModel.tokenList.observe(viewLifecycleOwner, Observer {
            binding.recyclerViewTokenList.layoutManager = LinearLayoutManager(context!!)
            binding.recyclerViewTokenList.setHasFixedSize(true)
            binding.recyclerViewTokenList.adapter =
                TokenListAdapter(
                    context!!,
                    it,
                    this
                )
        })
    }

    private fun showErrorToasty(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_LONG).show()
    }
}
