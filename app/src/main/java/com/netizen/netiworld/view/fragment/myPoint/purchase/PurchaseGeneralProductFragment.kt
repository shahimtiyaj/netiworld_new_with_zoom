package com.netizen.netiworld.view.fragment.myPoint.purchase

import android.app.Application
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentPurchaseGeneralProductBinding
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.utils.AppUtilsClass
import com.netizen.netiworld.view.fragment.SearchSpinnerDialogFragment
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.HomeViewModel
import com.netizen.netiworld.viewModel.MessageViewModel
import com.netizen.netiworld.viewModel.PurchaseViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class PurchaseGeneralProductFragment : Fragment() {

    private lateinit var binding: FragmentPurchaseGeneralProductBinding
    private lateinit var messageViewModel: MessageViewModel
    private lateinit var purchaseViewModel: PurchaseViewModel
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_purchase_general_product,
            container,
            false
        )

        purchaseViewModel.productQuantity.value = null

        initViews()
        initObservers()
        messageViewModel.getPurchasePointData()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel = ViewModelProvider(activity!!,
            HomeViewModel.HomeViewModelFactory(context!!.applicationContext as Application))
            .get(HomeViewModel::class.java)

        messageViewModel = ViewModelProvider(activity!!,
            MessageViewModel.MessageBalanceViewModelFactory(context!!.applicationContext as Application))
            .get(MessageViewModel::class.java)

        purchaseViewModel = ViewModelProvider(activity!!,
            PurchaseViewModel.PurchaseViewModelFactory(context!!.applicationContext as Application))
            .get(PurchaseViewModel::class.java)
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "General Product")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.editTextPurchasePoint.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("dataType", "message")
            bundle.putString("title", "purchase point")
            bundle.putInt("spinnerType", 1)

            val searchSpinnerFragment = SearchSpinnerDialogFragment()
            searchSpinnerFragment.arguments = bundle
            searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
        }

        binding.editTextProductName.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("dataType", "purchase")
            bundle.putString("title", "product")
            bundle.putInt("spinnerType", 2)

            val searchSpinnerFragment = SearchSpinnerDialogFragment()
            searchSpinnerFragment.arguments = bundle
            searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
        }

        binding.editTextProductQuantity.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (!s.isNullOrEmpty()) {
                    purchaseViewModel.totalCalculation(s.toString().toInt())
                } else {
                    purchaseViewModel.productQuantity.value = null
                    purchaseViewModel.totalCalculation(0)
                }
            }
        })

        binding.buttonSave.setOnClickListener {
            purchaseViewModel.checkPurchaseData()
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.success.value = null
                clearFields()
            }
        })

        Loaders.searchValue1.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.editTextProductName.text = null
                binding.editTextPurchasePoint.setText(it)
                messageViewModel.getPurchasePointId(it)
            }
        })

        Loaders.searchValue2.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.editTextProductName.setText(it)
                purchaseViewModel.getProduct(it)
            }
        })

        homeViewModel.homePageInfoLiveData.observe(viewLifecycleOwner, Observer {
//            binding.textViewWalletBalance.text = String.format(
//                "%.2f", it.getBasicInfo()?.getUserWalletBalance()
//            )
            if (it.getBasicInfo()?.getUserWalletBalance() != null) {
                binding.textViewWalletBalance.text = AppUtilsClass.getDecimalFormattedValue(
                    it.getBasicInfo()?.getUserWalletBalance()!!
                )
            }
        })

        messageViewModel.purchasePointId.observe(viewLifecycleOwner, Observer {
            purchaseViewModel.purchasePointId = it
            purchaseViewModel.getProductList(it)
        })

        purchaseViewModel.unitPrice.observe(viewLifecycleOwner, Observer {
            binding.editTextUnitPrice.setText("${AppUtilsClass.getDecimalFormattedValue(it.toDouble())} Tk")
        })

        purchaseViewModel.totalPrice.observe(viewLifecycleOwner, Observer {
            binding.editTextTotalPrice.setText("${AppUtilsClass.getDecimalFormattedValue(it)} Tk")
        })

        purchaseViewModel.vatAmount.observe(viewLifecycleOwner, Observer {
            binding.editTextVatAmount.setText("${AppUtilsClass.getDecimalFormattedValue(it)} Tk")
        })

        purchaseViewModel.payableAmount.observe(viewLifecycleOwner, Observer {
            binding.editTextPayableAmount.setText("${AppUtilsClass.getDecimalFormattedValue(it)} Tk")
        })
    }

    private fun clearFields() {
        binding.editTextPurchasePoint.text = null
        binding.editTextProductName.text = null
        binding.editTextProductQuantity.text = null
        binding.editTextUnitPrice.setText("0.0 Tk")
        binding.editTextTotalPrice.setText("0.0 Tk")
        binding.editTextVatAmount.setText("0.0 Tk")
        binding.editTextPayableAmount.setText("0.0 Tk")
    }
}
