package com.netizen.netiworld.view.fragment.token

import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentTokenDetailsBinding
import com.netizen.netiworld.model.TokenList
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.utils.AppUtilsClass
import com.netizen.netiworld.viewModel.TokenViewModel
import es.dmoral.toasty.Toasty
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass.
 */
class TokenDetailsFragment : DialogFragment() {

    private lateinit var binding: FragmentTokenDetailsBinding
    private lateinit var tokenViewModel: TokenViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_token_details,
            container,
            false
        )

        initViews()
        initObservers()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TransparentDialogTheme)

        tokenViewModel = ViewModelProvider(
            activity!!,
            TokenViewModel.TokenViewModelFactory(context!!.applicationContext as Application)
        ).get(TokenViewModel::class.java)
    }

    private fun initViews() {
        binding.textViewDownload.setOnClickListener {
            tokenViewModel.downloadImage()
        }
    }

    private fun initObservers() {
        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null

                GlobalScope.launch {
                    tokenViewModel.saveImage()
                }
            }
        })

        tokenViewModel.isDownloading.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        tokenViewModel.token.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                setData(it)
            }
        })
    }

    private fun setData(tokenList: TokenList) {
        tokenViewModel.downloadImageUrl = tokenList.getAttachmentPath()

        binding.textViewTokenId.text = tokenList.getCustomTokenID()
        binding.textViewSubmitDate.text = AppUtilsClass.getDate(tokenList.getCreateDate()?.toLong()!!)
        binding.textViewProblemType.text = tokenList.getTokenTypeInfoDTO()?.getCategoryName()
        binding.textViewProblemDetails.text = tokenList.getTokenDetails()
        binding.textViewContactNo.text = tokenList.getTokenContact()

        if (!tokenList.getSolveDate().isNullOrEmpty()) {
            binding.textViewSolvedDate.text = AppUtilsClass.getDate(tokenList.getSolveDate()!!.toLong())
        }

        when (tokenList.getTokenStatus()) {
            0 -> {
                binding.textViewStatus.text = "Pending"
            }
            10 -> {
                binding.textViewStatus.text = "Solved"
            }
            else -> {
                binding.textViewStatus.text = "N/A"
            }
        }
    }
 }
