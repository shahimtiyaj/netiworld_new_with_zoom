package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile

import android.app.Application
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentAddExperienceInfoBinding
import com.netizen.netiworld.model.ExperienceInfo
import com.netizen.netiworld.utils.AppUtilsClass
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.SearchSpinnerDialogFragment
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.ProfileViewModel
import es.dmoral.toasty.Toasty
import java.text.SimpleDateFormat
import java.util.*

class AddExperienceInfoFragment : Fragment() {
    private lateinit var binding: FragmentAddExperienceInfoBinding
    private lateinit var profileViewModel: ProfileViewModel
    private var calendar = Calendar.getInstance()
    private var dateSelection: Int? = null
    private var title = "Add Experience Info"
    private var isUpdate = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        profileViewModel = ViewModelProvider(
            activity!!,
            ProfileViewModel.ProfileViewModelFactory(context!!.applicationContext as Application)
        ).get(ProfileViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_add_experience_info,
            container,
            false
        )

        if (!arguments?.getString("title").isNullOrEmpty()) {
            isUpdate = true
            title = arguments?.getString("title").toString()
            setData(arguments?.get("experienceData") as ExperienceInfo)
        }

        initViews()

        initObservers()

        profileViewModel.getCountry()

        return binding.root
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Add Experience Info")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.editTextStartDate.setOnClickListener {
            dateSelection = 1
            showDatePicker()
        }

        binding.editTextEndDate.setOnClickListener {
            dateSelection = 2
            showDatePicker()
        }

        binding.editTextCountry.setOnClickListener {
            showSearchSpinnerDialog("profile", "country", 1)
        }

        binding.buttonSave.setOnClickListener {
            if (isUpdate) {
                profileViewModel.checkUpdateExperienceInfo(
                    binding.editTextCompanyName.text.toString(),
                    binding.editTextCompanyBusiness.text.toString(),
                    binding.editTextDesignation.text.toString(),
                    binding.editTextDepartment.text.toString(),
                    binding.editTextCompanyLocation.text.toString(),
                    binding.editTextStartDate.text.toString(),
                    binding.editTextEndDate.text.toString(),
                    binding.editTextResponsibilities.text.toString()
                )
            } else {
                profileViewModel.checkExperienceInfo(
                    binding.editTextCompanyName.text.toString(),
                    binding.editTextCompanyBusiness.text.toString(),
                    binding.editTextDesignation.text.toString(),
                    binding.editTextDepartment.text.toString(),
                    binding.editTextCompanyLocation.text.toString(),
                    binding.editTextStartDate.text.toString(),
                    binding.editTextEndDate.text.toString(),
                    binding.editTextResponsibilities.text.toString()
                )
            }
        }
    }

    private fun initObservers() {

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                showToastyError(it)
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
                clearFields()
            }
        })

        Loaders.searchValue1.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                profileViewModel.getCountryId(it)
                binding.editTextCountry.setText(it)
            }
        })

        profileViewModel.isCompanyNameEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Company Name can't left empty!")
            }
        })

        profileViewModel.isCompanyBusinessEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Company Business can't left empty!")
            }
        })

        profileViewModel.isDesignationNameEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Designation can't left empty!")
            }
        })

        profileViewModel.isWorkingDepartmentEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Department can't left empty!")
            }
        })

        profileViewModel.isCompanyLocationEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Company Location can't left empty!")
            }
        })

        profileViewModel.isEmploymentStartEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Start Date can't left empty!")
            }
        })

        profileViewModel.isEmploymentEndEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("End Date can't left empty!")
            }
        })

        profileViewModel.isResponsibilitiesDetailsEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Responsibilities Details can't left empty!")
            }
        })

        profileViewModel.isCountryEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Country can't left empty!")
            }
        })
    }

    private fun showSearchSpinnerDialog(dataType: String, title: String, spinnerType: Int) {
        val bundle = Bundle()
        bundle.putString("dataType", dataType)
        bundle.putString("title", title)
        bundle.putInt("spinnerType", spinnerType)

        val searchSpinnerFragment = SearchSpinnerDialogFragment()
        searchSpinnerFragment.arguments = bundle
        searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
    }

    private fun showDatePicker() {
        val datePickerDialog = DatePickerDialog(
            context!!, dateSetListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )

        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()
    }

    private val dateSetListener =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)

            when (dateSelection) {
                1 -> {
                    binding.editTextStartDate.setText(dateFormat.format(calendar.time))
                }
                2 -> {
                    binding.editTextEndDate.setText(dateFormat.format(calendar.time))
                }
            }
        }

    private fun clearFields() {
        binding.editTextCompanyName.text = null
        binding.editTextCompanyBusiness.text = null
        binding.editTextDesignation.text = null
        binding.editTextDepartment.text = null
        binding.editTextCompanyLocation.text = null
        binding.editTextStartDate.text = null
        binding.editTextEndDate.text = null
        binding.editTextResponsibilities.text = null
        binding.editTextCountry.text = null
    }

    private fun setData(experienceInfo: ExperienceInfo) {
        binding.editTextCompanyName.setText(experienceInfo.getCompanyName())
        binding.editTextCompanyBusiness.setText(experienceInfo.getCompanyBusiness())
        binding.editTextDesignation.setText(experienceInfo.getDesignationName())
        binding.editTextDepartment.setText(experienceInfo.getWorkingDepartment())
        binding.editTextCompanyLocation.setText(experienceInfo.getCompanyLocation())
        binding.editTextCountry.setText(experienceInfo.getCountryInfoDTO()?.getCategoryName())
        binding.editTextStartDate.setText(AppUtilsClass.getDate(experienceInfo.getEmploymentStart()))
        binding.editTextEndDate.setText(AppUtilsClass.getDate(experienceInfo.getEmploymentEnd()))
        binding.editTextResponsibilities.setText(experienceInfo.getResponsibilityDetails())
    }

    private fun showToastyError(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_LONG).show()
    }

    companion object
}
