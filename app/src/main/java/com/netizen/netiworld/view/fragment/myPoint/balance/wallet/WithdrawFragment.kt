package com.netizen.netiworld.view.fragment.myPoint.balance.wallet

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentWithdrawBinding
import com.netizen.netiworld.model.WithDrawInfo
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.viewModel.WalletViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class WithdrawFragment : Fragment() {

    private lateinit var binding: FragmentWithdrawBinding
    private lateinit var walletViewModel: WalletViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_withdraw,
            container,
            false
        )

        initViews()
        initObservers()
        walletViewModel.getWithdrawInfoList()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        walletViewModel = ViewModelProvider(activity!!,
            WalletViewModel.WalletViewModelFactory(context!!.applicationContext as Application))
            .get(WalletViewModel::class.java)
    }

    private fun initViews() {
        binding.buttonSave.setOnClickListener {
            walletViewModel.checkWithdrawData(binding.editTextWithdrawAmount.text.toString(),
                binding.editTextNote.text.toString())
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
                clearFields()
            }
        })

        walletViewModel.withDrawInfo.observe(viewLifecycleOwner, Observer {
            setWithdrawInfo(it)
        })
    }

    private fun setWithdrawInfo(withDrawInfo: WithDrawInfo?) {
        binding.editTextBankName.setText(withDrawInfo?.getUserBankAccountInfoDTO()
            ?.getCoreBankBranchInfoDTO()?.getCoreBankInfoDTO()?.getCategoryName())
        binding.editTextBranchName.setText(withDrawInfo?.getUserBankAccountInfoDTO()
            ?.getCoreBankBranchInfoDTO()?.getBranchName())
        binding.editTextAccountHolderName.setText(withDrawInfo?.getUserBankAccountInfoDTO()?.getBankAccHolderName())
        binding.editTextAccountNumber.setText(withDrawInfo?.getUserBankAccountInfoDTO()?.getBankAccNumber())
    }

    private fun clearFields() {
//        binding.editTextBankName.text = null
//        binding.editTextBranchName.text = null
//        binding.editTextAccountHolderName.text = null
//        binding.editTextAccountNumber.text = null
        binding.editTextWithdrawAmount.text = null
        binding.editTextNote.text = null
    }
}
