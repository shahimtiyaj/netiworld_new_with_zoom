package com.netizen.netiworld.view.fragment.myPoint.balance.wallet

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.myPoint.WalletPagerAdapter
import com.netizen.netiworld.databinding.FragmentWalletBinding
import com.netizen.netiworld.utils.AppUtilsClass
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.HomeViewModel

/**
 * A simple [Fragment] subclass.
 */
class WalletFragment : Fragment() {

    private lateinit var binding: FragmentWalletBinding
    private lateinit var homeViewModel: HomeViewModel

    private lateinit var viewpager: ViewPager
    private lateinit var adapter: WalletPagerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_wallet,
            container,
            false
        )

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        binding.viewPager.adapter = WalletPagerAdapter(childFragmentManager)
        binding.viewPager.offscreenPageLimit = 3
        binding.tabLayout.post { binding.tabLayout.setupWithViewPager(binding.viewPager) }

        initViews()
        initObservers()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel = ViewModelProvider(activity!!,
            HomeViewModel.HomeViewModelFactory(context!!.applicationContext as Application))
            .get(HomeViewModel::class.java)
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Wallet")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()
    }

    private fun initObservers() {
        homeViewModel.homePageInfoLiveData.observe(viewLifecycleOwner, Observer {
            if (it.getBasicInfo()?.getUserWalletBalance() != null) {
                binding.textViewWalletBalance.text = String.format("%s", AppUtilsClass.getDecimalFormattedValue(
                    it.getBasicInfo()?.getUserWalletBalance()!!
                )).plus("/-")
            }
        })
    }
}
