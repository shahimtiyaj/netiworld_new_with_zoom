package com.netizen.netiworld.view.fragment.token

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentSubmitTokenBinding
import com.netizen.netiworld.model.TokenList
import com.netizen.netiworld.utils.AppUtilsClass
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.SearchSpinnerDialogFragment
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.TokenViewModel
import es.dmoral.toasty.Toasty
import java.io.File
import java.io.FileOutputStream
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class SubmitTokenFragment : Fragment() {

    companion object {
        private const val IMAGE_PICK_CODE = 1000
        private const val PERMISSION_CODE_READ = 1001
        private const val PERMISSION_CODE_WRITE = 1002
        private const val PICK_FILE_RESULT_CODE = 1003
    }

    private lateinit var binding: FragmentSubmitTokenBinding
    private lateinit var tokenViewModel: TokenViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_submit_token,
            container,
            false
        )

        initViews()
        initObservers()
        tokenViewModel.getSolvedAndPendingTokenList()
        tokenViewModel.getProblemModuleList()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tokenViewModel = ViewModelProvider(
            activity!!,
            TokenViewModel.TokenViewModelFactory(context!!.applicationContext as Application)
        ).get(TokenViewModel::class.java)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
//            binding.imageViewUpload.setImageURI(data?.data)
            val bitmapImage = MediaStore.Images.Media.getBitmap(activity!!.contentResolver, data?.data)
//            image = bitMapToString(bitmapImage)

            val file = File("" + data?.data?.path)
//            binding.editTextChequeLeaf.setText(imageFile.name + file.extension)

//            Log.e("File", "${data?.data.toString()} --- ${file.name} --- ${file.path} --- ${file.absolutePath}")

            tokenViewModel.getFileName(context!!, data?.data!!)
            tokenViewModel.getRealPathFromURI(context!!, data.data!!)
//            bankAccountViewModel.encodeToBase64(file)
        }

        if (resultCode == Activity.RESULT_OK && requestCode == PICK_FILE_RESULT_CODE) {
            val uri: Uri? = data!!.data
            val src: String = uri?.path.toString()
            Log.e("PDF", src)
//            encodeImage(src)
//            getRealPath(uri!!)
        }
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Submit Token")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.textViewViewAll.setOnClickListener {
            findNavController().navigate(R.id.action_submitTokenFragment_to_tokenListFragment)
        }

        binding.editTextProblemModule.setOnClickListener {
            if (tokenViewModel.isProblemModuleListFound.value == true) {
                showSearchSpinner("token", "problem module", 1)
            } else {
                showErrorToasty("No data found!")
            }
        }

        binding.editTextProblemType.setOnClickListener {
            if (tokenViewModel.isProblemTypeListFound.value == true) {
                showSearchSpinner("token", "problem type", 2)
            } else {
                showErrorToasty("No data found!")
            }
        }

        binding.editTextAttachment.setOnClickListener {
            if (AppUtilsClass.checkPermissionForImage(activity!!)) {
                pickFilesFromMobileStorage()
//                getPdfFile()
            }
        }

        binding.buttonSubmit.setOnClickListener {
            tokenViewModel.validationSubmitToken(
                binding.editTextProblemDetails.text.toString(),
                binding.editTextMobileNumber.text.toString()
            )
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                showErrorToasty(it)
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
                clearFields()
                tokenViewModel.getSolvedAndPendingTokenList()
            }
        })

        Loaders.searchValue1.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.editTextProblemModule.setText(it)
                tokenViewModel.getProblemModuleId(it)
                binding.editTextProblemType.text = null
            }
        })

        Loaders.searchValue2.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.editTextProblemType.setText(it)
                tokenViewModel.getProblemTypeId(it)
            }
        })

        tokenViewModel.solvedAndPendingTokenList.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                setSolvedAndPendingToken(it)
            }
        })

        tokenViewModel.attachmentName.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.editTextAttachment.setText(it)
            }
        })

        tokenViewModel.isProblemModuleEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.editTextProblemModule.error = "Empty!"
                tokenViewModel.isProblemModuleEmpty.value = false
            }
        })

        tokenViewModel.isProblemTypeEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.editTextProblemType.error = "Empty!"
                tokenViewModel.isProblemTypeEmpty.value = false
            }
        })

        tokenViewModel.isProblemDetailsEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.editTextProblemDetails.error = "Empty!"
                tokenViewModel.isProblemDetailsEmpty.value = false
            }
        })

        tokenViewModel.isContactNumberEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.editTextMobileNumber.error = "Empty!"
                tokenViewModel.isContactNumberEmpty.value = false
            }
        })

        tokenViewModel.isAttachmentEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.editTextAttachment.error = "Empty!"
                tokenViewModel.isAttachmentEmpty.value = false
            }
        })
    }

    private fun setSolvedAndPendingToken(solvedAndPendingTokenList: List<TokenList>) {
        var solved = 0
        var pending = 0

        solvedAndPendingTokenList.forEach { token ->
            when {
                token.getTokenStatus() == 0 -> {
                    pending++
                }
                token.getTokenStatus() == 10 -> {
                    solved++
                }
            }
        }

        binding.textViewSolvedToken.text = solved.toString()
        binding.textViewPendingToken.text = pending.toString()
    }

    private fun showSearchSpinner(dataType: String, title: String, spinnerType: Int) {
        val bundle = Bundle()
        bundle.putString("dataType", dataType)
        bundle.putString("title", title)
        bundle.putInt("spinnerType", spinnerType)

        val searchSpinnerFragment = SearchSpinnerDialogFragment()
        searchSpinnerFragment.arguments = bundle
        searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
    }

    private fun pickFilesFromMobileStorage() {
//        val galleryIntent = Intent(Intent.ACTION_GET_CONTENT, null)
        val galleryIntent = Intent(Intent.ACTION_PICK, null)
        galleryIntent.type = "image/*"

        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        val pdfIntent = Intent(Intent.ACTION_GET_CONTENT)
        pdfIntent.type = "*/*"

        val chooser = Intent(Intent.ACTION_CHOOSER)
        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent)
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:")

//        val intentArray = arrayOf(cameraIntent, pdfIntent)
        val intentArray = arrayOf(cameraIntent)
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        startActivityForResult(chooser, IMAGE_PICK_CODE)
    }

    private fun getPdfFile() {
        var chooseFile = Intent(Intent.ACTION_GET_CONTENT)
        chooseFile.type = "*/*"
        chooseFile = Intent.createChooser(chooseFile, "Choose a file")
        startActivityForResult(chooseFile, PICK_FILE_RESULT_CODE)
    }

    private fun getRealPath(uri: Uri) {
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor = context!!.contentResolver.query(uri, filePathColumn, null, null, null)!!

        val columnindex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val file_path = cursor.getString(columnindex)
        Log.e(javaClass.name, "file_path$file_path")
        val fileUri = Uri.parse("file://$file_path")
        cursor.close()



//        val wholeID: String = DocumentsContract.getDocumentId(uri)
//
//        // Split at colon, use second item in the array
//        val id = wholeID.split(":").toTypedArray()[1]
//
//        val column = arrayOf(MediaStore.Images.Media.DATA)
//
//        // where id is equal to
//        val sel = MediaStore.Images.Media._ID + "=?"
//
//        val cursor: Cursor = context!!.contentResolver.query(
//            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//            column, sel, arrayOf(id), null
//        )!!
//
//        var filePath: String? = ""
//
//        val columnIndex = cursor.getColumnIndex(column[0])
//
//        if (cursor.moveToFirst()) {
//            filePath = cursor.getString(columnIndex)
//        }
//        cursor.close()
//
//        Log.e("Real Path", "$filePath")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun encodeImage(path: String) {
        val input_file: ByteArray = Files.readAllBytes(Paths.get(path))

        val encodedBytes: ByteArray = Base64.getEncoder().encode(input_file)
        val encodedString = String(encodedBytes)
        val decodedBytes: ByteArray = Base64.getDecoder().decode(encodedString.toByteArray())

        val fos = FileOutputStream(path)
        fos.write(decodedBytes)
        fos.flush()
        fos.close()
    }

    private fun clearFields() {
        binding.editTextProblemModule.text = null
        binding.editTextProblemType.text = null
        binding.editTextProblemDetails.text = null
        binding.editTextMobileNumber.text = null
        binding.editTextAttachment.text = null
        tokenViewModel.attachmentName.value = null
    }

    private fun showErrorToasty(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_LONG).show()
    }
}
