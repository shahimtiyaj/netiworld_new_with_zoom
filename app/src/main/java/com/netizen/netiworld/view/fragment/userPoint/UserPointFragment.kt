package com.netizen.netiworld.view.fragment.userPoint

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentUserPointBinding
import com.netizen.netiworld.utils.OnSwipeTouchListener
import com.netizen.netiworld.viewModel.HomeViewModel
import com.netizen.netiworld.viewModel.ProfileViewModel

class UserPointFragment : DialogFragment() {
    private lateinit var binding: FragmentUserPointBinding
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var profileViewModel: ProfileViewModel
    private var isSettingOpened = false
    private var isStudentPortalOpened = false
    private var isGuardianPortalOpened = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TransparentDialogTheme)

        homeViewModel = ViewModelProvider(
            activity!!,
            HomeViewModel.HomeViewModelFactory(context!!.applicationContext as Application)
        )
            .get(HomeViewModel::class.java)

        profileViewModel = ViewModelProvider(
            activity!!,
            ProfileViewModel.ProfileViewModelFactory(context!!.applicationContext as Application)
        )
            .get(ProfileViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user_point, container, false)

        initViews()

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog?.window?.attributes?.windowAnimations = R.style.HorizontalDialogAnimation
    }

    private fun initViews() {

        binding.layoutParentView.setOnTouchListener(object : OnSwipeTouchListener(context!!) {
            override fun onSwipeLeft() {
                super.onSwipeLeft()
                dismiss()
            }
        })

        binding.scrollMainView.setOnTouchListener(object : OnSwipeTouchListener(context!!) {
            override fun onSwipeLeft() {
                super.onSwipeLeft()
                dismiss()
            }
        })

        binding.textViewSetting.setOnClickListener {
            if (!isSettingOpened) {
                showSettingPoint()
            } else {
                hideSettingPoint()
            }

            hideStudentPortalPoint()
            hideGuardianPortalPoint()
        }

        binding.textViewStudentPortal.setOnClickListener {
            if (!isStudentPortalOpened) {
                showStudentPortalPoint()
            } else {
                hideStudentPortalPoint()
            }

            hideSettingPoint()
            hideGuardianPortalPoint()
        }

        binding.textViewGuardianPortal.setOnClickListener {
            if (!isGuardianPortalOpened) {
                showGuardianPortalPoint()
            } else {
                hideGuardianPortalPoint()
            }

            hideSettingPoint()
            hideStudentPortalPoint()
        }

        binding.textViewAddPortal.setOnClickListener {
            dismiss()
            homeViewModel.isAddPortalClicked.value = true
        }

        binding.textViewSInstituteProfile.setOnClickListener {
            dismiss()
            homeViewModel.isInstituteProfileClicked.value = true
        }

        binding.textViewProfile.setOnClickListener {
            dismiss()
            homeViewModel.isStudentProfileClicked.value = true
        }
        binding.textViewSAttendanceInfo.setOnClickListener {
            dismiss()
            homeViewModel.isStudentAttendanceClicked.value = true
        }

        binding.textViewSSubjectInfo.setOnClickListener {
            dismiss()
            homeViewModel.isSubjectClicked.value = true
        }


        binding.textViewSRountineInfo.setOnClickListener {
            dismiss()
            homeViewModel.isRoutineInfoClicked.value = true
        }

        binding.textViewSExamInfo.setOnClickListener {
            dismiss()
            homeViewModel.isExamInfoClicked.value = true
        }

        binding.textViewSFeesInfo.setOnClickListener {
            dismiss()
            homeViewModel.isFeesClicked.value = true
        }

        binding.textViewSInventoryInfo.setOnClickListener {
            dismiss()
            homeViewModel.isInventoryClicked.value = true
        }


    }

    private fun showSettingPoint() {
        isSettingOpened = true
        binding.textViewSetting.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewSetting.setCompoundDrawablesWithIntrinsicBounds(
            R.drawable.ic_home_white_24dp,
            0,
            R.drawable.ic_drop_up_icon,
            0
        )
        binding.textViewAddPortal.visibility = View.VISIBLE
    }

    private fun hideSettingPoint() {
        isSettingOpened = false
        binding.textViewSetting.setBackgroundResource(0)
        binding.textViewSetting.setCompoundDrawablesWithIntrinsicBounds(
            R.drawable.ic_home_white_24dp,
            0,
            R.drawable.ic_drop_down_icon,
            0
        )
        binding.textViewAddPortal.visibility = View.GONE
    }

    private fun showStudentPortalPoint() {
        isStudentPortalOpened = true

        binding.textViewStudentPortal.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewStudentPortal.setCompoundDrawablesWithIntrinsicBounds(
            R.drawable.ic_home_white_24dp,
            0,
            R.drawable.ic_drop_up_icon,
            0
        )
        binding.textViewSInstituteProfile.visibility = View.VISIBLE
        binding.textViewProfile.visibility = View.VISIBLE
        binding.textViewSAttendanceInfo.visibility = View.VISIBLE
        binding.textViewSSubjectInfo.visibility = View.VISIBLE
        binding.textViewSRountineInfo.visibility = View.VISIBLE
        binding.textViewSExamInfo.visibility = View.VISIBLE
        binding.textViewSFeesInfo.visibility = View.VISIBLE
        binding.textViewSInventoryInfo.visibility = View.VISIBLE
    }

    private fun hideStudentPortalPoint() {
        isStudentPortalOpened = false

        binding.textViewStudentPortal.setBackgroundResource(0)
        binding.textViewStudentPortal.setCompoundDrawablesWithIntrinsicBounds(
            R.drawable.ic_home_white_24dp,
            0,
            R.drawable.ic_drop_down_icon,
            0
        )
        binding.textViewSInstituteProfile.visibility = View.GONE
        binding.textViewProfile.visibility = View.GONE
        binding.textViewSAttendanceInfo.visibility = View.GONE
        binding.textViewSSubjectInfo.visibility = View.GONE
        binding.textViewSRountineInfo.visibility = View.GONE
        binding.textViewSExamInfo.visibility = View.GONE
        binding.textViewSFeesInfo.visibility = View.GONE
        binding.textViewSInventoryInfo.visibility = View.GONE
    }

    private fun showGuardianPortalPoint() {
        isGuardianPortalOpened = true

        binding.textViewGuardianPortal.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewGuardianPortal.setCompoundDrawablesWithIntrinsicBounds(
            R.drawable.ic_home_white_24dp,
            0,
            R.drawable.ic_drop_up_icon,
            0
        )
        binding.textViewGInstituteProfile.visibility = View.VISIBLE
        binding.textViewGProfile.visibility = View.VISIBLE
        binding.textViewGAttendanceInfo.visibility = View.VISIBLE
        binding.textViewGSubjectInfo.visibility = View.VISIBLE
        binding.textViewGRountineInfo.visibility = View.VISIBLE
        binding.textViewGExamInfo.visibility = View.VISIBLE
        binding.textViewGFeesInfo.visibility = View.VISIBLE
        binding.textViewGInventoryInfo.visibility = View.VISIBLE
    }

    private fun hideGuardianPortalPoint() {
        isGuardianPortalOpened = false

        binding.textViewGuardianPortal.setBackgroundResource(0)
        binding.textViewGuardianPortal.setCompoundDrawablesWithIntrinsicBounds(
            R.drawable.ic_home_white_24dp,
            0,
            R.drawable.ic_drop_down_icon,
            0
        )
        binding.textViewGInstituteProfile.visibility = View.GONE
        binding.textViewGProfile.visibility = View.GONE
        binding.textViewGAttendanceInfo.visibility = View.GONE
        binding.textViewGSubjectInfo.visibility = View.GONE
        binding.textViewGRountineInfo.visibility = View.GONE
        binding.textViewGExamInfo.visibility = View.GONE
        binding.textViewGFeesInfo.visibility = View.GONE
        binding.textViewGInventoryInfo.visibility = View.GONE
    }


    companion object
}