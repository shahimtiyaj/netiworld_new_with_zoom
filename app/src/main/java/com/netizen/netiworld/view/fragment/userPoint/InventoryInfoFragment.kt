package com.netizen.netiworld.view.fragment.userPoint

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.userPoint.InventoryAdapter
import com.netizen.netiworld.databinding.FragmentInventoryInfoBinding
import com.netizen.netiworld.model.UserPoint.StudentPortalProfile
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.SearchSpinnerDialogFragment
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.UserPoint.SPortalProfileViewModel
import es.dmoral.toasty.Toasty

class InventoryInfoFragment : Fragment() {
    private lateinit var binding: FragmentInventoryInfoBinding
    private lateinit var sPortalProfileViewModel: SPortalProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sPortalProfileViewModel = ViewModelProvider(
            activity!!,
            SPortalProfileViewModel.SPortalProfileViewModelFactory(context!!.applicationContext as Application)
        ).get(
            SPortalProfileViewModel::class.java
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_inventory_info, container, false)

        initViews()
        initObservers()

        sPortalProfileViewModel.getSPortalProfileInfo()
        sPortalProfileViewModel.getStudentPortalAtdYearData()
        sPortalProfileViewModel.getStudentPortalMonthData()

        return binding.root
    }

    private fun initViews() {

        val bundle = Bundle()
        bundle.putString("title", "Student Portal")
        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle
        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.layoutStudentInfo.instituteIdChange.setOnClickListener {
            InstituteChangeDialogFragment().show(activity!!.supportFragmentManager, null)
        }

        binding.editTextAcademicYear.setOnClickListener {
            showSearchSpinnerDialog("studentportal", "year", 1)
        }

        binding.editTextMonth.setOnClickListener {
            showSearchSpinnerDialog("studentportal", "month", 3)
        }

        binding.buttonSearch.setOnClickListener {
            sPortalProfileViewModel.getStudentPortalInventory(
                instituteId,
                studentId,  //student portal ->(for testing purpose use default studentId-105032050618)
                binding.editTextAcademicYear.text.toString(),
                binding.editTextMonth.text.toString()
            )
        }
    }

    private fun initObservers() {

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                showToastyError(it)
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
            }
        })

        sPortalProfileViewModel.sPortalProfileInfoLiveData.observe(
            viewLifecycleOwner,
            Observer { sPortalProfileInfo ->
                setSPortalProfileInfo(sPortalProfileInfo)
            })

        sPortalProfileViewModel.studentPortalInventoryDetailsInfo.observe(
            viewLifecycleOwner,
            Observer {
                binding.recyclerInventoryInfo.layoutManager = LinearLayoutManager(context!!)
                binding.recyclerInventoryInfo.setHasFixedSize(true)
                binding.recyclerInventoryInfo.adapter = InventoryAdapter(context!!, it)
            })

        Loaders.searchValue1.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.editTextAcademicYear.setText(it)
            }
        })

        Loaders.searchValue3.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.editTextMonth.setText(it)
            }
        })
    }

    private fun showSearchSpinnerDialog(dataType: String, title: String, spinnerType: Int) {
        val bundle = Bundle()
        bundle.putString("dataType", dataType)
        bundle.putString("title", title)
        bundle.putInt("spinnerType", spinnerType)

        val searchSpinnerFragment = SearchSpinnerDialogFragment()
        searchSpinnerFragment.arguments = bundle
        searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
    }

    private fun setSPortalProfileInfo(studentPortalProfile: StudentPortalProfile) {
        studentId = studentPortalProfile.getStudentId().toString()
        instituteId = studentPortalProfile.getInstituteId().toString()

        //Common Institute Info---------------------------------------------------------------------
        binding.layoutStudentInfo.textViewInstituteId.text = studentPortalProfile.getInstituteId()
        binding.layoutStudentInfo.textViewInstituteName.text =
            studentPortalProfile.getInstituteName()
        //Common Student Info-----------------------------------------------------------------------
        binding.layoutStudentInfo.textViewStudentId.text = studentPortalProfile.getStudentId()
        binding.layoutStudentInfo.textViewAcademicYear.text = studentPortalProfile.getAcademicYear()
        binding.layoutStudentInfo.textViewAcademicGroup.text = studentPortalProfile.getGroupName()
        binding.layoutStudentInfo.textViewAcademicClass.text = studentPortalProfile.getClassName()
        binding.layoutStudentInfo.textViewAcademicSection.text =
            studentPortalProfile.getSectionName()
        binding.layoutStudentInfo.textViewAcademicShift.text = studentPortalProfile.getShiftName()
        binding.layoutStudentInfo.textViewAcademicRoll.text =
            studentPortalProfile.getStudentRoll().toString()
    }

    private fun showToastyError(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_LONG).show()
    }

    companion object {
        var studentId = ""
        var instituteId = ""
    }
}