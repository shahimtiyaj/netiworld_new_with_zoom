package com.netizen.netiworld.view.fragment.myPoint.balance.message

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.myPoint.BalanceMessageListAdapter
import com.netizen.netiworld.databinding.FragmentMessageReportBinding
import com.netizen.netiworld.model.BalanceMessageGetData
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.MessageViewModel

class ReportMessageRechargeFragment : Fragment() {

    private lateinit var binding: FragmentMessageReportBinding
    private lateinit var messageViewModel: MessageViewModel
    private var reportList = ArrayList<BalanceMessageGetData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        messageViewModel = ViewModelProvider(
            activity!!,
            MessageViewModel.MessageBalanceViewModelFactory(context!!.applicationContext as Application)
        ).get(MessageViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_message_report, container, false)

        initViews()
        initObservers()

        return binding.root
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Message Log")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()
    }

    private fun initObservers() {
        messageViewModel.messageRechargeInfo.observe(viewLifecycleOwner, Observer { messageLogList ->
            if (!messageLogList.isNullOrEmpty()) {
                this.reportList= messageLogList as ArrayList<BalanceMessageGetData>
                setAdapter(messageLogList)
            }
        })
    }

    private fun setAdapter(messageLogList: List<BalanceMessageGetData?>?) {
        binding.recyclerViewRevenueReport.layoutManager = LinearLayoutManager(context!!)
        binding.recyclerViewRevenueReport.setHasFixedSize(true)
        binding.recyclerViewRevenueReport.adapter =
            BalanceMessageListAdapter(
                context!!,
                messageLogList!!
            )
    }
}
