package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile

import android.app.Application
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentAddProfessionalQualificationInfoBinding
import com.netizen.netiworld.model.CertificationInfo
import com.netizen.netiworld.utils.AppUtilsClass
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.SearchSpinnerDialogFragment
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.ProfileViewModel
import es.dmoral.toasty.Toasty
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class AddProfessionalQualificationInfoFragment : Fragment() {

    private lateinit var binding: FragmentAddProfessionalQualificationInfoBinding
    private lateinit var profileViewModel: ProfileViewModel

    private var calendar = Calendar.getInstance()
    private var dateInMilli: Long? = null
    private var title = "Add Certification Info"
    private var isUpdate = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_add_professional_qualification_info,
            container,
            false
        )
        if (!arguments?.getString("title").isNullOrEmpty()) {
            isUpdate = true
            title = arguments?.getString("title").toString()
            setData(arguments?.get("certificationData") as CertificationInfo)
        }

        initViews()
        initObservers()
        profileViewModel.getCountry()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        profileViewModel = ViewModelProvider(
            activity!!,
            ProfileViewModel.ProfileViewModelFactory(context!!.applicationContext as Application)
        ).get(ProfileViewModel::class.java)
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Add Certification Info")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.editTextCountry.setOnClickListener {
            showSearchSpinnerDialog("profile", "country", 1)
        }

        binding.editTextAchievementDate.setOnClickListener {
            showDatePicker()
        }

        binding.buttonSave.setOnClickListener {
            when {
                isUpdate -> profileViewModel.checkUpdateCertificationInfo(
                    binding.editTextCertificationName.text.toString(),
                    binding.editTextInstitute.text.toString(),
                    binding.editTextLocation.text.toString(),
                    dateInMilli.toString(),
                    binding.editTextDuration.text.toString()
                )
                else -> {
                    profileViewModel.checkCertificationInfo(
                        binding.editTextCertificationName.text.toString(),
                        binding.editTextInstitute.text.toString(),
                        binding.editTextLocation.text.toString(),
                        dateInMilli.toString(),
                        binding.editTextDuration.text.toString()
                    )
                }
            }
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                showToastyError(it)
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
                clearFields()
            }
        })

        Loaders.searchValue1.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                profileViewModel.getCountryId(it)
                binding.editTextCountry.setText(it)
            }
        })

        profileViewModel.isCertificationNameEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Certification name is empty!")
                profileViewModel.isCertificationNameEmpty.value = false
            }
        })

        profileViewModel.isInstituteEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Institute name is empty!")
                profileViewModel.isInstituteEmpty.value = false
            }
        })

        profileViewModel.isLocationEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Location is empty!")
                profileViewModel.isLocationEmpty.value = false
            }
        })

        profileViewModel.isCountryEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Country is empty!")
                profileViewModel.isCountryEmpty.value = false
            }
        })

        profileViewModel.isDateEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Date is empty!")
                profileViewModel.isDateEmpty.value = false
            }
        })

        profileViewModel.isDurationEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Duration is empty!")
                profileViewModel.isDurationEmpty.value = false
            }
        })
    }

    private fun showSearchSpinnerDialog(dataType: String, title: String, spinnerType: Int) {
        val bundle = Bundle()
        bundle.putString("dataType", dataType)
        bundle.putString("title", title)
        bundle.putInt("spinnerType", spinnerType)

        val searchSpinnerFragment = SearchSpinnerDialogFragment()
        searchSpinnerFragment.arguments = bundle
        searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
    }

    private fun showDatePicker() {
        val datePickerDialog = DatePickerDialog(context!!, dateSetListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH))

        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()
    }

    private val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, monthOfYear)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
//      val dateFormat = SimpleDateFormat(myFormat)

        dateInMilli = calendar.timeInMillis
        binding.editTextAchievementDate.setText(dateFormat.format(calendar.time))
    }

    private fun setData(certificationInfo: CertificationInfo) {
        binding.editTextCertificationName.setText(certificationInfo.getCertificationName())
        binding.editTextInstitute.setText(certificationInfo.getInstituteName())
        binding.editTextLocation.setText(certificationInfo.getInstituteLocation())
        binding.editTextCountry.setText(certificationInfo.getCountryInfoDTO()?.getCategoryName())
        binding.editTextAchievementDate.setText(AppUtilsClass.getDate(certificationInfo.getAchieveDate()))
        binding.editTextDuration.setText(certificationInfo.getCourseDuration())
    }

    private fun clearFields() {
        binding.editTextCertificationName.text = null
        binding.editTextAchievementDate.text = null
        binding.editTextInstitute.text = null
        binding.editTextLocation.text = null
        binding.editTextCountry.text = null
        binding.editTextDuration.text = null
    }

    private fun showToastyError(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_LONG).show()
    }
}
