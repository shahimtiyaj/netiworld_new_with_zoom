package com.netizen.netiworld.view.fragment.myPoint.balance.wallet

import android.app.Application
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentTransferReportSelectionBinding
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.viewModel.WalletViewModel
import es.dmoral.toasty.Toasty
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class TransferReportSelectionFragment : Fragment() {

    private lateinit var binding: FragmentTransferReportSelectionBinding
    private lateinit var walletViewModel: WalletViewModel

    private var calendar = Calendar.getInstance()
    private var dateSelection: Int? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_transfer_report_selection,
            container,
            false
        )

        initViews()
        initObservers()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        walletViewModel = ViewModelProvider(activity!!,
            WalletViewModel.WalletViewModelFactory(context!!.applicationContext as Application))
            .get(WalletViewModel::class.java)
    }

    private fun initViews() {
        binding.editTextFromDate.setOnClickListener {
            dateSelection = 1
            showDatePicker()
        }

        binding.editTextToDate.setOnClickListener {
            dateSelection = 2
            showDatePicker()
        }

        binding.buttonSearch.setOnClickListener {
            walletViewModel.checkBalanceTransferReportPostData(
                binding.editTextFromDate.text.toString(),
                binding.editTextToDate.text.toString()
            )
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        walletViewModel.isFromDateEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                Toasty.error(context!!, "From date is empty!", Toasty.LENGTH_LONG).show()
                walletViewModel.isFromDateEmpty.value = false
            }
        })

        walletViewModel.isToDateEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                Toasty.error(context!!, "To date is empty!", Toasty.LENGTH_LONG).show()
                walletViewModel.isToDateEmpty.value = false
            }
        })

        walletViewModel.isTransferDataFound.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_walletReportFragment_to_transferReportFragment)
                walletViewModel.isTransferDataFound.value = false
            }
        })
    }

    private fun showDatePicker() {
        val datePickerDialog = DatePickerDialog(context!!, dateSetListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH))

        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()
    }

    private val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, monthOfYear)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
//      val dateFormat = SimpleDateFormat(myFormat)

        when(dateSelection) {
            1 -> {
                binding.editTextFromDate.setText(dateFormat.format(calendar.time))
            }
            2 -> {
                binding.editTextToDate.setText(dateFormat.format(calendar.time))
            }
        }
    }
}
