package com.netizen.netiworld.view.fragment.myPoint.purchase

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.myPoint.PurchaseUnusedCodeListAdapter
import com.netizen.netiworld.databinding.FragmentUnusedPurchaseReportBinding
import com.netizen.netiworld.model.PurchaseCodeLogGetdata
import com.netizen.netiworld.viewModel.PurchaseViewModel


class UsedPurchaseReportFragment : Fragment() {
    private lateinit var binding: FragmentUnusedPurchaseReportBinding
    private lateinit var purchaseViewModel: PurchaseViewModel
    private var reportList = ArrayList<PurchaseCodeLogGetdata>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        purchaseViewModel = ViewModelProvider(
            activity!!,
            PurchaseViewModel.PurchaseViewModelFactory(context!!.applicationContext as Application)
        ).get(PurchaseViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_unused_purchase_report, container, false)

        initObservers()
        purchaseViewModel.getUnusedPurchaseCode(1)

        return binding.root
    }

    private fun initObservers() {
        purchaseViewModel.unusedPurchaseLogList.observe(viewLifecycleOwner, Observer { unusedpurchaseCodeLogList ->
            if (!unusedpurchaseCodeLogList.isNullOrEmpty()) {
                this.reportList= unusedpurchaseCodeLogList as ArrayList<PurchaseCodeLogGetdata>
                setAdapter(unusedpurchaseCodeLogList)
            }
        })
    }

    private fun setAdapter(unusedpurchaseCodeLogList: List<PurchaseCodeLogGetdata?>?) {
        binding.recyclerViewRevenueReport.layoutManager = LinearLayoutManager(context!!)
        binding.recyclerViewRevenueReport.setHasFixedSize(true)
        binding.recyclerViewRevenueReport.adapter =
            PurchaseUnusedCodeListAdapter(
                context!!,
                unusedpurchaseCodeLogList!!
            )
    }
}
