package com.netizen.netiworld.view.fragment.userPoint

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentExamRoutineBinding
import com.netizen.netiworld.view.fragment.ToolBarFragment

class ExamRoutineFragment : Fragment() {
    private lateinit var binding: FragmentExamRoutineBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_exam_routine, container, false)
        initViews()
        return binding.root
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Student Portal")
        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle
        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.layoutStudentInfo.instituteIdChange.setOnClickListener {
            InstituteChangeDialogFragment().show(activity!!.supportFragmentManager, null)
        }
        binding.buttonClassTest.setOnClickListener {
            findNavController().navigate(R.id.action_examRoutineFragment_to_classRoutineFragment)
        }
    }

    companion object
}