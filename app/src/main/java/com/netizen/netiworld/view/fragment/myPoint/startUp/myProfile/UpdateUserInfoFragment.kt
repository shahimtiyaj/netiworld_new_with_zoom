package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentUpdateUserInfoBinding
import com.netizen.netiworld.view.fragment.ToolBarFragment

/**
 * A simple [Fragment] subclass.
 */
class UpdateUserInfoFragment : Fragment() {

    private lateinit var binding: FragmentUpdateUserInfoBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_update_user_info,
            container,
            false
        )

        initViews()
        initObservers()

        return binding.root
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Update User Info")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()
    }

    private fun initObservers() {

    }
}
