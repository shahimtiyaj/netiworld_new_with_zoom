package com.netizen.netiworld.view.fragment.myPoint.Balance.Wallet

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.BalanceTransferSucessfullLayoutBinding
import com.netizen.netiworld.viewModel.HomeViewModel

class TransferBalanceSuccessDialogFragment() : DialogFragment() {
    private lateinit var binding: BalanceTransferSucessfullLayoutBinding
    private var transferAmount: String? = null
    private var walletBalance: String? = null
    private var name: String? = null
    private var nameNetiId: String? = null
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TransparentDialogTheme)
        homeViewModel = ViewModelProvider(activity!!, HomeViewModel.HomeViewModelFactory(context!!.applicationContext as Application)).get(HomeViewModel::class.java)

        transferAmount = arguments!!.getString("transAmt")
        nameNetiId = arguments!!.getString("nameNetiID")
        walletBalance = arguments!!.getString("walletBalance")
        name = arguments!!.getString("name")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding= DataBindingUtil.inflate(inflater, R.layout.balance_transfer_sucessfull_layout, container, false)

        initObservables()
        homeViewModel.getHomePageInfo()

        binding.nameNetiId.text = name+" (ID: "+nameNetiId+" )"
        binding.transBlnc.text = "Your "+transferAmount+" Taka Successfully Transfer To"

        binding.gotoLogin.setOnClickListener {
            dismiss()
            findNavController().navigate(R.id.homeFragment)
        }

        return binding.root
    }

    private fun initObservables() {
        homeViewModel.homePageInfoLiveData.observe(viewLifecycleOwner, Observer { homePageInfo ->
            binding.walletBlc.text = String.format("%, .2f",homePageInfo.getBasicInfo()?.getUserWalletBalance())
        })
    }
}
