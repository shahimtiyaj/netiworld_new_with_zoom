package com.netizen.netiworld.view.fragment.userPoint

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentStudentProfileBinding
import com.netizen.netiworld.model.UserPoint.StudentPortalProfile
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.UserPoint.SPortalProfileViewModel
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.layout_student_portal_profile.view.*

class StudentProfileFragment : Fragment() {
    private lateinit var binding: FragmentStudentProfileBinding
    private lateinit var sPortalProfileViewModel: SPortalProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sPortalProfileViewModel = ViewModelProvider(
            activity!!,
            SPortalProfileViewModel.SPortalProfileViewModelFactory(context!!.applicationContext as Application)
        )
            .get(SPortalProfileViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_student_profile, container, false)
        initViews()
        initObservers()
        sPortalProfileViewModel.getSPortalProfileInfo()

        return binding.root
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Student Portal")
        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle
        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.layoutStudentInfo.instituteIdChange.setOnClickListener {
            InstituteChangeDialogFragment().show(activity!!.supportFragmentManager, null)
        }
    }

    private fun initObservers() {

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        sPortalProfileViewModel.sPortalProfileInfoLiveData.observe(
            viewLifecycleOwner,
            Observer { sPortalProfileInfo ->
                setSPortalProfileInfo(sPortalProfileInfo)
            })
    }

    private fun setSPortalProfileInfo(studentPortalProfile: StudentPortalProfile) {
        //Common Institute Info
        binding.layoutStudentInfo.textViewInstituteId.text = studentPortalProfile.getInstituteId()
        binding.layoutStudentInfo.textViewInstituteName.text =
            studentPortalProfile.getInstituteName()

        //Common Student Info
        binding.layoutStudentInfo.textViewStudentId.text = studentPortalProfile.getStudentId()
        binding.layoutStudentInfo.textViewAcademicYear.text = studentPortalProfile.getAcademicYear()
        binding.layoutStudentInfo.textViewAcademicGroup.text = studentPortalProfile.getGroupName()
        binding.layoutStudentInfo.textViewAcademicClass.text = studentPortalProfile.getClassName()
        binding.layoutStudentInfo.textViewAcademicSection.text =
            studentPortalProfile.getSectionName()
        binding.layoutStudentInfo.textViewAcademicShift.text = studentPortalProfile.getShiftName()
        binding.layoutStudentInfo.textViewAcademicRoll.text =
            studentPortalProfile.getStudentRoll().toString()

        //Basic Info
        binding.layoutInstituteInfo.text_view_gender.text = studentPortalProfile.getStudentGender()
        binding.layoutInstituteInfo.text_view_religion.text =
            studentPortalProfile.getStudentReligion()
        binding.layoutInstituteInfo.text_view_date_of_birth.text =
            studentPortalProfile.getStudentDOB()
        binding.layoutInstituteInfo.text_view_mobile_no.text =
            studentPortalProfile.getStudentMobile()
        binding.layoutInstituteInfo.text_view_email.text = studentPortalProfile.getStudentEmail()
        binding.layoutInstituteInfo.text_view_birth_certification.text =
            studentPortalProfile.getBirthCertificateNo().toString()
        //Parent Info
        binding.layoutInstituteInfo.text_view_father_name.text =
            studentPortalProfile.getFatherName()
        binding.layoutInstituteInfo.text_view_mother_name.text =
            studentPortalProfile.getMotherName()
        binding.layoutInstituteInfo.text_view_guardian_mobile_no.text =
            studentPortalProfile.getGuardianMobile()
        binding.layoutInstituteInfo.text_view_roll_no.text =
            studentPortalProfile.getStudentRoll().toString()
        binding.layoutInstituteInfo.text_view_mothers_nid.text =
            studentPortalProfile.getMothersNID()
        binding.layoutInstituteInfo.text_view_guardian_email.text =
            studentPortalProfile.getGuardianEmail()
        //Address Info
        binding.layoutInstituteInfo.text_view_present_address.text =
            studentPortalProfile.getPresentHouseNo() + "," + studentPortalProfile.getPresentRoadNo() + "," + studentPortalProfile.getPresentVillage() + "," + studentPortalProfile.getPresentPostOffice() + ", " + studentPortalProfile.getPresentPostalCode() + "," + studentPortalProfile.getPresentDistrict() + "," + studentPortalProfile.getCountry()
        binding.layoutInstituteInfo.text_view_permanent_address.text =
            studentPortalProfile.getPresentHouseNo() + "," + studentPortalProfile.getPresentRoadNo() + "," + studentPortalProfile.getPresentVillage() + "," + studentPortalProfile.getPresentPostOffice() + ", " + studentPortalProfile.getPresentPostalCode() + "," + studentPortalProfile.getPresentDistrict() + "," + studentPortalProfile.getCountry()

        //Previous Institute Info
        binding.layoutInstituteInfo.text_view_institute_name.text = ""
        binding.layoutInstituteInfo.text_view_address.text = ""
        binding.layoutInstituteInfo.text_view_contact_no.text = ""
        binding.layoutInstituteInfo.text_view_email_institute.text = ""
        binding.layoutInstituteInfo.text_view_time_period.text =
            studentPortalProfile.getTimePeriod()
        binding.layoutInstituteInfo.text_view_last_education.text =
            studentPortalProfile.getLastEducation()
    }

    companion object
}