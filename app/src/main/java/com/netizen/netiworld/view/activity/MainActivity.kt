package com.netizen.netiworld.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.ActivityMainBinding
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.LogOutDialogFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController

    companion object {

        private lateinit var bottomNavigation: BottomNavigationView

        fun showBottomNavigation() {
            bottomNavigation.visibility = View.VISIBLE
        }

        fun hideBottomNavigation() {
            bottomNavigation.visibility = View.GONE
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AppTheme)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        navController = Navigation.findNavController(this@MainActivity, R.id.nav_host_fragment)

        bottomNavigation = binding.bottomNavigation

        initViews()
        initObservers()
    }

    private fun initViews() {
        bottomNavigation.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.navigation_home -> {
                    navController.popBackStack(R.id.homeFragment, true) // It clears the stack until this fragment
                    navController.navigate(R.id.homeFragment)
                }
                R.id.navigation_log_out -> {
                    LogOutDialogFragment().show(supportFragmentManager, null)
                }
            }

            return@setOnNavigationItemSelectedListener true
        }
    }

    private fun initObservers() {
        Loaders.isToolbarSettingClicked.observe(this, Observer {
            if (it) {
                navController.navigate(R.id.fragmentSetting)
                Loaders.isToolbarSettingClicked.value = false
            }
        })

        Loaders.isLogOut.observe(this, Observer {
            if (it) {
                navController.popBackStack(R.id.homeFragment, true) // It clears the stack until this fragment
                navController.navigate(R.id.signInFragment)
                Loaders.isLogOut.value = false
            }
        })
    }

    override fun onBackPressed() {
        when (navController.currentDestination?.id) {
            R.id.homeFragment -> {
                LogOutDialogFragment().show(supportFragmentManager, null)
            }

//            R.id.signInFragment -> {
//                finish()
//            }
//
//            R.id.fragmentSignUpOne -> {
//                navController.navigate(R.id.signInFragment)
//            }
//
//            R.id.fragmentSignUpTwo -> {
//                navController.navigate(R.id.fragmentSignUpOne)
//            }
//
//
//            R.id.fragmentForgotPasswordOne -> {
//                navController.navigate(R.id.signInFragment)
//            }
//
//            R.id.fragmentForgotPasswordTwo -> {
//                navController.navigate(R.id.fragmentForgotPasswordOne)
//            }
//
//            R.id.fragmentForgotPasswordThree -> {
//                navController.navigate(R.id.fragmentForgotPasswordTwo)
//            }
//
//            R.id.fragmentForgotPasswordFour -> {
//                navController.navigate(R.id.fragmentForgotPasswordOne)
//            }

            else -> {
                super.onBackPressed()
            }
        }
    }
}
