package com.netizen.netiworld.view.fragment.myPoint.balance.wallet

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.myPoint.BalanceStatementListAdapter
import com.netizen.netiworld.databinding.FragmentBalanceStatementReportBinding
import com.netizen.netiworld.model.BalanceStatementGetData
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.WalletViewModel

class BalanceStatementReportFragment : Fragment() {

    private lateinit var binding: FragmentBalanceStatementReportBinding
    private lateinit var walletViewModel: WalletViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        walletViewModel = ViewModelProvider(
            activity!!,
            WalletViewModel.WalletViewModelFactory(context!!.applicationContext as Application)
        )
            .get(WalletViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_balance_statement_report,
            container,
            false
        )

        initViews()
        initObservers()

        return binding.root
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Statement Report")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()
    }

    private fun initObservers() {
        walletViewModel.balanceStatementArrayList.observe(viewLifecycleOwner, Observer { statementLogList ->
            if (!statementLogList.isNullOrEmpty()) {
                setAdapter(statementLogList)
                binding.textViewFound.text = "Found: " + statementLogList.size.toString()
            }
        })
    }

    private fun setAdapter(statementLogList: List<BalanceStatementGetData?>?) {
        binding.recyclerViewRevenueReport.layoutManager = LinearLayoutManager(context!!)
        binding.recyclerViewRevenueReport.setHasFixedSize(true)
        binding.recyclerViewRevenueReport.adapter =
            BalanceStatementListAdapter(
                context!!,
                statementLogList
            )
    }
}


