package com.netizen.netiworld.view.fragment.myPoint.bankAccount

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentTagDialogBinding
import com.netizen.netiworld.model.TagType
import com.netizen.netiworld.viewModel.BankAccountViewModel


/**
 * A simple [Fragment] subclass.
 */
class TagDialogFragment : DialogFragment() {

    private lateinit var binding: FragmentTagDialogBinding
    private lateinit var bankAccountViewModel: BankAccountViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_tag_dialog,
            container,
            false
        )

        initViews()
        initObservers()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TransparentDialogTheme)

        bankAccountViewModel = ViewModelProvider(
            activity!!,
            BankAccountViewModel.BankAccountViewModelFactory(context!!.applicationContext as Application)
        ).get(BankAccountViewModel::class.java)
    }

    private fun initViews() {
        binding.imageViewClose.setOnClickListener {
            dismiss()
        }

        binding.spinnerTagType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    bankAccountViewModel.getCategoryDefaultCode(parent?.getItemAtPosition(position).toString())
                }
            }
        }

        binding.buttonSave.setOnClickListener {
            bankAccountViewModel.tagBankAccount()
            dismiss()
        }
    }

    private fun initObservers() {
        bankAccountViewModel.tempTagTypeList.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                setSpinnerAdapter(it)
            }
        })
    }

    private fun setSpinnerAdapter(tagTypeList: ArrayList<String>) {
        val adapter = ArrayAdapter(
            context!!,
            android.R.layout.simple_list_item_1,
            tagTypeList
        )
        binding.spinnerTagType.adapter = adapter
    }
}
