package com.netizen.netiworld.view.fragment.myPoint.balance.wallet

import android.app.Application
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentTransferBinding
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.myPoint.Balance.Wallet.TransferBalanceSuccessDialogFragment
import com.netizen.netiworld.viewModel.HomeViewModel
import com.netizen.netiworld.viewModel.WalletViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class TransferFragment : Fragment() {

    private lateinit var binding: FragmentTransferBinding
    private lateinit var walletViewModel: WalletViewModel
    private lateinit var homeViewModel: HomeViewModel

    private var netiMainID: String? = null
    private var customNetiID: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel = ViewModelProvider(
            activity!!,
            HomeViewModel.HomeViewModelFactory(context!!.applicationContext as Application)
        ).get(HomeViewModel::class.java)

        walletViewModel = ViewModelProvider(
            activity!!,
            WalletViewModel.WalletViewModelFactory(context!!.applicationContext as Application)
        )
            .get(WalletViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_transfer, container, false)

        initViews()

        initObservers()

        return binding.root
    }

    private fun initViews() {
        binding.etNetiId.addTextChangedListener(MyTextWatcher(binding.etNetiId))

        binding.btnSerachTrnsferNetiId.setOnClickListener {
            walletViewModel.checkTransferNetimanInfo(binding.etNetiId.text.toString())
        }

        binding.transferBtn.setOnClickListener {
            walletViewModel.checkForRequestOTP(
                binding.editTextTransferAmount.text.toString(),
                netiMainID, binding.transferNote.text.toString(),
                binding.etNetiId.text.toString(), customNetiID
            )
        }

        binding.otpLayout.button.setOnClickListener {
            if (binding.otpLayout.button.text == "Verify") {
                binding.otpLayout.timeCount?.text = " "
                walletViewModel.checkForOTPVarification(binding.otpLayout.pinView.text.toString())
            } else if (binding.otpLayout.button.text == "Transfer") {
                binding.otpLayout.timeCount?.text = " "
                walletViewModel.checkTransferWalletBalance(
                    binding.editTextTransferAmount.text.toString(),
                    netiMainID,
                    binding.transferNote.text.toString()
                )
            }
        }

        binding.otpLayout.resendOtp.setOnClickListener {
            walletViewModel.checkForRequestOTP(
                binding.editTextTransferAmount.text.toString(),
                netiMainID, binding.transferNote.text.toString(),
                binding.etNetiId.text.toString(), customNetiID
            )
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.success.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiSuccessforOTP.value = null
            }
        })

        Loaders.apiSuccessforOTP.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiSuccessforOTP.value = null
                binding.mmainLayoutId.visibility = View.GONE
                binding.otpLayout.otp.visibility = View.VISIBLE
                binding.otpLayout.topText.visibility = View.VISIBLE

                timer()

                binding.layoutMobile.visibility = View.VISIBLE
                val chunks = mobile.chunked(3)
                val c1 = chunks[1]
                val c3 = chunks[3]
                val phoneNo = "01" + c1.substring(0, 2) + "*****" + c3
               // binding.otpLayout.textView_noti.text = "A 6-digit verification code has been sent to ' $phoneNo ' contact number."
                binding.otpLayout.topText.text = "To verify your mobile number ' $phoneNo ' Please enter the OTP you have just received."
            }
        })

        Loaders.apiSuccessforOTPVarify.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {

                binding.otpLayout.pinView?.setLineColor(Color.GREEN)
                binding.otpLayout.topText.text = it
                binding.otpLayout.topText.setTextColor(Color.GREEN)
                binding.otpLayout.button.text = "Transfer"

                Toasty.success(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiSuccessforOTPVarify.value = null
            }
        })

        Loaders.apiErrorOTPInvalid.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.otpLayout.pinView?.setLineColor(Color.RED)
                binding.otpLayout.topText.text = it
                binding.otpLayout.topText.setTextColor(Color.RED)

                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiErrorOTPInvalid.value = null
            }
        })

        Loaders.apiSuccessBalanceTransfer.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiSuccessBalanceTransfer.value = null

                /*binding.mmainLayoutId.visibility=View.GONE
                binding.otpLayout.otp.visibility = View.GONE
                binding.layoutName.visibility = View.GONE
                binding.layoutMobile.visibility = View.GONE*/

                val bundle = Bundle()
                bundle.putString("transAmt", binding.editTextTransferAmount.text.toString())
                bundle.putString("nameNetiID", binding.etNetiId.text.toString())
                bundle.putString("name", name)
                val regFragment = TransferBalanceSuccessDialogFragment()
                regFragment.arguments = bundle
                regFragment.show(activity!!.supportFragmentManager, null)
            }
        })

        walletViewModel?.fullName?.observe(viewLifecycleOwner, Observer { fullName ->
            if (!fullName.isNullOrEmpty()) {
                binding.layoutName.visibility = View.VISIBLE
                binding.etName.text = fullName
                name = fullName
            }
        })

        walletViewModel?.PhoneNo?.observe(viewLifecycleOwner, Observer { PhoneNo ->
            if (!PhoneNo.isNullOrEmpty()) {
                binding.layoutMobile.visibility = View.VISIBLE
                val chunks = PhoneNo.chunked(3)
                val c1 = chunks[1]
                val c3 = chunks[3]
                val phoneNo = "01" + c1.substring(0, 2) + "*****" + c3
                binding.etMobileNo.text = phoneNo

                binding.transferBtn?.isEnabled = true
                binding.transferBtn?.setBackgroundResource(R.drawable.rec_gradient)
            }
        })

        walletViewModel?.netiMainID?.observe(viewLifecycleOwner, Observer { netiMainID ->
            if (!netiMainID.isNullOrEmpty()) {
                this.netiMainID = netiMainID
            }
        })

        walletViewModel?.customNetiId?.observe(viewLifecycleOwner, Observer { customNetiID ->
            if (!customNetiID.isNullOrEmpty()) {
                this.customNetiID = customNetiID
            }
        })

        homeViewModel.homePageInfoLiveData.observe(viewLifecycleOwner, Observer { homePageInfo ->
            mobile = homePageInfo.getBasicInfo()?.getBasicMobile()!!
        })
    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            binding.transferBtn.isEnabled = false
            binding.transferBtn.setBackgroundColor(Color.GRAY)// From android.graphics.Color
            binding.transferBtn.setBackgroundResource(R.drawable.disable_color)
        }

        override fun afterTextChanged(editable: Editable) {

        }
    }

    fun timer() {
        val timer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.otpLayout.timeCount?.text = "OTP expired on after " + millisUntilFinished / 1000 + " seconds"
                binding.otpLayout.resendOtp?.visibility = View.GONE
                binding.otpLayout.allReadyHaveAPassword.visibility = View.GONE
            }

            override fun onFinish() {
                binding.otpLayout.timeCount?.text = "(0)"
                binding.otpLayout.resendOtp?.visibility = View.VISIBLE
                binding.otpLayout.topText.visibility = View.GONE
                binding.otpLayout.allReadyHaveAPassword.visibility = View.VISIBLE
            }
        }
        timer.start()
    }

    companion object {
        var name = ""
        var mobile = ""
    }
}
