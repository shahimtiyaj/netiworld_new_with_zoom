package com.netizen.netiworld.view.fragment.userPoint

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.userPoint.SubjectInfoAdapter
import com.netizen.netiworld.databinding.FragmentSubjectListBinding
import com.netizen.netiworld.model.UserPoint.StudentPortalProfile
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.UserPoint.SPortalProfileViewModel
import es.dmoral.toasty.Toasty

class SubjectListFragment : Fragment() {
    private lateinit var binding: FragmentSubjectListBinding
    private lateinit var sPortalProfileViewModel: SPortalProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sPortalProfileViewModel = ViewModelProvider(
            activity!!,
            SPortalProfileViewModel.SPortalProfileViewModelFactory(context!!.applicationContext as Application)
        ).get(SPortalProfileViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_subject_list, container, false)

        initViews()
        initObservers()

        sPortalProfileViewModel.getSPortalProfileInfo()
        sPortalProfileViewModel.getSPortalSubjectInfo()

        return binding.root
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Student Portal")
        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle
        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.layoutStudentInfo.instituteIdChange.setOnClickListener {
            InstituteChangeDialogFragment().show(activity!!.supportFragmentManager, null)
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                showErrorToasty(it)
                Loaders.apiError.value = null
            }
        })

        sPortalProfileViewModel.sPortalProfileInfoLiveData.observe(
            viewLifecycleOwner,
            Observer { sPortalProfileInfo -> setSPortalProfileInfo(sPortalProfileInfo) })
        sPortalProfileViewModel.sProfileSubjectInfoAList.observe(viewLifecycleOwner, Observer {
            binding.recyclerSubjectInfo.layoutManager = LinearLayoutManager(context!!)
            binding.recyclerSubjectInfo.setHasFixedSize(true)
            binding.recyclerSubjectInfo.adapter = SubjectInfoAdapter(context!!, it)
        })
    }

    private fun setSPortalProfileInfo(studentPortalProfile: StudentPortalProfile) {
        //Common Institute Info
        binding.layoutStudentInfo.textViewInstituteId.text = studentPortalProfile.getInstituteId()
        binding.layoutStudentInfo.textViewInstituteName.text =
            studentPortalProfile.getInstituteName()
        //Common Student Info-----------------------------------------------------------------------
        binding.layoutStudentInfo.textViewStudentId.text = studentPortalProfile.getStudentId()
        binding.layoutStudentInfo.textViewAcademicYear.text = studentPortalProfile.getAcademicYear()
        binding.layoutStudentInfo.textViewAcademicGroup.text = studentPortalProfile.getGroupName()
        binding.layoutStudentInfo.textViewAcademicClass.text = studentPortalProfile.getClassName()
        binding.layoutStudentInfo.textViewAcademicSection.text =
            studentPortalProfile.getSectionName()
        binding.layoutStudentInfo.textViewAcademicShift.text = studentPortalProfile.getShiftName()
        binding.layoutStudentInfo.textViewAcademicRoll.text =
            studentPortalProfile.getStudentRoll().toString()
    }

    private fun showErrorToasty(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_LONG).show()
    }

    companion object
}