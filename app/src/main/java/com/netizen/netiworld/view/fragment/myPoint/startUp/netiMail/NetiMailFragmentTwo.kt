package com.netizen.netiworld.view.fragment.myPoint.startUp.netiMail

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentNetiMailTwoBinding
import com.netizen.netiworld.utils.AppUtilsClass
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.view.fragment.myPoint.startUp.netiMail.NetiMailFragmentOne.Companion.name
import com.netizen.netiworld.view.fragment.myPoint.startUp.netiMail.NetiMailFragmentOne.Companion.netMainID
import com.netizen.netiworld.view.fragment.myPoint.startUp.netiMail.NetiMailFragmentOne.Companion.phone
import com.netizen.netiworld.viewModel.BankAccountViewModel
import es.dmoral.toasty.Toasty
import java.io.File

class NetiMailFragmentTwo : Fragment() {
    private lateinit var binding: FragmentNetiMailTwoBinding
    private lateinit var bankAccountViewModel: BankAccountViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bankAccountViewModel = ViewModelProvider(
            activity!!,
            BankAccountViewModel.BankAccountViewModelFactory(context!!.applicationContext as Application)
        ).get(BankAccountViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding=DataBindingUtil.inflate(inflater, R.layout.fragment_neti_mail_two, container, false)

        initViews()
        initObservers()

        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
//            binding.imageViewUpload.setImageURI(data?.data)
            val bitmapImage = MediaStore.Images.Media.getBitmap(activity!!.contentResolver, data?.data)
//          image = bitMapToString(bitmapImage)

            val file = File("" + data?.data?.path)
            //binding.editTextChequeLeaf.setText(imageFile.name + file.extension)

            Log.e("File", "${data?.data.toString()} --- ${file.name} --- ${file.path} --- ${file.absolutePath}")

            bankAccountViewModel.getFileName(context!!, data?.data!!)
            bankAccountViewModel.getRealPathFromURI(context!!, data.data!!)
//          bankAccountViewModel.encodeToBase64(file)
        }
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Neti Mail")
        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle
        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.txtNameVal.text = name
        binding.txtPhoneVal.text = phone

        binding.addAttachment.setOnClickListener {
            bankAccountViewModel.isChequeLeafImage = true
            if (AppUtilsClass.checkPermissionForImage(activity!!)) {
                pickFilesFromMobileStorage()
            }
        }

        binding.buttonSend.setOnClickListener {
            bankAccountViewModel.onSendNetiMailClick(
                netMainID,
                binding.editTextDes.text.toString(),
                true
            )
        }
    }

    private fun initObservers() {

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.success.value = null
            }
        })

        bankAccountViewModel.isChatboxBody.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Message can't left empty.")
            }
        })

        bankAccountViewModel.chequeLeafImageName.observe(viewLifecycleOwner, Observer {
            binding.attachementImage.text = it
            binding.cross.visibility=View.VISIBLE
        })

        bankAccountViewModel.isChequeLeafImageEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Attachment cant't left empty .")
            }
        })
    }

    private fun pickFilesFromMobileStorage() {
        val galleryIntent = Intent(Intent.ACTION_GET_CONTENT, null)
        galleryIntent.type = "image/*"

        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        val pdfIntent = Intent(Intent.ACTION_GET_CONTENT)
        pdfIntent.type = "*/*"

        val chooser = Intent(Intent.ACTION_CHOOSER)
        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent)
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:")

        val intentArray = arrayOf(cameraIntent)
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        startActivityForResult(chooser,
            IMAGE_PICK_CODE
        )
    }

    companion object {
        private const val IMAGE_PICK_CODE = 1000
        private const val PERMISSION_CODE_READ = 1001
        private const val PERMISSION_CODE_WRITE = 1002
    }

    private fun showToastyError(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_LONG).show()
    }
}
