package com.netizen.netiworld.view.fragment.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.RegistrationSucessLayoutBinding


class RegSuccessDialogFragment() : DialogFragment() {
    private lateinit var binding: RegistrationSucessLayoutBinding
    private var username: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TransparentDialogTheme)
        username = arguments!!.getString("name")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding= DataBindingUtil.inflate(inflater, R.layout.registration_sucess_layout, container, false)

        binding.name.text = username

        binding.gotoLogin.setOnClickListener {
            dismiss()
            //findNavController().popBackStack(R.id.fragmentSignUpOne, false)
           // findNavController().popBackStack(R.id.fragmentSignUpTwo, false)
            findNavController().navigate(R.id.signInFragment)
        }
        return binding.root
    }
}
