package com.netizen.netiworld.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment

import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentLogOutDialogBinding
import com.netizen.netiworld.utils.Loaders

/**
 * A simple [Fragment] subclass.
 */
class LogOutDialogFragment : DialogFragment() {

    private lateinit var binding: FragmentLogOutDialogBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_log_out_dialog,
            container,
            false
        )

        initViews()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TransparentDialogTheme)
    }

    private fun initViews() {
        binding.textViewNo.setOnClickListener {
            dismiss()
        }

        binding.textViewYes.setOnClickListener {
            Loaders.isLogOut.value = true
            dismiss()
        }
    }
}
