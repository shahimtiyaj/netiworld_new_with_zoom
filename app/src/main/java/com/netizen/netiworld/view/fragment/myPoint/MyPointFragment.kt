package com.netizen.netiworld.view.fragment.myPoint

import android.app.Application
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentMyPointBinding
import com.netizen.netiworld.utils.OnSwipeTouchListener
import com.netizen.netiworld.viewModel.HomeViewModel
import com.netizen.netiworld.viewModel.ProfileViewModel

/**
 * A simple [Fragment] subclass.
 */
class MyPointFragment : DialogFragment() {

    private lateinit var binding: FragmentMyPointBinding
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var profileViewModel: ProfileViewModel

    private var isStartUpOpened = false
    private var isBalanceOpened = false
    private var isPurchaseOpened = false
    private var isSaleOpened = false
    private var isBalanceReportOpened = false
    private var isPurchaseReportOpened = false
    private var isPurchaseCodeReportOpened = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_point, container, false)

        initViews()
        initObservers()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TransparentDialogTheme)

        homeViewModel = ViewModelProvider(activity!!,
            HomeViewModel.HomeViewModelFactory(context!!.applicationContext as Application))
            .get(HomeViewModel::class.java)

        profileViewModel = ViewModelProvider(activity!!,
            ProfileViewModel.ProfileViewModelFactory(context!!.applicationContext as Application))
            .get(ProfileViewModel::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog?.window?.attributes?.windowAnimations = R.style.HorizontalDialogAnimation
    }

    private fun initViews() {
        binding.layoutParentView.setOnTouchListener(object : OnSwipeTouchListener(context!!) {
            override fun onSwipeLeft() {
                super.onSwipeLeft()
                dismiss()
            }
        })

        binding.scrollMainView.setOnTouchListener(object : OnSwipeTouchListener(context!!) {
            override fun onSwipeLeft() {
                super.onSwipeLeft()
                dismiss()
            }
        })

        binding.textViewStartUp.setOnClickListener {
            if (!isStartUpOpened) {
                showStartUpPoint()
            } else {
                hideStartUpPoint()
            }

            hideBalancePoint()
            hidePurchasePoint()
            hideSalePoint()
            hideBalanceReportPoint()
            hidePurchaseReportPoint()
            hidePurchaseCodeReportPoint()
        }

        binding.textViewBalance.setOnClickListener {
            if (!isBalanceOpened) {
                showBalancePoint()
            } else {
                hideBalancePoint()
            }

            hideStartUpPoint()
            hidePurchasePoint()
            hideSalePoint()
            hideBalanceReportPoint()
            hidePurchaseReportPoint()
            hidePurchaseCodeReportPoint()
        }

        binding.textViewPurchase.setOnClickListener {
            if (!isPurchaseOpened) {
                showPurchasePoint()
            } else {
                hidePurchasePoint()
            }

            hideStartUpPoint()
            hideBalancePoint()
            hideSalePoint()
            hideBalanceReportPoint()
            hidePurchaseReportPoint()
            hidePurchaseCodeReportPoint()
        }

        binding.textViewSale.setOnClickListener {
            if (!isSaleOpened) {
                showSalePoint()
            } else {
                hideSalePoint()
            }

            hideStartUpPoint()
            hideBalancePoint()
            hidePurchasePoint()
            hideBalanceReportPoint()
            hidePurchaseReportPoint()
            hidePurchaseCodeReportPoint()
        }

        binding.textViewBalanceReport.setOnClickListener {
            if (!isBalanceReportOpened) {
                showBalanceReportPoint()
            } else {
                hideBalanceReportPoint()
            }

            hideStartUpPoint()
            hidePurchaseReportPoint()
            hideBalancePoint()
            hidePurchasePoint()
            hideSalePoint()
            hidePurchaseCodeReportPoint()
        }

        binding.textViewPurchaseReport.setOnClickListener {
            if (!isPurchaseReportOpened) {
                showPurchaseReportPoint()
            } else {
                hidePurchaseReportPoint()
            }

            hideStartUpPoint()
            hideBalanceReportPoint()
            hideBalancePoint()
            hidePurchasePoint()
            hideSalePoint()
            hidePurchaseCodeReportPoint()
        }

        binding.textViewPurchaseCodeReport.setOnClickListener {
            if (!isPurchaseCodeReportOpened) {
                showPurchaseCodeReportPoint()
            } else {
                hidePurchaseCodeReportPoint()
            }

            hideStartUpPoint()
            hideBalanceReportPoint()
            hideBalancePoint()
            hidePurchasePoint()
            hideSalePoint()
            hidePurchaseReportPoint()
        }

        binding.textViewMyProfile.setOnClickListener {
            dismiss()
            homeViewModel.isMyProfileClicked.value = true
        }

        binding.textViewNetiMail.setOnClickListener {
            dismiss()
            homeViewModel.isNetiMailClicked.value = true
        }

        binding.textViewWallet.setOnClickListener {
            dismiss()
            homeViewModel.isWalletClicked.value = true
        }

        binding.textViewMessage.setOnClickListener {
            dismiss()
            homeViewModel.isMessageClicked.value = true
            findNavController().navigate(R.id.action_homeFragment_to_fragmentMessageRecharge)
        }

        binding.textViewOfferProduct.setOnClickListener {
            dismiss()
            homeViewModel.isOfferProductClicked.value = true
        }

        binding.textViewGeneralProduct.setOnClickListener {
            dismiss()
            homeViewModel.isGeneralProductClicked.value = true
        }

        binding.textViewBank.setOnClickListener {
            dismiss()
            homeViewModel.isBankClicked.value = true
        }

        binding.textViewMessageReport.setOnClickListener {
            dismiss()
            homeViewModel.isMessageLogClicked.value = true
        }

        binding.textViewStatementReport.setOnClickListener {
            dismiss()
            homeViewModel.isStatementReportClicked.value = true
        }

        binding.textViewRevenueReport.setOnClickListener {
            dismiss()
            homeViewModel.isRevenueReportClicked.value = true
        }

        binding.textViewGeneralProductReport.setOnClickListener {
            dismiss()
            homeViewModel.isGeneralProductReportClicked.value = true
        }

        binding.textViewCodeLogReport.setOnClickListener {
            dismiss()
            homeViewModel.isPurchaseCodeReportClicked.value = true
        }

        binding.textViewOfferProductReport.setOnClickListener {
            dismiss()
            homeViewModel.isOfferProductReportClicked.value = true
        }

        binding.textViewWalletReport.setOnClickListener {
            dismiss()
            homeViewModel.isWalletLogReportClicked.value = true
        }

        binding.textViewSupportToken.setOnClickListener {
            dismiss()
            homeViewModel.isSupportTokenClicked.value = true
        }

        binding.textViewAddPoint.setOnClickListener {
            dismiss()
            homeViewModel.isAddPointClicked.value = true
        }
    }

    private fun initObservers() {
        homeViewModel.homePageInfoLiveData.observe(viewLifecycleOwner, Observer {
//            binding.textViewUserName.text = it.getBasicInfo()?.getBasicEmail()
            binding.textViewNetiId.text = "Neti ID: " + it.getBasicInfo()?.getCustomNetiID().toString()
        })

        profileViewModel.profileInformation.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                binding.textViewUserName.text = it.getFullName().toString()
            }
        })

        profileViewModel.photoFileContent.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                val byteArray = Base64.decode(it, Base64.DEFAULT)
                binding.imageViewUser.setImageBitmap(convertToBitmap(byteArray))
            }
        })
    }

    private fun showStartUpPoint() {
        isStartUpOpened = true

        binding.textViewStartUp.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewStartUp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_up_icon, 0)
        binding.textViewMyProfile.visibility = View.VISIBLE
        binding.textViewAddPoint.visibility = View.VISIBLE
        binding.textViewNetiMail.visibility = View.VISIBLE
        binding.textViewSupportToken.visibility = View.VISIBLE
    }

    private fun hideStartUpPoint() {
        isStartUpOpened = false

        binding.textViewStartUp.setBackgroundResource(0)
        binding.textViewStartUp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_down_icon, 0)
        binding.textViewMyProfile.visibility = View.GONE
        binding.textViewAddPoint.visibility = View.GONE
        binding.textViewNetiMail.visibility = View.GONE
        binding.textViewSupportToken.visibility = View.GONE
    }

    private fun showBalancePoint() {
        isBalanceOpened = true

//        binding.textViewBalance.setBackgroundDrawable(resources.getDrawable(R.drawable.menu_hover_background))
//        binding.textViewBalance.background = ContextCompat.getDrawable(context!!, R.drawable.menu_hover_background)
        binding.textViewBalance.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewBalance.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_up_icon, 0)
        binding.textViewWallet.visibility = View.VISIBLE
        binding.textViewMessage.visibility = View.VISIBLE
    }

    private fun hideBalancePoint() {
        isBalanceOpened = false

        binding.textViewBalance.setBackgroundResource(0)
        binding.textViewBalance.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_down_icon, 0)
        binding.textViewWallet.visibility = View.GONE
        binding.textViewMessage.visibility = View.GONE
    }

    private fun showPurchasePoint() {
        isPurchaseOpened = true

        binding.textViewPurchase.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewPurchase.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_up_icon, 0)
        binding.textViewGeneralProduct.visibility = View.VISIBLE
        binding.textViewOfferProduct.visibility = View.VISIBLE
    }

    private fun hidePurchasePoint() {
        isPurchaseOpened = false

        binding.textViewPurchase.setBackgroundResource(0)
        binding.textViewPurchase.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_down_icon, 0)
        binding.textViewGeneralProduct.visibility = View.GONE
        binding.textViewOfferProduct.visibility = View.GONE
    }

    private fun showSalePoint() {
        isSaleOpened = true

        binding.textViewSale.setBackgroundResource(0)
        binding.textViewSale.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_up_icon, 0)
        binding.textViewSaleProduct.visibility = View.VISIBLE
    }

    private fun hideSalePoint() {
        isSaleOpened = false

        binding.textViewSale.setBackgroundResource(0)
        binding.textViewSale.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_down_icon, 0)
        binding.textViewSaleProduct.visibility = View.GONE
    }

    private fun showBalanceReportPoint() {
        isBalanceReportOpened = true

        binding.textViewBalanceReport.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewBalanceReport.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_up_icon, 0)
        binding.textViewWalletReport.visibility = View.VISIBLE
        binding.textViewMessageReport.visibility = View.VISIBLE
        binding.textViewStatementReport.visibility = View.VISIBLE
        binding.textViewRevenueReport.visibility = View.VISIBLE
    }

    private fun hideBalanceReportPoint() {
        isBalanceReportOpened = false

        binding.textViewBalanceReport.setBackgroundResource(0)
        binding.textViewBalanceReport.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_down_icon, 0)
        binding.textViewWalletReport.visibility = View.GONE
        binding.textViewMessageReport.visibility = View.GONE
        binding.textViewStatementReport.visibility = View.GONE
        binding.textViewRevenueReport.visibility = View.GONE
    }

    private fun showPurchaseReportPoint() {
        isPurchaseReportOpened = true

        binding.textViewPurchaseReport.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewPurchaseReport.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_up_icon, 0)
        binding.textViewGeneralProductReport.visibility = View.VISIBLE
        binding.textViewOfferProductReport.visibility = View.VISIBLE
        binding.textViewCodeLogReport.visibility = View.VISIBLE
    }

    private fun hidePurchaseReportPoint() {
        isPurchaseReportOpened = false

        binding.textViewPurchaseReport.setBackgroundResource(0)
        binding.textViewPurchaseReport.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_down_icon, 0)
        binding.textViewGeneralProductReport.visibility = View.GONE
        binding.textViewOfferProductReport.visibility = View.GONE
        binding.textViewCodeLogReport.visibility = View.GONE
    }

    private fun showPurchaseCodeReportPoint() {
        isPurchaseCodeReportOpened = true

        binding.textViewPurchaseCodeReport.setBackgroundResource(R.drawable.menu_hover_background)
        binding.textViewPurchaseCodeReport.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_up_icon, 0)
        binding.textViewUsedCode.visibility = View.VISIBLE
        binding.textViewUnusedCode.visibility = View.VISIBLE
    }

    private fun hidePurchaseCodeReportPoint() {
        isPurchaseCodeReportOpened = false

        binding.textViewPurchaseCodeReport.setBackgroundResource(0)
        binding.textViewPurchaseCodeReport.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home_white_24dp, 0, R.drawable.ic_drop_down_icon, 0)
        binding.textViewUsedCode.visibility = View.GONE
        binding.textViewUnusedCode.visibility = View.GONE
    }

    private fun convertToBitmap(bytes: ByteArray): Bitmap {
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
    }
}

