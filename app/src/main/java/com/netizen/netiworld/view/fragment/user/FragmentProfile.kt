package com.netizen.netiworld.view.fragment.user

import android.app.Application
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentProfileLayoutBinding
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.ProfileViewModel
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar
import es.dmoral.toasty.Toasty


class FragmentProfile : Fragment() {

    private lateinit var binding: FragmentProfileLayoutBinding
    private var profileViewModel: ProfileViewModel? = null
    private val mRoundedHorizontalProgressBar: RoundedHorizontalProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(null)
        profileViewModel = ViewModelProvider(
            this,
            ProfileViewModel.ProfileViewModelFactory(context!!.applicationContext as Application)
        ).get(ProfileViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile_layout, container, false)

        binding.lifecycleOwner = this

        profileViewModel?.getProfileInfo()

        initViews()

        initObservables()

        return binding.root
    }

    fun initViews(){
        val bundle = Bundle()
        bundle.putString("title", "Profile")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        //val completenessVal= 3*(100/19).
        //val finalVal= (15.div(19)).times(100)
        binding.progressBar1.animateProgress(1000, 0 , 80)
        binding.profileCompleteNess.setText("Profile Completenes - "+80+"%")
    }

    private fun initObservables() {

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        profileViewModel?.profileInformation?.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                binding.txtProfileName.text = it.getFullName().toString()
                binding.txtNetiId.text = "Neti ID : " + it.getCustomNetiID()
                binding.mobileNo.text = it.getBasicMobile().toString()
                binding.emailAddress.text = it.getBasicEmail().toString()
                binding.userActiveStatus.text = it.getUserStatus().toString()
                binding.village.text = it.getArea().toString()
                binding.upozilla.text = it.getUpazilla().toString()
                binding.district.text = it.getDistrict().toString()
                binding.division.text = it.getDivision().toString()
                profileViewModel?.getProfileImage(it.getImagePath().toString())
            }
        })

        profileViewModel?.photoFileContent?.observe(
            viewLifecycleOwner,
            Observer { photoFileContent ->
                if (!photoFileContent.isNullOrEmpty()) {
                    val theByteArray: ByteArray? = Base64.decode(photoFileContent, Base64.DEFAULT)
                    binding.profilePicId.setImageBitmap(theByteArray?.let {
                        convertToBitmap(it)
                    })
                }
            })
    }

    private fun convertToBitmap(b: ByteArray): Bitmap {
        Log.d("ArraySize", b.size.toString())
        return BitmapFactory.decodeByteArray(b, 0, b.size)
    }
}
