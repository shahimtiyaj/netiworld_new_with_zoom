package com.netizen.netiworld.view.fragment.myPoint.startUp.netiMail

import android.app.Application
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentNetiMailOneBinding
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.WalletViewModel
import es.dmoral.toasty.Toasty

class NetiMailFragmentOne : Fragment() {

    private lateinit var binding: FragmentNetiMailOneBinding
    private lateinit var walletViewModel: WalletViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        walletViewModel = ViewModelProvider(
            activity!!,
            WalletViewModel.WalletViewModelFactory(context!!.applicationContext as Application)
        ).get(WalletViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_neti_mail_one, container, false)
        initViews()
        initObservers()

        return binding.root
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Neti Mail")
        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle
        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.etNetiId.addTextChangedListener(MyTextWatcher(binding.etNetiId))

        binding.searchBtn.setOnClickListener {
            walletViewModel.checkTransferNetimanInfo(binding.etNetiId.text.toString())
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        walletViewModel.fullName.observe(viewLifecycleOwner, Observer { fullName ->
            if (!fullName.isNullOrEmpty()) {
                name = fullName
                findNavController().navigate(R.id.action_netiMailFragmentOne_to_netiMailFragmentTwo)
            }
        })


        walletViewModel.PhoneNo.observe(viewLifecycleOwner, Observer { PhoneNo ->
            if (!PhoneNo.isNullOrEmpty()) {
               phone = PhoneNo
            }
        })

        walletViewModel.netiMainID.observe(viewLifecycleOwner, Observer { netMainId ->
            if (!netMainId.isNullOrEmpty()) {
                netMainID = netMainId
            }
        })
    }

    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

        }

        override fun afterTextChanged(editable: Editable) {

        }
    }

    companion object {
        var name = ""
        var phone = ""
        var netMainID = ""
    }
}
