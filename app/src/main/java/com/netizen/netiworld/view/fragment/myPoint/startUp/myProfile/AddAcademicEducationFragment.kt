package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentAddAcademicEducationBinding
import com.netizen.netiworld.model.EducationInfo
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.SearchSpinnerDialogFragment
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.ProfileViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class AddAcademicEducationFragment : Fragment() {

    private lateinit var binding: FragmentAddAcademicEducationBinding
    private lateinit var profileViewModel: ProfileViewModel
    private var foreignStatus = 0
    private var educationInfoId = 0

    private var title = "Update Academic Education Info"
    private var isUpdate = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_add_academic_education,
            container,
            false
        )

        if (!arguments?.getString("title").isNullOrEmpty()) {
            isUpdate = true
            title = arguments?.getString("title").toString()
            setData(arguments?.get("educationData") as EducationInfo)
        }

        initViews()
        initObservers()
        profileViewModel.getEducationLevels()
        profileViewModel.getMajorSubjects()
        profileViewModel.getResults()
        profileViewModel.getPassingYears()
        profileViewModel.getBoards()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        profileViewModel = ViewModelProvider(
            activity!!,
            ProfileViewModel.ProfileViewModelFactory(context!!.applicationContext as Application)
        ).get(ProfileViewModel::class.java)
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Add Academic Info")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.spinnerForeignStatus.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (position == 2) {
                        foreignStatus = 1
                    }
                }
            }

        binding.editTextEducationLevel.setOnClickListener {
            showSearchSpinnerDialog("profile", "education level", 1)
        }

        binding.editTextMajor.setOnClickListener {
            showSearchSpinnerDialog("profile", "major/group", 2)
        }

        binding.editTextResult.setOnClickListener {
            showSearchSpinnerDialog("profile", "result", 3)
        }

        binding.editTextPassingYear.setOnClickListener {
            showSearchSpinnerDialog("profile", "passing year", 4)
        }

        binding.editTextBoard.setOnClickListener {
            showSearchSpinnerDialog("profile", "board", 5)
        }

        binding.buttonSave.setOnClickListener {
            when {
                isUpdate -> profileViewModel.checkUpdateAcademicEducationInfo(
                    binding.editTextSerial.text.toString(),
                    educationInfoId,
                    binding.editTextExamTitle.text.toString(),
                    binding.editTextDuration.text.toString(),
                    binding.editTextInstituteName.text.toString(),
                    binding.editTextAchievement.text.toString(),
                    foreignStatus
                )

                else -> profileViewModel.checkAcademicEducationInfo(
                    binding.editTextSerial.text.toString(),
                    binding.editTextExamTitle.text.toString(),
                    binding.editTextDuration.text.toString(),
                    binding.editTextInstituteName.text.toString(),
                    binding.editTextAchievement.text.toString(),
                    foreignStatus
                )
            }
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                showToastyError(it)
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
                clearFields()
            }
        })

        Loaders.searchValue1.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                profileViewModel.getEducationLevelId(it)
                binding.editTextEducationLevel.setText(it)
            }
        })

        Loaders.searchValue2.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                profileViewModel.getMajorSubjectId(it)
                binding.editTextMajor.setText(it)
            }
        })

        Loaders.searchValue3.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                profileViewModel.getResultId(it)
                binding.editTextResult.setText(it)
            }
        })

        Loaders.searchValue4.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                profileViewModel.getPassingYearId(it)
                binding.editTextPassingYear.setText(it)
            }
        })

        Loaders.searchValue5.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                profileViewModel.getBoardId(it)
                binding.editTextBoard.setText(it)
            }
        })

        profileViewModel.isSerialEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Serial is empty!")
                profileViewModel.isSerialEmpty.value = false
            }
        })

        profileViewModel.isEducationLevelEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Education level is empty!")
                profileViewModel.isEducationLevelEmpty.value = false
            }
        })

        profileViewModel.isExamEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Exam/Degree is empty!")
                profileViewModel.isExamEmpty.value = false
            }
        })

        profileViewModel.isMajorEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Major/Group is empty!")
                profileViewModel.isMajorEmpty.value = false
            }
        })

        profileViewModel.isResultEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Result is empty!")
                profileViewModel.isResultEmpty.value = false
            }
        })

        profileViewModel.isPassingYearEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Year of Passing is empty!")
                profileViewModel.isPassingYearEmpty.value = false
            }
        })

        profileViewModel.isDurationEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Duration is empty!")
                profileViewModel.isDurationEmpty.value = false
            }
        })

        profileViewModel.isBoardEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Board is empty!")
                profileViewModel.isBoardEmpty.value = false
            }
        })

        profileViewModel.isInstituteEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Institute name is empty!")
                profileViewModel.isInstituteEmpty.value = false
            }
        })

        profileViewModel.isAchievementEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Achievement is empty!")
                profileViewModel.isAchievementEmpty.value = false
            }
        })
    }

    private fun showSearchSpinnerDialog(dataType: String, title: String, spinnerType: Int) {
        val bundle = Bundle()
        bundle.putString("dataType", dataType)
        bundle.putString("title", title)
        bundle.putInt("spinnerType", spinnerType)

        val searchSpinnerFragment = SearchSpinnerDialogFragment()
        searchSpinnerFragment.arguments = bundle
        searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
    }

    private fun setData(educationInfo: EducationInfo) {
        educationInfoId = educationInfo.getEducationInfoId()!!
        binding.editTextSerial.setText(educationInfo.getEduInfoSerial().toString())
        binding.editTextEducationLevel.setText(educationInfo.getDegreeInfoDTO()?.getCategoryName())
        binding.editTextExamTitle.setText(educationInfo.getExamTitle())
        binding.editTextMajor.setText(educationInfo.getSubjectInfoDTO()?.getCategoryName())
        binding.editTextResult.setText(educationInfo.getGradeInfoDTO()?.getCategoryName())
        binding.editTextPassingYear.setText(
            educationInfo.getPassingYearInfoDTO()?.getCategoryName()
        )
        binding.editTextDuration.setText(educationInfo.getEduDuration())
        binding.editTextBoard.setText(educationInfo.getBoardInfoDTO()?.getCategoryName())
        binding.editTextInstituteName.setText(educationInfo.getInstituteName())
        // binding.spinnerForeignStatus.setPromptId(educationInfo.getInstituteForeginStatus()!!)
        binding.editTextAchievement.setText(educationInfo.getAchievementDetails())
    }

    private fun clearFields() {
        binding.editTextSerial.text = null
        binding.editTextEducationLevel.text = null
        binding.editTextExamTitle.text = null
        binding.editTextMajor.text = null
        binding.editTextResult.text = null
        binding.editTextPassingYear.text = null
        binding.editTextDuration.text = null
        binding.editTextBoard.text = null
        binding.editTextInstituteName.text = null
        binding.editTextAchievement.text = null
    }

    private fun showToastyError(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_LONG).show()
    }
}
