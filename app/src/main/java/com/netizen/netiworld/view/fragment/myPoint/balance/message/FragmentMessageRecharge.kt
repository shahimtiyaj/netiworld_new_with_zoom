package com.netizen.netiworld.view.fragment.myPoint.balance.message

import android.app.Application
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentMessageRechargeBinding
import com.netizen.netiworld.model.HomePageInfo
import com.netizen.netiworld.utils.AppPreferences
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.SearchSpinnerDialogFragment
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.HomeViewModel
import com.netizen.netiworld.viewModel.MessageViewModel
import es.dmoral.toasty.Toasty

class FragmentMessageRecharge : Fragment() {

    private lateinit var binding: FragmentMessageRechargeBinding
    private lateinit var messageViewModel: MessageViewModel
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var preferences: AppPreferences
    private var productRoleAssignID: String? = null
    private var productUnitPrice: String? = null
    private lateinit var productVatParcentage: String

    private var tempPurchasePointList: ArrayList<String>? = null
    private var tempMessageTypeList: ArrayList<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel = ViewModelProvider(
            activity!!,
            HomeViewModel.HomeViewModelFactory(context!!.applicationContext as Application)
        ).get(HomeViewModel::class.java)
        messageViewModel = ViewModelProvider(
            activity!!,
            MessageViewModel.MessageBalanceViewModelFactory(context!!.applicationContext as Application)
        ).get(
            MessageViewModel::class.java
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_message_recharge, container, false)

        binding.lifecycleOwner = this
        preferences = AppPreferences(context)

        initViews()
        initObservables()
        homeViewModel.getHomePageInfo()
        messageViewModel.getPurchasePointData()

        return binding.root
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Message Log")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.etMessageQuantity.addTextChangedListener(MyTextWatcher(binding.etMessageQuantity))

        binding.spinnerPurchasePoint.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("dataType", "message")
            bundle.putString("title", "purchase point")
            bundle.putInt("spinnerType", 1)

            val searchSpinnerFragment = SearchSpinnerDialogFragment()
            searchSpinnerFragment.arguments = bundle
            searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
        }

        binding.spinnerMessageType.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("dataType", "message")
            bundle.putString("title", "message type")
            bundle.putInt("spinnerType", 2)

            val searchSpinnerFragment = SearchSpinnerDialogFragment()
            searchSpinnerFragment.arguments = bundle
            searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
        }
    }

    private fun initObservables() {

        Loaders.searchValue1.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.spinnerMessageType.text = null
                binding.spinnerPurchasePoint.setText(it)
                messageViewModel.getRoleId(it)
            }
        })

        Loaders.searchValue2.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.spinnerMessageType.setText(it)
                messageViewModel.getProductId1(it)
                messageViewModel.productRoleAssignID1.observe(
                    viewLifecycleOwner,
                    Observer { productRoleAssignID ->
                        this.productRoleAssignID = productRoleAssignID
                    })
            }
        })

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        homeViewModel.homePageInfoLiveData.observe(viewLifecycleOwner, Observer { homePageInfo ->
            setHomePageInfo(homePageInfo)
        })

        messageViewModel.tempPurchasePointList.observe(
            viewLifecycleOwner,
            Observer { tempPurchasePointList ->
                if (!tempPurchasePointList.isNullOrEmpty()) {
                    this.tempPurchasePointList = tempPurchasePointList
                }
            })

        messageViewModel.tempMessageTypeList1.observe(
            viewLifecycleOwner,
            Observer { tempMessageList ->
                if (!tempMessageList.isNullOrEmpty()) {
                    this.tempMessageTypeList = tempMessageList
                }
            })

        binding.btnMessageRecharge.setOnClickListener {
            when {
                binding.etMessageQuantity.text.isNullOrEmpty() -> {
                    binding.etMessageQuantity.error = getString(R.string.err_mssg_messageQty)
                }
                productRoleAssignID.isNullOrEmpty() -> {
                    Toasty.error(
                        activity!!,
                        "Please Select Required Field !",
                        Toast.LENGTH_SHORT,
                        true
                    ).show()
                }
                else -> {
                    messageViewModel.checkMessageRechargeValues(
                        binding.etMessageQuantity.text.toString(),
                        productRoleAssignID!!
                    )
                }
            }
        }

        messageViewModel.isPurchasePointEmpty.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                Toasty.error(activity!!, "Select Purchase Point", Toast.LENGTH_SHORT, true).show()
                messageViewModel.isPurchasePointEmpty.value = null
            }
        })

        messageViewModel.isMessageTypeEmpty.observe(viewLifecycleOwner, Observer {
            if (it != null && it) {
                Toasty.error(activity!!, "Select Message Type", Toast.LENGTH_SHORT, true).show()
                messageViewModel.isMessageTypeEmpty.value = null
            }
        })

        messageViewModel.unitPrice.observe(viewLifecycleOwner, Observer { unitPrice ->
            this.productUnitPrice = unitPrice
            Log.d("Unit Price", unitPrice)
            binding.messageLayputPoint.unitPriceValue.text = unitPrice
        })

        messageViewModel.percentageVat.observe(viewLifecycleOwner, Observer { vatParcentage ->
            this.productVatParcentage = vatParcentage
            Log.d("Unit Price", vatParcentage)
        })

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.success.value = null
                homeViewModel.getHomePageInfo()
                homeViewModel.homePageInfoLiveData.observe(
                    viewLifecycleOwner,
                    Observer { homePageInfo ->
                        setHomePageInfo(homePageInfo)
                    })
                binding.etMessageQuantity.text?.clear()
                findNavController().popBackStack()
            }
        })
    }

    private inner class MyTextWatcher(private val view: View?) : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            try {
                val totalPrice = binding.etMessageQuantity.text.toString()
                    .toDouble() * productUnitPrice?.toDouble()!!
                binding.messageLayputPoint.totalPriceValue.text =
                    String.format("%, .2f", totalPrice)

                val vatCal = productVatParcentage.toDouble().times(totalPrice).div(100)
                binding.messageLayputPoint.vatAmtValue.text = String.format("%, .2f", vatCal)

                val totalPayable = totalPrice + vatCal
                Log.d("Total Payable :", totalPayable.toString())
                binding.messageLayputPoint.payableAmtValue.text =
                    String.format("%, .2f", totalPayable)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun afterTextChanged(editable: Editable) {

        }
    }

    private fun setHomePageInfo(homePageInfo: HomePageInfo) {
        binding.textViewWalletBalance.text =
            String.format("%, .2f", homePageInfo.getBasicInfo()?.getUserWalletBalance())
        binding.textViewMessageBalance.text =
            String.format("%, .0f", homePageInfo.getBasicInfo()?.getSmsBalance())
    }
}
