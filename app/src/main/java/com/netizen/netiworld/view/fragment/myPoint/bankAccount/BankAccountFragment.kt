package com.netizen.netiworld.view.fragment.myPoint.bankAccount

import android.Manifest
import android.app.Activity
import android.app.Application
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentBankAccountBinding
import com.netizen.netiworld.model.WithDrawInfo
import com.netizen.netiworld.utils.AppUtilsClass
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.SearchSpinnerDialogFragment
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.BankAccountViewModel
import com.netizen.netiworld.viewModel.WalletViewModel
import es.dmoral.toasty.Toasty
import java.io.File


/**
 * A simple [Fragment] subclass.
 */
class BankAccountFragment : Fragment() {

    companion object {
        private const val IMAGE_PICK_CODE = 1000
        private const val PERMISSION_CODE_READ = 1001
        private const val PERMISSION_CODE_WRITE = 1002
    }

    private lateinit var binding: FragmentBankAccountBinding
    private lateinit var bankAccountViewModel: BankAccountViewModel
    private lateinit var walletViewModel: WalletViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_bank_account,
            container,
            false
        )

        initViews()
        initObservers()
        bankAccountViewModel.getBankAccountList()
        bankAccountViewModel.getDistrictList()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bankAccountViewModel = ViewModelProvider(
            activity!!,
            BankAccountViewModel.BankAccountViewModelFactory(context!!.applicationContext as Application)
        ).get(BankAccountViewModel::class.java)

        walletViewModel = ViewModelProvider(
            activity!!,
            WalletViewModel.WalletViewModelFactory(context!!.applicationContext as Application)
        ).get(WalletViewModel::class.java)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
//            binding.imageViewUpload.setImageURI(data?.data)
            val bitmapImage = MediaStore.Images.Media.getBitmap(activity!!.contentResolver, data?.data)
//            image = bitMapToString(bitmapImage)

            val file = File("" + data?.data?.path)
//            binding.editTextChequeLeaf.setText(imageFile.name + file.extension)

            Log.e("File", "${data?.data.toString()} --- ${file.name} --- ${file.path} --- ${file.absolutePath}")

            bankAccountViewModel.getFileName(context!!, data?.data!!)
            bankAccountViewModel.getRealPathFromURI(context!!, data.data!!)
//            bankAccountViewModel.encodeToBase64(file)
        }
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Bank Account")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.textViewBankList.setOnClickListener {
            findNavController().navigate(R.id.action_bankFragment_to_bankListFragment)
        }

        binding.editTextBankName.setOnClickListener {
            if (bankAccountViewModel.isBankAccountListFound.value == true) {
                showSearchSpinnerDialog("bank", "bank", 1)
            } else {
                showToastyError("No value found!")
            }
        }

        binding.editTextDistrict.setOnClickListener {
            if (bankAccountViewModel.isDistrictListFound.value == true) {
                showSearchSpinnerDialog("bank", "district", 2)
            } else {
                showToastyError("No value found!")
            }
        }

        binding.editTextBranch.setOnClickListener {
            if (bankAccountViewModel.isBranchListFound.value == true) {
                showSearchSpinnerDialog("bank", "branch", 3)
            } else {
                showToastyError("No value found!")
            }
        }

        binding.editTextChequeLeaf.setOnClickListener {
            bankAccountViewModel.isChequeLeafImage = true
            if (AppUtilsClass.checkPermissionForImage(activity!!)) {
                pickFilesFromMobileStorage()
            }
        }

        binding.editTextOthers.setOnClickListener {
            bankAccountViewModel.isChequeLeafImage = false
            if (AppUtilsClass.checkPermissionForImage(activity!!)) {
                pickFilesFromMobileStorage()
            }
        }

        binding.buttonSave.setOnClickListener {
            bankAccountViewModel.checkValidation(
                binding.editTextAccountHolderName.text.toString(),
                binding.editTextAccountNumber.text.toString(),
                binding.editTextAccountDetails.text.toString()
            )
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                showToastyError(it)
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
                clearFields()
            }
        })

        Loaders.searchValue1.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.editTextBankName.setText(it)
                binding.editTextDistrict.text = null
                binding.editTextBranch.text = null
                bankAccountViewModel.getBankId(it)
            }
        })

        Loaders.searchValue2.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.editTextDistrict.setText(it)
                binding.editTextBranch.text = null
                bankAccountViewModel.getDistrictID(it)
            }
        })

        Loaders.searchValue3.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.editTextBranch.setText(it)
                bankAccountViewModel.getBranchId(it)
            }
        })

        walletViewModel.withDrawInfo.observe(viewLifecycleOwner, Observer {
            setWithdrawInfo(it)
        })

        bankAccountViewModel.routingNumber.observe(viewLifecycleOwner, Observer {
            binding.editTextRoutingNumber.setText(it)
        })

        bankAccountViewModel.chequeLeafImageName.observe(viewLifecycleOwner, Observer {
            binding.editTextChequeLeaf.setText(it)
        })

        bankAccountViewModel.otherImageName.observe(viewLifecycleOwner, Observer {
            binding.editTextOthers.setText(it)
        })

        bankAccountViewModel.isBankEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Please select a bank.")
            }
        })

        bankAccountViewModel.isDistrictEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Please select a district.")
            }
        })

        bankAccountViewModel.isBranchEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Please select a branch.")
            }
        })

        bankAccountViewModel.isRoutingNumberEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Routing number can not be empty!.")
            }
        })

        bankAccountViewModel.isAccountHolderNameEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Please provide an account holder name.")
            }
        })

        bankAccountViewModel.isAccountNumberEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Please provide an account number.")
            }
        })

        bankAccountViewModel.isAccountDetailsEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Please provide an account details.")
            }
        })

        bankAccountViewModel.isChequeLeafImageEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Please attach a cheque leaf image.")
            }
        })
    }

    private fun setWithdrawInfo(withDrawInfo: WithDrawInfo?) {
        binding.textViewBankName.text = withDrawInfo?.getUserBankAccountInfoDTO()
            ?.getCoreBankBranchInfoDTO()?.getCoreBankInfoDTO()?.getCategoryName()
        binding.textViewBranchName.text = withDrawInfo?.getUserBankAccountInfoDTO()
            ?.getCoreBankBranchInfoDTO()?.getBranchName()
    }

    private fun showSearchSpinnerDialog(dataType: String, title: String, spinnerType: Int) {
        val bundle = Bundle()
        bundle.putString("dataType", dataType)
        bundle.putString("title", title)
        bundle.putInt("spinnerType", spinnerType)

        val searchSpinnerFragment = SearchSpinnerDialogFragment()
        searchSpinnerFragment.arguments = bundle
        searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
    }

    private fun pickFilesFromMobileStorage() {
        val galleryIntent = Intent(Intent.ACTION_GET_CONTENT, null)
        galleryIntent.type = "image/*"

        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        val pdfIntent = Intent(Intent.ACTION_GET_CONTENT)
        pdfIntent.type = "*/*"

        val chooser = Intent(Intent.ACTION_CHOOSER)
        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent)
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:")

//        val intentArray = arrayOf(cameraIntent, pdfIntent)
        val intentArray = arrayOf(cameraIntent)
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        startActivityForResult(chooser, IMAGE_PICK_CODE)
    }

    private fun clearFields() {
        binding.editTextBankName.text = null
        binding.editTextDistrict.text = null
        binding.editTextBranch.text = null
        binding.editTextRoutingNumber.text = null
        binding.editTextAccountHolderName.text = null
        binding.editTextAccountNumber.text = null
        binding.editTextAccountDetails.text = null
        binding.editTextChequeLeaf.text = null
        binding.editTextOthers.text = null
    }

    private fun showToastyError(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_LONG).show()
    }
}
