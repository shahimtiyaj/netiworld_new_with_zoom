package com.netizen.netiworld.view.fragment.myPoint.purchase

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentOfferProductBinding
import com.netizen.netiworld.model.HomePageInfo
import com.netizen.netiworld.utils.AppUtilsClass
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.HomeViewModel
import com.netizen.netiworld.viewModel.PurchaseViewModel
import es.dmoral.toasty.Toasty

class FragmentOfferProduct : Fragment() {

    private lateinit var binding: FragmentOfferProductBinding
    private var purchaseViewModel: PurchaseViewModel? = null

    private  var productQnty: String? =null
    private  var unitPrice: String? =""
    private  var payableAmt: String? =""
    private  var discountAmt: String? =""
    private  var totalAmt: String? =""
    private  var productOfferID: String? =""
    private  var discountAmtPer: String? =""
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel = ViewModelProvider(
            activity!!,
            HomeViewModel.HomeViewModelFactory(context!!.applicationContext as Application)
        ).get(HomeViewModel::class.java)
        purchaseViewModel = ViewModelProvider(
            this,
            PurchaseViewModel.PurchaseViewModelFactory(context!!.applicationContext as Application)
        ).get(PurchaseViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_offer_product, container, false)

        binding.lifecycleOwner = this
        initViews()
        initObservables()

        return binding.root
    }

    fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Offer Product")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.btnSerachOfferCode.setOnClickListener {
            if (binding.etOfferCode.text.isNullOrEmpty()) {
               // binding.etOfferCode.error = getString(R.string.err_mssg_offerCode)
                Toasty.error(context!!, getString(R.string.err_mssg_offerCode), Toast.LENGTH_LONG, true).show()
            } else {
                purchaseViewModel?.getOfferProduct(binding.etOfferCode.text.toString())
            }
        }

        binding.btnPurchase.setOnClickListener {
            if (binding.etOfferCode.text.isNullOrEmpty()) {
                //binding.etOfferCode.error = getString(R.string.err_mssg_offerCode)
                Toasty.error(context!!, getString(R.string.err_mssg_offerCode), Toast.LENGTH_LONG, true).show()
            } else if (binding.pname.text.isNullOrEmpty()) {
                Toasty.error(context!!, "Please Search Offer Code", Toast.LENGTH_LONG, true).show()
            } else {
                purchaseViewModel?.submitOfferProduct(
                    unitPrice,
                    payableAmt,
                    discountAmt,
                    totalAmt,
                    productOfferID
                )
            }
        }
    }

    private fun initObservables() {

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.success.value = null
            }
        })

        homeViewModel.homePageInfoLiveData.observe(viewLifecycleOwner, Observer { homePageInfo ->
            setHomePageInfo(homePageInfo)
        })

        purchaseViewModel?.productName?.observe(viewLifecycleOwner, Observer { productName ->
            if (!productName.isNullOrEmpty()) {
                binding.pname.text = ": $productName"
            }
        })

        purchaseViewModel?.productOfferID?.observe(viewLifecycleOwner, Observer { productOfferID ->
            if (!productOfferID.isNullOrEmpty()) {
                this.productOfferID = productOfferID
            }
        })

        purchaseViewModel?.unitPrice?.observe(viewLifecycleOwner, Observer { unitPrice ->
            if (!unitPrice.isNullOrEmpty()) {
                this.unitPrice = unitPrice
                binding.unitPrice.text = ": " + unitPrice

                purchaseViewModel?.productQuantity?.observe(
                    viewLifecycleOwner,
                    Observer { productQnty ->
                        if (!productQnty.isNullOrEmpty()) {
                            this.productQnty = productQnty
                            binding.productQuantity.text = ": " + productQnty
                            binding.totalAmount.text =
                                ": " + (productQnty.toInt().times(unitPrice.toDoubleOrNull()!!)).toString()
                            this.totalAmt =
                                (productQnty.toInt().times(unitPrice.toDoubleOrNull()!!)).toString()

                            purchaseViewModel?.discountAmtPer?.observe(
                                viewLifecycleOwner,
                                Observer { discountAmtPer ->
                                    if (!discountAmtPer.isNullOrEmpty()) {
                                        this.discountAmtPer = discountAmtPer
                                        binding.etDiscountPercentage.text = "$discountAmtPer%"
                                        this.discountAmt =
                                            ((productQnty.toInt().times(unitPrice.toDoubleOrNull()!!) * discountAmtPer.toDoubleOrNull()!!) / 100).toString()

                                        binding.discountAmount.text = ": " + String.format(
                                            "%, .2f",
                                            (((productQnty.toInt().times(unitPrice.toDoubleOrNull()!!) * discountAmtPer.toDoubleOrNull()!!) / 100))
                                        )

                                        binding.payableAmount.text = ": " + String.format(
                                            "%, .2f",
                                            ((productQnty.toInt().times(unitPrice.toDoubleOrNull()!!)) - (((productQnty.toInt().times(
                                                unitPrice.toDoubleOrNull()!!
                                            ) * discountAmtPer.toDoubleOrNull()!!) / 100)))
                                        )

                                        this.payableAmt =
                                            ((productQnty.toInt().times(unitPrice.toDoubleOrNull()!!)) - (((productQnty.toInt().times(
                                                unitPrice.toDoubleOrNull()!!
                                            ) * discountAmtPer.toDoubleOrNull()!!) / 100))).toString()
                                    }
                                })

                        }
                    })

            }
        })

        purchaseViewModel?.code?.observe(viewLifecycleOwner, Observer { code ->
            if (!code.isNullOrEmpty()) {
                binding.code.text = ": " + code
            }
        })

        purchaseViewModel?.codeUsable?.observe(viewLifecycleOwner, Observer { codeUsable ->
            if (!codeUsable.isNullOrEmpty()) {
                binding.codeUsable.text = ": " + codeUsable
            }
        })

        purchaseViewModel?.codeUsed?.observe(viewLifecycleOwner, Observer { codeUsed ->
            if (!codeUsed.isNullOrEmpty()) {
                binding.codeUsed.text = ": " + codeUsed
            }
        })
    }

    private fun setHomePageInfo(homePageInfo: HomePageInfo) {
        binding.textViewWalletBalance.text = String.format("%s", AppUtilsClass.getDecimalFormattedValue(
            homePageInfo.getBasicInfo()?.getUserWalletBalance()!!
        )).plus("/-")
    }
}
