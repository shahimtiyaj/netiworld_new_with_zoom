package com.netizen.netiworld.view.fragment.user

import android.app.Application
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController

import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentForgotPasswordThreeBinding
import com.netizen.netiworld.databinding.FragmentForgotPasswordTwoBinding
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.user.FragmentForgotPasswordTwo.Companion.mobileVarify
import com.netizen.netiworld.view.fragment.user.FragmentForgotPasswordTwo.Companion.userInformation
import com.netizen.netiworld.viewModel.UserViewModel
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_trabsfer_otp.view.*

class FragmentForgotPasswordThree : Fragment() {

    private lateinit var binding: FragmentForgotPasswordThreeBinding
    private var userViewModel: UserViewModel? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userViewModel = ViewModelProvider(
            this,
            UserViewModel.SignInViewModelFactory(context!!.applicationContext as Application)
        ).get(UserViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_forgot_password_three,
            container,
            false
        )

        initViews()
        initObservers()
        timer()
        return binding.root
    }

    private fun initViews() {
        binding.next.setOnClickListener {
            if (binding.pinView.text.isNullOrEmpty()) {
                Toasty.error(context!!, "OTP can't Empty !", Toasty.LENGTH_LONG).show()
            } else {
                userViewModel?.getOTPVerifyGetForgotPassword(binding.pinView.text.toString())
            }
        }

        binding.resend.setOnClickListener {
            userViewModel?.getOtpForgotPassword(mobileVarify, userInformation)
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.success.value = null
                timer()
            }
        })

        Loaders.apiErrorOTPInvalid.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiErrorOTPInvalid.value = null
            }
        })

        Loaders.apiSuccessforOTPVarify.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiSuccessforOTPVarify.value = null
               // findNavController().popBackStack(R.id.fragmentForgotPasswordThree, true)
                findNavController().navigate(R.id.action_fragmentForgotPasswordThree_to_fragmentForgotPasswordFour)
            }
        })
    }


    fun timer() {
        val timer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.timeCount.text =
                    "OTP expired on after " + millisUntilFinished / 1000 + " seconds"
                binding.resend.visibility = View.GONE
                binding.allReadyHaveAPassword.visibility = View.GONE
            }

            override fun onFinish() {
                binding.timeCount.text = "(0)"
                binding.resend.visibility = View.VISIBLE
                binding.allReadyHaveAPassword.visibility = View.VISIBLE
            }
        }
        timer.start()
    }
}
