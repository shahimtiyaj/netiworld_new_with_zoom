package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile

import android.annotation.SuppressLint
import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.netizen.netiworld.R
import com.netizen.netiworld.adapter.myPoint.*
import com.netizen.netiworld.databinding.FragmentMyProfileBinding
import com.netizen.netiworld.model.*
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.BankAccountViewModel
import com.netizen.netiworld.viewModel.ProfileViewModel
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.personal_info_layout.view.*

/**
 * A simple [Fragment] subclass.
 */
class MyProfileFragment : Fragment(), BankInfoAdapter.OnEditClickListener,
    EducationInfoAdapter.OnEditClickListener,
    CertificationAdapter.OnEditClickListener, ExperienceInfoAdapter.OnEditClickListener,
    ReferenceInfoAdapter.OnEditClickListener, TrainingInfoAdapter.OnEditClickListener {

    private lateinit var binding: FragmentMyProfileBinding
    private lateinit var profileViewModel: ProfileViewModel
    private lateinit var bankAccountViewModel: BankAccountViewModel

    private var tagList = ArrayList<TagGetData>()
    private var educationInfoList = ArrayList<EducationInfo>()
    private var trainingInfoList = ArrayList<TrainingInfo>()
    private var certificationInfoList = ArrayList<CertificationInfo>()
    private var experienceInfoList = ArrayList<ExperienceInfo>()
    private var referenceInfoList = ArrayList<ReferenceInfo>()

    private var spinnerPosition = 0

    private var profileInformation = ProfileInformation()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_my_profile,
            container,
            false
        )

        initViews()
        initObservers()
        profileViewModel.getProfileInfo()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        profileViewModel = ViewModelProvider(
            activity!!,
            ProfileViewModel.ProfileViewModelFactory(context!!.applicationContext as Application)
        ).get(ProfileViewModel::class.java)

        bankAccountViewModel = ViewModelProvider(
            activity!!,
            BankAccountViewModel.BankAccountViewModelFactory(context!!.applicationContext as Application)
        ).get(BankAccountViewModel::class.java)
    }

    override fun onEditClick(tagGetData: TagGetData) {
        val action = MyProfileFragmentDirections.actionMyProfileFragmentToAddBankInfoFragment(
            tagGetData,
            "Edit Bank Info"
        )
        findNavController().navigate(action)
    }

    override fun onEditClick(educationInfo: EducationInfo) {
        val action =
            MyProfileFragmentDirections.actionMyProfileFragmentToAddAcademicEducationFragment(
                educationInfo,
                "Edit Academic Education Info"
            )
        findNavController().navigate(action)
    }

    override fun onEditClick(certificationInfo: CertificationInfo) {
        val action =
            MyProfileFragmentDirections.actionMyProfileFragmentToAddProfessionalQualificationInfoFragment(
                certificationInfo,
                "Edit Certification Info"
            )
        findNavController().navigate(action)
    }

    override fun onEditClick(experienceInfo: ExperienceInfo) {
        val action =
            MyProfileFragmentDirections.actionMyProfileFragmentToAddExperienceInfoFragment(
                experienceInfo,
                "Edit Experience Info"
            )
        findNavController().navigate(action)
    }

    override fun onEditClick(trainingInfo: TrainingInfo) {
        val action =
            MyProfileFragmentDirections.actionMyProfileFragmentToAddTrainingInfoFragment(
                trainingInfo,
                "Edit Training Info"
            )
        findNavController().navigate(action)
    }

    override fun onEditClick(referenceInfo: ReferenceInfo) {
        val action =
            MyProfileFragmentDirections.actionMyProfileFragmentToAddReferenceInfoFragment(
                referenceInfo,
                "Edit Reference Info"
            )
        findNavController().navigate(action)
    }

    private fun onProfileInfoEditClick(profileInformation: ProfileInformation) {
        val action =
            MyProfileFragmentDirections.actionMyProfileFragmentToUpdatePersonalInfoFragment(
                profileInformation,
                "Change Personal Info"
            )
        findNavController().navigate(action)
    }

    private fun onAddressInfoEditClick(profileInformation: ProfileInformation) {
        val action =
            MyProfileFragmentDirections.actionMyProfileFragmentToUpdateAddressInfoFragment(
                profileInformation,
                "Change Address Info"
            )
        findNavController().navigate(action)
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "My Profile")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.spinnerProfileInfoSelection.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                @SuppressLint("RestrictedApi")
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    spinnerPosition = position

                    if (position == 0) {
                        binding.layoutPersonalInfo.visibility = View.VISIBLE
                        binding.recyclerViewProfileInfo.visibility = View.GONE
                        binding.floatingButton.visibility = View.GONE
                    } else {
                        binding.layoutPersonalInfo.visibility = View.GONE
                        binding.recyclerViewProfileInfo.visibility = View.VISIBLE
                        binding.floatingButton.visibility = View.VISIBLE
                    }

                    when (position) {
                        1 -> {
                            if (tagList.isNullOrEmpty()) {
                                bankAccountViewModel.getTagList()
                            } else {
                                setBankInfo(tagList)
                            }
                        }
                        2 -> {
                            if (educationInfoList.isNullOrEmpty()) {
                                profileViewModel.getEducationInfo()
                            } else {
                                setEducationInfo(educationInfoList)
                            }
                        }
                        3 -> {
                            if (trainingInfoList.isNullOrEmpty()) {
                                profileViewModel.getTrainingInfo()
                            } else {
                                setTrainingInfo(trainingInfoList)
                            }
                        }
                        4 -> {
                            if (certificationInfoList.isNullOrEmpty()) {
                                profileViewModel.getCertificationInfo()
                            } else {
                                setCertificationInfo(certificationInfoList)
                            }
                        }
                        5 -> {
                            if (experienceInfoList.isNullOrEmpty()) {
                                profileViewModel.getExperienceInfo()
                            } else {
                                setExperienceInfo(experienceInfoList)
                            }
                        }
                        6 -> {
                            if (referenceInfoList.isNullOrEmpty()) {
                                profileViewModel.getReferenceInfo()
                            } else {
                                setReferenceInfo(referenceInfoList)
                            }
                        }
                    }
                }
            }

        binding.layoutPersonalInfo.image_view_personal_info_edit.setOnClickListener {
            onProfileInfoEditClick(profileInformation)
        }

        binding.layoutPersonalInfo.image_view_address_info_edit.setOnClickListener {
            // findNavController().navigate(R.id.action_myProfileFragment_to_updateAddressInfoFragment)
            onAddressInfoEditClick(profileInformation)
        }


        binding.layoutPersonalInfo.image_view_user_info_edit.setOnClickListener {
            // findNavController().navigate(R.id.action_myProfileFragment_to_updateUserInfoFragment)
        }

        binding.floatingButton.setOnClickListener {
            when (spinnerPosition) {
                1 -> {
                    findNavController().navigate(R.id.action_myProfileFragment_to_addBankInfoFragment)
                }
                2 -> {
                    findNavController().navigate(R.id.action_myProfileFragment_to_addAcademicEducationFragment)
                }
                3 -> {
                    findNavController().navigate(R.id.action_myProfileFragment_to_addTrainingInfoFragment)
                }
                4 -> {
                    findNavController().navigate(R.id.action_myProfileFragment_to_addProfessionalQualificationInfoFragment)
                }
                5 -> {
                    findNavController().navigate(R.id.action_myProfileFragment_to_addExperienceInfoFragment)
                }
                6 -> {
                    findNavController().navigate(R.id.action_myProfileFragment_to_addReferenceInfoFragment)
                }
            }
        }
    }

    private fun initObservers() {

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        profileViewModel.profileInformation.observe(viewLifecycleOwner, Observer {
            setPersonalInformation(it)
            profileInformation = it
            // onProfileInfoEditClick(it)
        })

        bankAccountViewModel.tagList.observe(viewLifecycleOwner, Observer {
            tagList = it as ArrayList<TagGetData>
            setBankInfo(it)
        })

        profileViewModel.educationInfoList.observe(viewLifecycleOwner, Observer {
            educationInfoList = it as ArrayList<EducationInfo>
            setEducationInfo(it)
        })

        profileViewModel.trainingInfoList.observe(viewLifecycleOwner, Observer {
            trainingInfoList = it as ArrayList<TrainingInfo>
            setTrainingInfo(it)
        })

        profileViewModel.certificationInfoList.observe(viewLifecycleOwner, Observer {
            certificationInfoList = it as ArrayList<CertificationInfo>
            setCertificationInfo(it)
        })

        profileViewModel.experienceInfoList.observe(viewLifecycleOwner, Observer {
            experienceInfoList = it as ArrayList<ExperienceInfo>
            setExperienceInfo(it)
        })

        profileViewModel.referenceInfoList.observe(viewLifecycleOwner, Observer {
            referenceInfoList = it as ArrayList<ReferenceInfo>
            setReferenceInfo(it)
        })
    }


    private fun setPersonalInformation(profileInformation: ProfileInformation) {
        binding.textViewNetiId.text =
            "Neti ID: " + profileInformation.getCustomNetiID().toString()
        binding.textViewUserName.text = profileInformation.getFullName()
        binding.textViewMobileNo.text = profileInformation.getBasicMobile().toString()
        binding.layoutPersonalInfo.text_view_father_name.text =
            profileInformation.getUserDetailsInfoResponseDTO()?.getFatherName()
        binding.layoutPersonalInfo.text_view_mother_name.text =
            profileInformation.getUserDetailsInfoResponseDTO()?.getMotherName()
        binding.layoutPersonalInfo.text_view_gender.text = profileInformation.getGender()
        binding.layoutPersonalInfo.text_view_religion.text = profileInformation.getReligion()
        binding.layoutPersonalInfo.text_view_date_of_birth.text =
            profileInformation.getDateOfBirth().toString()
        binding.layoutPersonalInfo.text_view_age.text = profileInformation.getAge().toString()
        binding.layoutPersonalInfo.text_view_blood_group.text =
            profileInformation.getBloodGroup().toString()
        binding.layoutPersonalInfo.text_view_marital_status.text =
            profileInformation.getUserDetailsInfoResponseDTO()?.getMaritalStatus()
        binding.layoutPersonalInfo.text_view_number_of_child.text =
            profileInformation.getUserDetailsInfoResponseDTO()?.getNumberOfChild().toString()
        binding.layoutPersonalInfo.text_view_nationality.text =
            profileInformation.getUserDetailsInfoResponseDTO()?.getNationality()
        binding.layoutPersonalInfo.text_view_national_id.text =
            profileInformation.getUserDetailsInfoResponseDTO()?.getNationalID().toString()
        binding.layoutPersonalInfo.text_view_passport_no.text =
            profileInformation.getUserDetailsInfoResponseDTO()?.getPassportNo().toString()
        binding.layoutPersonalInfo.text_view_birth_certification.text =
            profileInformation.getUserDetailsInfoResponseDTO()?.getBirthCertificateNo()
                .toString()
        binding.layoutPersonalInfo.text_view_hobby.text =
            profileInformation.getHobby().toString()
        binding.layoutPersonalInfo.text_view_address_details.text =
            profileInformation.getUserDetailsInfoResponseDTO()?.getAddressDetails()
        binding.layoutPersonalInfo.text_view_upazilla.text = profileInformation.getUpazilla()
        binding.layoutPersonalInfo.text_view_district.text = profileInformation.getDistrict()
        binding.layoutPersonalInfo.text_view_division.text = profileInformation.getDivision()
        binding.layoutPersonalInfo.text_view_longitude.text =
            profileInformation.getUserDetailsInfoResponseDTO()?.getLongitude().toString()
        binding.layoutPersonalInfo.text_view_latitude.text =
            profileInformation.getUserDetailsInfoResponseDTO()?.getLatitude().toString()
        binding.layoutPersonalInfo.text_view_user_name.text = profileInformation.getUsername()
//        binding.layoutPersonalInfo.change_password.text = profileInformation.getUserDetailsInfoResponseDTO()?.getFatherName()
        binding.layoutPersonalInfo.text_view_registration_date.text =
            profileInformation.getRegistrationDate().toString()
        binding.layoutPersonalInfo.text_view_registration_from.text =
            profileInformation.getRegisterFrom().toString()
        binding.layoutPersonalInfo.text_view_point.text =
            profileInformation.getNumOfPoint().toString()
    }

    private fun setBankInfo(tagList: List<TagGetData>) {
        binding.recyclerViewProfileInfo.layoutManager = LinearLayoutManager(context)
        binding.recyclerViewProfileInfo.setHasFixedSize(true)
        binding.recyclerViewProfileInfo.adapter =
            BankInfoAdapter(
                context!!,
                tagList,
                this
            )
    }

    private fun setEducationInfo(educationInfoList: List<EducationInfo>) {
        binding.recyclerViewProfileInfo.layoutManager = LinearLayoutManager(context)
        binding.recyclerViewProfileInfo.setHasFixedSize(true)
        binding.recyclerViewProfileInfo.adapter =
            EducationInfoAdapter(
                context!!,
                educationInfoList,
                this
            )
    }

    private fun setTrainingInfo(trainingInfoList: List<TrainingInfo>) {
        binding.recyclerViewProfileInfo.layoutManager = LinearLayoutManager(context)
        binding.recyclerViewProfileInfo.setHasFixedSize(true)
        binding.recyclerViewProfileInfo.adapter =
            TrainingInfoAdapter(
                context!!,
                trainingInfoList,
                this
            )
    }

    private fun setCertificationInfo(certificationInfoList: List<CertificationInfo>) {
        binding.recyclerViewProfileInfo.layoutManager = LinearLayoutManager(context)
        binding.recyclerViewProfileInfo.setHasFixedSize(true)
        binding.recyclerViewProfileInfo.adapter =
            CertificationAdapter(
                context!!,
                certificationInfoList,
                this
            )
    }

    private fun setExperienceInfo(experienceInfoList: List<ExperienceInfo>) {
        binding.recyclerViewProfileInfo.layoutManager = LinearLayoutManager(context)
        binding.recyclerViewProfileInfo.setHasFixedSize(true)
        binding.recyclerViewProfileInfo.adapter =
            ExperienceInfoAdapter(
                context!!,
                experienceInfoList,
                this
            )
    }

    private fun setReferenceInfo(referenceInfoList: List<ReferenceInfo>) {
        binding.recyclerViewProfileInfo.layoutManager = LinearLayoutManager(context)
        binding.recyclerViewProfileInfo.setHasFixedSize(true)
        binding.recyclerViewProfileInfo.adapter =
            ReferenceInfoAdapter(
                context!!,
                referenceInfoList,
                this
            )
    }

}


