package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentUpdateAddressInfoBinding
import com.netizen.netiworld.model.ProfileInformation
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.SearchSpinnerDialogFragment
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.ProfileViewModel
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class UpdateAddressInfoFragment : Fragment() {

    private lateinit var binding: FragmentUpdateAddressInfoBinding
    private var title = "Update Address Info"
    private lateinit var profileViewModel: ProfileViewModel
    private var netiId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        profileViewModel = ViewModelProvider(
            activity!!,
            ProfileViewModel.ProfileViewModelFactory(context!!.applicationContext as Application)
        ).get(ProfileViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_update_address_info,
            container,
            false
        )

        if (!arguments?.getString("title").isNullOrEmpty()) {
            title = arguments?.getString("title").toString()
            setData(arguments?.get("addressData") as ProfileInformation)
        }

        initViews()
        initObservers()
        profileViewModel.getDivision()

        return binding.root
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Update Address Info")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.editTextDivision.setOnClickListener {
            showSearchSpinnerDialog("profile", "division", 1)
        }

        binding.editTextDistrict.setOnClickListener {
            profileViewModel.getDistrict()
            showSearchSpinnerDialog("profile", "district", 2)
        }

        binding.editTextUpazilla.setOnClickListener {
            profileViewModel.getUpazilla()
            showSearchSpinnerDialog("profile", "upazilla", 3)
        }

        binding.buttonSave.setOnClickListener {
            profileViewModel.checkUpdateAddressInfo(
                netiId.toString(),
                binding.editTextAddressDetails.text.toString(),
                binding.editTextDivision.text.toString(),
                binding.editTextDistrict.text.toString(),
                binding.editTextUpazilla.text.toString(),
                binding.editTextLatitude.text.toString(),
                binding.editTextLongitude.text.toString()
            )
        }
    }

    private fun initObservers() {

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                showToastyError(it)
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
                //clearFields()
            }
        })

        Loaders.searchValue1.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                profileViewModel.getDivisionId(it)
                binding.editTextDivision.setText(it)
            }
        })

        Loaders.searchValue2.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                profileViewModel.getDistrictId(it)
                binding.editTextDistrict.setText(it)
            }
        })

        Loaders.searchValue3.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                profileViewModel.getUpazillaId(it)
                binding.editTextUpazilla.setText(it)
            }
        })

        profileViewModel.isAddressDetailsEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Address Details cant't left empty!")
                profileViewModel.isAddressDetailsEmpty.value = false
            }
        })

        profileViewModel.isDivisionEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Division cant't left empty!")
                profileViewModel.isDivisionEmpty.value = false
            }
        })

        profileViewModel.isDistrictEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("District cant't left empty!")
                profileViewModel.isDistrictEmpty.value = false
            }
        })

        profileViewModel.isUpazillaEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Upazilla cant't left empty!")
                profileViewModel.isUpazillaEmpty.value = false
            }
        })

    }

    private fun setData(profileInformation: ProfileInformation) {
        netiId = profileInformation.getNetiID()!!

        binding.editTextAddressDetails.setText(
            profileInformation.getUserDetailsInfoResponseDTO()?.getAddressDetails()
        )
        binding.editTextDivision.setText(profileInformation.getDivision())
        binding.editTextDistrict.setText(profileInformation.getDistrict())
        binding.editTextUpazilla.setText(profileInformation.getUpazilla())
        binding.editTextLongitude.setText(
            profileInformation.getUserDetailsInfoResponseDTO()?.getLongitude()
        )
        binding.editTextLatitude.setText(
            profileInformation.getUserDetailsInfoResponseDTO()?.getLatitude()
        )

    }

    private fun showSearchSpinnerDialog(dataType: String, title: String, spinnerType: Int) {
        val bundle = Bundle()
        bundle.putString("dataType", dataType)
        bundle.putString("title", title)
        bundle.putInt("spinnerType", spinnerType)

        val searchSpinnerFragment = SearchSpinnerDialogFragment()
        searchSpinnerFragment.arguments = bundle
        searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
    }

    private fun showToastyError(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_LONG).show()
    }
}
