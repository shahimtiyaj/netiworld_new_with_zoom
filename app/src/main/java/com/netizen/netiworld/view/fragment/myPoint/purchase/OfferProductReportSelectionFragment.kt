package com.netizen.netiworld.view.fragment.myPoint.purchase

import android.app.Application
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentOfferProductReportSelectionBinding
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.PurchaseViewModel
import es.dmoral.toasty.Toasty
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class OfferProductReportSelectionFragment : Fragment() {

    private lateinit var binding: FragmentOfferProductReportSelectionBinding
    private lateinit var purchaseViewModel: PurchaseViewModel

    private var calendar = Calendar.getInstance()
    private var dateSelection: Int? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_offer_product_report_selection,
            container,
            false
        )

        initViews()
        initObservers()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        purchaseViewModel = ViewModelProvider(
            activity!!,
            PurchaseViewModel.PurchaseViewModelFactory(context!!.applicationContext as Application)
        ).get(PurchaseViewModel::class.java)
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Offer Product")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.editTextFromDate.setOnClickListener {
            dateSelection = 1
            showDatePicker()
        }

        binding.editTextToDate.setOnClickListener {
            dateSelection = 2
            showDatePicker()
        }

        binding.buttonSearch.setOnClickListener {
            purchaseViewModel.checkOfferProductReportDate(
                binding.editTextFromDate.text.toString(),
                binding.editTextToDate.text.toString()
            )
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.error(context!!, it.toString(), Toasty.LENGTH_LONG).show()
                Loaders.apiError.value = null
            }
        })

        purchaseViewModel.isFromDateEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                Toasty.error(context!!, "From date is empty!", Toasty.LENGTH_LONG).show()
                purchaseViewModel.isFromDateEmpty.value = false
            }
        })

        purchaseViewModel.isToDateEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                Toasty.error(context!!, "To date is empty!", Toasty.LENGTH_LONG).show()
                purchaseViewModel.isToDateEmpty.value = false
            }
        })

        purchaseViewModel.isDataFound.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().navigate(R.id.action_offerProductReportSelectionFragment_to_offerProductReportFragment)
                purchaseViewModel.isDataFound.value = false
            }
        })
    }

    private fun showDatePicker() {
        val datePickerDialog = DatePickerDialog(context!!, dateSetListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH))

        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()
    }

    private val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, monthOfYear)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
//      val dateFormat = SimpleDateFormat(myFormat)

        when(dateSelection) {
            1 -> {
                binding.editTextFromDate.setText(dateFormat.format(calendar.time))
            }
            2 -> {
                binding.editTextToDate.setText(dateFormat.format(calendar.time))
            }
        }
    }
}
