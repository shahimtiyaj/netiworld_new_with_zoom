package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentAddReferenceInfoBinding
import com.netizen.netiworld.model.ReferenceInfo
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.SearchSpinnerDialogFragment
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.ProfileViewModel
import es.dmoral.toasty.Toasty

class AddReferenceInfoFragment : Fragment() {

    private lateinit var binding: FragmentAddReferenceInfoBinding
    private lateinit var profileViewModel: ProfileViewModel
    private var title = "Add Reference Info"
    private var isAdd = true
    private var isUpdate = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        profileViewModel = ViewModelProvider(
            activity!!,
            ProfileViewModel.ProfileViewModelFactory(context!!.applicationContext as Application)
        ).get(ProfileViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_add_reference_info,
            container,
            false
        )

        if (!arguments?.getString("title").isNullOrEmpty()) {
            isAdd = false
            isUpdate = true
            title = arguments?.getString("title").toString()
            setData(arguments?.get("referenceData") as ReferenceInfo)
        }

        initViews()

        initObservers()

        profileViewModel.getRelation()

        return binding.root
    }

    private fun initViews() {

        val bundle = Bundle()
        bundle.putString("title", "Add Reference Info")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.editTextRelation.setOnClickListener {
            showSearchSpinnerDialog("profile", "relation", 9)
        }

        binding.buttonSave.setOnClickListener {
            when {
                isAdd -> profileViewModel.checkReferenceInfo(
                    binding.editTextName.text.toString(),
                    binding.editTextDesignation.text.toString(),
                    binding.editTextOrganization.text.toString(),
                    binding.editTextMobile.text.toString(),
                    binding.editTextEmail.text.toString(),
                    binding.editTextAddress.text.toString()
                )
                isUpdate -> profileViewModel.checkUpdateReferenceInfo(
                    binding.editTextName.text.toString(),
                    binding.editTextDesignation.text.toString(),
                    binding.editTextOrganization.text.toString(),
                    binding.editTextMobile.text.toString(),
                    binding.editTextEmail.text.toString(),
                    binding.editTextAddress.text.toString()
                )
            }

        }
    }

    private fun initObservers() {

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                showToastyError(it)
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
                clearFields()
            }
        })

        Loaders.searchValue9.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                profileViewModel.getRelationId(it)
                binding.editTextRelation.setText(it)
            }
        })

        profileViewModel.isReferenceNameEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Name can't left empty!")
            }
        })

        profileViewModel.isReferenceDesignationEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Designation can't left empty!")
            }
        })

        profileViewModel.isReferenceOrganizationNameEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Organization can't left empty!")
            }
        })

        profileViewModel.isReferenceMobileEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Phone no. can't left empty!")
            }
        })

        profileViewModel.isReferenceEmailEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Email can't left empty!")
            }
        })

        profileViewModel.isReferenceAddressEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Address can't left empty!")
            }
        })

        profileViewModel.isRelationEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Relation can't left empty!")
            }
        })
    }

    private fun showSearchSpinnerDialog(dataType: String, title: String, spinnerType: Int) {
        val bundle = Bundle()
        bundle.putString("dataType", dataType)
        bundle.putString("title", title)
        bundle.putInt("spinnerType", spinnerType)

        val searchSpinnerFragment = SearchSpinnerDialogFragment()
        searchSpinnerFragment.arguments = bundle
        searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
    }

    private fun setData(referenceInfo: ReferenceInfo) {
        binding.editTextName.setText(referenceInfo.getReferenceName())
        binding.editTextDesignation.setText(referenceInfo.getReferenceDesignation())
        binding.editTextOrganization.setText(referenceInfo.getOrgainizationName())
        binding.editTextMobile.setText(referenceInfo.getReferenceMobile())
        binding.editTextEmail.setText(referenceInfo.getReferenceEmail())
        binding.editTextAddress.setText(referenceInfo.getReferenceAddress())
        binding.editTextRelation.setText(referenceInfo.getRelationInfoDTO()?.getCategoryName())
    }

    private fun clearFields() {
        binding.editTextName.text = null
        binding.editTextDesignation.text = null
        binding.editTextOrganization.text = null
        binding.editTextMobile.text = null
        binding.editTextEmail.text = null
        binding.editTextAddress.text = null
        binding.editTextRelation.text = null
    }


    private fun showToastyError(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_LONG).show()
    }

    companion object
}
