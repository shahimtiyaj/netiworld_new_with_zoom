package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile

import android.app.Application
import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentUpdatePersonalInfoBinding
import com.netizen.netiworld.model.ProfileInformation
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.SearchSpinnerDialogFragment
import com.netizen.netiworld.view.fragment.ToolBarFragment
import com.netizen.netiworld.viewModel.ProfileViewModel
import es.dmoral.toasty.Toasty
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class UpdatePersonalInfoFragment : Fragment() {

    private lateinit var binding: FragmentUpdatePersonalInfoBinding
    private var title = "Update Personal Info"
    private var gender: String? = null
    private var maritalStatus: String? = null
    private lateinit var profileViewModel: ProfileViewModel
    private var netiId = 0
    private var calendar = Calendar.getInstance()
    private var dateSelection: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        profileViewModel = ViewModelProvider(
            activity!!,
            ProfileViewModel.ProfileViewModelFactory(context!!.applicationContext as Application)
        ).get(ProfileViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_update_personal_info,
            container,
            false
        )

        // setData(arguments?.get("personalData") as ProfileInformation)

        if (!arguments?.getString("title").isNullOrEmpty()) {
            title = arguments?.getString("title").toString()
            setData(arguments?.get("personalData") as ProfileInformation)
        }

        initViews()
        initObservers()

        profileViewModel.getBloodGroup()

        return binding.root
    }

    private fun initViews() {
        val bundle = Bundle()
        bundle.putString("title", "Update Personal Info")

        val toolBarFragment = ToolBarFragment()
        toolBarFragment.arguments = bundle

        val fragmentTransaction = activity!!.supportFragmentManager.beginTransaction()
        fragmentTransaction.add(binding.fragmentContainer.id, toolBarFragment)
        fragmentTransaction.commit()

        binding.editTextBloodGroup.setOnClickListener {
            showSearchSpinnerDialog("profile", "blood", 10)
        }

        binding.rdGroup.setOnCheckedChangeListener { group, checkedId ->
            when {
                binding.rdbMale.isChecked -> {
                    gender = "Male"
                    Log.d("Gender", gender)
                }
                binding.rdbFemale.isChecked -> {
                    gender = "Female"
                    Log.d("Gender", gender)
                }
                else -> {
                    gender = "Others"
                }
            }
        }

        binding.rdMaritalGroup.setOnCheckedChangeListener { group, checkedId ->
            when {
                binding.rdbMarried.isChecked -> {
                    maritalStatus = "Married"
                    Log.d("Marital Status", maritalStatus)
                }
                binding.rdbUmaried.isChecked -> {
                    maritalStatus = "Unmarried"
                    Log.d("Marital Status", maritalStatus)
                }
            }
        }

        binding.buttonSave.setOnClickListener {

            profileViewModel.checkUpdatePersonalInfo(
                netiId.toString(),
                binding.editTextFatherName.text.toString(),
                binding.editTextMotherName.text.toString(),
                binding.editTextPhoneNo.text.toString(),
                binding.editTextDateOfBirth.text.toString(),
                gender,
                binding.editTextBloodGroup.text.toString(),
                maritalStatus,
                binding.editTextNumberOfChild.text.toString(),
                binding.editTextNationality.text.toString(),
                binding.editTextNationalId.text.toString(),
                binding.editTextPassportNo.text.toString(),
                binding.editTextBirthCertificationNo.text.toString()

            )

        }

        binding.editTextDateOfBirth.setOnClickListener {
            showDatePicker()
        }
    }

    private fun initObservers() {

        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.apiError.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                showToastyError(it)
                Loaders.apiError.value = null
            }
        })

        Loaders.apiSuccess.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.apiSuccess.value = null
                clearFields()
            }
        })

        Loaders.searchValue10.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                profileViewModel.getBloodGroupId(it)
                binding.editTextBloodGroup.setText(it)
            }
        })

        profileViewModel.isFatherNameEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Father's Name can't left empty!")
                profileViewModel.isFatherNameEmpty.value = false
            }
        })

        profileViewModel.isMotherNameEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Mother's Name can't left empty!")
                profileViewModel.isMotherNameEmpty.value = false
            }
        })

        profileViewModel.isMobileEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Mobile No. can't left empty!")
                profileViewModel.isMobileEmpty.value = false
            }
        })

        profileViewModel.isBirthDayEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Date of birth cant't left empty!")
                profileViewModel.isBirthDayEmpty.value = false
            }
        })

        /* profileViewModel.isBloodEmpty.observe(viewLifecycleOwner, Observer {
             if (it) {
                 showToastyError("Blood Group cant't left empty!")
                 profileViewModel.isBloodEmpty.value = false
             }
         })*/

        profileViewModel.isBloodGroupEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Blood Group cant't left empty!")
                profileViewModel.isBloodGroupEmpty.value = false
            }
        })

        profileViewModel.isMaritalStatusEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Marital Status cant't left empty!")
                profileViewModel.isMaritalStatusEmpty.value = false
            }
        })

        profileViewModel.isNumberOfChildEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Number of child cant't left empty!")
                profileViewModel.isNumberOfChildEmpty.value = false
            }
        })

        profileViewModel.isNationalityEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Nationality cant't left empty!")
                profileViewModel.isNationalityEmpty.value = false
            }
        })

        profileViewModel.isNationalIdEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("National Id cant't left empty!")
                profileViewModel.isNationalIdEmpty.value = false
            }
        })

        profileViewModel.isPassportEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Passport cant't left empty!")
                profileViewModel.isPassportEmpty.value = false
            }
        })

        profileViewModel.isBirthCertificationEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Birth Certification cant't left empty!")
                profileViewModel.isBirthCertificationEmpty.value = false
            }
        })

        profileViewModel.isNetiIdEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showToastyError("Neti ID cant't left empty!")
                profileViewModel.isNetiIdEmpty.value = false
            }
        })
    }

    private fun setData(profileInformation: ProfileInformation) {
        netiId = profileInformation.getNetiID()!!

        binding.editTextFatherName.setText(
            profileInformation.getUserDetailsInfoResponseDTO()?.getFatherName()
        )
        binding.editTextMotherName.setText(
            profileInformation.getUserDetailsInfoResponseDTO()?.getMotherName()
        )
        binding.editTextPhoneNo.setText(profileInformation.getBasicMobile())
        binding.editTextDateOfBirth.setText(profileInformation.getDateOfBirth())

        when {
            profileInformation.getGender().equals("Male") -> {
                binding.rdbMale.isChecked = true
            }
            profileInformation.getGender().equals("Female") -> {
                binding.rdbFemale.isChecked = true
            }
            else -> {
                binding.rdbOthers.isChecked = true
            }
        }
        when {
            profileInformation.getUserDetailsInfoResponseDTO()?.getMaritalStatus()
                .equals("Married") -> {
                binding.rdbMarried.isChecked = true
            }
            profileInformation.getUserDetailsInfoResponseDTO()?.getMaritalStatus()
                .equals("Unmarried") -> {
                binding.rdbUmaried.isChecked = true
            }
        }

        binding.editTextBloodGroup.setText(profileInformation.getBloodGroup())
        // binding.editTextMaritalStatus.setText(profileInformation.getUserDetailsInfoResponseDTO()?.getMaritalStatus())
        binding.editTextNumberOfChild.setText(
            profileInformation.getUserDetailsInfoResponseDTO()?.getNumberOfChild()
        )
        binding.editTextNationality.setText(
            profileInformation.getUserDetailsInfoResponseDTO()?.getNationality()
        )
        binding.editTextNationalId.setText(
            profileInformation.getUserDetailsInfoResponseDTO()?.getNationalID()
        )
        binding.editTextPassportNo.setText(
            profileInformation.getUserDetailsInfoResponseDTO()?.getPassportNo()
        )
        binding.editTextBirthCertificationNo.setText(
            profileInformation.getUserDetailsInfoResponseDTO()?.getBirthCertificateNo()
        )
    }

    private fun clearFields() {
        binding.editTextFatherName.text = null
        binding.editTextMotherName.text = null
        binding.editTextPhoneNo.text = null
        binding.editTextDateOfBirth.text = null
        binding.editTextBloodGroup.text = null
        binding.editTextNumberOfChild.text = null
        binding.editTextNationality.text = null
        binding.editTextNationalId.text = null
        binding.editTextPassportNo.text = null
        binding.editTextBirthCertificationNo.text = null
    }

    private fun showSearchSpinnerDialog(dataType: String, title: String, spinnerType: Int) {
        val bundle = Bundle()
        bundle.putString("dataType", dataType)
        bundle.putString("title", title)
        bundle.putInt("spinnerType", spinnerType)

        val searchSpinnerFragment = SearchSpinnerDialogFragment()
        searchSpinnerFragment.arguments = bundle
        searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
    }

    private fun showToastyError(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_LONG).show()
    }

    private fun showDatePicker() {
        val datePickerDialog = DatePickerDialog(
            context!!, dateSetListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )

        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()
    }

    private val dateSetListener =
        DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            binding.editTextDateOfBirth.setText(dateFormat.format(calendar.time))
        }
}


