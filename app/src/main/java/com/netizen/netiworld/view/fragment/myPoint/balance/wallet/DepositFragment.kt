package com.netizen.netiworld.view.fragment.myPoint.balance.wallet

import android.app.Activity
import android.app.Application
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.R
import com.netizen.netiworld.databinding.FragmentDepositBinding
import com.netizen.netiworld.utils.AppUtilsClass
import com.netizen.netiworld.utils.Loaders
import com.netizen.netiworld.view.fragment.SearchSpinnerDialogFragment
import com.netizen.netiworld.viewModel.TokenViewModel
import com.netizen.netiworld.viewModel.WalletViewModel
import es.dmoral.toasty.Toasty
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class DepositFragment : Fragment() {

    companion object {
        private const val IMAGE_PICK_CODE = 1000
        private const val PERMISSION_CODE_READ = 1001
        private const val PERMISSION_CODE_WRITE = 1002
    }

    private lateinit var binding: FragmentDepositBinding
    private lateinit var walletViewModel: WalletViewModel
    private lateinit var tokenViewModel: TokenViewModel

    private var calendar = Calendar.getInstance()
    private var imageBase64: String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_deposit,
            container,
            false
        )

        initViews()
        initObservers()
        walletViewModel.getAccountInfoList()

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        walletViewModel = ViewModelProvider(activity!!,
            WalletViewModel.WalletViewModelFactory(context!!.applicationContext as Application))
            .get(WalletViewModel::class.java)

        tokenViewModel = ViewModelProvider(activity!!,
            TokenViewModel.TokenViewModelFactory(context!!.applicationContext as Application))
            .get(TokenViewModel::class.java)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
//            binding.imageViewUpload.setImageURI(data?.data)
            val bitmapImage = MediaStore.Images.Media.getBitmap(activity!!.contentResolver, data?.data)
//            binding.imageViewUpload.visibility = View.VISIBLE
//            binding.textViewUpload.visibility = View.GONE
            imageBase64 = bitMapToString(bitmapImage)
            tokenViewModel.getFileName(context!!, data?.data!!)
//            tokenViewModel.getRealPathFromURI(context!!, data.data!!)
        }
    }

    private fun initViews() {
        binding.editTextAccountNumber.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("dataType", "wallet")
            bundle.putString("title", "account")
            bundle.putInt("spinnerType", 1)

            val searchSpinnerFragment = SearchSpinnerDialogFragment()
            searchSpinnerFragment.arguments = bundle
            searchSpinnerFragment.show(activity!!.supportFragmentManager, null)
        }

        binding.editTextDepositDate.setOnClickListener {
            showDatePicker()
        }

        var depositType: String? = null
        binding.radioGroupDeposit.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                binding.radioButtonCash.id -> {
                    depositType = "cash"
                }
                binding.radioButtonCheque.id -> {
                    depositType = "cheque"
                }
            }
        }

        binding.editTextMobileBankingNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (!s.toString().startsWith("01", true)) {
                    binding.editTextMobileBankingNumber.error = "Enter valid mobile number."
                }
            }
        })

        binding.editTextAttachment.setOnClickListener {
            if (AppUtilsClass.checkPermissionForImage(activity!!)) {
                pickFilesFromMobileStorage()
            }
        }

        binding.buttonSave.setOnClickListener {
            if (walletViewModel.isMobileBanking.value == true) {
                walletViewModel.checkMobileDepositData(
                    binding.editTextDepositAmount.text.toString(),
                    binding.editTextDepositDate.text.toString(),
                    binding.editTextMobileBankingNumber.text.toString(),
                    binding.editTextTransactionId.text.toString()
                )
            } else {
                walletViewModel.checkAccountDepositData(
                    binding.editTextDepositAmount.text.toString(),
                    binding.editTextDepositDate.text.toString(),
                    binding.editTextBranch.text.toString(),
                    depositType,
                    binding.editTextNote.text.toString(),
                    imageBase64
                )
            }
        }
    }

    private fun initObservers() {
        Loaders.isLoading0.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.lottieProgressbar.visibility = View.VISIBLE
            } else {
                binding.lottieProgressbar.visibility = View.GONE
            }
        })

        Loaders.isLoading1.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding.shimmerAccount.visibility = View.VISIBLE
            } else {
                binding.shimmerAccount.visibility = View.GONE
            }
        })

        Loaders.error.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                showErrorToast(it.toString())
                Loaders.error.value = null
            }
        })

        Loaders.success.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                clearFields()
                Toasty.success(context!!, it, Toasty.LENGTH_LONG).show()
                Loaders.success.value = null
            }
        })

        Loaders.searchValue1.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.editTextAccountNumber.setText(it)
                walletViewModel.checkAccountType(it)
            }
        })

        walletViewModel.isMobileBanking.observe(viewLifecycleOwner, Observer {
            if (it) {
                showMobileBankingLayouts()
                hideAccountLayouts()
            } else {
                showAccountLayouts()
                hideMobileBankingLayouts()
            }
        })

        walletViewModel.isAccountEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("Account number is empty!")
                walletViewModel.isAccountEmpty.value = false
            }
        })

        walletViewModel.isDepositAmountEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("Deposit amount is empty!")
                walletViewModel.isDepositAmountEmpty.value = false
            }
        })

        walletViewModel.isDepositDateEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("Deposit date is empty!")
                walletViewModel.isDepositDateEmpty.value = false
            }
        })

        walletViewModel.isMobileBankingNoEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("Mobile banking number is empty!")
                walletViewModel.isMobileBankingNoEmpty.value = false
            }
        })

        walletViewModel.isTransactionIdEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("Transaction ID is empty!")
                walletViewModel.isTransactionIdEmpty.value = false
            }
        })

        walletViewModel.isBranchEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("Branch is empty!")
                walletViewModel.isBranchEmpty.value = false
            }
        })

        walletViewModel.isDepositTypeEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("Deposit type is empty!")
                walletViewModel.isDepositTypeEmpty.value = false
            }
        })

        walletViewModel.isNoteEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("Note is empty!")
                walletViewModel.isNoteEmpty.value = false
            }
        })

        walletViewModel.isImageEmpty.observe(viewLifecycleOwner, Observer {
            if (it) {
                showErrorToast("Attachment is empty!")
            }
        })

        tokenViewModel.attachmentName.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                binding.editTextAttachment.setText(it)
            }
        })
    }

    private fun pickFilesFromMobileStorage() {
        val galleryIntent = Intent(Intent.ACTION_GET_CONTENT, null)
        galleryIntent.type = "image/*"

        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        val pdfIntent = Intent(Intent.ACTION_GET_CONTENT)
        pdfIntent.type = "*/*"

        val chooser = Intent(Intent.ACTION_CHOOSER)
        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent)
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:")

//        val intentArray = arrayOf(cameraIntent, pdfIntent)
        val intentArray = arrayOf(cameraIntent)
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        startActivityForResult(chooser, IMAGE_PICK_CODE)
    }

    private fun showDatePicker() {
        val datePickerDialog = DatePickerDialog(context!!, dateSetListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH))

        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()
    }

    private val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, monthOfYear)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
//      val dateFormat = SimpleDateFormat(myFormat)
        binding.editTextDepositDate.setText(dateFormat.format(calendar.time))
    }

    private fun bitMapToString(bitmap: Bitmap): String {
        val byteArray = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArray)
        val b = byteArray.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    private fun showMobileBankingLayouts() {
        binding.mobileBankingInputLayout.visibility = View.VISIBLE
        binding.transactionInputLayout.visibility = View.VISIBLE
    }

    private fun hideMobileBankingLayouts() {
        binding.mobileBankingInputLayout.visibility = View.GONE
        binding.transactionInputLayout.visibility = View.GONE
    }

    private fun showAccountLayouts() {
        binding.branchInputLayout.visibility = View.VISIBLE
        binding.layoutDepositType.visibility = View.VISIBLE
        binding.noteInputLayout.visibility = View.VISIBLE
        binding.layoutImage.visibility = View.VISIBLE
    }

    private fun hideAccountLayouts() {
        binding.branchInputLayout.visibility = View.GONE
        binding.layoutDepositType.visibility = View.GONE
        binding.noteInputLayout.visibility = View.GONE
        binding.layoutImage.visibility = View.GONE
    }

    private fun clearFields() {
        binding.editTextAccountNumber.text = null
        binding.editTextDepositAmount.text = null
        binding.editTextDepositDate.text = null
        binding.editTextMobileBankingNumber.text = null
        binding.editTextTransactionId.text = null
        binding.editTextBranch.text = null
        binding.editTextNote.text = null
        binding.editTextAttachment.text = null
    }

    private fun showErrorToast(message: String) {
        Toasty.error(context!!, message, Toasty.LENGTH_LONG).show()
    }
}
