package com.netizen.netiworld.apiService

import com.google.gson.JsonObject
import com.netizen.netiworld.model.*
import com.netizen.netiworld.model.UserPoint.*
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @FormUrlEncoded
    @POST("oauth/token")
    fun postData(@FieldMap params: HashMap<String?, String?>, @HeaderMap headers: HashMap<String?, String?>): Call<PostResponse>

    @GET("user/profile/details")
    fun getUserProfileInfo(@HeaderMap headers: HashMap<String?, String?>): Call<ProfileInformation>

    @GET("user/account/by/corebankid")
    fun getAccountNoInfo(@HeaderMap headers: HashMap<String?, String?>, @Query("coreBankID") localBankCategoryID: String): Call<MobileBankAcNo>

    @GET("user/category/by/type/2nd_parent_type")
    fun getBankAccountList(
        @Header("Authorization") token: String,
        @Query("typeDefaultCode") typeDefaultCode: String
    ): Call<List<BankAccountInfo>>

    @GET("user/message/types/by/point")
    fun getMessageType(@HeaderMap headers: HashMap<String?, String?>, @Query("roleID") code: String): Call<MessageType>

    //-------------------
    @GET("user/message/types/by/point")
    fun getMessageType1(@HeaderMap headers: HashMap<String?, String?>, @Query("roleID") code: String): Call<MessageType1>

    @GET("user/roles/assigned")
    fun getPurchasePoint(@HeaderMap headers: HashMap<String?, String?>): Call<PurchasePoint>

    @GET("user/products/by/role")
    fun getProductName(@HeaderMap headers: HashMap<String?, String?>, @Query("roleID") code: String?): Call<List<Products>>

    @POST("user/balance/deposit")
    fun submitDepositPostData(@HeaderMap headers: HashMap<String?, String?>, @Body obj: DepositPostData): Call<String>

    @POST("user/message/recharge")
    fun messageRechargepostData(@Body obj: MessageRecharge, @HeaderMap headers: HashMap<String?, String?>): Call<Void>

    @POST("user/product/purchase")
    fun generalProductPostData(@Body obj: GeneralProductPostData, @HeaderMap headers: HashMap<String?, String?>): Call<String>

    @GET("user/product/offer/by/code")
    fun getOfferProduct(@HeaderMap headers: HashMap<String?, String?>, @Query("code") code: String): Call<OfferProduct>

    @POST("user/product/purchase/offer")
    fun offerProductPostData(@Body obj: OfferProductPost, @HeaderMap headers: HashMap<String?, String?>): Call<String>

    @GET("user/profile/by/custom_id")
    fun getDataSearchByNetiID(@HeaderMap headers: HashMap<String?, String?>, @Query("custom_id") code: String): Call<String>

    @POST("user/transfer/check/balance")
    fun WalletBalanceTransfer(@Body obj: WalletTransfer, @HeaderMap headers: HashMap<String?, String?>): Call<Void>

    @GET("guest/core/check-otp")
    fun OTPVarify(@HeaderMap headers: HashMap<String?, String?>, @Query("code") code: String): Call<String>

    @POST("user/balance/transfer")
    fun WalletBalanceTransferFinal(@Body obj: WalletTransfer, @HeaderMap headers: HashMap<String?, String?>): Call<String>

    @POST("user/balance/requests/by/date_range")
    fun balanceDepositReportGetData(
        @HeaderMap headers: HashMap<String?, String?>,
        @Body obj: BalanceReportPostData
    ): Call<List<BalanceDepositReportGetData>>

    /*@POST("user/get-transfer-records")
    fun balanceTransferGetData(@Body obj: BalanceDepositPostForList, @HeaderMap headers: HashMap<String?, String?>): Call<List<BalanceTransferGetData>>
    */
    @POST("user/balance/transfer/by/date")
    fun balanceTransferGetData(
        @HeaderMap headers: HashMap<String?, String?>,
        @Body obj: BalanceReportPostData
    ): Call<List<BalanceTransferGetData>>

    @POST("user/message/by/date-range")
    fun messageRechargeGetData(@Body obj: BalanceDepositPostForList, @HeaderMap headers: HashMap<String?, String?>): Call<List<BalanceMessageGetData>>

    @POST("user/balance/statement")
    fun balanceStatementGetData(@HeaderMap headers: HashMap<String?, String?>,
                                @Body obj: BalanceDepositPostForList
    ): Call<List<BalanceStatementGetData>>

    @GET("user/product/purchases/by/date-range")
    fun getGeneralProductReportData(
        @HeaderMap headers: HashMap<String?, String?>,
        @Query("startDate") startDate: String,
        @Query("endDate") endDate: String
    ): Call<List<GeneralProductGetData>>

    @GET("user/purchase/codes/by/purchaseid")
    fun getProductGDetailsData(@HeaderMap headers: HashMap<String?, String?>, @Query("purchaseID") purchaseID: Int?): Call<List<ProductGDetailsGetData>>

    @POST("user/product/offer/by/date-range")
    fun getProductOfferReportData(
        @HeaderMap headers: HashMap<String?, String?>,
        @Body obj: ProductsOfferReportPostData
    ): Call<List<ProductsOfferGetData>>

    @GET("user/purchase/codes/by/purchaseid")
    fun getProductODetailsData(@HeaderMap headers: HashMap<String?, String?>, @Query("purchaseID") purchaseID: Int?): Call<List<ProductGDetailsGetData>>

    @GET("user/purchase/codes/by/usedstatus")
    fun getPurchaseCodeData(@HeaderMap headers: HashMap<String?, String?>, @Query("usedStatus") usedStatus: Int?): Call<List<PurchaseCodeLogGetdata>>

    @GET("guest/file/find")
    fun getProfile(@HeaderMap headers: HashMap<String?, String?>, @Query("filePath") filePath: String): Call<ProfileImage>

    @POST("guest/user/register-user")
    fun registrationSubmit(@Body obj: RegistrationPost): Call<Void>

    @GET("guest/addresses/district")
    fun getDistrict(): Call<List<DistrictModel>>

    @GET("guest/addresses/area")
    fun getArea(@Query("parentCategoryID") parentCategoryID: String): Call<List<AreaModel>>

    @GET("guest/user/profile/by")
    fun userCheckForgotPaasword(@Query("username") username: String): Call<UserCheckForgotPass>

      @POST("guest/recover/password/check_contact")
    fun forgotPasswordOTP(@Query("userContactNo") userContactNo: String, @Body obj: UserCheckForgotPass?): Call<Void>

    @GET("guest/core/check-otp")
    fun forgotPasswordOTPVarify(@Query("code") code: String): Call<Any>

    @POST("guest/recover/password/reset")
    fun resetPasswordAfterForgot(@Body obj: ResetPassword?): Call<Void>

    @POST("user/change/password")
    fun changePassword(@HeaderMap headers: HashMap<String?, String?>, @Body obj: ResetPassword?): Call<Void>

    @POST("user/chat/new")
    fun submitNetiMail(@Header("Authorization") token: String,
                       @Body netiMailSubmit: NetiMailSubmit
    ): Call<Void>

    //------------------------------------------------------------------------------------------------------
    @Headers(
        "Authorization: Basic ZGV2Z2xhbi1jbGllbnQ6ZGV2Z2xhbi1zZWNyZXQ=",
        "Content-Type:application/x-www-form-urlencoded",
        "NZUSER:absiddik:123:password"
    )
    @FormUrlEncoded
    @POST("oauth/token")
    fun userLogIn(@Field("name") name: String, @Field("password") password: String, @Field("grant_type") grant_type: String): Call<MSG>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/home/dashboard/info")
    fun getHomePageInfo(@Header("Authorization") token: String): Call<HomePageInfo>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/core/bank/accounts/by")
    fun getBalanceDepositAccountInfo(@Header("Authorization") token: String,
                                     @Query("defaultCode") defaultCode: String
    ): Call<List<BalanceDepositAccountInfo>?>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/tagged/bank-account/by")
    fun getBalanceWithdrawInfo(@Header("Authorization") token: String,
                               @Query("coreCategoryDefaultCode") codeCategoryDefaultCode: String
    ): Call<WithDrawInfo?>

    @POST("user/balance/withdraw")
    fun submitBalanceWithdrawData(@Header("Authorization") token: String,
                                  @Body obj: WithdrawPostData
    ): Call<String?>

    @POST("user/report/revenue/log")
    fun getRevenueLogData(@Header("Authorization") token: String,
                          @Body revenueLogPostData: RevenueLogPostData?
    ): Call<RevenueLogGetData>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/category/by/typeDefaultCode")
    fun getBankDistrictList(@Header("Authorization") token: String,
                            @Query("typeDefaultCode") typeDefaultCode: String?
    ): Call<List<BankAccountInfo>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/bank/branches/by")
    fun getBankBranchList(@Header("Authorization") token: String,
                          @Query("bankID") bankId: String?,
                          @Query("districtID") districtId: String?
    ): Call<List<BankBranchInfo>>

    @POST("user/bank/add-account")
    fun submitBankAccount(@Header("Authorization") token: String,
                          @Body bankAccountPostData: BankAccountPostData
    ): Call<String>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/bank/accounts/by/user")
    fun getBankList(@Header("Authorization") token: String): Call<List<BankList>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/category/by/typeDefaultCode")
    fun getTagTypeList(@Header("Authorization") token: String,
                       @Query("typeDefaultCode") typeDefaultCode: String
    ): Call<List<TagType>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/bank/account/taggings")
    fun getTagList(@Header("Authorization") token: String): Call<List<TagGetData>>

    @POST("user/bank/account/tag")
    fun tagBankAccount(@Header("Authorization") token: String,
                       @Body tagPostData: TagPostData
    ): Call<String>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/tokens/modules/by")
    fun getProblemModuleList(@Header("Authorization") token: String,
                             @Query("catDefaultCode") catDefaultCode: String
    ): Call<List<Problem>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/tokens/types/by")
    fun getProblemTypeList(@Header("Authorization") token: String,
                           @Query("parentCategoryID") parentCategoryID: String
    ): Call<List<Problem>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/tokens/by/pending")
    fun getSolvedAndPendingToken(@Header("Authorization") token: String): Call<List<TokenList>>

    @POST("user/tokens/new")
    fun submitNewToken(@Header("Authorization") token: String,
                       @Body tokenSubmitPostData: TokenSubmitPostData
    ): Call<Void>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/tokens/by/top10")
    fun getTokenList(@Header("Authorization") token: String): Call<List<TokenList>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("guest/file/find/")
    fun downloadTokenAttachment(@Header("Authorization") token: String,
                                @Query("filePath") imageUrl: String?
    ): Call<JsonObject>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/education-infos/find")
    fun getEducationInfo(@Header("Authorization") token: String): Call<List<EducationInfo>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/training-infos/find")
    fun getTrainingInfo(@Header("Authorization") token: String): Call<List<TrainingInfo>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/certificate-infos/find")
    fun getCertificationInfo(@Header("Authorization") token: String): Call<List<CertificationInfo>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/experience-infos/find")
    fun getExperienceInfo(@Header("Authorization") token: String): Call<List<ExperienceInfo>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/reference-infos/find")
    fun getReferenceInfo(@Header("Authorization") token: String): Call<List<ReferenceInfo>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/category/by/typeDefaultCode")
    fun getUsingPurposeList(@Header("Authorization") token: String,
                            @Query("typeDefaultCode") typeDefaultCode: String
    ): Call<List<UsingPurpose>>

    @POST("user/bank/account/add/and/tag")
    fun tagAndAddBankAccount(@Header("Authorization") token: String,
                             @Body tagAndAddBankAccount: TagAndAddBankAccount
    ): Call<String>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/category/by/typeDefaultCode")
    fun getDataByTypeDefaultCode(@Header("Authorization") token: String,
                                 @Query("typeDefaultCode") typeDefaultCode: String
    ): Call<List<DataByTypeDefaultCode>>

    @POST("user/education-info/save")
    fun saveAcademicEducationInfo(@Header("Authorization") token: String,
                                  @Body postAcademicEducation: PostAcademicEducation
    ): Call<String>

    @POST("user/training-info/save")
    fun saveTrainingInfo(@Header("Authorization") token: String,
                         @Body postTrainingInfo: PostTrainingInfo
    ): Call<String>

    @POST("user/certification-info/save")
    fun saveCertificationInfo(@Header("Authorization") token: String,
                              @Body postCertificationInfo: PostCertificationInfo
    ): Call<String>

    @POST("user/experience-info/save")
    fun saveExperienceInfo(
        @Header("Authorization") token: String,
        @Body postExperienceInfo: PostExperienceInfo
    ): Call<String>


    @POST("user/reference-info/save")
    fun saveReferenceInfo(
        @Header("Authorization") token: String,
        @Body postReferenceInfo: PostReferenceInfo
    ): Call<String>

    @PUT("user/experience-info/update")
    fun updateExperienceInfo(
        @Header("Authorization") token: String,
        @Body updateExperienceInfo: UpdateExperienceInfo
    ): Call<String>

    @PUT("user/bank/account/tagging/udate")
    fun updateBankAccount(@Header("Authorization") token: String,
                          @Body updateBankAccount: UpdateBankAccount
    ): Call<String>

    @PUT("user/training-info/update")
    fun updateTrainingInfo(@Header("Authorization") token: String,
                         @Body postTrainingInfo: PostTrainingInfo
    ): Call<String>

    @PUT("user/reference-info/update")
    fun updateReferenceInfo(
        @Header("Authorization") token: String,
        @Body updateReferenceInfo: UpdateReferenceInfo
    ): Call<String>

    @PUT("user/certification-info/update")
    fun updateCertificationInfo(
        @Header("Authorization") token: String,
        @Body updateCertificationInfo: UpdateCertificationInfo
    ): Call<String>

    @PUT("user/education-info/update")
    fun updateEducationInfo(
        @Header("Authorization") token: String,
        @Body updateCertificationInfo: PostAcademicEducation
    ): Call<String>

    @PUT("user/profile/edit/basic")
    fun updatePersonalInfo(
        @Header("Authorization") token: String,
        @Body profileInformation: ProfileInformation_
    ): Call<String>

    @PUT("user/profile/edit/basic")
    fun updateAddressInfo(
        @Header("Authorization") token: String,
        @Body profileInformation: ProfileInformation
    ): Call<String>

    @PUT("user/profile/edit/basic")
    fun getDivisionList(
        @Header("Authorization") token: String,
        @Body profileInformation: ProfileInformation
    ): Call<String>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/category/by/parentCatID")
    fun getDistrictList(
        @Header("Authorization") token: String,
        @Query("parentCategoryID") typeDefaultCode: String
    ): Call<List<DataByTypeDefaultCode>>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/em/user/point/assign")
    fun addPoint(@Header("Authorization") token: String): Call<String>

    // User Point API Interface---------------------------------------------

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/student/profile")
    fun getStudentPortalProfileInfo(
        @Header("Authorization") token: String,
        @Query("instituteId") instituteId: String, //10503
        @Query("studentId") studentId: String //105032091220
    ): Call<StudentPortalProfile>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/student/subjects")
    fun getStudentPortalSubjectInfo(
        @Header("Authorization") token: String,
        @Query("instituteId") instituteId: String, //10503
        @Query("studentId") studentId: String,//105032091220
        @Query("academicYear") academicYear: String //2020
    ): Call<StudentPortalSubject>


    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/student/class-tests")
    fun getStudentPortalClassTestInfo(
        @Header("Authorization") token: String,
        @Query("instituteId") instituteId: String, //10503
        @Query("studentId") studentId: String,//105032091220
        @Query("academicYear") academicYear: String //2020
    ): Call<String>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/sa/find/global-info/list/by/type-id")
    fun getStudentPortalGlobalInfo(
        @Header("Authorization") token: String,
        @Query("typeId") typeId: String //2101

    ): Call<YearByTypeId>


    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/student/exams")
    fun getStudentPortalExamInfo(
        @Header("Authorization") token: String,
        @Query("instituteId") instituteId: String, //10503
        @Query("studentId") studentId: String,//105032091220
        @Query("academicYear") academicYear: String //2020
    ): Call<String>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/student/class-routine")
    fun getStudentPortalClassRoutineInfo(
        @Header("Authorization") token: String,
        @Query("instituteId") instituteId: String, //10503
        @Query("studentId") studentId: String,//105032091220
        @Query("academicYear") academicYear: String //2020
    ): Call<String>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/student/sessions")
    fun getStudentPortalExamRoutineSession(
        @Header("Authorization") token: String,
        @Query("instituteId") instituteId: String //10503

    ): Call<String>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/student/core-settings/by/type-id")
    fun getStudentPortalPeriod(
        @Header("Authorization") token: String,
        @Query("typeId") typeId: String,
        @Query("instituteId") instituteId: String //10503

    ): Call<StudentPortalPeriod>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/student/accounts/invoice/unpaid/list")
    fun getStudentPortalUnpaidFees(
        @Header("Authorization") token: String,
        @Query("instituteId") instituteId: String, //10597
        @Query("studentId") studentId: String,//105972031518
        @Query("academicYear") academicYear: String //2020

    ): Call<UnpaidFees>

    // Request URL: https://api.netiworld.com/user/student/accounts/inventory/details?instituteId=10503&studentId=105032050618&year=2019&monthName=April

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/student/accounts/inventory/details")
    fun getStudentPortalInventoryDetails(
        @Header("Authorization") token: String,
        @Query("instituteId") instituteId: String, //10597
        @Query("studentId") studentId: String,//105972031518
        @Query("year") year: String, //2020
        @Query("monthName") monthName: String //2020

    ): Call<InventoryDetails>

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/student/class-tests")
    fun getStudentPortalClassTest(
        @Header("Authorization") token: String,
        @Query("instituteId") instituteId: String, //10597
        @Query("studentId") studentId: String,//105972031518
        @Query("academicYear") year: String //2020

    ): Call<ClassTest>

    // Request URL: https://api.netiworld.com/user/student/class-test/marks?instituteId=10503&studentId=105032050618&academicYear=2019&classTestConfigId=8190

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @GET("user/student/class-test/marks")
    fun getStudentPortalClassTestDetails(
        @Header("Authorization") token: String,
        @Query("instituteId") instituteId: String, //10503
        @Query("studentId") studentId: String,//105032050618
        @Query("academicYear") year: String,//2019
        @Query("classTestConfigId") classTestConfigId: String //8190

    ): Call<ClassTestDetails>

}
