package com.netizen.netiworld.zoom.sdksample.startjoinmeeting.apiuser;

import com.netizen.netiworld.zoom.sdksample.initsdk.AuthConstants;

public interface APIUserConstants {

    // TODO Change it to your web API Key
    public final static String API_KEY = AuthConstants.SDK_KEY;

    // TODO Change it to your web API Secret
    public final static String API_SECRET = AuthConstants.SDK_SECRET;

	// TODO change it to your user ID, do not need for login user
	public final static String USER_ID = "Your user id";

    // TODO change it to your Zoom access token expired time
	public final static long EXPIRED_TIME = 3600 * 2; //two hours

}
