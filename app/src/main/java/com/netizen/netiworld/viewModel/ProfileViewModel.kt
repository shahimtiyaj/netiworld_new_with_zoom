package com.netizen.netiworld.viewModel

import android.app.Application
import androidx.lifecycle.*
import com.netizen.netiworld.model.*
import com.netizen.netiworld.repository.ProfileRepository

class ProfileViewModel(application: Application) : AndroidViewModel(application) {

    private val profileRepository = ProfileRepository(application)

    var profileInformation = profileRepository.profileInformation
    var photoFileContent = profileRepository.photoFileContent
    val educationInfoList = profileRepository.educationInfoList
    val trainingInfoList = profileRepository.trainingInfoList
    val certificationInfoList = profileRepository.certificationInfoList
    val experienceInfoList = profileRepository.experienceInfoList
    val referenceInfoList = profileRepository.referenceInfoList
    private val educationLevelList = profileRepository.educationLevelList
    private val majorSubjectList = profileRepository.majorSubjectList
    private val resultList = profileRepository.resultList
    private val passingYearList = profileRepository.passingYearList
    private val boardList = profileRepository.boardList
    private val countryList = profileRepository.countryList
    private val relationList = profileRepository.relationList
    private val bloodGroupList = profileRepository.bloodGroupList
    private val divisionList = profileRepository.divisionList
    private val districtList = profileRepository.districtList
    private val upazillaList = profileRepository.upazillaList

    private var educationLevelId: String? = null
    private var majorSubjectId: String? = null
    private var resultId: String? = null
    private var passingYearId: String? = null
    private var boardId: String? = null
    private var countryId: String? = null
    private var coreCategoryID: String? = null
    private var bloodGroupId: String? = null
    private var divisionId: String? = null
    private var districtId: String? = null
    private var upozillaId: String? = null

    var isSerialEmpty = MutableLiveData<Boolean>()
    var isEducationLevelEmpty = MutableLiveData<Boolean>()
    var isExamEmpty = MutableLiveData<Boolean>()
    var isMajorEmpty = MutableLiveData<Boolean>()
    var isResultEmpty = MutableLiveData<Boolean>()
    var isPassingYearEmpty = MutableLiveData<Boolean>()
    var isDurationEmpty = MutableLiveData<Boolean>()
    var isBoardEmpty = MutableLiveData<Boolean>()
    var isInstituteEmpty = MutableLiveData<Boolean>()
    var isAchievementEmpty = MutableLiveData<Boolean>()
    var isTrainingTitleEmpty = MutableLiveData<Boolean>()
    var isTopicEmpty = MutableLiveData<Boolean>()
    var isLocationEmpty = MutableLiveData<Boolean>()
    var isCountryEmpty = MutableLiveData<Boolean>()
    var isCertificationNameEmpty = MutableLiveData<Boolean>()
    var isDateEmpty = MutableLiveData<Boolean>()
    var isReferenceNameEmpty = MutableLiveData<Boolean>()
    var isReferenceDesignationEmpty = MutableLiveData<Boolean>()
    var isReferenceOrganizationNameEmpty = MutableLiveData<Boolean>()
    var isReferenceMobileEmpty = MutableLiveData<Boolean>()
    var isReferenceEmailEmpty = MutableLiveData<Boolean>()
    var isReferenceAddressEmpty = MutableLiveData<Boolean>()
    var isRelationEmpty = MutableLiveData<Boolean>()

    var isCompanyNameEmpty = MutableLiveData<Boolean>()
    var isCompanyBusinessEmpty = MutableLiveData<Boolean>()
    var isDesignationNameEmpty = MutableLiveData<Boolean>()
    var isWorkingDepartmentEmpty = MutableLiveData<Boolean>()
    var isCompanyLocationEmpty = MutableLiveData<Boolean>()
    var isEmploymentStartEmpty = MutableLiveData<Boolean>()
    var isEmploymentEndEmpty = MutableLiveData<Boolean>()
    var isResponsibilitiesDetailsEmpty = MutableLiveData<Boolean>()

    //----------------------------
    var isFatherNameEmpty = MutableLiveData<Boolean>()
    var isMotherNameEmpty = MutableLiveData<Boolean>()
    var isMobileEmpty = MutableLiveData<Boolean>()
    var isBirthDayEmpty = MutableLiveData<Boolean>()
    var isGenderEmpty = MutableLiveData<Boolean>()
    var isBloodEmpty = MutableLiveData<Boolean>()
    var isMaritalStatusEmpty = MutableLiveData<Boolean>()
    var isNumberOfChildEmpty = MutableLiveData<Boolean>()
    var isNationalityEmpty = MutableLiveData<Boolean>()
    var isNationalIdEmpty = MutableLiveData<Boolean>()
    var isPassportEmpty = MutableLiveData<Boolean>()
    var isBirthCertificationEmpty = MutableLiveData<Boolean>()
    var isBloodGroupEmpty = MutableLiveData<Boolean>()
    var isNetiIdEmpty = MutableLiveData<Boolean>()

    var isAddressDetailsEmpty = MutableLiveData<Boolean>()
    var isDivisionEmpty = MutableLiveData<Boolean>()
    var isDistrictEmpty = MutableLiveData<Boolean>()
    var isUpazillaEmpty = MutableLiveData<Boolean>()
    var isLatitudeEmpty = MutableLiveData<Boolean>()
    var isLongitudeEmpty = MutableLiveData<Boolean>()

    val tempEducationLevelList = Transformations.map(educationLevelList) {
        getTempEducationLevelList(it)
    }
    val tempMajorSubjectsList = Transformations.map(majorSubjectList) {
        getTempMajorSubjectList(it)
    }
    val tempResultList = Transformations.map(resultList) {
        getTempResultList(it)
    }
    val tempPassingYearList = Transformations.map(passingYearList) {
        getTempPassingYearList(it)
    }
    val tempBoardList = Transformations.map(boardList) {
        getTempBoardList(it)
    }

    val tempCountryList = Transformations.map(countryList) {
        getTempCountryList(it)
    }

    val tempRelationList = Transformations.map(relationList) {
        getTempCountryList(it)
    }

    val tempBloodGroupList = Transformations.map(bloodGroupList) {
        getTempBloodGroupList(it)
    }

    val tempDivisionList = Transformations.map(divisionList) {
        getTempDivisionList(it)
    }

    val tempDistrictList = Transformations.map(districtList) {
        getTempDistrictList(it)
    }

    val tempUpazillaList = Transformations.map(upazillaList) {
        getTempUpazillaList(it)
    }

    private fun getTempDivisionList(divisionList: List<DataByTypeDefaultCode>): ArrayList<String> {
        val tempDivisionList = ArrayList<String>()

        divisionList.forEach { division ->
            tempDivisionList.add(division.getCategoryName().toString())
        }

        return tempDivisionList
    }

    private fun getTempDistrictList(districtList: List<DataByTypeDefaultCode>): ArrayList<String> {
        val tempDistrictList = ArrayList<String>()

        districtList.forEach { district ->
            tempDistrictList.add(district.getCategoryName().toString())
        }

        return tempDistrictList
    }

    private fun getTempUpazillaList(upazillaList: List<DataByTypeDefaultCode>): ArrayList<String> {
        val tempUpazillaList = ArrayList<String>()

        upazillaList.forEach { upazilla ->
            tempUpazillaList.add(upazilla.getCategoryName().toString())
        }

        return tempUpazillaList
    }

    private fun getTempBloodGroupList(bloodGroupList: List<DataByTypeDefaultCode>): ArrayList<String> {
        val tempBloodList = ArrayList<String>()

        bloodGroupList.forEach { country ->
            tempBloodList.add(country.getCategoryName().toString())
        }

        return tempBloodList
    }


    private fun getTempEducationLevelList(educationLevelList: List<DataByTypeDefaultCode>): ArrayList<String> {
        val tempEducationLevelList = ArrayList<String>()

        educationLevelList.forEach { educationLevel ->
            tempEducationLevelList.add(educationLevel.getCategoryName().toString())
        }

        return tempEducationLevelList
    }

    private fun getTempMajorSubjectList(majorSubjectList: List<DataByTypeDefaultCode>): ArrayList<String> {
        val tempMajorSubjectList = ArrayList<String>()

        majorSubjectList.forEach { majorSubject ->
            tempMajorSubjectList.add(majorSubject.getCategoryName().toString())
        }

        return tempMajorSubjectList
    }

    private fun getTempResultList(resultList: List<DataByTypeDefaultCode>): ArrayList<String> {
        val tempResultList = ArrayList<String>()

        resultList.forEach { educationLevel ->
            tempResultList.add(educationLevel.getCategoryName().toString())
        }

        return tempResultList
    }

    private fun getTempPassingYearList(passingYearList: List<DataByTypeDefaultCode>): ArrayList<String> {
        val tempPassingYearList = ArrayList<String>()

        passingYearList.forEach { passingYear ->
            tempPassingYearList.add(passingYear.getCategoryName().toString())
        }

        return tempPassingYearList
    }

    private fun getTempBoardList(boardList: List<DataByTypeDefaultCode>): ArrayList<String> {
        val tempBoardList = ArrayList<String>()

        boardList.forEach { board ->
            tempBoardList.add(board.getCategoryName().toString())
        }

        return tempBoardList
    }

    private fun getTempCountryList(countryList: List<DataByTypeDefaultCode>): ArrayList<String> {
        val tempCountryList = ArrayList<String>()

        countryList.forEach { country ->
            tempCountryList.add(country.getCategoryName().toString())
        }

        return tempCountryList
    }

    private fun getTempRelationList(relationList: List<DataByTypeDefaultCode>): ArrayList<String> {
        val tempRelationList = ArrayList<String>()

        relationList.forEach { relation ->
            tempRelationList.add(relation.getCategoryName().toString())
        }
        return tempRelationList
    }

    fun getBloodGroupId(bloodName: String?) {
        bloodGroupList.value?.forEach {
            if (it.getCategoryName().equals(bloodName, true)) {
                bloodGroupId = it.getCoreCategoryID().toString()
                return
            }
        }
    }

    fun getEducationLevelId(educationLevelName: String?) {
        educationLevelList.value?.forEach {
            if (it.getCategoryName().equals(educationLevelName, true)) {
                educationLevelId = it.getCoreCategoryID().toString()
                return
            }
        }
    }

    fun getMajorSubjectId(majorSubjectName: String?) {
        majorSubjectList.value?.forEach {
            if (it.getCategoryName().equals(majorSubjectName, true)) {
                majorSubjectId = it.getCoreCategoryID().toString()
                return
            }
        }
    }

    fun getResultId(resultName: String?) {
        resultList.value?.forEach {
            if (it.getCategoryName().equals(resultName, true)) {
                resultId = it.getCoreCategoryID().toString()
                return
            }
        }
    }

    fun getPassingYearId(passingYearName: String?) {
        passingYearList.value?.forEach {
            if (it.getCategoryName().equals(passingYearName, true)) {
                passingYearId = it.getCoreCategoryID().toString()
                return
            }
        }
    }

    fun getBoardId(boardName: String?) {
        boardList.value?.forEach {
            if (it.getCategoryName().equals(boardName, true)) {
                boardId = it.getCoreCategoryID().toString()
                return
            }
        }
    }

    fun getCountryId(countryName: String?) {
        countryList.value?.forEach {
            if (it.getCategoryName().equals(countryName, true)) {
                countryId = it.getCoreCategoryID().toString()
                return
            }
        }
    }

    fun getDivisionId(countryName: String?) {
        divisionList.value?.forEach {
            if (it.getCategoryName().equals(countryName, true)) {
                divisionId = it.getCoreCategoryID().toString()
                return
            }
        }
    }

    fun getDistrictId(countryName: String?) {
        districtList.value?.forEach {
            if (it.getCategoryName().equals(countryName, true)) {
                districtId = it.getCoreCategoryID().toString()
                return
            }
        }
    }

    fun getUpazillaId(countryName: String?) {
        upazillaList.value?.forEach {
            if (it.getCategoryName().equals(countryName, true)) {
                upozillaId = it.getCoreCategoryID().toString()
                return
            }
        }
    }

    fun getRelationId(relationName: String?) {
        relationList.value?.forEach {
            if (it.getCategoryName().equals(relationName, true)) {
                coreCategoryID = it.getCoreCategoryID().toString()
                return
            }
        }
    }

    fun getProfileInfo() {
        if (profileInformation.value == null) {
            profileRepository.getUserProfileInfoData()
        }
    }

    fun getProfileImage(imagePath: String) {
        if (photoFileContent.value.isNullOrEmpty()) {
            profileRepository.getProfilePhoto(imagePath)
        }
    }

    fun getEducationInfo() {
        if (educationInfoList.value.isNullOrEmpty()) {
            profileRepository.getEducationInfo()
        }
    }

    fun getTrainingInfo() {
        if (trainingInfoList.value.isNullOrEmpty()) {
            profileRepository.getTrainingInfo()
        }
    }

    fun getCertificationInfo() {
        if (certificationInfoList.value.isNullOrEmpty()) {
            profileRepository.getCertificationInfo()
        }
    }

    fun getExperienceInfo() {
        if (experienceInfoList.value.isNullOrEmpty()) {
            profileRepository.getExperienceInfo()
        }
    }

    fun getReferenceInfo() {
        if (referenceInfoList.value.isNullOrEmpty()) {
            profileRepository.getReferenceInfo()
        }
    }

    fun getBloodGroup() {
       // if (bloodGroupList.value.isNullOrEmpty()) {
            profileRepository.getBloodGroup()
        //  }
    }

    fun getEducationLevels() {
        if (educationLevelList.value.isNullOrEmpty()) {
            profileRepository.getEducationLevels()
        }
    }

    fun getMajorSubjects() {
        if (majorSubjectList.value.isNullOrEmpty()) {
            profileRepository.getMajorSubjects()
        }
    }

    fun getResults() {
        if (resultList.value.isNullOrEmpty()) {
            profileRepository.getResults()
        }
    }

    fun getPassingYears() {
        if (passingYearList.value.isNullOrEmpty()) {
            profileRepository.getPassingYears()
        }
    }

    fun getBoards() {
        if (boardList.value.isNullOrEmpty()) {
            profileRepository.getBoards()
        }
    }

    fun getCountry() {
        //  if (countryList.value.isNullOrEmpty()) {
            profileRepository.getCountry()
        //  }
    }

    fun getDivision() {
        // if (divisionList.value.isNullOrEmpty()) {
            profileRepository.getDivision()
        //  }
    }

    fun getDistrict() {
        // if (districtList.value.isNullOrEmpty()) {
            profileRepository.getDistrict(divisionId)
        //  }
    }

    fun getUpazilla() {
        //  if (upazillaList.value.isNullOrEmpty()) {
            profileRepository.getUpazilla(districtId)
        //  }
    }

    fun addPoint() {
        profileRepository.addPoint()
    }

    fun getRelation() {
        // if (relationList.value.isNullOrEmpty()) {
            profileRepository.getRelation()
        //  }
    }

    fun checkAcademicEducationInfo(
        serial: String?,
        examTitle: String?,
        duration: String?,
        instituteName: String?,
        achievement: String?,
        foreignStatus: Int
    ) {
        when {
            serial.isNullOrEmpty() -> {
                isSerialEmpty.value = true
            }
            educationLevelId.isNullOrEmpty() -> {
                isEducationLevelEmpty.value = true
            }
            examTitle.isNullOrEmpty() -> {
                isExamEmpty.value = true
            }
            majorSubjectId.isNullOrEmpty() -> {
                isMajorEmpty.value = true
            }
            resultId.isNullOrEmpty() -> {
                isResultEmpty.value = true
            }
            passingYearId.isNullOrEmpty() -> {
                isPassingYearEmpty.value = true
            }
            duration.isNullOrEmpty() -> {
                isDurationEmpty.value = true
            }
            boardId.isNullOrEmpty() -> {
                isBoardEmpty.value = true
            }
            instituteName.isNullOrEmpty() -> {
                isInstituteEmpty.value = true
            }
            achievement.isNullOrEmpty() -> {
                isAchievementEmpty.value = true
            }
            else -> {
                val degreeInfoDTO = PostAcademicEducation.DegreeInfoDTO()
                degreeInfoDTO.setCoreCategoryID(educationLevelId!!)

                val subjectInfoDTO = PostAcademicEducation.SubjectInfoDTO()
                subjectInfoDTO.setCoreCategoryID(majorSubjectId!!)

                val gradeInfoDTO = PostAcademicEducation.GradeInfoDTO()
                gradeInfoDTO.setCoreCategoryID(resultId!!)

                val passingYearInfoDTO = PostAcademicEducation.PassingYearInfoDTO()
                passingYearInfoDTO.setCoreCategoryID(passingYearId!!)

                val boardInfoDTO = PostAcademicEducation.BoardInfoDTO()
                boardInfoDTO.setCoreCategoryID(boardId!!)

                val postAcademicEducation = PostAcademicEducation()
                postAcademicEducation.setDegreeInfoDTO(degreeInfoDTO)
                postAcademicEducation.setSubjectInfoDTO(subjectInfoDTO)
                postAcademicEducation.setGradeInfoDTO(gradeInfoDTO)
                postAcademicEducation.setPassingYearInfoDTO(passingYearInfoDTO)
                postAcademicEducation.setBoardInfoDTO(boardInfoDTO)
                postAcademicEducation.setEduInfoSerial(serial)
                postAcademicEducation.setExamTitle(examTitle)
                postAcademicEducation.setEduDuration(duration)
                postAcademicEducation.setInstituteName(instituteName)
                postAcademicEducation.setAchievementDetails(achievement)
                postAcademicEducation.setInstituteForeginStatus(foreignStatus)

                profileRepository.saveAcademicEducation(postAcademicEducation)
            }
        }
    }

    fun checkTrainingInfo(
        title: String?,
        topic: String?,
        instituteName: String?,
        location: String?,
        duration: String?
    ) {
        when {
            title.isNullOrEmpty() -> {
                isTrainingTitleEmpty.value = true
            }
            topic.isNullOrEmpty() -> {
                isTopicEmpty.value = true
            }
            instituteName.isNullOrEmpty() -> {
                isInstituteEmpty.value = true
            }
            location.isNullOrEmpty() -> {
                isLocationEmpty.value = true
            }
            countryId.isNullOrEmpty() -> {
                isCountryEmpty.value = true
            }
            passingYearId.isNullOrEmpty() -> {
                isPassingYearEmpty.value = true
            }
            duration.isNullOrEmpty() -> {
                isDurationEmpty.value = true
            }
            else -> {

                val countryInfoDTO = PostTrainingInfo.CountryInfoDTO()
                countryInfoDTO.setCoreCategoryID(countryId!!.toInt())

                val trainingYearInfoDTO = PostTrainingInfo.TrainingYearInfoDTO()
                trainingYearInfoDTO.setCoreCategoryID(passingYearId!!.toInt())

                val postTrainingInfo = PostTrainingInfo()
                postTrainingInfo.setCountryInfoDTO(countryInfoDTO)
                postTrainingInfo.setTrainingYearInfoDTO(trainingYearInfoDTO)
                postTrainingInfo.setInstituteLocation(location)
                postTrainingInfo.setInstituteName(instituteName)
                postTrainingInfo.setTopicCoveredDetails(topic)
                postTrainingInfo.setTrainingDuration(duration)
                postTrainingInfo.setTrainingTitle(title)

                profileRepository.saveTrainingInfo(postTrainingInfo)
            }
        }
    }


    fun checkCertificationInfo(
        certificationName: String?,
        instituteName: String?,
        location: String?,
        date: String?,
        courseDuration: String?
    ) {
        when {
            certificationName.isNullOrEmpty() -> {
                isCertificationNameEmpty.value = true
            }
            instituteName.isNullOrEmpty() -> {
                isInstituteEmpty.value = true
            }
            location.isNullOrEmpty() -> {
                isLocationEmpty.value = true
            }
            countryId.isNullOrEmpty() -> {
                isCountryEmpty.value = true
            }
            date.isNullOrEmpty() -> {
                isDateEmpty.value = true
            }
            courseDuration.isNullOrEmpty() -> {
                isDurationEmpty.value = true
            }
            else -> {
                val countryInfoDTO = PostCertificationInfo.CountryInfoDTO()
                countryInfoDTO.setCoreCategoryID(countryId!!)

                val postCertificationInfo = PostCertificationInfo()
                postCertificationInfo.setCountryInfoDTO(countryInfoDTO)
                postCertificationInfo.setAchieveDate(date)
                postCertificationInfo.setCertificationName(certificationName)
                postCertificationInfo.setCourseDuration(courseDuration)
                postCertificationInfo.setInstituteLocation(location)
                postCertificationInfo.setInstituteName(instituteName)

                profileRepository.saveCertificationInfo(postCertificationInfo)
            }
        }
    }

    fun checkExperienceInfo(
        companyName: String?,
        companyBusiness: String?,
        designationName: String?,
        workingDepartment: String?,
        companyLocation: String?,
        employmentStart: String?,
        employmentEnd: String?,
        responsibilityDetails: String?

    ) {
        when {
            companyName.isNullOrEmpty() -> {
                isCompanyNameEmpty.value = true
            }
            companyBusiness.isNullOrEmpty() -> {
                isCompanyBusinessEmpty.value = true
            }
            designationName.isNullOrEmpty() -> {
                isDesignationNameEmpty.value = true
            }
            workingDepartment.isNullOrEmpty() -> {
                isWorkingDepartmentEmpty.value = true
            }
            companyLocation.isNullOrEmpty() -> {
                isCompanyLocationEmpty.value = true
            }
            employmentStart.isNullOrEmpty() -> {
                isEmploymentStartEmpty.value = true
            }
            employmentEnd.isNullOrEmpty() -> {
                isEmploymentEndEmpty.value = true
            }
            responsibilityDetails.isNullOrEmpty() -> {
                isResponsibilitiesDetailsEmpty.value = true
            }
            countryId.isNullOrEmpty() -> {
                isCountryEmpty.value = true
            }

            else -> {
                val countryInfoDTO = PostExperienceInfo.CountryInfoDTO()
                countryInfoDTO.setCoreCategoryID(countryId)

                val postExperienceInfo = PostExperienceInfo()
                postExperienceInfo.setCountryInfoDTO(countryInfoDTO)
                postExperienceInfo.setCompanyName(companyName)
                postExperienceInfo.setCompanyBusiness(companyBusiness)
                postExperienceInfo.setDesignationName(designationName)
                postExperienceInfo.setWorkingDepartment(workingDepartment)
                postExperienceInfo.setCompanyLocation(companyLocation)
                postExperienceInfo.setEmploymentStart(employmentStart)
                postExperienceInfo.setEmploymentEnd(employmentEnd)
                postExperienceInfo.setResponsibilityDetails(responsibilityDetails)

                profileRepository.saveExperienceInfo(postExperienceInfo)
            }
        }
    }

    fun checkReferenceInfo(
        referenceName: String?,
        referenceDesignation: String?,
        organizationName: String?,
        referenceMobile: String?,
        referenceEmail: String?,
        referenceAddress: String?
    ) {
        when {
            referenceName.isNullOrEmpty() -> {
                isReferenceNameEmpty.value = true
            }
            referenceDesignation.isNullOrEmpty() -> {
                isReferenceDesignationEmpty.value = true
            }
            organizationName.isNullOrEmpty() -> {
                isReferenceOrganizationNameEmpty.value = true
            }
            referenceMobile.isNullOrEmpty() -> {
                isReferenceMobileEmpty.value = true
            }
            referenceEmail.isNullOrEmpty() -> {
                isReferenceEmailEmpty.value = true
            }
            referenceAddress.isNullOrEmpty() -> {
                isReferenceAddressEmpty.value = true
            }

            coreCategoryID.isNullOrEmpty() -> {
                isRelationEmpty.value = true
            }

            else -> {
                val relationInfoDTO = PostReferenceInfo.RelationInfoDTO()
                relationInfoDTO.setCoreCategoryID(coreCategoryID) //3639

                val postReferenceInfo = PostReferenceInfo()
                postReferenceInfo.setRelationInfoDTO(relationInfoDTO)
                postReferenceInfo.setReferenceName(referenceName)
                postReferenceInfo.setReferenceDesignation(referenceDesignation)
                postReferenceInfo.setOrgainizationName(organizationName)
                postReferenceInfo.setReferenceMobile(referenceMobile)
                postReferenceInfo.setReferenceEmail(referenceEmail)
                postReferenceInfo.setReferenceAddress(referenceAddress)

                profileRepository.saveReferenceInfo(postReferenceInfo)
            }
        }
    }

    fun checkUpdateAcademicEducationInfo(
        serial: String?,
        educationInfoId: Int?,
        examTitle: String?,
        duration: String?,
        instituteName: String?,
        achievement: String?,
        foreignStatus: Int
    ) {
        when {
            serial.isNullOrEmpty() -> {
                isSerialEmpty.value = true
            }
            educationLevelId.isNullOrEmpty() -> {
                isEducationLevelEmpty.value = true
            }
            examTitle.isNullOrEmpty() -> {
                isExamEmpty.value = true
            }
            majorSubjectId.isNullOrEmpty() -> {
                isMajorEmpty.value = true
            }
            resultId.isNullOrEmpty() -> {
                isResultEmpty.value = true
            }
            passingYearId.isNullOrEmpty() -> {
                isPassingYearEmpty.value = true
            }
            duration.isNullOrEmpty() -> {
                isDurationEmpty.value = true
            }
            boardId.isNullOrEmpty() -> {
                isBoardEmpty.value = true
            }
            instituteName.isNullOrEmpty() -> {
                isInstituteEmpty.value = true
            }
            achievement.isNullOrEmpty() -> {
                isAchievementEmpty.value = true
            }
            else -> {
                val degreeInfoDTO = PostAcademicEducation.DegreeInfoDTO()
                degreeInfoDTO.setCoreCategoryID(educationLevelId!!)

                val subjectInfoDTO = PostAcademicEducation.SubjectInfoDTO()
                subjectInfoDTO.setCoreCategoryID(majorSubjectId!!)

                val gradeInfoDTO = PostAcademicEducation.GradeInfoDTO()
                gradeInfoDTO.setCoreCategoryID(resultId!!)

                val passingYearInfoDTO = PostAcademicEducation.PassingYearInfoDTO()
                passingYearInfoDTO.setCoreCategoryID(passingYearId!!)

                val boardInfoDTO = PostAcademicEducation.BoardInfoDTO()
                boardInfoDTO.setCoreCategoryID(boardId!!)

                val postAcademicEducation = PostAcademicEducation()
                postAcademicEducation.setDegreeInfoDTO(degreeInfoDTO)
                postAcademicEducation.setSubjectInfoDTO(subjectInfoDTO)
                postAcademicEducation.setGradeInfoDTO(gradeInfoDTO)
                postAcademicEducation.setPassingYearInfoDTO(passingYearInfoDTO)
                postAcademicEducation.setBoardInfoDTO(boardInfoDTO)
                postAcademicEducation.setEduInfoSerial(serial)
                postAcademicEducation.setEducationInfoId(educationInfoId) // Use for Update
                postAcademicEducation.setExamTitle(examTitle)
                postAcademicEducation.setEduDuration(duration)
                postAcademicEducation.setInstituteName(instituteName)
                postAcademicEducation.setAchievementDetails(achievement)
                postAcademicEducation.setInstituteForeginStatus(foreignStatus)

                profileRepository.updateAcademicEducation(postAcademicEducation)
            }
        }
    }

    fun checkUpdateTrainingInfo(
        trainingInfoId: Int,
        title: String?,
        topic: String?,
        instituteName: String?,
        location: String?,
        duration: String?
    ) {
        when {
            title.isNullOrEmpty() -> {
                isTrainingTitleEmpty.value = true
            }
            topic.isNullOrEmpty() -> {
                isTopicEmpty.value = true
            }
            instituteName.isNullOrEmpty() -> {
                isInstituteEmpty.value = true
            }
            location.isNullOrEmpty() -> {
                isLocationEmpty.value = true
            }
            countryId.isNullOrEmpty() -> {
                isCountryEmpty.value = true
            }
            passingYearId.isNullOrEmpty() -> {
                isPassingYearEmpty.value = true
            }
            duration.isNullOrEmpty() -> {
                isDurationEmpty.value = true
            }
            else -> {
                val countryInfoDTO = PostTrainingInfo.CountryInfoDTO()
                countryInfoDTO.setCoreCategoryID(countryId!!.toInt())

                val trainingYearInfoDTO = PostTrainingInfo.TrainingYearInfoDTO()
                trainingYearInfoDTO.setCoreCategoryID(passingYearId!!.toInt())

                val postTrainingInfo = PostTrainingInfo()
                postTrainingInfo.setCountryInfoDTO(countryInfoDTO)
                postTrainingInfo.setTrainingYearInfoDTO(trainingYearInfoDTO)
                postTrainingInfo.setTrainingInfoId(trainingInfoId)
                postTrainingInfo.setInstituteLocation(location)
                postTrainingInfo.setInstituteName(instituteName)
                postTrainingInfo.setTopicCoveredDetails(topic)
                postTrainingInfo.setTrainingDuration(duration)
                postTrainingInfo.setTrainingTitle(title)

                profileRepository.updateTrainingInfo(postTrainingInfo)
            }
        }
    }

    fun checkUpdateCertificationInfo(
        certificationName: String?,
        instituteName: String?,
        location: String?,
        date: String?,
        courseDuration: String?
    ) {
        when {
            certificationName.isNullOrEmpty() -> {
                isCertificationNameEmpty.value = true
            }
            instituteName.isNullOrEmpty() -> {
                isInstituteEmpty.value = true
            }
            location.isNullOrEmpty() -> {
                isLocationEmpty.value = true
            }
            countryId.isNullOrEmpty() -> {
                isCountryEmpty.value = true
            }
            date.isNullOrEmpty() -> {
                isDateEmpty.value = true
            }
            courseDuration.isNullOrEmpty() -> {
                isDurationEmpty.value = true
            }
            else -> {
                val countryInfoDTO = UpdateCertificationInfo.CountryInfoDTO()
                countryInfoDTO.setCoreCategoryID(countryId!!.toInt())

                val updateCertificationInfo = UpdateCertificationInfo()
                updateCertificationInfo.setCertificateInfoId(1)
                updateCertificationInfo.setCountryInfoDTO(countryInfoDTO)
                updateCertificationInfo.setAchieveDate(date)
                updateCertificationInfo.setCertificationName(certificationName)
                updateCertificationInfo.setCourseDuration(courseDuration)
                updateCertificationInfo.setInstituteLocation(location)
                updateCertificationInfo.setInstituteName(instituteName)

                profileRepository.updateCertificationInfo(updateCertificationInfo)
            }
        }
    }

    fun checkUpdateExperienceInfo(
        companyName: String?,
        companyBusiness: String?,
        designationName: String?,
        workingDepartment: String?,
        companyLocation: String?,
        employmentStart: String?,
        employmentEnd: String?,
        responsibilityDetails: String?

    ) {
        when {
            companyName.isNullOrEmpty() -> {
                isCompanyNameEmpty.value = true
            }
            companyBusiness.isNullOrEmpty() -> {
                isCompanyBusinessEmpty.value = true
            }
            designationName.isNullOrEmpty() -> {
                isDesignationNameEmpty.value = true
            }
            workingDepartment.isNullOrEmpty() -> {
                isWorkingDepartmentEmpty.value = true
            }
            companyLocation.isNullOrEmpty() -> {
                isCompanyLocationEmpty.value = true
            }
            employmentStart.isNullOrEmpty() -> {
                isEmploymentStartEmpty.value = true
            }
            employmentEnd.isNullOrEmpty() -> {
                isEmploymentEndEmpty.value = true
            }
            responsibilityDetails.isNullOrEmpty() -> {
                isResponsibilitiesDetailsEmpty.value = true
            }
            countryId.isNullOrEmpty() -> {
                isCountryEmpty.value = true
            }

            else -> {
                val countryInfoDTO = UpdateExperienceInfo.CountryInfoDTO()
                countryInfoDTO.setCoreCategoryID(countryId!!.toInt())

                val updateExperienceInfo = UpdateExperienceInfo()

                updateExperienceInfo.setCountryInfoDTO(countryInfoDTO)
                updateExperienceInfo.setExperienceId(1)
                updateExperienceInfo.setCompanyName(companyName)
                updateExperienceInfo.setCompanyBusiness(companyBusiness)
                updateExperienceInfo.setDesignationName(designationName)
                updateExperienceInfo.setWorkingDepartment(workingDepartment)
                updateExperienceInfo.setCompanyLocation(companyLocation)
                updateExperienceInfo.setEmploymentStart(employmentStart)
                updateExperienceInfo.setEmploymentEnd(employmentEnd)
                updateExperienceInfo.setResponsibilityDetails(responsibilityDetails)

                profileRepository.updateExperienceInfo(updateExperienceInfo)
            }
        }
    }

    fun checkUpdateReferenceInfo(
        referenceName: String?,
        referenceDesignation: String?,
        organizationName: String?,
        referenceMobile: String?,
        referenceEmail: String?,
        referenceAddress: String?
    ) {
        when {
            referenceName.isNullOrEmpty() -> {
                isReferenceNameEmpty.value = true
            }
            referenceDesignation.isNullOrEmpty() -> {
                isReferenceDesignationEmpty.value = true
            }
            organizationName.isNullOrEmpty() -> {
                isReferenceOrganizationNameEmpty.value = true
            }
            referenceMobile.isNullOrEmpty() -> {
                isReferenceMobileEmpty.value = true
            }
            referenceEmail.isNullOrEmpty() -> {
                isReferenceEmailEmpty.value = true
            }
            referenceAddress.isNullOrEmpty() -> {
                isReferenceAddressEmpty.value = true
            }

            coreCategoryID.isNullOrEmpty() -> {
                isRelationEmpty.value = true
            }

            else -> {
                val relationInfoDTO = UpdateReferenceInfo.RelationInfoDTO()
                relationInfoDTO.setCoreCategoryID(coreCategoryID!!.toInt())

                val updateReferenceInfo = UpdateReferenceInfo()
                updateReferenceInfo.setReferenceId(1)
                updateReferenceInfo.setRelationInfoDTO(relationInfoDTO)
                updateReferenceInfo.setReferenceName(referenceName)
                updateReferenceInfo.setReferenceDesignation(referenceDesignation)
                updateReferenceInfo.setOrgainizationName(organizationName)
                updateReferenceInfo.setReferenceMobile(referenceMobile)
                updateReferenceInfo.setReferenceEmail(referenceEmail)
                updateReferenceInfo.setReferenceAddress(referenceAddress)

                profileRepository.updateReferenceInfo(updateReferenceInfo)
            }
        }
    }

    fun checkUpdatePersonalInfo(
        netiId: String?,
        fathersName: String?,
        mothersName: String?,
        mobileNo: String?,
        birthday: String?,
        gender: String?,
        bloodGroup: String?,
        maritalStatusStatus: String?,
        noOfChild: String?,
        nationality: String?,
        nationalID: String?,
        passport: String?,
        birthCertificate: String?

    ) {
        when {
            fathersName.isNullOrEmpty() -> {
                isFatherNameEmpty.value = true
            }
            mothersName.isNullOrEmpty() -> {
                isMotherNameEmpty.value = true
            }
            mobileNo.isNullOrEmpty() -> {
                isMobileEmpty.value = true
            }
            birthday.isNullOrEmpty() -> {
                isBirthDayEmpty.value = true
            }
            gender.isNullOrEmpty() -> {
                isGenderEmpty.value = true
            }
            /*bloodGroup.isNullOrEmpty() -> {
                isBloodEmpty.value = true
            }*/
            maritalStatusStatus.isNullOrEmpty() -> {
                isMaritalStatusEmpty.value = true
            }
            noOfChild.isNullOrEmpty() -> {
                isNumberOfChildEmpty.value = true
            }
            nationality.isNullOrEmpty() -> {
                isNationalityEmpty.value = true
            }
            nationalID.isNullOrEmpty() -> {
                isNationalIdEmpty.value = true
            }

            passport.isNullOrEmpty() -> {
                isPassportEmpty.value = true
            }

            birthCertificate.isNullOrEmpty() -> {
                isBirthCertificationEmpty.value = true
            }

            bloodGroupId.isNullOrEmpty() -> {
                isBloodGroupEmpty.value = true
            }

            netiId.isNullOrEmpty() -> {
                isNetiIdEmpty.value = true
            }
            else -> {
                val profileInfoDTO = ProfileInformation_.UserDetailsInfoResponseDTO()
                profileInfoDTO.setFatherName(fathersName)
                profileInfoDTO.setMotherName(mothersName)
                profileInfoDTO.setMaritalStatus(maritalStatusStatus)
                profileInfoDTO.setNumberOfChild(noOfChild)
                profileInfoDTO.setNationality(nationality)
                profileInfoDTO.setNationalID(nationalID)
                profileInfoDTO.setPassportNo(passport)
                profileInfoDTO.setBirthCertificateNo(birthCertificate)

                val updateProfileInformation = ProfileInformation_()
                updateProfileInformation.setUserDetailsInfoResponseDTO(profileInfoDTO)
                updateProfileInformation.setBasicMobile(mobileNo)
                updateProfileInformation.setDateOfBirth(birthday)
                updateProfileInformation.setGender(gender)
                updateProfileInformation.setBloodGroup(bloodGroup)
                updateProfileInformation.setBloodGroup(bloodGroupId)
                updateProfileInformation.setNetiID(netiId.toInt()) // Use for Update neti ID

                profileRepository.updatePersonalInfo(updateProfileInformation)
            }
        }
    }

    fun checkUpdateAddressInfo(
        netiId: String?,
        addressDetails: String?,
        divisionName: String?,
        districtName: String?,
        upazillaName: String?,
        latitude: String?,
        longitude: String?

    ) {
        when {
            addressDetails.isNullOrEmpty() -> {
                isAddressDetailsEmpty.value = true
            }

            divisionName.isNullOrEmpty() -> {
                isDivisionEmpty.value = true
            }

            districtName.isNullOrEmpty() -> {
                isDistrictEmpty.value = true
            }

            upazillaName.isNullOrEmpty() -> {
                isUpazillaEmpty.value = true
            }

            /*latitude.isNullOrEmpty() -> {
                isLatitudeEmpty.value = true
            }
            longitude.isNullOrEmpty() -> {
                isLongitudeEmpty.value = true
            }*/

            else -> {
                val profileInfoDTO = ProfileInformation_.UserDetailsInfoResponseDTO()
                profileInfoDTO.setAddressDetails(addressDetails)
                profileInfoDTO.setLatitude(latitude)
                profileInfoDTO.setLongitude(longitude)
                val globalAreaInfoDTO = ProfileInformation_.GlobalAreaInfoDTO()
                globalAreaInfoDTO.setCoreCategoryID(upozillaId?.toInt())

                val updateProfileInformation = ProfileInformation_()
                updateProfileInformation.setUserDetailsInfoResponseDTO(profileInfoDTO)
                updateProfileInformation.setGlobalAreaInfoDTO(globalAreaInfoDTO)
                // updateProfileInformation.setDivision(divisionName)
                // updateProfileInformation.setDistrict(districtName)
                // updateProfileInformation.setUpazilla(upazillaName)
                updateProfileInformation.setNetiID(netiId?.toInt()) // Use for Update neti ID

                profileRepository.updatePersonalInfo(updateProfileInformation)
            }
        }
    }

    class ProfileViewModelFactory(val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ProfileViewModel::class.java)) {
                return ProfileViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }
    }
}