package com.netizen.netiworld.viewModel.UserPoint

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.model.UserPoint.ClassTest
import com.netizen.netiworld.model.UserPoint.StudentPortalPeriod
import com.netizen.netiworld.model.UserPoint.YearByTypeId
import com.netizen.netiworld.repository.UserPoint.SPortalProfileRepository

class SPortalProfileViewModel(application: Application) : AndroidViewModel(application) {

    private val sPortalProfileRepository = SPortalProfileRepository(application)

    val sPortalProfileInfoLiveData = sPortalProfileRepository.studentPortalProfileInfo
    val sProfileSubjectInfoAList = sPortalProfileRepository.studentPortalSubjectInfo
    private val sPortalAttendancePeriod = sPortalProfileRepository.studentPortalPeriodData
    private val sPortalAttendanceYear = sPortalProfileRepository.studentPortalYearInfo
    private val studentPortalClassTest = sPortalProfileRepository.studentPortalClassTest
    val studentPortalUnpaidFeesInfo = sPortalProfileRepository.studentPortalUnpaidFeesInfo
    val unpaidFeesList = sPortalProfileRepository.unpaidFeesList
    val studentPortalInventoryDetailsInfo =
        sPortalProfileRepository.studentPortalInventoryDetailsInfo
    val studentPortalClassTestDetails = sPortalProfileRepository.studentPortalClassTestDetails
    val monthsList = sPortalProfileRepository.monthsList

    var tempClassTestList = Transformations.map(studentPortalClassTest) {
        getTempClassTest(it)
    }

    private fun getTempClassTest(classTestList: List<ClassTest.Item>): ArrayList<String> {
        val tempClassTestList = ArrayList<String>()
        tempClassTestList.add("Select Class Test")

        classTestList.forEach { classTest -> tempClassTestList.add(classTest.getTestName()!!) }

        return tempClassTestList
    }

    var tempAttendanceYearList = Transformations.map(sPortalAttendanceYear) {
        getTempAttendanceYear(it)
    }

    private fun getTempAttendanceYear(yearList: List<YearByTypeId.Item>): ArrayList<String> {
        val tempAttendanceYearList = ArrayList<String>()
        tempAttendanceYearList.add("Select Year")

        yearList.forEach { year -> tempAttendanceYearList.add(year.getName()!!) }

        return tempAttendanceYearList
    }

    var tempAttendancePeriodList = Transformations.map(sPortalAttendancePeriod) {
        getTempAttendancePeriod(it)
    }

    private fun getTempAttendancePeriod(periodList: List<StudentPortalPeriod.Item>): ArrayList<String> {
        val tempAttendancePeriodList = ArrayList<String>()
        tempAttendancePeriodList.add("Select Period")

        periodList.forEach { period -> tempAttendancePeriodList.add(period.getName()!!) }

        return tempAttendancePeriodList
    }

    fun getSPortalProfileInfo() {
        if (sPortalProfileInfoLiveData.value == null) {
            sPortalProfileRepository.getStudentPortalProfile()
        }
    }

    fun getStudentPortalAtdPeriodData() {
        if (sPortalAttendancePeriod.value == null) {
            sPortalProfileRepository.getStudentPortalAttendancePeriodData()
        }
    }

    fun getStudentPortalAtdYearData() {
        sPortalProfileRepository.getStudentPortalYear()
    }

    fun getStudentPortalMonthData() {
        sPortalProfileRepository.spinnerStudentPortalMonth()
    }

    fun getSPortalSubjectInfo() {
        sPortalProfileRepository.getStudentPortalSubjectsData()
    }

    fun getSPortalClassTestInfo(instituteId: String, studentId: String, academicYear: String) {
        sPortalProfileRepository.getStudentPortalClassTest(instituteId, studentId, academicYear)
    }

    fun getStudentPortalUnpaidData(instituteId: String, studentId: String, academicYear: String) {
        if (studentPortalUnpaidFeesInfo.value == null) {
            sPortalProfileRepository.getStudentPortalUnpaidFees(
                instituteId,
                studentId,
                academicYear
            )
        }
    }

    fun getStudentPortalInventory(
        instituteId: String,
        studentId: String,
        year: String,
        monthName: String
    ) {
        if (studentPortalInventoryDetailsInfo.value == null) {
            sPortalProfileRepository.getStudentInventoryDetails(
                instituteId,
                studentId,
                year,
                monthName
            )
        }
    }

    fun spinnerStudentPortalUnpaidFees() {
        sPortalProfileRepository.spinnerStudentPortalUnpaidFees()
    }

    fun getStudentPortalClassTestDetails() {
        sPortalProfileRepository.getStudentPortalClassTestDetails()
    }

    @Suppress("UNCHECKED_CAST")
    class SPortalProfileViewModelFactory(private val application: Application) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SPortalProfileViewModel::class.java)) {
                return SPortalProfileViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel Class")
        }
    }
}