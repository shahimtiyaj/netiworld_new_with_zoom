package com.netizen.netiworld.viewModel

import android.app.Application
import android.text.TextUtils
import androidx.lifecycle.*
import com.netizen.netiworld.model.*
import com.netizen.netiworld.repository.UserRepository
import com.netizen.netiworld.model.ResetPassword

class UserViewModel(application: Application)  : AndroidViewModel(application) {

    private val userRepository = UserRepository(application)

    val isUserNameEmpty = MutableLiveData<Boolean>()
    val isPasswordEmpty = MutableLiveData<Boolean>()

    val isLoggedIn = userRepository.isLoggedIn
    var accessToken = userRepository.accessToken

    var districtList = userRepository.districtList
    var areaList = userRepository.areaList
    var coreCategoryIdDistrict: String? = null
    var coreCategoryIdArea: String? = null

    var isFullnameEmpty = MutableLiveData<Boolean>()
    var isGenderEmpty = MutableLiveData<Boolean>()
    var isReligionEmpty = MutableLiveData<Boolean>()
    var isBirthdayEmpty = MutableLiveData<Boolean>()
    var isMobileEmpty = MutableLiveData<Boolean>()
    var isEmailEmpty = MutableLiveData<Boolean>()
    var isDistrictEmpty = MutableLiveData<Boolean>()
    var isAreaEmpty = MutableLiveData<Boolean>()
    val isListFound = userRepository.isListFound

    var userInfoListForgotPass = userRepository.userInfoListForgotPass

    fun getUserInfoForgotPassword(userName: String) {
        userRepository.getUserInfoForgotPassword(userName)
    }

    fun getOtpForgotPassword(userContactNo: String, userInformation: UserCheckForgotPass?) {
      /*  val userInfo = UserCheckForgotPass()
        val userInformation =
            UserCheckForgotPass(
                userInfo.basicMobile,
                userInfo.basicEmail,
                userInfo.fullName,
                userInfo.customNetiID,
                userInfo.dateOfBirth,
                userInfo.userName,
                userInfo.netiID
            )*/


        userRepository.otpGetForgotPassword(userContactNo, userInformation)
    }

    fun getOTPVerifyGetForgotPassword(otpCode: String) {
        userRepository.otpVerifyGetForgotPassword(otpCode)
    }

    fun getResetPassword(userName: String, password: String, newPassword: String) {
                val resetPassword = ResetPassword(
                    userName,
                    password,
                    newPassword
                )
                userRepository.resetPassword(resetPassword)
    }

    fun getChangePassword(password: String, newPassword: String) {
        val resetPassword =
            ResetPassword(password, newPassword)
        userRepository.changePassword(resetPassword)
    }


    val tempDistrictList = Transformations.map(districtList) {
        getTempDistrict(it)
    }

    private fun getTempDistrict(districtList: List<DistrictModel>): ArrayList<String> {
        val tempDistrictList = ArrayList<String>()
        tempDistrictList.add("Select District")

        districtList.forEach { district ->
            tempDistrictList.add(district.getCategoryName().toString())
        }

        return tempDistrictList
    }

    val tempAreaList = Transformations.map(areaList) {
        getTempArea(it)
    }

    private fun getTempArea(areaList: List<AreaModel>): ArrayList<String> {
        val tempAreaList = ArrayList<String>()
        tempAreaList.add("Select Area")

        areaList.forEach { area ->
            tempAreaList.add(area.getCategoryName().toString())
        }

        return tempAreaList
    }

    fun getDistrictList() {
        userRepository.getDistrictList()
    }

    fun getCoreCategoryIdDistrict(selectItemName: String) {
        districtList.value!!.forEach { selectName ->
            if (selectName?.getCategoryName().toString().equals(selectItemName, true)) {
                coreCategoryIdDistrict = selectName.getCoreCategoryID().toString()
                userRepository.getAreaList(coreCategoryIdDistrict!!)

                return
            }
        }
    }


    fun getCoreCategoryIdArea(selectItemName: String) {
        areaList.value!!.forEach { selectName ->
            if (selectName?.getCategoryName().toString().equals(selectItemName, true)) {
                coreCategoryIdArea = selectName?.getCoreCategoryID().toString()
                return
            }
        }
    }

    init {
        isUserNameEmpty.value = false
        isPasswordEmpty.value = false
    }

    fun onLogInClick(userName: String, password: String) {

        when {
            TextUtils.isEmpty(userName) -> {
                isUserNameEmpty.value = true
            }
            TextUtils.isEmpty(password) -> {
                isPasswordEmpty.value = true
            }
            else -> {
                val loginUser = LoginUser(userName, password)
                userRepository.loginAuthentication(loginUser)
            }
        }
    }

    fun onSignUpClick(
        fullName: String?, gender: String?,
        religion: String?,
        dateOfBirth: String?,
        basicMobile: String?,
        basicEmail: String?,
        userName: String?,
        userPassword: String?
    ) {
        val signUpUser = RegistrationPost(
            fullName,
            gender,
            religion,
            dateOfBirth,
            basicMobile,
            basicEmail,
            userName,
            userPassword,
            RegistrationPost.GlobalAreaInfoDTO(coreCategoryIdArea?.toInt())
        )

        when {
            fullName.isNullOrEmpty() -> {
                isFullnameEmpty.value = true
            }
            gender.isNullOrEmpty() -> {
                isGenderEmpty.value = true
            }
            religion.isNullOrEmpty() -> {
                isReligionEmpty.value = true
            }

            dateOfBirth.isNullOrEmpty() -> {
                isBirthdayEmpty.value = true
            }

            basicMobile.isNullOrEmpty() -> {
                isMobileEmpty.value = true
            }

            basicEmail.isNullOrEmpty() -> {
                isEmailEmpty.value = true
            }

            userName.isNullOrEmpty() -> {
                isUserNameEmpty.value = true
            }

            userPassword.isNullOrEmpty() -> {
                isPasswordEmpty.value = true
            }
            coreCategoryIdDistrict.isNullOrEmpty() -> {
                isDistrictEmpty.value = true
            }

            coreCategoryIdArea.isNullOrEmpty() -> {
                isAreaEmpty.value = true
            }
            else -> {
                userRepository.registrationUser(signUpUser)
            }
        }
    }

    class SignInViewModelFactory(val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(UserViewModel::class.java)) {
                return UserViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }
    }
}