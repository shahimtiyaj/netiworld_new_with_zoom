package com.netizen.netiworld.viewModel

import android.app.Application
import androidx.lifecycle.*
import com.netizen.netiworld.model.MessageType
import com.netizen.netiworld.model.MessageType1
import com.netizen.netiworld.model.PurchasePoint
import com.netizen.netiworld.model.RevenueLogPostData
import com.netizen.netiworld.repository.MessageRepository
import com.netizen.netiworld.repository.PurchaseRepository

class MessageViewModel(application: Application) : AndroidViewModel(application) {

    private val messageBalanceRepository = MessageRepository(application)

    private val purchasePointArrayList = messageBalanceRepository.purchasePointArrayList
    val isPurchasePointEmpty = MutableLiveData<Boolean>()
    val isMessageTypeEmpty = MutableLiveData<Boolean>()
    var purchasePointRoleId: String? = null
    var productId: String? = null
    val unitPrice = MutableLiveData<String>()
    val percentageVat = MutableLiveData<String>()
    val productRoleAssignID1 = MutableLiveData<String>()
    val messageRechargeInfo = messageBalanceRepository.messageRechargeInfo
    val revenueLogList = messageBalanceRepository.revenueLogList
    val messageTypeInfo = messageBalanceRepository.messageTypeInfo

    var purchasePointId = MutableLiveData<String>()

    var isFromDateEmpty = MutableLiveData<Boolean>()
    var isToDateEmpty = MutableLiveData<Boolean>()
    val isRevenueLogDataFound = messageBalanceRepository.isRevenueLogDataFound
    val isMessageLogDataFound = messageBalanceRepository.isMessageLogDataFound

    //Purchase Point--------------------------------------
    val tempPurchasePointList = Transformations.map(purchasePointArrayList) {
        getTempPurchasePointList(it)
    }

    private fun getTempPurchasePointList(purchasePointList: List<PurchasePoint>): ArrayList<String> {
        val tempPurchasePointList = ArrayList<String>()
        tempPurchasePointList.add("Select Purchase Point")

        purchasePointList.forEach { purchasePoint -> tempPurchasePointList.add(purchasePoint.getCoreRoleName().toString()) }

        return tempPurchasePointList
    }

    fun getPurchasePointData() {
        if (purchasePointArrayList.value.isNullOrEmpty()) {
            messageBalanceRepository.getPurchasePointDataFromServer()
        }
    }

    fun getPurchasePointId(pointName: String) {
        purchasePointArrayList.value?.forEach { purchasePoint ->
            if (purchasePoint.getCoreRoleName().equals(pointName, true)) {
                purchasePointId.value = purchasePoint.getCoreRoleID().toString()
                return
            }
        }
    }

    fun getRoleId(selectItemName: String) {
        purchasePointArrayList.value!!.forEach { selectName ->
            if (selectName.getCoreRoleName().toString().equals(selectItemName, true)) {
                purchasePointRoleId = selectName.getCoreRoleID().toString()
                messageBalanceRepository.getMessageTypeData(purchasePointRoleId!!)
                return
            }
        }
    }

    //Message Type---------------------------------------

    val tempMessageTypeList1 = Transformations.map(messageTypeInfo) {
        getTempMessageTypeList1(it)
    }

    private fun getTempMessageTypeList1(messageTypeList: List<MessageType1?>): ArrayList<String> {
        val tempMessageTypeList = ArrayList<String>()
        tempMessageTypeList.add("Select Message Type")

        messageTypeList.forEach { messageType -> tempMessageTypeList.add(messageType?.productInfoDTO?.getProductName().toString()) }

        return tempMessageTypeList
    }

    fun getProductId1(selectItemName: String) {
        messageTypeInfo.value!!.forEach { selectName ->
            if (selectName?.productInfoDTO?.getProductName().toString().equals(selectItemName, true)) {
                productId = selectName?.productInfoDTO?.getProductID().toString()
                unitPrice.postValue(selectName?.productInfoDTO?.getSalesPrice().toString())
                percentageVat.postValue(selectName?.productInfoDTO?.getPercentVat().toString())
                productRoleAssignID1.postValue(selectName?.productRoleAssignID)

                return
            }
        }
    }

    fun checkMessageRechargeValues(messageQuantity: String, productRoleAssignID: String) {
        when {
            /*purchasePointId.value.isNullOrEmpty() -> {

            }
            purchasePointRoleId.isNullOrEmpty() -> {
                isPurchasePointEmpty.value = true
            }*/
            productId.isNullOrEmpty() -> {
                isMessageTypeEmpty.value = true
            }
            else -> {
                messageBalanceRepository.submitMessageRechargeData(messageQuantity, productId!!, productRoleAssignID)
            }
        }
    }

    //Message Recharge Information Reports -------------------------------------------
    fun getMessageRechargeInfoList(startDateMessage: String, endDateMessage: String) {
        if (messageRechargeInfo.value == null) {
            messageBalanceRepository.getMessageRechargeListData(startDateMessage, endDateMessage)
        }
    }

    fun checkRevenueLogData(fromDate: String?, toDate: String?) {
        when {
            fromDate.isNullOrEmpty() -> {
                isFromDateEmpty.value = true
            }
            toDate.isNullOrEmpty() -> {
                isToDateEmpty.value = true
            }
            else -> {
                val revenueLogPostData = RevenueLogPostData()
                revenueLogPostData.setStartDate(fromDate)
                revenueLogPostData.setEndDate(toDate)

                messageBalanceRepository.getRevenueLogData(revenueLogPostData)
            }
        }
    }

    fun checkMessageLogData(fromDate: String?, toDate: String?) {
        when {
            fromDate.isNullOrEmpty() -> {
                isFromDateEmpty.value = true
            }
            toDate.isNullOrEmpty() -> {
                isToDateEmpty.value = true
            }
            else -> {

                messageBalanceRepository.getMessageRechargeListData(fromDate,toDate)
            }
        }
    }

    class MessageBalanceViewModelFactory(val application: Application) :
        ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(MessageViewModel::class.java)) {
                return MessageViewModel(
                    application
                ) as T
            }
            throw IllegalStateException("Unknown ViewModel class")
        }
    }
}