package com.netizen.netiworld.viewModel

import android.app.Application
import androidx.lifecycle.*
import com.netizen.netiworld.model.BalanceDepositAccountInfo
import com.netizen.netiworld.model.BalanceReportPostData
import com.netizen.netiworld.model.DepositPostData
import com.netizen.netiworld.model.WithdrawPostData
import com.netizen.netiworld.repository.WalletRepository
import com.netizen.netiworld.utils.Loaders

class WalletViewModel(application: Application) : AndroidViewModel(application) {

    private val walletRepository = WalletRepository(application)

    private val accountInfoList = walletRepository.accountInfoList
    val withDrawInfo = walletRepository.withDrawInfo
    val fullName = walletRepository.fullName
    val PhoneNo = walletRepository.phoneNo
    val netiMainID = walletRepository.netiMainID
    val customNetiId = walletRepository.customNetiId
     val balanceStatementArrayList = walletRepository.balanceStatementArrayList
    val balanceTransferArrayList = walletRepository.balanceTransferArrayList

    val balanceDepositReportDataList = walletRepository.balanceDepositReportDataList
    val balanceTransferReportDataList = walletRepository.balanceTransferReportDataList

    var isMobileBanking = MutableLiveData<Boolean>()
    var isAccountEmpty = MutableLiveData<Boolean>()
    var isDepositAmountEmpty = MutableLiveData<Boolean>()
    var isDepositDateEmpty = MutableLiveData<Boolean>()
    var isMobileBankingNoEmpty = MutableLiveData<Boolean>()
    var isTransactionIdEmpty = MutableLiveData<Boolean>()
    var isBranchEmpty = MutableLiveData<Boolean>()
    var isDepositTypeEmpty = MutableLiveData<Boolean>()
    var isNoteEmpty = MutableLiveData<Boolean>()
    var isImageEmpty = MutableLiveData<Boolean>()
    var isFromDateEmpty = MutableLiveData<Boolean>()
    var isToDateEmpty = MutableLiveData<Boolean>()
    val isDepositDataFound = walletRepository.isDepositDataFound
    val isWithdrawDataFound = walletRepository.isWithdrawDataFound
    val isTransferDataFound = walletRepository.isTransferDataFound

    val isBalanceStatementDataFound = walletRepository.isBalanceSatementDataFound

    var tempAccountNumberList = Transformations.map(accountInfoList) {
        getTempAccountInfoList(it)
    }

    private var coreBankAccountId: String? = null

    private fun getTempAccountInfoList(bankBalanceDepositAccountInfoList: List<BalanceDepositAccountInfo>): ArrayList<String> {
        val tempBankAccountList = ArrayList<String>()
        tempBankAccountList.add("Select Account")

        bankBalanceDepositAccountInfoList.forEach { accountInfo ->
            tempBankAccountList.add(accountInfo.getAccShortName().toString())
        }

        return tempBankAccountList
    }

    fun getAccountInfoList() {
        if (accountInfoList.value.isNullOrEmpty()) {
            walletRepository.getAccountInfo()
        }
    }

    fun getWithdrawInfoList() {
        if (withDrawInfo.value == null) {
            walletRepository.getWithdrawInfo()
        }
    }

    fun checkAccountType(accountName: String) {
        accountInfoList.value?.forEach { account ->
            if (account.getAccShortName().equals(accountName, true)) {
                coreBankAccountId = account.getCoreBankAccId().toString()
                isMobileBanking.value = account.getCoreCategoryInfoDTO()?.getParentTypeInfoDTO()?.getCategoryName()
                    .equals("Mobile Banking", true)
            }
        }
    }

    fun checkMobileDepositData(
        depositAmount: String?,
        depositDate: String?,
        mobileBankingNo: String?,
        transactionId: String?
    ) {
        when {
            coreBankAccountId.isNullOrEmpty() -> {
                isAccountEmpty.value = true
            }
            depositAmount.isNullOrEmpty() -> {
                isDepositAmountEmpty.value = true
            }
            depositDate.isNullOrEmpty() -> {
                isDepositDateEmpty.value = true
            }
            mobileBankingNo.isNullOrEmpty() -> {
                isMobileBankingNoEmpty.value = true
            }
            transactionId.isNullOrEmpty() -> {
                isTransactionIdEmpty.value = true
            }
            else -> {
                val coreBankAccountInfoDTO = DepositPostData.CoreBankAccountInfoDTO()
                coreBankAccountInfoDTO.setCoreBankAccId(coreBankAccountId!!.toInt())

                val depositPostData = DepositPostData()
                depositPostData.setCoreBankAccountInfoDTO(coreBankAccountInfoDTO)
                depositPostData.setRequestedAmount(depositAmount)
                depositPostData.setTransactionDate(depositDate)
                depositPostData.setFromWhere(mobileBankingNo)
                depositPostData.setTransactionNumber(transactionId)

                walletRepository.submitDepositPostData(depositPostData)
            }
        }
    }

    fun checkAccountDepositData(
        depositAmount: String?,
        depositDate: String?,
        branchName: String?,
        depositType: String?,
        note: String?,
        image: String?
    ) {
        when {
            coreBankAccountId == null -> {
                isAccountEmpty.value = true
            }
            depositAmount.isNullOrEmpty() -> {
                isDepositAmountEmpty.value = true
            }
            depositDate.isNullOrEmpty() -> {
                isDepositDateEmpty.value = true
            }
            branchName.isNullOrEmpty() -> {
                isBranchEmpty.value = true
            }
            depositType.isNullOrEmpty() -> {
                isDepositTypeEmpty.value = true
            }
            note.isNullOrEmpty() -> {
                isNoteEmpty.value = true
            }
            image.isNullOrEmpty() -> {
                isImageEmpty.value = true
            }
            else -> {
                val coreBankAccountInfoDTO = DepositPostData.CoreBankAccountInfoDTO()
                coreBankAccountInfoDTO.setCoreBankAccId(coreBankAccountId!!.toInt())

                val depositPostData = DepositPostData()
                depositPostData.setCoreBankAccountInfoDTO(coreBankAccountInfoDTO)
                depositPostData.setRequestedAmount(depositAmount)
                depositPostData.setTransactionDate(depositDate)
                depositPostData.setFromWhere(branchName)
                depositPostData.setRequestNote(note)
                depositPostData.setPaymentType(depositType)
                depositPostData.setAttachFileContent(image)
                depositPostData.setAttachFileSaveOrEditable(true)

                walletRepository.submitDepositPostData(depositPostData)
            }
        }
    }

    fun checkWithdrawData(withdrawAmount: String?, note: String?) {
        when {
            withDrawInfo.value?.getUserBankAccountInfoDTO()
                ?.getCoreBankBranchInfoDTO()?.getCoreBankInfoDTO()?.getCategoryName().isNullOrEmpty() -> {
                Loaders.error?.value = "Bank name is empty!"
            }
            withDrawInfo.value?.getUserBankAccountInfoDTO()
                ?.getCoreBankBranchInfoDTO()?.getBranchName().isNullOrEmpty() -> {
                Loaders.error?.value = "Branch name is empty!"
            }
            withDrawInfo.value?.getUserBankAccountInfoDTO()?.getBankAccHolderName().isNullOrEmpty() -> {
                Loaders.error?.value = "Account holder name is empty!"
            }
            withDrawInfo.value?.getUserBankAccountInfoDTO()?.getBankAccNumber().isNullOrEmpty() -> {
                Loaders.error?.value = "Account number is empty!"
            }
            withdrawAmount.isNullOrEmpty() -> {
                Loaders.error?.value = "Withdraw amount is empty!"
            }
            note.isNullOrEmpty() -> {
                Loaders.error?.value = "Note is empty!"
            }
            else -> {
                val userBankAccountInfoDTO = WithdrawPostData.UserBankAccountInfoDTO()
                userBankAccountInfoDTO.setUserBankAccId(withDrawInfo.value?.getUserBankAccountInfoDTO()?.getUserBankAccId())

                val withdrawPostData = WithdrawPostData()
                withdrawPostData.setRequestedAmount(withdrawAmount)
                withdrawPostData.setRequestNote(note)
                withdrawPostData.setUserBankAccountInfoDTO(userBankAccountInfoDTO)

                walletRepository.submitWithdrawData(withdrawPostData)
            }
        }
    }

    //----------------------------------------Balance Transfer

    fun checkTransferNetimanInfo(netiId: String?) {

        when {
            netiId?.isEmpty()!! -> {
                Loaders.error.value = "Custom neti ID can't left empty."
            }
            else -> {
                walletRepository.getPersonInfoForWalletTransfer(netiId)
            }

        }
    }

    fun checkForRequestOTP(TransferAmount: String, netiMainID: String?, note: String, inputNetiID: String?, customNetiID: String?) {

        when {
            TransferAmount?.isEmpty()!! -> {
                Loaders.error.value = "Transfer Amount can't left empty."
            }
            inputNetiID?.isEmpty()!! -> {
                Loaders.error.value = "Custom neti ID can't left empty."
            }

            /*inputNetiID==customNetiID -> {
                Loaders.apiError.value = "You can't transfer balance to your own account"
            }*/
            TransferAmount.equals("0")-> {
                Loaders.error.value = "Transfer amount can't be 0"
            }
            else -> {
                walletRepository.requestForOTP(TransferAmount, netiMainID, note)
            }
        }
    }

    fun checkForOTPVarification(OTP: String) {

        when {
            OTP.isEmpty()-> {
                Loaders.error.value = "Please Enter your OTP"
            }
            else -> {
                walletRepository.varifyOTP(OTP)
            }
        }
    }

    fun checkTransferWalletBalance(TransferAmount: String, netiMainID: String?, note: String) {

        when {
            TransferAmount?.isEmpty()!! -> {
                Loaders.error.value = "Transfer Amount can't left empty."
            }
            netiMainID?.isEmpty()!! -> {
                Loaders.error.value = "Custom neti ID can't left empty."
            }

            TransferAmount.equals("0")-> {
                Loaders.error.value = "Transfer amount can't be 0"
            }

            else -> {
                walletRepository.transferWalletBalance(TransferAmount, netiMainID, note)
            }
        }
    }

    fun checkBalanceReportPostData(fromDate: String?, toDate: String?, type: String) {
        when {
            fromDate.isNullOrEmpty() -> {
                isFromDateEmpty.value = true
            }
            toDate.isNullOrEmpty() -> {
                isToDateEmpty.value = true
            }
            else -> {
                val balanceReportPostData = BalanceReportPostData()
                balanceReportPostData.setRequestStartDate(fromDate)
                balanceReportPostData.setRequestEndDate(toDate)
                balanceReportPostData.setRequestType(type)
                balanceReportPostData.setTransactionType(null)
                balanceReportPostData.setStatus(5)
                balanceReportPostData.setLimit(100)

                walletRepository.getBalanceReportData(balanceReportPostData, type)
            }
        }
    }

    fun checkBalanceTransferReportPostData(fromDate: String?, toDate: String?) {
        when {
            fromDate.isNullOrEmpty() -> {
                isFromDateEmpty.value = true
            }
            toDate.isNullOrEmpty() -> {
                isToDateEmpty.value = true
            }
            else -> {
                val balanceReportPostData = BalanceReportPostData()
                balanceReportPostData.setRequestStartDate(fromDate)
                balanceReportPostData.setRequestEndDate(toDate)
                balanceReportPostData.setTransactionType(null)
                balanceReportPostData.setStatus(5)
                balanceReportPostData.setLimit(100)

                walletRepository.getBalanceTransferReportData(balanceReportPostData)
            }
        }
    }

    //Wallet Balance Statement Reports----------------------------------------
    fun getBalanceStatementReportList(startDate: String, endDate: String) {
        if (balanceStatementArrayList.value.isNullOrEmpty()) {
            walletRepository.getBalanceStatementListData(startDate, endDate)
        }
    }

    fun checkBalanceStatementReportData(fromDate: String?, toDate: String?) {
        when {
            fromDate.isNullOrEmpty() -> {
                isFromDateEmpty.value = true
            }
            toDate.isNullOrEmpty() -> {
                isToDateEmpty.value = true
            }
            else -> {
                walletRepository.getBalanceStatementListData(fromDate, toDate)
            }
        }
    }

    class WalletViewModelFactory(private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(WalletViewModel::class.java)) {
                return WalletViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel Class")
        }
    }
}