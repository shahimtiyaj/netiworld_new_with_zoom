package com.netizen.netiworld.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.netizen.netiworld.repository.HomeRepository

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    private val homeRepository = HomeRepository(application)

    val homePageInfoLiveData = homeRepository.homePageInfoLiveData

    val isMyProfileClicked = MutableLiveData<Boolean>()
    val isNetiMailClicked = MutableLiveData<Boolean>()
    val isSupportTokenClicked = MutableLiveData<Boolean>()
    val isAddPointClicked = MutableLiveData<Boolean>()

    val isWalletClicked = MutableLiveData<Boolean>()
    val isMessageClicked = MutableLiveData<Boolean>()
    val isGeneralProductClicked = MutableLiveData<Boolean>()
    val isOfferProductClicked = MutableLiveData<Boolean>()
    val isMessageLogClicked = MutableLiveData<Boolean>()
    val isBankClicked = MutableLiveData<Boolean>()
    val isRevenueReportClicked = MutableLiveData<Boolean>()
    val isGeneralProductReportClicked = MutableLiveData<Boolean>()
    val isStatementReportClicked = MutableLiveData<Boolean>()
    val isPurchaseCodeReportClicked = MutableLiveData<Boolean>()
    val isOfferProductReportClicked = MutableLiveData<Boolean>()
    val isWalletLogReportClicked = MutableLiveData<Boolean>()

    val isAddPortalClicked = MutableLiveData<Boolean>()
    val isInstituteProfileClicked = MutableLiveData<Boolean>()
    val isStudentProfileClicked = MutableLiveData<Boolean>()
    val isStudentAttendanceClicked = MutableLiveData<Boolean>()
    val isSubjectClicked = MutableLiveData<Boolean>()
    val isRoutineInfoClicked = MutableLiveData<Boolean>()
    val isExamInfoClicked = MutableLiveData<Boolean>()
    val isFeesClicked = MutableLiveData<Boolean>()
    val isInventoryClicked = MutableLiveData<Boolean>()


    fun getHomePageInfo() {
        if (homePageInfoLiveData.value == null) {
            homeRepository.getHomePageInfo()
        }
    }

    @Suppress("UNCHECKED_CAST")
    class HomeViewModelFactory(private val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
                return HomeViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel Class")
        }
    }
}