package com.netizen.netiworld.viewModel

import android.app.Application
import androidx.lifecycle.*
import com.netizen.netiworld.model.GeneralProductPostData
import com.netizen.netiworld.model.Products
import com.netizen.netiworld.model.ProductsOfferReportPostData
import com.netizen.netiworld.repository.PurchaseRepository
import com.netizen.netiworld.utils.Loaders

class PurchaseViewModel(application: Application) : AndroidViewModel(application) {

    private val purchaseRepository = PurchaseRepository(application)

    val code = purchaseRepository.code
    var codeUsable = purchaseRepository.codeUsable
    var codeUsed = purchaseRepository.codeUsed
    var productQuantity = purchaseRepository.productQnty
    var unitPrice = purchaseRepository.unitPrice
    var productName = purchaseRepository.productName
    var discountAmtPer = purchaseRepository.discountAmtPer
    var productOfferID = purchaseRepository.productOfferID

    var totalPrice = MutableLiveData<Double>()
    var vatAmount = MutableLiveData<Double>()
    var payableAmount = MutableLiveData<Double>()
    var isFromDateEmpty = MutableLiveData<Boolean>()
    var isToDateEmpty = MutableLiveData<Boolean>()
    val isDataFound = purchaseRepository.isDataFound

    var purchasePointId: String? = null
    private var product: Products? = null

    private val productList = purchaseRepository.productList
    val generalProductReportList = purchaseRepository.generalProductReportList

    val unusedPurchaseLogList = purchaseRepository.unusedPurchaseLogList
    val isUnusedPurchaseLogDataFound = purchaseRepository.isUnusedPurchaseLogDataFound
    val productOfferReportList = purchaseRepository.productOfferReportList

    val tempProductList = Transformations.map(productList) {
        getTempProducts(it)
    }

    fun getOfferProduct(offerCode: String) {
        purchaseRepository.getOfferProductData(offerCode)
    }

    fun getProductList(roleId: String) {
        purchaseRepository.getProductsByRole(roleId)
    }

    private fun getTempProducts(productList: List<Products>): ArrayList<String> {
        val tempProductList = ArrayList<String>()
        tempProductList.add("Select Product")

        productList.forEach { product ->
            tempProductList.add(product.getProductInfoDTO()?.getProductName().toString())
        }

        return tempProductList
    }

    fun submitOfferProduct(
        unitPrice: String?,
        payableAmt: String?,
        discountAmt: String?,
        totalAmount: String?,
        productOfferID: String?
    ) {

        purchaseRepository.submitOfferProductData(
            unitPrice,
            payableAmt,
            discountAmt,
            totalAmount,
            productOfferID
        )
    }

    fun getProduct(productName: String) {
        productList.value?.forEach { product ->
            if (product.getProductInfoDTO()?.getProductName().equals(productName, true)) {
                this.product = product
                productQuantity.value = "0"
                totalCalculation(0)
                return
            }
        }
    }

    fun totalCalculation(quantity: Int) {
        productQuantity.value = quantity.toString()

        val totalPrice = product?.getProductInfoDTO()?.getSalesPrice()?.times(quantity)
        val vatAmount = product?.getProductInfoDTO()?.getPercentVat()?.times(totalPrice!!)?.div(100)
        val totalPayable = totalPrice?.plus(vatAmount!!)

        unitPrice.value = product?.getProductInfoDTO()?.getSalesPrice().toString()
        this.totalPrice.value = totalPrice
        this.vatAmount.value = vatAmount
        payableAmount.value = totalPayable
    }

    fun checkPurchaseData() {
        when {
            purchasePointId.isNullOrEmpty() -> {
                Loaders.error.value = "Purchase point is empty!"
            }
            product == null -> {
                Loaders.error.value = "Product is empty!"
            }
            productQuantity.value.equals("0") -> {
                Loaders.error.value = "Product quantity is empty!"
            }
//            unitPrice.value.isNullOrEmpty() -> {
//                Loaders.error.value = "Unit price is empty!"
//            }
//            totalPrice.value == 0 -> {
//                Loaders.error.value = "Total price is empty!"
//            }
//            vatAmount.value == 0 -> {
//                Loaders.error.value = "Vat amount is empty!"
//            }
//            payableAmount.value == 0 -> {
//                Loaders.error.value = "Payable amount is empty!"
//            }
            else -> {
                val productInfoDTO = GeneralProductPostData.ProductInfoDTO()
                productInfoDTO.setProductID(product?.getProductInfoDTO()?.getProductID())

                val productRoleAssignDTO = GeneralProductPostData.ProductRoleAssignDTO()
                productRoleAssignDTO.setProductRoleAssignID(product?.getProductRoleAssignID())

                val generalProductPostData = GeneralProductPostData()
                generalProductPostData.setUnitPrice(unitPrice.value!!.toDouble())
                generalProductPostData.setTotalAmount(totalPrice.value!!)
                generalProductPostData.setVatAmount(vatAmount.value!!)
                generalProductPostData.setPaidAmount(payableAmount.value!!)
                generalProductPostData.setPurchaseQuantity(productQuantity.value!!.toInt())
                generalProductPostData.setProductRoleAssignDTO(productRoleAssignDTO)
                generalProductPostData.setProductInfoDTO(productInfoDTO)

                purchaseRepository.submitGeneralProductPostData(generalProductPostData)
            }
        }
    }

    fun checkGeneralProductReportDate(fromDate: String?, toDate: String?) {
        when {
            fromDate.isNullOrEmpty() -> {
                isFromDateEmpty.value = true
            }
            toDate.isNullOrEmpty() -> {
                isToDateEmpty.value = true
            }
            else -> {
                purchaseRepository.getGeneralProductReportData(fromDate, toDate)
            }
        }
    }

    fun getUnusedPurchaseCode(usedStatus: Int?) {
        purchaseRepository.getPurchaseUnusedData(usedStatus)
    }

    fun checkOfferProductReportDate(fromDate: String?, toDate: String?) {
        when {
            fromDate.isNullOrEmpty() -> {
                isFromDateEmpty.value = true
            }
            toDate.isNullOrEmpty() -> {
                isToDateEmpty.value = true
            }
            else -> {
                val productsOfferReportPostData = ProductsOfferReportPostData()
                productsOfferReportPostData.setStartDate(fromDate)
                productsOfferReportPostData.setEndDate(toDate)
                productsOfferReportPostData.setPageLimit(100)
                productsOfferReportPostData.setPageNo(0)

                purchaseRepository.getProductProductReportData(productsOfferReportPostData)
            }
        }
    }

    class PurchaseViewModelFactory(val application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(PurchaseViewModel::class.java)) {
                return PurchaseViewModel(
                    application
                ) as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }
    }
}