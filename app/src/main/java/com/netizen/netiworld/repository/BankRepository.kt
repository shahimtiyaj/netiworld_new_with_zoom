package com.netizen.netiworld.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.netizen.netiworld.apiService.ApiClient
import com.netizen.netiworld.apiService.ApiInterface
import com.netizen.netiworld.model.*
import com.netizen.netiworld.utils.AppPreferences
import com.netizen.netiworld.utils.Loaders
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BankRepository(private val application: Application) {

    private val appPreferences = AppPreferences(application)

    var usingPurposeList = MutableLiveData<List<UsingPurpose>>()
    var bankList = MutableLiveData<List<BankAccountInfo>>()
    var districtList = MutableLiveData<List<BankAccountInfo>>()
    var branchList = MutableLiveData<List<BankBranchInfo>>()
    var userBankAccountList = MutableLiveData<List<BankList>>()
    var tagList = MutableLiveData<List<TagGetData>>()
    var tagTypeList = MutableLiveData<List<TagType>>()

    var isUsingPurposeListFound = MutableLiveData<Boolean>()
    var isBankAccountListFound = MutableLiveData<Boolean>()
    var isDistrictListFound = MutableLiveData<Boolean>()
    var isBranchListFound = MutableLiveData<Boolean>()

    fun getUsingPurposeList() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getUsingPurposeList("bearer" + appPreferences.getToken(), "T123")

        call.enqueue(object: Callback<List<UsingPurpose>> {
            override fun onFailure(call: Call<List<UsingPurpose>>, t: Throwable) {
                Loaders.isLoading0.value = false
                isUsingPurposeListFound.value = false
                Loaders.apiError.value = "Couldn't get the using purpose list! Please try again."
            }

            override fun onResponse(
                call: Call<List<UsingPurpose>>,
                response: Response<List<UsingPurpose>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<UsingPurpose>>() {}.type
                        val errorResponse: List<UsingPurpose>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        usingPurposeList.value = errorResponse
                        isUsingPurposeListFound.value = true
                    } else {
                        Loaders.apiError.value = "No value found!"
                        isUsingPurposeListFound.value = false
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    isUsingPurposeListFound.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun getBankList() {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getBankAccountList("bearer" + appPreferences.getToken(), "T100")

        call.enqueue(object: Callback<List<BankAccountInfo>> {
            override fun onFailure(call: Call<List<BankAccountInfo>>, t: Throwable) {
                Loaders.isLoading0.value = false
                isBankAccountListFound.value = false
                Loaders.apiError.value = "Couldn't get the account list! Please try again."
            }

            override fun onResponse(
                call: Call<List<BankAccountInfo>>,
                response: Response<List<BankAccountInfo>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<BankAccountInfo>>() {}.type
                        val errorResponse: List<BankAccountInfo>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        bankList.value = errorResponse
                        isBankAccountListFound.value = true
                    } else {
                        Loaders.apiError.value = "No value found!"
                        isBankAccountListFound.value = false
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    isBankAccountListFound.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun getDistrictList() {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getBankDistrictList("bearer" + appPreferences.getToken(), "T10203")

        call.enqueue(object: Callback<List<BankAccountInfo>> {
            override fun onFailure(call: Call<List<BankAccountInfo>>, t: Throwable) {
                Loaders.isLoading0.value = false
                isDistrictListFound.value = false
                Loaders.apiError.value = "Couldn't get the district list! Please try again."
            }

            override fun onResponse(
                call: Call<List<BankAccountInfo>>,
                response: Response<List<BankAccountInfo>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<BankAccountInfo>>() {}.type
                        val errorResponse: List<BankAccountInfo>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        districtList.value = errorResponse
                        isDistrictListFound.value = true
                    } else {
                        Loaders.apiError.value = "No value found!"
                        isDistrictListFound.value = false
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    isDistrictListFound.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun getBranchList(bankId: String, districtId: String) {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getBankBranchList("bearer" + appPreferences.getToken(), bankId, districtId)

        call.enqueue(object: Callback<List<BankBranchInfo>> {
            override fun onFailure(call: Call<List<BankBranchInfo>>, t: Throwable) {
                Loaders.isLoading0.value = false
                isBranchListFound.value = false
                Loaders.apiError.value = "Couldn't get the branch list! Please try again."
            }

            override fun onResponse(
                call: Call<List<BankBranchInfo>>,
                response: Response<List<BankBranchInfo>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<BankBranchInfo>>() {}.type
                        val errorResponse: List<BankBranchInfo>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        branchList.value = errorResponse
                        isBranchListFound.value = true
                    } else {
                        Loaders.apiError.value = "No value found!"
                        isBranchListFound.value = false
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    isBranchListFound.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun submitBankInfo(bankAccountPostData: BankAccountPostData) {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.submitBankAccount("bearer" + appPreferences.getToken(), bankAccountPostData)

        call.enqueue(object: Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't submit bank account info! Please try again."
            }

            override fun onResponse(
                call: Call<String>,
                response: Response<String>
            ) {
                try {
                    if (response.code() == 201) {
                        Loaders.apiSuccess.value = "Bank account added successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't submit bank account info! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun saveAddAndTagBankAccountInfo(tagAndAddBankAccount: TagAndAddBankAccount) {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.tagAndAddBankAccount("bearer" + appPreferences.getToken(), tagAndAddBankAccount)

        call.enqueue(object: Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't submit bank account info! Please try again."
            }

            override fun onResponse(
                call: Call<String>,
                response: Response<String>
            ) {
                try {
                    if (response.code() == 201) {
                        Loaders.apiSuccess.value = "Bank account added successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't submit bank account info! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun getUserBankList() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getBankList("bearer" + appPreferences.getToken())

        call.enqueue(object: Callback<List<BankList>> {
            override fun onFailure(call: Call<List<BankList>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't get the account list! Please try again."
            }

            override fun onResponse(
                call: Call<List<BankList>>,
                response: Response<List<BankList>>
            ) {
                try {
                    if (response.code() == 200) {
//                        val type = object : TypeToken<List<BankAccountInfo>>() {}.type
//                        val errorResponse: List<BankAccountInfo>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        userBankAccountList.value = response.body()
                    } else {
                        Loaders.apiError.value = "No value found!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun getTagList() {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getTagList("bearer" + appPreferences.getToken())

        call.enqueue(object: Callback<List<TagGetData>> {
            override fun onFailure(call: Call<List<TagGetData>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't get the district list! Please try again."
            }

            override fun onResponse(
                call: Call<List<TagGetData>>,
                response: Response<List<TagGetData>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<TagGetData>>() {}.type
                        val errorResponse: List<TagGetData>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        tagList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "No value found!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun getTagTypeList() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getTagTypeList("bearer" + appPreferences.getToken(), "T123")

        call.enqueue(object: Callback<List<TagType>> {
            override fun onFailure(call: Call<List<TagType>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't get the district list! Please try again."
            }

            override fun onResponse(
                call: Call<List<TagType>>,
                response: Response<List<TagType>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<TagType>>() {}.type
                        val errorResponse: List<TagType>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        tagTypeList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "No value found!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun tagBankAccount(tagPostData: TagPostData) {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.tagBankAccount("bearer" + appPreferences.getToken(), tagPostData)

        call.enqueue(object: Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't tag bank account! Please try again."
            }

            override fun onResponse(
                call: Call<String>,
                response: Response<String>
            ) {
                try {
                    if (response.code() == 201) {
                        Loaders.apiSuccess.value = "Bank account tagged successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't tagged bank account info! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun sendNetiMail(netiMailSubmit: NetiMailSubmit) {

        Loaders.isLoading0.value = true
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.submitNetiMail("bearer " + appPreferences.getToken(), netiMailSubmit)

        //calling the api---------------------------------------------------------------
        call?.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                try {

                    Log.d("onResponse", "Registration Success:" + response.body())

                    when {
                        response.code() == 201 -> {
                            Loaders.success.postValue("The Message has been sent Successfully.")
                        }
                        else -> {
                            Loaders.error.postValue("Message has not been sent !")
                            Loaders.isLoading0.value = false
                        }
                    }
                    Loaders.isLoading0.value = false
                }

                catch (e: Exception){
                    e.printStackTrace()
                    Loaders.isLoading0.value = false
                    Loaders.error.postValue("Something went wrong! Please try again.")
                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Loaders.isLoading0.value = false
                Loaders.error.postValue("Something went wrong! Please try again.")
            }
        })
    }

    fun updateAddAndTagBankAccountInfo(updateBankAccount: UpdateBankAccount) {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.updateBankAccount("bearer" + appPreferences.getToken(), updateBankAccount)

        call.enqueue(object: Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't update bank account info! Please try again."
            }

            override fun onResponse(
                call: Call<String>,
                response: Response<String>
            ) {
                try {
                    if (response.code() == 201) {
                        Loaders.apiSuccess.value = "Bank account updated successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't update bank account info! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }
}
