package com.netizen.netiworld.repository

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.netizen.netiworld.apiService.ApiClient
import com.netizen.netiworld.apiService.ApiInterface
import com.netizen.netiworld.model.*
import com.netizen.netiworld.utils.AppPreferences
import com.netizen.netiworld.utils.Loaders
import es.dmoral.toasty.Toasty
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PurchaseRepository(val application: Application) {

    private val appPreferences = AppPreferences(application)

    var code = MutableLiveData<String>()
    var codeUsable = MutableLiveData<String>()
    var codeUsed = MutableLiveData<String>()
    var productQnty = MutableLiveData<String>()
    var unitPrice = MutableLiveData<String>()
    var productName = MutableLiveData<String>()
    var discountAmtPer = MutableLiveData<String>()
    var productOfferID = MutableLiveData<String>()
    var isDataFound = MutableLiveData<Boolean>()

    val productList = MutableLiveData<List<Products>>()
    val generalProductReportList = MutableLiveData<List<GeneralProductGetData>>()
    val productOfferReportList = MutableLiveData<List<ProductsOfferGetData>>()

    var unusedPurchaseLogList = MutableLiveData<List<PurchaseCodeLogGetdata?>>()
    var isUnusedPurchaseLogDataFound = MutableLiveData<Boolean>()

    fun getOfferProductData(offerCode: String) {
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val userCall = service?.getOfferProduct(header, offerCode)

        userCall?.enqueue(object : Callback<OfferProduct> {

            override fun onResponse(call: Call<OfferProduct>, response: Response<OfferProduct>) {
                try {
                    Log.d("onResponse", "Main :" + response.body().toString())

                    when {
                        response.code() == 302 -> {

                            val offer = response.errorBody()?.source()?.buffer()?.readUtf8()

                            val jsonObj = JSONObject(offer!!)

                            code.postValue(jsonObj.getString("offerCode"))
                            codeUsable.postValue(jsonObj.getString("offerUseableTime"))
                            codeUsed.postValue(jsonObj.getString("offerUsed"))
                            productQnty.postValue(jsonObj.getString("offerQuantity"))
                            unitPrice.postValue(jsonObj.getString("actualPrice"))
                            productName.postValue(jsonObj.getString("productName"))
                            discountAmtPer.postValue(jsonObj.getString("discountPercent"))
                            productOfferID.postValue(jsonObj.getString("productOfferID"))

                            Log.d("Discount Amount :", jsonObj.getString("discountPercent"))
                            Log.d("onResponse", "Product Info :$offer")
                            val h1 = response.headers().get("Location")
                            Log.d("onResponse", "Header1 url :$h1")
                        }
                        response.code() == 404 ->{
                            Loaders.error.value = "Invalid Offer Code !"
                            Loaders.isLoading0.value = false
                        }
                        response.code() == 400 ->{
                            Loaders.error.value = "Offer Limit Exceed !"
                            Loaders.isLoading0.value = false
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                    Loaders.error.value = "Something went wrong! Please try again."
                    Loaders.isLoading0.value = false
                }
                Loaders.isLoading0.value = false
            }

            override fun onFailure(call: Call<OfferProduct>, t: Throwable) {
                Log.d("onFailure", t.toString())
                Loaders.error.value = "Couldn't Find Offer Product ! Please try again."
                Loaders.isLoading0.value = false
            }
        })
    }

    fun submitOfferProductData(
        unitPrice: String?,
        payableAmt: String?,
        discountAmt: String?,
        totalAmount: String?,
        productOfferID: String?
    ) {

        Loaders.isLoading0.value = true
        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        try {
            val offerProduct = OfferProductPost(
                unitPrice?.toDoubleOrNull()!!,
                payableAmt?.toDoubleOrNull(),
                discountAmt?.toDoubleOrNull(),
                totalAmount?.toDoubleOrNull(),
                OfferProductPost.ProductOfferDTO(productOfferID?.toIntOrNull())
            )

            Log.d("Offer Product:", "JSON:" + offerProduct)

            val service = ApiClient.getClient?.create(ApiInterface::class.java)
            val call = service?.offerProductPostData(offerProduct, header)

            //calling the api------------------------------------------------------------
            call?.enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    try {

                        Log.d("onResponse", "Message Recharge:" + response.body())

                        if (response.code() == 200) {
                            Log.d("onResponse", "Token:" + response.body())
                            Loaders.success.value = "Offer Product Purchase Successfully"
                        } else {
                            Loaders.error.value = response.message().toString()
                        }
                        Loaders.isLoading0.value = false
                    }

                    catch (e: Exception){
                        Loaders.isLoading0.value = false
                        Loaders.error.value = "Something went wrong! Please try again."
                    }
                    Loaders.isLoading0.value = false
                }
                override fun onFailure(call: Call<String>, t: Throwable) {
                    Log.d("onFailure", t.toString())
                    Loaders.isLoading0.value = false
                    Loaders.error.value = "Couldn't Purchase Offer Product ! Please try again."
                }
            })

        } catch (e: NumberFormatException) {
            e.printStackTrace()
            Loaders.isLoading0.value = false
        } catch (e: NullPointerException) {
            e.printStackTrace()
            Loaders.isLoading0.value = false
            Loaders.error.value = "Please Search Offer Code"
        }
    }

    fun getProductsByRole(roleId: String?) {
        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getProductName(header, roleId)

        call.enqueue(object : Callback<List<Products>>{
            override fun onFailure(call: Call<List<Products>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.error.value = "Couldn't get product list! Please try again"
            }

            override fun onResponse(call: Call<List<Products>>, response: Response<List<Products>>) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<Products>>() {}.type
                        val errorResponse: List<Products>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        productList.value = errorResponse

//                        Log.e("PRODUCT", productList.value?.size.toString())
                    } else {
                        Loaders.error.value = response.message().toString()
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.error.value = "Something went wrong! Please try again"
                }
            }
        })
    }

    fun submitGeneralProductPostData(generalProductPostData: GeneralProductPostData) {
        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.generalProductPostData(generalProductPostData, header)

        call.enqueue(object : Callback<String>{
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.error.value = "Couldn't submit data! Please try again"
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    if (response.isSuccessful) {
                        Loaders.success.value = "Successfully data submitted."
                    } else {
                        Loaders.error.value = response.message().toString()
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.error.value = "Something went wrong! Please try again"
                }
            }
        })
    }

    fun getGeneralProductReportData(fromDate: String, toDate: String) {
        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getGeneralProductReportData(header, fromDate, toDate)

        call.enqueue(object : Callback<List<GeneralProductGetData>> {
            override fun onFailure(call: Call<List<GeneralProductGetData>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't get general product report data! Please try again."
            }

            override fun onResponse(
                call: Call<List<GeneralProductGetData>>,
                response: Response<List<GeneralProductGetData>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<GeneralProductGetData>>() {}.type
                        val errorResponse: List<GeneralProductGetData>? = Gson().fromJson(response.errorBody()!!.charStream(), type)

                        generalProductReportList.value = errorResponse
                        isDataFound.value = true
                    } else {
                        isDataFound.value = false
                        Loaders.apiError.value = response.message().toString()
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    isDataFound.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

   //Purchase unused code report ------------------------------------------------------------------

     fun getPurchaseUnusedData(usedStatus: Int?) {
        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getPurchaseCodeData(header, 0)

        //calling the api
        call?.enqueue(object : Callback<List<PurchaseCodeLogGetdata>> {
            override fun onResponse(
                call: Call<List<PurchaseCodeLogGetdata>>,
                response: Response<List<PurchaseCodeLogGetdata>>
            ) {
//                Log.d("onResponse", "Purchase Code List:" + response.body())

                if (response.code() == 302) {
//                    Log.d("onResponse", "Unused List:" + response.body())
                    val type = object : TypeToken<List<PurchaseCodeLogGetdata>>() {}.type
                    val errorResponse: List<PurchaseCodeLogGetdata>? = Gson().fromJson(response.errorBody()!!.charStream(), type)

                    if (errorResponse.isNullOrEmpty()) {
                        isUnusedPurchaseLogDataFound.value = false
                        Loaders.apiError.value = "No data found!"
                    } else {
                        unusedPurchaseLogList.value = errorResponse
                        isUnusedPurchaseLogDataFound.value = true
                    }
                } else {
                    isUnusedPurchaseLogDataFound.value = false
                    Loaders.apiError.value = response.message().toString()
                }

                Loaders.isLoading0.value = false
            }

            override fun onFailure(call: Call<List<PurchaseCodeLogGetdata>>, t: Throwable) {
//                Log.d("onFailure", t.toString())
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't get purchase code report data! Please try again."
            }
        })
    }

    fun getProductProductReportData(productsOfferReportPostData: ProductsOfferReportPostData) {
        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getProductOfferReportData(header, productsOfferReportPostData)

        call.enqueue(object : Callback<List<ProductsOfferGetData>> {
            override fun onFailure(call: Call<List<ProductsOfferGetData>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't get product offer report data! Please try again."
            }

            override fun onResponse(
                call: Call<List<ProductsOfferGetData>>,
                response: Response<List<ProductsOfferGetData>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<ProductsOfferGetData>>() {}.type
                        val errorResponse: List<ProductsOfferGetData>? = Gson().fromJson(response.errorBody()!!.charStream(), type)

                        productOfferReportList.value = errorResponse
                        isDataFound.value = true
                    } else {
                        isDataFound.value = false
                        Loaders.apiError.value = response.message().toString()
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    isDataFound.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }
}