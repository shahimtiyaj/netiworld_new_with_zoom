package com.netizen.netiworld.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.netizen.netiworld.apiService.ApiClient
import com.netizen.netiworld.apiService.ApiInterface
import com.netizen.netiworld.model.Problem
import com.netizen.netiworld.model.TokenList
import com.netizen.netiworld.model.TokenSubmitPostData
import com.netizen.netiworld.utils.AppPreferences
import com.netizen.netiworld.utils.Loaders
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TokenRepository(private val application: Application) {

    private val appPreferences = AppPreferences(application.applicationContext)

    var solvedAndPendingTokenList = MutableLiveData<List<TokenList>>()
    var problemModuleList = MutableLiveData<List<Problem>>()
    var problemTypeList = MutableLiveData<List<Problem>>()
    var tokenList = MutableLiveData<List<TokenList>>()

    var isProblemModuleListFound = MutableLiveData<Boolean>()
    var isProblemTypeListFound = MutableLiveData<Boolean>()
    var isDownloading = MutableLiveData<Boolean>()
    var downloadedAttachment = MutableLiveData<String>()

    fun getSolvedAndPendingTokenList() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getSolvedAndPendingToken("bearer " + appPreferences.getToken())

        call.enqueue(object : Callback<List<TokenList>> {
            override fun onFailure(call: Call<List<TokenList>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't get solved and token list! Please try again."
            }

            override fun onResponse(
                call: Call<List<TokenList>>,
                response: Response<List<TokenList>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<TokenList>>() {}.type
                        val errorResponse: List<TokenList>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        solvedAndPendingTokenList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't get get solved and token list! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun getProblemModuleList() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getProblemModuleList("bearer " + appPreferences.getToken(), "C11001002")

        call.enqueue(object : Callback<List<Problem>> {
            override fun onFailure(call: Call<List<Problem>>, t: Throwable) {
                Loaders.isLoading0.value = false
                isProblemModuleListFound.value = false
                Loaders.apiError.value = "Couldn't get problem modules! Please try again."
            }

            override fun onResponse(
                call: Call<List<Problem>>,
                response: Response<List<Problem>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<Problem>>() {}.type
                        val errorResponse: List<Problem>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        problemModuleList.value = errorResponse
                        isProblemModuleListFound.value = true
                    } else {
                        isProblemModuleListFound.value = false
                        Loaders.apiError.value = "Couldn't get problem modules! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    isProblemModuleListFound.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun getProblemTypeList(parentCategoryId: String) {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getProblemTypeList("bearer " + appPreferences.getToken(), parentCategoryId)

        call.enqueue(object : Callback<List<Problem>> {
            override fun onFailure(call: Call<List<Problem>>, t: Throwable) {
                Loaders.isLoading0.value = false
                isProblemTypeListFound.value = false
                Loaders.apiError.value = "Couldn't get problem types! Please try again."
            }

            override fun onResponse(
                call: Call<List<Problem>>,
                response: Response<List<Problem>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<Problem>>() {}.type
                        val errorResponse: List<Problem>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        problemTypeList.value = errorResponse
                        isProblemTypeListFound.value = true
                    } else {
                        isProblemTypeListFound.value = false
                        Loaders.apiError.value = "Couldn't get problem types! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    isProblemTypeListFound.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun submitToken(tokenSubmitPostData: TokenSubmitPostData) {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.submitNewToken("bearer" + appPreferences.getToken(), tokenSubmitPostData)

        call.enqueue(object : Callback<Void> {
            override fun onFailure(call: Call<Void>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't submit token! Please try again."
            }

            override fun onResponse(
                call: Call<Void>,
                response: Response<Void>
            ) {
                try {
                    if (response.code() == 201) {
                        Loaders.apiSuccess.value = "Token submitted successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't submit token! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun getTokenList() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.getTokenList("bearer " + appPreferences.getToken())

        call.enqueue(object : Callback<List<TokenList>> {
            override fun onFailure(call: Call<List<TokenList>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't get token list! Please try again."
            }

            override fun onResponse(
                call: Call<List<TokenList>>,
                response: Response<List<TokenList>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<TokenList>>() {}.type
                        val errorResponse: List<TokenList>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        tokenList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't get token! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }

    fun downloadTokenAttachment(imageUrl: String?) {
        isDownloading.value = true

        val apiInterface = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiInterface.downloadTokenAttachment("bearer " + appPreferences.getToken(), imageUrl)

        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                isDownloading.value = false
                Loaders.apiError.value = "Couldn't download the attachment! Please try again."
            }

            override fun onResponse(
                call: Call<JsonObject>,
                response: Response<JsonObject>
            ) {
                try {
                    if (response.code() == 200) {
                        Log.e("RESPONSE", response.body().toString())
                        downloadedAttachment.value = response.body()?.asJsonObject?.get("fileContent")?.asString
                        Loaders.apiSuccess.value = "Downloading completed."
                    } else {
                        Loaders.apiError.value = "Couldn't download the attachment! Please try again."
                    }

                    isDownloading.value = false
                } catch (e: Exception) {
                    isDownloading.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }
        })
    }
}