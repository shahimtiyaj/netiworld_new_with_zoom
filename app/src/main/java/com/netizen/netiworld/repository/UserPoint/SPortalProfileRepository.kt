package com.netizen.netiworld.repository.UserPoint

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.netizen.netiworld.apiService.ApiClient
import com.netizen.netiworld.apiService.ApiInterface
import com.netizen.netiworld.model.UserPoint.*
import com.netizen.netiworld.utils.AppPreferences
import com.netizen.netiworld.utils.Loaders
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SPortalProfileRepository(val application: Application) {

    private val appPreferences = AppPreferences(application)

    var studentPortalProfileInfo = MutableLiveData<StudentPortalProfile>()
    var studentPortalSubjectInfo = MutableLiveData<List<StudentPortalSubject.Item>>()
    var isStudentPortalSubjectDataFound = MutableLiveData<Boolean>()
    var isStudentPortalClassTestDataFound = MutableLiveData<Boolean>()
    var studentPortalPeriodData = MutableLiveData<List<StudentPortalPeriod.Item>>()
    var studentPortalYearInfo = MutableLiveData<List<YearByTypeId.Item>>()
    var studentPortalClassTest = MutableLiveData<List<ClassTest.Item>>()
    var studentPortalUnpaidFeesInfo = MutableLiveData<List<UnpaidFees.Item>>()
    var studentPortalInventoryDetailsInfo = MutableLiveData<List<InventoryDetails.Item>>()
    var studentPortalClassTestDetails = MutableLiveData<List<ClassTestDetails.Item.StdCtExamMark>>()

    var isStudentPortalFessDataFound = MutableLiveData<Boolean>()
    val unpaidFeesList = MutableLiveData<List<String>>()
    val monthsList = MutableLiveData<List<String>>()

    fun getStudentPortalProfile() {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getStudentPortalProfileInfo(
            "bearer " + appPreferences.getToken(),
            "10503",
            "105032091220"
        )

        call?.enqueue(object : Callback<StudentPortalProfile> {
            override fun onFailure(call: Call<StudentPortalProfile>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load student portal profile data!"
            }

            override fun onResponse(
                call: Call<StudentPortalProfile>,
                response: Response<StudentPortalProfile>
            ) {
                try {
                    if (response.isSuccessful) {
                        studentPortalProfileInfo.value = response.body() as StudentPortalProfile

                    } else {
                        Loaders.apiError.value = "Couldn't load student portal profile data!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load student portal profile data!"
                }
            }
        })
    }

    //STUDENT PORTAL SUBJECT DATA-------------------------------------------------------------------

    fun getStudentPortalSubjectsData() {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getStudentPortalSubjectInfo(
            "bearer " + appPreferences.getToken(),
            "10597",
            "105972031518",
            "2020"
        )

        val emptyList = ArrayList<StudentPortalSubject.Item>()

        //calling the api--------------------------------------------
        call?.enqueue(object : Callback<StudentPortalSubject> {
            override fun onResponse(
                call: Call<StudentPortalSubject>,
                response: Response<StudentPortalSubject>
            ) {
                try {
                    if (response.isSuccessful) {
                        Log.d("onResponse", "Token:" + response.body())
                        studentPortalSubjectInfo.value =
                            response.body()?.getItem() as List<StudentPortalSubject.Item>
                        isStudentPortalSubjectDataFound.value = true
                    } else {
                        isStudentPortalSubjectDataFound.value = false
                        studentPortalSubjectInfo.postValue(emptyList)
                        Loaders.apiError.value = "Sorry ! No Data Found."
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    isStudentPortalSubjectDataFound.value = false
                    studentPortalSubjectInfo.postValue(emptyList)
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load student portal subject data!"
                }

                Loaders.isLoading0.value = false
            }

            override fun onFailure(call: Call<StudentPortalSubject>, t: Throwable) {
                Log.d("onFailure", t.toString())
                studentPortalSubjectInfo.postValue(emptyList)
                isStudentPortalSubjectDataFound.value = false
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load student portal subject data!"
            }
        })
    }

    fun getStudentPortalUnpaidFees(instituteId: String, studentId: String, academicYear: String) {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getStudentPortalUnpaidFees(
            "bearer " + appPreferences.getToken(),
            instituteId,
            studentId,
            academicYear
        )
        val emptyList = ArrayList<UnpaidFees.Item>()

        //calling the api--------------------------------------------
        call?.enqueue(object : Callback<UnpaidFees> {
            override fun onResponse(
                call: Call<UnpaidFees>,
                response: Response<UnpaidFees>
            ) {
                try {
                    if (response.isSuccessful) {
                        Log.d("onResponse", "Token:" + response.body())
                        studentPortalUnpaidFeesInfo.value =
                            response.body()?.getItem() as List<UnpaidFees.Item>
                        isStudentPortalFessDataFound.value = true
                    } else {
                        isStudentPortalFessDataFound.value = false
                        studentPortalUnpaidFeesInfo.postValue(emptyList)
                        Loaders.apiError.value = "Sorry ! No Data Found."
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    isStudentPortalFessDataFound.value = false
                    studentPortalUnpaidFeesInfo.postValue(emptyList)
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load student portal unpaid fees data!"
                }

                Loaders.isLoading0.value = false
            }

            override fun onFailure(call: Call<UnpaidFees>, t: Throwable) {
                Log.d("onFailure", t.toString())
                studentPortalUnpaidFeesInfo.postValue(emptyList)
                isStudentPortalFessDataFound.value = false
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load student portal unpaid fees data!"
            }
        })
    }

    //Student Portal Attendance period data
    fun getStudentPortalAttendancePeriodData() {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getStudentPortalPeriod(
            "bearer " + appPreferences.getToken(),
            "2301", "10597"
        )

        call?.enqueue(object : Callback<StudentPortalPeriod> {

            override fun onFailure(call: Call<StudentPortalPeriod>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Fail to load student portal period data!"
            }

            override fun onResponse(
                call: Call<StudentPortalPeriod>,
                response: Response<StudentPortalPeriod>
            ) {
                try {
                    if (response.isSuccessful) {
                        studentPortalPeriodData.value =
                            response.body()?.getItem() as List<StudentPortalPeriod.Item>
                        Loaders.apiSuccess.value = "Get Period Data !"

                    } else {
                        Loaders.apiError.value = "Couldn't load student portal period data!"
                    }
                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load student portal period data!"
                }
            }
        })
    }

    fun getStudentPortalYear() {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call =
            apiInterface?.getStudentPortalGlobalInfo("bearer " + appPreferences.getToken(), "2101")

        call?.enqueue(object : Callback<YearByTypeId> {
            override fun onFailure(call: Call<YearByTypeId>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Fail to load student portal year data!"
            }

            override fun onResponse(
                call: Call<YearByTypeId>,
                response: Response<YearByTypeId>
            ) {
                try {
                    if (response.isSuccessful) {
                        studentPortalYearInfo.value =
                            response.body()?.getItem() as List<YearByTypeId.Item>
                        Loaders.apiSuccess.value = "Get Academic Year"
                    } else {
                        Loaders.apiError.value = "Couldn't load student portal year data!"
                    }
                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load student portal year data!"
                }
            }
        })
    }

    fun getStudentInventoryDetails(
        instituteId: String,
        studentId: String,
        year: String,
        monthName: String
    ) {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getStudentPortalInventoryDetails(
            "bearer " + appPreferences.getToken(),
            instituteId,
            studentId,
            year,
            monthName
        )

        call?.enqueue(object : Callback<InventoryDetails> {
            override fun onFailure(call: Call<InventoryDetails>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Fail to load student portal inventory data!"
            }

            override fun onResponse(
                call: Call<InventoryDetails>,
                response: Response<InventoryDetails>
            ) {
                try {
                    if (response.isSuccessful) {
                        studentPortalInventoryDetailsInfo.value =
                            response.body()?.getItem() as List<InventoryDetails.Item>
                        Loaders.apiSuccess.value = "Get Academic Year"
                    } else {
                        Loaders.apiError.value = "Couldn't load student portal inventory data!"
                    }
                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load student portal inventory data!"
                }
            }
        })
    }


    fun getStudentPortalClassTest(instituteId: String, studentId: String, academicYear: String) {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getStudentPortalClassTest(
            "bearer " + appPreferences.getToken(),
            instituteId,
            studentId,
            academicYear
        )

        call?.enqueue(object : Callback<ClassTest> {
            override fun onFailure(call: Call<ClassTest>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load student portal class test data!"
            }

            override fun onResponse(
                call: Call<ClassTest>,
                response: Response<ClassTest>
            ) {
                try {
                    if (response.isSuccessful) {
                        studentPortalClassTest.value =
                            response.body()?.getItem() as List<ClassTest.Item>
                    } else {
                        Loaders.apiError.value = "Couldn't load student portal class test data!"
                    }
                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load student portal class test data!"
                }
            }
        })
    }

    fun getStudentPortalClassTestDetails() {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getStudentPortalClassTestDetails(
            "bearer " + appPreferences.getToken(),
            "10503",
            "105032050618",
            "2019",
            "8190"
        )

        val emptyList = ArrayList<ClassTestDetails.Item.StdCtExamMark>()

        //calling the api--------------------------------------------
        call?.enqueue(object : Callback<ClassTestDetails> {
            override fun onResponse(
                call: Call<ClassTestDetails>,
                response: Response<ClassTestDetails>
            ) {
                try {
                    if (response.isSuccessful) {
                        Log.d("onResponse", "Token:" + response.body())
                        studentPortalClassTestDetails.value = response.body()?.getItem()
                            ?.getStdCtExamMarks() as List<ClassTestDetails.Item.StdCtExamMark>
                        isStudentPortalClassTestDataFound.value = true
                    } else {
                        isStudentPortalClassTestDataFound.value = false
                        studentPortalClassTestDetails.postValue(emptyList)
                        Loaders.apiError.value = "Sorry ! No Data Found."
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    isStudentPortalClassTestDataFound.value = false
                    studentPortalClassTestDetails.postValue(emptyList)
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value =
                        "Couldn't load student portal class test details  data!"
                }

                Loaders.isLoading0.value = false
            }

            override fun onFailure(call: Call<ClassTestDetails>, t: Throwable) {
                Log.d("onFailure", t.toString())
                studentPortalClassTestDetails.postValue(emptyList)
                isStudentPortalClassTestDataFound.value = false
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load student portal class test details  data!"
            }
        })
    }

    fun spinnerStudentPortalUnpaidFees() {
        val unpaidStringList: MutableList<String> = ArrayList()
        unpaidStringList.add("Select Status")
        unpaidStringList.add("Paid")
        unpaidStringList.add("Unpaid")
        unpaidFeesList.value = unpaidStringList
    }

    fun spinnerStudentPortalMonth() {
        val monthStringList: MutableList<String> = ArrayList()
        monthStringList.add("Select Month")
        monthStringList.add("January")
        monthStringList.add("February")
        monthStringList.add("March")
        monthStringList.add("April")
        monthStringList.add("May")
        monthStringList.add("June")
        monthStringList.add("July")
        monthStringList.add("August")
        monthStringList.add("September")
        monthStringList.add("October")
        monthStringList.add("November")
        monthStringList.add("December")
        monthsList.value = monthStringList
    }
}