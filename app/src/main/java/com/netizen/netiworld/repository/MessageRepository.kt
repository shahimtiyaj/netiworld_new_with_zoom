package com.netizen.netiworld.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.netizen.netiworld.apiService.ApiClient
import com.netizen.netiworld.apiService.ApiInterface
import com.netizen.netiworld.model.*
import com.netizen.netiworld.utils.AppPreferences
import com.netizen.netiworld.utils.Loaders
import org.json.JSONArray
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MessageRepository(val application: Application) {

    private val appPreferences = AppPreferences(application)

    val purchasePointArrayList = MutableLiveData<List<PurchasePoint>>()
    val messageTypeArrayList = MutableLiveData<List<MessageType>>()
    val purchasePointArrayListData = ArrayList<PurchasePoint>()
    val messageTypeArrayListData = ArrayList<MessageType>()
    var productRoleAssignID = MutableLiveData<String>()

    var messageRechargeInfo = MutableLiveData<List<BalanceMessageGetData?>>()
    var revenueLogList = MutableLiveData<List<RevenueLogGetData?>>()
    var messageTypeInfo = MutableLiveData<List<MessageType1?>>()

    var isRevenueLogDataFound = MutableLiveData<Boolean>()
    var isMessageLogDataFound = MutableLiveData<Boolean>()

    fun getPurchasePointDataFromServer() {

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val userCall = service?.getPurchasePoint(header)

        purchasePointArrayListData.clear()
        val emptyList = ArrayList<PurchasePoint>()

        userCall?.enqueue(object : Callback<PurchasePoint> {

            override fun onResponse(
                call: Call<PurchasePoint>,
                response: Response<PurchasePoint>
            ) {
                Log.d("onResponse", "Main :" + response.errorBody().toString())

                if (response.code() == 302) {
                    val purchasePointInfo = response.errorBody()?.source()?.buffer()?.readUtf8()
                    val getData = JSONArray(purchasePointInfo!!)

                    for (i in 0 until getData.length()) {
                        //continue to loop it getting null value
                        if (getData.isNull(i))
                            continue
                        // Getting json object node
                        val c = getData.getJSONObject(i)

                        // Get the item model
                        val purchasePointList = PurchasePoint()
                        //set the json data in the model
                        purchasePointList.setUserRoleAssignID(c.getLong("userRoleAssignID"))
                        purchasePointList.setCoreRoleName(c.getString("coreRoleName"))
                        purchasePointList.setCoreRoleID(c.getLong("coreRoleID"))

                        Log.d("coreRoleName", "General Purchase :" + c.getString("coreRoleName"))

                        purchasePointArrayListData.add(purchasePointList)

                        Log.d("UserRole ID :", purchasePointList.getCoreRoleID()?.toString()!!)
                        Log.d("UserRole Name :", purchasePointList.getCoreRoleName()!!)
                    }

                    purchasePointArrayList.postValue(purchasePointArrayListData)
                }
            }

            override fun onFailure(call: Call<PurchasePoint>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }

    fun getMessageTypeData(purchasePointRoleId: String) {

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        Log.e("Role ID: ", purchasePointRoleId)

        val userCall = service?.getMessageType1(header, purchasePointRoleId) // Role ID

        userCall?.enqueue(object : Callback<MessageType1> {

            override fun onResponse(
                call: Call<MessageType1>,
                response: Response<MessageType1>
            ) {
                try {
                    Log.d("onResponse", "Main :" + response.errorBody().toString())

                    if (response.code() == 302) {
                        Log.d("onResponse", "Recharge List:" + response.body())
                        val type = object : TypeToken<List<MessageType1>>() {}.type
                        messageTypeInfo.value = Gson().fromJson(response.errorBody()!!.charStream(), type)
                    }

                } catch (e: NullPointerException) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            override fun onFailure(call: Call<MessageType1>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }

    fun submitMessageRechargeData(
        messageQuantity: String,
        productID: String,
        productRoleAssignID: String
    ) {
        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        val service = ApiClient.getClient?.create(ApiInterface::class.java)

        try {

            val messageRecharge = MessageRecharge(
                messageQuantity.toIntOrNull(),
                ProductInfoDTO(productID.toIntOrNull()),
                ProductPurchaseLogDTO(ProductRoleAssignDTO(productRoleAssignID.toInt()))
            )

            val call = service?.messageRechargepostData(messageRecharge, header)

            //calling the api
            call?.enqueue(object : Callback<Void> {
                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    try {

                        Log.d("onResponse", "Message Recharge:" + response.body())

                        when {
                            response.code() == 201 -> {
                                Log.d("onResponse", "Token:" + response.body())
                                Loaders.success?.value = "Message Recharge successfully."
                            }
                            response.code() == 400 -> {

                            }
                            response.code() == 409 -> {

                            }
                            response.code() == 500 -> {

                            }
                            else -> {
                                Loaders.error.value = response.message().toString()
                            }
                        }
                        Loaders.isLoading0.value = false
                    } catch (e: Exception) {
                        Loaders.isLoading0.value = false
                        Loaders.error?.value = "Something went wrong! Please try again."
                    }

                }

                override fun onFailure(call: Call<Void>, t: Throwable) {
                    Log.d("onFailure", t.toString())
                    Loaders.isLoading0.value = false
                    Loaders.error?.value = "Couldn't submit Message Recharge! Please try again."
                }
            })

        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }
    }

    //Message Recharge Reports --------------------------------------------------------
    fun getMessageRechargeListData(startDateMessage: String, endDateMessage: String) {
        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        header["Content-Type"] = "application/json"

        val balanceTransfer =
            BalanceDepositPostForList(startDateMessage, endDateMessage, "Recharge", 100, 0)

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.messageRechargeGetData(balanceTransfer, header)

        //calling the api
        call?.enqueue(object : Callback<List<BalanceMessageGetData>> {
            override fun onResponse(
                call: Call<List<BalanceMessageGetData>>,
                response: Response<List<BalanceMessageGetData>>
            ) {
                try {
//                    Log.d("onResponse", "Message Recharge List:" + response.body())

                    if (response.code() == 302) {
//                        Log.d("onResponse", "Recharge List:" + response.body())
                        val type = object : TypeToken<List<BalanceMessageGetData>>() {}.type
                        val errorResponse: List<BalanceMessageGetData?> =
                            Gson().fromJson(response.errorBody()!!.charStream(), type)
                        messageRechargeInfo.value = errorResponse
                        isMessageLogDataFound.value = true
                    } else {
                        isMessageLogDataFound.value = false
                        Loaders.apiError.value = response.message().toString()
                    }
                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    e.printStackTrace()
                    Loaders.isLoading0.value = false
                    isMessageLogDataFound.value = false
                    Loaders.apiError.value = "Something went wrong! Please try again."
                }
            }

            override fun onFailure(call: Call<List<BalanceMessageGetData>>, t: Throwable) {
//                Log.d("onFailure", t.toString())
                isMessageLogDataFound.value = false
                Loaders.isLoading0.value = false
                Loaders.apiError?.value = "Couldn't get message recharge data ! Please try again."
            }
        })
    }

    fun getRevenueLogData(revenueLogPostData: RevenueLogPostData?) {
        Loaders.isLoading0.value = true

        val apiService = ApiClient.getClient!!.create(ApiInterface::class.java)
        val call = apiService.getRevenueLogData(
            "bearer " + appPreferences.getToken().toString(),
            revenueLogPostData
        )

        call.enqueue(object : Callback<RevenueLogGetData> {
            override fun onFailure(call: Call<RevenueLogGetData>, t: Throwable) {
                Loaders.isLoading0.value = false
                isRevenueLogDataFound.value = false
                Loaders.error.value = "Couldn't get revenue log data! Please try again."
            }

            override fun onResponse(
                call: Call<RevenueLogGetData>,
                response: Response<RevenueLogGetData>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<RevenueLogGetData>>() {}.type
                        val errorResponse: List<RevenueLogGetData>? =
                            Gson().fromJson(response.errorBody()!!.charStream(), type)

                        if (errorResponse.isNullOrEmpty()) {
                            isRevenueLogDataFound.value = false
                            Loaders.error.value = "No data found!"
                        } else {
                            revenueLogList.value = errorResponse
                            isRevenueLogDataFound.value = true
                        }
                    } else {
                        isRevenueLogDataFound.value = false
                        Loaders.error.value = response.message().toString()
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    isRevenueLogDataFound.value = false
                    Loaders.error.value = "Something went wrong! Please try again."
                }
            }
        })
    }
}