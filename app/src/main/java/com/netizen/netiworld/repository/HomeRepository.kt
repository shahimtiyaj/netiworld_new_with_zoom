package com.netizen.netiworld.repository

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.netizen.netiworld.apiService.ApiClient
import com.netizen.netiworld.apiService.ApiInterface
import com.netizen.netiworld.model.HomePageInfo
import com.netizen.netiworld.utils.AppPreferences
import com.netizen.netiworld.utils.Loaders
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeRepository(application: Application) {

    private val appPreferences = AppPreferences(application)

    val homePageInfoLiveData = MutableLiveData<HomePageInfo>()

    fun getHomePageInfo() {
        Loaders.isLoading0.value = true

        val apiService = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiService?.getHomePageInfo("bearer" + appPreferences.getToken().toString())

        call?.enqueue(object : Callback<HomePageInfo>{
            override fun onFailure(call: Call<HomePageInfo>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError?.value = "Couldn't get data! Please try again."
            }

            override fun onResponse(call: Call<HomePageInfo>, response: Response<HomePageInfo>) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<HomePageInfo>() {}.type
                        val errorResponse: HomePageInfo? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        homePageInfoLiveData.value = errorResponse
                    } else {
                        Loaders.apiError?.value = response.message().toString()
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError?.value = "Something went wrong! Please try again."
                }
            }
        })
    }
}