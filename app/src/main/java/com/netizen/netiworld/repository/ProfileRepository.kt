package com.netizen.netiworld.repository

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.netizen.netiworld.apiService.ApiClient
import com.netizen.netiworld.apiService.ApiInterface
import com.netizen.netiworld.model.*
import com.netizen.netiworld.utils.AppPreferences
import com.netizen.netiworld.utils.Loaders
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileRepository(val application: Application) {

    private val appPreferences = AppPreferences(application)

    var profileInformation = MutableLiveData<ProfileInformation>()
    var photoFileContent = MutableLiveData<String>()
    var educationInfoList = MutableLiveData<List<EducationInfo>>()
    var trainingInfoList = MutableLiveData<List<TrainingInfo>>()
    var certificationInfoList = MutableLiveData<List<CertificationInfo>>()
    var experienceInfoList = MutableLiveData<List<ExperienceInfo>>()
    var referenceInfoList = MutableLiveData<List<ReferenceInfo>>()
    var educationLevelList = MutableLiveData<List<DataByTypeDefaultCode>>()
    var majorSubjectList = MutableLiveData<List<DataByTypeDefaultCode>>()
    var resultList = MutableLiveData<List<DataByTypeDefaultCode>>()
    var passingYearList = MutableLiveData<List<DataByTypeDefaultCode>>()
    var boardList = MutableLiveData<List<DataByTypeDefaultCode>>()
    var countryList = MutableLiveData<List<DataByTypeDefaultCode>>()
    var relationList = MutableLiveData<List<DataByTypeDefaultCode>>()
    var bloodGroupList = MutableLiveData<List<DataByTypeDefaultCode>>()
    var divisionList = MutableLiveData<List<DataByTypeDefaultCode>>()
    var districtList = MutableLiveData<List<DataByTypeDefaultCode>>()
    var upazillaList = MutableLiveData<List<DataByTypeDefaultCode>>()

    fun getUserProfileInfoData() {
        Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val userCall = service?.getUserProfileInfo(header)

        userCall?.enqueue(object : Callback<ProfileInformation> {
            override fun onResponse(
                call: Call<ProfileInformation>,
                response: Response<ProfileInformation>
            ) {
                try {
                    if (response.isSuccessful) {
                        profileInformation.postValue(response.body())
                    } else {
                        Loaders.apiError.value = "Couldn't load profile information!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    e.printStackTrace()
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load profile information!"
                }
            }

            override fun onFailure(call: Call<ProfileInformation>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load profile information!"
            }
        })
    }

    fun getProfilePhoto(imagePath: String) {
         Loaders.isLoading0.value = true

        val header = HashMap<String?, String?>()
        header["Authorization"] = "bearer ${appPreferences.getToken()}"

        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getProfile(header, imagePath)

        //calling the api
        call?.enqueue(object : Callback<ProfileImage> {
            override fun onResponse(call: Call<ProfileImage>, response: Response<ProfileImage>) {
                try {
                    if (response.code() == 200) {
                        photoFileContent.postValue(response.body()?.fileContent)
                    } else {
                        Loaders.apiError.value = "Couldn't load profile picture!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load profile picture!"
                }
                Log.d("onResponse", "Main Response:" + response.body())

                if (response.code() == 200) {
                    Log.d("onResponse", "Profile Image:" + response.body()?.fileContent)
                    photoFileContent.postValue(response.body()?.fileContent)
                    Log.d("onResponse", "Photo:" + response.body()?.fileContent)
                }
            }

            override fun onFailure(call: Call<ProfileImage>, t: Throwable) {
//                Log.d("onFailure", t.toString())
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load profile picture!"
            }
        })
    }

    fun getEducationInfo() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getEducationInfo("bearer" + appPreferences.getToken())

        call?.enqueue(object : Callback<List<EducationInfo>> {
            override fun onFailure(call: Call<List<EducationInfo>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load education info!"
            }

            override fun onResponse(
                call: Call<List<EducationInfo>>,
                response: Response<List<EducationInfo>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<EducationInfo>>() {}.type
                        val errorResponse: List<EducationInfo>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        educationInfoList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load education info!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load education info!"
                }
            }
        })
    }

    fun getTrainingInfo() {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getTrainingInfo("bearer" + appPreferences.getToken())

        call?.enqueue(object : Callback<List<TrainingInfo>> {
            override fun onFailure(call: Call<List<TrainingInfo>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load training info!"
            }

            override fun onResponse(
                call: Call<List<TrainingInfo>>,
                response: Response<List<TrainingInfo>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<TrainingInfo>>() {}.type
                        val errorResponse: List<TrainingInfo>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        trainingInfoList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load training info!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load training info!"
                }
            }
        })
    }

    fun getCertificationInfo() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getCertificationInfo("bearer" + appPreferences.getToken())

        call?.enqueue(object : Callback<List<CertificationInfo>> {
            override fun onFailure(call: Call<List<CertificationInfo>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load education info!"
            }

            override fun onResponse(
                call: Call<List<CertificationInfo>>,
                response: Response<List<CertificationInfo>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<CertificationInfo>>() {}.type
                        val errorResponse: List<CertificationInfo>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        certificationInfoList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load education info!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load education info!"
                }
            }
        })
    }

    fun getExperienceInfo() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getExperienceInfo("bearer" + appPreferences.getToken())

        call?.enqueue(object : Callback<List<ExperienceInfo>> {
            override fun onFailure(call: Call<List<ExperienceInfo>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load experience info!"
            }

            override fun onResponse(
                call: Call<List<ExperienceInfo>>,
                response: Response<List<ExperienceInfo>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<ExperienceInfo>>() {}.type
                        val errorResponse: List<ExperienceInfo>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        experienceInfoList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load experience info!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load experience info!"
                }
            }
        })
    }

    fun getReferenceInfo() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getReferenceInfo("bearer" + appPreferences.getToken())

        call?.enqueue(object : Callback<List<ReferenceInfo>> {
            override fun onFailure(call: Call<List<ReferenceInfo>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load reference info!"
            }

            override fun onResponse(
                call: Call<List<ReferenceInfo>>,
                response: Response<List<ReferenceInfo>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<ReferenceInfo>>() {}.type
                        val errorResponse: List<ReferenceInfo>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        referenceInfoList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load reference info!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load reference info!"
                }
            }
        })
    }

    fun getBloodGroup() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call =
            apiInterface?.getDataByTypeDefaultCode("bearer " + appPreferences.getToken(), "T139")

        call?.enqueue(object : Callback<List<DataByTypeDefaultCode>> {
            override fun onFailure(call: Call<List<DataByTypeDefaultCode>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load level of education!"
            }

            override fun onResponse(
                call: Call<List<DataByTypeDefaultCode>>,
                response: Response<List<DataByTypeDefaultCode>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<DataByTypeDefaultCode>>() {}.type
                        val errorResponse: List<DataByTypeDefaultCode>? =
                            Gson().fromJson(response.errorBody()!!.charStream(), type)
                        bloodGroupList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load level of education!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load level of education!"
                }
            }
        })
    }

    fun getEducationLevels() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getDataByTypeDefaultCode("bearer " + appPreferences.getToken(), "T140")

        call?.enqueue(object : Callback<List<DataByTypeDefaultCode>> {
            override fun onFailure(call: Call<List<DataByTypeDefaultCode>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load level of education!"
            }

            override fun onResponse(
                call: Call<List<DataByTypeDefaultCode>>,
                response: Response<List<DataByTypeDefaultCode>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<DataByTypeDefaultCode>>() {}.type
                        val errorResponse: List<DataByTypeDefaultCode>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        educationLevelList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load level of education!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load level of education!"
                }
            }
        })
    }

    fun getMajorSubjects() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getDataByTypeDefaultCode("bearer " + appPreferences.getToken(), "T142")

        call?.enqueue(object : Callback<List<DataByTypeDefaultCode>> {
            override fun onFailure(call: Call<List<DataByTypeDefaultCode>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load major subjects!"
            }

            override fun onResponse(
                call: Call<List<DataByTypeDefaultCode>>,
                response: Response<List<DataByTypeDefaultCode>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<DataByTypeDefaultCode>>() {}.type
                        val errorResponse: List<DataByTypeDefaultCode>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        majorSubjectList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load major subjects!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load major subjects!"
                }
            }
        })
    }

    fun getResults() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getDataByTypeDefaultCode("bearer " + appPreferences.getToken(), "T116")

        call?.enqueue(object : Callback<List<DataByTypeDefaultCode>> {
            override fun onFailure(call: Call<List<DataByTypeDefaultCode>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load results!"
            }

            override fun onResponse(
                call: Call<List<DataByTypeDefaultCode>>,
                response: Response<List<DataByTypeDefaultCode>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<DataByTypeDefaultCode>>() {}.type
                        val errorResponse: List<DataByTypeDefaultCode>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        resultList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load results!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load results!"
                }
            }
        })
    }

    fun getPassingYears() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getDataByTypeDefaultCode("bearer " + appPreferences.getToken(), "T141")

        call?.enqueue(object : Callback<List<DataByTypeDefaultCode>> {
            override fun onFailure(call: Call<List<DataByTypeDefaultCode>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load passing years!"
            }

            override fun onResponse(
                call: Call<List<DataByTypeDefaultCode>>,
                response: Response<List<DataByTypeDefaultCode>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<DataByTypeDefaultCode>>() {}.type
                        val errorResponse: List<DataByTypeDefaultCode>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        passingYearList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load passing years!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load passing years!"
                }
            }
        })
    }

    fun getBoards() {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getDataByTypeDefaultCode("bearer " + appPreferences.getToken(), "T106")

        call?.enqueue(object : Callback<List<DataByTypeDefaultCode>> {
            override fun onFailure(call: Call<List<DataByTypeDefaultCode>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load boards!"
            }

            override fun onResponse(
                call: Call<List<DataByTypeDefaultCode>>,
                response: Response<List<DataByTypeDefaultCode>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<DataByTypeDefaultCode>>() {}.type
                        val errorResponse: List<DataByTypeDefaultCode>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        boardList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load boards!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load boards!"
                }
            }
        })
    }

    fun getCountry() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.getDataByTypeDefaultCode("bearer " + appPreferences.getToken(), "T10201")

        call?.enqueue(object : Callback<List<DataByTypeDefaultCode>> {
            override fun onFailure(call: Call<List<DataByTypeDefaultCode>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load country!"
            }

            override fun onResponse(
                call: Call<List<DataByTypeDefaultCode>>,
                response: Response<List<DataByTypeDefaultCode>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<DataByTypeDefaultCode>>() {}.type
                        val errorResponse: List<DataByTypeDefaultCode>? = Gson().fromJson(response.errorBody()!!.charStream(), type)
                        countryList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load country!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load country!"
                }
            }
        })
    }

    fun getRelation() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call =
            apiInterface?.getDataByTypeDefaultCode("bearer " + appPreferences.getToken(), "T144")

        call?.enqueue(object : Callback<List<DataByTypeDefaultCode>> {
            override fun onFailure(call: Call<List<DataByTypeDefaultCode>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load relation data!"
            }

            override fun onResponse(
                call: Call<List<DataByTypeDefaultCode>>,
                response: Response<List<DataByTypeDefaultCode>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<DataByTypeDefaultCode>>() {}.type
                        val errorResponse: List<DataByTypeDefaultCode>? =
                            Gson().fromJson(response.errorBody()!!.charStream(), type)
                        relationList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load relation data!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load relation data!"
                }
            }
        })
    }

    fun saveAcademicEducation(postAcademicEducation: PostAcademicEducation) {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.saveAcademicEducationInfo("bearer " + appPreferences.getToken(), postAcademicEducation)

        call?.enqueue(object : Callback<String>{
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't save information! Please try again."
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.value = "Saved successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't save information! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't save information! Please try again."
                }
            }
        })
    }

    fun saveTrainingInfo(postTrainingInfo: PostTrainingInfo) {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.saveTrainingInfo("bearer " + appPreferences.getToken(), postTrainingInfo)

        call?.enqueue(object : Callback<String>{
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't save information! Please try again."
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.value = "Saved successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't save information! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't save information! Please try again."
                }
            }
        })
    }

    fun saveCertificationInfo(postCertificationInfo: PostCertificationInfo) {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.saveCertificationInfo("bearer " + appPreferences.getToken(), postCertificationInfo)

        call?.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't save information! Please try again."
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.value = "Saved successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't save information! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't save information! Please try again."
                }
            }
        })
    }

    fun saveExperienceInfo(postExperienceInfo: PostExperienceInfo) {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.saveExperienceInfo(
            "bearer " + appPreferences.getToken(),
            postExperienceInfo
        )

        call?.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't save information! Please try again."
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.value = "Saved successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't save information! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't save information! Please try again."
                }
            }
        })
    }

    fun saveReferenceInfo(postReferenceInfo: PostReferenceInfo) {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.saveReferenceInfo(
            "bearer " + appPreferences.getToken(),
            postReferenceInfo
        )

        call?.enqueue(object : Callback<String>{
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't save information! Please try again."
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.value = "Saved successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't save information! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't save information! Please try again."
                }
            }
        })
    }

    fun updateCertificationInfo(updateCertificationInfo: UpdateCertificationInfo) {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.updateCertificationInfo(
            "bearer " + appPreferences.getToken(),
            updateCertificationInfo
        )

        call?.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't update information! Please try again."
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.value = "Updated successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't update information! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't update information! Please try again."
                }
            }
        })
    }


    fun updateExperienceInfo(updateExperienceInfo: UpdateExperienceInfo) {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.updateExperienceInfo(
            "bearer " + appPreferences.getToken(),
            updateExperienceInfo
        )

        call?.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't update information! Please try again."
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.value = "Updated successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't update information! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't update information! Please try again."
                }
            }
        })
    }

    fun updateAcademicEducation(postAcademicEducation: PostAcademicEducation) {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.updateEducationInfo(
            "bearer " + appPreferences.getToken(),
            postAcademicEducation
        )

        call?.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't update information! Please try again."
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.value = "Updated successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't update information! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't update information! Please try again."
                }
            }
        })
    }

    fun updateTrainingInfo(postTrainingInfo: PostTrainingInfo) {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.updateTrainingInfo("bearer " + appPreferences.getToken(), postTrainingInfo)

        call?.enqueue(object : Callback<String>{
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't save information! Please try again."
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.value = "Saved successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't save information! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't save information! Please try again."
                }
            }
        })
    }

    fun updateReferenceInfo(updateReferenceInfo: UpdateReferenceInfo) {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.updateReferenceInfo(
            "bearer " + appPreferences.getToken(),
            updateReferenceInfo
        )

        call?.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't update information! Please try again."
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.value = "Updated successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't update information! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't update information! Please try again."
                }
            }
        })
    }

    fun updatePersonalInfo(profileInformation: ProfileInformation_) {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.updatePersonalInfo(
            "bearer " + appPreferences.getToken(),
            profileInformation
        )

        call?.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't update information! Please try again."
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.value = "Updated successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't update information! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't update information! Please try again."
                }
            }
        })
    }

    fun getDivision() {
        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call =
            apiInterface?.getDataByTypeDefaultCode("bearer " + appPreferences.getToken(), "T10202")

        call?.enqueue(object : Callback<List<DataByTypeDefaultCode>> {
            override fun onFailure(call: Call<List<DataByTypeDefaultCode>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load division data!"
            }

            override fun onResponse(
                call: Call<List<DataByTypeDefaultCode>>,
                response: Response<List<DataByTypeDefaultCode>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<DataByTypeDefaultCode>>() {}.type
                        val errorResponse: List<DataByTypeDefaultCode>? =
                            Gson().fromJson(response.errorBody()!!.charStream(), type)
                        divisionList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load division data!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load division data!"
                }
            }
        })
    }

    fun getDistrict(divisionId: String?) {
        Loaders.isLoading0.value = true
        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call =
            apiInterface?.getDistrictList("bearer " + appPreferences.getToken(), divisionId!!)

        call?.enqueue(object : Callback<List<DataByTypeDefaultCode>> {
            override fun onFailure(call: Call<List<DataByTypeDefaultCode>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load district data!"
            }

            override fun onResponse(
                call: Call<List<DataByTypeDefaultCode>>,
                response: Response<List<DataByTypeDefaultCode>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<DataByTypeDefaultCode>>() {}.type
                        val errorResponse: List<DataByTypeDefaultCode>? =
                            Gson().fromJson(response.errorBody()!!.charStream(), type)
                        districtList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load district data!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load district data!"
                }
            }
        })
    }

    fun getUpazilla(districtId: String?) {
        Loaders.isLoading0.value = true
        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call =
            apiInterface?.getDistrictList("bearer " + appPreferences.getToken(), districtId!!)

        call?.enqueue(object : Callback<List<DataByTypeDefaultCode>> {
            override fun onFailure(call: Call<List<DataByTypeDefaultCode>>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't load upazilla data!"
            }

            override fun onResponse(
                call: Call<List<DataByTypeDefaultCode>>,
                response: Response<List<DataByTypeDefaultCode>>
            ) {
                try {
                    if (response.code() == 302) {
                        val type = object : TypeToken<List<DataByTypeDefaultCode>>() {}.type
                        val errorResponse: List<DataByTypeDefaultCode>? =
                            Gson().fromJson(response.errorBody()!!.charStream(), type)
                        upazillaList.value = errorResponse
                    } else {
                        Loaders.apiError.value = "Couldn't load upazilla data!"
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't load upazilla data!"
                }
            }
        })
    }


    fun addPoint() {

        Loaders.isLoading0.value = true

        val apiInterface = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = apiInterface?.addPoint("bearer " + appPreferences.getToken())

        call?.enqueue(object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                Loaders.isLoading0.value = false
                Loaders.apiError.value = "Couldn't Add Point ! Please try again."
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    if (response.isSuccessful) {
                        Loaders.apiSuccess.value = "Add Point successfully."
                    } else {
                        Loaders.apiError.value = "Couldn't Add Point ! Please try again."
                    }

                    Loaders.isLoading0.value = false
                } catch (e: Exception) {
                    Loaders.isLoading0.value = false
                    Loaders.apiError.value = "Couldn't Add Point ! Please try again."
                }
            }
        })
    }
}