# List of SDK dependencies of this app, this information is also included in an encrypted form in the APK.
# For more information visit: https://d.android.com/r/tools/dependency-metadata

library {
  maven_library {
    groupId: "androidx.databinding"
    artifactId: "databinding-common"
    version: "4.0.1"
  }
  digests {
    sha256: "\034\311i\334\227T\352\316<\202U\270{W\312VO\340\2607\006\037#\345\267g\330\241\367S\210H"
  }
}
library {
  maven_library {
    groupId: "androidx.databinding"
    artifactId: "databinding-runtime"
    version: "4.0.1"
  }
  digests {
    sha256: "\316\214\271\204\204(\250(\366\223}\216\202\235hE\243\362\320\200D<\222\335\270\303t\216\331Pz\214"
  }
}
library {
  maven_library {
    groupId: "androidx.databinding"
    artifactId: "viewbinding"
    version: "4.0.1"
  }
  digests {
    sha256: "r8\222\221\032v\243\335\307Cm\222\r\202\261\300T9\216\337\a\313{\032\b\004\021\221\037\271\006m"
  }
}
library {
  maven_library {
    groupId: "androidx.lifecycle"
    artifactId: "lifecycle-runtime"
    version: "2.2.0"
  }
  digests {
    sha256: "/\206l\a\241\363:\214\233\266\232\225E\324\362\vO\006(\315\n\025T28m|\260\201\341\340\274"
  }
}
library {
  maven_library {
    groupId: "androidx.collection"
    artifactId: "collection"
    version: "1.1.0"
  }
  digests {
    sha256: "c*\016T\aF\035\347t@\223R\224\016)*)\0207rB\a\247\207\202\fw\332\367\323;r"
  }
}
library {
  maven_library {
    groupId: "androidx.annotation"
    artifactId: "annotation"
    version: "1.1.0"
  }
  digests {
    sha256: "\323\215c\355\263\017\024g\201\215P\252\360_\212i-\352\21319*\004\233\372\231\033\025\232\325\266\222"
  }
}
library {
  maven_library {
    groupId: "androidx.lifecycle"
    artifactId: "lifecycle-common"
    version: "2.2.0"
  }
  digests {
    sha256: "c\211\215\253\367\317\345\354]~\330\270\302VL\024\'\276\207n\024\226\352\331\\\'\003\317Y\323sK"
  }
}
library {
  maven_library {
    groupId: "androidx.arch.core"
    artifactId: "core-common"
    version: "2.1.0"
  }
  digests {
    sha256: "\376\0227\277\002\235\006>\177)\3769\256\257s\357t\310\260\243e\204\206\374)\323\305C&e8\211"
  }
}
library {
  maven_library {
    groupId: "androidx.databinding"
    artifactId: "databinding-adapters"
    version: "4.0.1"
  }
  digests {
    sha256: "zm\232/\2009\305\257\240\324t\263\250\355\263\r]\363\337i\273@r\234h\370\322\272\360+I\355"
  }
}
library {
  maven_library {
    groupId: "org.jetbrains.kotlin"
    artifactId: "kotlin-android-extensions-runtime"
    version: "1.3.70"
  }
  digests {
    sha256: "x\363\344\354t\213\230\260\3415\333\247\365\355\0363\213b\354\314\243\3429\305\340\271\252X\237\217\227J"
  }
}
library {
  maven_library {
    groupId: "org.jetbrains.kotlin"
    artifactId: "kotlin-stdlib"
    version: "1.3.70"
  }
  digests {
    sha256: "2\004<\016\243*(\034\336(\215j\276\271\262\216ff\000\325\266\274^\313\312\270\360\3519\37123"
  }
}
library {
  maven_library {
    groupId: "org.jetbrains.kotlin"
    artifactId: "kotlin-stdlib-common"
    version: "1.3.70"
  }
  digests {
    sha256: "n#w\317\304\211\217/\262D)\225\033\023;W\v%\016?\206\n\204X\262\241\370\246<\365:P"
  }
}
library {
  maven_library {
    groupId: "org.jetbrains"
    artifactId: "annotations"
    version: "13.0"
  }
  digests {
    sha256: "\254\342\241\r\310\342\325\3754\222^\312\300>I\210\262\300\370Qe\f\224\270\316\364\233\241\275\021\024x"
  }
}
library {
  maven_library {
    groupId: "org.jetbrains.kotlin"
    artifactId: "kotlin-stdlib-jdk7"
    version: "1.3.70"
  }
  digests {
    sha256: "\364\001\252\314\301L\"4\357#]\250\240\350.\321\341f\257\367\340\006$\266@\352=\337\305$Z\366"
  }
}
library {
  maven_library {
    groupId: "androidx.appcompat"
    artifactId: "appcompat"
    version: "1.1.0"
  }
  digests {
    sha256: "\215r\231\274\244L\263\275\361\177U\225vj\313\364Y\374\201\376\342#\350hl\306\254\323\244*\265\300"
  }
}
library {
  maven_library {
    groupId: "androidx.core"
    artifactId: "core"
    version: "1.2.0"
  }
  digests {
    sha256: "RK\213\210\316\266\247J~D\346\265g\2415f\017!\027\231\220L\262\030\277\356[\341\026h \262"
  }
}
library {
  maven_library {
    groupId: "androidx.cursoradapter"
    artifactId: "cursoradapter"
    version: "1.0.0"
  }
  digests {
    sha256: "\250\034\217\347\210\025\372G\337[t\235\353Rrz\321\037\223\227\332X\261`\027\364\353,\021\342\205d"
  }
}
library {
  maven_library {
    groupId: "androidx.fragment"
    artifactId: "fragment"
    version: "1.2.0"
  }
  digests {
    sha256: "\375\320\352\310\fk&\307\220\223\246?\306\2310?\222\214\301\372s\312q\226\325Y\nw\353mhs"
  }
}
library {
  maven_library {
    groupId: "androidx.appcompat"
    artifactId: "appcompat-resources"
    version: "1.1.0"
  }
  digests {
    sha256: "\031\224M2\264eQ\241|4~!\211K\225\203\177\275{\252\257\311\342\b\'\2244O\"/sa"
  }
}
library {
  maven_library {
    groupId: "androidx.drawerlayout"
    artifactId: "drawerlayout"
    version: "1.0.0"
  }
  digests {
    sha256: "\224\002D,\334ZC\317b\373\024\370\317\230\3063B\324\331\331\270\005\310\003<l\367\350\002t\232\301"
  }
}
library {
  maven_library {
    groupId: "androidx.versionedparcelable"
    artifactId: "versionedparcelable"
    version: "1.1.0"
  }
  digests {
    sha256: "\232\035w\024\n\302\"\267\206kPT\356}\025\233\301\200\t\207\355-F\335j\375\321E\253\267\020\301"
  }
}
library {
  maven_library {
    groupId: "androidx.viewpager"
    artifactId: "viewpager"
    version: "1.0.0"
  }
  digests {
    sha256: "\024z\364\341J\031\204\001\r\217\025^^\031\327\201\360<\035p\337\355\002\250\340\321\204(\270\374\206\202"
  }
}
library {
  maven_library {
    groupId: "androidx.loader"
    artifactId: "loader"
    version: "1.0.0"
  }
  digests {
    sha256: "\021\3675\313;U\304X\324p\276\331\342RT7[Q\213K\033\255i&x:p&\333\017P%"
  }
}
library {
  maven_library {
    groupId: "androidx.activity"
    artifactId: "activity"
    version: "1.1.0"
  }
  digests {
    sha256: "O+5\221gh\003/}\f \342P\342\213)\003~\324\316\236\277=\244\374\325\033\313\f`g\357"
  }
}
library {
  maven_library {
    groupId: "androidx.lifecycle"
    artifactId: "lifecycle-livedata-core"
    version: "2.2.0"
  }
  digests {
    sha256: "Ul\037:\371\n\251\327\320\3230VZ\333\366\332q\262B\221H\272\311\036\a\304\205\364\371\253\366\024"
  }
}
library {
  maven_library {
    groupId: "androidx.lifecycle"
    artifactId: "lifecycle-viewmodel"
    version: "2.2.0"
  }
  digests {
    sha256: "\226~\372\262MlI\335AJ\214\n\304\241\315\t\260\030\360\270\273C\2679\2556\fAX\353\336\'"
  }
}
library {
  maven_library {
    groupId: "androidx.lifecycle"
    artifactId: "lifecycle-viewmodel-savedstate"
    version: "1.0.0"
  }
  digests {
    sha256: "\364\314\352\373\370j\317\307\363\272ja\326\334hB\246s\214\022ta\ag\323\253\217\212\021L\272\227"
  }
}
library {
  maven_library {
    groupId: "androidx.customview"
    artifactId: "customview"
    version: "1.0.0"
  }
  digests {
    sha256: " \345\270\366Rj4YZ`OVq\215\250\021g\300\264\nz\224\245}\2525Vc\362YM\362"
  }
}
library {
  maven_library {
    groupId: "androidx.lifecycle"
    artifactId: "lifecycle-livedata"
    version: "2.2.0"
  }
  digests {
    sha256: "\330:\371H`\252\237d\313\334Q\364\a\226\247\317U\261\026\360\346\357\327R\350E\300\020L\213\026\366"
  }
}
library {
  maven_library {
    groupId: "androidx.arch.core"
    artifactId: "core-runtime"
    version: "2.1.0"
  }
  digests {
    sha256: "\335wa[\323\335\'Z\373\021\266-\362[\256F\261\vJ\021|\323yC\257E\275\313\370uXR"
  }
}
library {
  maven_library {
    groupId: "androidx.savedstate"
    artifactId: "savedstate"
    version: "1.0.0"
  }
  digests {
    sha256: "%\020\245a\2347W\234\234\341\240Et\372\2572<\320\377\342\374N \372\217\217\001\345\273@.\203"
  }
}
library {
  maven_library {
    groupId: "androidx.vectordrawable"
    artifactId: "vectordrawable"
    version: "1.1.0"
  }
  digests {
    sha256: "F\375c:\300\033I\267\374\253\302c\277\t\214Z\213\236\232iwM#N\334\312\004\373\002\337\216&"
  }
}
library {
  maven_library {
    groupId: "androidx.vectordrawable"
    artifactId: "vectordrawable-animated"
    version: "1.1.0"
  }
  digests {
    sha256: "v\332,P#q\331\303\200T\337^+$\215\000\332\207\200\236\320X\3636>\256\207\316^$\003\370"
  }
}
library {
  maven_library {
    groupId: "androidx.interpolator"
    artifactId: "interpolator"
    version: "1.0.0"
  }
  digests {
    sha256: "3\03115\246O\342\037\242\303^\354f\210\361\247nQ&\006\300\374\203\334\033h\2367\255\327s*"
  }
}
library {
  maven_library {
    groupId: "androidx.core"
    artifactId: "core-ktx"
    version: "1.2.0"
  }
  digests {
    sha256: "\334\267MQ\rU+5\357\367;\r\322{\202\226IS_9\002\345\265\241\362`@8<\020\251@"
  }
}
library {
  maven_library {
    groupId: "androidx.constraintlayout"
    artifactId: "constraintlayout"
    version: "1.1.3"
  }
  digests {
    sha256: "_\370d\336\371\324\034\320N\b4\215iY\021C\272\343\316\377B\204\317\206\b\274\353\230\303j\3100"
  }
}
library {
  maven_library {
    groupId: "androidx.constraintlayout"
    artifactId: "constraintlayout-solver"
    version: "1.1.3"
  }
  digests {
    sha256: "\226\\\027~d\373\330\033\321\322{@+f\357\235{\307\265\313_q\200D\277zE:\274T E"
  }
}
library {
  maven_library {
    groupId: "androidx.legacy"
    artifactId: "legacy-support-v4"
    version: "1.0.0"
  }
  digests {
    sha256: "x\376\301H_\0178\212GI\002-\325\024\026\205q\'\315%D\256\034?\320\261e\211\005T\200\260"
  }
}
library {
  maven_library {
    groupId: "androidx.media"
    artifactId: "media"
    version: "1.0.0"
  }
  digests {
    sha256: "\262;R{+\254\207\fJtQ\346\230-q2\344\023\350\215\177\'\333\353\037\307d\nr\f\331\356"
  }
}
library {
  maven_library {
    groupId: "androidx.legacy"
    artifactId: "legacy-support-core-utils"
    version: "1.0.0"
  }
  digests {
    sha256: "\247\355\317\001\325\265+04\a0\'\274Gu\267\212Gd\273b\002\273\221\326\034\202\232\335\215\321\307"
  }
}
library {
  maven_library {
    groupId: "androidx.legacy"
    artifactId: "legacy-support-core-ui"
    version: "1.0.0"
  }
  digests {
    sha256: "\r\022`\306\347\346\2437\370u\337q\265\026\223\036p?qn\220\210\230\027\315: \372Z\303\331G"
  }
}
library {
  maven_library {
    groupId: "androidx.documentfile"
    artifactId: "documentfile"
    version: "1.0.0"
  }
  digests {
    sha256: "\206Z\006\036\362\372\321e\"\370C56\270\324r\b\304o\367\307tQ\227\337\241\356\264\201\206\224\207"
  }
}
library {
  maven_library {
    groupId: "androidx.localbroadcastmanager"
    artifactId: "localbroadcastmanager"
    version: "1.0.0"
  }
  digests {
    sha256: "\347\0342\214\356\365\304\247\327o-\206\337\033e\326_\342\254\370h\261\244\357\330J?43a\206\330"
  }
}
library {
  maven_library {
    groupId: "androidx.print"
    artifactId: "print"
    version: "1.0.0"
  }
  digests {
    sha256: "\035\\\17715\241\273\246a\3747?\327.\021\353\nJ\333\2639g\207\202m\330\344\031\r]\236\335"
  }
}
library {
  maven_library {
    groupId: "androidx.coordinatorlayout"
    artifactId: "coordinatorlayout"
    version: "1.0.0"
  }
  digests {
    sha256: "\345\b\306\225H\224\2237M\224+\367\264\356\002\253\367W\035%\252\304\306\"\345}l\325\315)\353s"
  }
}
library {
  maven_library {
    groupId: "androidx.slidingpanelayout"
    artifactId: "slidingpanelayout"
    version: "1.0.0"
  }
  digests {
    sha256: "v\277\373|\357\277x\a\224\330\201p\002\332\321V/>\'\300\251\367F\326$\001\310\355\263\n\356\336"
  }
}
library {
  maven_library {
    groupId: "androidx.swiperefreshlayout"
    artifactId: "swiperefreshlayout"
    version: "1.0.0"
  }
  digests {
    sha256: "\227a\263\250\t\311\260\223\375\006\243\304\273\306Eum\354\016\225\265\311\332A\233\311\362\243\363\002n\215"
  }
}
library {
  maven_library {
    groupId: "androidx.asynclayoutinflater"
    artifactId: "asynclayoutinflater"
    version: "1.0.0"
  }
  digests {
    sha256: "\367\352\266\fW\255\335\224\273\006\'X2\376v\000a\033\352\252\341\241\354Y|#\031V\372\371l\213"
  }
}
library {
  maven_library {
    groupId: "com.squareup.retrofit2"
    artifactId: "retrofit"
    version: "2.6.1"
  }
  digests {
    sha256: "o\355\372\2138\320Z\317NR\335\344\273\327J\355m\276\003H|]&\177\340Hmz(~\227\022"
  }
}
library {
  maven_library {
    groupId: "com.squareup.okhttp3"
    artifactId: "okhttp"
    version: "3.12.0"
  }
  digests {
    sha256: "qx\177,Y\236\004A\307\244A9\203\277\335\223\324\vV\341\272\334^\004\023\326\244\304\205\272?5"
  }
}
library {
  maven_library {
    groupId: "com.squareup.okio"
    artifactId: "okio"
    version: "1.15.0"
  }
  digests {
    sha256: "i?\243\031\247\350\2043\000`+ @#\267gO\020n\274\265w\362\335X\a!+f\021\213\322"
  }
}
library {
  maven_library {
    groupId: "com.squareup.retrofit2"
    artifactId: "converter-gson"
    version: "2.6.1"
  }
  digests {
    sha256: "\246\205\260\031\263/e\322YAF\037qe\320\267&\210\246\033\036\024!\246\356\332\363\356\033\021mV"
  }
}
library {
  maven_library {
    groupId: "com.google.code.gson"
    artifactId: "gson"
    version: "2.8.5"
  }
  digests {
    sha256: "#:\001I\3746\\\237n\333\326\203\317\342f\261\233\334w;\351\216\253\332\366\263\311$\264\216}\201"
  }
}
library {
  maven_library {
    groupId: "com.squareup.okhttp3"
    artifactId: "logging-interceptor"
    version: "3.10.0"
  }
  digests {
    sha256: "\370o\312p<-o\307l\272s\304iW\322\261\026\035\377 \361\342q\367\232\336\362{\277\033\337\241"
  }
}
library {
  maven_library {
    groupId: "com.squareup.retrofit2"
    artifactId: "converter-scalars"
    version: "2.5.0"
  }
  digests {
    sha256: "+{W\276\006}m\022.\373\202d\242\310\004\251\035\217\222\315T\325K\343a\251\260\b1\211\303\323"
  }
}
library {
  maven_library {
    groupId: "com.squareup.okhttp"
    artifactId: "okhttp"
    version: "2.7.5"
  }
  digests {
    sha256: "\210\254\237\321\273Q\370+\314fL\301\353\234\"\\\220\334C\211\326`#\033L\3077\276\277\347\320\252"
  }
}
library {
  maven_library {
    groupId: "androidx.recyclerview"
    artifactId: "recyclerview"
    version: "1.0.0"
  }
  digests {
    sha256: "\006\225o\261\254\001@\'\312\235+@F\232KB\252a\264\225{\261\030H\341\3775\'\001\253EH"
  }
}
library {
  maven_library {
    groupId: "androidx.lifecycle"
    artifactId: "lifecycle-extensions"
    version: "2.2.0"
  }
  digests {
    sha256: "d\214\215\341\321\v\002]RJ.F\254\231O\303\366\277\030h&\300\236\301\246-%\v\361\270w\256"
  }
}
library {
  maven_library {
    groupId: "androidx.lifecycle"
    artifactId: "lifecycle-process"
    version: "2.2.0"
  }
  digests {
    sha256: ":\227~wx\374\204\030t-8\204\t\332\253\247\352\217\352\210#\322\037\373\226\344\304#oqPp"
  }
}
library {
  maven_library {
    groupId: "androidx.lifecycle"
    artifactId: "lifecycle-service"
    version: "2.2.0"
  }
  digests {
    sha256: "\312(\001\377\300iUZ\376\330\355\335\"\222\023\017CiVE+\310\273\2550\373V\370\344\343\202\240"
  }
}
library {
  maven_library {
    groupId: "androidx.navigation"
    artifactId: "navigation-fragment-ktx"
    version: "2.0.0"
  }
  digests {
    sha256: "\212\327\267\355vP\t\'\034\300\376\227\352\222[?\275\220\251\274\200\256\357\262p\212UMs0\270\323"
  }
}
library {
  maven_library {
    groupId: "androidx.navigation"
    artifactId: "navigation-fragment"
    version: "2.0.0"
  }
  digests {
    sha256: "8\335\361\257\311\232\005\205\220O!!\356\347\023\037\357\023Y^d\311\273k\"\366\215\326k\215\037\234"
  }
}
library {
  maven_library {
    groupId: "androidx.navigation"
    artifactId: "navigation-runtime-ktx"
    version: "2.0.0"
  }
  digests {
    sha256: "A\361\024_\232\312Z}\227V}\376\b\222N\301\316\274\307,\262\305\326\321\241\3151\247\200f\223X"
  }
}
library {
  maven_library {
    groupId: "androidx.navigation"
    artifactId: "navigation-runtime"
    version: "2.0.0"
  }
  digests {
    sha256: "\220<\331=4$0\017\360H\251 \206\327s\317\305\342?\222\200c\324I:E\264\260B\362\245P"
  }
}
library {
  maven_library {
    groupId: "androidx.navigation"
    artifactId: "navigation-common"
    version: "2.0.0"
  }
  digests {
    sha256: "T.\306\371\223\266\323\277\257\342\345>\017%\v\206\2053\036*\002X\3711:\371\3563v\3719Y"
  }
}
library {
  maven_library {
    groupId: "androidx.navigation"
    artifactId: "navigation-common-ktx"
    version: "2.0.0"
  }
  digests {
    sha256: "\355\000\266\367=\212\314~\006r\237_\031\315\353A\f\241\320\344Z\373\317o\3645\241\t\034:=W"
  }
}
library {
  maven_library {
    groupId: "androidx.navigation"
    artifactId: "navigation-ui-ktx"
    version: "2.0.0"
  }
  digests {
    sha256: "\373(\202kN\262\nS\3519\205v\273\236z2\025\317\227\016\231\240]|\314\004\"\325\227\217\251\025"
  }
}
library {
  maven_library {
    groupId: "androidx.navigation"
    artifactId: "navigation-ui"
    version: "2.0.0"
  }
  digests {
    sha256: "\326\355\300\215i&\211g\321\0236\275\214-\316\037\025O\n\350\021{\351\207H\225\366\227A#\355 "
  }
}
library {
  maven_library {
    groupId: "com.google.android.material"
    artifactId: "material"
    version: "1.0.0"
  }
  digests {
    sha256: "v\200\343\201\243\3007\230\331\231\262\344A\312\255\330\245j\n\200\216\020\200$\246z\371\376&\301\032\334"
  }
}
library {
  maven_library {
    groupId: "androidx.transition"
    artifactId: "transition"
    version: "1.0.0"
  }
  digests {
    sha256: "\240\n\017v?@\032\274\354\332\233\016\257\313s\211)\305\200\033\021\032\232AK\201\241\223\320\364\000\215"
  }
}
library {
  maven_library {
    groupId: "androidx.cardview"
    artifactId: "cardview"
    version: "1.0.0"
  }
  digests {
    sha256: "\021\223\300L\"\243\326\265\224m\256\237N\214Y\326\255\336jq\266\275]\207\373\231\330-\332\032\376\307"
  }
}
library {
  maven_library {
    groupId: "androidx.room"
    artifactId: "room-runtime"
    version: "2.2.4"
  }
  digests {
    sha256: "\217\2067~-\315\036\201o\002&j\0001\341}\364\357\264\240\226\333\222s\375\236\272z\245\205\r\317"
  }
}
library {
  maven_library {
    groupId: "androidx.room"
    artifactId: "room-common"
    version: "2.2.4"
  }
  digests {
    sha256: "+\023\r\324\241\323\331\033g\001\3553\tm8\237\001\304\374\021\227\247\254\326\271\027$\335\305\254\374\006"
  }
}
library {
  maven_library {
    groupId: "androidx.sqlite"
    artifactId: "sqlite-framework"
    version: "2.1.0"
  }
  digests {
    sha256: "\206ss\177\333.\373\255\221\256\256\355\031\'\353\262\222\022\323j\206}\223\271c\234\200i\001\237\212\036"
  }
}
library {
  maven_library {
    groupId: "androidx.sqlite"
    artifactId: "sqlite"
    version: "2.1.0"
  }
  digests {
    sha256: "\203A\377\t-``\326*\a\"\177)#qU\377\363o\261o\226\311_\275\232\210N7]\271\022"
  }
}
library {
  maven_library {
    groupId: "androidx.room"
    artifactId: "room-ktx"
    version: "2.2.4"
  }
  digests {
    sha256: "(#\3619.\023SF\354\332\320<\377:\262$\320P\277\335C2\227\030\364\f+6\2032R\332"
  }
}
library {
  maven_library {
    groupId: "org.jetbrains.kotlinx"
    artifactId: "kotlinx-coroutines-android"
    version: "1.3.0"
  }
  digests {
    sha256: "\310\n\252\337\004\037\004M2J\031\247?\210\207\235\375\036M\002k\024\343#\000u\377\220\201\224*\343"
  }
}
library {
  maven_library {
    groupId: "org.jetbrains.kotlinx"
    artifactId: "kotlinx-coroutines-core"
    version: "1.3.0"
  }
  digests {
    sha256: "o:`\376\242@;\2008[9\231R\256\263\244\317\t\205\244[\215\240Ko1\202Qq\220\032\035"
  }
}
library {
  maven_library {
    groupId: "com.intuit.sdp"
    artifactId: "sdp-android"
    version: "1.0.3"
  }
  digests {
    sha256: "\210\354\n\001\234\343\030\234\366\320\272D\237\353\244\243\352\027\034\023bOkk\32153m\316\332\022\257"
  }
}
library {
  maven_library {
    groupId: "com.fasterxml.jackson.core"
    artifactId: "jackson-databind"
    version: "2.9.5"
  }
  digests {
    sha256: "\017\264\340y\301\030\347R\314\224\301Z\322.g\202\260\337\305\334\t\024_H\023\3739\330.h`G"
  }
}
library {
  maven_library {
    groupId: "com.fasterxml.jackson.core"
    artifactId: "jackson-annotations"
    version: "2.9.0"
  }
  digests {
    sha256: "E\323*\306\036\370\247D\264d\305L+4\024\276W\020\026\335F\277\302\276\302&v\034\367\256Ez"
  }
}
library {
  maven_library {
    groupId: "com.fasterxml.jackson.core"
    artifactId: "jackson-core"
    version: "2.9.5"
  }
  digests {
    sha256: "\242\276\272\243%\255%E[\002\024\234g\346\005#g\247\327\374\034\347}\340\000\356\322\204\245!N\254"
  }
}
library {
  maven_library {
    groupId: "androidx.work"
    artifactId: "work-runtime-ktx"
    version: "2.3.3"
  }
  digests {
    sha256: "\203h\365\200R\221r%\000sF\304GY\264\335\345\\>G-\017\331\030\267O\031\201\350E_\300"
  }
}
library {
  maven_library {
    groupId: "androidx.work"
    artifactId: "work-runtime"
    version: "2.3.3"
  }
  digests {
    sha256: "\307S\3172c\350\231\323\0233\326;/1JMZ\221yM\240@\312\325\344\"\327\016t\035\365 "
  }
}
library {
  maven_library {
    groupId: "com.google.guava"
    artifactId: "listenablefuture"
    version: "1.0"
  }
  digests {
    sha256: "\344\255v\a\345\300G|o\211\016\362jI\313\215\033\264\337\373e\v\253E\002\257\356ddN0i"
  }
}
library {
  maven_library {
    groupId: "com.github.bumptech.glide"
    artifactId: "glide"
    version: "4.7.1"
  }
  digests {
    sha256: "\256\351\334\247Tm\036\235\310)\000m3G\326\240b\343E\321\333=\335\332\254\304\023\323\266I\320\025"
  }
}
library {
  maven_library {
    groupId: "com.github.bumptech.glide"
    artifactId: "gifdecoder"
    version: "4.7.1"
  }
  digests {
    sha256: "\233g\341\345\240\243\r\320g\"\252]\256E\327\023\375\373\032Y\004\rf\362gC\310\254\326\361As"
  }
}
library {
  maven_library {
    groupId: "com.github.bumptech.glide"
    artifactId: "disklrucache"
    version: "4.7.1"
  }
  digests {
    sha256: "\347\350HrGw\002\257\006\370\251\332\213vW\376K8V\tf\215*H\3376\305\323@P`!"
  }
}
library {
  maven_library {
    groupId: "com.github.bumptech.glide"
    artifactId: "annotations"
    version: "4.7.1"
  }
  digests {
    sha256: ")-\026\036K\f\035.\335\372\261[\001\371\320\276\006M\245\305\377\037)\315T\365\016\302\225\"\315\362"
  }
}
library {
  maven_library {
    groupId: "com.squareup.picasso"
    artifactId: "picasso"
    version: "2.71828"
  }
  digests {
    sha256: "\351\346\263,dW>\331\340\233\205\310\3372\005\260)\337\fo?\017x:\230K\263\373\374\006\350;"
  }
}
library {
  maven_library {
    groupId: "androidx.exifinterface"
    artifactId: "exifinterface"
    version: "1.0.0"
  }
  digests {
    sha256: "\356H\276\020\252\270\365N\377\364\301Kw\321\036\020\271\356\356Cy\325\357k\362\227\242\222<U\314\021"
  }
}
library {
  maven_library {
    groupId: "de.hdodenhof"
    artifactId: "circleimageview"
    version: "3.0.1"
  }
  digests {
    sha256: "{\017\b\2046\255M\313\263mw\237\320\233\362\031-\234\301\341\2474\273c7\220JvH\371v\027"
  }
}
library {
  maven_library {
    groupId: "com.github.GrenderG"
    artifactId: "Toasty"
    version: "1.4.2"
  }
  digests {
    sha256: "I07`\365:W\3357U%\277A\3139(\235\245\2563\365=*Y0\271\t\212\314\016\2768"
  }
}
library {
  maven_library {
    groupId: "com.crashlytics.sdk.android"
    artifactId: "crashlytics"
    version: "2.10.1"
  }
  digests {
    sha256: "\031Q\375~t.\307j\034\263p\207R\037\321w/\327h7\325\220\037\265\210\231\355\005gu\264,"
  }
}
library {
  maven_library {
    groupId: "com.crashlytics.sdk.android"
    artifactId: "crashlytics-core"
    version: "2.7.0"
  }
  digests {
    sha256: ":\2722\273r#A\353\364F~\024F\025\255Z\037\377\231C|\000t\n\247\257^=\006\313\004p"
  }
}
library {
  maven_library {
    groupId: "com.crashlytics.sdk.android"
    artifactId: "beta"
    version: "1.2.10"
  }
  digests {
    sha256: "*kw7N\276ahX\213\331\021l\335f\366\017|\025\033\3311\271\240\370\262\001,\006\261\315M"
  }
}
library {
  maven_library {
    groupId: "io.fabric.sdk.android"
    artifactId: "fabric"
    version: "1.4.8"
  }
  digests {
    sha256: "\262*\32570n\340c5Z\030\356\265i\245\367*\003R\363\016\f<\303j(Tf\222!\001\330"
  }
}
library {
  maven_library {
    groupId: "com.crashlytics.sdk.android"
    artifactId: "answers"
    version: "1.4.7"
  }
  digests {
    sha256: "7\n\257`\312Dp\346U\260\356\272\330Q\203\275)~\025\371\207\214\\\263!\334\301B\215\327?!"
  }
}
library {
  maven_library {
    groupId: "com.facebook.shimmer"
    artifactId: "shimmer"
    version: "0.4.0"
  }
  digests {
    sha256: "\005,\352\265\274\037j\301\335Z\020$&\366w\253\203\a\025<\316\022\360\237c\263\016j\321x\216\315"
  }
}
library {
  maven_library {
    groupId: "com.airbnb.android"
    artifactId: "lottie"
    version: "3.0.3"
  }
  digests {
    sha256: "HF\336\005\374\022\r\f\360\276$\241\264\332\352Q\330\251J,\376?h\332\206\345\234\211\216\2247X"
  }
}
library {
  maven_library {
    groupId: "androidx.multidex"
    artifactId: "multidex"
    version: "2.0.0"
  }
  digests {
    sha256: "\300\027\000\t\020r\340\377]\216\302\320\016\254k\217\226\352\030d`\200B^\234\343\306\247\265\366n3"
  }
}
library {
  maven_library {
    groupId: "com.chaos.view"
    artifactId: "pinview"
    version: "1.3.2"
  }
  digests {
    sha256: "\003\360\352\000\210\245\016\224#z-kcM\360\200\226\027\022\027\214\022\242B\244\301G6!L|\362"
  }
}
library {
  maven_library {
    groupId: "com.orhanobut"
    artifactId: "logger"
    version: "2.2.0"
  }
  digests {
    sha256: "\034E\231=9\330\231\301@U\305\261\202\327`X=\021g\333\261\n\355NL\022\362\f\037\333S\333"
  }
}
library {
  maven_library {
    groupId: "yogesh.firzen"
    artifactId: "FilesLister"
    version: "2.0.12"
  }
  digests {
    sha256: "~\211\n\217W\020\226\342\232V3\347\262S\376\255\233\341\317\353x\371\361\311\220B\b\006\306\223\277\367"
  }
}
library {
  maven_library {
    groupId: "org.jetbrains.kotlin"
    artifactId: "kotlin-stdlib-jdk8"
    version: "1.3.41"
  }
  digests {
    sha256: "\367\333\272\356>\bAu\201\207\242\023\300R8\212Na\236\021\310z\261oK\302)\317\347\316_\355"
  }
}
library {
  maven_library {
    groupId: "yogesh.firzen"
    artifactId: "MukkiyaSevaigal"
    version: "3.0.8"
  }
  digests {
    sha256: "\2636vo+\250\330\301\005g\271\212\033\\\230\314\216vE+\354\177d\272`\323\363\024\231\321\200\002"
  }
}
library {
  maven_library {
    groupId: "lib.kashif"
    artifactId: "folderpicker"
    version: "2.4"
  }
  digests {
    sha256: "\301\307\212\021\027/\200\267\366H\235%\2369G\373;2\237x\023R-\345}\370\036ZM\342\374\214"
  }
}
library {
  maven_library {
    groupId: "com.sasank.roundedhorizontalprogress"
    artifactId: "roundedhorizontalprogress"
    version: "1.0.1"
  }
  digests {
    sha256: "\fl\230\323pg\3354#4^\270w\265@/\374%\213\263\336u\255\376t\360Q@\374\242\205\202"
  }
}
library {
  maven_library {
    groupId: "com.github.santalu"
    artifactId: "mask-edittext"
    version: "1.1.1"
  }
  digests {
    sha256: "\223\3564\273\203\361\300\272G\305A#J2\223\005\323\344\356\251\020w\273\321\200\326\027qI\366H8"
  }
}
library {
  maven_library {
    groupId: "com.akexorcist"
    artifactId: "round-corner-progress-bar"
    version: "2.1.1"
  }
  digests {
    sha256: "\223.\240?\212\267\216\2556\314\362QR\276\236\"\265\311]w\246\347|\232\247\t\027\022\027p\025\365"
  }
}
library_dependencies {
}
library_dependencies {
  library_index: 1
  library_dep_index: 2
  library_dep_index: 3
  library_dep_index: 4
  library_dep_index: 0
}
library_dependencies {
  library_index: 2
  library_dep_index: 5
}
library_dependencies {
  library_index: 5
}
library_dependencies {
  library_index: 3
  library_dep_index: 6
  library_dep_index: 7
  library_dep_index: 5
}
library_dependencies {
  library_index: 6
  library_dep_index: 5
}
library_dependencies {
  library_index: 7
  library_dep_index: 5
}
library_dependencies {
  library_index: 4
  library_dep_index: 5
}
library_dependencies {
  library_index: 8
  library_dep_index: 0
  library_dep_index: 1
}
library_dependencies {
  library_index: 9
  library_dep_index: 10
}
library_dependencies {
  library_index: 10
  library_dep_index: 11
  library_dep_index: 12
}
library_dependencies {
  library_index: 11
}
library_dependencies {
  library_index: 12
}
library_dependencies {
  library_index: 13
  library_dep_index: 10
}
library_dependencies {
  library_index: 14
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 16
  library_dep_index: 17
  library_dep_index: 18
  library_dep_index: 19
  library_dep_index: 4
}
library_dependencies {
  library_index: 15
  library_dep_index: 5
  library_dep_index: 3
  library_dep_index: 20
  library_dep_index: 4
}
library_dependencies {
  library_index: 20
  library_dep_index: 5
  library_dep_index: 4
}
library_dependencies {
  library_index: 16
  library_dep_index: 5
}
library_dependencies {
  library_index: 17
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 4
  library_dep_index: 21
  library_dep_index: 22
  library_dep_index: 23
  library_dep_index: 24
  library_dep_index: 25
  library_dep_index: 26
}
library_dependencies {
  library_index: 21
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 27
}
library_dependencies {
  library_index: 27
  library_dep_index: 5
  library_dep_index: 15
}
library_dependencies {
  library_index: 22
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 28
  library_dep_index: 25
}
library_dependencies {
  library_index: 28
  library_dep_index: 29
  library_dep_index: 24
  library_dep_index: 7
}
library_dependencies {
  library_index: 29
  library_dep_index: 5
  library_dep_index: 7
}
library_dependencies {
  library_index: 24
  library_dep_index: 6
  library_dep_index: 7
  library_dep_index: 29
}
library_dependencies {
  library_index: 25
  library_dep_index: 5
}
library_dependencies {
  library_index: 23
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 3
  library_dep_index: 25
  library_dep_index: 30
  library_dep_index: 26
}
library_dependencies {
  library_index: 30
  library_dep_index: 5
  library_dep_index: 7
  library_dep_index: 6
}
library_dependencies {
  library_index: 26
  library_dep_index: 5
  library_dep_index: 30
  library_dep_index: 24
  library_dep_index: 25
}
library_dependencies {
  library_index: 18
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 31
  library_dep_index: 32
  library_dep_index: 4
}
library_dependencies {
  library_index: 31
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 4
}
library_dependencies {
  library_index: 32
  library_dep_index: 31
  library_dep_index: 33
  library_dep_index: 4
}
library_dependencies {
  library_index: 33
  library_dep_index: 5
}
library_dependencies {
  library_index: 19
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 27
}
library_dependencies {
  library_index: 34
  library_dep_index: 10
  library_dep_index: 5
  library_dep_index: 15
}
library_dependencies {
  library_index: 35
  library_dep_index: 36
}
library_dependencies {
  library_index: 36
}
library_dependencies {
  library_index: 37
  library_dep_index: 15
  library_dep_index: 38
  library_dep_index: 39
  library_dep_index: 40
  library_dep_index: 17
}
library_dependencies {
  library_index: 38
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 20
}
library_dependencies {
  library_index: 39
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 41
  library_dep_index: 22
  library_dep_index: 42
  library_dep_index: 43
}
library_dependencies {
  library_index: 41
  library_dep_index: 5
}
library_dependencies {
  library_index: 42
  library_dep_index: 5
}
library_dependencies {
  library_index: 43
  library_dep_index: 5
}
library_dependencies {
  library_index: 40
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 39
  library_dep_index: 27
  library_dep_index: 21
  library_dep_index: 44
  library_dep_index: 19
  library_dep_index: 45
  library_dep_index: 33
  library_dep_index: 46
  library_dep_index: 47
  library_dep_index: 16
}
library_dependencies {
  library_index: 44
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 27
}
library_dependencies {
  library_index: 45
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 27
}
library_dependencies {
  library_index: 46
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 33
}
library_dependencies {
  library_index: 47
  library_dep_index: 5
  library_dep_index: 15
}
library_dependencies {
  library_index: 48
  library_dep_index: 49
}
library_dependencies {
  library_index: 49
  library_dep_index: 50
}
library_dependencies {
  library_index: 50
}
library_dependencies {
  library_index: 51
  library_dep_index: 48
  library_dep_index: 52
}
library_dependencies {
  library_index: 52
}
library_dependencies {
  library_index: 53
  library_dep_index: 49
}
library_dependencies {
  library_index: 54
  library_dep_index: 48
}
library_dependencies {
  library_index: 55
  library_dep_index: 50
}
library_dependencies {
  library_index: 56
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 40
}
library_dependencies {
  library_index: 57
  library_dep_index: 3
  library_dep_index: 7
  library_dep_index: 29
  library_dep_index: 17
  library_dep_index: 6
  library_dep_index: 28
  library_dep_index: 58
  library_dep_index: 59
  library_dep_index: 25
}
library_dependencies {
  library_index: 58
  library_dep_index: 3
}
library_dependencies {
  library_index: 59
  library_dep_index: 3
}
library_dependencies {
  library_index: 60
  library_dep_index: 10
  library_dep_index: 61
  library_dep_index: 62
}
library_dependencies {
  library_index: 61
  library_dep_index: 17
  library_dep_index: 63
}
library_dependencies {
  library_index: 63
  library_dep_index: 39
  library_dep_index: 64
}
library_dependencies {
  library_index: 64
  library_dep_index: 15
  library_dep_index: 4
}
library_dependencies {
  library_index: 62
  library_dep_index: 10
  library_dep_index: 63
  library_dep_index: 65
}
library_dependencies {
  library_index: 65
  library_dep_index: 10
  library_dep_index: 64
  library_dep_index: 4
}
library_dependencies {
  library_index: 66
  library_dep_index: 10
  library_dep_index: 67
  library_dep_index: 62
}
library_dependencies {
  library_index: 67
  library_dep_index: 68
  library_dep_index: 63
}
library_dependencies {
  library_index: 68
  library_dep_index: 5
  library_dep_index: 15
  library_dep_index: 40
  library_dep_index: 39
  library_dep_index: 17
  library_dep_index: 69
  library_dep_index: 14
  library_dep_index: 70
  library_dep_index: 56
}
library_dependencies {
  library_index: 69
  library_dep_index: 5
  library_dep_index: 15
}
library_dependencies {
  library_index: 70
  library_dep_index: 5
}
library_dependencies {
  library_index: 71
  library_dep_index: 72
  library_dep_index: 73
  library_dep_index: 74
  library_dep_index: 29
}
library_dependencies {
  library_index: 72
  library_dep_index: 5
}
library_dependencies {
  library_index: 73
  library_dep_index: 5
  library_dep_index: 74
}
library_dependencies {
  library_index: 74
  library_dep_index: 5
}
library_dependencies {
  library_index: 75
  library_dep_index: 10
  library_dep_index: 76
  library_dep_index: 72
  library_dep_index: 71
}
library_dependencies {
  library_index: 76
  library_dep_index: 10
  library_dep_index: 77
}
library_dependencies {
  library_index: 77
  library_dep_index: 10
  library_dep_index: 11
}
library_dependencies {
  library_index: 78
}
library_dependencies {
  library_index: 79
  library_dep_index: 80
  library_dep_index: 81
}
library_dependencies {
  library_index: 80
}
library_dependencies {
  library_index: 81
}
library_dependencies {
  library_index: 82
  library_dep_index: 83
  library_dep_index: 10
  library_dep_index: 76
}
library_dependencies {
  library_index: 83
  library_dep_index: 84
  library_dep_index: 28
  library_dep_index: 71
  library_dep_index: 74
  library_dep_index: 73
  library_dep_index: 15
  library_dep_index: 59
}
library_dependencies {
  library_index: 84
}
library_dependencies {
  library_index: 85
  library_dep_index: 86
  library_dep_index: 87
  library_dep_index: 88
  library_dep_index: 17
}
library_dependencies {
  library_index: 86
  library_dep_index: 5
}
library_dependencies {
  library_index: 87
}
library_dependencies {
  library_index: 88
}
library_dependencies {
  library_index: 89
  library_dep_index: 49
  library_dep_index: 5
  library_dep_index: 90
}
library_dependencies {
  library_index: 90
  library_dep_index: 5
}
library_dependencies {
  library_index: 91
}
library_dependencies {
  library_index: 92
  library_dep_index: 14
}
library_dependencies {
  library_index: 93
  library_dep_index: 94
  library_dep_index: 95
  library_dep_index: 96
  library_dep_index: 97
}
library_dependencies {
  library_index: 94
  library_dep_index: 96
  library_dep_index: 97
}
library_dependencies {
  library_index: 96
}
library_dependencies {
  library_index: 97
  library_dep_index: 96
}
library_dependencies {
  library_index: 95
  library_dep_index: 96
}
library_dependencies {
  library_index: 98
  library_dep_index: 5
}
library_dependencies {
  library_index: 99
  library_dep_index: 14
}
library_dependencies {
  library_index: 100
}
library_dependencies {
  library_index: 101
  library_dep_index: 14
}
library_dependencies {
  library_index: 102
  library_dep_index: 5
}
library_dependencies {
  library_index: 103
  library_dep_index: 9
  library_dep_index: 104
  library_dep_index: 14
  library_dep_index: 68
  library_dep_index: 56
  library_dep_index: 5
  library_dep_index: 105
  library_dep_index: 35
}
library_dependencies {
  library_index: 104
  library_dep_index: 10
  library_dep_index: 13
}
library_dependencies {
  library_index: 105
  library_dep_index: 14
  library_dep_index: 37
  library_dep_index: 68
  library_dep_index: 5
  library_dep_index: 34
  library_dep_index: 104
}
library_dependencies {
  library_index: 106
}
library_dependencies {
  library_index: 107
  library_dep_index: 14
}
library_dependencies {
  library_index: 108
  library_dep_index: 13
  library_dep_index: 14
  library_dep_index: 68
}
library_dependencies {
  library_index: 109
  library_dep_index: 5
  library_dep_index: 27
}
module_dependencies {
  module_name: "base"
  dependency_index: 0
  dependency_index: 1
  dependency_index: 66
  dependency_index: 70
  dependency_index: 71
  dependency_index: 8
  dependency_index: 9
  dependency_index: 75
  dependency_index: 13
  dependency_index: 14
  dependency_index: 78
  dependency_index: 79
  dependency_index: 82
  dependency_index: 85
  dependency_index: 89
  dependency_index: 91
  dependency_index: 92
  dependency_index: 93
  dependency_index: 34
  dependency_index: 98
  dependency_index: 35
  dependency_index: 99
  dependency_index: 100
  dependency_index: 37
  dependency_index: 101
  dependency_index: 102
  dependency_index: 103
  dependency_index: 106
  dependency_index: 107
  dependency_index: 108
  dependency_index: 109
  dependency_index: 48
  dependency_index: 51
  dependency_index: 53
  dependency_index: 54
  dependency_index: 55
  dependency_index: 56
  dependency_index: 57
  dependency_index: 60
}
