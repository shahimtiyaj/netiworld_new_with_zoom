package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\fJ\r\u0010\r\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\fJ\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000f\u001a\u00020\u00102\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0011\u001a\u00020\u00102\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0012J\u0015\u0010\u0013\u001a\u00020\u00102\b\u0010\b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0012J\u0010\u0010\u0014\u001a\u00020\u00102\b\u0010\t\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0016\u0010\b\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/netizen/netiworld/model/ProductsOfferReportPostData;", "", "()V", "endDate", "", "pageLimit", "", "Ljava/lang/Integer;", "pageNo", "startDate", "getEndDate", "getPageLimit", "()Ljava/lang/Integer;", "getPageNo", "getStartDate", "setEndDate", "", "setPageLimit", "(Ljava/lang/Integer;)V", "setPageNo", "setStartDate", "app_release"})
public final class ProductsOfferReportPostData {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "startDate")
    private java.lang.String startDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "endDate")
    private java.lang.String endDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "pageLimit")
    private java.lang.Integer pageLimit;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "pageNo")
    private java.lang.Integer pageNo;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getStartDate() {
        return null;
    }
    
    public final void setStartDate(@org.jetbrains.annotations.Nullable()
    java.lang.String startDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEndDate() {
        return null;
    }
    
    public final void setEndDate(@org.jetbrains.annotations.Nullable()
    java.lang.String endDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getPageLimit() {
        return null;
    }
    
    public final void setPageLimit(@org.jetbrains.annotations.Nullable()
    java.lang.Integer pageLimit) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getPageNo() {
        return null;
    }
    
    public final void setPageNo(@org.jetbrains.annotations.Nullable()
    java.lang.Integer pageNo) {
    }
    
    public ProductsOfferReportPostData() {
        super();
    }
}