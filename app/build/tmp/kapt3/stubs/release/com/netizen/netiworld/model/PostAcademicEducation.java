package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u0002\n\u0002\b\u0012\u0018\u00002\u00020\u0001:\u000523456B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0018\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0019\u001a\u0004\u0018\u00010\bJ\b\u0010\u001a\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004J\r\u0010\u001c\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010\u001dJ\b\u0010\u001e\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001f\u001a\u0004\u0018\u00010\u0010J\r\u0010 \u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010\u001dJ\b\u0010!\u001a\u0004\u0018\u00010\u0004J\b\u0010\"\u001a\u0004\u0018\u00010\u0014J\b\u0010#\u001a\u0004\u0018\u00010\u0016J\u0010\u0010$\u001a\u00020%2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010&\u001a\u00020%2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\'\u001a\u00020%2\b\u0010\u0007\u001a\u0004\u0018\u00010\bJ\u0010\u0010(\u001a\u00020%2\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0010\u0010)\u001a\u00020%2\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\u0015\u0010*\u001a\u00020%2\b\u0010\u000b\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010+J\u0010\u0010,\u001a\u00020%2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\u0010\u0010-\u001a\u00020%2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010J\u0015\u0010.\u001a\u00020%2\b\u0010\u0011\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010+J\u0010\u0010/\u001a\u00020%2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\u0010\u00100\u001a\u00020%2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014J\u0010\u00101\u001a\u00020%2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\rR\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0011\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\rR\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0004\u0018\u00010\u00148\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0004\u0018\u00010\u00168\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00067"}, d2 = {"Lcom/netizen/netiworld/model/PostAcademicEducation;", "", "()V", "achievementDetails", "", "boardInfoDTO", "Lcom/netizen/netiworld/model/PostAcademicEducation$BoardInfoDTO;", "degreeInfoDTO", "Lcom/netizen/netiworld/model/PostAcademicEducation$DegreeInfoDTO;", "eduDuration", "eduInfoSerial", "educationInfoId", "", "Ljava/lang/Integer;", "examTitle", "gradeInfoDTO", "Lcom/netizen/netiworld/model/PostAcademicEducation$GradeInfoDTO;", "instituteForeginStatus", "instituteName", "passingYearInfoDTO", "Lcom/netizen/netiworld/model/PostAcademicEducation$PassingYearInfoDTO;", "subjectInfoDTO", "Lcom/netizen/netiworld/model/PostAcademicEducation$SubjectInfoDTO;", "getAchievementDetails", "getBoardInfoDTO", "getDegreeInfoDTO", "getEduDuration", "getEduInfoSerial", "getEducationInfoId", "()Ljava/lang/Integer;", "getExamTitle", "getGradeInfoDTO", "getInstituteForeginStatus", "getInstituteName", "getPassingYearInfoDTO", "getSubjectInfoDTO", "setAchievementDetails", "", "setBoardInfoDTO", "setDegreeInfoDTO", "setEduDuration", "setEduInfoSerial", "setEducationInfoId", "(Ljava/lang/Integer;)V", "setExamTitle", "setGradeInfoDTO", "setInstituteForeginStatus", "setInstituteName", "setPassingYearInfoDTO", "setSubjectInfoDTO", "BoardInfoDTO", "DegreeInfoDTO", "GradeInfoDTO", "PassingYearInfoDTO", "SubjectInfoDTO", "app_release"})
public final class PostAcademicEducation {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "educationInfoId")
    private java.lang.Integer educationInfoId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "degreeInfoDTO")
    private com.netizen.netiworld.model.PostAcademicEducation.DegreeInfoDTO degreeInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "subjectInfoDTO")
    private com.netizen.netiworld.model.PostAcademicEducation.SubjectInfoDTO subjectInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "gradeInfoDTO")
    private com.netizen.netiworld.model.PostAcademicEducation.GradeInfoDTO gradeInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "passingYearInfoDTO")
    private com.netizen.netiworld.model.PostAcademicEducation.PassingYearInfoDTO passingYearInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "boardInfoDTO")
    private com.netizen.netiworld.model.PostAcademicEducation.BoardInfoDTO boardInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "eduInfoSerial")
    private java.lang.String eduInfoSerial;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "examTitle")
    private java.lang.String examTitle;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "eduDuration")
    private java.lang.String eduDuration;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteName")
    private java.lang.String instituteName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteForeginStatus")
    private java.lang.Integer instituteForeginStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "achievementDetails")
    private java.lang.String achievementDetails;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getEducationInfoId() {
        return null;
    }
    
    public final void setEducationInfoId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer educationInfoId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.PostAcademicEducation.DegreeInfoDTO getDegreeInfoDTO() {
        return null;
    }
    
    public final void setDegreeInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.PostAcademicEducation.DegreeInfoDTO degreeInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.PostAcademicEducation.SubjectInfoDTO getSubjectInfoDTO() {
        return null;
    }
    
    public final void setSubjectInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.PostAcademicEducation.SubjectInfoDTO subjectInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.PostAcademicEducation.GradeInfoDTO getGradeInfoDTO() {
        return null;
    }
    
    public final void setGradeInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.PostAcademicEducation.GradeInfoDTO gradeInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.PostAcademicEducation.PassingYearInfoDTO getPassingYearInfoDTO() {
        return null;
    }
    
    public final void setPassingYearInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.PostAcademicEducation.PassingYearInfoDTO passingYearInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.PostAcademicEducation.BoardInfoDTO getBoardInfoDTO() {
        return null;
    }
    
    public final void setBoardInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.PostAcademicEducation.BoardInfoDTO boardInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEduInfoSerial() {
        return null;
    }
    
    public final void setEduInfoSerial(@org.jetbrains.annotations.Nullable()
    java.lang.String eduInfoSerial) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getExamTitle() {
        return null;
    }
    
    public final void setExamTitle(@org.jetbrains.annotations.Nullable()
    java.lang.String examTitle) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEduDuration() {
        return null;
    }
    
    public final void setEduDuration(@org.jetbrains.annotations.Nullable()
    java.lang.String eduDuration) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteName() {
        return null;
    }
    
    public final void setInstituteName(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getInstituteForeginStatus() {
        return null;
    }
    
    public final void setInstituteForeginStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer instituteForeginStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAchievementDetails() {
        return null;
    }
    
    public final void setAchievementDetails(@org.jetbrains.annotations.Nullable()
    java.lang.String achievementDetails) {
    }
    
    public PostAcademicEducation() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/netizen/netiworld/model/PostAcademicEducation$GradeInfoDTO;", "", "()V", "coreCategoryID", "", "getCoreCategoryID", "setCoreCategoryID", "", "app_release"})
    public static final class GradeInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.String coreCategoryID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.String coreCategoryID) {
        }
        
        public GradeInfoDTO() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/netizen/netiworld/model/PostAcademicEducation$PassingYearInfoDTO;", "", "()V", "coreCategoryID", "", "getCoreCategoryID", "setCoreCategoryID", "", "app_release"})
    public static final class PassingYearInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.String coreCategoryID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.String coreCategoryID) {
        }
        
        public PassingYearInfoDTO() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/netizen/netiworld/model/PostAcademicEducation$SubjectInfoDTO;", "", "()V", "coreCategoryID", "", "getCoreCategoryID", "setCoreCategoryID", "", "app_release"})
    public static final class SubjectInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.String coreCategoryID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.String coreCategoryID) {
        }
        
        public SubjectInfoDTO() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/netizen/netiworld/model/PostAcademicEducation$DegreeInfoDTO;", "", "()V", "coreCategoryID", "", "getCoreCategoryID", "setCoreCategoryID", "", "app_release"})
    public static final class DegreeInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.String coreCategoryID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.String coreCategoryID) {
        }
        
        public DegreeInfoDTO() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/netizen/netiworld/model/PostAcademicEducation$BoardInfoDTO;", "", "()V", "coreCategoryID", "", "getCoreCategoryID", "setCoreCategoryID", "", "app_release"})
    public static final class BoardInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.String coreCategoryID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.String coreCategoryID) {
        }
        
        public BoardInfoDTO() {
            super();
        }
    }
}