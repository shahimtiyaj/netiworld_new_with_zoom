package com.netizen.netiworld.view.fragment.myPoint;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0016H\u0002J\b\u0010\u0018\u001a\u00020\u0016H\u0002J\b\u0010\u0019\u001a\u00020\u0016H\u0002J\b\u0010\u001a\u001a\u00020\u0016H\u0002J\b\u0010\u001b\u001a\u00020\u0016H\u0002J\b\u0010\u001c\u001a\u00020\u0016H\u0002J\b\u0010\u001d\u001a\u00020\u0016H\u0002J\b\u0010\u001e\u001a\u00020\u0016H\u0002J\u0012\u0010\u001f\u001a\u00020\u00162\b\u0010 \u001a\u0004\u0018\u00010!H\u0016J\u0012\u0010\"\u001a\u00020\u00162\b\u0010 \u001a\u0004\u0018\u00010!H\u0016J&\u0010#\u001a\u0004\u0018\u00010$2\u0006\u0010%\u001a\u00020&2\b\u0010\'\u001a\u0004\u0018\u00010(2\b\u0010 \u001a\u0004\u0018\u00010!H\u0016J\b\u0010)\u001a\u00020\u0016H\u0002J\b\u0010*\u001a\u00020\u0016H\u0002J\b\u0010+\u001a\u00020\u0016H\u0002J\b\u0010,\u001a\u00020\u0016H\u0002J\b\u0010-\u001a\u00020\u0016H\u0002J\b\u0010.\u001a\u00020\u0016H\u0002J\b\u0010/\u001a\u00020\u0016H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00060"}, d2 = {"Lcom/netizen/netiworld/view/fragment/myPoint/MyPointFragment;", "Landroidx/fragment/app/DialogFragment;", "()V", "binding", "Lcom/netizen/netiworld/databinding/FragmentMyPointBinding;", "homeViewModel", "Lcom/netizen/netiworld/viewModel/HomeViewModel;", "isBalanceOpened", "", "isBalanceReportOpened", "isPurchaseCodeReportOpened", "isPurchaseOpened", "isPurchaseReportOpened", "isSaleOpened", "isStartUpOpened", "profileViewModel", "Lcom/netizen/netiworld/viewModel/ProfileViewModel;", "convertToBitmap", "Landroid/graphics/Bitmap;", "bytes", "", "hideBalancePoint", "", "hideBalanceReportPoint", "hidePurchaseCodeReportPoint", "hidePurchasePoint", "hidePurchaseReportPoint", "hideSalePoint", "hideStartUpPoint", "initObservers", "initViews", "onActivityCreated", "savedInstanceState", "Landroid/os/Bundle;", "onCreate", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "showBalancePoint", "showBalanceReportPoint", "showPurchaseCodeReportPoint", "showPurchasePoint", "showPurchaseReportPoint", "showSalePoint", "showStartUpPoint", "app_release"})
public final class MyPointFragment extends androidx.fragment.app.DialogFragment {
    private com.netizen.netiworld.databinding.FragmentMyPointBinding binding;
    private com.netizen.netiworld.viewModel.HomeViewModel homeViewModel;
    private com.netizen.netiworld.viewModel.ProfileViewModel profileViewModel;
    private boolean isStartUpOpened = false;
    private boolean isBalanceOpened = false;
    private boolean isPurchaseOpened = false;
    private boolean isSaleOpened = false;
    private boolean isBalanceReportOpened = false;
    private boolean isPurchaseReportOpened = false;
    private boolean isPurchaseCodeReportOpened = false;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onActivityCreated(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initViews() {
    }
    
    private final void initObservers() {
    }
    
    private final void showStartUpPoint() {
    }
    
    private final void hideStartUpPoint() {
    }
    
    private final void showBalancePoint() {
    }
    
    private final void hideBalancePoint() {
    }
    
    private final void showPurchasePoint() {
    }
    
    private final void hidePurchasePoint() {
    }
    
    private final void showSalePoint() {
    }
    
    private final void hideSalePoint() {
    }
    
    private final void showBalanceReportPoint() {
    }
    
    private final void hideBalanceReportPoint() {
    }
    
    private final void showPurchaseReportPoint() {
    }
    
    private final void hidePurchaseReportPoint() {
    }
    
    private final void showPurchaseCodeReportPoint() {
    }
    
    private final void hidePurchaseCodeReportPoint() {
    }
    
    private final android.graphics.Bitmap convertToBitmap(byte[] bytes) {
        return null;
    }
    
    public MyPointFragment() {
        super();
    }
}