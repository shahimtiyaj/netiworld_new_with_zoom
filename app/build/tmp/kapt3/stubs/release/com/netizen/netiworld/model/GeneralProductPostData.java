package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u000e\n\u0002\u0010\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001:\u0002#$B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0010\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0011J\b\u0010\u0012\u001a\u0004\u0018\u00010\u0007J\b\u0010\u0013\u001a\u0004\u0018\u00010\tJ\r\u0010\u0014\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u0015J\r\u0010\u0016\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0011J\r\u0010\u0017\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0011J\r\u0010\u0018\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0011J\u0015\u0010\u0019\u001a\u00020\u001a2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001bJ\u0010\u0010\u001c\u001a\u00020\u001a2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u001d\u001a\u00020\u001a2\b\u0010\b\u001a\u0004\u0018\u00010\tJ\u0015\u0010\u001e\u001a\u00020\u001a2\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u001fJ\u0015\u0010 \u001a\u00020\u001a2\b\u0010\r\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001bJ\u0015\u0010!\u001a\u00020\u001a2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001bJ\u0015\u0010\"\u001a\u00020\u001a2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001bR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\fR\u0016\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006%"}, d2 = {"Lcom/netizen/netiworld/model/GeneralProductPostData;", "", "()V", "paidAmount", "", "Ljava/lang/Double;", "productInfoDTO", "Lcom/netizen/netiworld/model/GeneralProductPostData$ProductInfoDTO;", "productRoleAssignDTO", "Lcom/netizen/netiworld/model/GeneralProductPostData$ProductRoleAssignDTO;", "purchaseQuantity", "", "Ljava/lang/Integer;", "totalAmount", "unitPrice", "vatAmount", "getPaidAmount", "()Ljava/lang/Double;", "getProductInfoDTO", "getProductRoleAssignDTO", "getPurchaseQuantity", "()Ljava/lang/Integer;", "getTotalAmount", "getUnitPrice", "getVatAmount", "setPaidAmount", "", "(Ljava/lang/Double;)V", "setProductInfoDTO", "setProductRoleAssignDTO", "setPurchaseQuantity", "(Ljava/lang/Integer;)V", "setTotalAmount", "setUnitPrice", "setVatAmount", "ProductInfoDTO", "ProductRoleAssignDTO", "app_release"})
public final class GeneralProductPostData {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "purchaseQuantity")
    private java.lang.Integer purchaseQuantity;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "unitPrice")
    private java.lang.Double unitPrice;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "totalAmount")
    private java.lang.Double totalAmount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "vatAmount")
    private java.lang.Double vatAmount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "paidAmount")
    private java.lang.Double paidAmount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productInfoDTO")
    private com.netizen.netiworld.model.GeneralProductPostData.ProductInfoDTO productInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productRoleAssignDTO")
    private com.netizen.netiworld.model.GeneralProductPostData.ProductRoleAssignDTO productRoleAssignDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getPurchaseQuantity() {
        return null;
    }
    
    public final void setPurchaseQuantity(@org.jetbrains.annotations.Nullable()
    java.lang.Integer purchaseQuantity) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getUnitPrice() {
        return null;
    }
    
    public final void setUnitPrice(@org.jetbrains.annotations.Nullable()
    java.lang.Double unitPrice) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getTotalAmount() {
        return null;
    }
    
    public final void setTotalAmount(@org.jetbrains.annotations.Nullable()
    java.lang.Double totalAmount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getVatAmount() {
        return null;
    }
    
    public final void setVatAmount(@org.jetbrains.annotations.Nullable()
    java.lang.Double vatAmount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getPaidAmount() {
        return null;
    }
    
    public final void setPaidAmount(@org.jetbrains.annotations.Nullable()
    java.lang.Double paidAmount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.GeneralProductPostData.ProductInfoDTO getProductInfoDTO() {
        return null;
    }
    
    public final void setProductInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.GeneralProductPostData.ProductInfoDTO productInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.GeneralProductPostData.ProductRoleAssignDTO getProductRoleAssignDTO() {
        return null;
    }
    
    public final void setProductRoleAssignDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.GeneralProductPostData.ProductRoleAssignDTO productRoleAssignDTO) {
    }
    
    public GeneralProductPostData() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0007J\u0015\u0010\b\u001a\u00020\t2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\nR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/netiworld/model/GeneralProductPostData$ProductInfoDTO;", "", "()V", "productID", "", "Ljava/lang/Integer;", "getProductID", "()Ljava/lang/Integer;", "setProductID", "", "(Ljava/lang/Integer;)V", "app_release"})
    public static final class ProductInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "productID")
        private java.lang.Integer productID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getProductID() {
            return null;
        }
        
        public final void setProductID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer productID) {
        }
        
        public ProductInfoDTO() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0007J\u0015\u0010\b\u001a\u00020\t2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\nR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/netiworld/model/GeneralProductPostData$ProductRoleAssignDTO;", "", "()V", "productRoleAssignID", "", "Ljava/lang/Integer;", "getProductRoleAssignID", "()Ljava/lang/Integer;", "setProductRoleAssignID", "", "(Ljava/lang/Integer;)V", "app_release"})
    public static final class ProductRoleAssignDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "productRoleAssignID")
        private java.lang.Integer productRoleAssignID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getProductRoleAssignID() {
            return null;
        }
        
        public final void setProductRoleAssignID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer productRoleAssignID) {
        }
        
        public ProductRoleAssignDTO() {
            super();
        }
    }
}