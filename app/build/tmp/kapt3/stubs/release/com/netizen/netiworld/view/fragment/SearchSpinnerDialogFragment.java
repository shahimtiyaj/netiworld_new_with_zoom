package com.netizen.netiworld.view.fragment;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u001d\u001a\u00020\u001eH\u0002J\b\u0010\u001f\u001a\u00020\u001eH\u0002J\b\u0010 \u001a\u00020\u001eH\u0002J\u0012\u0010!\u001a\u00020\u001e2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\u0012\u0010$\u001a\u00020\u001e2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J&\u0010%\u001a\u0004\u0018\u00010&2\u0006\u0010\'\u001a\u00020(2\b\u0010)\u001a\u0004\u0018\u00010*2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\u0016\u0010+\u001a\u00020\u001e2\f\u0010,\u001a\b\u0012\u0004\u0012\u00020\b0-H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\u0011\u001a\u0016\u0012\u0004\u0012\u00020\b\u0018\u00010\u0012j\n\u0012\u0004\u0012\u00020\b\u0018\u0001`\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0016R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006."}, d2 = {"Lcom/netizen/netiworld/view/fragment/SearchSpinnerDialogFragment;", "Landroidx/fragment/app/DialogFragment;", "()V", "bankAccountViewModel", "Lcom/netizen/netiworld/viewModel/BankAccountViewModel;", "binding", "Lcom/netizen/netiworld/databinding/FragmentSearchSpinnerBinding;", "dataType", "", "messageViewModel", "Lcom/netizen/netiworld/viewModel/MessageViewModel;", "profileViewModel", "Lcom/netizen/netiworld/viewModel/ProfileViewModel;", "purchaseViewModel", "Lcom/netizen/netiworld/viewModel/PurchaseViewModel;", "sPortalProfileViewModel", "Lcom/netizen/netiworld/viewModel/UserPoint/SPortalProfileViewModel;", "spinnerDataList", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "spinnerType", "", "Ljava/lang/Integer;", "tokenViewModel", "Lcom/netizen/netiworld/viewModel/TokenViewModel;", "userViewModel", "Lcom/netizen/netiworld/viewModel/UserViewModel;", "walletViewModel", "Lcom/netizen/netiworld/viewModel/WalletViewModel;", "initObservers", "", "initViewModels", "initViews", "onActivityCreated", "savedInstanceState", "Landroid/os/Bundle;", "onCreate", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "setSpinnerDataList", "tempList", "", "app_release"})
public final class SearchSpinnerDialogFragment extends androidx.fragment.app.DialogFragment {
    private com.netizen.netiworld.databinding.FragmentSearchSpinnerBinding binding;
    private com.netizen.netiworld.viewModel.WalletViewModel walletViewModel;
    private com.netizen.netiworld.viewModel.MessageViewModel messageViewModel;
    private com.netizen.netiworld.viewModel.PurchaseViewModel purchaseViewModel;
    private com.netizen.netiworld.viewModel.BankAccountViewModel bankAccountViewModel;
    private com.netizen.netiworld.viewModel.UserViewModel userViewModel;
    private com.netizen.netiworld.viewModel.TokenViewModel tokenViewModel;
    private com.netizen.netiworld.viewModel.ProfileViewModel profileViewModel;
    private java.util.ArrayList<java.lang.String> spinnerDataList;
    private java.lang.String dataType;
    private java.lang.Integer spinnerType;
    private com.netizen.netiworld.viewModel.UserPoint.SPortalProfileViewModel sPortalProfileViewModel;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onActivityCreated(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initViewModels() {
    }
    
    private final void initViews() {
    }
    
    private final void initObservers() {
    }
    
    private final void setSpinnerDataList(java.util.List<java.lang.String> tempList) {
    }
    
    public SearchSpinnerDialogFragment() {
        super();
    }
}