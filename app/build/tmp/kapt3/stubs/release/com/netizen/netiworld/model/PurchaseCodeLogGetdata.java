package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u0002\n\u0002\b\u0007\b\u0007\u0018\u00002\u00020\u0001:\u0003\u0017\u0018\u0019B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u0010\u0014\u001a\u00020\u00132\u0006\u0010\r\u001a\u00020\u0004J\u000e\u0010\u0015\u001a\u00020\u00132\u0006\u0010\u000b\u001a\u00020\u0004J\u000e\u0010\u0016\u001a\u00020\u00132\u0006\u0010\f\u001a\u00020\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata;", "", "()V", "coreRoleNote", "", "productPurchaseLogDTO", "Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata$ProductPurchaseLogDTO;", "getProductPurchaseLogDTO", "()Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata$ProductPurchaseLogDTO;", "setProductPurchaseLogDTO", "(Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata$ProductPurchaseLogDTO;)V", "purchaseCode", "purchaseDate", "usedDate", "getCoreRoleNote", "getProductUseDate", "getPurchaseCode", "getPurchaseDate", "setCoreRoleNote", "", "setProductUseDate", "setPurchaseCode", "setPurchaseDate", "ProductInfoDTO", "ProductPurchaseLogDTO", "ProductTypeInfoDTO", "app_release"})
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
public final class PurchaseCodeLogGetdata {
    @com.google.gson.annotations.SerializedName(value = "purchaseDate")
    private java.lang.String purchaseDate;
    @com.google.gson.annotations.SerializedName(value = "coreRoleNote")
    private java.lang.String coreRoleNote;
    @com.google.gson.annotations.SerializedName(value = "purchaseCode")
    private java.lang.String purchaseCode;
    @com.google.gson.annotations.SerializedName(value = "usedDate")
    private java.lang.String usedDate;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productPurchaseLogDTO")
    private com.netizen.netiworld.model.PurchaseCodeLogGetdata.ProductPurchaseLogDTO productPurchaseLogDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.PurchaseCodeLogGetdata.ProductPurchaseLogDTO getProductPurchaseLogDTO() {
        return null;
    }
    
    public final void setProductPurchaseLogDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.PurchaseCodeLogGetdata.ProductPurchaseLogDTO p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPurchaseDate() {
        return null;
    }
    
    public final void setPurchaseDate(@org.jetbrains.annotations.NotNull()
    java.lang.String purchaseDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCoreRoleNote() {
        return null;
    }
    
    public final void setCoreRoleNote(@org.jetbrains.annotations.NotNull()
    java.lang.String coreRoleNote) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPurchaseCode() {
        return null;
    }
    
    public final void setPurchaseCode(@org.jetbrains.annotations.NotNull()
    java.lang.String purchaseCode) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getProductUseDate() {
        return null;
    }
    
    public final void setProductUseDate(@org.jetbrains.annotations.NotNull()
    java.lang.String usedDate) {
    }
    
    public PurchaseCodeLogGetdata() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u000e\u0010\f\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\n\u00a8\u0006\u000e"}, d2 = {"Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata$ProductInfoDTO;", "", "()V", "productName", "", "productTypeInfoDTO", "Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata$ProductTypeInfoDTO;", "getProductTypeInfoDTO", "()Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata$ProductTypeInfoDTO;", "setProductTypeInfoDTO", "(Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata$ProductTypeInfoDTO;)V", "getProductName", "setProductName", "", "app_release"})
    public static final class ProductInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "productName")
        private java.lang.String productName;
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "productTypeInfoDTO")
        private com.netizen.netiworld.model.PurchaseCodeLogGetdata.ProductTypeInfoDTO productTypeInfoDTO;
        
        @org.jetbrains.annotations.Nullable()
        public final com.netizen.netiworld.model.PurchaseCodeLogGetdata.ProductTypeInfoDTO getProductTypeInfoDTO() {
            return null;
        }
        
        public final void setProductTypeInfoDTO(@org.jetbrains.annotations.Nullable()
        com.netizen.netiworld.model.PurchaseCodeLogGetdata.ProductTypeInfoDTO p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getProductName() {
            return null;
        }
        
        public final void setProductName(@org.jetbrains.annotations.NotNull()
        java.lang.String productName) {
        }
        
        public ProductInfoDTO() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata$ProductTypeInfoDTO;", "", "()V", "categoryName", "", "getProductType", "setProductType", "", "app_release"})
    public static final class ProductTypeInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryName")
        private java.lang.String categoryName;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getProductType() {
            return null;
        }
        
        public final void setProductType(@org.jetbrains.annotations.NotNull()
        java.lang.String categoryName) {
        }
        
        public ProductTypeInfoDTO() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R \u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\t"}, d2 = {"Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata$ProductPurchaseLogDTO;", "", "()V", "productInfoDTO", "Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata$ProductInfoDTO;", "getProductInfoDTO", "()Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata$ProductInfoDTO;", "setProductInfoDTO", "(Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata$ProductInfoDTO;)V", "app_release"})
    public static final class ProductPurchaseLogDTO {
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "productInfoDTO")
        private com.netizen.netiworld.model.PurchaseCodeLogGetdata.ProductInfoDTO productInfoDTO;
        
        @org.jetbrains.annotations.Nullable()
        public final com.netizen.netiworld.model.PurchaseCodeLogGetdata.ProductInfoDTO getProductInfoDTO() {
            return null;
        }
        
        public final void setProductInfoDTO(@org.jetbrains.annotations.Nullable()
        com.netizen.netiworld.model.PurchaseCodeLogGetdata.ProductInfoDTO p0) {
        }
        
        public ProductPurchaseLogDTO() {
            super();
        }
    }
}