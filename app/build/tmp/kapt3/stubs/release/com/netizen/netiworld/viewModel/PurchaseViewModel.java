package com.netizen.netiworld.viewModel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\r\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0001bB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010K\u001a\u00020L2\b\u0010M\u001a\u0004\u0018\u00010\u00072\b\u0010N\u001a\u0004\u0018\u00010\u0007J\u001a\u0010O\u001a\u00020L2\b\u0010M\u001a\u0004\u0018\u00010\u00072\b\u0010N\u001a\u0004\u0018\u00010\u0007J\u0006\u0010P\u001a\u00020LJ\u000e\u0010Q\u001a\u00020L2\u0006\u0010R\u001a\u00020\u0007J\u000e\u0010S\u001a\u00020L2\u0006\u0010&\u001a\u00020\u0007J\u000e\u0010T\u001a\u00020L2\u0006\u0010U\u001a\u00020\u0007J&\u0010V\u001a\u0012\u0012\u0004\u0012\u00020\u00070;j\b\u0012\u0004\u0012\u00020\u0007`W2\f\u0010%\u001a\b\u0012\u0004\u0012\u00020$0\u0015H\u0002J\u0015\u0010X\u001a\u00020L2\b\u0010Y\u001a\u0004\u0018\u00010Z\u00a2\u0006\u0002\u0010[J8\u0010\\\u001a\u00020L2\b\u0010B\u001a\u0004\u0018\u00010\u00072\b\u0010]\u001a\u0004\u0018\u00010\u00072\b\u0010^\u001a\u0004\u0018\u00010\u00072\b\u0010_\u001a\u0004\u0018\u00010\u00072\b\u0010)\u001a\u0004\u0018\u00010\u0007J\u000e\u0010`\u001a\u00020L2\u0006\u0010a\u001a\u00020ZR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR \u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\t\"\u0004\b\f\u0010\rR \u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\t\"\u0004\b\u0010\u0010\rR \u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\t\"\u0004\b\u0013\u0010\rR\u001d\u0010\u0014\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00150\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\tR\u0017\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00190\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\tR \u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00190\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\t\"\u0004\b\u001b\u0010\rR \u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00190\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\t\"\u0004\b\u001d\u0010\rR\u0017\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00190\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\tR \u0010\u001f\u001a\b\u0012\u0004\u0012\u00020 0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\t\"\u0004\b\"\u0010\rR\u0010\u0010#\u001a\u0004\u0018\u00010$X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010%\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020$0\u00150\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010&\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\'\u0010\t\"\u0004\b(\u0010\rR \u0010)\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\t\"\u0004\b+\u0010\rR\u001d\u0010,\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020-0\u00150\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b.\u0010\tR \u0010/\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b0\u0010\t\"\u0004\b1\u0010\rR\u001c\u00102\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u00104\"\u0004\b5\u00106R\u000e\u00107\u001a\u000208X\u0082\u0004\u00a2\u0006\u0002\n\u0000R+\u00109\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0007 <*\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010;0;0:\u00a2\u0006\b\n\u0000\u001a\u0004\b=\u0010>R \u0010?\u001a\b\u0012\u0004\u0012\u00020 0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010\t\"\u0004\bA\u0010\rR \u0010B\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bC\u0010\t\"\u0004\bD\u0010\rR\u001f\u0010E\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010F0\u00150\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\bG\u0010\tR \u0010H\u001a\b\u0012\u0004\u0012\u00020 0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bI\u0010\t\"\u0004\bJ\u0010\r\u00a8\u0006c"}, d2 = {"Lcom/netizen/netiworld/viewModel/PurchaseViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "code", "Landroidx/lifecycle/MutableLiveData;", "", "getCode", "()Landroidx/lifecycle/MutableLiveData;", "codeUsable", "getCodeUsable", "setCodeUsable", "(Landroidx/lifecycle/MutableLiveData;)V", "codeUsed", "getCodeUsed", "setCodeUsed", "discountAmtPer", "getDiscountAmtPer", "setDiscountAmtPer", "generalProductReportList", "", "Lcom/netizen/netiworld/model/GeneralProductGetData;", "getGeneralProductReportList", "isDataFound", "", "isFromDateEmpty", "setFromDateEmpty", "isToDateEmpty", "setToDateEmpty", "isUnusedPurchaseLogDataFound", "payableAmount", "", "getPayableAmount", "setPayableAmount", "product", "Lcom/netizen/netiworld/model/Products;", "productList", "productName", "getProductName", "setProductName", "productOfferID", "getProductOfferID", "setProductOfferID", "productOfferReportList", "Lcom/netizen/netiworld/model/ProductsOfferGetData;", "getProductOfferReportList", "productQuantity", "getProductQuantity", "setProductQuantity", "purchasePointId", "getPurchasePointId", "()Ljava/lang/String;", "setPurchasePointId", "(Ljava/lang/String;)V", "purchaseRepository", "Lcom/netizen/netiworld/repository/PurchaseRepository;", "tempProductList", "Landroidx/lifecycle/LiveData;", "Ljava/util/ArrayList;", "kotlin.jvm.PlatformType", "getTempProductList", "()Landroidx/lifecycle/LiveData;", "totalPrice", "getTotalPrice", "setTotalPrice", "unitPrice", "getUnitPrice", "setUnitPrice", "unusedPurchaseLogList", "Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata;", "getUnusedPurchaseLogList", "vatAmount", "getVatAmount", "setVatAmount", "checkGeneralProductReportDate", "", "fromDate", "toDate", "checkOfferProductReportDate", "checkPurchaseData", "getOfferProduct", "offerCode", "getProduct", "getProductList", "roleId", "getTempProducts", "Lkotlin/collections/ArrayList;", "getUnusedPurchaseCode", "usedStatus", "", "(Ljava/lang/Integer;)V", "submitOfferProduct", "payableAmt", "discountAmt", "totalAmount", "totalCalculation", "quantity", "PurchaseViewModelFactory", "app_release"})
public final class PurchaseViewModel extends androidx.lifecycle.AndroidViewModel {
    private final com.netizen.netiworld.repository.PurchaseRepository purchaseRepository = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.String> code = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> codeUsable;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> codeUsed;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> productQuantity;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> unitPrice;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> productName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> discountAmtPer;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> productOfferID;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Double> totalPrice;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Double> vatAmount;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Double> payableAmount;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isFromDateEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isToDateEmpty;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDataFound = null;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String purchasePointId;
    private com.netizen.netiworld.model.Products product;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.Products>> productList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.GeneralProductGetData>> generalProductReportList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.PurchaseCodeLogGetdata>> unusedPurchaseLogList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUnusedPurchaseLogDataFound = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.ProductsOfferGetData>> productOfferReportList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempProductList = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getCode() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getCodeUsable() {
        return null;
    }
    
    public final void setCodeUsable(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getCodeUsed() {
        return null;
    }
    
    public final void setCodeUsed(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getProductQuantity() {
        return null;
    }
    
    public final void setProductQuantity(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getUnitPrice() {
        return null;
    }
    
    public final void setUnitPrice(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getProductName() {
        return null;
    }
    
    public final void setProductName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getDiscountAmtPer() {
        return null;
    }
    
    public final void setDiscountAmtPer(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getProductOfferID() {
        return null;
    }
    
    public final void setProductOfferID(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Double> getTotalPrice() {
        return null;
    }
    
    public final void setTotalPrice(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Double> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Double> getVatAmount() {
        return null;
    }
    
    public final void setVatAmount(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Double> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Double> getPayableAmount() {
        return null;
    }
    
    public final void setPayableAmount(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Double> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isFromDateEmpty() {
        return null;
    }
    
    public final void setFromDateEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isToDateEmpty() {
        return null;
    }
    
    public final void setToDateEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDataFound() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPurchasePointId() {
        return null;
    }
    
    public final void setPurchasePointId(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.GeneralProductGetData>> getGeneralProductReportList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.PurchaseCodeLogGetdata>> getUnusedPurchaseLogList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUnusedPurchaseLogDataFound() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.ProductsOfferGetData>> getProductOfferReportList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempProductList() {
        return null;
    }
    
    public final void getOfferProduct(@org.jetbrains.annotations.NotNull()
    java.lang.String offerCode) {
    }
    
    public final void getProductList(@org.jetbrains.annotations.NotNull()
    java.lang.String roleId) {
    }
    
    private final java.util.ArrayList<java.lang.String> getTempProducts(java.util.List<com.netizen.netiworld.model.Products> productList) {
        return null;
    }
    
    public final void submitOfferProduct(@org.jetbrains.annotations.Nullable()
    java.lang.String unitPrice, @org.jetbrains.annotations.Nullable()
    java.lang.String payableAmt, @org.jetbrains.annotations.Nullable()
    java.lang.String discountAmt, @org.jetbrains.annotations.Nullable()
    java.lang.String totalAmount, @org.jetbrains.annotations.Nullable()
    java.lang.String productOfferID) {
    }
    
    public final void getProduct(@org.jetbrains.annotations.NotNull()
    java.lang.String productName) {
    }
    
    public final void totalCalculation(int quantity) {
    }
    
    public final void checkPurchaseData() {
    }
    
    public final void checkGeneralProductReportDate(@org.jetbrains.annotations.Nullable()
    java.lang.String fromDate, @org.jetbrains.annotations.Nullable()
    java.lang.String toDate) {
    }
    
    public final void getUnusedPurchaseCode(@org.jetbrains.annotations.Nullable()
    java.lang.Integer usedStatus) {
    }
    
    public final void checkOfferProductReportDate(@org.jetbrains.annotations.Nullable()
    java.lang.String fromDate, @org.jetbrains.annotations.Nullable()
    java.lang.String toDate) {
    }
    
    public PurchaseViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\'\u0010\u0007\u001a\u0002H\b\"\n\b\u0000\u0010\b*\u0004\u0018\u00010\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016\u00a2\u0006\u0002\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\r"}, d2 = {"Lcom/netizen/netiworld/viewModel/PurchaseViewModel$PurchaseViewModelFactory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "getApplication", "()Landroid/app/Application;", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "app_release"})
    public static final class PurchaseViewModelFactory implements androidx.lifecycle.ViewModelProvider.Factory {
        @org.jetbrains.annotations.NotNull()
        private final android.app.Application application = null;
        
        @java.lang.Override()
        public <T extends androidx.lifecycle.ViewModel>T create(@org.jetbrains.annotations.NotNull()
        java.lang.Class<T> modelClass) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.app.Application getApplication() {
            return null;
        }
        
        public PurchaseViewModelFactory(@org.jetbrains.annotations.NotNull()
        android.app.Application application) {
            super();
        }
    }
}