package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u001bB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\f\u001a\u0004\u0018\u00010\u0004J\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0012\u001a\u0004\u0018\u00010\u000bJ\u0010\u0010\u0013\u001a\u00020\u00142\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0015\u001a\u00020\u00142\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0016\u001a\u00020\u00142\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0017\u001a\u00020\u00142\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0018\u001a\u00020\u00142\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0019\u001a\u00020\u00142\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001a\u001a\u00020\u00142\b\u0010\n\u001a\u0004\u0018\u00010\u000bR\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"}, d2 = {"Lcom/netizen/netiworld/model/PostReferenceInfo;", "", "()V", "orgainizationName", "", "referenceAddress", "referenceDesignation", "referenceEmail", "referenceMobile", "referenceName", "relationInfoDTO", "Lcom/netizen/netiworld/model/PostReferenceInfo$RelationInfoDTO;", "getOrgainizationName", "getReferenceAddress", "getReferenceDesignation", "getReferenceEmail", "getReferenceMobile", "getReferenceName", "getRelationInfoDTO", "setOrgainizationName", "", "setReferenceAddress", "setReferenceDesignation", "setReferenceEmail", "setReferenceMobile", "setReferenceName", "setRelationInfoDTO", "RelationInfoDTO", "app_release"})
public final class PostReferenceInfo {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "relationInfoDTO")
    private com.netizen.netiworld.model.PostReferenceInfo.RelationInfoDTO relationInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "referenceName")
    private java.lang.String referenceName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "referenceDesignation")
    private java.lang.String referenceDesignation;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "orgainizationName")
    private java.lang.String orgainizationName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "referenceMobile")
    private java.lang.String referenceMobile;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "referenceEmail")
    private java.lang.String referenceEmail;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "referenceAddress")
    private java.lang.String referenceAddress;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.PostReferenceInfo.RelationInfoDTO getRelationInfoDTO() {
        return null;
    }
    
    public final void setRelationInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.PostReferenceInfo.RelationInfoDTO relationInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReferenceName() {
        return null;
    }
    
    public final void setReferenceName(@org.jetbrains.annotations.Nullable()
    java.lang.String referenceName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReferenceDesignation() {
        return null;
    }
    
    public final void setReferenceDesignation(@org.jetbrains.annotations.Nullable()
    java.lang.String referenceDesignation) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOrgainizationName() {
        return null;
    }
    
    public final void setOrgainizationName(@org.jetbrains.annotations.Nullable()
    java.lang.String orgainizationName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReferenceMobile() {
        return null;
    }
    
    public final void setReferenceMobile(@org.jetbrains.annotations.Nullable()
    java.lang.String referenceMobile) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReferenceEmail() {
        return null;
    }
    
    public final void setReferenceEmail(@org.jetbrains.annotations.Nullable()
    java.lang.String referenceEmail) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReferenceAddress() {
        return null;
    }
    
    public final void setReferenceAddress(@org.jetbrains.annotations.Nullable()
    java.lang.String referenceAddress) {
    }
    
    public PostReferenceInfo() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/netizen/netiworld/model/PostReferenceInfo$RelationInfoDTO;", "", "()V", "coreCategoryID", "", "getCoreCategoryID", "setCoreCategoryID", "", "app_release"})
    public static final class RelationInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.String coreCategoryID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.String coreCategoryID) {
        }
        
        public RelationInfoDTO() {
            super();
        }
    }
}