package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\f\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\rJ\b\u0010\u000e\u001a\u0004\u0018\u00010\u0007J\b\u0010\u000f\u001a\u0004\u0018\u00010\u0007J\b\u0010\u0010\u001a\u0004\u0018\u00010\u0007J\r\u0010\u0011\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\rJ\b\u0010\u0012\u001a\u0004\u0018\u00010\u0007J\u0015\u0010\u0013\u001a\u00020\u00142\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0015J\u0010\u0010\u0016\u001a\u00020\u00142\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0017\u001a\u00020\u00142\b\u0010\b\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0018\u001a\u00020\u00142\b\u0010\t\u001a\u0004\u0018\u00010\u0007J\u0015\u0010\u0019\u001a\u00020\u00142\b\u0010\n\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0015J\u0010\u0010\u001a\u001a\u00020\u00142\b\u0010\u000b\u001a\u0004\u0018\u00010\u0007R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lcom/netizen/netiworld/model/BalanceReportPostData;", "", "()V", "limit", "", "Ljava/lang/Integer;", "requestEndDate", "", "requestStartDate", "requestType", "status", "transactionType", "getLimit", "()Ljava/lang/Integer;", "getRequestEndDate", "getRequestStartDate", "getRequestType", "getStatus", "getTransactionType", "setLimit", "", "(Ljava/lang/Integer;)V", "setRequestEndDate", "setRequestStartDate", "setRequestType", "setStatus", "setTransactionType", "app_release"})
public final class BalanceReportPostData {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "requestStartDate")
    private java.lang.String requestStartDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "requestEndDate")
    private java.lang.String requestEndDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "requestType")
    private java.lang.String requestType;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "transactionType")
    private java.lang.String transactionType;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "status")
    private java.lang.Integer status;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "limit")
    private java.lang.Integer limit;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRequestStartDate() {
        return null;
    }
    
    public final void setRequestStartDate(@org.jetbrains.annotations.Nullable()
    java.lang.String requestStartDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRequestEndDate() {
        return null;
    }
    
    public final void setRequestEndDate(@org.jetbrains.annotations.Nullable()
    java.lang.String requestEndDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRequestType() {
        return null;
    }
    
    public final void setRequestType(@org.jetbrains.annotations.Nullable()
    java.lang.String requestType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTransactionType() {
        return null;
    }
    
    public final void setTransactionType(@org.jetbrains.annotations.Nullable()
    java.lang.String transactionType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getStatus() {
        return null;
    }
    
    public final void setStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer status) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getLimit() {
        return null;
    }
    
    public final void setLimit(@org.jetbrains.annotations.Nullable()
    java.lang.Integer limit) {
    }
    
    public BalanceReportPostData() {
        super();
    }
}