package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\rB\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002B%\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\bR \u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/netizen/netiworld/model/WalletTransfer;", "", "()V", "requestedAmmount", "", "receiver", "Lcom/netizen/netiworld/model/WalletTransfer$Receiver;", "requestNote", "(Ljava/lang/String;Lcom/netizen/netiworld/model/WalletTransfer$Receiver;Ljava/lang/String;)V", "getReceiver", "()Lcom/netizen/netiworld/model/WalletTransfer$Receiver;", "setReceiver", "(Lcom/netizen/netiworld/model/WalletTransfer$Receiver;)V", "Receiver", "app_release"})
public final class WalletTransfer {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "requestedAmmount")
    private java.lang.String requestedAmmount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "requestNote")
    private java.lang.String requestNote;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "receiver")
    private com.netizen.netiworld.model.WalletTransfer.Receiver receiver;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.WalletTransfer.Receiver getReceiver() {
        return null;
    }
    
    public final void setReceiver(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.WalletTransfer.Receiver p0) {
    }
    
    public WalletTransfer() {
        super();
    }
    
    public WalletTransfer(@org.jetbrains.annotations.Nullable()
    java.lang.String requestedAmmount, @org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.WalletTransfer.Receiver receiver, @org.jetbrains.annotations.Nullable()
    java.lang.String requestNote) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\"\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\b\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004\u00a8\u0006\t"}, d2 = {"Lcom/netizen/netiworld/model/WalletTransfer$Receiver;", "", "netiID", "", "(Ljava/lang/Long;)V", "getNetiID", "()Ljava/lang/Long;", "setNetiID", "Ljava/lang/Long;", "app_release"})
    public static final class Receiver {
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "netiID")
        private java.lang.Long netiID = 0L;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Long getNetiID() {
            return null;
        }
        
        public final void setNetiID(@org.jetbrains.annotations.Nullable()
        java.lang.Long p0) {
        }
        
        public Receiver(@org.jetbrains.annotations.Nullable()
        java.lang.Long netiID) {
            super();
        }
    }
}