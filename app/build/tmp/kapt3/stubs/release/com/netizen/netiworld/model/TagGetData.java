package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0002\f\rB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004J\b\u0010\b\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\t\u001a\u00020\n2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000b\u001a\u00020\n2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/netizen/netiworld/model/TagGetData;", "Ljava/io/Serializable;", "()V", "taggingTypeCoreCategoryInfoDTO", "Lcom/netizen/netiworld/model/TagGetData$TaggingTypeCoreCategoryInfoDTO;", "userBankAccountInfoDTO", "Lcom/netizen/netiworld/model/TagGetData$UserBankAccountInfoDTO;", "getTaggingTypeCoreCategoryInfoDTO", "getUserBankAccountInfoDTO", "setTaggingTypeCoreCategoryInfoDTO", "", "setUserBankAccountInfoDTO", "TaggingTypeCoreCategoryInfoDTO", "UserBankAccountInfoDTO", "app_release"})
public final class TagGetData implements java.io.Serializable {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "taggingTypeCoreCategoryInfoDTO")
    private com.netizen.netiworld.model.TagGetData.TaggingTypeCoreCategoryInfoDTO taggingTypeCoreCategoryInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userBankAccountInfoDTO")
    private com.netizen.netiworld.model.TagGetData.UserBankAccountInfoDTO userBankAccountInfoDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.TagGetData.TaggingTypeCoreCategoryInfoDTO getTaggingTypeCoreCategoryInfoDTO() {
        return null;
    }
    
    public final void setTaggingTypeCoreCategoryInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.TagGetData.TaggingTypeCoreCategoryInfoDTO taggingTypeCoreCategoryInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.TagGetData.UserBankAccountInfoDTO getUserBankAccountInfoDTO() {
        return null;
    }
    
    public final void setUserBankAccountInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.TagGetData.UserBankAccountInfoDTO userBankAccountInfoDTO) {
    }
    
    public TagGetData() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0010\u001a\u00020\u000e2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0011R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/netiworld/model/TagGetData$TaggingTypeCoreCategoryInfoDTO;", "", "()V", "categoryDefaultCode", "", "categoryName", "coreCategoryID", "", "Ljava/lang/Integer;", "getCategoryDefaultCode", "getCategoryName", "getCoreCategoryID", "()Ljava/lang/Integer;", "setCategoryDefaultCode", "", "setCategoryName", "setCoreCategoryID", "(Ljava/lang/Integer;)V", "app_release"})
    public static final class TaggingTypeCoreCategoryInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.Integer coreCategoryID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryDefaultCode")
        private java.lang.String categoryDefaultCode;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryName")
        private java.lang.String categoryName;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryDefaultCode() {
            return null;
        }
        
        public final void setCategoryDefaultCode(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryDefaultCode) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryName() {
            return null;
        }
        
        public final void setCategoryName(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryName) {
        }
        
        public TaggingTypeCoreCategoryInfoDTO() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001\u0016B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\b\u0010\f\u001a\u0004\u0018\u00010\u0004J\b\u0010\r\u001a\u0004\u0018\u00010\u0007J\r\u0010\u000e\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u00112\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0012\u001a\u00020\u00112\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0013\u001a\u00020\u00112\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0015\u0010\u0014\u001a\u00020\u00112\b\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u0015R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\n\u00a8\u0006\u0017"}, d2 = {"Lcom/netizen/netiworld/model/TagGetData$UserBankAccountInfoDTO;", "", "()V", "bankAccHolderName", "", "bankAccNumber", "coreBankBranchInfoDTO", "Lcom/netizen/netiworld/model/TagGetData$UserBankAccountInfoDTO$CoreBankBranchInfoDTO;", "userBankAccId", "", "Ljava/lang/Integer;", "getBankAccHolderName", "getBankAccNumber", "getCoreBankBranchInfoDTO", "getUserBankAccId", "()Ljava/lang/Integer;", "setBankAccHolderName", "", "setBankAccNumber", "setCoreBankBranchInfoDTO", "setUserBankAccId", "(Ljava/lang/Integer;)V", "CoreBankBranchInfoDTO", "app_release"})
    public static final class UserBankAccountInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "userBankAccId")
        private java.lang.Integer userBankAccId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "bankAccNumber")
        private java.lang.String bankAccNumber;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "bankAccHolderName")
        private java.lang.String bankAccHolderName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreBankBranchInfoDTO")
        private com.netizen.netiworld.model.TagGetData.UserBankAccountInfoDTO.CoreBankBranchInfoDTO coreBankBranchInfoDTO;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getUserBankAccId() {
            return null;
        }
        
        public final void setUserBankAccId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer userBankAccId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getBankAccNumber() {
            return null;
        }
        
        public final void setBankAccNumber(@org.jetbrains.annotations.Nullable()
        java.lang.String bankAccNumber) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getBankAccHolderName() {
            return null;
        }
        
        public final void setBankAccHolderName(@org.jetbrains.annotations.Nullable()
        java.lang.String bankAccHolderName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.netizen.netiworld.model.TagGetData.UserBankAccountInfoDTO.CoreBankBranchInfoDTO getCoreBankBranchInfoDTO() {
            return null;
        }
        
        public final void setCoreBankBranchInfoDTO(@org.jetbrains.annotations.Nullable()
        com.netizen.netiworld.model.TagGetData.UserBankAccountInfoDTO.CoreBankBranchInfoDTO coreBankBranchInfoDTO) {
        }
        
        public UserBankAccountInfoDTO() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u0019B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\f\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\rJ\b\u0010\u000e\u001a\u0004\u0018\u00010\u0007J\b\u0010\u000f\u001a\u0004\u0018\u00010\tJ\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0007J\u0015\u0010\u0012\u001a\u00020\u00132\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0014J\u0010\u0010\u0015\u001a\u00020\u00132\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0016\u001a\u00020\u00132\b\u0010\b\u001a\u0004\u0018\u00010\tJ\u0010\u0010\u0017\u001a\u00020\u00132\b\u0010\n\u001a\u0004\u0018\u00010\u0001J\u0010\u0010\u0018\u001a\u00020\u00132\b\u0010\u000b\u001a\u0004\u0018\u00010\u0007R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lcom/netizen/netiworld/model/TagGetData$UserBankAccountInfoDTO$CoreBankBranchInfoDTO;", "", "()V", "branchID", "", "Ljava/lang/Integer;", "branchName", "", "coreBankInfoDTO", "Lcom/netizen/netiworld/model/TagGetData$UserBankAccountInfoDTO$CoreBankBranchInfoDTO$CoreBankInfoDTO;", "coreDistrictInfoDTO", "routingNumber", "getBranchID", "()Ljava/lang/Integer;", "getBranchName", "getCoreBankInfoDTO", "getCoreDistrictInfoDTO", "getRoutingNumber", "setBranchID", "", "(Ljava/lang/Integer;)V", "setBranchName", "setCoreBankInfoDTO", "setCoreDistrictInfoDTO", "setRoutingNumber", "CoreBankInfoDTO", "app_release"})
        public static final class CoreBankBranchInfoDTO {
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "branchID")
            private java.lang.Integer branchID;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "branchName")
            private java.lang.String branchName;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "routingNumber")
            private java.lang.String routingNumber;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "coreBankInfoDTO")
            private com.netizen.netiworld.model.TagGetData.UserBankAccountInfoDTO.CoreBankBranchInfoDTO.CoreBankInfoDTO coreBankInfoDTO;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "coreDistrictInfoDTO")
            private java.lang.Object coreDistrictInfoDTO;
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Integer getBranchID() {
                return null;
            }
            
            public final void setBranchID(@org.jetbrains.annotations.Nullable()
            java.lang.Integer branchID) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getBranchName() {
                return null;
            }
            
            public final void setBranchName(@org.jetbrains.annotations.Nullable()
            java.lang.String branchName) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getRoutingNumber() {
                return null;
            }
            
            public final void setRoutingNumber(@org.jetbrains.annotations.Nullable()
            java.lang.String routingNumber) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final com.netizen.netiworld.model.TagGetData.UserBankAccountInfoDTO.CoreBankBranchInfoDTO.CoreBankInfoDTO getCoreBankInfoDTO() {
                return null;
            }
            
            public final void setCoreBankInfoDTO(@org.jetbrains.annotations.Nullable()
            com.netizen.netiworld.model.TagGetData.UserBankAccountInfoDTO.CoreBankBranchInfoDTO.CoreBankInfoDTO coreBankInfoDTO) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Object getCoreDistrictInfoDTO() {
                return null;
            }
            
            public final void setCoreDistrictInfoDTO(@org.jetbrains.annotations.Nullable()
            java.lang.Object coreDistrictInfoDTO) {
            }
            
            public CoreBankBranchInfoDTO() {
                super();
            }
            
            @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0010\u001a\u00020\u000e2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0011R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/netiworld/model/TagGetData$UserBankAccountInfoDTO$CoreBankBranchInfoDTO$CoreBankInfoDTO;", "", "()V", "categoryDefaultCode", "", "categoryName", "coreCategoryID", "", "Ljava/lang/Integer;", "getCategoryDefaultCode", "getCategoryName", "getCoreCategoryID", "()Ljava/lang/Integer;", "setCategoryDefaultCode", "", "setCategoryName", "setCoreCategoryID", "(Ljava/lang/Integer;)V", "app_release"})
            public static final class CoreBankInfoDTO {
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
                private java.lang.Integer coreCategoryID;
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "categoryDefaultCode")
                private java.lang.String categoryDefaultCode;
                @com.google.gson.annotations.Expose()
                @com.google.gson.annotations.SerializedName(value = "categoryName")
                private java.lang.String categoryName;
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.Integer getCoreCategoryID() {
                    return null;
                }
                
                public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
                java.lang.Integer coreCategoryID) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getCategoryDefaultCode() {
                    return null;
                }
                
                public final void setCategoryDefaultCode(@org.jetbrains.annotations.Nullable()
                java.lang.String categoryDefaultCode) {
                }
                
                @org.jetbrains.annotations.Nullable()
                public final java.lang.String getCategoryName() {
                    return null;
                }
                
                public final void setCategoryName(@org.jetbrains.annotations.Nullable()
                java.lang.String categoryName) {
                }
                
                public CoreBankInfoDTO() {
                    super();
                }
            }
        }
    }
}