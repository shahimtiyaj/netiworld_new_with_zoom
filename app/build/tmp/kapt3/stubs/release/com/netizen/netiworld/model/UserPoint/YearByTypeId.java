package com.netizen.netiworld.model.UserPoint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001:\u0001\u0014B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u0004J\b\u0010\f\u001a\u0004\u0018\u00010\u0007J\r\u0010\r\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u000eJ\u0018\u0010\u000f\u001a\u00020\u00102\u0010\u0010\u0003\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u0004J\u0010\u0010\u0011\u001a\u00020\u00102\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0015\u0010\u0012\u001a\u00020\u00102\b\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u0013R\u001c\u0010\u0003\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/YearByTypeId;", "", "()V", "item", "", "Lcom/netizen/netiworld/model/UserPoint/YearByTypeId$Item;", "message", "", "msgType", "", "Ljava/lang/Integer;", "getItem", "getMessage", "getMsgType", "()Ljava/lang/Integer;", "setItem", "", "setMessage", "setMsgType", "(Ljava/lang/Integer;)V", "Item", "app_release"})
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
public final class YearByTypeId {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "message")
    private java.lang.String message;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "msgType")
    private java.lang.Integer msgType;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "item")
    private java.util.List<com.netizen.netiworld.model.UserPoint.YearByTypeId.Item> item;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.Nullable()
    java.lang.String message) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getMsgType() {
        return null;
    }
    
    public final void setMsgType(@org.jetbrains.annotations.Nullable()
    java.lang.Integer msgType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.netizen.netiworld.model.UserPoint.YearByTypeId.Item> getItem() {
        return null;
    }
    
    public final void setItem(@org.jetbrains.annotations.Nullable()
    java.util.List<com.netizen.netiworld.model.UserPoint.YearByTypeId.Item> item) {
    }
    
    public YearByTypeId() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\fJ\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000e\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\fJ\u0010\u0010\u000f\u001a\u00020\u00102\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0011\u001a\u00020\u00102\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u00102\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0014\u001a\u00020\u00102\b\u0010\t\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0012R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007\u00a8\u0006\u0015"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/YearByTypeId$Item;", "", "()V", "defaultId", "", "id", "", "Ljava/lang/Integer;", "name", "typeId", "getDefaultId", "getId", "()Ljava/lang/Integer;", "getName", "getTypeId", "setDefaultId", "", "setId", "(Ljava/lang/Integer;)V", "setName", "setTypeId", "app_release"})
    public static final class Item {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "id")
        private java.lang.Integer id;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "name")
        private java.lang.String name;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "typeId")
        private java.lang.Integer typeId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "defaultId")
        private java.lang.String defaultId;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getId() {
            return null;
        }
        
        public final void setId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer id) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getName() {
            return null;
        }
        
        public final void setName(@org.jetbrains.annotations.Nullable()
        java.lang.String name) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getTypeId() {
            return null;
        }
        
        public final void setTypeId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer typeId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getDefaultId() {
            return null;
        }
        
        public final void setDefaultId(@org.jetbrains.annotations.Nullable()
        java.lang.String defaultId) {
        }
        
        public Item() {
            super();
        }
    }
}