package com.netizen.netiworld.viewModel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0094\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b5\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u00002\u00020\u0001:\u0002\u00a3\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J$\u0010t\u001a\u00020u2\b\u0010v\u001a\u0004\u0018\u00010\u00062\b\u0010w\u001a\u0004\u0018\u00010\u00062\b\u0010x\u001a\u0004\u0018\u00010\u0006J\u0010\u0010y\u001a\u00020\u00062\u0006\u0010z\u001a\u00020{H\u0002J$\u0010|\u001a\u00020u2\b\u0010v\u001a\u0004\u0018\u00010\u00062\b\u0010w\u001a\u0004\u0018\u00010\u00062\b\u0010x\u001a\u0004\u0018\u00010\u0006J\u0010\u0010}\u001a\u00020u2\u0006\u0010~\u001a\u00020\u007fH\u0002J\u0007\u0010\u0080\u0001\u001a\u00020uJ\u0010\u0010\u0081\u0001\u001a\u00020u2\u0007\u0010\u0082\u0001\u001a\u00020\u0006J\u0007\u0010\u0083\u0001\u001a\u00020uJ\u0012\u0010\u0084\u0001\u001a\u00020u2\t\u0010\u0085\u0001\u001a\u0004\u0018\u00010\u0006J\u001d\u0010\u0086\u0001\u001a\u00020u2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0017\u001a\u0004\u0018\u00010\u0006H\u0002J\u0010\u0010\u0087\u0001\u001a\u00020u2\u0007\u0010\u0088\u0001\u001a\u00020\u0006J\u0010\u0010\u0089\u0001\u001a\u00020u2\u0007\u0010\u008a\u0001\u001a\u00020\u0006J\u0007\u0010\u008b\u0001\u001a\u00020uJ\u001b\u0010\u008c\u0001\u001a\u00020u2\b\u0010\u008d\u0001\u001a\u00030\u008e\u00012\b\u0010\u008f\u0001\u001a\u00030\u0090\u0001J\u001b\u0010\u0091\u0001\u001a\u00020u2\b\u0010\u008d\u0001\u001a\u00030\u008e\u00012\b\u0010\u0092\u0001\u001a\u00030\u0090\u0001J!\u0010\u0093\u0001\u001a\u00020u2\n\u0010\u008d\u0001\u001a\u0005\u0018\u00010\u008e\u00012\n\u0010\u0092\u0001\u001a\u0005\u0018\u00010\u0090\u0001H\u0007J\u001f\u0010\u0094\u0001\u001a\u00020u2\b\u0010\u008d\u0001\u001a\u00030\u008e\u00012\n\u0010\u008f\u0001\u001a\u0005\u0018\u00010\u0090\u0001H\u0007J\u001d\u0010\u0095\u0001\u001a\u00020u2\b\u0010\u008d\u0001\u001a\u00030\u008e\u00012\n\u0010\u0092\u0001\u001a\u0005\u0018\u00010\u0090\u0001J\u0006\u0010Q\u001a\u00020uJ\u0007\u0010\u0096\u0001\u001a\u00020uJ\'\u0010Y\u001a\u0013\u0012\u0004\u0012\u00020\u00060Wj\t\u0012\u0004\u0012\u00020\u0006`\u0097\u00012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002J\'\u0010^\u001a\u0013\u0012\u0004\u0012\u00020\u00060Wj\t\u0012\u0004\u0012\u00020\u0006`\u0097\u00012\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\tH\u0002J\'\u0010a\u001a\u0013\u0012\u0004\u0012\u00020\u00060Wj\t\u0012\u0004\u0012\u00020\u0006`\u0097\u00012\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002J\'\u0010d\u001a\u0013\u0012\u0004\u0012\u00020\u00060Wj\t\u0012\u0004\u0012\u00020\u0006`\u0097\u00012\f\u0010S\u001a\b\u0012\u0004\u0012\u00020T0\tH\u0002J\'\u0010g\u001a\u0013\u0012\u0004\u0012\u00020\u00060Wj\t\u0012\u0004\u0012\u00020\u0006`\u0097\u00012\f\u0010r\u001a\b\u0012\u0004\u0012\u00020s0\tH\u0002J\u0010\u0010\u0098\u0001\u001a\u00020u2\u0007\u0010\u0099\u0001\u001a\u00020\u0006J\u0010\u0010\u009a\u0001\u001a\u00020u2\u0007\u0010\u0099\u0001\u001a\u00020\u0006J\u0007\u0010\u009b\u0001\u001a\u00020uJ.\u0010\u009c\u0001\u001a\u00020u2\t\u0010\u009d\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u009e\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u009f\u0001\u001a\u0004\u0018\u00010\u001a\u00a2\u0006\u0003\u0010\u00a0\u0001J\u0007\u0010\u00a1\u0001\u001a\u00020uJ%\u0010\u00a2\u0001\u001a\u00020u2\b\u0010v\u001a\u0004\u0018\u00010\u00062\b\u0010w\u001a\u0004\u0018\u00010\u00062\b\u0010x\u001a\u0004\u0018\u00010\u0006R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\t0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\u0012\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u0014\"\u0004\b\u001b\u0010\u0016R \u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0014\"\u0004\b\u001d\u0010\u0016R \u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u0014\"\u0004\b\u001f\u0010\u0016R \u0010 \u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u0014\"\u0004\b!\u0010\u0016R \u0010\"\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0014\"\u0004\b#\u0010\u0016R \u0010$\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u0014\"\u0004\b%\u0010\u0016R \u0010&\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u0014\"\u0004\b\'\u0010\u0016R \u0010(\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0014\"\u0004\b)\u0010\u0016R \u0010*\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\u0014\"\u0004\b+\u0010\u0016R \u0010,\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\u0014\"\u0004\b-\u0010\u0016R\u001e\u0010.\u001a\u0004\u0018\u00010\u001aX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u00102\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R \u00103\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u0010\u0014\"\u0004\b4\u0010\u0016R \u00105\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b5\u0010\u0014\"\u0004\b6\u0010\u0016R \u00107\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u0010\u0014\"\u0004\b8\u0010\u0016R \u00109\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b9\u0010\u0014\"\u0004\b:\u0010\u0016R \u0010;\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b;\u0010\u0014\"\u0004\b<\u0010\u0016R \u0010=\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b=\u0010\u0014\"\u0004\b>\u0010\u0016R \u0010?\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b?\u0010\u0014\"\u0004\b@\u0010\u0016R \u0010A\u001a\b\u0012\u0004\u0012\u00020\u001a0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bA\u0010\u0014\"\u0004\bB\u0010\u0016R\u001c\u0010C\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010E\"\u0004\bF\u0010GR\u0010\u0010H\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010I\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bJ\u0010\u0014\"\u0004\bK\u0010\u0016R\"\u0010L\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00060\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bM\u0010\u0014\"\u0004\bN\u0010\u0016R&\u0010O\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020P0\t0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bQ\u0010\u0014\"\u0004\bR\u0010\u0016R\u001a\u0010S\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020T0\t0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R4\u0010U\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0006 X*\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010W0W0VX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bY\u0010Z\"\u0004\b[\u0010\\R4\u0010]\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0006 X*\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010W0W0VX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b^\u0010Z\"\u0004\b_\u0010\\R4\u0010`\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0006 X*\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010W0W0VX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\ba\u0010Z\"\u0004\bb\u0010\\R4\u0010c\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0006 X*\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010W0W0VX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bd\u0010Z\"\u0004\be\u0010\\R4\u0010f\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0006 X*\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010W0W0VX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bg\u0010Z\"\u0004\bh\u0010\\R\u001c\u0010i\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bj\u0010E\"\u0004\bk\u0010GR&\u0010l\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020m0\t0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bn\u0010\u0014\"\u0004\bo\u0010\u0016R\u0010\u0010p\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010q\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020s0\t0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u00a4\u0001"}, d2 = {"Lcom/netizen/netiworld/viewModel/BankAccountViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "bankId", "", "bankList", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/netizen/netiworld/model/BankAccountInfo;", "bankRepository", "Lcom/netizen/netiworld/repository/BankRepository;", "branchId", "branchList", "Lcom/netizen/netiworld/model/BankBranchInfo;", "categoryDefaultCode", "chequeLeafImage", "chequeLeafImageName", "getChequeLeafImageName", "()Landroidx/lifecycle/MutableLiveData;", "setChequeLeafImageName", "(Landroidx/lifecycle/MutableLiveData;)V", "districtId", "districtList", "isAccountDetailsEmpty", "", "setAccountDetailsEmpty", "isAccountHolderNameEmpty", "setAccountHolderNameEmpty", "isAccountNumberEmpty", "setAccountNumberEmpty", "isAttachment", "setAttachment", "isAttachmentName", "setAttachmentName", "isBankAccountListFound", "setBankAccountListFound", "isBankEmpty", "setBankEmpty", "isBranchEmpty", "setBranchEmpty", "isBranchListFound", "setBranchListFound", "isChatboxBody", "setChatboxBody", "isChequeLeafImage", "()Ljava/lang/Boolean;", "setChequeLeafImage", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "isChequeLeafImageEmpty", "setChequeLeafImageEmpty", "isDistrictEmpty", "setDistrictEmpty", "isDistrictListFound", "setDistrictListFound", "isOtherImageEmpty", "setOtherImageEmpty", "isPartnerReferenceId", "setPartnerReferenceId", "isRoutingNumberEmpty", "setRoutingNumberEmpty", "isUsingPurposeEmpty", "setUsingPurposeEmpty", "isUsingPurposeListFound", "setUsingPurposeListFound", "netiId", "getNetiId", "()Ljava/lang/String;", "setNetiId", "(Ljava/lang/String;)V", "otherImage", "otherImageName", "getOtherImageName", "setOtherImageName", "routingNumber", "getRoutingNumber", "setRoutingNumber", "tagList", "Lcom/netizen/netiworld/model/TagGetData;", "getTagList", "setTagList", "tagTypeList", "Lcom/netizen/netiworld/model/TagType;", "tempBankList", "Landroidx/lifecycle/LiveData;", "Ljava/util/ArrayList;", "kotlin.jvm.PlatformType", "getTempBankList", "()Landroidx/lifecycle/LiveData;", "setTempBankList", "(Landroidx/lifecycle/LiveData;)V", "tempBranchList", "getTempBranchList", "setTempBranchList", "tempDistrictList", "getTempDistrictList", "setTempDistrictList", "tempTagTypeList", "getTempTagTypeList", "setTempTagTypeList", "tempUsingPurposeList", "getTempUsingPurposeList", "setTempUsingPurposeList", "userBankAccountId", "getUserBankAccountId", "setUserBankAccountId", "userBankAccountList", "Lcom/netizen/netiworld/model/BankList;", "getUserBankAccountList", "setUserBankAccountList", "usingPurposeDefaultCode", "usingPurposeId", "usingPurposeList", "Lcom/netizen/netiworld/model/UsingPurpose;", "addAndTagBankAccount", "", "accountHolderName", "accountNumber", "accountDetails", "bitMapToString", "bitmap", "Landroid/graphics/Bitmap;", "checkValidation", "encodeToBase64", "file", "Ljava/io/File;", "getBankAccountList", "getBankId", "bankName", "getBankList", "getBranchId", "branchName", "getBranchList", "getCategoryDefaultCode", "tagTypeName", "getDistrictID", "districtName", "getDistrictList", "getFileName", "context", "Landroid/content/Context;", "uri", "Landroid/net/Uri;", "getRealPathFromURI", "contentUri", "getRealPathFromURI_API11to18", "getRealPathFromURI_API19", "getRealPathFromURI_BelowAPI11", "getTagTypeList", "Lkotlin/collections/ArrayList;", "getUsingDefaultCode", "usingPurposeName", "getUsingPurposeId", "getUsingPurposeList", "onSendNetiMailClick", "partnerReferenceId", "chatboxBody", "attachmentSave", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V", "tagBankAccount", "updateBankAccount", "BankAccountViewModelFactory", "app_release"})
public final class BankAccountViewModel extends androidx.lifecycle.AndroidViewModel {
    private final com.netizen.netiworld.repository.BankRepository bankRepository = null;
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UsingPurpose>> usingPurposeList;
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankAccountInfo>> bankList;
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankAccountInfo>> districtList;
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankBranchInfo>> branchList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankList>> userBankAccountList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TagGetData>> tagList;
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TagType>> tagTypeList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempUsingPurposeList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempBankList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempDistrictList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempBranchList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempTagTypeList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUsingPurposeListFound;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBankAccountListFound;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDistrictListFound;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBranchListFound;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> routingNumber;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> chequeLeafImageName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> otherImageName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUsingPurposeEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBankEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDistrictEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBranchEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isRoutingNumberEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAccountHolderNameEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAccountNumberEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAccountDetailsEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isChequeLeafImageEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isOtherImageEmpty;
    private java.lang.String usingPurposeId;
    private java.lang.String usingPurposeDefaultCode;
    private java.lang.String bankId;
    private java.lang.String districtId;
    private java.lang.String branchId;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String userBankAccountId;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String netiId;
    private java.lang.String categoryDefaultCode;
    private java.lang.String chequeLeafImage;
    private java.lang.String otherImage;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean isChequeLeafImage;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPartnerReferenceId;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAttachment;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAttachmentName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isChatboxBody;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankList>> getUserBankAccountList() {
        return null;
    }
    
    public final void setUserBankAccountList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankList>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TagGetData>> getTagList() {
        return null;
    }
    
    public final void setTagList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TagGetData>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempUsingPurposeList() {
        return null;
    }
    
    public final void setTempUsingPurposeList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempBankList() {
        return null;
    }
    
    public final void setTempBankList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempDistrictList() {
        return null;
    }
    
    public final void setTempDistrictList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempBranchList() {
        return null;
    }
    
    public final void setTempBranchList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempTagTypeList() {
        return null;
    }
    
    public final void setTempTagTypeList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUsingPurposeListFound() {
        return null;
    }
    
    public final void setUsingPurposeListFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBankAccountListFound() {
        return null;
    }
    
    public final void setBankAccountListFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDistrictListFound() {
        return null;
    }
    
    public final void setDistrictListFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBranchListFound() {
        return null;
    }
    
    public final void setBranchListFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getRoutingNumber() {
        return null;
    }
    
    public final void setRoutingNumber(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getChequeLeafImageName() {
        return null;
    }
    
    public final void setChequeLeafImageName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getOtherImageName() {
        return null;
    }
    
    public final void setOtherImageName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUsingPurposeEmpty() {
        return null;
    }
    
    public final void setUsingPurposeEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBankEmpty() {
        return null;
    }
    
    public final void setBankEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDistrictEmpty() {
        return null;
    }
    
    public final void setDistrictEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBranchEmpty() {
        return null;
    }
    
    public final void setBranchEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isRoutingNumberEmpty() {
        return null;
    }
    
    public final void setRoutingNumberEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAccountHolderNameEmpty() {
        return null;
    }
    
    public final void setAccountHolderNameEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAccountNumberEmpty() {
        return null;
    }
    
    public final void setAccountNumberEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAccountDetailsEmpty() {
        return null;
    }
    
    public final void setAccountDetailsEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isChequeLeafImageEmpty() {
        return null;
    }
    
    public final void setChequeLeafImageEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isOtherImageEmpty() {
        return null;
    }
    
    public final void setOtherImageEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUserBankAccountId() {
        return null;
    }
    
    public final void setUserBankAccountId(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getNetiId() {
        return null;
    }
    
    public final void setNetiId(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean isChequeLeafImage() {
        return null;
    }
    
    public final void setChequeLeafImage(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPartnerReferenceId() {
        return null;
    }
    
    public final void setPartnerReferenceId(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAttachment() {
        return null;
    }
    
    public final void setAttachment(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAttachmentName() {
        return null;
    }
    
    public final void setAttachmentName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isChatboxBody() {
        return null;
    }
    
    public final void setChatboxBody(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    public final void getUsingPurposeList() {
    }
    
    public final void getBankAccountList() {
    }
    
    public final void getDistrictList() {
    }
    
    private final void getBranchList(java.lang.String bankId, java.lang.String districtId) {
    }
    
    public final void getBankList() {
    }
    
    public final void getTagList() {
    }
    
    public final void getTagTypeList() {
    }
    
    private final java.util.ArrayList<java.lang.String> getTempUsingPurposeList(java.util.List<com.netizen.netiworld.model.UsingPurpose> usingPurposeList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempBankList(java.util.List<com.netizen.netiworld.model.BankAccountInfo> bankList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempDistrictList(java.util.List<com.netizen.netiworld.model.BankAccountInfo> districtList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempBranchList(java.util.List<com.netizen.netiworld.model.BankBranchInfo> branchList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempTagTypeList(java.util.List<com.netizen.netiworld.model.TagType> tagTypeList) {
        return null;
    }
    
    public final void getUsingPurposeId(@org.jetbrains.annotations.NotNull()
    java.lang.String usingPurposeName) {
    }
    
    public final void getUsingDefaultCode(@org.jetbrains.annotations.NotNull()
    java.lang.String usingPurposeName) {
    }
    
    public final void getBankId(@org.jetbrains.annotations.NotNull()
    java.lang.String bankName) {
    }
    
    public final void getDistrictID(@org.jetbrains.annotations.NotNull()
    java.lang.String districtName) {
    }
    
    public final void getBranchId(@org.jetbrains.annotations.Nullable()
    java.lang.String branchName) {
    }
    
    public final void getCategoryDefaultCode(@org.jetbrains.annotations.NotNull()
    java.lang.String tagTypeName) {
    }
    
    public final void tagBankAccount() {
    }
    
    private final java.lang.String bitMapToString(android.graphics.Bitmap bitmap) {
        return null;
    }
    
    public final void getFileName(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.net.Uri uri) {
    }
    
    public final void getRealPathFromURI(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.net.Uri contentUri) {
    }
    
    private final void encodeToBase64(java.io.File file) {
    }
    
    @android.annotation.SuppressLint(value = {"NewApi"})
    public final void getRealPathFromURI_API19(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.net.Uri uri) {
    }
    
    @android.annotation.SuppressLint(value = {"NewApi"})
    public final void getRealPathFromURI_API11to18(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.net.Uri contentUri) {
    }
    
    public final void getRealPathFromURI_BelowAPI11(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.net.Uri contentUri) {
    }
    
    public final void checkValidation(@org.jetbrains.annotations.Nullable()
    java.lang.String accountHolderName, @org.jetbrains.annotations.Nullable()
    java.lang.String accountNumber, @org.jetbrains.annotations.Nullable()
    java.lang.String accountDetails) {
    }
    
    public final void addAndTagBankAccount(@org.jetbrains.annotations.Nullable()
    java.lang.String accountHolderName, @org.jetbrains.annotations.Nullable()
    java.lang.String accountNumber, @org.jetbrains.annotations.Nullable()
    java.lang.String accountDetails) {
    }
    
    public final void updateBankAccount(@org.jetbrains.annotations.Nullable()
    java.lang.String accountHolderName, @org.jetbrains.annotations.Nullable()
    java.lang.String accountNumber, @org.jetbrains.annotations.Nullable()
    java.lang.String accountDetails) {
    }
    
    public final void onSendNetiMailClick(@org.jetbrains.annotations.Nullable()
    java.lang.String partnerReferenceId, @org.jetbrains.annotations.Nullable()
    java.lang.String chatboxBody, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean attachmentSave) {
    }
    
    public BankAccountViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
    
    @kotlin.Suppress(names = {"UNCHECKED_CAST"})
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\'\u0010\u0005\u001a\u0002H\u0006\"\n\b\u0000\u0010\u0006*\u0004\u0018\u00010\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016\u00a2\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/netiworld/viewModel/BankAccountViewModel$BankAccountViewModelFactory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "app_release"})
    public static final class BankAccountViewModelFactory implements androidx.lifecycle.ViewModelProvider.Factory {
        private final android.app.Application application = null;
        
        @java.lang.Override()
        public <T extends androidx.lifecycle.ViewModel>T create(@org.jetbrains.annotations.NotNull()
        java.lang.Class<T> modelClass) {
            return null;
        }
        
        public BankAccountViewModelFactory(@org.jetbrains.annotations.NotNull()
        android.app.Application application) {
            super();
        }
    }
}