package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\n\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u000bJ\b\u0010\f\u001a\u0004\u0018\u00010\u0007J\b\u0010\r\u001a\u0004\u0018\u00010\u0007J\b\u0010\u000e\u001a\u0004\u0018\u00010\u0007J\u0015\u0010\u000f\u001a\u00020\u00102\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0011J\u0010\u0010\u0012\u001a\u00020\u00102\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0013\u001a\u00020\u00102\b\u0010\b\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0014\u001a\u00020\u00102\b\u0010\t\u001a\u0004\u0018\u00010\u0007R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/netizen/netiworld/model/BankBranchInfo;", "", "()V", "branchID", "", "Ljava/lang/Integer;", "branchName", "", "detailsNote", "routingNumber", "getBranchID", "()Ljava/lang/Integer;", "getBranchName", "getDetailsNote", "getRoutingNumber", "setBranchID", "", "(Ljava/lang/Integer;)V", "setBranchName", "setDetailsNote", "setRoutingNumber", "app_release"})
public final class BankBranchInfo {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "branchID")
    private java.lang.Integer branchID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "branchName")
    private java.lang.String branchName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "routingNumber")
    private java.lang.String routingNumber;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "detailsNote")
    private java.lang.String detailsNote;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getBranchID() {
        return null;
    }
    
    public final void setBranchID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer branchID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBranchName() {
        return null;
    }
    
    public final void setBranchName(@org.jetbrains.annotations.Nullable()
    java.lang.String branchName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRoutingNumber() {
        return null;
    }
    
    public final void setRoutingNumber(@org.jetbrains.annotations.Nullable()
    java.lang.String routingNumber) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDetailsNote() {
        return null;
    }
    
    public final void setDetailsNote(@org.jetbrains.annotations.Nullable()
    java.lang.String detailsNote) {
    }
    
    public BankBranchInfo() {
        super();
    }
}