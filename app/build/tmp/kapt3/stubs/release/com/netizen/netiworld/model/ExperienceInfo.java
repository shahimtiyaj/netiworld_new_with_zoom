package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0012\n\u0002\u0010\u0002\n\u0002\b\u000f\u0018\u00002\u00020\u0001:\u00013B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0018\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0019\u001a\u0004\u0018\u00010\bJ\b\u0010\u001a\u001a\u0004\u0018\u00010\u0004J\r\u0010\u001b\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u001cJ\r\u0010\u001d\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u001cJ\r\u0010\u001e\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0002\u0010\u001fJ\b\u0010 \u001a\u0004\u0018\u00010\u0012J\b\u0010!\u001a\u0004\u0018\u00010\u0004J\b\u0010\"\u001a\u0004\u0018\u00010\u0012J\b\u0010#\u001a\u0004\u0018\u00010\u0004J\u0010\u0010$\u001a\u00020%2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010&\u001a\u00020%2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\'\u001a\u00020%2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0010\u0010(\u001a\u00020%2\b\u0010\u0007\u001a\u0004\u0018\u00010\bJ\u0010\u0010)\u001a\u00020%2\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0015\u0010*\u001a\u00020%2\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010+J\u0015\u0010,\u001a\u00020%2\b\u0010\r\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010+J\u0015\u0010-\u001a\u00020%2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\u0002\u0010.J\u0010\u0010/\u001a\u00020%2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012J\u0010\u00100\u001a\u00020%2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\u0010\u00101\u001a\u00020%2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0012J\u0010\u00102\u001a\u00020%2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\fR\u0016\u0010\r\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\fR\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u000f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0010R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00128\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0004\u0018\u00010\u00128\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00064"}, d2 = {"Lcom/netizen/netiworld/model/ExperienceInfo;", "Ljava/io/Serializable;", "()V", "companyBusiness", "", "companyLocation", "companyName", "countryInfoDTO", "Lcom/netizen/netiworld/model/ExperienceInfo$CountryInfoDTO;", "designationName", "employmentEnd", "", "Ljava/lang/Long;", "employmentStart", "experienceId", "", "Ljava/lang/Integer;", "experienceSerial", "", "responsibilityDetails", "userBasicInfoDTO", "workingDepartment", "getCompanyBusiness", "getCompanyLocation", "getCompanyName", "getCountryInfoDTO", "getDesignationName", "getEmploymentEnd", "()Ljava/lang/Long;", "getEmploymentStart", "getExperienceId", "()Ljava/lang/Integer;", "getExperienceSerial", "getResponsibilityDetails", "getUserBasicInfoDTO", "getWorkingDepartment", "setCompanyBusiness", "", "setCompanyLocation", "setCompanyName", "setCountryInfoDTO", "setDesignationName", "setEmploymentEnd", "(Ljava/lang/Long;)V", "setEmploymentStart", "setExperienceId", "(Ljava/lang/Integer;)V", "setExperienceSerial", "setResponsibilityDetails", "setUserBasicInfoDTO", "setWorkingDepartment", "CountryInfoDTO", "app_release"})
public final class ExperienceInfo implements java.io.Serializable {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "experienceId")
    private java.lang.Integer experienceId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "experienceSerial")
    private java.lang.Object experienceSerial;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "companyName")
    private java.lang.String companyName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "companyBusiness")
    private java.lang.String companyBusiness;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "designationName")
    private java.lang.String designationName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "workingDepartment")
    private java.lang.String workingDepartment;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "companyLocation")
    private java.lang.String companyLocation;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "employmentStart")
    private java.lang.Long employmentStart;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "employmentEnd")
    private java.lang.Long employmentEnd;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "responsibilityDetails")
    private java.lang.String responsibilityDetails;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userBasicInfoDTO")
    private java.lang.Object userBasicInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "countryInfoDTO")
    private com.netizen.netiworld.model.ExperienceInfo.CountryInfoDTO countryInfoDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getExperienceId() {
        return null;
    }
    
    public final void setExperienceId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer experienceId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getExperienceSerial() {
        return null;
    }
    
    public final void setExperienceSerial(@org.jetbrains.annotations.Nullable()
    java.lang.Object experienceSerial) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCompanyName() {
        return null;
    }
    
    public final void setCompanyName(@org.jetbrains.annotations.Nullable()
    java.lang.String companyName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCompanyBusiness() {
        return null;
    }
    
    public final void setCompanyBusiness(@org.jetbrains.annotations.Nullable()
    java.lang.String companyBusiness) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDesignationName() {
        return null;
    }
    
    public final void setDesignationName(@org.jetbrains.annotations.Nullable()
    java.lang.String designationName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getWorkingDepartment() {
        return null;
    }
    
    public final void setWorkingDepartment(@org.jetbrains.annotations.Nullable()
    java.lang.String workingDepartment) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCompanyLocation() {
        return null;
    }
    
    public final void setCompanyLocation(@org.jetbrains.annotations.Nullable()
    java.lang.String companyLocation) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getEmploymentStart() {
        return null;
    }
    
    public final void setEmploymentStart(@org.jetbrains.annotations.Nullable()
    java.lang.Long employmentStart) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getEmploymentEnd() {
        return null;
    }
    
    public final void setEmploymentEnd(@org.jetbrains.annotations.Nullable()
    java.lang.Long employmentEnd) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getResponsibilityDetails() {
        return null;
    }
    
    public final void setResponsibilityDetails(@org.jetbrains.annotations.Nullable()
    java.lang.String responsibilityDetails) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getUserBasicInfoDTO() {
        return null;
    }
    
    public final void setUserBasicInfoDTO(@org.jetbrains.annotations.Nullable()
    java.lang.Object userBasicInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.ExperienceInfo.CountryInfoDTO getCountryInfoDTO() {
        return null;
    }
    
    public final void setCountryInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.ExperienceInfo.CountryInfoDTO countryInfoDTO) {
    }
    
    public ExperienceInfo() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0010\u001a\u00020\u000e2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0011R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/netiworld/model/ExperienceInfo$CountryInfoDTO;", "", "()V", "categoryDefaultCode", "", "categoryName", "coreCategoryID", "", "Ljava/lang/Integer;", "getCategoryDefaultCode", "getCategoryName", "getCoreCategoryID", "()Ljava/lang/Integer;", "setCategoryDefaultCode", "", "setCategoryName", "setCoreCategoryID", "(Ljava/lang/Integer;)V", "app_release"})
    public static final class CountryInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.Integer coreCategoryID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryDefaultCode")
        private java.lang.String categoryDefaultCode;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryName")
        private java.lang.String categoryName;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryDefaultCode() {
            return null;
        }
        
        public final void setCategoryDefaultCode(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryDefaultCode) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryName() {
            return null;
        }
        
        public final void setCategoryName(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryName) {
        }
        
        public CountryInfoDTO() {
            super();
        }
    }
}