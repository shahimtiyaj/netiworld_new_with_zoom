package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\u0002\n\u0002\b\r\u0018\u00002\u00020\u0001:\u0001+B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0015\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u0016J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001J\b\u0010\u0018\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0019\u001a\u0004\u0018\u00010\rJ\b\u0010\u001a\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001J\u0010\u0010\u001e\u001a\u00020\u001f2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010 \u001a\u00020\u001f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010!\u001a\u00020\u001f2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\"\u001a\u00020\u001f2\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010#J\u0010\u0010$\u001a\u00020\u001f2\b\u0010\n\u001a\u0004\u0018\u00010\u0001J\u0010\u0010%\u001a\u00020\u001f2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010&\u001a\u00020\u001f2\b\u0010\f\u001a\u0004\u0018\u00010\rJ\u0010\u0010\'\u001a\u00020\u001f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\u0010\u0010(\u001a\u00020\u001f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\u0010\u0010)\u001a\u00020\u001f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\u0010\u0010*\u001a\u00020\u001f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\n\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\r8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"}, d2 = {"Lcom/netizen/netiworld/model/UpdateCertificationInfo;", "", "()V", "achieveDate", "", "achieveDateEdit", "achieveDateView", "certificateInfoId", "", "Ljava/lang/Integer;", "certificateSerial", "certificationName", "countryInfoDTO", "Lcom/netizen/netiworld/model/UpdateCertificationInfo$CountryInfoDTO;", "courseDuration", "instituteLocation", "instituteName", "userBasicInfoDTO", "getAchieveDate", "getAchieveDateEdit", "getAchieveDateView", "getCertificateInfoId", "()Ljava/lang/Integer;", "getCertificateSerial", "getCertificationName", "getCountryInfoDTO", "getCourseDuration", "getInstituteLocation", "getInstituteName", "getUserBasicInfoDTO", "setAchieveDate", "", "setAchieveDateEdit", "setAchieveDateView", "setCertificateInfoId", "(Ljava/lang/Integer;)V", "setCertificateSerial", "setCertificationName", "setCountryInfoDTO", "setCourseDuration", "setInstituteLocation", "setInstituteName", "setUserBasicInfoDTO", "CountryInfoDTO", "app_release"})
public final class UpdateCertificationInfo {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "certificateInfoId")
    private java.lang.Integer certificateInfoId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "certificateSerial")
    private java.lang.Object certificateSerial;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "certificationName")
    private java.lang.String certificationName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteName")
    private java.lang.String instituteName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteLocation")
    private java.lang.String instituteLocation;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "achieveDate")
    private java.lang.String achieveDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "courseDuration")
    private java.lang.String courseDuration;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userBasicInfoDTO")
    private java.lang.Object userBasicInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "countryInfoDTO")
    private com.netizen.netiworld.model.UpdateCertificationInfo.CountryInfoDTO countryInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "achieveDateView")
    private java.lang.String achieveDateView;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "achieveDateEdit")
    private java.lang.String achieveDateEdit;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCertificateInfoId() {
        return null;
    }
    
    public final void setCertificateInfoId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer certificateInfoId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getCertificateSerial() {
        return null;
    }
    
    public final void setCertificateSerial(@org.jetbrains.annotations.Nullable()
    java.lang.Object certificateSerial) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCertificationName() {
        return null;
    }
    
    public final void setCertificationName(@org.jetbrains.annotations.Nullable()
    java.lang.String certificationName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteName() {
        return null;
    }
    
    public final void setInstituteName(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteLocation() {
        return null;
    }
    
    public final void setInstituteLocation(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteLocation) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAchieveDate() {
        return null;
    }
    
    public final void setAchieveDate(@org.jetbrains.annotations.Nullable()
    java.lang.String achieveDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCourseDuration() {
        return null;
    }
    
    public final void setCourseDuration(@org.jetbrains.annotations.Nullable()
    java.lang.String courseDuration) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getUserBasicInfoDTO() {
        return null;
    }
    
    public final void setUserBasicInfoDTO(@org.jetbrains.annotations.Nullable()
    java.lang.Object userBasicInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.UpdateCertificationInfo.CountryInfoDTO getCountryInfoDTO() {
        return null;
    }
    
    public final void setCountryInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.UpdateCertificationInfo.CountryInfoDTO countryInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAchieveDateView() {
        return null;
    }
    
    public final void setAchieveDateView(@org.jetbrains.annotations.Nullable()
    java.lang.String achieveDateView) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAchieveDateEdit() {
        return null;
    }
    
    public final void setAchieveDateEdit(@org.jetbrains.annotations.Nullable()
    java.lang.String achieveDateEdit) {
    }
    
    public UpdateCertificationInfo() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u001b\n\u0002\u0010\u0002\n\u0002\b\u000e\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0014\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0018\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\r\u0010\u0019\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\r\u0010\u001a\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001J\r\u0010\u001e\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001J\r\u0010 \u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\u0010\u0010!\u001a\u00020\"2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0015\u0010#\u001a\u00020\"2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0010\u0010%\u001a\u00020\"2\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010&\u001a\u00020\"2\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\'\u001a\u00020\"2\b\u0010\n\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0015\u0010(\u001a\u00020\"2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0015\u0010)\u001a\u00020\"2\b\u0010\f\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0010\u0010*\u001a\u00020\"2\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\u0010\u0010+\u001a\u00020\"2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\u0010\u0010,\u001a\u00020\"2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001J\u0015\u0010-\u001a\u00020\"2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0010\u0010.\u001a\u00020\"2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001J\u0015\u0010/\u001a\u00020\"2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0016\u0010\f\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007\u00a8\u00060"}, d2 = {"Lcom/netizen/netiworld/model/UpdateCertificationInfo$CountryInfoDTO;", "", "()V", "categoryDefaultCode", "", "categoryEnableStatus", "", "Ljava/lang/Integer;", "categoryName", "categoryNote", "categorySerial", "coreCategoryID", "lastDateExecuted", "lastIpExecuted", "lastUserExecuted", "parentCoreCategoryInfoDTO", "parentStatus", "parentTypeInfoDTO", "typeStatus", "getCategoryDefaultCode", "getCategoryEnableStatus", "()Ljava/lang/Integer;", "getCategoryName", "getCategoryNote", "getCategorySerial", "getCoreCategoryID", "getLastDateExecuted", "getLastIpExecuted", "getLastUserExecuted", "getParentCoreCategoryInfoDTO", "getParentStatus", "getParentTypeInfoDTO", "getTypeStatus", "setCategoryDefaultCode", "", "setCategoryEnableStatus", "(Ljava/lang/Integer;)V", "setCategoryName", "setCategoryNote", "setCategorySerial", "setCoreCategoryID", "setLastDateExecuted", "setLastIpExecuted", "setLastUserExecuted", "setParentCoreCategoryInfoDTO", "setParentStatus", "setParentTypeInfoDTO", "setTypeStatus", "app_release"})
    public static final class CountryInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.Integer coreCategoryID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryDefaultCode")
        private java.lang.String categoryDefaultCode;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryName")
        private java.lang.String categoryName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryNote")
        private java.lang.String categoryNote;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryEnableStatus")
        private java.lang.Integer categoryEnableStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categorySerial")
        private java.lang.Integer categorySerial;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "typeStatus")
        private java.lang.Integer typeStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "parentStatus")
        private java.lang.Integer parentStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "parentTypeInfoDTO")
        private java.lang.Object parentTypeInfoDTO;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "parentCoreCategoryInfoDTO")
        private java.lang.Object parentCoreCategoryInfoDTO;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastUserExecuted")
        private java.lang.String lastUserExecuted;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastIpExecuted")
        private java.lang.String lastIpExecuted;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastDateExecuted")
        private java.lang.Integer lastDateExecuted;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryDefaultCode() {
            return null;
        }
        
        public final void setCategoryDefaultCode(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryDefaultCode) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryName() {
            return null;
        }
        
        public final void setCategoryName(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryNote() {
            return null;
        }
        
        public final void setCategoryNote(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryNote) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCategoryEnableStatus() {
            return null;
        }
        
        public final void setCategoryEnableStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer categoryEnableStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCategorySerial() {
            return null;
        }
        
        public final void setCategorySerial(@org.jetbrains.annotations.Nullable()
        java.lang.Integer categorySerial) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getTypeStatus() {
            return null;
        }
        
        public final void setTypeStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer typeStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getParentStatus() {
            return null;
        }
        
        public final void setParentStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer parentStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getParentTypeInfoDTO() {
            return null;
        }
        
        public final void setParentTypeInfoDTO(@org.jetbrains.annotations.Nullable()
        java.lang.Object parentTypeInfoDTO) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getParentCoreCategoryInfoDTO() {
            return null;
        }
        
        public final void setParentCoreCategoryInfoDTO(@org.jetbrains.annotations.Nullable()
        java.lang.Object parentCoreCategoryInfoDTO) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLastUserExecuted() {
            return null;
        }
        
        public final void setLastUserExecuted(@org.jetbrains.annotations.Nullable()
        java.lang.String lastUserExecuted) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLastIpExecuted() {
            return null;
        }
        
        public final void setLastIpExecuted(@org.jetbrains.annotations.Nullable()
        java.lang.String lastIpExecuted) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getLastDateExecuted() {
            return null;
        }
        
        public final void setLastDateExecuted(@org.jetbrains.annotations.Nullable()
        java.lang.Integer lastDateExecuted) {
        }
        
        public CountryInfoDTO() {
            super();
        }
    }
}