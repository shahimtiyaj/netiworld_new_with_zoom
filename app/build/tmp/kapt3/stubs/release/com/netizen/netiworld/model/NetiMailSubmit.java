package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u001aB\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002B9\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u000bJ\b\u0010\r\u001a\u0004\u0018\u00010\u0006J\r\u0010\u000e\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u000fJ\b\u0010\u0010\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0013\u001a\u00020\u00142\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006J\u0015\u0010\u0015\u001a\u00020\u00142\b\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u0016J\u0010\u0010\u0017\u001a\u00020\u00142\b\u0010\n\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u0018\u001a\u00020\u00142\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u0019\u001a\u00020\u00142\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\fR\u0014\u0010\n\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lcom/netizen/netiworld/model/NetiMailSubmit;", "", "()V", "receiverBasicInfoDTO", "Lcom/netizen/netiworld/model/NetiMailSubmit$ReceiverBasicInfoDTO;", "chatboxBody", "", "attachContent", "attachSaveOrEditable", "", "chatboxAttachmentName", "(Lcom/netizen/netiworld/model/NetiMailSubmit$ReceiverBasicInfoDTO;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V", "Ljava/lang/Boolean;", "getAttachContent", "getAttachSaveOrEditable", "()Ljava/lang/Boolean;", "getChatboxAttachmentName", "getChatboxBody", "getReceiverBasicInfoDTO", "setAttachContent", "", "setAttachSaveOrEditable", "(Ljava/lang/Boolean;)V", "setChatboxAttachmentName", "setChatboxBody", "setReceiverBasicInfoDTO", "ReceiverBasicInfoDTO", "app_release"})
public final class NetiMailSubmit {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "receiverBasicInfoDTO")
    private com.netizen.netiworld.model.NetiMailSubmit.ReceiverBasicInfoDTO receiverBasicInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "chatboxBody")
    private java.lang.String chatboxBody;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "attachContent")
    private java.lang.String attachContent;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "attachSaveOrEditable")
    private java.lang.Boolean attachSaveOrEditable;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "chatboxAttachmentName")
    private java.lang.String chatboxAttachmentName;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.NetiMailSubmit.ReceiverBasicInfoDTO getReceiverBasicInfoDTO() {
        return null;
    }
    
    public final void setReceiverBasicInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.NetiMailSubmit.ReceiverBasicInfoDTO receiverBasicInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getChatboxBody() {
        return null;
    }
    
    public final void setChatboxBody(@org.jetbrains.annotations.Nullable()
    java.lang.String chatboxBody) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAttachContent() {
        return null;
    }
    
    public final void setAttachContent(@org.jetbrains.annotations.Nullable()
    java.lang.String attachContent) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getAttachSaveOrEditable() {
        return null;
    }
    
    public final void setAttachSaveOrEditable(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean attachSaveOrEditable) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getChatboxAttachmentName() {
        return null;
    }
    
    public final void setChatboxAttachmentName(@org.jetbrains.annotations.Nullable()
    java.lang.String chatboxAttachmentName) {
    }
    
    public NetiMailSubmit() {
        super();
    }
    
    public NetiMailSubmit(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.NetiMailSubmit.ReceiverBasicInfoDTO receiverBasicInfoDTO, @org.jetbrains.annotations.Nullable()
    java.lang.String chatboxBody, @org.jetbrains.annotations.Nullable()
    java.lang.String attachContent, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean attachSaveOrEditable, @org.jetbrains.annotations.Nullable()
    java.lang.String chatboxAttachmentName) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002B\u0011\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0005J\r\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\bJ\u0015\u0010\t\u001a\u00020\n2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0005R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/netiworld/model/NetiMailSubmit$ReceiverBasicInfoDTO;", "", "()V", "netiID", "", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getNetiID", "()Ljava/lang/Integer;", "setNetiID", "", "app_release"})
    public static final class ReceiverBasicInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "netiID")
        private java.lang.Integer netiID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getNetiID() {
            return null;
        }
        
        public final void setNetiID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer netiID) {
        }
        
        public ReceiverBasicInfoDTO() {
            super();
        }
        
        public ReceiverBasicInfoDTO(@org.jetbrains.annotations.Nullable()
        java.lang.Integer netiID) {
            super();
        }
    }
}