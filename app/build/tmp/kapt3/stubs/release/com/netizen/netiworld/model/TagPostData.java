package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0002\u0010\u0011B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\b\u0010\n\u001a\u0004\u0018\u00010\u0006J\b\u0010\u000b\u001a\u0004\u0018\u00010\bJ\u0010\u0010\f\u001a\u00020\r2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000e\u001a\u00020\r2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u000f\u001a\u00020\r2\b\u0010\u0007\u001a\u0004\u0018\u00010\bR\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/netiworld/model/TagPostData;", "", "()V", "taggingTypeDefaultCode", "", "userBankAccountTaggingDTO", "Lcom/netizen/netiworld/model/TagPostData$UserBankAccountTaggingDTO;", "userBasicInfoDTO", "Lcom/netizen/netiworld/model/TagPostData$UserBasicInfoDTO;", "getTaggingTypeDefaultCode", "getUserBankAccountTaggingDTO", "getUserBasicInfoDTO", "setTaggingTypeDefaultCode", "", "setUserBankAccountTaggingDTO", "setUserBasicInfoDTO", "UserBankAccountTaggingDTO", "UserBasicInfoDTO", "app_release"})
public final class TagPostData {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userBasicInfoDTO")
    private com.netizen.netiworld.model.TagPostData.UserBasicInfoDTO userBasicInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userBankAccountTaggingDTO")
    private com.netizen.netiworld.model.TagPostData.UserBankAccountTaggingDTO userBankAccountTaggingDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "taggingTypeDefaultCode")
    private java.lang.String taggingTypeDefaultCode;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.TagPostData.UserBasicInfoDTO getUserBasicInfoDTO() {
        return null;
    }
    
    public final void setUserBasicInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.TagPostData.UserBasicInfoDTO userBasicInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.TagPostData.UserBankAccountTaggingDTO getUserBankAccountTaggingDTO() {
        return null;
    }
    
    public final void setUserBankAccountTaggingDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.TagPostData.UserBankAccountTaggingDTO userBankAccountTaggingDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTaggingTypeDefaultCode() {
        return null;
    }
    
    public final void setTaggingTypeDefaultCode(@org.jetbrains.annotations.Nullable()
    java.lang.String taggingTypeDefaultCode) {
    }
    
    public TagPostData() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\bB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/netizen/netiworld/model/TagPostData$UserBankAccountTaggingDTO;", "", "()V", "userBankAccountInfoDTO", "Lcom/netizen/netiworld/model/TagPostData$UserBankAccountTaggingDTO$UserBankAccountInfoDTO;", "getUserBankAccountInfoDTO", "setUserBankAccountInfoDTO", "", "UserBankAccountInfoDTO", "app_release"})
    public static final class UserBankAccountTaggingDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "userBankAccountInfoDTO")
        private com.netizen.netiworld.model.TagPostData.UserBankAccountTaggingDTO.UserBankAccountInfoDTO userBankAccountInfoDTO;
        
        @org.jetbrains.annotations.Nullable()
        public final com.netizen.netiworld.model.TagPostData.UserBankAccountTaggingDTO.UserBankAccountInfoDTO getUserBankAccountInfoDTO() {
            return null;
        }
        
        public final void setUserBankAccountInfoDTO(@org.jetbrains.annotations.Nullable()
        com.netizen.netiworld.model.TagPostData.UserBankAccountTaggingDTO.UserBankAccountInfoDTO userBankAccountInfoDTO) {
        }
        
        public UserBankAccountTaggingDTO() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0007J\u0015\u0010\b\u001a\u00020\t2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\nR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/netiworld/model/TagPostData$UserBankAccountTaggingDTO$UserBankAccountInfoDTO;", "", "()V", "userBankAccId", "", "Ljava/lang/Integer;", "getUserBankAccId", "()Ljava/lang/Integer;", "setUserBankAccId", "", "(Ljava/lang/Integer;)V", "app_release"})
        public static final class UserBankAccountInfoDTO {
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "userBankAccId")
            private java.lang.Integer userBankAccId;
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Integer getUserBankAccId() {
                return null;
            }
            
            public final void setUserBankAccId(@org.jetbrains.annotations.Nullable()
            java.lang.Integer userBankAccId) {
            }
            
            public UserBankAccountInfoDTO() {
                super();
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0007J\u0015\u0010\b\u001a\u00020\t2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\nR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/netiworld/model/TagPostData$UserBasicInfoDTO;", "", "()V", "netiID", "", "Ljava/lang/Integer;", "getNetiID", "()Ljava/lang/Integer;", "setNetiID", "", "(Ljava/lang/Integer;)V", "app_release"})
    public static final class UserBasicInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "netiID")
        private java.lang.Integer netiID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getNetiID() {
            return null;
        }
        
        public final void setNetiID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer netiID) {
        }
        
        public UserBasicInfoDTO() {
            super();
        }
    }
}