package com.netizen.netiworld.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u001e\u001a\u00020\u001f2\b\u0010 \u001a\u0004\u0018\u00010!J\u000e\u0010\u0013\u001a\u00020\u001f2\u0006\u0010\"\u001a\u00020\u0007J\u0006\u0010\u0016\u001a\u00020\u001fJ\u000e\u0010#\u001a\u00020\u001f2\u0006\u0010$\u001a\u00020\u0007J\u000e\u0010%\u001a\u00020\u001f2\u0006\u0010&\u001a\u00020\'J\u0018\u0010(\u001a\u00020\u001f2\u0006\u0010)\u001a\u00020\u00072\b\u0010*\u001a\u0004\u0018\u00010\u001cJ\u000e\u0010+\u001a\u00020\u001f2\u0006\u0010,\u001a\u00020\u0007J\u000e\u0010-\u001a\u00020\u001f2\u0006\u0010.\u001a\u00020/J\u0010\u0010 \u001a\u00020\u001f2\b\u0010 \u001a\u0004\u0018\u00010!R \u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u000e\u0010\f\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u001d\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\tR\u001d\u0010\u0014\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00150\u00110\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\tR \u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00180\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\t\"\u0004\b\u0019\u0010\u000bR\u0017\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00180\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\tR\u0017\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\t\u00a8\u00060"}, d2 = {"Lcom/netizen/netiworld/repository/UserRepository;", "", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "accessToken", "Landroidx/lifecycle/MutableLiveData;", "", "getAccessToken", "()Landroidx/lifecycle/MutableLiveData;", "setAccessToken", "(Landroidx/lifecycle/MutableLiveData;)V", "appPreferences", "Lcom/netizen/netiworld/utils/AppPreferences;", "getApplication", "()Landroid/app/Application;", "areaList", "", "Lcom/netizen/netiworld/model/AreaModel;", "getAreaList", "districtList", "Lcom/netizen/netiworld/model/DistrictModel;", "getDistrictList", "isListFound", "", "setListFound", "isLoggedIn", "userInfoListForgotPass", "Lcom/netizen/netiworld/model/UserCheckForgotPass;", "getUserInfoListForgotPass", "changePassword", "", "resetPassword", "Lcom/netizen/netiworld/model/ResetPassword;", "coreCategoryId", "getUserInfoForgotPassword", "userName", "loginAuthentication", "loginUser", "Lcom/netizen/netiworld/model/LoginUser;", "otpGetForgotPassword", "userContactNo", "userCheckForgotPass", "otpVerifyGetForgotPassword", "code", "registrationUser", "registrationPost", "Lcom/netizen/netiworld/model/RegistrationPost;", "app_release"})
public final class UserRepository {
    private final com.netizen.netiworld.utils.AppPreferences appPreferences = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isLoggedIn = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> accessToken;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DistrictModel>> districtList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.AreaModel>> areaList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.UserCheckForgotPass> userInfoListForgotPass = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isListFound;
    @org.jetbrains.annotations.NotNull()
    private final android.app.Application application = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isLoggedIn() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getAccessToken() {
        return null;
    }
    
    public final void setAccessToken(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DistrictModel>> getDistrictList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.AreaModel>> getAreaList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.UserCheckForgotPass> getUserInfoListForgotPass() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isListFound() {
        return null;
    }
    
    public final void setListFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    public final void loginAuthentication(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.LoginUser loginUser) {
    }
    
    public final void registrationUser(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.RegistrationPost registrationPost) {
    }
    
    public final void getDistrictList() {
    }
    
    public final void getAreaList(@org.jetbrains.annotations.NotNull()
    java.lang.String coreCategoryId) {
    }
    
    public final void getUserInfoForgotPassword(@org.jetbrains.annotations.NotNull()
    java.lang.String userName) {
    }
    
    public final void otpGetForgotPassword(@org.jetbrains.annotations.NotNull()
    java.lang.String userContactNo, @org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.UserCheckForgotPass userCheckForgotPass) {
    }
    
    public final void otpVerifyGetForgotPassword(@org.jetbrains.annotations.NotNull()
    java.lang.String code) {
    }
    
    public final void resetPassword(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.ResetPassword resetPassword) {
    }
    
    public final void changePassword(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.ResetPassword resetPassword) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.app.Application getApplication() {
        return null;
    }
    
    public UserRepository(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super();
    }
}