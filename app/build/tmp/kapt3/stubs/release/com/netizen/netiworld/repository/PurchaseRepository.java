package com.netizen.netiworld.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u00108\u001a\u0002092\u0006\u0010:\u001a\u00020\u000b2\u0006\u0010;\u001a\u00020\u000bJ\u000e\u0010<\u001a\u0002092\u0006\u0010=\u001a\u00020\u000bJ\u000e\u0010>\u001a\u0002092\u0006\u0010?\u001a\u00020@J\u0010\u0010A\u001a\u0002092\b\u0010B\u001a\u0004\u0018\u00010\u000bJ\u0015\u0010C\u001a\u0002092\b\u0010D\u001a\u0004\u0018\u00010E\u00a2\u0006\u0002\u0010FJ\u000e\u0010G\u001a\u0002092\u0006\u0010H\u001a\u00020IJ8\u0010J\u001a\u0002092\b\u00101\u001a\u0004\u0018\u00010\u000b2\b\u0010K\u001a\u0004\u0018\u00010\u000b2\b\u0010L\u001a\u0004\u0018\u00010\u000b2\b\u0010M\u001a\u0004\u0018\u00010\u000b2\b\u0010(\u001a\u0004\u0018\u00010\u000bR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR \u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR \u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\r\"\u0004\b\u0012\u0010\u000fR \u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\r\"\u0004\b\u0015\u0010\u000fR \u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\r\"\u0004\b\u0018\u0010\u000fR\u001d\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001b0\u001a0\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\rR \u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001e0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\r\"\u0004\b\u001f\u0010\u000fR \u0010 \u001a\b\u0012\u0004\u0012\u00020\u001e0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\r\"\u0004\b!\u0010\u000fR\u001d\u0010\"\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020#0\u001a0\n\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\rR \u0010%\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\r\"\u0004\b\'\u0010\u000fR \u0010(\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010\r\"\u0004\b*\u0010\u000fR\u001d\u0010+\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020,0\u001a0\n\u00a2\u0006\b\n\u0000\u001a\u0004\b-\u0010\rR \u0010.\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u0010\r\"\u0004\b0\u0010\u000fR \u00101\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u0010\r\"\u0004\b3\u0010\u000fR(\u00104\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u0001050\u001a0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b6\u0010\r\"\u0004\b7\u0010\u000f\u00a8\u0006N"}, d2 = {"Lcom/netizen/netiworld/repository/PurchaseRepository;", "", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "appPreferences", "Lcom/netizen/netiworld/utils/AppPreferences;", "getApplication", "()Landroid/app/Application;", "code", "Landroidx/lifecycle/MutableLiveData;", "", "getCode", "()Landroidx/lifecycle/MutableLiveData;", "setCode", "(Landroidx/lifecycle/MutableLiveData;)V", "codeUsable", "getCodeUsable", "setCodeUsable", "codeUsed", "getCodeUsed", "setCodeUsed", "discountAmtPer", "getDiscountAmtPer", "setDiscountAmtPer", "generalProductReportList", "", "Lcom/netizen/netiworld/model/GeneralProductGetData;", "getGeneralProductReportList", "isDataFound", "", "setDataFound", "isUnusedPurchaseLogDataFound", "setUnusedPurchaseLogDataFound", "productList", "Lcom/netizen/netiworld/model/Products;", "getProductList", "productName", "getProductName", "setProductName", "productOfferID", "getProductOfferID", "setProductOfferID", "productOfferReportList", "Lcom/netizen/netiworld/model/ProductsOfferGetData;", "getProductOfferReportList", "productQnty", "getProductQnty", "setProductQnty", "unitPrice", "getUnitPrice", "setUnitPrice", "unusedPurchaseLogList", "Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata;", "getUnusedPurchaseLogList", "setUnusedPurchaseLogList", "getGeneralProductReportData", "", "fromDate", "toDate", "getOfferProductData", "offerCode", "getProductProductReportData", "productsOfferReportPostData", "Lcom/netizen/netiworld/model/ProductsOfferReportPostData;", "getProductsByRole", "roleId", "getPurchaseUnusedData", "usedStatus", "", "(Ljava/lang/Integer;)V", "submitGeneralProductPostData", "generalProductPostData", "Lcom/netizen/netiworld/model/GeneralProductPostData;", "submitOfferProductData", "payableAmt", "discountAmt", "totalAmount", "app_release"})
public final class PurchaseRepository {
    private final com.netizen.netiworld.utils.AppPreferences appPreferences = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> code;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> codeUsable;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> codeUsed;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> productQnty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> unitPrice;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> productName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> discountAmtPer;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> productOfferID;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDataFound;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.Products>> productList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.GeneralProductGetData>> generalProductReportList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.ProductsOfferGetData>> productOfferReportList = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.PurchaseCodeLogGetdata>> unusedPurchaseLogList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUnusedPurchaseLogDataFound;
    @org.jetbrains.annotations.NotNull()
    private final android.app.Application application = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getCode() {
        return null;
    }
    
    public final void setCode(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getCodeUsable() {
        return null;
    }
    
    public final void setCodeUsable(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getCodeUsed() {
        return null;
    }
    
    public final void setCodeUsed(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getProductQnty() {
        return null;
    }
    
    public final void setProductQnty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getUnitPrice() {
        return null;
    }
    
    public final void setUnitPrice(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getProductName() {
        return null;
    }
    
    public final void setProductName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getDiscountAmtPer() {
        return null;
    }
    
    public final void setDiscountAmtPer(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getProductOfferID() {
        return null;
    }
    
    public final void setProductOfferID(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDataFound() {
        return null;
    }
    
    public final void setDataFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.Products>> getProductList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.GeneralProductGetData>> getGeneralProductReportList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.ProductsOfferGetData>> getProductOfferReportList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.PurchaseCodeLogGetdata>> getUnusedPurchaseLogList() {
        return null;
    }
    
    public final void setUnusedPurchaseLogList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.PurchaseCodeLogGetdata>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUnusedPurchaseLogDataFound() {
        return null;
    }
    
    public final void setUnusedPurchaseLogDataFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    public final void getOfferProductData(@org.jetbrains.annotations.NotNull()
    java.lang.String offerCode) {
    }
    
    public final void submitOfferProductData(@org.jetbrains.annotations.Nullable()
    java.lang.String unitPrice, @org.jetbrains.annotations.Nullable()
    java.lang.String payableAmt, @org.jetbrains.annotations.Nullable()
    java.lang.String discountAmt, @org.jetbrains.annotations.Nullable()
    java.lang.String totalAmount, @org.jetbrains.annotations.Nullable()
    java.lang.String productOfferID) {
    }
    
    public final void getProductsByRole(@org.jetbrains.annotations.Nullable()
    java.lang.String roleId) {
    }
    
    public final void submitGeneralProductPostData(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.GeneralProductPostData generalProductPostData) {
    }
    
    public final void getGeneralProductReportData(@org.jetbrains.annotations.NotNull()
    java.lang.String fromDate, @org.jetbrains.annotations.NotNull()
    java.lang.String toDate) {
    }
    
    public final void getPurchaseUnusedData(@org.jetbrains.annotations.Nullable()
    java.lang.Integer usedStatus) {
    }
    
    public final void getProductProductReportData(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.ProductsOfferReportPostData productsOfferReportPostData) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.app.Application getApplication() {
        return null;
    }
    
    public PurchaseRepository(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super();
    }
}