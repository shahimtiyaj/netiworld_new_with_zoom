package com.netizen.netiworld.viewModel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001OB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u00108\u001a\u000209J\u0010\u0010:\u001a\u0002092\u0006\u0010;\u001a\u00020<H\u0002J\u0016\u0010=\u001a\u0002092\u0006\u0010>\u001a\u00020?2\u0006\u0010@\u001a\u00020AJ\u000e\u0010B\u001a\u0002092\u0006\u0010C\u001a\u00020\u0006J\u0006\u0010D\u001a\u000209J\u000e\u0010E\u001a\u0002092\u0006\u0010F\u001a\u00020\u0006J\u0016\u0010G\u001a\u0002092\u0006\u0010>\u001a\u00020?2\u0006\u0010H\u001a\u00020AJ\u0006\u0010)\u001a\u000209J&\u0010.\u001a\u0012\u0012\u0004\u0012\u00020\u00060,j\b\u0012\u0004\u0012\u00020\u0006`I2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020%0$H\u0002J&\u00101\u001a\u0012\u0012\u0004\u0012\u00020\u00060,j\b\u0012\u0004\u0012\u00020\u0006`I2\f\u0010&\u001a\b\u0012\u0004\u0012\u00020%0$H\u0002J\u0006\u00105\u001a\u000209J\u0011\u0010J\u001a\u000209H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010KJ\u001a\u0010L\u001a\u0002092\b\u0010M\u001a\u0004\u0018\u00010\u00062\b\u0010N\u001a\u0004\u0018\u00010\u0006R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R \u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u0010\u0010\r\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000e\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00060\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\n\"\u0004\b\u0016\u0010\fR \u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00150\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\n\"\u0004\b\u0018\u0010\fR\u0017\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00150\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\nR \u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00150\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\n\"\u0004\b\u001b\u0010\fR \u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00150\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\n\"\u0004\b\u001d\u0010\fR\u0017\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00150\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\nR \u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00150\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\n\"\u0004\b \u0010\fR\u0017\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00150\b\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\nR\u0010\u0010\"\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010#\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020%0$0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010&\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020%0$0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\'\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020(0$0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010\nR+\u0010*\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0006 -*\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010,0,0+\u00a2\u0006\b\n\u0000\u001a\u0004\b.\u0010/R+\u00100\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0006 -*\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010,0,0+\u00a2\u0006\b\n\u0000\u001a\u0004\b1\u0010/R\u0017\u00102\u001a\b\u0012\u0004\u0012\u00020(0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b3\u0010\nR\u001d\u00104\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020(0$0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b5\u0010\nR\u000e\u00106\u001a\u000207X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006P"}, d2 = {"Lcom/netizen/netiworld/viewModel/TokenViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "attachmentBase64", "", "attachmentName", "Landroidx/lifecycle/MutableLiveData;", "getAttachmentName", "()Landroidx/lifecycle/MutableLiveData;", "setAttachmentName", "(Landroidx/lifecycle/MutableLiveData;)V", "coreCategoryId", "downloadImageUrl", "getDownloadImageUrl", "()Ljava/lang/String;", "setDownloadImageUrl", "(Ljava/lang/String;)V", "downloadedAttachment", "isAttachmentEmpty", "", "setAttachmentEmpty", "isContactNumberEmpty", "setContactNumberEmpty", "isDownloading", "isProblemDetailsEmpty", "setProblemDetailsEmpty", "isProblemModuleEmpty", "setProblemModuleEmpty", "isProblemModuleListFound", "isProblemTypeEmpty", "setProblemTypeEmpty", "isProblemTypeListFound", "parentCategoryId", "problemModuleList", "", "Lcom/netizen/netiworld/model/Problem;", "problemTypeList", "solvedAndPendingTokenList", "Lcom/netizen/netiworld/model/TokenList;", "getSolvedAndPendingTokenList", "tempProblemModuleList", "Landroidx/lifecycle/LiveData;", "Ljava/util/ArrayList;", "kotlin.jvm.PlatformType", "getTempProblemModuleList", "()Landroidx/lifecycle/LiveData;", "tempProblemTypeList", "getTempProblemTypeList", "token", "getToken", "tokenList", "getTokenList", "tokenRepository", "Lcom/netizen/netiworld/repository/TokenRepository;", "downloadImage", "", "encodeToBase64", "file", "Ljava/io/File;", "getFileName", "context", "Landroid/content/Context;", "uri", "Landroid/net/Uri;", "getProblemModuleId", "problemModuleName", "getProblemModuleList", "getProblemTypeId", "problemTypeName", "getRealPathFromURI", "contentUri", "Lkotlin/collections/ArrayList;", "saveImage", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "validationSubmitToken", "details", "contactNumber", "TokenViewModelFactory", "app_release"})
public final class TokenViewModel extends androidx.lifecycle.AndroidViewModel {
    private final com.netizen.netiworld.repository.TokenRepository tokenRepository = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TokenList>> solvedAndPendingTokenList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.Problem>> problemModuleList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.Problem>> problemTypeList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TokenList>> tokenList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.TokenList> token = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProblemModuleListFound = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProblemTypeListFound = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDownloading = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.String> downloadedAttachment = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempProblemModuleList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempProblemTypeList = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> attachmentName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProblemModuleEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProblemTypeEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProblemDetailsEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isContactNumberEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAttachmentEmpty;
    private java.lang.String parentCategoryId;
    private java.lang.String coreCategoryId;
    private java.lang.String attachmentBase64;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String downloadImageUrl;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TokenList>> getSolvedAndPendingTokenList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TokenList>> getTokenList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.TokenList> getToken() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProblemModuleListFound() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProblemTypeListFound() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDownloading() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempProblemModuleList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempProblemTypeList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getAttachmentName() {
        return null;
    }
    
    public final void setAttachmentName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProblemModuleEmpty() {
        return null;
    }
    
    public final void setProblemModuleEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProblemTypeEmpty() {
        return null;
    }
    
    public final void setProblemTypeEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isProblemDetailsEmpty() {
        return null;
    }
    
    public final void setProblemDetailsEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isContactNumberEmpty() {
        return null;
    }
    
    public final void setContactNumberEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAttachmentEmpty() {
        return null;
    }
    
    public final void setAttachmentEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDownloadImageUrl() {
        return null;
    }
    
    public final void setDownloadImageUrl(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    public final void getSolvedAndPendingTokenList() {
    }
    
    public final void getProblemModuleList() {
    }
    
    public final void getTokenList() {
    }
    
    private final java.util.ArrayList<java.lang.String> getTempProblemModuleList(java.util.List<com.netizen.netiworld.model.Problem> problemModuleList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempProblemTypeList(java.util.List<com.netizen.netiworld.model.Problem> problemTypeList) {
        return null;
    }
    
    public final void getProblemModuleId(@org.jetbrains.annotations.NotNull()
    java.lang.String problemModuleName) {
    }
    
    public final void getProblemTypeId(@org.jetbrains.annotations.NotNull()
    java.lang.String problemTypeName) {
    }
    
    public final void getFileName(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.net.Uri uri) {
    }
    
    public final void getRealPathFromURI(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.net.Uri contentUri) {
    }
    
    private final void encodeToBase64(java.io.File file) {
    }
    
    public final void validationSubmitToken(@org.jetbrains.annotations.Nullable()
    java.lang.String details, @org.jetbrains.annotations.Nullable()
    java.lang.String contactNumber) {
    }
    
    public final void downloadImage() {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object saveImage(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p0) {
        return null;
    }
    
    public TokenViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
    
    @kotlin.Suppress(names = {"UNCHECKED_CAST"})
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\'\u0010\u0005\u001a\u0002H\u0006\"\n\b\u0000\u0010\u0006*\u0004\u0018\u00010\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016\u00a2\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/netiworld/viewModel/TokenViewModel$TokenViewModelFactory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "app_release"})
    public static final class TokenViewModelFactory implements androidx.lifecycle.ViewModelProvider.Factory {
        private final android.app.Application application = null;
        
        @java.lang.Override()
        public <T extends androidx.lifecycle.ViewModel>T create(@org.jetbrains.annotations.NotNull()
        java.lang.Class<T> modelClass) {
            return null;
        }
        
        public TokenViewModelFactory(@org.jetbrains.annotations.NotNull()
        android.app.Application application) {
            super();
        }
    }
}