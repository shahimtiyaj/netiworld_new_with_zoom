package com.netizen.netiworld.viewModel.UserPoint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u00017B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\'\u001a\u00020(J\u0006\u0010)\u001a\u00020(J\u0006\u0010*\u001a\u00020(J\u0006\u0010+\u001a\u00020(J\u0006\u0010,\u001a\u00020(J\u001e\u0010-\u001a\u00020(2\u0006\u0010.\u001a\u00020\b2\u0006\u0010/\u001a\u00020\b2\u0006\u00100\u001a\u00020\bJ&\u00101\u001a\u0012\u0012\u0004\u0012\u00020\b0\u001cj\b\u0012\u0004\u0012\u00020\b`22\f\u00103\u001a\b\u0012\u0004\u0012\u00020\f0\u0007H\u0002J&\u00104\u001a\u0012\u0012\u0004\u0012\u00020\b0\u001cj\b\u0012\u0004\u0012\u00020\b`22\f\u00105\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0007H\u0002J\u0006\u00106\u001a\u00020(R\u001d\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\nR\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u0014\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00150\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\nR\u001d\u0010\u0017\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\nR4\u0010\u001a\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\b \u001d*\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u001c0\u001c0\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R4\u0010\"\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\b \u001d*\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u001c0\u001c0\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u001f\"\u0004\b$\u0010!R\u001d\u0010%\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\n\u00a8\u00068"}, d2 = {"Lcom/netizen/netiworld/viewModel/UserPoint/SPortalProfileViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "monthsList", "Landroidx/lifecycle/MutableLiveData;", "", "", "getMonthsList", "()Landroidx/lifecycle/MutableLiveData;", "sPortalAttendancePeriod", "Lcom/netizen/netiworld/model/UserPoint/StudentPortalPeriod$Item;", "sPortalAttendanceYear", "Lcom/netizen/netiworld/model/UserPoint/YearByTypeId$Item;", "sPortalProfileInfoLiveData", "Lcom/netizen/netiworld/model/UserPoint/StudentPortalProfile;", "getSPortalProfileInfoLiveData", "sPortalProfileRepository", "Lcom/netizen/netiworld/repository/UserPoint/SPortalProfileRepository;", "sProfileSubjectInfoAList", "Lcom/netizen/netiworld/model/UserPoint/StudentPortalSubject$Item;", "getSProfileSubjectInfoAList", "studentPortalUnpaidFeesInfo", "Lcom/netizen/netiworld/model/UserPoint/UnpaidFees$Item;", "getStudentPortalUnpaidFeesInfo", "tempAttendancePeriodList", "Landroidx/lifecycle/LiveData;", "Ljava/util/ArrayList;", "kotlin.jvm.PlatformType", "getTempAttendancePeriodList", "()Landroidx/lifecycle/LiveData;", "setTempAttendancePeriodList", "(Landroidx/lifecycle/LiveData;)V", "tempAttendanceYearList", "getTempAttendanceYearList", "setTempAttendanceYearList", "unpaidFeesList", "getUnpaidFeesList", "getSPortalProfileInfo", "", "getSPortalSubjectInfo", "getStudentPortalAtdPeriodData", "getStudentPortalAtdYearData", "getStudentPortalMonthData", "getStudentPortalUnpaidData", "instituteId", "studentId", "academicYear", "getTempAttendancePeriod", "Lkotlin/collections/ArrayList;", "periodList", "getTempAttendanceYear", "yearList", "spinnerStudentPortalUnpaidFees", "SPortalProfileViewModelFactory", "app_release"})
public final class SPortalProfileViewModel extends androidx.lifecycle.AndroidViewModel {
    private final com.netizen.netiworld.repository.UserPoint.SPortalProfileRepository sPortalProfileRepository = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.UserPoint.StudentPortalProfile> sPortalProfileInfoLiveData = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalSubject.Item>> sProfileSubjectInfoAList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalPeriod.Item>> sPortalAttendancePeriod = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.YearByTypeId.Item>> sPortalAttendanceYear = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.UnpaidFees.Item>> studentPortalUnpaidFeesInfo = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<java.lang.String>> unpaidFeesList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<java.lang.String>> monthsList = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempAttendanceYearList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempAttendancePeriodList;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.UserPoint.StudentPortalProfile> getSPortalProfileInfoLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalSubject.Item>> getSProfileSubjectInfoAList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.UnpaidFees.Item>> getStudentPortalUnpaidFeesInfo() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<java.lang.String>> getUnpaidFeesList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<java.lang.String>> getMonthsList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempAttendanceYearList() {
        return null;
    }
    
    public final void setTempAttendanceYearList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> p0) {
    }
    
    private final java.util.ArrayList<java.lang.String> getTempAttendanceYear(java.util.List<com.netizen.netiworld.model.UserPoint.YearByTypeId.Item> yearList) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempAttendancePeriodList() {
        return null;
    }
    
    public final void setTempAttendancePeriodList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> p0) {
    }
    
    private final java.util.ArrayList<java.lang.String> getTempAttendancePeriod(java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalPeriod.Item> periodList) {
        return null;
    }
    
    public final void getSPortalProfileInfo() {
    }
    
    public final void getStudentPortalAtdPeriodData() {
    }
    
    public final void getStudentPortalAtdYearData() {
    }
    
    public final void getStudentPortalMonthData() {
    }
    
    public final void getSPortalSubjectInfo() {
    }
    
    public final void getStudentPortalUnpaidData(@org.jetbrains.annotations.NotNull()
    java.lang.String instituteId, @org.jetbrains.annotations.NotNull()
    java.lang.String studentId, @org.jetbrains.annotations.NotNull()
    java.lang.String academicYear) {
    }
    
    public final void spinnerStudentPortalUnpaidFees() {
    }
    
    public SPortalProfileViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
    
    @kotlin.Suppress(names = {"UNCHECKED_CAST"})
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\'\u0010\u0005\u001a\u0002H\u0006\"\n\b\u0000\u0010\u0006*\u0004\u0018\u00010\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016\u00a2\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/netiworld/viewModel/UserPoint/SPortalProfileViewModel$SPortalProfileViewModelFactory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "app_release"})
    public static final class SPortalProfileViewModelFactory implements androidx.lifecycle.ViewModelProvider.Factory {
        private final android.app.Application application = null;
        
        @java.lang.Override()
        public <T extends androidx.lifecycle.ViewModel>T create(@org.jetbrains.annotations.NotNull()
        java.lang.Class<T> modelClass) {
            return null;
        }
        
        public SPortalProfileViewModelFactory(@org.jetbrains.annotations.NotNull()
        android.app.Application application) {
            super();
        }
    }
}