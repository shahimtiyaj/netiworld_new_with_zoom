package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\t\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0010\u0002\n\u0002\b\r\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0013\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0014J\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001J\r\u0010\u0017\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0014J\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001J\r\u0010\u0019\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0014J\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001J\r\u0010\u001c\u001a\u0004\u0018\u00010\u000e\u00a2\u0006\u0002\u0010\u001dJ\b\u0010\u001e\u001a\u0004\u0018\u00010\u0011J\b\u0010\u001f\u001a\u0004\u0018\u00010\u0011J\u0015\u0010 \u001a\u00020!2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\"J\u0010\u0010#\u001a\u00020!2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001J\u0010\u0010$\u001a\u00020!2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0001J\u0015\u0010%\u001a\u00020!2\b\u0010\b\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\"J\u0010\u0010&\u001a\u00020!2\b\u0010\t\u001a\u0004\u0018\u00010\u0001J\u0015\u0010\'\u001a\u00020!2\b\u0010\n\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\"J\u0010\u0010(\u001a\u00020!2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001J\u0010\u0010)\u001a\u00020!2\b\u0010\f\u001a\u0004\u0018\u00010\u0001J\u0015\u0010*\u001a\u00020!2\b\u0010\r\u001a\u0004\u0018\u00010\u000e\u00a2\u0006\u0002\u0010+J\u0010\u0010,\u001a\u00020!2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011J\u0010\u0010-\u001a\u00020!2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000fR\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00118\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u00118\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006."}, d2 = {"Lcom/netizen/netiworld/model/BalanceStatementGetData;", "", "()V", "amount", "", "Ljava/lang/Double;", "basicMobile", "customID", "expense", "fullName", "income", "netiID", "note", "transactionDate", "", "Ljava/lang/Long;", "transactionFor", "", "trxAmount", "getAmount", "()Ljava/lang/Double;", "getBasicMobile", "getCustomID", "getExpense", "getFullName", "getIncome", "getNetiID", "getNote", "getTransactionDate", "()Ljava/lang/Long;", "getTransactionFor", "getTrxAmount", "setAmount", "", "(Ljava/lang/Double;)V", "setBasicMobile", "setCustomID", "setExpense", "setFullName", "setIncome", "setNetiID", "setNote", "setTransactionDate", "(Ljava/lang/Long;)V", "setTransactionFor", "setTrxAmount", "app_release"})
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
public final class BalanceStatementGetData {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "transactionDate")
    private java.lang.Long transactionDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "netiID")
    private java.lang.Object netiID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "customID")
    private java.lang.Object customID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "fullName")
    private java.lang.Object fullName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "basicMobile")
    private java.lang.Object basicMobile;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "note")
    private java.lang.Object note;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "amount")
    private java.lang.Double amount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "transactionFor")
    private java.lang.String transactionFor;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "income")
    private java.lang.Double income;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "expense")
    private java.lang.Double expense;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "trxAmount")
    private java.lang.String trxAmount;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getTransactionDate() {
        return null;
    }
    
    public final void setTransactionDate(@org.jetbrains.annotations.Nullable()
    java.lang.Long transactionDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getNetiID() {
        return null;
    }
    
    public final void setNetiID(@org.jetbrains.annotations.Nullable()
    java.lang.Object netiID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getCustomID() {
        return null;
    }
    
    public final void setCustomID(@org.jetbrains.annotations.Nullable()
    java.lang.Object customID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getFullName() {
        return null;
    }
    
    public final void setFullName(@org.jetbrains.annotations.Nullable()
    java.lang.Object fullName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getBasicMobile() {
        return null;
    }
    
    public final void setBasicMobile(@org.jetbrains.annotations.Nullable()
    java.lang.Object basicMobile) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getNote() {
        return null;
    }
    
    public final void setNote(@org.jetbrains.annotations.Nullable()
    java.lang.Object note) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getAmount() {
        return null;
    }
    
    public final void setAmount(@org.jetbrains.annotations.Nullable()
    java.lang.Double amount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTransactionFor() {
        return null;
    }
    
    public final void setTransactionFor(@org.jetbrains.annotations.Nullable()
    java.lang.String transactionFor) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getIncome() {
        return null;
    }
    
    public final void setIncome(@org.jetbrains.annotations.Nullable()
    java.lang.Double income) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getExpense() {
        return null;
    }
    
    public final void setExpense(@org.jetbrains.annotations.Nullable()
    java.lang.Double expense) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTrxAmount() {
        return null;
    }
    
    public final void setTrxAmount(@org.jetbrains.annotations.Nullable()
    java.lang.String trxAmount) {
    }
    
    public BalanceStatementGetData() {
        super();
    }
}