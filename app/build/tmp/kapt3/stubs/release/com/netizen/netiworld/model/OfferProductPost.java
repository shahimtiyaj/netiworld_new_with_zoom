package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u0010B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002B9\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000bR\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000bR \u0010\b\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000bR\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000b\u00a8\u0006\u0011"}, d2 = {"Lcom/netizen/netiworld/model/OfferProductPost;", "", "()V", "actualUnitPrice", "", "payableAmount", "totalDiscount", "totalPrice", "productOfferDTO", "Lcom/netizen/netiworld/model/OfferProductPost$ProductOfferDTO;", "(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lcom/netizen/netiworld/model/OfferProductPost$ProductOfferDTO;)V", "Ljava/lang/Double;", "getProductOfferDTO", "()Lcom/netizen/netiworld/model/OfferProductPost$ProductOfferDTO;", "setProductOfferDTO", "(Lcom/netizen/netiworld/model/OfferProductPost$ProductOfferDTO;)V", "ProductOfferDTO", "app_release"})
public final class OfferProductPost {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "actualUnitPrice")
    private java.lang.Double actualUnitPrice;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "payableAmount")
    private java.lang.Double payableAmount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "totalDiscount")
    private java.lang.Double totalDiscount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "totalPrice")
    private java.lang.Double totalPrice;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "productOfferDTO")
    private com.netizen.netiworld.model.OfferProductPost.ProductOfferDTO productOfferDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.OfferProductPost.ProductOfferDTO getProductOfferDTO() {
        return null;
    }
    
    public final void setProductOfferDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.OfferProductPost.ProductOfferDTO p0) {
    }
    
    public OfferProductPost() {
        super();
    }
    
    public OfferProductPost(@org.jetbrains.annotations.Nullable()
    java.lang.Double actualUnitPrice, @org.jetbrains.annotations.Nullable()
    java.lang.Double payableAmount, @org.jetbrains.annotations.Nullable()
    java.lang.Double totalDiscount, @org.jetbrains.annotations.Nullable()
    java.lang.Double totalPrice, @org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.OfferProductPost.ProductOfferDTO productOfferDTO) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\"\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\b\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004\u00a8\u0006\t"}, d2 = {"Lcom/netizen/netiworld/model/OfferProductPost$ProductOfferDTO;", "", "productOfferID", "", "(Ljava/lang/Integer;)V", "getProductOfferID", "()Ljava/lang/Integer;", "setProductOfferID", "Ljava/lang/Integer;", "app_release"})
    public static final class ProductOfferDTO {
        @org.jetbrains.annotations.Nullable()
        @com.google.gson.annotations.SerializedName(value = "productOfferID")
        private java.lang.Integer productOfferID = 0;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getProductOfferID() {
            return null;
        }
        
        public final void setProductOfferID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer p0) {
        }
        
        public ProductOfferDTO(@org.jetbrains.annotations.Nullable()
        java.lang.Integer productOfferID) {
            super();
        }
    }
}