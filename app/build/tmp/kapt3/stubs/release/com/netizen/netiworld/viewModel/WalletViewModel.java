package com.netizen.netiworld.viewModel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u001f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b!\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001pB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004JB\u0010J\u001a\u00020K2\b\u0010L\u001a\u0004\u0018\u00010\u00072\b\u0010M\u001a\u0004\u0018\u00010\u00072\b\u0010N\u001a\u0004\u0018\u00010\u00072\b\u0010O\u001a\u0004\u0018\u00010\u00072\b\u0010P\u001a\u0004\u0018\u00010\u00072\b\u0010Q\u001a\u0004\u0018\u00010\u0007J\u000e\u0010R\u001a\u00020K2\u0006\u0010S\u001a\u00020\u0007J\"\u0010T\u001a\u00020K2\b\u0010U\u001a\u0004\u0018\u00010\u00072\b\u0010V\u001a\u0004\u0018\u00010\u00072\u0006\u0010W\u001a\u00020\u0007J\u001a\u0010X\u001a\u00020K2\b\u0010U\u001a\u0004\u0018\u00010\u00072\b\u0010V\u001a\u0004\u0018\u00010\u0007J\u001a\u0010Y\u001a\u00020K2\b\u0010U\u001a\u0004\u0018\u00010\u00072\b\u0010V\u001a\u0004\u0018\u00010\u0007J\u000e\u0010Z\u001a\u00020K2\u0006\u0010[\u001a\u00020\u0007J4\u0010\\\u001a\u00020K2\u0006\u0010]\u001a\u00020\u00072\b\u0010;\u001a\u0004\u0018\u00010\u00072\u0006\u0010P\u001a\u00020\u00072\b\u0010^\u001a\u0004\u0018\u00010\u00072\b\u0010_\u001a\u0004\u0018\u00010\u0007J.\u0010`\u001a\u00020K2\b\u0010L\u001a\u0004\u0018\u00010\u00072\b\u0010M\u001a\u0004\u0018\u00010\u00072\b\u0010a\u001a\u0004\u0018\u00010\u00072\b\u0010b\u001a\u0004\u0018\u00010\u0007J\u0010\u0010c\u001a\u00020K2\b\u0010d\u001a\u0004\u0018\u00010\u0007J \u0010e\u001a\u00020K2\u0006\u0010]\u001a\u00020\u00072\b\u0010;\u001a\u0004\u0018\u00010\u00072\u0006\u0010P\u001a\u00020\u0007J\u001a\u0010f\u001a\u00020K2\b\u0010g\u001a\u0004\u0018\u00010\u00072\b\u0010P\u001a\u0004\u0018\u00010\u0007J\u0006\u0010h\u001a\u00020KJ\u0016\u0010i\u001a\u00020K2\u0006\u0010j\u001a\u00020\u00072\u0006\u0010k\u001a\u00020\u0007J&\u0010l\u001a\u0012\u0012\u0004\u0012\u00020\u00070?j\b\u0012\u0004\u0012\u00020\u0007`m2\f\u0010n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bH\u0002J\u0006\u0010o\u001a\u00020KR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u001a\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\u000b0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\tR\u001d\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00110\u000b0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\tR\u001d\u0010\u0013\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00140\u000b0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\tR\u001d\u0010\u0016\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00140\u000b0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\tR\u0010\u0010\u0018\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\tR\u0017\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\tR \u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\t\"\u0004\b\u001f\u0010 R\u0017\u0010!\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\tR \u0010\"\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\t\"\u0004\b#\u0010 R \u0010$\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\t\"\u0004\b%\u0010 R\u0017\u0010&\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\tR \u0010\'\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\'\u0010\t\"\u0004\b(\u0010 R \u0010)\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010\t\"\u0004\b*\u0010 R \u0010+\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\t\"\u0004\b,\u0010 R \u0010-\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b-\u0010\t\"\u0004\b.\u0010 R \u0010/\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b/\u0010\t\"\u0004\b0\u0010 R \u00101\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\t\"\u0004\b2\u0010 R \u00103\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b3\u0010\t\"\u0004\b4\u0010 R \u00105\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b5\u0010\t\"\u0004\b6\u0010 R \u00107\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u0010\t\"\u0004\b8\u0010 R\u0017\u00109\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b9\u0010\tR\u0017\u0010:\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b:\u0010\tR\u0017\u0010;\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b<\u0010\tR4\u0010=\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0007 @*\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010?0?0>X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bA\u0010B\"\u0004\bC\u0010DR\u000e\u0010E\u001a\u00020FX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010G\u001a\b\u0012\u0004\u0012\u00020H0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\bI\u0010\t\u00a8\u0006q"}, d2 = {"Lcom/netizen/netiworld/viewModel/WalletViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "PhoneNo", "Landroidx/lifecycle/MutableLiveData;", "", "getPhoneNo", "()Landroidx/lifecycle/MutableLiveData;", "accountInfoList", "", "Lcom/netizen/netiworld/model/BalanceDepositAccountInfo;", "balanceDepositReportDataList", "Lcom/netizen/netiworld/model/BalanceDepositReportGetData;", "getBalanceDepositReportDataList", "balanceStatementArrayList", "Lcom/netizen/netiworld/model/BalanceStatementGetData;", "getBalanceStatementArrayList", "balanceTransferArrayList", "Lcom/netizen/netiworld/model/BalanceTransferGetData;", "getBalanceTransferArrayList", "balanceTransferReportDataList", "getBalanceTransferReportDataList", "coreBankAccountId", "customNetiId", "getCustomNetiId", "fullName", "getFullName", "isAccountEmpty", "", "setAccountEmpty", "(Landroidx/lifecycle/MutableLiveData;)V", "isBalanceStatementDataFound", "isBranchEmpty", "setBranchEmpty", "isDepositAmountEmpty", "setDepositAmountEmpty", "isDepositDataFound", "isDepositDateEmpty", "setDepositDateEmpty", "isDepositTypeEmpty", "setDepositTypeEmpty", "isFromDateEmpty", "setFromDateEmpty", "isImageEmpty", "setImageEmpty", "isMobileBanking", "setMobileBanking", "isMobileBankingNoEmpty", "setMobileBankingNoEmpty", "isNoteEmpty", "setNoteEmpty", "isToDateEmpty", "setToDateEmpty", "isTransactionIdEmpty", "setTransactionIdEmpty", "isTransferDataFound", "isWithdrawDataFound", "netiMainID", "getNetiMainID", "tempAccountNumberList", "Landroidx/lifecycle/LiveData;", "Ljava/util/ArrayList;", "kotlin.jvm.PlatformType", "getTempAccountNumberList", "()Landroidx/lifecycle/LiveData;", "setTempAccountNumberList", "(Landroidx/lifecycle/LiveData;)V", "walletRepository", "Lcom/netizen/netiworld/repository/WalletRepository;", "withDrawInfo", "Lcom/netizen/netiworld/model/WithDrawInfo;", "getWithDrawInfo", "checkAccountDepositData", "", "depositAmount", "depositDate", "branchName", "depositType", "note", "image", "checkAccountType", "accountName", "checkBalanceReportPostData", "fromDate", "toDate", "type", "checkBalanceStatementReportData", "checkBalanceTransferReportPostData", "checkForOTPVarification", "OTP", "checkForRequestOTP", "TransferAmount", "inputNetiID", "customNetiID", "checkMobileDepositData", "mobileBankingNo", "transactionId", "checkTransferNetimanInfo", "netiId", "checkTransferWalletBalance", "checkWithdrawData", "withdrawAmount", "getAccountInfoList", "getBalanceStatementReportList", "startDate", "endDate", "getTempAccountInfoList", "Lkotlin/collections/ArrayList;", "bankBalanceDepositAccountInfoList", "getWithdrawInfoList", "WalletViewModelFactory", "app_release"})
public final class WalletViewModel extends androidx.lifecycle.AndroidViewModel {
    private final com.netizen.netiworld.repository.WalletRepository walletRepository = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceDepositAccountInfo>> accountInfoList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.WithDrawInfo> withDrawInfo = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.String> fullName = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.String> PhoneNo = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.String> netiMainID = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.String> customNetiId = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceStatementGetData>> balanceStatementArrayList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceTransferGetData>> balanceTransferArrayList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceDepositReportGetData>> balanceDepositReportDataList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceTransferGetData>> balanceTransferReportDataList = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMobileBanking;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAccountEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDepositAmountEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDepositDateEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMobileBankingNoEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isTransactionIdEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBranchEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDepositTypeEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isNoteEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isImageEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isFromDateEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isToDateEmpty;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDepositDataFound = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isWithdrawDataFound = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isTransferDataFound = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBalanceStatementDataFound = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempAccountNumberList;
    private java.lang.String coreBankAccountId;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.WithDrawInfo> getWithDrawInfo() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getFullName() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getPhoneNo() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getNetiMainID() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getCustomNetiId() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceStatementGetData>> getBalanceStatementArrayList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceTransferGetData>> getBalanceTransferArrayList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceDepositReportGetData>> getBalanceDepositReportDataList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceTransferGetData>> getBalanceTransferReportDataList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMobileBanking() {
        return null;
    }
    
    public final void setMobileBanking(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAccountEmpty() {
        return null;
    }
    
    public final void setAccountEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDepositAmountEmpty() {
        return null;
    }
    
    public final void setDepositAmountEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDepositDateEmpty() {
        return null;
    }
    
    public final void setDepositDateEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMobileBankingNoEmpty() {
        return null;
    }
    
    public final void setMobileBankingNoEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isTransactionIdEmpty() {
        return null;
    }
    
    public final void setTransactionIdEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBranchEmpty() {
        return null;
    }
    
    public final void setBranchEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDepositTypeEmpty() {
        return null;
    }
    
    public final void setDepositTypeEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isNoteEmpty() {
        return null;
    }
    
    public final void setNoteEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isImageEmpty() {
        return null;
    }
    
    public final void setImageEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isFromDateEmpty() {
        return null;
    }
    
    public final void setFromDateEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isToDateEmpty() {
        return null;
    }
    
    public final void setToDateEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDepositDataFound() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isWithdrawDataFound() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isTransferDataFound() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBalanceStatementDataFound() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempAccountNumberList() {
        return null;
    }
    
    public final void setTempAccountNumberList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> p0) {
    }
    
    private final java.util.ArrayList<java.lang.String> getTempAccountInfoList(java.util.List<com.netizen.netiworld.model.BalanceDepositAccountInfo> bankBalanceDepositAccountInfoList) {
        return null;
    }
    
    public final void getAccountInfoList() {
    }
    
    public final void getWithdrawInfoList() {
    }
    
    public final void checkAccountType(@org.jetbrains.annotations.NotNull()
    java.lang.String accountName) {
    }
    
    public final void checkMobileDepositData(@org.jetbrains.annotations.Nullable()
    java.lang.String depositAmount, @org.jetbrains.annotations.Nullable()
    java.lang.String depositDate, @org.jetbrains.annotations.Nullable()
    java.lang.String mobileBankingNo, @org.jetbrains.annotations.Nullable()
    java.lang.String transactionId) {
    }
    
    public final void checkAccountDepositData(@org.jetbrains.annotations.Nullable()
    java.lang.String depositAmount, @org.jetbrains.annotations.Nullable()
    java.lang.String depositDate, @org.jetbrains.annotations.Nullable()
    java.lang.String branchName, @org.jetbrains.annotations.Nullable()
    java.lang.String depositType, @org.jetbrains.annotations.Nullable()
    java.lang.String note, @org.jetbrains.annotations.Nullable()
    java.lang.String image) {
    }
    
    public final void checkWithdrawData(@org.jetbrains.annotations.Nullable()
    java.lang.String withdrawAmount, @org.jetbrains.annotations.Nullable()
    java.lang.String note) {
    }
    
    public final void checkTransferNetimanInfo(@org.jetbrains.annotations.Nullable()
    java.lang.String netiId) {
    }
    
    public final void checkForRequestOTP(@org.jetbrains.annotations.NotNull()
    java.lang.String TransferAmount, @org.jetbrains.annotations.Nullable()
    java.lang.String netiMainID, @org.jetbrains.annotations.NotNull()
    java.lang.String note, @org.jetbrains.annotations.Nullable()
    java.lang.String inputNetiID, @org.jetbrains.annotations.Nullable()
    java.lang.String customNetiID) {
    }
    
    public final void checkForOTPVarification(@org.jetbrains.annotations.NotNull()
    java.lang.String OTP) {
    }
    
    public final void checkTransferWalletBalance(@org.jetbrains.annotations.NotNull()
    java.lang.String TransferAmount, @org.jetbrains.annotations.Nullable()
    java.lang.String netiMainID, @org.jetbrains.annotations.NotNull()
    java.lang.String note) {
    }
    
    public final void checkBalanceReportPostData(@org.jetbrains.annotations.Nullable()
    java.lang.String fromDate, @org.jetbrains.annotations.Nullable()
    java.lang.String toDate, @org.jetbrains.annotations.NotNull()
    java.lang.String type) {
    }
    
    public final void checkBalanceTransferReportPostData(@org.jetbrains.annotations.Nullable()
    java.lang.String fromDate, @org.jetbrains.annotations.Nullable()
    java.lang.String toDate) {
    }
    
    public final void getBalanceStatementReportList(@org.jetbrains.annotations.NotNull()
    java.lang.String startDate, @org.jetbrains.annotations.NotNull()
    java.lang.String endDate) {
    }
    
    public final void checkBalanceStatementReportData(@org.jetbrains.annotations.Nullable()
    java.lang.String fromDate, @org.jetbrains.annotations.Nullable()
    java.lang.String toDate) {
    }
    
    public WalletViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\'\u0010\u0005\u001a\u0002H\u0006\"\n\b\u0000\u0010\u0006*\u0004\u0018\u00010\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016\u00a2\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/netiworld/viewModel/WalletViewModel$WalletViewModelFactory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "app_release"})
    public static final class WalletViewModelFactory implements androidx.lifecycle.ViewModelProvider.Factory {
        private final android.app.Application application = null;
        
        @java.lang.Override()
        public <T extends androidx.lifecycle.ViewModel>T create(@org.jetbrains.annotations.NotNull()
        java.lang.Class<T> modelClass) {
            return null;
        }
        
        public WalletViewModelFactory(@org.jetbrains.annotations.NotNull()
        android.app.Application application) {
            super();
        }
    }
}