package com.netizen.netiworld.view.fragment.myPoint.balance.wallet;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 .2\u00020\u0001:\u0001.B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\b\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0015\u001a\u00020\u0014H\u0002J\b\u0010\u0016\u001a\u00020\u0014H\u0002J\b\u0010\u0017\u001a\u00020\u0014H\u0002J\b\u0010\u0018\u001a\u00020\u0014H\u0002J\"\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001b2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J\u0012\u0010\u001f\u001a\u00020\u00142\b\u0010 \u001a\u0004\u0018\u00010!H\u0016J&\u0010\"\u001a\u0004\u0018\u00010#2\u0006\u0010$\u001a\u00020%2\b\u0010&\u001a\u0004\u0018\u00010\'2\b\u0010 \u001a\u0004\u0018\u00010!H\u0016J\b\u0010(\u001a\u00020\u0014H\u0002J\b\u0010)\u001a\u00020\u0014H\u0002J\b\u0010*\u001a\u00020\u0014H\u0002J\u0010\u0010+\u001a\u00020\u00142\u0006\u0010,\u001a\u00020\u000bH\u0002J\b\u0010-\u001a\u00020\u0014H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"}, d2 = {"Lcom/netizen/netiworld/view/fragment/myPoint/balance/wallet/DepositFragment;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/netizen/netiworld/databinding/FragmentDepositBinding;", "calendar", "Ljava/util/Calendar;", "kotlin.jvm.PlatformType", "dateSetListener", "Landroid/app/DatePickerDialog$OnDateSetListener;", "imageBase64", "", "tokenViewModel", "Lcom/netizen/netiworld/viewModel/TokenViewModel;", "walletViewModel", "Lcom/netizen/netiworld/viewModel/WalletViewModel;", "bitMapToString", "bitmap", "Landroid/graphics/Bitmap;", "clearFields", "", "hideAccountLayouts", "hideMobileBankingLayouts", "initObservers", "initViews", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "pickFilesFromMobileStorage", "showAccountLayouts", "showDatePicker", "showErrorToast", "message", "showMobileBankingLayouts", "Companion", "app_release"})
public final class DepositFragment extends androidx.fragment.app.Fragment {
    private com.netizen.netiworld.databinding.FragmentDepositBinding binding;
    private com.netizen.netiworld.viewModel.WalletViewModel walletViewModel;
    private com.netizen.netiworld.viewModel.TokenViewModel tokenViewModel;
    private java.util.Calendar calendar;
    private java.lang.String imageBase64;
    private final android.app.DatePickerDialog.OnDateSetListener dateSetListener = null;
    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE_READ = 1001;
    private static final int PERMISSION_CODE_WRITE = 1002;
    public static final com.netizen.netiworld.view.fragment.myPoint.balance.wallet.DepositFragment.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    private final void initViews() {
    }
    
    private final void initObservers() {
    }
    
    private final void pickFilesFromMobileStorage() {
    }
    
    private final void showDatePicker() {
    }
    
    private final java.lang.String bitMapToString(android.graphics.Bitmap bitmap) {
        return null;
    }
    
    private final void showMobileBankingLayouts() {
    }
    
    private final void hideMobileBankingLayouts() {
    }
    
    private final void showAccountLayouts() {
    }
    
    private final void hideAccountLayouts() {
    }
    
    private final void clearFields() {
    }
    
    private final void showErrorToast(java.lang.String message) {
    }
    
    public DepositFragment() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/netizen/netiworld/view/fragment/myPoint/balance/wallet/DepositFragment$Companion;", "", "()V", "IMAGE_PICK_CODE", "", "PERMISSION_CODE_READ", "PERMISSION_CODE_WRITE", "app_release"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}