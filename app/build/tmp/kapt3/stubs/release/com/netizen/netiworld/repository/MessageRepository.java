package com.netizen.netiworld.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010.\u001a\u00020/2\u0006\u00100\u001a\u00020\"2\u0006\u00101\u001a\u00020\"J\u000e\u00102\u001a\u00020/2\u0006\u00103\u001a\u00020\"J\u0006\u00104\u001a\u00020/J\u0010\u00105\u001a\u00020/2\b\u00106\u001a\u0004\u0018\u000107J\u001e\u00108\u001a\u00020/2\u0006\u00109\u001a\u00020\"2\u0006\u0010:\u001a\u00020\"2\u0006\u0010!\u001a\u00020\"R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR \u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\f\"\u0004\b\r\u0010\u000eR \u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\f\"\u0004\b\u0010\u0010\u000eR(\u0010\u0011\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00130\u00120\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\f\"\u0004\b\u0015\u0010\u000eR\u001d\u0010\u0016\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00170\u00120\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\fR\u0017\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00170\u001a\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR(\u0010\u001d\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u001e0\u00120\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\f\"\u0004\b \u0010\u000eR \u0010!\u001a\b\u0012\u0004\u0012\u00020\"0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\f\"\u0004\b$\u0010\u000eR\u001d\u0010%\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020&0\u00120\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\'\u0010\fR\u0017\u0010(\u001a\b\u0012\u0004\u0012\u00020&0\u001a\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010\u001cR(\u0010*\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010+0\u00120\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\f\"\u0004\b-\u0010\u000e\u00a8\u0006;"}, d2 = {"Lcom/netizen/netiworld/repository/MessageRepository;", "", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "appPreferences", "Lcom/netizen/netiworld/utils/AppPreferences;", "getApplication", "()Landroid/app/Application;", "isMessageLogDataFound", "Landroidx/lifecycle/MutableLiveData;", "", "()Landroidx/lifecycle/MutableLiveData;", "setMessageLogDataFound", "(Landroidx/lifecycle/MutableLiveData;)V", "isRevenueLogDataFound", "setRevenueLogDataFound", "messageRechargeInfo", "", "Lcom/netizen/netiworld/model/BalanceMessageGetData;", "getMessageRechargeInfo", "setMessageRechargeInfo", "messageTypeArrayList", "Lcom/netizen/netiworld/model/MessageType;", "getMessageTypeArrayList", "messageTypeArrayListData", "Ljava/util/ArrayList;", "getMessageTypeArrayListData", "()Ljava/util/ArrayList;", "messageTypeInfo", "Lcom/netizen/netiworld/model/MessageType1;", "getMessageTypeInfo", "setMessageTypeInfo", "productRoleAssignID", "", "getProductRoleAssignID", "setProductRoleAssignID", "purchasePointArrayList", "Lcom/netizen/netiworld/model/PurchasePoint;", "getPurchasePointArrayList", "purchasePointArrayListData", "getPurchasePointArrayListData", "revenueLogList", "Lcom/netizen/netiworld/model/RevenueLogGetData;", "getRevenueLogList", "setRevenueLogList", "getMessageRechargeListData", "", "startDateMessage", "endDateMessage", "getMessageTypeData", "purchasePointRoleId", "getPurchasePointDataFromServer", "getRevenueLogData", "revenueLogPostData", "Lcom/netizen/netiworld/model/RevenueLogPostData;", "submitMessageRechargeData", "messageQuantity", "productID", "app_release"})
public final class MessageRepository {
    private final com.netizen.netiworld.utils.AppPreferences appPreferences = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.PurchasePoint>> purchasePointArrayList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.MessageType>> messageTypeArrayList = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.ArrayList<com.netizen.netiworld.model.PurchasePoint> purchasePointArrayListData = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.ArrayList<com.netizen.netiworld.model.MessageType> messageTypeArrayListData = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> productRoleAssignID;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceMessageGetData>> messageRechargeInfo;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.RevenueLogGetData>> revenueLogList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.MessageType1>> messageTypeInfo;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isRevenueLogDataFound;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMessageLogDataFound;
    @org.jetbrains.annotations.NotNull()
    private final android.app.Application application = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.PurchasePoint>> getPurchasePointArrayList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.MessageType>> getMessageTypeArrayList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.netizen.netiworld.model.PurchasePoint> getPurchasePointArrayListData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.netizen.netiworld.model.MessageType> getMessageTypeArrayListData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getProductRoleAssignID() {
        return null;
    }
    
    public final void setProductRoleAssignID(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceMessageGetData>> getMessageRechargeInfo() {
        return null;
    }
    
    public final void setMessageRechargeInfo(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceMessageGetData>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.RevenueLogGetData>> getRevenueLogList() {
        return null;
    }
    
    public final void setRevenueLogList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.RevenueLogGetData>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.MessageType1>> getMessageTypeInfo() {
        return null;
    }
    
    public final void setMessageTypeInfo(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.MessageType1>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isRevenueLogDataFound() {
        return null;
    }
    
    public final void setRevenueLogDataFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMessageLogDataFound() {
        return null;
    }
    
    public final void setMessageLogDataFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    public final void getPurchasePointDataFromServer() {
    }
    
    public final void getMessageTypeData(@org.jetbrains.annotations.NotNull()
    java.lang.String purchasePointRoleId) {
    }
    
    public final void submitMessageRechargeData(@org.jetbrains.annotations.NotNull()
    java.lang.String messageQuantity, @org.jetbrains.annotations.NotNull()
    java.lang.String productID, @org.jetbrains.annotations.NotNull()
    java.lang.String productRoleAssignID) {
    }
    
    public final void getMessageRechargeListData(@org.jetbrains.annotations.NotNull()
    java.lang.String startDateMessage, @org.jetbrains.annotations.NotNull()
    java.lang.String endDateMessage) {
    }
    
    public final void getRevenueLogData(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.RevenueLogPostData revenueLogPostData) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.app.Application getApplication() {
        return null;
    }
    
    public MessageRepository(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super();
    }
}