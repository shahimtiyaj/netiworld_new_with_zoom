package com.netizen.netiworld.view.fragment.myPoint.balance.message;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\"B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0015\u001a\u00020\u0014H\u0002J\u0012\u0010\u0016\u001a\u00020\u00142\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J&\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J\u0010\u0010\u001f\u001a\u00020\u00142\u0006\u0010 \u001a\u00020!H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\"\u0010\u000f\u001a\u0016\u0012\u0004\u0012\u00020\f\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\f\u0018\u0001`\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\u0012\u001a\u0016\u0012\u0004\u0012\u00020\f\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\f\u0018\u0001`\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"}, d2 = {"Lcom/netizen/netiworld/view/fragment/myPoint/balance/message/FragmentMessageRecharge;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/netizen/netiworld/databinding/FragmentMessageRechargeBinding;", "homeViewModel", "Lcom/netizen/netiworld/viewModel/HomeViewModel;", "messageViewModel", "Lcom/netizen/netiworld/viewModel/MessageViewModel;", "preferences", "Lcom/netizen/netiworld/utils/AppPreferences;", "productRoleAssignID", "", "productUnitPrice", "productVatParcentage", "tempMessageTypeList", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "tempPurchasePointList", "initObservables", "", "initViews", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "setHomePageInfo", "homePageInfo", "Lcom/netizen/netiworld/model/HomePageInfo;", "MyTextWatcher", "app_release"})
public final class FragmentMessageRecharge extends androidx.fragment.app.Fragment {
    private com.netizen.netiworld.databinding.FragmentMessageRechargeBinding binding;
    private com.netizen.netiworld.viewModel.MessageViewModel messageViewModel;
    private com.netizen.netiworld.viewModel.HomeViewModel homeViewModel;
    private com.netizen.netiworld.utils.AppPreferences preferences;
    private java.lang.String productRoleAssignID;
    private java.lang.String productUnitPrice;
    private java.lang.String productVatParcentage;
    private java.util.ArrayList<java.lang.String> tempPurchasePointList;
    private java.util.ArrayList<java.lang.String> tempMessageTypeList;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void initViews() {
    }
    
    private final void initObservables() {
    }
    
    private final void setHomePageInfo(com.netizen.netiworld.model.HomePageInfo homePageInfo) {
    }
    
    public FragmentMessageRecharge() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\b\u0082\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J(\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\rH\u0016J(\u0010\u0010\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\rH\u0016R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"}, d2 = {"Lcom/netizen/netiworld/view/fragment/myPoint/balance/message/FragmentMessageRecharge$MyTextWatcher;", "Landroid/text/TextWatcher;", "view", "Landroid/view/View;", "(Lcom/netizen/netiworld/view/fragment/myPoint/balance/message/FragmentMessageRecharge;Landroid/view/View;)V", "afterTextChanged", "", "editable", "Landroid/text/Editable;", "beforeTextChanged", "charSequence", "", "i", "", "i1", "i2", "onTextChanged", "app_release"})
    final class MyTextWatcher implements android.text.TextWatcher {
        private final android.view.View view = null;
        
        @java.lang.Override()
        public void beforeTextChanged(@org.jetbrains.annotations.NotNull()
        java.lang.CharSequence charSequence, int i, int i1, int i2) {
        }
        
        @java.lang.Override()
        public void onTextChanged(@org.jetbrains.annotations.NotNull()
        java.lang.CharSequence charSequence, int i, int i1, int i2) {
        }
        
        @java.lang.Override()
        public void afterTextChanged(@org.jetbrains.annotations.NotNull()
        android.text.Editable editable) {
        }
        
        public MyTextWatcher(@org.jetbrains.annotations.Nullable()
        android.view.View view) {
            super();
        }
    }
}