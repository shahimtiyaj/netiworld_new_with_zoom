package com.netizen.netiworld.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u000e\u001a\u00020\u000fJ\u0006\u0010\u0010\u001a\u00020\u000fR\u001a\u0010\u0002\u001a\u00020\u0003X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004R\u001a\u0010\b\u001a\u00020\tX\u0080.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u0011"}, d2 = {"Lcom/netizen/netiworld/utils/ViewDialog;", "", "activity", "Landroid/app/Activity;", "(Landroid/app/Activity;)V", "getActivity$app_release", "()Landroid/app/Activity;", "setActivity$app_release", "dialog", "Landroid/app/Dialog;", "getDialog$app_release", "()Landroid/app/Dialog;", "setDialog$app_release", "(Landroid/app/Dialog;)V", "hideDialog", "", "showDialog", "app_release"})
public final class ViewDialog {
    @org.jetbrains.annotations.NotNull()
    public android.app.Dialog dialog;
    @org.jetbrains.annotations.NotNull()
    private android.app.Activity activity;
    
    @org.jetbrains.annotations.NotNull()
    public final android.app.Dialog getDialog$app_release() {
        return null;
    }
    
    public final void setDialog$app_release(@org.jetbrains.annotations.NotNull()
    android.app.Dialog p0) {
    }
    
    public final void showDialog() {
    }
    
    public final void hideDialog() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.app.Activity getActivity$app_release() {
        return null;
    }
    
    public final void setActivity$app_release(@org.jetbrains.annotations.NotNull()
    android.app.Activity p0) {
    }
    
    public ViewDialog(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
        super();
    }
}