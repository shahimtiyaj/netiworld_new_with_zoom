package com.netizen.netiworld.model.UserPoint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u0014B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u0004J\b\u0010\f\u001a\u0004\u0018\u00010\u0007J\r\u0010\r\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u000eJ\u0016\u0010\u000f\u001a\u00020\u00102\u000e\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004J\u0010\u0010\u0011\u001a\u00020\u00102\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0015\u0010\u0012\u001a\u00020\u00102\b\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u0013R\u001a\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/UnpaidFees;", "", "()V", "item", "", "Lcom/netizen/netiworld/model/UserPoint/UnpaidFees$Item;", "message", "", "msgType", "", "Ljava/lang/Integer;", "getItem", "getMessage", "getMsgType", "()Ljava/lang/Integer;", "setItem", "", "setMessage", "setMsgType", "(Ljava/lang/Integer;)V", "Item", "app_release"})
public final class UnpaidFees {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "message")
    private java.lang.String message;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "msgType")
    private java.lang.Integer msgType;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "item")
    private java.util.List<com.netizen.netiworld.model.UserPoint.UnpaidFees.Item> item;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.Nullable()
    java.lang.String message) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getMsgType() {
        return null;
    }
    
    public final void setMsgType(@org.jetbrains.annotations.Nullable()
    java.lang.Integer msgType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.netizen.netiworld.model.UserPoint.UnpaidFees.Item> getItem() {
        return null;
    }
    
    public final void setItem(@org.jetbrains.annotations.Nullable()
    java.util.List<com.netizen.netiworld.model.UserPoint.UnpaidFees.Item> item) {
    }
    
    public UnpaidFees() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0007\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u0012\n\u0002\u0010\u0002\n\u0002\b\u0010\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0005J\r\u0010\u0018\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0019J\b\u0010\u001a\u001a\u0004\u0018\u00010\u0005J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0005J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0005J\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001J\b\u0010\u001e\u001a\u0004\u0018\u00010\u0005J\r\u0010\u001f\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0019J\r\u0010 \u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0019J\b\u0010!\u001a\u0004\u0018\u00010\u0005J\b\u0010\"\u001a\u0004\u0018\u00010\u0005J\b\u0010#\u001a\u0004\u0018\u00010\u0005J\r\u0010$\u001a\u0004\u0018\u00010\u0014\u00a2\u0006\u0002\u0010%J\u0010\u0010&\u001a\u00020\'2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0001J\u0010\u0010(\u001a\u00020\'2\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0015\u0010)\u001a\u00020\'2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010*J\u0010\u0010+\u001a\u00020\'2\b\u0010\t\u001a\u0004\u0018\u00010\u0005J\u0010\u0010,\u001a\u00020\'2\b\u0010\n\u001a\u0004\u0018\u00010\u0005J\u0010\u0010-\u001a\u00020\'2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0005J\u0010\u0010.\u001a\u00020\'2\b\u0010\f\u001a\u0004\u0018\u00010\u0001J\u0010\u0010/\u001a\u00020\'2\b\u0010\r\u001a\u0004\u0018\u00010\u0005J\u0015\u00100\u001a\u00020\'2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010*J\u0015\u00101\u001a\u00020\'2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010*J\u0010\u00102\u001a\u00020\'2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0005J\u0010\u00103\u001a\u00020\'2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0005J\u0010\u00104\u001a\u00020\'2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0005J\u0015\u00105\u001a\u00020\'2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014\u00a2\u0006\u0002\u00106R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\t\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0016\u0010\u000f\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0013\u001a\u0004\u0018\u00010\u00148\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0015\u00a8\u00067"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/UnpaidFees$Item;", "", "()V", "classConfigName", "date", "", "dueAmt", "", "Ljava/lang/Float;", "feeHeadsName", "feeSubHeadsName", "invoiceId", "ledgerName", "mobileNo", "paidAmt", "payableAmt", "studentCustomId", "studentId", "studentName", "studentRoll", "", "Ljava/lang/Integer;", "getClassConfigName", "getDate", "getDueAmt", "()Ljava/lang/Float;", "getFeeHeadsName", "getFeeSubHeadsName", "getInvoiceId", "getLedgerName", "getMobileNo", "getPaidAmt", "getPayableAmt", "getStudentCustomId", "getStudentId", "getStudentName", "getStudentRoll", "()Ljava/lang/Integer;", "setClassConfigName", "", "setDate", "setDueAmt", "(Ljava/lang/Float;)V", "setFeeHeadsName", "setFeeSubHeadsName", "setInvoiceId", "setLedgerName", "setMobileNo", "setPaidAmt", "setPayableAmt", "setStudentCustomId", "setStudentId", "setStudentName", "setStudentRoll", "(Ljava/lang/Integer;)V", "app_release"})
    public static final class Item {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "invoiceId")
        private java.lang.String invoiceId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "studentName")
        private java.lang.String studentName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "studentRoll")
        private java.lang.Integer studentRoll;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "studentCustomId")
        private java.lang.String studentCustomId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "studentId")
        private java.lang.String studentId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "mobileNo")
        private java.lang.String mobileNo;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "feeHeadsName")
        private java.lang.String feeHeadsName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "feeSubHeadsName")
        private java.lang.String feeSubHeadsName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "classConfigName")
        private java.lang.Object classConfigName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "ledgerName")
        private java.lang.Object ledgerName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "payableAmt")
        private java.lang.Float payableAmt;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "paidAmt")
        private java.lang.Float paidAmt;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "dueAmt")
        private java.lang.Float dueAmt;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "date")
        private java.lang.String date;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getInvoiceId() {
            return null;
        }
        
        public final void setInvoiceId(@org.jetbrains.annotations.Nullable()
        java.lang.String invoiceId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getStudentName() {
            return null;
        }
        
        public final void setStudentName(@org.jetbrains.annotations.Nullable()
        java.lang.String studentName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getStudentRoll() {
            return null;
        }
        
        public final void setStudentRoll(@org.jetbrains.annotations.Nullable()
        java.lang.Integer studentRoll) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getStudentCustomId() {
            return null;
        }
        
        public final void setStudentCustomId(@org.jetbrains.annotations.Nullable()
        java.lang.String studentCustomId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getStudentId() {
            return null;
        }
        
        public final void setStudentId(@org.jetbrains.annotations.Nullable()
        java.lang.String studentId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getMobileNo() {
            return null;
        }
        
        public final void setMobileNo(@org.jetbrains.annotations.Nullable()
        java.lang.String mobileNo) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getFeeHeadsName() {
            return null;
        }
        
        public final void setFeeHeadsName(@org.jetbrains.annotations.Nullable()
        java.lang.String feeHeadsName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getFeeSubHeadsName() {
            return null;
        }
        
        public final void setFeeSubHeadsName(@org.jetbrains.annotations.Nullable()
        java.lang.String feeSubHeadsName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getClassConfigName() {
            return null;
        }
        
        public final void setClassConfigName(@org.jetbrains.annotations.Nullable()
        java.lang.Object classConfigName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getLedgerName() {
            return null;
        }
        
        public final void setLedgerName(@org.jetbrains.annotations.Nullable()
        java.lang.Object ledgerName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Float getPayableAmt() {
            return null;
        }
        
        public final void setPayableAmt(@org.jetbrains.annotations.Nullable()
        java.lang.Float payableAmt) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Float getPaidAmt() {
            return null;
        }
        
        public final void setPaidAmt(@org.jetbrains.annotations.Nullable()
        java.lang.Float paidAmt) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Float getDueAmt() {
            return null;
        }
        
        public final void setDueAmt(@org.jetbrains.annotations.Nullable()
        java.lang.Float dueAmt) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getDate() {
            return null;
        }
        
        public final void setDate(@org.jetbrains.annotations.Nullable()
        java.lang.String date) {
        }
        
        public Item() {
            super();
        }
    }
}