package com.netizen.netiworld.model.UserPoint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u0013B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\n\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u0004J\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001J\r\u0010\f\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\rJ\u0016\u0010\u000e\u001a\u00020\u000f2\u000e\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004J\u0010\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001J\u0015\u0010\u0011\u001a\u00020\u000f2\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u0012R\u001a\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\t\u00a8\u0006\u0014"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/StudentPortalSubject;", "", "()V", "item", "", "Lcom/netizen/netiworld/model/UserPoint/StudentPortalSubject$Item;", "message", "msgType", "", "Ljava/lang/Integer;", "getItem", "getMessage", "getMsgType", "()Ljava/lang/Integer;", "setItem", "", "setMessage", "setMsgType", "(Ljava/lang/Integer;)V", "Item", "app_release"})
public final class StudentPortalSubject {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "message")
    private java.lang.Object message;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "msgType")
    private java.lang.Integer msgType;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "item")
    private java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalSubject.Item> item;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.Nullable()
    java.lang.Object message) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getMsgType() {
        return null;
    }
    
    public final void setMsgType(@org.jetbrains.annotations.Nullable()
    java.lang.Integer msgType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalSubject.Item> getItem() {
        return null;
    }
    
    public final void setItem(@org.jetbrains.annotations.Nullable()
    java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalSubject.Item> item) {
    }
    
    public StudentPortalSubject() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\fJ\r\u0010\r\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\fJ\b\u0010\u000e\u001a\u0004\u0018\u00010\bJ\r\u0010\u000f\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\fJ\b\u0010\u0010\u001a\u0004\u0018\u00010\bJ\u0015\u0010\u0011\u001a\u00020\u00122\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0013J\u0015\u0010\u0014\u001a\u00020\u00122\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0013J\u0010\u0010\u0015\u001a\u00020\u00122\b\u0010\u0007\u001a\u0004\u0018\u00010\bJ\u0015\u0010\u0016\u001a\u00020\u00122\b\u0010\t\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0013J\u0010\u0010\u0017\u001a\u00020\u00122\b\u0010\n\u001a\u0004\u0018\u00010\bR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\n\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/StudentPortalSubject$Item;", "", "()V", "subjectId", "", "Ljava/lang/Integer;", "subjectMergeId", "subjectName", "", "subjectSerial", "subjectType", "getSubjectId", "()Ljava/lang/Integer;", "getSubjectMergeId", "getSubjectName", "getSubjectSerial", "getSubjectType", "setSubjectId", "", "(Ljava/lang/Integer;)V", "setSubjectMergeId", "setSubjectName", "setSubjectSerial", "setSubjectType", "app_release"})
    public static final class Item {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "subjectId")
        private java.lang.Integer subjectId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "subjectName")
        private java.lang.String subjectName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "subjectMergeId")
        private java.lang.Integer subjectMergeId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "subjectSerial")
        private java.lang.Integer subjectSerial;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "subjectType")
        private java.lang.String subjectType;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getSubjectId() {
            return null;
        }
        
        public final void setSubjectId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer subjectId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getSubjectName() {
            return null;
        }
        
        public final void setSubjectName(@org.jetbrains.annotations.Nullable()
        java.lang.String subjectName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getSubjectMergeId() {
            return null;
        }
        
        public final void setSubjectMergeId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer subjectMergeId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getSubjectSerial() {
            return null;
        }
        
        public final void setSubjectSerial(@org.jetbrains.annotations.Nullable()
        java.lang.Integer subjectSerial) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getSubjectType() {
            return null;
        }
        
        public final void setSubjectType(@org.jetbrains.annotations.Nullable()
        java.lang.String subjectType) {
        }
        
        public Item() {
            super();
        }
    }
}