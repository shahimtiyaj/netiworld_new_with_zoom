package com.netizen.netiworld.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u00104\u001a\u000205J\u0016\u00106\u001a\u0002052\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u00020\u001bJ\u0016\u0010:\u001a\u0002052\u0006\u0010;\u001a\u00020\u001b2\u0006\u0010<\u001a\u00020\u001bJ\u000e\u0010=\u001a\u0002052\u0006\u00107\u001a\u000208J\u000e\u0010>\u001a\u0002052\u0006\u0010?\u001a\u00020\u001bJ\u0006\u0010@\u001a\u000205J \u0010A\u001a\u0002052\u0006\u0010B\u001a\u00020\u001b2\b\u0010C\u001a\u0004\u0018\u00010\u001b2\u0006\u0010D\u001a\u00020\u001bJ\u000e\u0010E\u001a\u0002052\u0006\u0010F\u001a\u00020GJ\u000e\u0010H\u001a\u0002052\u0006\u0010I\u001a\u00020JJ\u001e\u0010K\u001a\u0002052\u0006\u0010B\u001a\u00020\u001b2\u0006\u0010*\u001a\u00020\u001b2\u0006\u0010D\u001a\u00020\u001bJ\u000e\u0010L\u001a\u0002052\u0006\u0010M\u001a\u00020\u001bR&\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\nR\u001d\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\nR\u001d\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\nR\u001d\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\nR \u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001b0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\n\"\u0004\b\u001d\u0010\fR \u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001b0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\n\"\u0004\b \u0010\fR \u0010!\u001a\b\u0012\u0004\u0012\u00020\"0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\n\"\u0004\b#\u0010\fR \u0010$\u001a\b\u0012\u0004\u0012\u00020\"0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\n\"\u0004\b%\u0010\fR \u0010&\u001a\b\u0012\u0004\u0012\u00020\"0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\n\"\u0004\b\'\u0010\fR \u0010(\u001a\b\u0012\u0004\u0012\u00020\"0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\n\"\u0004\b)\u0010\fR \u0010*\u001a\b\u0012\u0004\u0012\u00020\u001b0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\n\"\u0004\b,\u0010\fR \u0010-\u001a\b\u0012\u0004\u0012\u00020\u001b0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\n\"\u0004\b/\u0010\fR \u00100\u001a\b\u0012\u0004\u0012\u0002010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u0010\n\"\u0004\b3\u0010\f\u00a8\u0006N"}, d2 = {"Lcom/netizen/netiworld/repository/WalletRepository;", "", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "accountInfoList", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/netizen/netiworld/model/BalanceDepositAccountInfo;", "getAccountInfoList", "()Landroidx/lifecycle/MutableLiveData;", "setAccountInfoList", "(Landroidx/lifecycle/MutableLiveData;)V", "appPreferences", "Lcom/netizen/netiworld/utils/AppPreferences;", "balanceDepositReportDataList", "Lcom/netizen/netiworld/model/BalanceDepositReportGetData;", "getBalanceDepositReportDataList", "balanceStatementArrayList", "Lcom/netizen/netiworld/model/BalanceStatementGetData;", "getBalanceStatementArrayList", "balanceTransferArrayList", "Lcom/netizen/netiworld/model/BalanceTransferGetData;", "getBalanceTransferArrayList", "balanceTransferReportDataList", "getBalanceTransferReportDataList", "customNetiId", "", "getCustomNetiId", "setCustomNetiId", "fullName", "getFullName", "setFullName", "isBalanceSatementDataFound", "", "setBalanceSatementDataFound", "isDepositDataFound", "setDepositDataFound", "isTransferDataFound", "setTransferDataFound", "isWithdrawDataFound", "setWithdrawDataFound", "netiMainID", "getNetiMainID", "setNetiMainID", "phoneNo", "getPhoneNo", "setPhoneNo", "withDrawInfo", "Lcom/netizen/netiworld/model/WithDrawInfo;", "getWithDrawInfo", "setWithDrawInfo", "getAccountInfo", "", "getBalanceReportData", "balanceReportPostData", "Lcom/netizen/netiworld/model/BalanceReportPostData;", "type", "getBalanceStatementListData", "startDateStatement", "endDateStatement", "getBalanceTransferReportData", "getPersonInfoForWalletTransfer", "customNetiID", "getWithdrawInfo", "requestForOTP", "TransferAmount", "mainNetiID", "note", "submitDepositPostData", "depositPostData", "Lcom/netizen/netiworld/model/DepositPostData;", "submitWithdrawData", "withdrawPostData", "Lcom/netizen/netiworld/model/WithdrawPostData;", "transferWalletBalance", "varifyOTP", "OTP", "app_release"})
public final class WalletRepository {
    private final com.netizen.netiworld.utils.AppPreferences appPreferences = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceDepositAccountInfo>> accountInfoList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.WithDrawInfo> withDrawInfo;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> fullName;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> phoneNo;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> netiMainID;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> customNetiId;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDepositDataFound;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isWithdrawDataFound;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isTransferDataFound;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceTransferGetData>> balanceTransferArrayList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceStatementGetData>> balanceStatementArrayList = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBalanceSatementDataFound;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceDepositReportGetData>> balanceDepositReportDataList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceTransferGetData>> balanceTransferReportDataList = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceDepositAccountInfo>> getAccountInfoList() {
        return null;
    }
    
    public final void setAccountInfoList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceDepositAccountInfo>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.WithDrawInfo> getWithDrawInfo() {
        return null;
    }
    
    public final void setWithDrawInfo(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.WithDrawInfo> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getFullName() {
        return null;
    }
    
    public final void setFullName(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getPhoneNo() {
        return null;
    }
    
    public final void setPhoneNo(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getNetiMainID() {
        return null;
    }
    
    public final void setNetiMainID(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getCustomNetiId() {
        return null;
    }
    
    public final void setCustomNetiId(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDepositDataFound() {
        return null;
    }
    
    public final void setDepositDataFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isWithdrawDataFound() {
        return null;
    }
    
    public final void setWithdrawDataFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isTransferDataFound() {
        return null;
    }
    
    public final void setTransferDataFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceTransferGetData>> getBalanceTransferArrayList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceStatementGetData>> getBalanceStatementArrayList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBalanceSatementDataFound() {
        return null;
    }
    
    public final void setBalanceSatementDataFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceDepositReportGetData>> getBalanceDepositReportDataList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceTransferGetData>> getBalanceTransferReportDataList() {
        return null;
    }
    
    public final void getAccountInfo() {
    }
    
    public final void submitDepositPostData(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.DepositPostData depositPostData) {
    }
    
    public final void getWithdrawInfo() {
    }
    
    public final void submitWithdrawData(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.WithdrawPostData withdrawPostData) {
    }
    
    public final void getPersonInfoForWalletTransfer(@org.jetbrains.annotations.NotNull()
    java.lang.String customNetiID) {
    }
    
    public final void requestForOTP(@org.jetbrains.annotations.NotNull()
    java.lang.String TransferAmount, @org.jetbrains.annotations.Nullable()
    java.lang.String mainNetiID, @org.jetbrains.annotations.NotNull()
    java.lang.String note) {
    }
    
    public final void varifyOTP(@org.jetbrains.annotations.NotNull()
    java.lang.String OTP) {
    }
    
    public final void transferWalletBalance(@org.jetbrains.annotations.NotNull()
    java.lang.String TransferAmount, @org.jetbrains.annotations.NotNull()
    java.lang.String netiMainID, @org.jetbrains.annotations.NotNull()
    java.lang.String note) {
    }
    
    public final void getBalanceStatementListData(@org.jetbrains.annotations.NotNull()
    java.lang.String startDateStatement, @org.jetbrains.annotations.NotNull()
    java.lang.String endDateStatement) {
    }
    
    public final void getBalanceReportData(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.BalanceReportPostData balanceReportPostData, @org.jetbrains.annotations.NotNull()
    java.lang.String type) {
    }
    
    public final void getBalanceTransferReportData(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.BalanceReportPostData balanceReportPostData) {
    }
    
    public WalletRepository(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super();
    }
}