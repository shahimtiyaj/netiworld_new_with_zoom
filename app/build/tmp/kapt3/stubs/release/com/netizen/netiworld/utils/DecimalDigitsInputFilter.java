package com.netizen.netiworld.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J:\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\r2\u0006\u0010\u0012\u001a\u00020\rH\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u0013"}, d2 = {"Lcom/netizen/netiworld/utils/DecimalDigitsInputFilter;", "Landroid/text/InputFilter;", "()V", "mPattern", "Ljava/util/regex/Pattern;", "getMPattern$app_release", "()Ljava/util/regex/Pattern;", "setMPattern$app_release", "(Ljava/util/regex/Pattern;)V", "filter", "", "source", "start", "", "end", "dest", "Landroid/text/Spanned;", "dstart", "dend", "app_release"})
public final class DecimalDigitsInputFilter implements android.text.InputFilter {
    @org.jetbrains.annotations.NotNull()
    private java.util.regex.Pattern mPattern;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.regex.Pattern getMPattern$app_release() {
        return null;
    }
    
    public final void setMPattern$app_release(@org.jetbrains.annotations.NotNull()
    java.util.regex.Pattern p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.CharSequence filter(@org.jetbrains.annotations.NotNull()
    java.lang.CharSequence source, int start, int end, @org.jetbrains.annotations.NotNull()
    android.text.Spanned dest, int dstart, int dend) {
        return null;
    }
    
    public DecimalDigitsInputFilter() {
        super();
    }
}