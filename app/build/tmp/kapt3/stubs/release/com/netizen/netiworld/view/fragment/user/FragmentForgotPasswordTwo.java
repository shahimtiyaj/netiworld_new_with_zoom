package com.netizen.netiworld.view.fragment.user;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u00020\bH\u0002J\b\u0010\t\u001a\u00020\bH\u0002J\u0012\u0010\n\u001a\u00020\b2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0016J&\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u00122\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0016J\u0012\u0010\u0013\u001a\u00020\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/netizen/netiworld/view/fragment/user/FragmentForgotPasswordTwo;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/netizen/netiworld/databinding/FragmentForgotPasswordTwoBinding;", "userViewModel", "Lcom/netizen/netiworld/viewModel/UserViewModel;", "initObservers", "", "initViews", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "setUserInfo", "userCheckForgotPass", "Lcom/netizen/netiworld/model/UserCheckForgotPass;", "Companion", "app_release"})
public final class FragmentForgotPasswordTwo extends androidx.fragment.app.Fragment {
    private com.netizen.netiworld.databinding.FragmentForgotPasswordTwoBinding binding;
    private com.netizen.netiworld.viewModel.UserViewModel userViewModel;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String username = "";
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String mobileVarify = "";
    @org.jetbrains.annotations.NotNull()
    private static com.netizen.netiworld.model.UserCheckForgotPass userInformation;
    public static final com.netizen.netiworld.view.fragment.user.FragmentForgotPasswordTwo.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void initViews() {
    }
    
    private final void initObservers() {
    }
    
    private final void setUserInfo(com.netizen.netiworld.model.UserCheckForgotPass userCheckForgotPass) {
    }
    
    public FragmentForgotPasswordTwo() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0006\"\u0004\b\u0011\u0010\b\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/netiworld/view/fragment/user/FragmentForgotPasswordTwo$Companion;", "", "()V", "mobileVarify", "", "getMobileVarify", "()Ljava/lang/String;", "setMobileVarify", "(Ljava/lang/String;)V", "userInformation", "Lcom/netizen/netiworld/model/UserCheckForgotPass;", "getUserInformation", "()Lcom/netizen/netiworld/model/UserCheckForgotPass;", "setUserInformation", "(Lcom/netizen/netiworld/model/UserCheckForgotPass;)V", "username", "getUsername", "setUsername", "app_release"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getUsername() {
            return null;
        }
        
        public final void setUsername(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getMobileVarify() {
            return null;
        }
        
        public final void setMobileVarify(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.netizen.netiworld.model.UserCheckForgotPass getUserInformation() {
            return null;
        }
        
        public final void setUserInformation(@org.jetbrains.annotations.NotNull()
        com.netizen.netiworld.model.UserCheckForgotPass p0) {
        }
        
        private Companion() {
            super();
        }
    }
}