package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u0018B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\b\u0010\f\u001a\u0004\u0018\u00010\u0004J\b\u0010\r\u001a\u0004\u0018\u00010\u0007J\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0011\u001a\u00020\u00122\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0013\u001a\u00020\u00122\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0014\u001a\u00020\u00122\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0010\u0010\u0015\u001a\u00020\u00122\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0016\u001a\u00020\u00122\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0017\u001a\u00020\u00122\b\u0010\n\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"}, d2 = {"Lcom/netizen/netiworld/model/PostCertificationInfo;", "", "()V", "achieveDate", "", "certificationName", "countryInfoDTO", "Lcom/netizen/netiworld/model/PostCertificationInfo$CountryInfoDTO;", "courseDuration", "instituteLocation", "instituteName", "getAchieveDate", "getCertificationName", "getCountryInfoDTO", "getCourseDuration", "getInstituteLocation", "getInstituteName", "setAchieveDate", "", "setCertificationName", "setCountryInfoDTO", "setCourseDuration", "setInstituteLocation", "setInstituteName", "CountryInfoDTO", "app_release"})
public final class PostCertificationInfo {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "countryInfoDTO")
    private com.netizen.netiworld.model.PostCertificationInfo.CountryInfoDTO countryInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "certificationName")
    private java.lang.String certificationName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteName")
    private java.lang.String instituteName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteLocation")
    private java.lang.String instituteLocation;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "achieveDate")
    private java.lang.String achieveDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "courseDuration")
    private java.lang.String courseDuration;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.PostCertificationInfo.CountryInfoDTO getCountryInfoDTO() {
        return null;
    }
    
    public final void setCountryInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.PostCertificationInfo.CountryInfoDTO countryInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCertificationName() {
        return null;
    }
    
    public final void setCertificationName(@org.jetbrains.annotations.Nullable()
    java.lang.String certificationName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteName() {
        return null;
    }
    
    public final void setInstituteName(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteLocation() {
        return null;
    }
    
    public final void setInstituteLocation(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteLocation) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAchieveDate() {
        return null;
    }
    
    public final void setAchieveDate(@org.jetbrains.annotations.Nullable()
    java.lang.String achieveDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCourseDuration() {
        return null;
    }
    
    public final void setCourseDuration(@org.jetbrains.annotations.Nullable()
    java.lang.String courseDuration) {
    }
    
    public PostCertificationInfo() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/netizen/netiworld/model/PostCertificationInfo$CountryInfoDTO;", "", "()V", "coreCategoryID", "", "getCoreCategoryID", "setCoreCategoryID", "", "app_release"})
    public static final class CountryInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.String coreCategoryID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.String coreCategoryID) {
        }
        
        public CountryInfoDTO() {
            super();
        }
    }
}