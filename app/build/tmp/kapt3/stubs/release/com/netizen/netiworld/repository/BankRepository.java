package com.netizen.netiworld.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u000b\u001a\u00020/J\u0016\u0010\u0011\u001a\u00020/2\u0006\u00100\u001a\u0002012\u0006\u00102\u001a\u000201J\u0006\u0010\u0014\u001a\u00020/J\u0006\u0010!\u001a\u00020/J\u0006\u0010%\u001a\u00020/J\u0006\u00103\u001a\u00020/J\u0006\u0010-\u001a\u00020/J\u000e\u00104\u001a\u00020/2\u0006\u00105\u001a\u000206J\u000e\u00107\u001a\u00020/2\u0006\u00108\u001a\u000209J\u000e\u0010:\u001a\u00020/2\u0006\u0010;\u001a\u00020<J\u000e\u0010=\u001a\u00020/2\u0006\u0010>\u001a\u00020?J\u000e\u0010@\u001a\u00020/2\u0006\u0010A\u001a\u00020BR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R&\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR&\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\t0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\f\"\u0004\b\u0012\u0010\u000eR&\u0010\u0013\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\f\"\u0004\b\u0015\u0010\u000eR \u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00170\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\f\"\u0004\b\u0018\u0010\u000eR \u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00170\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\f\"\u0004\b\u001a\u0010\u000eR \u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00170\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\f\"\u0004\b\u001c\u0010\u000eR \u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00170\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\f\"\u0004\b\u001e\u0010\u000eR&\u0010\u001f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020 0\t0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\f\"\u0004\b\"\u0010\u000eR&\u0010#\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020$0\t0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\f\"\u0004\b&\u0010\u000eR&\u0010\'\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020(0\t0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010\f\"\u0004\b*\u0010\u000eR&\u0010+\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020,0\t0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b-\u0010\f\"\u0004\b.\u0010\u000e\u00a8\u0006C"}, d2 = {"Lcom/netizen/netiworld/repository/BankRepository;", "", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "appPreferences", "Lcom/netizen/netiworld/utils/AppPreferences;", "bankList", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/netizen/netiworld/model/BankAccountInfo;", "getBankList", "()Landroidx/lifecycle/MutableLiveData;", "setBankList", "(Landroidx/lifecycle/MutableLiveData;)V", "branchList", "Lcom/netizen/netiworld/model/BankBranchInfo;", "getBranchList", "setBranchList", "districtList", "getDistrictList", "setDistrictList", "isBankAccountListFound", "", "setBankAccountListFound", "isBranchListFound", "setBranchListFound", "isDistrictListFound", "setDistrictListFound", "isUsingPurposeListFound", "setUsingPurposeListFound", "tagList", "Lcom/netizen/netiworld/model/TagGetData;", "getTagList", "setTagList", "tagTypeList", "Lcom/netizen/netiworld/model/TagType;", "getTagTypeList", "setTagTypeList", "userBankAccountList", "Lcom/netizen/netiworld/model/BankList;", "getUserBankAccountList", "setUserBankAccountList", "usingPurposeList", "Lcom/netizen/netiworld/model/UsingPurpose;", "getUsingPurposeList", "setUsingPurposeList", "", "bankId", "", "districtId", "getUserBankList", "saveAddAndTagBankAccountInfo", "tagAndAddBankAccount", "Lcom/netizen/netiworld/model/TagAndAddBankAccount;", "sendNetiMail", "netiMailSubmit", "Lcom/netizen/netiworld/model/NetiMailSubmit;", "submitBankInfo", "bankAccountPostData", "Lcom/netizen/netiworld/model/BankAccountPostData;", "tagBankAccount", "tagPostData", "Lcom/netizen/netiworld/model/TagPostData;", "updateAddAndTagBankAccountInfo", "updateBankAccount", "Lcom/netizen/netiworld/model/UpdateBankAccount;", "app_release"})
public final class BankRepository {
    private final com.netizen.netiworld.utils.AppPreferences appPreferences = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UsingPurpose>> usingPurposeList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankAccountInfo>> bankList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankAccountInfo>> districtList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankBranchInfo>> branchList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankList>> userBankAccountList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TagGetData>> tagList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TagType>> tagTypeList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUsingPurposeListFound;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBankAccountListFound;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDistrictListFound;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBranchListFound;
    private final android.app.Application application = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UsingPurpose>> getUsingPurposeList() {
        return null;
    }
    
    public final void setUsingPurposeList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UsingPurpose>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankAccountInfo>> getBankList() {
        return null;
    }
    
    public final void setBankList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankAccountInfo>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankAccountInfo>> getDistrictList() {
        return null;
    }
    
    public final void setDistrictList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankAccountInfo>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankBranchInfo>> getBranchList() {
        return null;
    }
    
    public final void setBranchList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankBranchInfo>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankList>> getUserBankAccountList() {
        return null;
    }
    
    public final void setUserBankAccountList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BankList>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TagGetData>> getTagList() {
        return null;
    }
    
    public final void setTagList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TagGetData>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TagType>> getTagTypeList() {
        return null;
    }
    
    public final void setTagTypeList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TagType>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUsingPurposeListFound() {
        return null;
    }
    
    public final void setUsingPurposeListFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBankAccountListFound() {
        return null;
    }
    
    public final void setBankAccountListFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDistrictListFound() {
        return null;
    }
    
    public final void setDistrictListFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBranchListFound() {
        return null;
    }
    
    public final void setBranchListFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    public final void getUsingPurposeList() {
    }
    
    public final void getBankList() {
    }
    
    public final void getDistrictList() {
    }
    
    public final void getBranchList(@org.jetbrains.annotations.NotNull()
    java.lang.String bankId, @org.jetbrains.annotations.NotNull()
    java.lang.String districtId) {
    }
    
    public final void submitBankInfo(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.BankAccountPostData bankAccountPostData) {
    }
    
    public final void saveAddAndTagBankAccountInfo(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.TagAndAddBankAccount tagAndAddBankAccount) {
    }
    
    public final void getUserBankList() {
    }
    
    public final void getTagList() {
    }
    
    public final void getTagTypeList() {
    }
    
    public final void tagBankAccount(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.TagPostData tagPostData) {
    }
    
    public final void sendNetiMail(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.NetiMailSubmit netiMailSubmit) {
    }
    
    public final void updateAddAndTagBankAccountInfo(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.UpdateBankAccount updateBankAccount) {
    }
    
    public BankRepository(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super();
    }
}