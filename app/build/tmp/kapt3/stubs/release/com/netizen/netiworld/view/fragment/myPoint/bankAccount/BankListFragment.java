package com.netizen.netiworld.view.fragment.myPoint.bankAccount;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u000e\u001a\u00020\u000fH\u0002J\b\u0010\u0010\u001a\u00020\u000fH\u0002J\u0012\u0010\u0011\u001a\u00020\u000f2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J&\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u00192\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\u0018\u0010\u001a\u001a\u00020\u000f2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001cH\u0016J\b\u0010\u001e\u001a\u00020\u000fH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\"\u0010\u0006\u001a\u0016\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007j\n\u0012\u0004\u0012\u00020\b\u0018\u0001`\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\"\u0010\f\u001a\u0016\u0012\u0004\u0012\u00020\r\u0018\u00010\u0007j\n\u0012\u0004\u0012\u00020\r\u0018\u0001`\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"Lcom/netizen/netiworld/view/fragment/myPoint/bankAccount/BankListFragment;", "Landroidx/fragment/app/Fragment;", "Lcom/netizen/netiworld/adapter/myPoint/BankListAdapter$OnTagClickListener;", "()V", "bankAccountViewModel", "Lcom/netizen/netiworld/viewModel/BankAccountViewModel;", "bankList", "Ljava/util/ArrayList;", "Lcom/netizen/netiworld/model/BankList;", "Lkotlin/collections/ArrayList;", "binding", "Lcom/netizen/netiworld/databinding/FragmentBankListBinding;", "tagList", "Lcom/netizen/netiworld/model/TagGetData;", "initObservers", "", "initViews", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onTagClick", "userBankAccountId", "", "netiId", "setAdapter", "app_release"})
public final class BankListFragment extends androidx.fragment.app.Fragment implements com.netizen.netiworld.adapter.myPoint.BankListAdapter.OnTagClickListener {
    private com.netizen.netiworld.databinding.FragmentBankListBinding binding;
    private com.netizen.netiworld.viewModel.BankAccountViewModel bankAccountViewModel;
    private java.util.ArrayList<com.netizen.netiworld.model.BankList> bankList;
    private java.util.ArrayList<com.netizen.netiworld.model.TagGetData> tagList;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onTagClick(@org.jetbrains.annotations.NotNull()
    java.lang.String userBankAccountId, @org.jetbrains.annotations.NotNull()
    java.lang.String netiId) {
    }
    
    private final void initViews() {
    }
    
    private final void initObservers() {
    }
    
    private final void setAdapter() {
    }
    
    public BankListFragment() {
        super();
    }
}