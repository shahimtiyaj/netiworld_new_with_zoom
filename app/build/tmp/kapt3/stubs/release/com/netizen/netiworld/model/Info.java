package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u001d\n\u0002\u0010\u0002\n\u0002\b\u0015\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001d\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001e\u001a\u0004\u0018\u00010\u0004J\r\u0010\u001f\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010 J\b\u0010!\u001a\u0004\u0018\u00010\u0004J\r\u0010\"\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010#J\b\u0010$\u001a\u0004\u0018\u00010\u0004J\b\u0010%\u001a\u0004\u0018\u00010\u0004J\b\u0010&\u001a\u0004\u0018\u00010\u0004J\b\u0010\'\u001a\u0004\u0018\u00010\u0004J\r\u0010(\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010 J\b\u0010)\u001a\u0004\u0018\u00010\u0014J\b\u0010*\u001a\u0004\u0018\u00010\u0004J\r\u0010+\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010#J\r\u0010,\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010 J\r\u0010-\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010#J\r\u0010.\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010#J\r\u0010/\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010 J\r\u00100\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010#J\u000e\u00101\u001a\u0002022\u0006\u0010\u0006\u001a\u00020\u0004J\u000e\u00103\u001a\u0002022\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u00104\u001a\u0002022\u0006\u0010\u0005\u001a\u00020\u0004J\u0015\u00105\u001a\u0002022\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u00106J\u000e\u00107\u001a\u0002022\u0006\u0010\n\u001a\u00020\u0004J\u0015\u00108\u001a\u0002022\b\u0010\u000b\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u00109J\u000e\u0010:\u001a\u0002022\u0006\u0010\u000e\u001a\u00020\u0004J\u000e\u0010;\u001a\u0002022\u0006\u0010\u000f\u001a\u00020\u0004J\u000e\u0010<\u001a\u0002022\u0006\u0010\u0010\u001a\u00020\u0004J\u000e\u0010=\u001a\u0002022\u0006\u0010\u0011\u001a\u00020\u0004J\u0015\u0010>\u001a\u0002022\b\u0010\u0012\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u00106J\u0010\u0010?\u001a\u0002022\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014J\u000e\u0010@\u001a\u0002022\u0006\u0010\u0015\u001a\u00020\u0004J\u0015\u0010A\u001a\u0002022\b\u0010\u0016\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u00109J\u0015\u0010B\u001a\u0002022\b\u0010\u0017\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u00106J\u0015\u0010C\u001a\u0002022\b\u0010\u0018\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u00109J\u0015\u0010D\u001a\u0002022\b\u0010\u0019\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u00109J\u0015\u0010E\u001a\u0002022\b\u0010\u001a\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u00106J\u0015\u0010F\u001a\u0002022\b\u0010\u001b\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u00109R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\rR\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\u0013\u001a\u0004\u0018\u00010\u00148\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0016\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\rR\u0016\u0010\u0017\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0016\u0010\u0018\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\rR\u0016\u0010\u0019\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\rR\u0016\u0010\u001a\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0016\u0010\u001b\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\r\u00a8\u0006G"}, d2 = {"Lcom/netizen/netiworld/model/Info;", "", "()V", "basicEmail", "", "basicMobile", "categoryName", "customNetiID", "", "Ljava/lang/Integer;", "dateOfBirth", "emailBalance", "", "Ljava/lang/Double;", "fullName", "gender", "imageName", "imagePath", "netiID", "registrationDate", "Ljava/util/Date;", "religion", "smsBalance", "userEnableStatus", "userReserveBalance", "userWalletBalance", "validationStatus", "voiceBalance", "getAddress", "getBasicEmail", "getBasicMobile", "getCustomNetiID", "()Ljava/lang/Integer;", "getDateOfBirth", "getEmailBalance", "()Ljava/lang/Double;", "getFullName", "getGender", "getImageName", "getImagePath", "getNetiID", "getRegistrationDate", "getReligion", "getSmsBalance", "getUserEnableStatus", "getUserReserveBalance", "getUserWalletBalance", "getValidationStatus", "getVoiceBalance", "setAddress", "", "setBasicEmail", "setBasicMobile", "setCustomNetiID", "(Ljava/lang/Integer;)V", "setDateOfBirth", "setEmailBalance", "(Ljava/lang/Double;)V", "setFullName", "setGender", "setImageName", "setImagePath", "setNetiID", "setRegistrationDate", "setReligion", "setSmsBalance", "setUserEnableStatus", "setUserReserveBalance", "setUserWalletBalance", "setValidationStatus", "setVoiceBalance", "app_release"})
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
public final class Info {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "netiID")
    private java.lang.Integer netiID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "customNetiID")
    private java.lang.Integer customNetiID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "categoryName")
    private java.lang.String categoryName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "fullName")
    private java.lang.String fullName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "gender")
    private java.lang.String gender;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "religion")
    private java.lang.String religion;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "dateOfBirth")
    private java.lang.String dateOfBirth;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "basicMobile")
    private java.lang.String basicMobile;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "basicEmail")
    private java.lang.String basicEmail;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "imagePath")
    private java.lang.String imagePath;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "imageName")
    private java.lang.String imageName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userWalletBalance")
    private java.lang.Double userWalletBalance;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userReserveBalance")
    private java.lang.Double userReserveBalance;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "smsBalance")
    private java.lang.Double smsBalance;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "voiceBalance")
    private java.lang.Double voiceBalance;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "emailBalance")
    private java.lang.Double emailBalance;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "validationStatus")
    private java.lang.Integer validationStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userEnableStatus")
    private java.lang.Integer userEnableStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "registrationDate")
    private java.util.Date registrationDate;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getNetiID() {
        return null;
    }
    
    public final void setNetiID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer netiID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCustomNetiID() {
        return null;
    }
    
    public final void setCustomNetiID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer customNetiID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFullName() {
        return null;
    }
    
    public final void setFullName(@org.jetbrains.annotations.NotNull()
    java.lang.String fullName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getGender() {
        return null;
    }
    
    public final void setGender(@org.jetbrains.annotations.NotNull()
    java.lang.String gender) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReligion() {
        return null;
    }
    
    public final void setReligion(@org.jetbrains.annotations.NotNull()
    java.lang.String religion) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDateOfBirth() {
        return null;
    }
    
    public final void setDateOfBirth(@org.jetbrains.annotations.NotNull()
    java.lang.String dateOfBirth) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBasicMobile() {
        return null;
    }
    
    public final void setBasicMobile(@org.jetbrains.annotations.NotNull()
    java.lang.String basicMobile) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBasicEmail() {
        return null;
    }
    
    public final void setBasicEmail(@org.jetbrains.annotations.NotNull()
    java.lang.String basicEmail) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getImagePath() {
        return null;
    }
    
    public final void setImagePath(@org.jetbrains.annotations.NotNull()
    java.lang.String imagePath) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getImageName() {
        return null;
    }
    
    public final void setImageName(@org.jetbrains.annotations.NotNull()
    java.lang.String imageName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getUserWalletBalance() {
        return null;
    }
    
    public final void setUserWalletBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Double userWalletBalance) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getUserReserveBalance() {
        return null;
    }
    
    public final void setUserReserveBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Double userReserveBalance) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getSmsBalance() {
        return null;
    }
    
    public final void setSmsBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Double smsBalance) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getVoiceBalance() {
        return null;
    }
    
    public final void setVoiceBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Double voiceBalance) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getEmailBalance() {
        return null;
    }
    
    public final void setEmailBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Double emailBalance) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getValidationStatus() {
        return null;
    }
    
    public final void setValidationStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer validationStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getUserEnableStatus() {
        return null;
    }
    
    public final void setUserEnableStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer userEnableStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.Date getRegistrationDate() {
        return null;
    }
    
    public final void setRegistrationDate(@org.jetbrains.annotations.Nullable()
    java.util.Date registrationDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAddress() {
        return null;
    }
    
    public final void setAddress(@org.jetbrains.annotations.NotNull()
    java.lang.String categoryName) {
    }
    
    public Info() {
        super();
    }
}