package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002B%\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\bJ\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\r\u0010\f\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\rJ\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0007\u001a\u00020\u0004J\u000e\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u0006R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\t\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/netiworld/model/ProductGDetailsGetData;", "", "()V", "purchaseCode", "", "usedStatus", "", "usedDate", "(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V", "Ljava/lang/Integer;", "getPurchaseCode", "getUsedDate", "getUsedStatus", "()Ljava/lang/Integer;", "setPurchaseCode", "", "setUsedDate", "setUsedStatus", "app_release"})
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
public final class ProductGDetailsGetData {
    @com.google.gson.annotations.SerializedName(value = "purchaseCode")
    private java.lang.String purchaseCode;
    @com.google.gson.annotations.SerializedName(value = "usedStatus")
    private java.lang.Integer usedStatus;
    @com.google.gson.annotations.SerializedName(value = "usedDate")
    private java.lang.String usedDate;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPurchaseCode() {
        return null;
    }
    
    public final void setPurchaseCode(@org.jetbrains.annotations.NotNull()
    java.lang.String purchaseCode) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getUsedStatus() {
        return null;
    }
    
    public final void setUsedStatus(int usedStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUsedDate() {
        return null;
    }
    
    public final void setUsedDate(@org.jetbrains.annotations.NotNull()
    java.lang.String usedDate) {
    }
    
    public ProductGDetailsGetData() {
        super();
    }
    
    public ProductGDetailsGetData(@org.jetbrains.annotations.Nullable()
    java.lang.String purchaseCode, @org.jetbrains.annotations.Nullable()
    java.lang.Integer usedStatus, @org.jetbrains.annotations.Nullable()
    java.lang.String usedDate) {
        super();
    }
}