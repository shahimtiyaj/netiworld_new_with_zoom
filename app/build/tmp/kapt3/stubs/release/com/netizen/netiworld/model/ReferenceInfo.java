package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001:\u0001)B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0015\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0016\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u0017J\b\u0010\u0018\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0019\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001a\u001a\u0004\u0018\u00010\u000eJ\b\u0010\u001b\u001a\u0004\u0018\u00010\u0010J\b\u0010\u001c\u001a\u0004\u0018\u00010\u000eJ\u0010\u0010\u001d\u001a\u00020\u001e2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001f\u001a\u00020\u001e2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010 \u001a\u00020\u001e2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0010\u0010!\u001a\u00020\u001e2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\"\u001a\u00020\u001e2\b\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010#J\u0010\u0010$\u001a\u00020\u001e2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010%\u001a\u00020\u001e2\b\u0010\f\u001a\u0004\u0018\u00010\u0004J\u0010\u0010&\u001a\u00020\u001e2\b\u0010\r\u001a\u0004\u0018\u00010\u000eJ\u0010\u0010\'\u001a\u00020\u001e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010J\u0010\u0010(\u001a\u00020\u001e2\b\u0010\u0011\u001a\u0004\u0018\u00010\u000eR\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\nR\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"}, d2 = {"Lcom/netizen/netiworld/model/ReferenceInfo;", "Ljava/io/Serializable;", "()V", "orgainizationName", "", "referenceAddress", "referenceDesignation", "referenceEmail", "referenceId", "", "Ljava/lang/Integer;", "referenceMobile", "referenceName", "referenceSerial", "", "relationInfoDTO", "Lcom/netizen/netiworld/model/ReferenceInfo$RelationInfoDTO;", "userBasicInfoDTO", "getOrgainizationName", "getReferenceAddress", "getReferenceDesignation", "getReferenceEmail", "getReferenceId", "()Ljava/lang/Integer;", "getReferenceMobile", "getReferenceName", "getReferenceSerial", "getRelationInfoDTO", "getUserBasicInfoDTO", "setOrgainizationName", "", "setReferenceAddress", "setReferenceDesignation", "setReferenceEmail", "setReferenceId", "(Ljava/lang/Integer;)V", "setReferenceMobile", "setReferenceName", "setReferenceSerial", "setRelationInfoDTO", "setUserBasicInfoDTO", "RelationInfoDTO", "app_release"})
public final class ReferenceInfo implements java.io.Serializable {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "referenceId")
    private java.lang.Integer referenceId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "referenceSerial")
    private java.lang.Object referenceSerial;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "referenceName")
    private java.lang.String referenceName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "referenceDesignation")
    private java.lang.String referenceDesignation;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "orgainizationName")
    private java.lang.String orgainizationName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "referenceMobile")
    private java.lang.String referenceMobile;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "referenceEmail")
    private java.lang.String referenceEmail;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "referenceAddress")
    private java.lang.String referenceAddress;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "relationInfoDTO")
    private com.netizen.netiworld.model.ReferenceInfo.RelationInfoDTO relationInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userBasicInfoDTO")
    private java.lang.Object userBasicInfoDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getReferenceId() {
        return null;
    }
    
    public final void setReferenceId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer referenceId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getReferenceSerial() {
        return null;
    }
    
    public final void setReferenceSerial(@org.jetbrains.annotations.Nullable()
    java.lang.Object referenceSerial) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReferenceName() {
        return null;
    }
    
    public final void setReferenceName(@org.jetbrains.annotations.Nullable()
    java.lang.String referenceName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReferenceDesignation() {
        return null;
    }
    
    public final void setReferenceDesignation(@org.jetbrains.annotations.Nullable()
    java.lang.String referenceDesignation) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOrgainizationName() {
        return null;
    }
    
    public final void setOrgainizationName(@org.jetbrains.annotations.Nullable()
    java.lang.String orgainizationName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReferenceMobile() {
        return null;
    }
    
    public final void setReferenceMobile(@org.jetbrains.annotations.Nullable()
    java.lang.String referenceMobile) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReferenceEmail() {
        return null;
    }
    
    public final void setReferenceEmail(@org.jetbrains.annotations.Nullable()
    java.lang.String referenceEmail) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReferenceAddress() {
        return null;
    }
    
    public final void setReferenceAddress(@org.jetbrains.annotations.Nullable()
    java.lang.String referenceAddress) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.ReferenceInfo.RelationInfoDTO getRelationInfoDTO() {
        return null;
    }
    
    public final void setRelationInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.ReferenceInfo.RelationInfoDTO relationInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getUserBasicInfoDTO() {
        return null;
    }
    
    public final void setUserBasicInfoDTO(@org.jetbrains.annotations.Nullable()
    java.lang.Object userBasicInfoDTO) {
    }
    
    public ReferenceInfo() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0010\u001a\u00020\u000e2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0011R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/netiworld/model/ReferenceInfo$RelationInfoDTO;", "", "()V", "categoryDefaultCode", "", "categoryName", "coreCategoryID", "", "Ljava/lang/Integer;", "getCategoryDefaultCode", "getCategoryName", "getCoreCategoryID", "()Ljava/lang/Integer;", "setCategoryDefaultCode", "", "setCategoryName", "setCoreCategoryID", "(Ljava/lang/Integer;)V", "app_release"})
    public static final class RelationInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.Integer coreCategoryID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryDefaultCode")
        private java.lang.String categoryDefaultCode;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryName")
        private java.lang.String categoryName;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryDefaultCode() {
            return null;
        }
        
        public final void setCategoryDefaultCode(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryDefaultCode) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryName() {
            return null;
        }
        
        public final void setCategoryName(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryName) {
        }
        
        public RelationInfoDTO() {
            super();
        }
    }
}