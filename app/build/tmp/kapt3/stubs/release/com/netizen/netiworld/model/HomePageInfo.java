package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0002\f\rB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004J\b\u0010\b\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\t\u001a\u00020\n2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000b\u001a\u00020\n2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/netizen/netiworld/model/HomePageInfo;", "", "()V", "basicInfo", "Lcom/netizen/netiworld/model/HomePageInfo$BasicInfo;", "tokenSummary", "Lcom/netizen/netiworld/model/HomePageInfo$TokenSummary;", "getBasicInfo", "getTokenSummary", "setBasicInfo", "", "setTokenSummary", "BasicInfo", "TokenSummary", "app_release"})
public final class HomePageInfo {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "tokenSummary")
    private com.netizen.netiworld.model.HomePageInfo.TokenSummary tokenSummary;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "basicInfo")
    private com.netizen.netiworld.model.HomePageInfo.BasicInfo basicInfo;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.HomePageInfo.TokenSummary getTokenSummary() {
        return null;
    }
    
    public final void setTokenSummary(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.HomePageInfo.TokenSummary tokenSummary) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.HomePageInfo.BasicInfo getBasicInfo() {
        return null;
    }
    
    public final void setBasicInfo(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.HomePageInfo.BasicInfo basicInfo) {
    }
    
    public HomePageInfo() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0006\n\u0002\b\u0010\n\u0002\u0010\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0013\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0014J\r\u0010\u0015\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0014J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0017\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010\u0018J\r\u0010\u0019\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0014J\r\u0010\u001a\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010\u0018J\r\u0010\u001b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0014J\u0010\u0010\u001c\u001a\u00020\u001d2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001e\u001a\u00020\u001d2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u001f\u001a\u00020\u001d2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010 J\u0015\u0010!\u001a\u00020\u001d2\b\u0010\t\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010 J\u0010\u0010\"\u001a\u00020\u001d2\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\u0015\u0010#\u001a\u00020\u001d2\b\u0010\u000b\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010$J\u0015\u0010%\u001a\u00020\u001d2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010 J\u0015\u0010&\u001a\u00020\u001d2\b\u0010\u000f\u001a\u0004\u0018\u00010\f\u00a2\u0006\u0002\u0010$J\u0015\u0010\'\u001a\u00020\u001d2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010 R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0016\u0010\t\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\rR\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0016\u0010\u000f\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\rR\u0016\u0010\u0010\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006("}, d2 = {"Lcom/netizen/netiworld/model/HomePageInfo$BasicInfo;", "", "()V", "basicEmail", "", "basicMobile", "customNetiID", "", "Ljava/lang/Integer;", "parentCustomNetiID", "parentName", "smsBalance", "", "Ljava/lang/Double;", "userEnableStatus", "userWalletBalance", "validationStatus", "getBasicEmail", "getBasicMobile", "getCustomNetiID", "()Ljava/lang/Integer;", "getParentCustomNetiID", "getParentName", "getSmsBalance", "()Ljava/lang/Double;", "getUserEnableStatus", "getUserWalletBalance", "getValidationStatus", "setBasicEmail", "", "setBasicMobile", "setCustomNetiID", "(Ljava/lang/Integer;)V", "setParentCustomNetiID", "setParentName", "setSmsBalance", "(Ljava/lang/Double;)V", "setUserEnableStatus", "setUserWalletBalance", "setValidationStatus", "app_release"})
    public static final class BasicInfo {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "basicEmail")
        private java.lang.String basicEmail;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "basicMobile")
        private java.lang.String basicMobile;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "parentName")
        private java.lang.String parentName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "userEnableStatus")
        private java.lang.Integer userEnableStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "smsBalance")
        private java.lang.Double smsBalance;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "customNetiID")
        private java.lang.Integer customNetiID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "userWalletBalance")
        private java.lang.Double userWalletBalance;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "parentCustomNetiID")
        private java.lang.Integer parentCustomNetiID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "validationStatus")
        private java.lang.Integer validationStatus;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getBasicEmail() {
            return null;
        }
        
        public final void setBasicEmail(@org.jetbrains.annotations.Nullable()
        java.lang.String basicEmail) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getBasicMobile() {
            return null;
        }
        
        public final void setBasicMobile(@org.jetbrains.annotations.Nullable()
        java.lang.String basicMobile) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getParentName() {
            return null;
        }
        
        public final void setParentName(@org.jetbrains.annotations.Nullable()
        java.lang.String parentName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getUserEnableStatus() {
            return null;
        }
        
        public final void setUserEnableStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer userEnableStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Double getSmsBalance() {
            return null;
        }
        
        public final void setSmsBalance(@org.jetbrains.annotations.Nullable()
        java.lang.Double smsBalance) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCustomNetiID() {
            return null;
        }
        
        public final void setCustomNetiID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer customNetiID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Double getUserWalletBalance() {
            return null;
        }
        
        public final void setUserWalletBalance(@org.jetbrains.annotations.Nullable()
        java.lang.Double userWalletBalance) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getParentCustomNetiID() {
            return null;
        }
        
        public final void setParentCustomNetiID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer parentCustomNetiID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getValidationStatus() {
            return null;
        }
        
        public final void setValidationStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer validationStatus) {
        }
        
        public BasicInfo() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\b\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\tJ\r\u0010\n\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\tJ\r\u0010\u000b\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\tJ\u0015\u0010\f\u001a\u00020\r2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u000eJ\u0015\u0010\u000f\u001a\u00020\r2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u000eJ\u0015\u0010\u0010\u001a\u00020\r2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u000eR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005\u00a8\u0006\u0011"}, d2 = {"Lcom/netizen/netiworld/model/HomePageInfo$TokenSummary;", "", "()V", "pending", "", "Ljava/lang/Integer;", "rejected", "solved", "getPending", "()Ljava/lang/Integer;", "getRejected", "getSolved", "setPending", "", "(Ljava/lang/Integer;)V", "setRejected", "setSolved", "app_release"})
    public static final class TokenSummary {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "rejected")
        private java.lang.Integer rejected;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "pending")
        private java.lang.Integer pending;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "solved")
        private java.lang.Integer solved;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getRejected() {
            return null;
        }
        
        public final void setRejected(@org.jetbrains.annotations.Nullable()
        java.lang.Integer rejected) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getPending() {
            return null;
        }
        
        public final void setPending(@org.jetbrains.annotations.Nullable()
        java.lang.Integer pending) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getSolved() {
            return null;
        }
        
        public final void setSolved(@org.jetbrains.annotations.Nullable()
        java.lang.Integer solved) {
        }
        
        public TokenSummary() {
            super();
        }
    }
}