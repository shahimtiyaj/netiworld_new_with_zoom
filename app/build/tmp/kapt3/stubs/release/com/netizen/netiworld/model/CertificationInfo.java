package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001:\u0001\'B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u0012\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0013J\r\u0010\u0014\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u0016\u001a\u0004\u0018\u00010\nJ\b\u0010\u0017\u001a\u0004\u0018\u00010\fJ\b\u0010\u0018\u001a\u0004\u0018\u00010\u000eJ\b\u0010\u0019\u001a\u0004\u0018\u00010\fJ\b\u0010\u001a\u001a\u0004\u0018\u00010\fJ\b\u0010\u001b\u001a\u0004\u0018\u00010\fJ\u0015\u0010\u001c\u001a\u00020\u001d2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u001eJ\u0015\u0010\u001f\u001a\u00020\u001d2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010 J\u0010\u0010!\u001a\u00020\u001d2\b\u0010\t\u001a\u0004\u0018\u00010\nJ\u0010\u0010\"\u001a\u00020\u001d2\b\u0010\u000b\u001a\u0004\u0018\u00010\fJ\u0010\u0010#\u001a\u00020\u001d2\b\u0010\r\u001a\u0004\u0018\u00010\u000eJ\u0010\u0010$\u001a\u00020\u001d2\b\u0010\u000f\u001a\u0004\u0018\u00010\fJ\u0010\u0010%\u001a\u00020\u001d2\b\u0010\u0010\u001a\u0004\u0018\u00010\fJ\u0010\u0010&\u001a\u00020\u001d2\b\u0010\u0011\u001a\u0004\u0018\u00010\fR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\bR\u0014\u0010\t\u001a\u0004\u0018\u00010\n8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006("}, d2 = {"Lcom/netizen/netiworld/model/CertificationInfo;", "Ljava/io/Serializable;", "()V", "achieveDate", "", "Ljava/lang/Long;", "certificateInfoId", "", "Ljava/lang/Integer;", "certificateSerial", "", "certificationName", "", "countryInfoDTO", "Lcom/netizen/netiworld/model/CertificationInfo$CountryInfoDTO;", "courseDuration", "instituteLocation", "instituteName", "getAchieveDate", "()Ljava/lang/Long;", "getCertificateInfoId", "()Ljava/lang/Integer;", "getCertificateSerial", "getCertificationName", "getCountryInfoDTO", "getCourseDuration", "getInstituteLocation", "getInstituteName", "setAchieveDate", "", "(Ljava/lang/Long;)V", "setCertificateInfoId", "(Ljava/lang/Integer;)V", "setCertificateSerial", "setCertificationName", "setCountryInfoDTO", "setCourseDuration", "setInstituteLocation", "setInstituteName", "CountryInfoDTO", "app_release"})
public final class CertificationInfo implements java.io.Serializable {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "certificateInfoId")
    private java.lang.Integer certificateInfoId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "certificateSerial")
    private java.lang.Object certificateSerial;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "certificationName")
    private java.lang.String certificationName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteName")
    private java.lang.String instituteName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteLocation")
    private java.lang.String instituteLocation;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "achieveDate")
    private java.lang.Long achieveDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "courseDuration")
    private java.lang.String courseDuration;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "countryInfoDTO")
    private com.netizen.netiworld.model.CertificationInfo.CountryInfoDTO countryInfoDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCertificateInfoId() {
        return null;
    }
    
    public final void setCertificateInfoId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer certificateInfoId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getCertificateSerial() {
        return null;
    }
    
    public final void setCertificateSerial(@org.jetbrains.annotations.Nullable()
    java.lang.Object certificateSerial) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCertificationName() {
        return null;
    }
    
    public final void setCertificationName(@org.jetbrains.annotations.Nullable()
    java.lang.String certificationName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteName() {
        return null;
    }
    
    public final void setInstituteName(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteLocation() {
        return null;
    }
    
    public final void setInstituteLocation(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteLocation) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getAchieveDate() {
        return null;
    }
    
    public final void setAchieveDate(@org.jetbrains.annotations.Nullable()
    java.lang.Long achieveDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCourseDuration() {
        return null;
    }
    
    public final void setCourseDuration(@org.jetbrains.annotations.Nullable()
    java.lang.String courseDuration) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.CertificationInfo.CountryInfoDTO getCountryInfoDTO() {
        return null;
    }
    
    public final void setCountryInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.CertificationInfo.CountryInfoDTO countryInfoDTO) {
    }
    
    public CertificationInfo() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0010\u001a\u00020\u000e2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0011R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/netiworld/model/CertificationInfo$CountryInfoDTO;", "", "()V", "categoryDefaultCode", "", "categoryName", "coreCategoryID", "", "Ljava/lang/Integer;", "getCategoryDefaultCode", "getCategoryName", "getCoreCategoryID", "()Ljava/lang/Integer;", "setCategoryDefaultCode", "", "setCategoryName", "setCoreCategoryID", "(Ljava/lang/Integer;)V", "app_release"})
    public static final class CountryInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.Integer coreCategoryID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryDefaultCode")
        private java.lang.String categoryDefaultCode;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryName")
        private java.lang.String categoryName;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryDefaultCode() {
            return null;
        }
        
        public final void setCategoryDefaultCode(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryDefaultCode) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryName() {
            return null;
        }
        
        public final void setCategoryName(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryName) {
        }
        
        public CountryInfoDTO() {
            super();
        }
    }
}