package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0006\n\u0002\b\u0012\n\u0002\u0010\u0002\n\u0002\b\u0010\u0018\u00002\u00020\u0001:\u00015B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0017\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0018J\b\u0010\u0019\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001a\u001a\u0004\u0018\u00010\u0004J\r\u0010\u001b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0018J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\r\u0010\u001d\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0018J\b\u0010\u001e\u001a\u0004\u0018\u00010\u000eJ\b\u0010\u001f\u001a\u0004\u0018\u00010\u0004J\b\u0010 \u001a\u0004\u0018\u00010\u0004J\b\u0010!\u001a\u0004\u0018\u00010\u0004J\r\u0010\"\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0002\u0010#J\r\u0010$\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0002\u0010#J\u0010\u0010%\u001a\u00020&2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\'\u001a\u00020&2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010(J\u0010\u0010)\u001a\u00020&2\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010*\u001a\u00020&2\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0015\u0010+\u001a\u00020&2\b\u0010\n\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010(J\u0010\u0010,\u001a\u00020&2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u0015\u0010-\u001a\u00020&2\b\u0010\f\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010(J\u0010\u0010.\u001a\u00020&2\b\u0010\r\u001a\u0004\u0018\u00010\u000eJ\u0010\u0010/\u001a\u00020&2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\u0010\u00100\u001a\u00020&2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\u0010\u00101\u001a\u00020&2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\u0015\u00102\u001a\u00020&2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0002\u00103J\u0015\u00104\u001a\u00020&2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0002\u00103R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\u0004\u0018\u00010\u00138\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0014R\u0016\u0010\u0015\u001a\u0004\u0018\u00010\u00138\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0014\u00a8\u00066"}, d2 = {"Lcom/netizen/netiworld/model/BalanceDepositAccountInfo;", "", "()V", "accDefaultCode", "", "accEnableStatus", "", "Ljava/lang/Integer;", "accInfo", "accNote", "accSerial", "accShortName", "coreBankAccId", "coreCategoryInfoDTO", "Lcom/netizen/netiworld/model/BalanceDepositAccountInfo$CoreCategoryInfoDTO;", "lastDateExecuted", "lastIpExecuted", "lastUserExecuted", "maxRecharge", "", "Ljava/lang/Double;", "minRecharge", "getAccDefaultCode", "getAccEnableStatus", "()Ljava/lang/Integer;", "getAccInfo", "getAccNote", "getAccSerial", "getAccShortName", "getCoreBankAccId", "getCoreCategoryInfoDTO", "getLastDateExecuted", "getLastIpExecuted", "getLastUserExecuted", "getMaxRecharge", "()Ljava/lang/Double;", "getMinRecharge", "setAccDefaultCode", "", "setAccEnableStatus", "(Ljava/lang/Integer;)V", "setAccInfo", "setAccNote", "setAccSerial", "setAccShortName", "setCoreBankAccId", "setCoreCategoryInfoDTO", "setLastDateExecuted", "setLastIpExecuted", "setLastUserExecuted", "setMaxRecharge", "(Ljava/lang/Double;)V", "setMinRecharge", "CoreCategoryInfoDTO", "app_release"})
public final class BalanceDepositAccountInfo {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "coreBankAccId")
    private java.lang.Integer coreBankAccId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "accDefaultCode")
    private java.lang.String accDefaultCode;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "accInfo")
    private java.lang.String accInfo;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "accEnableStatus")
    private java.lang.Integer accEnableStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "accSerial")
    private java.lang.Integer accSerial;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "accNote")
    private java.lang.String accNote;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "maxRecharge")
    private java.lang.Double maxRecharge;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "minRecharge")
    private java.lang.Double minRecharge;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "accShortName")
    private java.lang.String accShortName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "lastUserExecuted")
    private java.lang.String lastUserExecuted;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "lastIpExecuted")
    private java.lang.String lastIpExecuted;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "lastDateExecuted")
    private java.lang.String lastDateExecuted;
    @com.google.gson.annotations.Expose()
    private com.netizen.netiworld.model.BalanceDepositAccountInfo.CoreCategoryInfoDTO coreCategoryInfoDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCoreBankAccId() {
        return null;
    }
    
    public final void setCoreBankAccId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer coreBankAccId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccDefaultCode() {
        return null;
    }
    
    public final void setAccDefaultCode(@org.jetbrains.annotations.Nullable()
    java.lang.String accDefaultCode) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccInfo() {
        return null;
    }
    
    public final void setAccInfo(@org.jetbrains.annotations.Nullable()
    java.lang.String accInfo) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getAccEnableStatus() {
        return null;
    }
    
    public final void setAccEnableStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer accEnableStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getAccSerial() {
        return null;
    }
    
    public final void setAccSerial(@org.jetbrains.annotations.Nullable()
    java.lang.Integer accSerial) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccNote() {
        return null;
    }
    
    public final void setAccNote(@org.jetbrains.annotations.Nullable()
    java.lang.String accNote) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getMaxRecharge() {
        return null;
    }
    
    public final void setMaxRecharge(@org.jetbrains.annotations.Nullable()
    java.lang.Double maxRecharge) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getMinRecharge() {
        return null;
    }
    
    public final void setMinRecharge(@org.jetbrains.annotations.Nullable()
    java.lang.Double minRecharge) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccShortName() {
        return null;
    }
    
    public final void setAccShortName(@org.jetbrains.annotations.Nullable()
    java.lang.String accShortName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLastUserExecuted() {
        return null;
    }
    
    public final void setLastUserExecuted(@org.jetbrains.annotations.Nullable()
    java.lang.String lastUserExecuted) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLastIpExecuted() {
        return null;
    }
    
    public final void setLastIpExecuted(@org.jetbrains.annotations.Nullable()
    java.lang.String lastIpExecuted) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLastDateExecuted() {
        return null;
    }
    
    public final void setLastDateExecuted(@org.jetbrains.annotations.Nullable()
    java.lang.String lastDateExecuted) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.BalanceDepositAccountInfo.CoreCategoryInfoDTO getCoreCategoryInfoDTO() {
        return null;
    }
    
    public final void setCoreCategoryInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.BalanceDepositAccountInfo.CoreCategoryInfoDTO coreCategoryInfoDTO) {
    }
    
    public BalanceDepositAccountInfo() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\bB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/netizen/netiworld/model/BalanceDepositAccountInfo$CoreCategoryInfoDTO;", "", "()V", "parentTypeInfoDTO", "Lcom/netizen/netiworld/model/BalanceDepositAccountInfo$CoreCategoryInfoDTO$ParentTypeInfoDTO;", "getParentTypeInfoDTO", "setParentTypeInfoDTO", "", "ParentTypeInfoDTO", "app_release"})
    public static final class CoreCategoryInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "parentTypeInfoDTO")
        private com.netizen.netiworld.model.BalanceDepositAccountInfo.CoreCategoryInfoDTO.ParentTypeInfoDTO parentTypeInfoDTO;
        
        @org.jetbrains.annotations.Nullable()
        public final com.netizen.netiworld.model.BalanceDepositAccountInfo.CoreCategoryInfoDTO.ParentTypeInfoDTO getParentTypeInfoDTO() {
            return null;
        }
        
        public final void setParentTypeInfoDTO(@org.jetbrains.annotations.Nullable()
        com.netizen.netiworld.model.BalanceDepositAccountInfo.CoreCategoryInfoDTO.ParentTypeInfoDTO parentTypeInfoDTO) {
        }
        
        public CoreCategoryInfoDTO() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/netizen/netiworld/model/BalanceDepositAccountInfo$CoreCategoryInfoDTO$ParentTypeInfoDTO;", "", "()V", "categoryName", "", "getCategoryName", "setCategoryName", "", "app_release"})
        public static final class ParentTypeInfoDTO {
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "categoryName")
            private java.lang.String categoryName;
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getCategoryName() {
                return null;
            }
            
            public final void setCategoryName(@org.jetbrains.annotations.Nullable()
            java.lang.String categoryName) {
            }
            
            public ParentTypeInfoDTO() {
                super();
            }
        }
    }
}