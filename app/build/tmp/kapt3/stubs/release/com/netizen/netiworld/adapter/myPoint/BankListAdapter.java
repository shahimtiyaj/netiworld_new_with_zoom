package com.netizen.netiworld.adapter.myPoint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0017\u0018B1\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0006\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\fJ\b\u0010\r\u001a\u00020\u000eH\u0016J\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u000eH\u0016J\u0018\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000eH\u0016R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"}, d2 = {"Lcom/netizen/netiworld/adapter/myPoint/BankListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/netizen/netiworld/adapter/myPoint/BankListAdapter$ViewHolder;", "context", "Landroid/content/Context;", "bankList", "", "Lcom/netizen/netiworld/model/BankList;", "tagList", "Lcom/netizen/netiworld/model/TagGetData;", "listener", "Lcom/netizen/netiworld/adapter/myPoint/BankListAdapter$OnTagClickListener;", "(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Lcom/netizen/netiworld/adapter/myPoint/BankListAdapter$OnTagClickListener;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "OnTagClickListener", "ViewHolder", "app_release"})
public final class BankListAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.netizen.netiworld.adapter.myPoint.BankListAdapter.ViewHolder> {
    private final android.content.Context context = null;
    private final java.util.List<com.netizen.netiworld.model.BankList> bankList = null;
    private final java.util.List<com.netizen.netiworld.model.TagGetData> tagList = null;
    private final com.netizen.netiworld.adapter.myPoint.BankListAdapter.OnTagClickListener listener = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.netizen.netiworld.adapter.myPoint.BankListAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.adapter.myPoint.BankListAdapter.ViewHolder holder, int position) {
    }
    
    public BankListAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<com.netizen.netiworld.model.BankList> bankList, @org.jetbrains.annotations.NotNull()
    java.util.List<com.netizen.netiworld.model.TagGetData> tagList, @org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.adapter.myPoint.BankListAdapter.OnTagClickListener listener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H\u0016\u00a8\u0006\u0007"}, d2 = {"Lcom/netizen/netiworld/adapter/myPoint/BankListAdapter$OnTagClickListener;", "", "onTagClick", "", "userBankAccountId", "", "netiId", "app_release"})
    public static abstract interface OnTagClickListener {
        
        public abstract void onTagClick(@org.jetbrains.annotations.NotNull()
        java.lang.String userBankAccountId, @org.jetbrains.annotations.NotNull()
        java.lang.String netiId);
        
        @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 3)
        public static final class DefaultImpls {
            
            public static void onTagClick(com.netizen.netiworld.adapter.myPoint.BankListAdapter.OnTagClickListener $this, @org.jetbrains.annotations.NotNull()
            java.lang.String userBankAccountId, @org.jetbrains.annotations.NotNull()
            java.lang.String netiId) {
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001c\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u000e"}, d2 = {"Lcom/netizen/netiworld/adapter/myPoint/BankListAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemBinding", "Lcom/netizen/netiworld/databinding/SingleBankListLayoutBinding;", "(Lcom/netizen/netiworld/databinding/SingleBankListLayoutBinding;)V", "getItemBinding", "()Lcom/netizen/netiworld/databinding/SingleBankListLayoutBinding;", "bind", "", "bankList", "Lcom/netizen/netiworld/model/BankList;", "tagList", "", "Lcom/netizen/netiworld/model/TagGetData;", "app_release"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final com.netizen.netiworld.databinding.SingleBankListLayoutBinding itemBinding = null;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.netizen.netiworld.model.BankList bankList, @org.jetbrains.annotations.NotNull()
        java.util.List<com.netizen.netiworld.model.TagGetData> tagList) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.netizen.netiworld.databinding.SingleBankListLayoutBinding getItemBinding() {
            return null;
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        com.netizen.netiworld.databinding.SingleBankListLayoutBinding itemBinding) {
            super(null);
        }
    }
}