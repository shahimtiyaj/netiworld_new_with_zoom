package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002B\u000f\b\u0016\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005B\u0017\b\u0016\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\bJ\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\fJ\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u0004J\u0015\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0010R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\t\u00a8\u0006\u0011"}, d2 = {"Lcom/netizen/netiworld/model/MobileBankAcNo;", "", "()V", "accShortName", "", "(Ljava/lang/String;)V", "coreBankAccId", "", "(ILjava/lang/String;)V", "Ljava/lang/Integer;", "getAccShortName", "getCoreBankAccountId", "()Ljava/lang/Integer;", "setAccShortName", "", "setCoreBankAccountId", "(Ljava/lang/Integer;)V", "app_debug"})
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
public final class MobileBankAcNo {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "coreBankAccId")
    private java.lang.Integer coreBankAccId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "accShortName")
    private java.lang.String accShortName;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCoreBankAccountId() {
        return null;
    }
    
    public final void setCoreBankAccountId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer coreBankAccId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccShortName() {
        return null;
    }
    
    public final void setAccShortName(@org.jetbrains.annotations.NotNull()
    java.lang.String accShortName) {
    }
    
    public MobileBankAcNo() {
        super();
    }
    
    public MobileBankAcNo(@org.jetbrains.annotations.NotNull()
    java.lang.String accShortName) {
        super();
    }
    
    public MobileBankAcNo(int coreBankAccId, @org.jetbrains.annotations.NotNull()
    java.lang.String accShortName) {
        super();
    }
}