package com.netizen.netiworld.adapter.myPoint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0015\u0016B#\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\u000b\u001a\u00020\fH\u0016J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\fH\u0016J\u0018\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\fH\u0016R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/netizen/netiworld/adapter/myPoint/CertificationAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/netizen/netiworld/adapter/myPoint/CertificationAdapter$ViewHolder;", "context", "Landroid/content/Context;", "certificationInfoList", "", "Lcom/netizen/netiworld/model/CertificationInfo;", "listener", "Lcom/netizen/netiworld/adapter/myPoint/CertificationAdapter$OnEditClickListener;", "(Landroid/content/Context;Ljava/util/List;Lcom/netizen/netiworld/adapter/myPoint/CertificationAdapter$OnEditClickListener;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "OnEditClickListener", "ViewHolder", "app_debug"})
public final class CertificationAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.netizen.netiworld.adapter.myPoint.CertificationAdapter.ViewHolder> {
    private final android.content.Context context = null;
    private final java.util.List<com.netizen.netiworld.model.CertificationInfo> certificationInfoList = null;
    private final com.netizen.netiworld.adapter.myPoint.CertificationAdapter.OnEditClickListener listener = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.netizen.netiworld.adapter.myPoint.CertificationAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.adapter.myPoint.CertificationAdapter.ViewHolder holder, int position) {
    }
    
    public CertificationAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<com.netizen.netiworld.model.CertificationInfo> certificationInfoList, @org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.adapter.myPoint.CertificationAdapter.OnEditClickListener listener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lcom/netizen/netiworld/adapter/myPoint/CertificationAdapter$OnEditClickListener;", "", "onEditClick", "", "certificationInfo", "Lcom/netizen/netiworld/model/CertificationInfo;", "app_debug"})
    public static abstract interface OnEditClickListener {
        
        public abstract void onEditClick(@org.jetbrains.annotations.NotNull()
        com.netizen.netiworld.model.CertificationInfo certificationInfo);
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\r"}, d2 = {"Lcom/netizen/netiworld/adapter/myPoint/CertificationAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemBinding", "Lcom/netizen/netiworld/databinding/SingleCertificationLayoutBinding;", "(Lcom/netizen/netiworld/databinding/SingleCertificationLayoutBinding;)V", "getItemBinding", "()Lcom/netizen/netiworld/databinding/SingleCertificationLayoutBinding;", "bind", "", "certificationInfo", "Lcom/netizen/netiworld/model/CertificationInfo;", "position", "", "app_debug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final com.netizen.netiworld.databinding.SingleCertificationLayoutBinding itemBinding = null;
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.netizen.netiworld.model.CertificationInfo certificationInfo, int position) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.netizen.netiworld.databinding.SingleCertificationLayoutBinding getItemBinding() {
            return null;
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        com.netizen.netiworld.databinding.SingleCertificationLayoutBinding itemBinding) {
            super(null);
        }
    }
}