package com.netizen.netiworld.model.UserPoint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u0014B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u0004J\b\u0010\f\u001a\u0004\u0018\u00010\u0007J\r\u0010\r\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u000eJ\u0018\u0010\u000f\u001a\u00020\u00102\u0010\u0010\u0003\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u0004J\u0010\u0010\u0011\u001a\u00020\u00102\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0015\u0010\u0012\u001a\u00020\u00102\b\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u0013R\u001c\u0010\u0003\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/StudentPortalPeriod;", "Ljava/io/Serializable;", "()V", "item", "", "Lcom/netizen/netiworld/model/UserPoint/StudentPortalPeriod$Item;", "message", "", "msgType", "", "Ljava/lang/Integer;", "getItem", "getMessage", "getMsgType", "()Ljava/lang/Integer;", "setItem", "", "setMessage", "setMsgType", "(Ljava/lang/Integer;)V", "Item", "app_debug"})
public final class StudentPortalPeriod implements java.io.Serializable {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "message")
    private java.lang.Object message;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "msgType")
    private java.lang.Integer msgType;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "item")
    private java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalPeriod.Item> item;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.Nullable()
    java.lang.Object message) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getMsgType() {
        return null;
    }
    
    public final void setMsgType(@org.jetbrains.annotations.Nullable()
    java.lang.Integer msgType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalPeriod.Item> getItem() {
        return null;
    }
    
    public final void setItem(@org.jetbrains.annotations.Nullable()
    java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalPeriod.Item> item) {
    }
    
    public StudentPortalPeriod() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u000f\n\u0002\u0010\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000e\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u000fJ\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0011\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u000fJ\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0013\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u000fJ\r\u0010\u0014\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u0015\u001a\u00020\u00162\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0017\u001a\u00020\u00162\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0018J\u0010\u0010\u0019\u001a\u00020\u00162\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u001a\u001a\u00020\u00162\b\u0010\t\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0018J\u0010\u0010\u001b\u001a\u00020\u00162\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u001c\u001a\u00020\u00162\b\u0010\u000b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0018J\u0015\u0010\u001d\u001a\u00020\u00162\b\u0010\f\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0018R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0016\u0010\f\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007\u00a8\u0006\u001e"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/StudentPortalPeriod$Item;", "", "()V", "defaultId", "", "id", "", "Ljava/lang/Integer;", "name", "typeId", "typeName", "viewSerial", "viewStatus", "getDefaultId", "getId", "()Ljava/lang/Integer;", "getName", "getTypeId", "getTypeName", "getViewSerial", "getViewStatus", "setDefaultId", "", "setId", "(Ljava/lang/Integer;)V", "setName", "setTypeId", "setTypeName", "setViewSerial", "setViewStatus", "app_debug"})
    public static final class Item {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "id")
        private java.lang.Integer id;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "name")
        private java.lang.String name;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "typeId")
        private java.lang.Integer typeId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "typeName")
        private java.lang.String typeName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "defaultId")
        private java.lang.String defaultId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "viewStatus")
        private java.lang.Integer viewStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "viewSerial")
        private java.lang.Integer viewSerial;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getId() {
            return null;
        }
        
        public final void setId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer id) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getName() {
            return null;
        }
        
        public final void setName(@org.jetbrains.annotations.Nullable()
        java.lang.String name) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getTypeId() {
            return null;
        }
        
        public final void setTypeId(@org.jetbrains.annotations.Nullable()
        java.lang.Integer typeId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getTypeName() {
            return null;
        }
        
        public final void setTypeName(@org.jetbrains.annotations.Nullable()
        java.lang.String typeName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getDefaultId() {
            return null;
        }
        
        public final void setDefaultId(@org.jetbrains.annotations.Nullable()
        java.lang.String defaultId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getViewStatus() {
            return null;
        }
        
        public final void setViewStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer viewStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getViewSerial() {
            return null;
        }
        
        public final void setViewSerial(@org.jetbrains.annotations.Nullable()
        java.lang.Integer viewSerial) {
        }
        
        public Item() {
            super();
        }
    }
}