package com.netizen.netiworld.viewModel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u008a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\bn\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0018\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\bX\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0002\u00a1\u0002B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u00ba\u0001\u001a\u00030\u00bb\u0001JI\u0010\u00bc\u0001\u001a\u00030\u00bb\u00012\t\u0010\u00bd\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00be\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00bf\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c0\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c1\u0001\u001a\u0004\u0018\u00010\u00062\b\u0010\u00c2\u0001\u001a\u00030\u00c3\u0001J?\u0010\u00c4\u0001\u001a\u00030\u00bb\u00012\t\u0010\u00c5\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c0\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c6\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c7\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c8\u0001\u001a\u0004\u0018\u00010\u0006J`\u0010\u00c9\u0001\u001a\u00030\u00bb\u00012\t\u0010\u00ca\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00cb\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00cc\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00cd\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00ce\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00cf\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00d0\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00d1\u0001\u001a\u0004\u0018\u00010\u0006JJ\u0010\u00d2\u0001\u001a\u00030\u00bb\u00012\t\u0010\u00d3\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00d4\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00d5\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00d6\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00d7\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00d8\u0001\u001a\u0004\u0018\u00010\u0006J?\u0010\u00d9\u0001\u001a\u00030\u00bb\u00012\t\u0010\u00da\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00db\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c0\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c6\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00bf\u0001\u001a\u0004\u0018\u00010\u0006J[\u0010\u00dc\u0001\u001a\u00030\u00bb\u00012\t\u0010\u00bd\u0001\u001a\u0004\u0018\u00010\u00062\n\u0010\u00dd\u0001\u001a\u0005\u0018\u00010\u00c3\u00012\t\u0010\u00be\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00bf\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c0\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c1\u0001\u001a\u0004\u0018\u00010\u00062\b\u0010\u00c2\u0001\u001a\u00030\u00c3\u0001\u00a2\u0006\u0003\u0010\u00de\u0001JU\u0010\u00df\u0001\u001a\u00030\u00bb\u00012\t\u0010\u00e0\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00e1\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00e2\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00e3\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00e4\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00e5\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00e6\u0001\u001a\u0004\u0018\u00010\u0006J?\u0010\u00e7\u0001\u001a\u00030\u00bb\u00012\t\u0010\u00c5\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c0\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c6\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c7\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c8\u0001\u001a\u0004\u0018\u00010\u0006J`\u0010\u00e8\u0001\u001a\u00030\u00bb\u00012\t\u0010\u00ca\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00cb\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00cc\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00cd\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00ce\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00cf\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00d0\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00d1\u0001\u001a\u0004\u0018\u00010\u0006J\u0097\u0001\u0010\u00e9\u0001\u001a\u00030\u00bb\u00012\t\u0010\u00e0\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00ea\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00eb\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00ec\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00ed\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00ee\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00ef\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00f0\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00f1\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00f2\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00f3\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00f4\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00f5\u0001\u001a\u0004\u0018\u00010\u0006JJ\u0010\u00f6\u0001\u001a\u00030\u00bb\u00012\t\u0010\u00d3\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00d4\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00d5\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00d6\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00d7\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00d8\u0001\u001a\u0004\u0018\u00010\u0006JI\u0010\u00f7\u0001\u001a\u00030\u00bb\u00012\b\u0010\u00f8\u0001\u001a\u00030\u00c3\u00012\t\u0010\u00da\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00db\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c0\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00c6\u0001\u001a\u0004\u0018\u00010\u00062\t\u0010\u00bf\u0001\u001a\u0004\u0018\u00010\u0006J\b\u0010\u00f9\u0001\u001a\u00030\u00bb\u0001J\u0013\u0010\u00fa\u0001\u001a\u00030\u00bb\u00012\t\u0010\u00fb\u0001\u001a\u0004\u0018\u00010\u0006J\u0013\u0010\u00fc\u0001\u001a\u00030\u00bb\u00012\t\u0010\u00fd\u0001\u001a\u0004\u0018\u00010\u0006J\b\u0010\u00fe\u0001\u001a\u00030\u00bb\u0001J\b\u0010\u00ff\u0001\u001a\u00030\u00bb\u0001J\b\u0010\u0080\u0002\u001a\u00030\u00bb\u0001J\u0013\u0010\u0081\u0002\u001a\u00030\u00bb\u00012\t\u0010\u0082\u0002\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0083\u0002\u001a\u00030\u00bb\u0001J\u0013\u0010\u0084\u0002\u001a\u00030\u00bb\u00012\t\u0010\u0082\u0002\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0085\u0002\u001a\u00030\u00bb\u0001J\u0013\u0010\u0086\u0002\u001a\u00030\u00bb\u00012\t\u0010\u0082\u0002\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0087\u0002\u001a\u00030\u00bb\u0001J\u0013\u0010\u0088\u0002\u001a\u00030\u00bb\u00012\t\u0010\u0089\u0002\u001a\u0004\u0018\u00010\u0006J\b\u0010\u008a\u0002\u001a\u00030\u00bb\u0001J\b\u0010\u008b\u0002\u001a\u00030\u00bb\u0001J\u0013\u0010\u008c\u0002\u001a\u00030\u00bb\u00012\t\u0010\u008d\u0002\u001a\u0004\u0018\u00010\u0006J\b\u0010\u008e\u0002\u001a\u00030\u00bb\u0001J\u0013\u0010\u008f\u0002\u001a\u00030\u00bb\u00012\t\u0010\u0090\u0002\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0091\u0002\u001a\u00030\u00bb\u0001J\u0011\u0010\u0092\u0002\u001a\u00030\u00bb\u00012\u0007\u0010\u0093\u0002\u001a\u00020\u0006J\b\u0010\u0094\u0002\u001a\u00030\u00bb\u0001J\b\u0010\u0095\u0002\u001a\u00030\u00bb\u0001J\b\u0010\u0096\u0002\u001a\u00030\u00bb\u0001J\u0013\u0010\u0097\u0002\u001a\u00030\u00bb\u00012\t\u0010\u0098\u0002\u001a\u0004\u0018\u00010\u0006J\u0013\u0010\u0099\u0002\u001a\u00030\u00bb\u00012\t\u0010\u009a\u0002\u001a\u0004\u0018\u00010\u0006J\b\u0010\u009b\u0002\u001a\u00030\u00bb\u0001J)\u0010\u009f\u0001\u001a\u0014\u0012\u0004\u0012\u00020\u00060\u009d\u0001j\t\u0012\u0004\u0012\u00020\u0006`\u009c\u00022\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002J)\u0010\u00a2\u0001\u001a\u0014\u0012\u0004\u0012\u00020\u00060\u009d\u0001j\t\u0012\u0004\u0012\u00020\u0006`\u009c\u00022\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002J)\u0010\u00a4\u0001\u001a\u0014\u0012\u0004\u0012\u00020\u00060\u009d\u0001j\t\u0012\u0004\u0012\u00020\u0006`\u009c\u00022\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002J)\u0010\u00a6\u0001\u001a\u0014\u0012\u0004\u0012\u00020\u00060\u009d\u0001j\t\u0012\u0004\u0012\u00020\u0006`\u009c\u00022\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002J)\u0010\u00a8\u0001\u001a\u0014\u0012\u0004\u0012\u00020\u00060\u009d\u0001j\t\u0012\u0004\u0012\u00020\u0006`\u009c\u00022\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002J)\u0010\u00aa\u0001\u001a\u0014\u0012\u0004\u0012\u00020\u00060\u009d\u0001j\t\u0012\u0004\u0012\u00020\u0006`\u009c\u00022\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002J*\u0010\u009d\u0002\u001a\u0014\u0012\u0004\u0012\u00020\u00060\u009d\u0001j\t\u0012\u0004\u0012\u00020\u0006`\u009c\u00022\r\u0010\u0089\u0001\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002J*\u0010\u00ae\u0001\u001a\u0014\u0012\u0004\u0012\u00020\u00060\u009d\u0001j\t\u0012\u0004\u0012\u00020\u0006`\u009c\u00022\r\u0010\u008b\u0001\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002J*\u0010\u00b0\u0001\u001a\u0014\u0012\u0004\u0012\u00020\u00060\u009d\u0001j\t\u0012\u0004\u0012\u00020\u0006`\u009c\u00022\r\u0010\u0098\u0001\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002J*\u0010\u00b2\u0001\u001a\u0014\u0012\u0004\u0012\u00020\u00060\u009d\u0001j\t\u0012\u0004\u0012\u00020\u0006`\u009c\u00022\r\u0010\u009a\u0001\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002J*\u0010\u00b4\u0001\u001a\u0014\u0012\u0004\u0012\u00020\u00060\u009d\u0001j\t\u0012\u0004\u0012\u00020\u0006`\u009c\u00022\r\u0010\u00b8\u0001\u001a\b\u0012\u0004\u0012\u00020\n0\tH\u0002J\b\u0010\u009e\u0002\u001a\u00030\u00bb\u0001J\b\u0010\u009f\u0002\u001a\u00030\u00bb\u0001J\u0013\u0010\u00a0\u0002\u001a\u00030\u00bb\u00012\t\u0010\u0082\u0002\u001a\u0004\u0018\u00010\u0006R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\t0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0013\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0017\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190\t0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0010R\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001c\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001e0\t0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0010R \u0010 \u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u0010\"\u0004\b\"\u0010#R \u0010$\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u0010\"\u0004\b%\u0010#R \u0010&\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u0010\"\u0004\b\'\u0010#R \u0010(\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0010\"\u0004\b)\u0010#R \u0010*\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\u0010\"\u0004\b+\u0010#R \u0010,\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\u0010\"\u0004\b-\u0010#R \u0010.\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u0010\"\u0004\b/\u0010#R \u00100\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b0\u0010\u0010\"\u0004\b1\u0010#R \u00102\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u0010\u0010\"\u0004\b3\u0010#R \u00104\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\u0010\"\u0004\b5\u0010#R \u00106\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b6\u0010\u0010\"\u0004\b7\u0010#R \u00108\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b8\u0010\u0010\"\u0004\b9\u0010#R \u0010:\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010\u0010\"\u0004\b;\u0010#R \u0010<\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b<\u0010\u0010\"\u0004\b=\u0010#R \u0010>\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b>\u0010\u0010\"\u0004\b?\u0010#R \u0010@\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010\u0010\"\u0004\bA\u0010#R \u0010B\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bB\u0010\u0010\"\u0004\bC\u0010#R \u0010D\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010\u0010\"\u0004\bE\u0010#R \u0010F\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bF\u0010\u0010\"\u0004\bG\u0010#R \u0010H\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bH\u0010\u0010\"\u0004\bI\u0010#R \u0010J\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bJ\u0010\u0010\"\u0004\bK\u0010#R \u0010L\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bL\u0010\u0010\"\u0004\bM\u0010#R \u0010N\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bN\u0010\u0010\"\u0004\bO\u0010#R \u0010P\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bP\u0010\u0010\"\u0004\bQ\u0010#R \u0010R\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bR\u0010\u0010\"\u0004\bS\u0010#R \u0010T\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bT\u0010\u0010\"\u0004\bU\u0010#R \u0010V\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bV\u0010\u0010\"\u0004\bW\u0010#R \u0010X\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bX\u0010\u0010\"\u0004\bY\u0010#R \u0010Z\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bZ\u0010\u0010\"\u0004\b[\u0010#R \u0010\\\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\\\u0010\u0010\"\u0004\b]\u0010#R \u0010^\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b^\u0010\u0010\"\u0004\b_\u0010#R \u0010`\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b`\u0010\u0010\"\u0004\ba\u0010#R \u0010b\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bb\u0010\u0010\"\u0004\bc\u0010#R \u0010d\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bd\u0010\u0010\"\u0004\be\u0010#R \u0010f\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bf\u0010\u0010\"\u0004\bg\u0010#R \u0010h\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bh\u0010\u0010\"\u0004\bi\u0010#R \u0010j\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bj\u0010\u0010\"\u0004\bk\u0010#R \u0010l\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bl\u0010\u0010\"\u0004\bm\u0010#R \u0010n\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bn\u0010\u0010\"\u0004\bo\u0010#R \u0010p\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bp\u0010\u0010\"\u0004\bq\u0010#R \u0010r\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\br\u0010\u0010\"\u0004\bs\u0010#R \u0010t\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bt\u0010\u0010\"\u0004\bu\u0010#R \u0010v\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bv\u0010\u0010\"\u0004\bw\u0010#R \u0010x\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bx\u0010\u0010\"\u0004\by\u0010#R \u0010z\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bz\u0010\u0010\"\u0004\b{\u0010#R \u0010|\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b|\u0010\u0010\"\u0004\b}\u0010#R \u0010~\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b~\u0010\u0010\"\u0004\b\u007f\u0010#R#\u0010\u0080\u0001\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0080\u0001\u0010\u0010\"\u0005\b\u0081\u0001\u0010#R#\u0010\u0082\u0001\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0082\u0001\u0010\u0010\"\u0005\b\u0083\u0001\u0010#R#\u0010\u0084\u0001\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0084\u0001\u0010\u0010\"\u0005\b\u0085\u0001\u0010#R#\u0010\u0086\u0001\u001a\b\u0012\u0004\u0012\u00020!0\bX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0086\u0001\u0010\u0010\"\u0005\b\u0087\u0001\u0010#R\u0011\u0010\u0088\u0001\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0089\u0001\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u008a\u0001\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u008b\u0001\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R#\u0010\u008c\u0001\u001a\b\u0012\u0004\u0012\u00020\u00060\bX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u008d\u0001\u0010\u0010\"\u0005\b\u008e\u0001\u0010#R$\u0010\u008f\u0001\u001a\t\u0012\u0005\u0012\u00030\u0090\u00010\bX\u0086\u000e\u00a2\u0006\u0010\n\u0000\u001a\u0005\b\u0091\u0001\u0010\u0010\"\u0005\b\u0092\u0001\u0010#R\u0010\u0010\u0093\u0001\u001a\u00030\u0094\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0095\u0001\u001a\u000f\u0012\u000b\u0012\t\u0012\u0005\u0012\u00030\u0096\u00010\t0\b\u00a2\u0006\t\n\u0000\u001a\u0005\b\u0097\u0001\u0010\u0010R\u001b\u0010\u0098\u0001\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0099\u0001\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u009a\u0001\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u009b\u0001\u001a \u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0006 \u009e\u0001*\u000b\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u009d\u00010\u009d\u00010\u009c\u0001\u00a2\u0006\n\n\u0000\u001a\u0006\b\u009f\u0001\u0010\u00a0\u0001R2\u0010\u00a1\u0001\u001a \u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0006 \u009e\u0001*\u000b\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u009d\u00010\u009d\u00010\u009c\u0001\u00a2\u0006\n\n\u0000\u001a\u0006\b\u00a2\u0001\u0010\u00a0\u0001R2\u0010\u00a3\u0001\u001a \u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0006 \u009e\u0001*\u000b\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u009d\u00010\u009d\u00010\u009c\u0001\u00a2\u0006\n\n\u0000\u001a\u0006\b\u00a4\u0001\u0010\u00a0\u0001R2\u0010\u00a5\u0001\u001a \u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0006 \u009e\u0001*\u000b\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u009d\u00010\u009d\u00010\u009c\u0001\u00a2\u0006\n\n\u0000\u001a\u0006\b\u00a6\u0001\u0010\u00a0\u0001R2\u0010\u00a7\u0001\u001a \u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0006 \u009e\u0001*\u000b\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u009d\u00010\u009d\u00010\u009c\u0001\u00a2\u0006\n\n\u0000\u001a\u0006\b\u00a8\u0001\u0010\u00a0\u0001R2\u0010\u00a9\u0001\u001a \u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0006 \u009e\u0001*\u000b\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u009d\u00010\u009d\u00010\u009c\u0001\u00a2\u0006\n\n\u0000\u001a\u0006\b\u00aa\u0001\u0010\u00a0\u0001R2\u0010\u00ab\u0001\u001a \u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0006 \u009e\u0001*\u000b\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u009d\u00010\u009d\u00010\u009c\u0001\u00a2\u0006\n\n\u0000\u001a\u0006\b\u00ac\u0001\u0010\u00a0\u0001R2\u0010\u00ad\u0001\u001a \u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0006 \u009e\u0001*\u000b\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u009d\u00010\u009d\u00010\u009c\u0001\u00a2\u0006\n\n\u0000\u001a\u0006\b\u00ae\u0001\u0010\u00a0\u0001R2\u0010\u00af\u0001\u001a \u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0006 \u009e\u0001*\u000b\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u009d\u00010\u009d\u00010\u009c\u0001\u00a2\u0006\n\n\u0000\u001a\u0006\b\u00b0\u0001\u0010\u00a0\u0001R2\u0010\u00b1\u0001\u001a \u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0006 \u009e\u0001*\u000b\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u009d\u00010\u009d\u00010\u009c\u0001\u00a2\u0006\n\n\u0000\u001a\u0006\b\u00b2\u0001\u0010\u00a0\u0001R2\u0010\u00b3\u0001\u001a \u0012\u001b\u0012\u0019\u0012\u0004\u0012\u00020\u0006 \u009e\u0001*\u000b\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u009d\u00010\u009d\u00010\u009c\u0001\u00a2\u0006\n\n\u0000\u001a\u0006\b\u00b4\u0001\u0010\u00a0\u0001R \u0010\u00b5\u0001\u001a\u000f\u0012\u000b\u0012\t\u0012\u0005\u0012\u00030\u00b6\u00010\t0\b\u00a2\u0006\t\n\u0000\u001a\u0005\b\u00b7\u0001\u0010\u0010R\u001b\u0010\u00b8\u0001\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u00b9\u0001\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u00a2\u0002"}, d2 = {"Lcom/netizen/netiworld/viewModel/ProfileViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "bloodGroupId", "", "bloodGroupList", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/netizen/netiworld/model/DataByTypeDefaultCode;", "boardId", "boardList", "certificationInfoList", "Lcom/netizen/netiworld/model/CertificationInfo;", "getCertificationInfoList", "()Landroidx/lifecycle/MutableLiveData;", "coreCategoryID", "countryId", "countryList", "districtId", "districtList", "divisionId", "divisionList", "educationInfoList", "Lcom/netizen/netiworld/model/EducationInfo;", "getEducationInfoList", "educationLevelId", "educationLevelList", "experienceInfoList", "Lcom/netizen/netiworld/model/ExperienceInfo;", "getExperienceInfoList", "isAchievementEmpty", "", "setAchievementEmpty", "(Landroidx/lifecycle/MutableLiveData;)V", "isAddressDetailsEmpty", "setAddressDetailsEmpty", "isBirthCertificationEmpty", "setBirthCertificationEmpty", "isBirthDayEmpty", "setBirthDayEmpty", "isBloodEmpty", "setBloodEmpty", "isBloodGroupEmpty", "setBloodGroupEmpty", "isBoardEmpty", "setBoardEmpty", "isCertificationNameEmpty", "setCertificationNameEmpty", "isCompanyBusinessEmpty", "setCompanyBusinessEmpty", "isCompanyLocationEmpty", "setCompanyLocationEmpty", "isCompanyNameEmpty", "setCompanyNameEmpty", "isCountryEmpty", "setCountryEmpty", "isDateEmpty", "setDateEmpty", "isDesignationNameEmpty", "setDesignationNameEmpty", "isDistrictEmpty", "setDistrictEmpty", "isDivisionEmpty", "setDivisionEmpty", "isDurationEmpty", "setDurationEmpty", "isEducationLevelEmpty", "setEducationLevelEmpty", "isEmploymentEndEmpty", "setEmploymentEndEmpty", "isEmploymentStartEmpty", "setEmploymentStartEmpty", "isExamEmpty", "setExamEmpty", "isFatherNameEmpty", "setFatherNameEmpty", "isGenderEmpty", "setGenderEmpty", "isInstituteEmpty", "setInstituteEmpty", "isLatitudeEmpty", "setLatitudeEmpty", "isLocationEmpty", "setLocationEmpty", "isLongitudeEmpty", "setLongitudeEmpty", "isMajorEmpty", "setMajorEmpty", "isMaritalStatusEmpty", "setMaritalStatusEmpty", "isMobileEmpty", "setMobileEmpty", "isMotherNameEmpty", "setMotherNameEmpty", "isNationalIdEmpty", "setNationalIdEmpty", "isNationalityEmpty", "setNationalityEmpty", "isNetiIdEmpty", "setNetiIdEmpty", "isNumberOfChildEmpty", "setNumberOfChildEmpty", "isPassingYearEmpty", "setPassingYearEmpty", "isPassportEmpty", "setPassportEmpty", "isReferenceAddressEmpty", "setReferenceAddressEmpty", "isReferenceDesignationEmpty", "setReferenceDesignationEmpty", "isReferenceEmailEmpty", "setReferenceEmailEmpty", "isReferenceMobileEmpty", "setReferenceMobileEmpty", "isReferenceNameEmpty", "setReferenceNameEmpty", "isReferenceOrganizationNameEmpty", "setReferenceOrganizationNameEmpty", "isRelationEmpty", "setRelationEmpty", "isResponsibilitiesDetailsEmpty", "setResponsibilitiesDetailsEmpty", "isResultEmpty", "setResultEmpty", "isSerialEmpty", "setSerialEmpty", "isTopicEmpty", "setTopicEmpty", "isTrainingTitleEmpty", "setTrainingTitleEmpty", "isUpazillaEmpty", "setUpazillaEmpty", "isWorkingDepartmentEmpty", "setWorkingDepartmentEmpty", "majorSubjectId", "majorSubjectList", "passingYearId", "passingYearList", "photoFileContent", "getPhotoFileContent", "setPhotoFileContent", "profileInformation", "Lcom/netizen/netiworld/model/ProfileInformation;", "getProfileInformation", "setProfileInformation", "profileRepository", "Lcom/netizen/netiworld/repository/ProfileRepository;", "referenceInfoList", "Lcom/netizen/netiworld/model/ReferenceInfo;", "getReferenceInfoList", "relationList", "resultId", "resultList", "tempBloodGroupList", "Landroidx/lifecycle/LiveData;", "Ljava/util/ArrayList;", "kotlin.jvm.PlatformType", "getTempBloodGroupList", "()Landroidx/lifecycle/LiveData;", "tempBoardList", "getTempBoardList", "tempCountryList", "getTempCountryList", "tempDistrictList", "getTempDistrictList", "tempDivisionList", "getTempDivisionList", "tempEducationLevelList", "getTempEducationLevelList", "tempMajorSubjectsList", "getTempMajorSubjectsList", "tempPassingYearList", "getTempPassingYearList", "tempRelationList", "getTempRelationList", "tempResultList", "getTempResultList", "tempUpazillaList", "getTempUpazillaList", "trainingInfoList", "Lcom/netizen/netiworld/model/TrainingInfo;", "getTrainingInfoList", "upazillaList", "upozillaId", "addPoint", "", "checkAcademicEducationInfo", "serial", "examTitle", "duration", "instituteName", "achievement", "foreignStatus", "", "checkCertificationInfo", "certificationName", "location", "date", "courseDuration", "checkExperienceInfo", "companyName", "companyBusiness", "designationName", "workingDepartment", "companyLocation", "employmentStart", "employmentEnd", "responsibilityDetails", "checkReferenceInfo", "referenceName", "referenceDesignation", "organizationName", "referenceMobile", "referenceEmail", "referenceAddress", "checkTrainingInfo", "title", "topic", "checkUpdateAcademicEducationInfo", "educationInfoId", "(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V", "checkUpdateAddressInfo", "netiId", "addressDetails", "divisionName", "districtName", "upazillaName", "latitude", "longitude", "checkUpdateCertificationInfo", "checkUpdateExperienceInfo", "checkUpdatePersonalInfo", "fathersName", "mothersName", "mobileNo", "birthday", "gender", "bloodGroup", "maritalStatusStatus", "noOfChild", "nationality", "nationalID", "passport", "birthCertificate", "checkUpdateReferenceInfo", "checkUpdateTrainingInfo", "trainingInfoId", "getBloodGroup", "getBloodGroupId", "bloodName", "getBoardId", "boardName", "getBoards", "getCertificationInfo", "getCountry", "getCountryId", "countryName", "getDistrict", "getDistrictId", "getDivision", "getDivisionId", "getEducationInfo", "getEducationLevelId", "educationLevelName", "getEducationLevels", "getExperienceInfo", "getMajorSubjectId", "majorSubjectName", "getMajorSubjects", "getPassingYearId", "passingYearName", "getPassingYears", "getProfileImage", "imagePath", "getProfileInfo", "getReferenceInfo", "getRelation", "getRelationId", "relationName", "getResultId", "resultName", "getResults", "Lkotlin/collections/ArrayList;", "getTempMajorSubjectList", "getTrainingInfo", "getUpazilla", "getUpazillaId", "ProfileViewModelFactory", "app_debug"})
public final class ProfileViewModel extends androidx.lifecycle.AndroidViewModel {
    private final com.netizen.netiworld.repository.ProfileRepository profileRepository = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.ProfileInformation> profileInformation;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> photoFileContent;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.EducationInfo>> educationInfoList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TrainingInfo>> trainingInfoList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.CertificationInfo>> certificationInfoList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.ExperienceInfo>> experienceInfoList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.ReferenceInfo>> referenceInfoList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> educationLevelList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> majorSubjectList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> resultList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> passingYearList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> boardList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> countryList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> relationList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> bloodGroupList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> divisionList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> districtList = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> upazillaList = null;
    private java.lang.String educationLevelId;
    private java.lang.String majorSubjectId;
    private java.lang.String resultId;
    private java.lang.String passingYearId;
    private java.lang.String boardId;
    private java.lang.String countryId;
    private java.lang.String coreCategoryID;
    private java.lang.String bloodGroupId;
    private java.lang.String divisionId;
    private java.lang.String districtId;
    private java.lang.String upozillaId;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isSerialEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isEducationLevelEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isExamEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMajorEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isResultEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPassingYearEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDurationEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBoardEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isInstituteEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAchievementEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isTrainingTitleEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isTopicEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isLocationEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isCountryEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isCertificationNameEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDateEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReferenceNameEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReferenceDesignationEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReferenceOrganizationNameEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReferenceMobileEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReferenceEmailEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReferenceAddressEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isRelationEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isCompanyNameEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isCompanyBusinessEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDesignationNameEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isWorkingDepartmentEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isCompanyLocationEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isEmploymentStartEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isEmploymentEndEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isResponsibilitiesDetailsEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isFatherNameEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMotherNameEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMobileEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBirthDayEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isGenderEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBloodEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMaritalStatusEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isNumberOfChildEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isNationalityEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isNationalIdEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPassportEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBirthCertificationEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBloodGroupEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isNetiIdEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAddressDetailsEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDivisionEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDistrictEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUpazillaEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isLatitudeEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isLongitudeEmpty;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempEducationLevelList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempMajorSubjectsList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempResultList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempPassingYearList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempBoardList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempCountryList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempRelationList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempBloodGroupList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempDivisionList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempDistrictList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempUpazillaList = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.ProfileInformation> getProfileInformation() {
        return null;
    }
    
    public final void setProfileInformation(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.ProfileInformation> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getPhotoFileContent() {
        return null;
    }
    
    public final void setPhotoFileContent(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.EducationInfo>> getEducationInfoList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TrainingInfo>> getTrainingInfoList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.CertificationInfo>> getCertificationInfoList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.ExperienceInfo>> getExperienceInfoList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.ReferenceInfo>> getReferenceInfoList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isSerialEmpty() {
        return null;
    }
    
    public final void setSerialEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isEducationLevelEmpty() {
        return null;
    }
    
    public final void setEducationLevelEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isExamEmpty() {
        return null;
    }
    
    public final void setExamEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMajorEmpty() {
        return null;
    }
    
    public final void setMajorEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isResultEmpty() {
        return null;
    }
    
    public final void setResultEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPassingYearEmpty() {
        return null;
    }
    
    public final void setPassingYearEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDurationEmpty() {
        return null;
    }
    
    public final void setDurationEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBoardEmpty() {
        return null;
    }
    
    public final void setBoardEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isInstituteEmpty() {
        return null;
    }
    
    public final void setInstituteEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAchievementEmpty() {
        return null;
    }
    
    public final void setAchievementEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isTrainingTitleEmpty() {
        return null;
    }
    
    public final void setTrainingTitleEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isTopicEmpty() {
        return null;
    }
    
    public final void setTopicEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isLocationEmpty() {
        return null;
    }
    
    public final void setLocationEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isCountryEmpty() {
        return null;
    }
    
    public final void setCountryEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isCertificationNameEmpty() {
        return null;
    }
    
    public final void setCertificationNameEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDateEmpty() {
        return null;
    }
    
    public final void setDateEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReferenceNameEmpty() {
        return null;
    }
    
    public final void setReferenceNameEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReferenceDesignationEmpty() {
        return null;
    }
    
    public final void setReferenceDesignationEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReferenceOrganizationNameEmpty() {
        return null;
    }
    
    public final void setReferenceOrganizationNameEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReferenceMobileEmpty() {
        return null;
    }
    
    public final void setReferenceMobileEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReferenceEmailEmpty() {
        return null;
    }
    
    public final void setReferenceEmailEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isReferenceAddressEmpty() {
        return null;
    }
    
    public final void setReferenceAddressEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isRelationEmpty() {
        return null;
    }
    
    public final void setRelationEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isCompanyNameEmpty() {
        return null;
    }
    
    public final void setCompanyNameEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isCompanyBusinessEmpty() {
        return null;
    }
    
    public final void setCompanyBusinessEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDesignationNameEmpty() {
        return null;
    }
    
    public final void setDesignationNameEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isWorkingDepartmentEmpty() {
        return null;
    }
    
    public final void setWorkingDepartmentEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isCompanyLocationEmpty() {
        return null;
    }
    
    public final void setCompanyLocationEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isEmploymentStartEmpty() {
        return null;
    }
    
    public final void setEmploymentStartEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isEmploymentEndEmpty() {
        return null;
    }
    
    public final void setEmploymentEndEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isResponsibilitiesDetailsEmpty() {
        return null;
    }
    
    public final void setResponsibilitiesDetailsEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isFatherNameEmpty() {
        return null;
    }
    
    public final void setFatherNameEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMotherNameEmpty() {
        return null;
    }
    
    public final void setMotherNameEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMobileEmpty() {
        return null;
    }
    
    public final void setMobileEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBirthDayEmpty() {
        return null;
    }
    
    public final void setBirthDayEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isGenderEmpty() {
        return null;
    }
    
    public final void setGenderEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBloodEmpty() {
        return null;
    }
    
    public final void setBloodEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMaritalStatusEmpty() {
        return null;
    }
    
    public final void setMaritalStatusEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isNumberOfChildEmpty() {
        return null;
    }
    
    public final void setNumberOfChildEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isNationalityEmpty() {
        return null;
    }
    
    public final void setNationalityEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isNationalIdEmpty() {
        return null;
    }
    
    public final void setNationalIdEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPassportEmpty() {
        return null;
    }
    
    public final void setPassportEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBirthCertificationEmpty() {
        return null;
    }
    
    public final void setBirthCertificationEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isBloodGroupEmpty() {
        return null;
    }
    
    public final void setBloodGroupEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isNetiIdEmpty() {
        return null;
    }
    
    public final void setNetiIdEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isAddressDetailsEmpty() {
        return null;
    }
    
    public final void setAddressDetailsEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDivisionEmpty() {
        return null;
    }
    
    public final void setDivisionEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isDistrictEmpty() {
        return null;
    }
    
    public final void setDistrictEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isUpazillaEmpty() {
        return null;
    }
    
    public final void setUpazillaEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isLatitudeEmpty() {
        return null;
    }
    
    public final void setLatitudeEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isLongitudeEmpty() {
        return null;
    }
    
    public final void setLongitudeEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempEducationLevelList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempMajorSubjectsList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempResultList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempPassingYearList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempBoardList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempCountryList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempRelationList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempBloodGroupList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempDivisionList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempDistrictList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempUpazillaList() {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempDivisionList(java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode> divisionList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempDistrictList(java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode> districtList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempUpazillaList(java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode> upazillaList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempBloodGroupList(java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode> bloodGroupList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempEducationLevelList(java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode> educationLevelList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempMajorSubjectList(java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode> majorSubjectList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempResultList(java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode> resultList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempPassingYearList(java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode> passingYearList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempBoardList(java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode> boardList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempCountryList(java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode> countryList) {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempRelationList(java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode> relationList) {
        return null;
    }
    
    public final void getBloodGroupId(@org.jetbrains.annotations.Nullable()
    java.lang.String bloodName) {
    }
    
    public final void getEducationLevelId(@org.jetbrains.annotations.Nullable()
    java.lang.String educationLevelName) {
    }
    
    public final void getMajorSubjectId(@org.jetbrains.annotations.Nullable()
    java.lang.String majorSubjectName) {
    }
    
    public final void getResultId(@org.jetbrains.annotations.Nullable()
    java.lang.String resultName) {
    }
    
    public final void getPassingYearId(@org.jetbrains.annotations.Nullable()
    java.lang.String passingYearName) {
    }
    
    public final void getBoardId(@org.jetbrains.annotations.Nullable()
    java.lang.String boardName) {
    }
    
    public final void getCountryId(@org.jetbrains.annotations.Nullable()
    java.lang.String countryName) {
    }
    
    public final void getDivisionId(@org.jetbrains.annotations.Nullable()
    java.lang.String countryName) {
    }
    
    public final void getDistrictId(@org.jetbrains.annotations.Nullable()
    java.lang.String countryName) {
    }
    
    public final void getUpazillaId(@org.jetbrains.annotations.Nullable()
    java.lang.String countryName) {
    }
    
    public final void getRelationId(@org.jetbrains.annotations.Nullable()
    java.lang.String relationName) {
    }
    
    public final void getProfileInfo() {
    }
    
    public final void getProfileImage(@org.jetbrains.annotations.NotNull()
    java.lang.String imagePath) {
    }
    
    public final void getEducationInfo() {
    }
    
    public final void getTrainingInfo() {
    }
    
    public final void getCertificationInfo() {
    }
    
    public final void getExperienceInfo() {
    }
    
    public final void getReferenceInfo() {
    }
    
    public final void getBloodGroup() {
    }
    
    public final void getEducationLevels() {
    }
    
    public final void getMajorSubjects() {
    }
    
    public final void getResults() {
    }
    
    public final void getPassingYears() {
    }
    
    public final void getBoards() {
    }
    
    public final void getCountry() {
    }
    
    public final void getDivision() {
    }
    
    public final void getDistrict() {
    }
    
    public final void getUpazilla() {
    }
    
    public final void addPoint() {
    }
    
    public final void getRelation() {
    }
    
    public final void checkAcademicEducationInfo(@org.jetbrains.annotations.Nullable()
    java.lang.String serial, @org.jetbrains.annotations.Nullable()
    java.lang.String examTitle, @org.jetbrains.annotations.Nullable()
    java.lang.String duration, @org.jetbrains.annotations.Nullable()
    java.lang.String instituteName, @org.jetbrains.annotations.Nullable()
    java.lang.String achievement, int foreignStatus) {
    }
    
    public final void checkTrainingInfo(@org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String topic, @org.jetbrains.annotations.Nullable()
    java.lang.String instituteName, @org.jetbrains.annotations.Nullable()
    java.lang.String location, @org.jetbrains.annotations.Nullable()
    java.lang.String duration) {
    }
    
    public final void checkCertificationInfo(@org.jetbrains.annotations.Nullable()
    java.lang.String certificationName, @org.jetbrains.annotations.Nullable()
    java.lang.String instituteName, @org.jetbrains.annotations.Nullable()
    java.lang.String location, @org.jetbrains.annotations.Nullable()
    java.lang.String date, @org.jetbrains.annotations.Nullable()
    java.lang.String courseDuration) {
    }
    
    public final void checkExperienceInfo(@org.jetbrains.annotations.Nullable()
    java.lang.String companyName, @org.jetbrains.annotations.Nullable()
    java.lang.String companyBusiness, @org.jetbrains.annotations.Nullable()
    java.lang.String designationName, @org.jetbrains.annotations.Nullable()
    java.lang.String workingDepartment, @org.jetbrains.annotations.Nullable()
    java.lang.String companyLocation, @org.jetbrains.annotations.Nullable()
    java.lang.String employmentStart, @org.jetbrains.annotations.Nullable()
    java.lang.String employmentEnd, @org.jetbrains.annotations.Nullable()
    java.lang.String responsibilityDetails) {
    }
    
    public final void checkReferenceInfo(@org.jetbrains.annotations.Nullable()
    java.lang.String referenceName, @org.jetbrains.annotations.Nullable()
    java.lang.String referenceDesignation, @org.jetbrains.annotations.Nullable()
    java.lang.String organizationName, @org.jetbrains.annotations.Nullable()
    java.lang.String referenceMobile, @org.jetbrains.annotations.Nullable()
    java.lang.String referenceEmail, @org.jetbrains.annotations.Nullable()
    java.lang.String referenceAddress) {
    }
    
    public final void checkUpdateAcademicEducationInfo(@org.jetbrains.annotations.Nullable()
    java.lang.String serial, @org.jetbrains.annotations.Nullable()
    java.lang.Integer educationInfoId, @org.jetbrains.annotations.Nullable()
    java.lang.String examTitle, @org.jetbrains.annotations.Nullable()
    java.lang.String duration, @org.jetbrains.annotations.Nullable()
    java.lang.String instituteName, @org.jetbrains.annotations.Nullable()
    java.lang.String achievement, int foreignStatus) {
    }
    
    public final void checkUpdateTrainingInfo(int trainingInfoId, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String topic, @org.jetbrains.annotations.Nullable()
    java.lang.String instituteName, @org.jetbrains.annotations.Nullable()
    java.lang.String location, @org.jetbrains.annotations.Nullable()
    java.lang.String duration) {
    }
    
    public final void checkUpdateCertificationInfo(@org.jetbrains.annotations.Nullable()
    java.lang.String certificationName, @org.jetbrains.annotations.Nullable()
    java.lang.String instituteName, @org.jetbrains.annotations.Nullable()
    java.lang.String location, @org.jetbrains.annotations.Nullable()
    java.lang.String date, @org.jetbrains.annotations.Nullable()
    java.lang.String courseDuration) {
    }
    
    public final void checkUpdateExperienceInfo(@org.jetbrains.annotations.Nullable()
    java.lang.String companyName, @org.jetbrains.annotations.Nullable()
    java.lang.String companyBusiness, @org.jetbrains.annotations.Nullable()
    java.lang.String designationName, @org.jetbrains.annotations.Nullable()
    java.lang.String workingDepartment, @org.jetbrains.annotations.Nullable()
    java.lang.String companyLocation, @org.jetbrains.annotations.Nullable()
    java.lang.String employmentStart, @org.jetbrains.annotations.Nullable()
    java.lang.String employmentEnd, @org.jetbrains.annotations.Nullable()
    java.lang.String responsibilityDetails) {
    }
    
    public final void checkUpdateReferenceInfo(@org.jetbrains.annotations.Nullable()
    java.lang.String referenceName, @org.jetbrains.annotations.Nullable()
    java.lang.String referenceDesignation, @org.jetbrains.annotations.Nullable()
    java.lang.String organizationName, @org.jetbrains.annotations.Nullable()
    java.lang.String referenceMobile, @org.jetbrains.annotations.Nullable()
    java.lang.String referenceEmail, @org.jetbrains.annotations.Nullable()
    java.lang.String referenceAddress) {
    }
    
    public final void checkUpdatePersonalInfo(@org.jetbrains.annotations.Nullable()
    java.lang.String netiId, @org.jetbrains.annotations.Nullable()
    java.lang.String fathersName, @org.jetbrains.annotations.Nullable()
    java.lang.String mothersName, @org.jetbrains.annotations.Nullable()
    java.lang.String mobileNo, @org.jetbrains.annotations.Nullable()
    java.lang.String birthday, @org.jetbrains.annotations.Nullable()
    java.lang.String gender, @org.jetbrains.annotations.Nullable()
    java.lang.String bloodGroup, @org.jetbrains.annotations.Nullable()
    java.lang.String maritalStatusStatus, @org.jetbrains.annotations.Nullable()
    java.lang.String noOfChild, @org.jetbrains.annotations.Nullable()
    java.lang.String nationality, @org.jetbrains.annotations.Nullable()
    java.lang.String nationalID, @org.jetbrains.annotations.Nullable()
    java.lang.String passport, @org.jetbrains.annotations.Nullable()
    java.lang.String birthCertificate) {
    }
    
    public final void checkUpdateAddressInfo(@org.jetbrains.annotations.Nullable()
    java.lang.String netiId, @org.jetbrains.annotations.Nullable()
    java.lang.String addressDetails, @org.jetbrains.annotations.Nullable()
    java.lang.String divisionName, @org.jetbrains.annotations.Nullable()
    java.lang.String districtName, @org.jetbrains.annotations.Nullable()
    java.lang.String upazillaName, @org.jetbrains.annotations.Nullable()
    java.lang.String latitude, @org.jetbrains.annotations.Nullable()
    java.lang.String longitude) {
    }
    
    public ProfileViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\'\u0010\u0007\u001a\u0002H\b\"\n\b\u0000\u0010\b*\u0004\u0018\u00010\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016\u00a2\u0006\u0002\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\r"}, d2 = {"Lcom/netizen/netiworld/viewModel/ProfileViewModel$ProfileViewModelFactory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "getApplication", "()Landroid/app/Application;", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "app_debug"})
    public static final class ProfileViewModelFactory implements androidx.lifecycle.ViewModelProvider.Factory {
        @org.jetbrains.annotations.NotNull()
        private final android.app.Application application = null;
        
        @java.lang.Override()
        public <T extends androidx.lifecycle.ViewModel>T create(@org.jetbrains.annotations.NotNull()
        java.lang.Class<T> modelClass) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.app.Application getApplication() {
            return null;
        }
        
        public ProfileViewModelFactory(@org.jetbrains.annotations.NotNull()
        android.app.Application application) {
            super();
        }
    }
}