package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001:\u0001!B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0011\u001a\u0004\u0018\u00010\bJ\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0015\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0017\u001a\u00020\u00182\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0019\u001a\u00020\u00182\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001a\u001a\u00020\u00182\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001b\u001a\u00020\u00182\b\u0010\u0007\u001a\u0004\u0018\u00010\bJ\u0010\u0010\u001c\u001a\u00020\u00182\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001d\u001a\u00020\u00182\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001e\u001a\u00020\u00182\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001f\u001a\u00020\u00182\b\u0010\f\u001a\u0004\u0018\u00010\u0004J\u0010\u0010 \u001a\u00020\u00182\b\u0010\r\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""}, d2 = {"Lcom/netizen/netiworld/model/PostExperienceInfo;", "", "()V", "companyBusiness", "", "companyLocation", "companyName", "countryInfoDTO", "Lcom/netizen/netiworld/model/PostExperienceInfo$CountryInfoDTO;", "designationName", "employmentEnd", "employmentStart", "responsibilityDetails", "workingDepartment", "getCompanyBusiness", "getCompanyLocation", "getCompanyName", "getCountryInfoDTO", "getDesignationName", "getEmploymentEnd", "getEmploymentStart", "getResponsibilityDetails", "getWorkingDepartment", "setCompanyBusiness", "", "setCompanyLocation", "setCompanyName", "setCountryInfoDTO", "setDesignationName", "setEmploymentEnd", "setEmploymentStart", "setResponsibilityDetails", "setWorkingDepartment", "CountryInfoDTO", "app_debug"})
public final class PostExperienceInfo {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "countryInfoDTO")
    private com.netizen.netiworld.model.PostExperienceInfo.CountryInfoDTO countryInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "companyName")
    private java.lang.String companyName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "companyBusiness")
    private java.lang.String companyBusiness;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "designationName")
    private java.lang.String designationName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "workingDepartment")
    private java.lang.String workingDepartment;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "companyLocation")
    private java.lang.String companyLocation;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "employmentStart")
    private java.lang.String employmentStart;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "employmentEnd")
    private java.lang.String employmentEnd;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "responsibilityDetails")
    private java.lang.String responsibilityDetails;
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.PostExperienceInfo.CountryInfoDTO getCountryInfoDTO() {
        return null;
    }
    
    public final void setCountryInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.PostExperienceInfo.CountryInfoDTO countryInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCompanyName() {
        return null;
    }
    
    public final void setCompanyName(@org.jetbrains.annotations.Nullable()
    java.lang.String companyName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCompanyBusiness() {
        return null;
    }
    
    public final void setCompanyBusiness(@org.jetbrains.annotations.Nullable()
    java.lang.String companyBusiness) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDesignationName() {
        return null;
    }
    
    public final void setDesignationName(@org.jetbrains.annotations.Nullable()
    java.lang.String designationName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getWorkingDepartment() {
        return null;
    }
    
    public final void setWorkingDepartment(@org.jetbrains.annotations.Nullable()
    java.lang.String workingDepartment) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCompanyLocation() {
        return null;
    }
    
    public final void setCompanyLocation(@org.jetbrains.annotations.Nullable()
    java.lang.String companyLocation) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEmploymentStart() {
        return null;
    }
    
    public final void setEmploymentStart(@org.jetbrains.annotations.Nullable()
    java.lang.String employmentStart) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEmploymentEnd() {
        return null;
    }
    
    public final void setEmploymentEnd(@org.jetbrains.annotations.Nullable()
    java.lang.String employmentEnd) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getResponsibilityDetails() {
        return null;
    }
    
    public final void setResponsibilityDetails(@org.jetbrains.annotations.Nullable()
    java.lang.String responsibilityDetails) {
    }
    
    public PostExperienceInfo() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0006\u001a\u00020\u00072\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/netizen/netiworld/model/PostExperienceInfo$CountryInfoDTO;", "", "()V", "coreCategoryID", "", "getCoreCategoryID", "setCoreCategoryID", "", "app_debug"})
    public static final class CountryInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.String coreCategoryID;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.String coreCategoryID) {
        }
        
        public CountryInfoDTO() {
            super();
        }
    }
}