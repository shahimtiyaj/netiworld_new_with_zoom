package com.netizen.netiworld.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\f\u001a\u00020\rR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\u000e"}, d2 = {"Lcom/netizen/netiworld/repository/HomeRepository;", "", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "appPreferences", "Lcom/netizen/netiworld/utils/AppPreferences;", "homePageInfoLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/netizen/netiworld/model/HomePageInfo;", "getHomePageInfoLiveData", "()Landroidx/lifecycle/MutableLiveData;", "getHomePageInfo", "", "app_debug"})
public final class HomeRepository {
    private final com.netizen.netiworld.utils.AppPreferences appPreferences = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.HomePageInfo> homePageInfoLiveData = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.HomePageInfo> getHomePageInfoLiveData() {
        return null;
    }
    
    public final void getHomePageInfo() {
    }
    
    public HomeRepository(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super();
    }
}