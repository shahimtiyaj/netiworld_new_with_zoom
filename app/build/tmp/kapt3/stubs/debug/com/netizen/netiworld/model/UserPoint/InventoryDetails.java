package com.netizen.netiworld.model.UserPoint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u0014B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u0004J\b\u0010\f\u001a\u0004\u0018\u00010\u0007J\r\u0010\r\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u000eJ\u0016\u0010\u000f\u001a\u00020\u00102\u000e\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0004J\u0010\u0010\u0011\u001a\u00020\u00102\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007J\u0015\u0010\u0012\u001a\u00020\u00102\b\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u0013R\u001a\u0010\u0003\u001a\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\n\u00a8\u0006\u0015"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/InventoryDetails;", "", "()V", "item", "", "Lcom/netizen/netiworld/model/UserPoint/InventoryDetails$Item;", "message", "", "msgType", "", "Ljava/lang/Integer;", "getItem", "getMessage", "getMsgType", "()Ljava/lang/Integer;", "setItem", "", "setMessage", "setMsgType", "(Ljava/lang/Integer;)V", "Item", "app_debug"})
public final class InventoryDetails {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "message")
    private java.lang.String message;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "msgType")
    private java.lang.Integer msgType;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "item")
    private java.util.List<com.netizen.netiworld.model.UserPoint.InventoryDetails.Item> item;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.Nullable()
    java.lang.String message) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getMsgType() {
        return null;
    }
    
    public final void setMsgType(@org.jetbrains.annotations.Nullable()
    java.lang.Integer msgType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.netizen.netiworld.model.UserPoint.InventoryDetails.Item> getItem() {
        return null;
    }
    
    public final void setItem(@org.jetbrains.annotations.Nullable()
    java.util.List<com.netizen.netiworld.model.UserPoint.InventoryDetails.Item> item) {
    }
    
    public InventoryDetails() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\f\u001a\u0004\u0018\u00010\u0004J\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000f\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u0010J\r\u0010\u0011\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u0010J\r\u0010\u0012\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u0010J\u0010\u0010\u0013\u001a\u00020\u00142\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0015\u001a\u00020\u00142\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0016\u001a\u00020\u00142\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0017\u001a\u00020\u00142\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u0018J\u0015\u0010\u0019\u001a\u00020\u00142\b\u0010\n\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u0018J\u0015\u0010\u001a\u001a\u00020\u00142\b\u0010\u000b\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u0018R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0016\u0010\n\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0016\u0010\u000b\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\t\u00a8\u0006\u001b"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/InventoryDetails$Item;", "", "()V", "category", "", "date", "item", "quantity", "", "Ljava/lang/Float;", "totalPrice", "unitPrice", "getCategory", "getDate", "getItem", "getQuantity", "()Ljava/lang/Float;", "getTotalPrice", "getUnitPrice", "setCategory", "", "setDate", "setItem", "setQuantity", "(Ljava/lang/Float;)V", "setTotalPrice", "setUnitPrice", "app_debug"})
    public static final class Item {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "date")
        private java.lang.String date;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "category")
        private java.lang.String category;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "item")
        private java.lang.String item;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "unitPrice")
        private java.lang.Float unitPrice;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "quantity")
        private java.lang.Float quantity;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "totalPrice")
        private java.lang.Float totalPrice;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getDate() {
            return null;
        }
        
        public final void setDate(@org.jetbrains.annotations.Nullable()
        java.lang.String date) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategory() {
            return null;
        }
        
        public final void setCategory(@org.jetbrains.annotations.Nullable()
        java.lang.String category) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getItem() {
            return null;
        }
        
        public final void setItem(@org.jetbrains.annotations.Nullable()
        java.lang.String item) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Float getUnitPrice() {
            return null;
        }
        
        public final void setUnitPrice(@org.jetbrains.annotations.Nullable()
        java.lang.Float unitPrice) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Float getQuantity() {
            return null;
        }
        
        public final void setQuantity(@org.jetbrains.annotations.Nullable()
        java.lang.Float quantity) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Float getTotalPrice() {
            return null;
        }
        
        public final void setTotalPrice(@org.jetbrains.annotations.Nullable()
        java.lang.Float totalPrice) {
        }
        
        public Item() {
            super();
        }
    }
}