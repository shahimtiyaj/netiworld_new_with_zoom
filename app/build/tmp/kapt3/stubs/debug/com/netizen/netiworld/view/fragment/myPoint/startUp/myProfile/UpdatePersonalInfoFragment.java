package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0014\u001a\u00020\u0015H\u0002J\b\u0010\u0016\u001a\u00020\u0015H\u0002J\b\u0010\u0017\u001a\u00020\u0015H\u0002J\u0012\u0010\u0018\u001a\u00020\u00152\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J&\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 2\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\u0010\u0010!\u001a\u00020\u00152\u0006\u0010\"\u001a\u00020#H\u0002J\b\u0010$\u001a\u00020\u0015H\u0002J \u0010%\u001a\u00020\u00152\u0006\u0010&\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\'\u001a\u00020\tH\u0002J\u0010\u0010(\u001a\u00020\u00152\u0006\u0010)\u001a\u00020\u000eH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\nR\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006*"}, d2 = {"Lcom/netizen/netiworld/view/fragment/myPoint/startUp/myProfile/UpdatePersonalInfoFragment;", "Landroidx/fragment/app/Fragment;", "()V", "binding", "Lcom/netizen/netiworld/databinding/FragmentUpdatePersonalInfoBinding;", "calendar", "Ljava/util/Calendar;", "kotlin.jvm.PlatformType", "dateSelection", "", "Ljava/lang/Integer;", "dateSetListener", "Landroid/app/DatePickerDialog$OnDateSetListener;", "gender", "", "maritalStatus", "netiId", "profileViewModel", "Lcom/netizen/netiworld/viewModel/ProfileViewModel;", "title", "clearFields", "", "initObservers", "initViews", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "setData", "profileInformation", "Lcom/netizen/netiworld/model/ProfileInformation;", "showDatePicker", "showSearchSpinnerDialog", "dataType", "spinnerType", "showToastyError", "message", "app_debug"})
public final class UpdatePersonalInfoFragment extends androidx.fragment.app.Fragment {
    private com.netizen.netiworld.databinding.FragmentUpdatePersonalInfoBinding binding;
    private java.lang.String title = "Update Personal Info";
    private java.lang.String gender;
    private java.lang.String maritalStatus;
    private com.netizen.netiworld.viewModel.ProfileViewModel profileViewModel;
    private int netiId = 0;
    private java.util.Calendar calendar;
    private java.lang.Integer dateSelection;
    private final android.app.DatePickerDialog.OnDateSetListener dateSetListener = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    private final void initViews() {
    }
    
    private final void initObservers() {
    }
    
    private final void setData(com.netizen.netiworld.model.ProfileInformation profileInformation) {
    }
    
    private final void clearFields() {
    }
    
    private final void showSearchSpinnerDialog(java.lang.String dataType, java.lang.String title, int spinnerType) {
    }
    
    private final void showToastyError(java.lang.String message) {
    }
    
    private final void showDatePicker() {
    }
    
    public UpdatePersonalInfoFragment() {
        super();
    }
}