package com.netizen.netiworld.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00ac\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0017\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010K\u001a\u00020LJ\u0006\u0010M\u001a\u00020LJ\u0006\u0010N\u001a\u00020LJ\u0006\u0010O\u001a\u00020LJ\u0006\u0010P\u001a\u00020LJ\u0010\u0010Q\u001a\u00020L2\b\u0010R\u001a\u0004\u0018\u000103J\u0006\u0010S\u001a\u00020LJ\u0006\u0010T\u001a\u00020LJ\u0006\u0010U\u001a\u00020LJ\u0006\u0010V\u001a\u00020LJ\u0006\u0010W\u001a\u00020LJ\u0006\u0010X\u001a\u00020LJ\u000e\u0010Y\u001a\u00020L2\u0006\u0010Z\u001a\u000203J\u0006\u0010[\u001a\u00020LJ\u0006\u0010\\\u001a\u00020LJ\u0006\u0010]\u001a\u00020LJ\u0006\u0010^\u001a\u00020LJ\u0010\u0010_\u001a\u00020L2\b\u0010`\u001a\u0004\u0018\u000103J\u0006\u0010a\u001a\u00020LJ\u000e\u0010b\u001a\u00020L2\u0006\u0010c\u001a\u00020dJ\u000e\u0010e\u001a\u00020L2\u0006\u0010f\u001a\u00020gJ\u000e\u0010h\u001a\u00020L2\u0006\u0010i\u001a\u00020jJ\u000e\u0010k\u001a\u00020L2\u0006\u0010l\u001a\u00020mJ\u000e\u0010n\u001a\u00020L2\u0006\u0010o\u001a\u00020pJ\u000e\u0010q\u001a\u00020L2\u0006\u0010c\u001a\u00020dJ\u000e\u0010r\u001a\u00020L2\u0006\u0010r\u001a\u00020sJ\u000e\u0010t\u001a\u00020L2\u0006\u0010t\u001a\u00020uJ\u000e\u0010v\u001a\u00020L2\u0006\u00106\u001a\u00020wJ\u000e\u0010x\u001a\u00020L2\u0006\u0010x\u001a\u00020yJ\u000e\u0010z\u001a\u00020L2\u0006\u0010o\u001a\u00020pR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR&\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R&\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u000e\"\u0004\b\u0013\u0010\u0010R&\u0010\u0014\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00150\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u000e\"\u0004\b\u0017\u0010\u0010R&\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u000e\"\u0004\b\u001a\u0010\u0010R&\u0010\u001b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u000e\"\u0004\b\u001d\u0010\u0010R&\u0010\u001e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\u000e\"\u0004\b \u0010\u0010R&\u0010!\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\"0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u000e\"\u0004\b$\u0010\u0010R&\u0010%\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u000e\"\u0004\b\'\u0010\u0010R&\u0010(\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020)0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\u000e\"\u0004\b+\u0010\u0010R&\u0010,\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b-\u0010\u000e\"\u0004\b.\u0010\u0010R&\u0010/\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b0\u0010\u000e\"\u0004\b1\u0010\u0010R \u00102\u001a\b\u0012\u0004\u0012\u0002030\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\u000e\"\u0004\b5\u0010\u0010R \u00106\u001a\b\u0012\u0004\u0012\u0002070\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b8\u0010\u000e\"\u0004\b9\u0010\u0010R&\u0010:\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020;0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b<\u0010\u000e\"\u0004\b=\u0010\u0010R&\u0010>\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b?\u0010\u000e\"\u0004\b@\u0010\u0010R&\u0010A\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bB\u0010\u000e\"\u0004\bC\u0010\u0010R&\u0010D\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020E0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bF\u0010\u000e\"\u0004\bG\u0010\u0010R&\u0010H\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bI\u0010\u000e\"\u0004\bJ\u0010\u0010\u00a8\u0006{"}, d2 = {"Lcom/netizen/netiworld/repository/ProfileRepository;", "", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "appPreferences", "Lcom/netizen/netiworld/utils/AppPreferences;", "getApplication", "()Landroid/app/Application;", "bloodGroupList", "Landroidx/lifecycle/MutableLiveData;", "", "Lcom/netizen/netiworld/model/DataByTypeDefaultCode;", "getBloodGroupList", "()Landroidx/lifecycle/MutableLiveData;", "setBloodGroupList", "(Landroidx/lifecycle/MutableLiveData;)V", "boardList", "getBoardList", "setBoardList", "certificationInfoList", "Lcom/netizen/netiworld/model/CertificationInfo;", "getCertificationInfoList", "setCertificationInfoList", "countryList", "getCountryList", "setCountryList", "districtList", "getDistrictList", "setDistrictList", "divisionList", "getDivisionList", "setDivisionList", "educationInfoList", "Lcom/netizen/netiworld/model/EducationInfo;", "getEducationInfoList", "setEducationInfoList", "educationLevelList", "getEducationLevelList", "setEducationLevelList", "experienceInfoList", "Lcom/netizen/netiworld/model/ExperienceInfo;", "getExperienceInfoList", "setExperienceInfoList", "majorSubjectList", "getMajorSubjectList", "setMajorSubjectList", "passingYearList", "getPassingYearList", "setPassingYearList", "photoFileContent", "", "getPhotoFileContent", "setPhotoFileContent", "profileInformation", "Lcom/netizen/netiworld/model/ProfileInformation;", "getProfileInformation", "setProfileInformation", "referenceInfoList", "Lcom/netizen/netiworld/model/ReferenceInfo;", "getReferenceInfoList", "setReferenceInfoList", "relationList", "getRelationList", "setRelationList", "resultList", "getResultList", "setResultList", "trainingInfoList", "Lcom/netizen/netiworld/model/TrainingInfo;", "getTrainingInfoList", "setTrainingInfoList", "upazillaList", "getUpazillaList", "setUpazillaList", "addPoint", "", "getBloodGroup", "getBoards", "getCertificationInfo", "getCountry", "getDistrict", "divisionId", "getDivision", "getEducationInfo", "getEducationLevels", "getExperienceInfo", "getMajorSubjects", "getPassingYears", "getProfilePhoto", "imagePath", "getReferenceInfo", "getRelation", "getResults", "getTrainingInfo", "getUpazilla", "districtId", "getUserProfileInfoData", "saveAcademicEducation", "postAcademicEducation", "Lcom/netizen/netiworld/model/PostAcademicEducation;", "saveCertificationInfo", "postCertificationInfo", "Lcom/netizen/netiworld/model/PostCertificationInfo;", "saveExperienceInfo", "postExperienceInfo", "Lcom/netizen/netiworld/model/PostExperienceInfo;", "saveReferenceInfo", "postReferenceInfo", "Lcom/netizen/netiworld/model/PostReferenceInfo;", "saveTrainingInfo", "postTrainingInfo", "Lcom/netizen/netiworld/model/PostTrainingInfo;", "updateAcademicEducation", "updateCertificationInfo", "Lcom/netizen/netiworld/model/UpdateCertificationInfo;", "updateExperienceInfo", "Lcom/netizen/netiworld/model/UpdateExperienceInfo;", "updatePersonalInfo", "Lcom/netizen/netiworld/model/ProfileInformation_;", "updateReferenceInfo", "Lcom/netizen/netiworld/model/UpdateReferenceInfo;", "updateTrainingInfo", "app_debug"})
public final class ProfileRepository {
    private final com.netizen.netiworld.utils.AppPreferences appPreferences = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.ProfileInformation> profileInformation;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> photoFileContent;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.EducationInfo>> educationInfoList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TrainingInfo>> trainingInfoList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.CertificationInfo>> certificationInfoList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.ExperienceInfo>> experienceInfoList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.ReferenceInfo>> referenceInfoList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> educationLevelList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> majorSubjectList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> resultList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> passingYearList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> boardList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> countryList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> relationList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> bloodGroupList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> divisionList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> districtList;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> upazillaList;
    @org.jetbrains.annotations.NotNull()
    private final android.app.Application application = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.ProfileInformation> getProfileInformation() {
        return null;
    }
    
    public final void setProfileInformation(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.ProfileInformation> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getPhotoFileContent() {
        return null;
    }
    
    public final void setPhotoFileContent(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.EducationInfo>> getEducationInfoList() {
        return null;
    }
    
    public final void setEducationInfoList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.EducationInfo>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TrainingInfo>> getTrainingInfoList() {
        return null;
    }
    
    public final void setTrainingInfoList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.TrainingInfo>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.CertificationInfo>> getCertificationInfoList() {
        return null;
    }
    
    public final void setCertificationInfoList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.CertificationInfo>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.ExperienceInfo>> getExperienceInfoList() {
        return null;
    }
    
    public final void setExperienceInfoList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.ExperienceInfo>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.ReferenceInfo>> getReferenceInfoList() {
        return null;
    }
    
    public final void setReferenceInfoList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.ReferenceInfo>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> getEducationLevelList() {
        return null;
    }
    
    public final void setEducationLevelList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> getMajorSubjectList() {
        return null;
    }
    
    public final void setMajorSubjectList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> getResultList() {
        return null;
    }
    
    public final void setResultList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> getPassingYearList() {
        return null;
    }
    
    public final void setPassingYearList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> getBoardList() {
        return null;
    }
    
    public final void setBoardList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> getCountryList() {
        return null;
    }
    
    public final void setCountryList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> getRelationList() {
        return null;
    }
    
    public final void setRelationList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> getBloodGroupList() {
        return null;
    }
    
    public final void setBloodGroupList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> getDivisionList() {
        return null;
    }
    
    public final void setDivisionList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> getDistrictList() {
        return null;
    }
    
    public final void setDistrictList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> getUpazillaList() {
        return null;
    }
    
    public final void setUpazillaList(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> p0) {
    }
    
    public final void getUserProfileInfoData() {
    }
    
    public final void getProfilePhoto(@org.jetbrains.annotations.NotNull()
    java.lang.String imagePath) {
    }
    
    public final void getEducationInfo() {
    }
    
    public final void getTrainingInfo() {
    }
    
    public final void getCertificationInfo() {
    }
    
    public final void getExperienceInfo() {
    }
    
    public final void getReferenceInfo() {
    }
    
    public final void getBloodGroup() {
    }
    
    public final void getEducationLevels() {
    }
    
    public final void getMajorSubjects() {
    }
    
    public final void getResults() {
    }
    
    public final void getPassingYears() {
    }
    
    public final void getBoards() {
    }
    
    public final void getCountry() {
    }
    
    public final void getRelation() {
    }
    
    public final void saveAcademicEducation(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.PostAcademicEducation postAcademicEducation) {
    }
    
    public final void saveTrainingInfo(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.PostTrainingInfo postTrainingInfo) {
    }
    
    public final void saveCertificationInfo(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.PostCertificationInfo postCertificationInfo) {
    }
    
    public final void saveExperienceInfo(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.PostExperienceInfo postExperienceInfo) {
    }
    
    public final void saveReferenceInfo(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.PostReferenceInfo postReferenceInfo) {
    }
    
    public final void updateCertificationInfo(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.UpdateCertificationInfo updateCertificationInfo) {
    }
    
    public final void updateExperienceInfo(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.UpdateExperienceInfo updateExperienceInfo) {
    }
    
    public final void updateAcademicEducation(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.PostAcademicEducation postAcademicEducation) {
    }
    
    public final void updateTrainingInfo(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.PostTrainingInfo postTrainingInfo) {
    }
    
    public final void updateReferenceInfo(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.UpdateReferenceInfo updateReferenceInfo) {
    }
    
    public final void updatePersonalInfo(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.ProfileInformation_ profileInformation) {
    }
    
    public final void getDivision() {
    }
    
    public final void getDistrict(@org.jetbrains.annotations.Nullable()
    java.lang.String divisionId) {
    }
    
    public final void getUpazilla(@org.jetbrains.annotations.Nullable()
    java.lang.String districtId) {
    }
    
    public final void addPoint() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.app.Application getApplication() {
        return null;
    }
    
    public ProfileRepository(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super();
    }
}