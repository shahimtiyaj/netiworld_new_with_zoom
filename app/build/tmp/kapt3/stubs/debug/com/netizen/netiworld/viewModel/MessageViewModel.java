package com.netizen.netiworld.viewModel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001LB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u00109\u001a\u00020:2\b\u0010;\u001a\u0004\u0018\u00010\u001b2\b\u0010<\u001a\u0004\u0018\u00010\u001bJ\u0016\u0010=\u001a\u00020:2\u0006\u0010>\u001a\u00020\u001b2\u0006\u0010?\u001a\u00020\u001bJ\u001a\u0010@\u001a\u00020:2\b\u0010;\u001a\u0004\u0018\u00010\u001b2\b\u0010<\u001a\u0004\u0018\u00010\u001bJ\u0016\u0010A\u001a\u00020:2\u0006\u0010B\u001a\u00020\u001b2\u0006\u0010C\u001a\u00020\u001bJ\u000e\u0010D\u001a\u00020:2\u0006\u0010E\u001a\u00020\u001bJ\u0006\u0010F\u001a\u00020:J\u000e\u0010\'\u001a\u00020:2\u0006\u0010G\u001a\u00020\u001bJ\u000e\u0010H\u001a\u00020:2\u0006\u0010E\u001a\u00020\u001bJ(\u00103\u001a\u0012\u0012\u0004\u0012\u00020\u001b01j\b\u0012\u0004\u0012\u00020\u001b`I2\u000e\u0010J\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00180\u0014H\u0002J&\u00106\u001a\u0012\u0012\u0004\u0012\u00020\u001b01j\b\u0012\u0004\u0012\u00020\u001b`I2\f\u0010K\u001a\b\u0012\u0004\u0012\u00020%0\u0014H\u0002R \u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\b\"\u0004\b\t\u0010\nR\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\bR\u0017\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\bR\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\bR\u0017\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\bR \u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\b\"\u0004\b\u0010\u0010\nR\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001f\u0010\u0013\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00150\u00140\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\bR\u001f\u0010\u0017\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010\u00180\u00140\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\bR\u0017\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001b0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\bR\u001c\u0010\u001d\u001a\u0004\u0018\u00010\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u0017\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u001b0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010\bR\u001a\u0010$\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020%0\u00140\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010&\u001a\b\u0012\u0004\u0012\u00020\u001b0\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\'\u0010\b\"\u0004\b(\u0010\nR\u001c\u0010)\u001a\u0004\u0018\u00010\u001bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\u001f\"\u0004\b+\u0010!R\u001f\u0010,\u001a\u0010\u0012\f\u0012\n\u0012\u0006\u0012\u0004\u0018\u00010-0\u00140\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b.\u0010\bR+\u0010/\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u001b 2*\n\u0012\u0004\u0012\u00020\u001b\u0018\u0001010100\u00a2\u0006\b\n\u0000\u001a\u0004\b3\u00104R+\u00105\u001a\u001c\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u001b 2*\n\u0012\u0004\u0012\u00020\u001b\u0018\u0001010100\u00a2\u0006\b\n\u0000\u001a\u0004\b6\u00104R\u0017\u00107\u001a\b\u0012\u0004\u0012\u00020\u001b0\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b8\u0010\b\u00a8\u0006M"}, d2 = {"Lcom/netizen/netiworld/viewModel/MessageViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "isFromDateEmpty", "Landroidx/lifecycle/MutableLiveData;", "", "()Landroidx/lifecycle/MutableLiveData;", "setFromDateEmpty", "(Landroidx/lifecycle/MutableLiveData;)V", "isMessageLogDataFound", "isMessageTypeEmpty", "isPurchasePointEmpty", "isRevenueLogDataFound", "isToDateEmpty", "setToDateEmpty", "messageBalanceRepository", "Lcom/netizen/netiworld/repository/MessageRepository;", "messageRechargeInfo", "", "Lcom/netizen/netiworld/model/BalanceMessageGetData;", "getMessageRechargeInfo", "messageTypeInfo", "Lcom/netizen/netiworld/model/MessageType1;", "getMessageTypeInfo", "percentageVat", "", "getPercentageVat", "productId", "getProductId", "()Ljava/lang/String;", "setProductId", "(Ljava/lang/String;)V", "productRoleAssignID1", "getProductRoleAssignID1", "purchasePointArrayList", "Lcom/netizen/netiworld/model/PurchasePoint;", "purchasePointId", "getPurchasePointId", "setPurchasePointId", "purchasePointRoleId", "getPurchasePointRoleId", "setPurchasePointRoleId", "revenueLogList", "Lcom/netizen/netiworld/model/RevenueLogGetData;", "getRevenueLogList", "tempMessageTypeList1", "Landroidx/lifecycle/LiveData;", "Ljava/util/ArrayList;", "kotlin.jvm.PlatformType", "getTempMessageTypeList1", "()Landroidx/lifecycle/LiveData;", "tempPurchasePointList", "getTempPurchasePointList", "unitPrice", "getUnitPrice", "checkMessageLogData", "", "fromDate", "toDate", "checkMessageRechargeValues", "messageQuantity", "productRoleAssignID", "checkRevenueLogData", "getMessageRechargeInfoList", "startDateMessage", "endDateMessage", "getProductId1", "selectItemName", "getPurchasePointData", "pointName", "getRoleId", "Lkotlin/collections/ArrayList;", "messageTypeList", "purchasePointList", "MessageBalanceViewModelFactory", "app_debug"})
public final class MessageViewModel extends androidx.lifecycle.AndroidViewModel {
    private final com.netizen.netiworld.repository.MessageRepository messageBalanceRepository = null;
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.PurchasePoint>> purchasePointArrayList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPurchasePointEmpty = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMessageTypeEmpty = null;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String purchasePointRoleId;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String productId;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.String> unitPrice = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.String> percentageVat = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.String> productRoleAssignID1 = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceMessageGetData>> messageRechargeInfo = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.RevenueLogGetData>> revenueLogList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.MessageType1>> messageTypeInfo = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.String> purchasePointId;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isFromDateEmpty;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isToDateEmpty;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isRevenueLogDataFound = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMessageLogDataFound = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempPurchasePointList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> tempMessageTypeList1 = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isPurchasePointEmpty() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMessageTypeEmpty() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPurchasePointRoleId() {
        return null;
    }
    
    public final void setPurchasePointRoleId(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getProductId() {
        return null;
    }
    
    public final void setProductId(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getUnitPrice() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getPercentageVat() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getProductRoleAssignID1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.BalanceMessageGetData>> getMessageRechargeInfo() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.RevenueLogGetData>> getRevenueLogList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.MessageType1>> getMessageTypeInfo() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.String> getPurchasePointId() {
        return null;
    }
    
    public final void setPurchasePointId(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isFromDateEmpty() {
        return null;
    }
    
    public final void setFromDateEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isToDateEmpty() {
        return null;
    }
    
    public final void setToDateEmpty(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isRevenueLogDataFound() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isMessageLogDataFound() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempPurchasePointList() {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempPurchasePointList(java.util.List<com.netizen.netiworld.model.PurchasePoint> purchasePointList) {
        return null;
    }
    
    public final void getPurchasePointData() {
    }
    
    public final void getPurchasePointId(@org.jetbrains.annotations.NotNull()
    java.lang.String pointName) {
    }
    
    public final void getRoleId(@org.jetbrains.annotations.NotNull()
    java.lang.String selectItemName) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<java.lang.String>> getTempMessageTypeList1() {
        return null;
    }
    
    private final java.util.ArrayList<java.lang.String> getTempMessageTypeList1(java.util.List<com.netizen.netiworld.model.MessageType1> messageTypeList) {
        return null;
    }
    
    public final void getProductId1(@org.jetbrains.annotations.NotNull()
    java.lang.String selectItemName) {
    }
    
    public final void checkMessageRechargeValues(@org.jetbrains.annotations.NotNull()
    java.lang.String messageQuantity, @org.jetbrains.annotations.NotNull()
    java.lang.String productRoleAssignID) {
    }
    
    public final void getMessageRechargeInfoList(@org.jetbrains.annotations.NotNull()
    java.lang.String startDateMessage, @org.jetbrains.annotations.NotNull()
    java.lang.String endDateMessage) {
    }
    
    public final void checkRevenueLogData(@org.jetbrains.annotations.Nullable()
    java.lang.String fromDate, @org.jetbrains.annotations.Nullable()
    java.lang.String toDate) {
    }
    
    public final void checkMessageLogData(@org.jetbrains.annotations.Nullable()
    java.lang.String fromDate, @org.jetbrains.annotations.Nullable()
    java.lang.String toDate) {
    }
    
    public MessageViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\'\u0010\u0007\u001a\u0002H\b\"\n\b\u0000\u0010\b*\u0004\u0018\u00010\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016\u00a2\u0006\u0002\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\r"}, d2 = {"Lcom/netizen/netiworld/viewModel/MessageViewModel$MessageBalanceViewModelFactory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "getApplication", "()Landroid/app/Application;", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "app_debug"})
    public static final class MessageBalanceViewModelFactory implements androidx.lifecycle.ViewModelProvider.Factory {
        @org.jetbrains.annotations.NotNull()
        private final android.app.Application application = null;
        
        @java.lang.Override()
        public <T extends androidx.lifecycle.ViewModel>T create(@org.jetbrains.annotations.NotNull()
        java.lang.Class<T> modelClass) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.app.Application getApplication() {
            return null;
        }
        
        public MessageBalanceViewModelFactory(@org.jetbrains.annotations.NotNull()
        android.app.Application application) {
            super();
        }
    }
}