package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0017\n\u0002\u0010\u0002\n\u0002\b\u0012\u0018\u00002\u00020\u0001:\u0001:B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0018\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0019\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001a\u001a\u0004\u0018\u00010\bJ\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001d\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001e\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001f\u001a\u0004\u0018\u00010\u0004J\b\u0010 \u001a\u0004\u0018\u00010\u0004J\b\u0010!\u001a\u0004\u0018\u00010\u0004J\r\u0010\"\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\u0002\u0010#J\b\u0010$\u001a\u0004\u0018\u00010\u0001J\b\u0010%\u001a\u0004\u0018\u00010\u0004J\b\u0010&\u001a\u0004\u0018\u00010\u0001J\b\u0010\'\u001a\u0004\u0018\u00010\u0004J\u0010\u0010(\u001a\u00020)2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010*\u001a\u00020)2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010+\u001a\u00020)2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0010\u0010,\u001a\u00020)2\b\u0010\u0007\u001a\u0004\u0018\u00010\bJ\u0010\u0010-\u001a\u00020)2\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0010\u0010.\u001a\u00020)2\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\u0010\u0010/\u001a\u00020)2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u0010\u00100\u001a\u00020)2\b\u0010\f\u001a\u0004\u0018\u00010\u0004J\u0010\u00101\u001a\u00020)2\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\u0010\u00102\u001a\u00020)2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\u0010\u00103\u001a\u00020)2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\u0015\u00104\u001a\u00020)2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\u0002\u00105J\u0010\u00106\u001a\u00020)2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001J\u0010\u00107\u001a\u00020)2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\u0010\u00108\u001a\u00020)2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001J\u0010\u00109\u001a\u00020)2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\u0004\u0018\u00010\u00118\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0012R\u0014\u0010\u0013\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006;"}, d2 = {"Lcom/netizen/netiworld/model/UpdateExperienceInfo;", "", "()V", "companyBusiness", "", "companyLocation", "companyName", "countryInfoDTO", "Lcom/netizen/netiworld/model/UpdateExperienceInfo$CountryInfoDTO;", "designationName", "employmentEnd", "employmentEndEdit", "employmentEndView", "employmentStart", "employmentStartEdit", "employmentStartView", "experienceId", "", "Ljava/lang/Integer;", "experienceSerial", "responsibilityDetails", "userBasicInfoDTO", "workingDepartment", "getCompanyBusiness", "getCompanyLocation", "getCompanyName", "getCountryInfoDTO", "getDesignationName", "getEmploymentEnd", "getEmploymentEndEdit", "getEmploymentEndView", "getEmploymentStart", "getEmploymentStartEdit", "getEmploymentStartView", "getExperienceId", "()Ljava/lang/Integer;", "getExperienceSerial", "getResponsibilityDetails", "getUserBasicInfoDTO", "getWorkingDepartment", "setCompanyBusiness", "", "setCompanyLocation", "setCompanyName", "setCountryInfoDTO", "setDesignationName", "setEmploymentEnd", "setEmploymentEndEdit", "setEmploymentEndView", "setEmploymentStart", "setEmploymentStartEdit", "setEmploymentStartView", "setExperienceId", "(Ljava/lang/Integer;)V", "setExperienceSerial", "setResponsibilityDetails", "setUserBasicInfoDTO", "setWorkingDepartment", "CountryInfoDTO", "app_debug"})
public final class UpdateExperienceInfo {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "experienceId")
    private java.lang.Integer experienceId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "experienceSerial")
    private java.lang.Object experienceSerial;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "companyName")
    private java.lang.String companyName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "companyBusiness")
    private java.lang.String companyBusiness;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "designationName")
    private java.lang.String designationName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "workingDepartment")
    private java.lang.String workingDepartment;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "companyLocation")
    private java.lang.String companyLocation;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "employmentStart")
    private java.lang.String employmentStart;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "employmentEnd")
    private java.lang.String employmentEnd;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "responsibilityDetails")
    private java.lang.String responsibilityDetails;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userBasicInfoDTO")
    private java.lang.Object userBasicInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "countryInfoDTO")
    private com.netizen.netiworld.model.UpdateExperienceInfo.CountryInfoDTO countryInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "employmentStartView")
    private java.lang.String employmentStartView;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "employmentStartEdit")
    private java.lang.String employmentStartEdit;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "employmentEndView")
    private java.lang.String employmentEndView;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "employmentEndEdit")
    private java.lang.String employmentEndEdit;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getExperienceId() {
        return null;
    }
    
    public final void setExperienceId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer experienceId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getExperienceSerial() {
        return null;
    }
    
    public final void setExperienceSerial(@org.jetbrains.annotations.Nullable()
    java.lang.Object experienceSerial) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCompanyName() {
        return null;
    }
    
    public final void setCompanyName(@org.jetbrains.annotations.Nullable()
    java.lang.String companyName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCompanyBusiness() {
        return null;
    }
    
    public final void setCompanyBusiness(@org.jetbrains.annotations.Nullable()
    java.lang.String companyBusiness) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDesignationName() {
        return null;
    }
    
    public final void setDesignationName(@org.jetbrains.annotations.Nullable()
    java.lang.String designationName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getWorkingDepartment() {
        return null;
    }
    
    public final void setWorkingDepartment(@org.jetbrains.annotations.Nullable()
    java.lang.String workingDepartment) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCompanyLocation() {
        return null;
    }
    
    public final void setCompanyLocation(@org.jetbrains.annotations.Nullable()
    java.lang.String companyLocation) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEmploymentStart() {
        return null;
    }
    
    public final void setEmploymentStart(@org.jetbrains.annotations.Nullable()
    java.lang.String employmentStart) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEmploymentEnd() {
        return null;
    }
    
    public final void setEmploymentEnd(@org.jetbrains.annotations.Nullable()
    java.lang.String employmentEnd) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getResponsibilityDetails() {
        return null;
    }
    
    public final void setResponsibilityDetails(@org.jetbrains.annotations.Nullable()
    java.lang.String responsibilityDetails) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getUserBasicInfoDTO() {
        return null;
    }
    
    public final void setUserBasicInfoDTO(@org.jetbrains.annotations.Nullable()
    java.lang.Object userBasicInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.UpdateExperienceInfo.CountryInfoDTO getCountryInfoDTO() {
        return null;
    }
    
    public final void setCountryInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.UpdateExperienceInfo.CountryInfoDTO countryInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEmploymentStartView() {
        return null;
    }
    
    public final void setEmploymentStartView(@org.jetbrains.annotations.Nullable()
    java.lang.String employmentStartView) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEmploymentStartEdit() {
        return null;
    }
    
    public final void setEmploymentStartEdit(@org.jetbrains.annotations.Nullable()
    java.lang.String employmentStartEdit) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEmploymentEndView() {
        return null;
    }
    
    public final void setEmploymentEndView(@org.jetbrains.annotations.Nullable()
    java.lang.String employmentEndView) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getEmploymentEndEdit() {
        return null;
    }
    
    public final void setEmploymentEndEdit(@org.jetbrains.annotations.Nullable()
    java.lang.String employmentEndEdit) {
    }
    
    public UpdateExperienceInfo() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u001b\n\u0002\u0010\u0002\n\u0002\b\u000e\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0014\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0018\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\r\u0010\u0019\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\r\u0010\u001a\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001J\r\u0010\u001e\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001J\r\u0010 \u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0015J\u0010\u0010!\u001a\u00020\"2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0015\u0010#\u001a\u00020\"2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0010\u0010%\u001a\u00020\"2\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010&\u001a\u00020\"2\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\'\u001a\u00020\"2\b\u0010\n\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0015\u0010(\u001a\u00020\"2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0015\u0010)\u001a\u00020\"2\b\u0010\f\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0010\u0010*\u001a\u00020\"2\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\u0010\u0010+\u001a\u00020\"2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\u0010\u0010,\u001a\u00020\"2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001J\u0015\u0010-\u001a\u00020\"2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$J\u0010\u0010.\u001a\u00020\"2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001J\u0015\u0010/\u001a\u00020\"2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010$R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0016\u0010\f\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007\u00a8\u00060"}, d2 = {"Lcom/netizen/netiworld/model/UpdateExperienceInfo$CountryInfoDTO;", "", "()V", "categoryDefaultCode", "", "categoryEnableStatus", "", "Ljava/lang/Integer;", "categoryName", "categoryNote", "categorySerial", "coreCategoryID", "lastDateExecuted", "lastIpExecuted", "lastUserExecuted", "parentCoreCategoryInfoDTO", "parentStatus", "parentTypeInfoDTO", "typeStatus", "getCategoryDefaultCode", "getCategoryEnableStatus", "()Ljava/lang/Integer;", "getCategoryName", "getCategoryNote", "getCategorySerial", "getCoreCategoryID", "getLastDateExecuted", "getLastIpExecuted", "getLastUserExecuted", "getParentCoreCategoryInfoDTO", "getParentStatus", "getParentTypeInfoDTO", "getTypeStatus", "setCategoryDefaultCode", "", "setCategoryEnableStatus", "(Ljava/lang/Integer;)V", "setCategoryName", "setCategoryNote", "setCategorySerial", "setCoreCategoryID", "setLastDateExecuted", "setLastIpExecuted", "setLastUserExecuted", "setParentCoreCategoryInfoDTO", "setParentStatus", "setParentTypeInfoDTO", "setTypeStatus", "app_debug"})
    public static final class CountryInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.Integer coreCategoryID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryDefaultCode")
        private java.lang.String categoryDefaultCode;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryName")
        private java.lang.String categoryName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryNote")
        private java.lang.String categoryNote;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryEnableStatus")
        private java.lang.Integer categoryEnableStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categorySerial")
        private java.lang.Integer categorySerial;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "typeStatus")
        private java.lang.Integer typeStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "parentStatus")
        private java.lang.Integer parentStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "parentTypeInfoDTO")
        private java.lang.Object parentTypeInfoDTO;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "parentCoreCategoryInfoDTO")
        private java.lang.Object parentCoreCategoryInfoDTO;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastUserExecuted")
        private java.lang.String lastUserExecuted;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastIpExecuted")
        private java.lang.String lastIpExecuted;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "lastDateExecuted")
        private java.lang.Integer lastDateExecuted;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryDefaultCode() {
            return null;
        }
        
        public final void setCategoryDefaultCode(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryDefaultCode) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryName() {
            return null;
        }
        
        public final void setCategoryName(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryNote() {
            return null;
        }
        
        public final void setCategoryNote(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryNote) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCategoryEnableStatus() {
            return null;
        }
        
        public final void setCategoryEnableStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer categoryEnableStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCategorySerial() {
            return null;
        }
        
        public final void setCategorySerial(@org.jetbrains.annotations.Nullable()
        java.lang.Integer categorySerial) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getTypeStatus() {
            return null;
        }
        
        public final void setTypeStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer typeStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getParentStatus() {
            return null;
        }
        
        public final void setParentStatus(@org.jetbrains.annotations.Nullable()
        java.lang.Integer parentStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getParentTypeInfoDTO() {
            return null;
        }
        
        public final void setParentTypeInfoDTO(@org.jetbrains.annotations.Nullable()
        java.lang.Object parentTypeInfoDTO) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getParentCoreCategoryInfoDTO() {
            return null;
        }
        
        public final void setParentCoreCategoryInfoDTO(@org.jetbrains.annotations.Nullable()
        java.lang.Object parentCoreCategoryInfoDTO) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLastUserExecuted() {
            return null;
        }
        
        public final void setLastUserExecuted(@org.jetbrains.annotations.Nullable()
        java.lang.String lastUserExecuted) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLastIpExecuted() {
            return null;
        }
        
        public final void setLastIpExecuted(@org.jetbrains.annotations.Nullable()
        java.lang.String lastIpExecuted) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getLastDateExecuted() {
            return null;
        }
        
        public final void setLastDateExecuted(@org.jetbrains.annotations.Nullable()
        java.lang.Integer lastDateExecuted) {
        }
        
        public CountryInfoDTO() {
            super();
        }
    }
}