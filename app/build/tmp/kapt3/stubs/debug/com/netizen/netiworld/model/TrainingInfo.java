package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001:\u0002\'(B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0013\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0014\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0015\u001a\u0004\u0018\u00010\u0006J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0006J\r\u0010\u0017\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u0018J\b\u0010\u0019\u001a\u0004\u0018\u00010\u000eJ\b\u0010\u001a\u001a\u0004\u0018\u00010\u0006J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0011J\u0010\u0010\u001c\u001a\u00020\u001d2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001e\u001a\u00020\u001d2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u001f\u001a\u00020\u001d2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006J\u0010\u0010 \u001a\u00020\u001d2\b\u0010\b\u001a\u0004\u0018\u00010\u0006J\u0010\u0010!\u001a\u00020\u001d2\b\u0010\t\u001a\u0004\u0018\u00010\u0006J\u0015\u0010\"\u001a\u00020\u001d2\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010#J\u0010\u0010$\u001a\u00020\u001d2\b\u0010\r\u001a\u0004\u0018\u00010\u000eJ\u0010\u0010%\u001a\u00020\u001d2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0006J\u0010\u0010&\u001a\u00020\u001d2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\fR\u0014\u0010\r\u001a\u0004\u0018\u00010\u000e8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00118\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"}, d2 = {"Lcom/netizen/netiworld/model/TrainingInfo;", "Ljava/io/Serializable;", "()V", "countryInfoDTO", "Lcom/netizen/netiworld/model/TrainingInfo$CountryInfoDTO;", "instituteLocation", "", "instituteName", "topicCoveredDetails", "trainingDuration", "trainingInfoId", "", "Ljava/lang/Integer;", "trainingSerial", "", "trainingTitle", "trainingYearInfoDTO", "Lcom/netizen/netiworld/model/TrainingInfo$TrainingYearInfoDTO;", "getCountryInfoDTO", "getInstituteLocation", "getInstituteName", "getTopicCoveredDetails", "getTrainingDuration", "getTrainingInfoId", "()Ljava/lang/Integer;", "getTrainingSerial", "getTrainingTitle", "getTrainingYearInfoDTO", "setCountryInfoDTO", "", "setInstituteLocation", "setInstituteName", "setTopicCoveredDetails", "setTrainingDuration", "setTrainingInfoId", "(Ljava/lang/Integer;)V", "setTrainingSerial", "setTrainingTitle", "setTrainingYearInfoDTO", "CountryInfoDTO", "TrainingYearInfoDTO", "app_debug"})
public final class TrainingInfo implements java.io.Serializable {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "trainingInfoId")
    private java.lang.Integer trainingInfoId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "trainingSerial")
    private java.lang.Object trainingSerial;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "trainingTitle")
    private java.lang.String trainingTitle;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "topicCoveredDetails")
    private java.lang.String topicCoveredDetails;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteName")
    private java.lang.String instituteName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteLocation")
    private java.lang.String instituteLocation;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "trainingDuration")
    private java.lang.String trainingDuration;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "countryInfoDTO")
    private com.netizen.netiworld.model.TrainingInfo.CountryInfoDTO countryInfoDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "trainingYearInfoDTO")
    private com.netizen.netiworld.model.TrainingInfo.TrainingYearInfoDTO trainingYearInfoDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getTrainingInfoId() {
        return null;
    }
    
    public final void setTrainingInfoId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer trainingInfoId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getTrainingSerial() {
        return null;
    }
    
    public final void setTrainingSerial(@org.jetbrains.annotations.Nullable()
    java.lang.Object trainingSerial) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTrainingTitle() {
        return null;
    }
    
    public final void setTrainingTitle(@org.jetbrains.annotations.Nullable()
    java.lang.String trainingTitle) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTopicCoveredDetails() {
        return null;
    }
    
    public final void setTopicCoveredDetails(@org.jetbrains.annotations.Nullable()
    java.lang.String topicCoveredDetails) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteName() {
        return null;
    }
    
    public final void setInstituteName(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteLocation() {
        return null;
    }
    
    public final void setInstituteLocation(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteLocation) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTrainingDuration() {
        return null;
    }
    
    public final void setTrainingDuration(@org.jetbrains.annotations.Nullable()
    java.lang.String trainingDuration) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.TrainingInfo.CountryInfoDTO getCountryInfoDTO() {
        return null;
    }
    
    public final void setCountryInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.TrainingInfo.CountryInfoDTO countryInfoDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.TrainingInfo.TrainingYearInfoDTO getTrainingYearInfoDTO() {
        return null;
    }
    
    public final void setTrainingYearInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.TrainingInfo.TrainingYearInfoDTO trainingYearInfoDTO) {
    }
    
    public TrainingInfo() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0010\u001a\u00020\u000e2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0011R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/netiworld/model/TrainingInfo$CountryInfoDTO;", "", "()V", "categoryDefaultCode", "", "categoryName", "coreCategoryID", "", "Ljava/lang/Integer;", "getCategoryDefaultCode", "getCategoryName", "getCoreCategoryID", "()Ljava/lang/Integer;", "setCategoryDefaultCode", "", "setCategoryName", "setCoreCategoryID", "(Ljava/lang/Integer;)V", "app_debug"})
    public static final class CountryInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.Integer coreCategoryID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryDefaultCode")
        private java.lang.String categoryDefaultCode;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryName")
        private java.lang.String categoryName;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryDefaultCode() {
            return null;
        }
        
        public final void setCategoryDefaultCode(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryDefaultCode) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryName() {
            return null;
        }
        
        public final void setCategoryName(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryName) {
        }
        
        public CountryInfoDTO() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0010\u001a\u00020\u000e2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0011R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006\u0012"}, d2 = {"Lcom/netizen/netiworld/model/TrainingInfo$TrainingYearInfoDTO;", "", "()V", "categoryDefaultCode", "", "categoryName", "coreCategoryID", "", "Ljava/lang/Integer;", "getCategoryDefaultCode", "getCategoryName", "getCoreCategoryID", "()Ljava/lang/Integer;", "setCategoryDefaultCode", "", "setCategoryName", "setCoreCategoryID", "(Ljava/lang/Integer;)V", "app_debug"})
    public static final class TrainingYearInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
        private java.lang.Integer coreCategoryID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryDefaultCode")
        private java.lang.String categoryDefaultCode;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "categoryName")
        private java.lang.String categoryName;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getCoreCategoryID() {
            return null;
        }
        
        public final void setCoreCategoryID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer coreCategoryID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryDefaultCode() {
            return null;
        }
        
        public final void setCategoryDefaultCode(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryDefaultCode) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCategoryName() {
            return null;
        }
        
        public final void setCategoryName(@org.jetbrains.annotations.Nullable()
        java.lang.String categoryName) {
        }
        
        public TrainingYearInfoDTO() {
            super();
        }
    }
}