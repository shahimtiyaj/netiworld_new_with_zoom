package com.netizen.netiworld.model.UserPoint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000%\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u001c\n\u0002\u0010\b\n\u0003\b\u0084\u0001\n\u0002\u0010\u0002\n\u0002\bP\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010U\u001a\u0004\u0018\u00010\u0004J\b\u0010V\u001a\u0004\u0018\u00010\u0004J\b\u0010W\u001a\u0004\u0018\u00010\u0004J\b\u0010X\u001a\u0004\u0018\u00010\u0004J\b\u0010Y\u001a\u0004\u0018\u00010\u0004J\b\u0010Z\u001a\u0004\u0018\u00010\u0001J\b\u0010[\u001a\u0004\u0018\u00010\u0004J\b\u0010\\\u001a\u0004\u0018\u00010\u0004J\b\u0010]\u001a\u0004\u0018\u00010\u0004J\b\u0010^\u001a\u0004\u0018\u00010\u0004J\b\u0010_\u001a\u0004\u0018\u00010\u0004J\b\u0010`\u001a\u0004\u0018\u00010\u0004J\b\u0010a\u001a\u0004\u0018\u00010\u0004J\b\u0010b\u001a\u0004\u0018\u00010\u0004J\b\u0010c\u001a\u0004\u0018\u00010\u0004J\b\u0010d\u001a\u0004\u0018\u00010\u0004J\b\u0010e\u001a\u0004\u0018\u00010\u0004J\b\u0010f\u001a\u0004\u0018\u00010\u0004J\b\u0010g\u001a\u0004\u0018\u00010\u0004J\b\u0010h\u001a\u0004\u0018\u00010\u0004J\b\u0010i\u001a\u0004\u0018\u00010\u0004J\b\u0010j\u001a\u0004\u0018\u00010\u0004J\b\u0010k\u001a\u0004\u0018\u00010\u0004J\b\u0010l\u001a\u0004\u0018\u00010\u0004J\b\u0010m\u001a\u0004\u0018\u00010\u0004J\b\u0010n\u001a\u0004\u0018\u00010\u0004J\b\u0010o\u001a\u0004\u0018\u00010\u0001J\b\u0010p\u001a\u0004\u0018\u00010\u0004J\r\u0010q\u001a\u0004\u0018\u00010!\u00a2\u0006\u0002\u0010rJ\b\u0010s\u001a\u0004\u0018\u00010\u0004J\b\u0010t\u001a\u0004\u0018\u00010\u0004J\b\u0010u\u001a\u0004\u0018\u00010\u0004J\b\u0010v\u001a\u0004\u0018\u00010\u0004J\b\u0010w\u001a\u0004\u0018\u00010\u0004J\b\u0010x\u001a\u0004\u0018\u00010\u0004J\b\u0010y\u001a\u0004\u0018\u00010\u0004J\b\u0010z\u001a\u0004\u0018\u00010\u0004J\b\u0010{\u001a\u0004\u0018\u00010\u0004J\b\u0010|\u001a\u0004\u0018\u00010\u0004J\b\u0010}\u001a\u0004\u0018\u00010\u0004J\b\u0010~\u001a\u0004\u0018\u00010\u0004J\b\u0010\u007f\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0080\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0081\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0082\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0083\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0084\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0085\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0086\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0087\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0088\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0089\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u008a\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u008b\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u008c\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u008d\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u008e\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u008f\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0090\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0091\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0092\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0093\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0094\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0095\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0096\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0097\u0001\u001a\u0004\u0018\u00010\u0001J\t\u0010\u0098\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u0099\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u009a\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u009b\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u009c\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u009d\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u009e\u0001\u001a\u0004\u0018\u00010\u0004J\u000e\u0010\u009f\u0001\u001a\u0004\u0018\u00010!\u00a2\u0006\u0002\u0010rJ\t\u0010\u00a0\u0001\u001a\u0004\u0018\u00010\u0001J\t\u0010\u00a1\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u00a2\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u00a3\u0001\u001a\u0004\u0018\u00010\u0004J\t\u0010\u00a4\u0001\u001a\u0004\u0018\u00010\u0001J\u0012\u0010\u00a5\u0001\u001a\u00030\u00a6\u00012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00a7\u0001\u001a\u00030\u00a6\u00012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00a8\u0001\u001a\u00030\u00a6\u00012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00a9\u0001\u001a\u00030\u00a6\u00012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00aa\u0001\u001a\u00030\u00a6\u00012\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00ab\u0001\u001a\u00030\u00a6\u00012\b\u0010\t\u001a\u0004\u0018\u00010\u0001J\u0012\u0010\u00ac\u0001\u001a\u00030\u00a6\u00012\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00ad\u0001\u001a\u00030\u00a6\u00012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00ae\u0001\u001a\u00030\u00a6\u00012\b\u0010\f\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00af\u0001\u001a\u00030\u00a6\u00012\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00b0\u0001\u001a\u00030\u00a6\u00012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00b1\u0001\u001a\u00030\u00a6\u00012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00b2\u0001\u001a\u00030\u00a6\u00012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00b3\u0001\u001a\u00030\u00a6\u00012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00b4\u0001\u001a\u00030\u00a6\u00012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00b5\u0001\u001a\u00030\u00a6\u00012\b\u0010\u0013\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00b6\u0001\u001a\u00030\u00a6\u00012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00b7\u0001\u001a\u00030\u00a6\u00012\b\u0010\u0015\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00b8\u0001\u001a\u00030\u00a6\u00012\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00b9\u0001\u001a\u00030\u00a6\u00012\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00ba\u0001\u001a\u00030\u00a6\u00012\b\u0010\u0018\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00bb\u0001\u001a\u00030\u00a6\u00012\b\u0010\u0019\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00bc\u0001\u001a\u00030\u00a6\u00012\b\u0010\u001a\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00bd\u0001\u001a\u00030\u00a6\u00012\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00be\u0001\u001a\u00030\u00a6\u00012\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00bf\u0001\u001a\u00030\u00a6\u00012\b\u0010\u001d\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00c0\u0001\u001a\u00030\u00a6\u00012\b\u0010\u001e\u001a\u0004\u0018\u00010\u0001J\u0012\u0010\u00c1\u0001\u001a\u00030\u00a6\u00012\b\u0010\u001f\u001a\u0004\u0018\u00010\u0004J\u0018\u0010\u00c2\u0001\u001a\u00030\u00a6\u00012\b\u0010 \u001a\u0004\u0018\u00010!\u00a2\u0006\u0003\u0010\u00c3\u0001J\u0012\u0010\u00c4\u0001\u001a\u00030\u00a6\u00012\b\u0010#\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00c5\u0001\u001a\u00030\u00a6\u00012\b\u0010$\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00c6\u0001\u001a\u00030\u00a6\u00012\b\u0010%\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00c7\u0001\u001a\u00030\u00a6\u00012\b\u0010&\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00c8\u0001\u001a\u00030\u00a6\u00012\b\u0010\'\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00c9\u0001\u001a\u00030\u00a6\u00012\b\u0010(\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00ca\u0001\u001a\u00030\u00a6\u00012\b\u0010)\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00cb\u0001\u001a\u00030\u00a6\u00012\b\u0010*\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00cc\u0001\u001a\u00030\u00a6\u00012\b\u0010+\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00cd\u0001\u001a\u00030\u00a6\u00012\b\u0010,\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00ce\u0001\u001a\u00030\u00a6\u00012\b\u0010-\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00cf\u0001\u001a\u00030\u00a6\u00012\b\u0010.\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00d0\u0001\u001a\u00030\u00a6\u00012\b\u0010/\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00d1\u0001\u001a\u00030\u00a6\u00012\b\u00100\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00d2\u0001\u001a\u00030\u00a6\u00012\b\u00101\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00d3\u0001\u001a\u00030\u00a6\u00012\b\u00102\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00d4\u0001\u001a\u00030\u00a6\u00012\b\u00103\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00d5\u0001\u001a\u00030\u00a6\u00012\b\u00104\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00d6\u0001\u001a\u00030\u00a6\u00012\b\u00105\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00d7\u0001\u001a\u00030\u00a6\u00012\b\u00106\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00d8\u0001\u001a\u00030\u00a6\u00012\b\u00107\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00d9\u0001\u001a\u00030\u00a6\u00012\b\u00108\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00da\u0001\u001a\u00030\u00a6\u00012\b\u00109\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00db\u0001\u001a\u00030\u00a6\u00012\b\u0010:\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00dc\u0001\u001a\u00030\u00a6\u00012\b\u0010;\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00dd\u0001\u001a\u00030\u00a6\u00012\b\u0010<\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00de\u0001\u001a\u00030\u00a6\u00012\b\u0010=\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00df\u0001\u001a\u00030\u00a6\u00012\b\u0010>\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00e0\u0001\u001a\u00030\u00a6\u00012\b\u0010?\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00e1\u0001\u001a\u00030\u00a6\u00012\b\u0010@\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00e2\u0001\u001a\u00030\u00a6\u00012\b\u0010A\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00e3\u0001\u001a\u00030\u00a6\u00012\b\u0010B\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00e4\u0001\u001a\u00030\u00a6\u00012\b\u0010C\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00e5\u0001\u001a\u00030\u00a6\u00012\b\u0010D\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00e6\u0001\u001a\u00030\u00a6\u00012\b\u0010E\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00e7\u0001\u001a\u00030\u00a6\u00012\b\u0010F\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00e8\u0001\u001a\u00030\u00a6\u00012\b\u0010G\u001a\u0004\u0018\u00010\u0001J\u0012\u0010\u00e9\u0001\u001a\u00030\u00a6\u00012\b\u0010H\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00ea\u0001\u001a\u00030\u00a6\u00012\b\u0010I\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00eb\u0001\u001a\u00030\u00a6\u00012\b\u0010J\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00ec\u0001\u001a\u00030\u00a6\u00012\b\u0010K\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00ed\u0001\u001a\u00030\u00a6\u00012\b\u0010L\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00ee\u0001\u001a\u00030\u00a6\u00012\b\u0010M\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00ef\u0001\u001a\u00030\u00a6\u00012\b\u0010N\u001a\u0004\u0018\u00010\u0004J\u0018\u0010\u00f0\u0001\u001a\u00030\u00a6\u00012\b\u0010O\u001a\u0004\u0018\u00010!\u00a2\u0006\u0003\u0010\u00c3\u0001J\u0012\u0010\u00f1\u0001\u001a\u00030\u00a6\u00012\b\u0010P\u001a\u0004\u0018\u00010\u0001J\u0012\u0010\u00f2\u0001\u001a\u00030\u00a6\u00012\b\u0010Q\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00f3\u0001\u001a\u00030\u00a6\u00012\b\u0010R\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00f4\u0001\u001a\u00030\u00a6\u00012\b\u0010S\u001a\u0004\u0018\u00010\u0004J\u0012\u0010\u00f5\u0001\u001a\u00030\u00a6\u00012\b\u0010T\u001a\u0004\u0018\u00010\u0001R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001a\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001c\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001d\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001e\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010 \u001a\u0004\u0018\u00010!8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\"R\u0014\u0010#\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010$\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010%\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010&\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\'\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010(\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010)\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010*\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010+\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010,\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010-\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010.\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010/\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00100\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00101\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00102\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00103\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00104\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00105\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00106\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00107\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00108\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u00109\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010:\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010;\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010<\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010=\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010>\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010?\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010@\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010A\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010B\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010C\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010D\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010E\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010F\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010G\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010H\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010I\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010J\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010K\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010L\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010M\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010N\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010O\u001a\u0004\u0018\u00010!8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\"R\u0014\u0010P\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010Q\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010R\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010S\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010T\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u00f6\u0001"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/StudentPortalProfile;", "", "()V", "academicYear", "", "addressType", "admissionCategory", "admissionDate", "admissionYear", "birthCertificateNo", "bloodGroup", "board", "classConfigId", "className", "country", "customStudentId", "district", "division", "exGrade", "exPoint", "exSession", "examName", "exgroup", "fatherName", "fathersNID", "groupName", "guardianEducation", "guardianEmail", "guardianMobile", "guardianName", "height", "houseNo", "identificationId", "", "Ljava/lang/Integer;", "imageName", "imagePath", "instituteAddress", "instituteEmail", "instituteId", "instituteMobile", "instituteName", "institutePhone", "lastEducation", "logoName", "logoPath", "monthlyIncome", "motherName", "mothersNID", "nationality", "occupation", "passingYear", "postOffice", "postalCode", "presentDistrict", "presentDivision", "presentHouseNo", "presentPostOffice", "presentPostalCode", "presentRoadNo", "presentThana", "presentVillage", "previousInstituteAddress", "previousInstituteCode", "previousInstituteName", "previousStdRoll", "regId", "relation", "roadNo", "sectionName", "shiftName", "specialDisease", "studentDOB", "studentEmail", "studentGender", "studentId", "studentMobile", "studentName", "studentReligion", "studentRoll", "tcNo", "thana", "timePeriod", "village", "weight", "getAcademicYear", "getAddressType", "getAdmissionCategory", "getAdmissionDate", "getAdmissionYear", "getBirthCertificateNo", "getBloodGroup", "getBoard", "getClassConfigId", "getClassName", "getCountry", "getCustomStudentId", "getDistrict", "getDivision", "getExGrade", "getExPoint", "getExSession", "getExamName", "getExgroup", "getFatherName", "getFathersNID", "getGroupName", "getGuardianEducation", "getGuardianEmail", "getGuardianMobile", "getGuardianName", "getHeight", "getHouseNo", "getIdentificationId", "()Ljava/lang/Integer;", "getImageName", "getImagePath", "getInstituteAddress", "getInstituteEmail", "getInstituteId", "getInstituteMobile", "getInstituteName", "getInstitutePhone", "getLastEducation", "getLogoName", "getLogoPath", "getMonthlyIncome", "getMotherName", "getMothersNID", "getNationality", "getOccupation", "getPassingYear", "getPostOffice", "getPostalCode", "getPresentDistrict", "getPresentDivision", "getPresentHouseNo", "getPresentPostOffice", "getPresentPostalCode", "getPresentRoadNo", "getPresentThana", "getPresentVillage", "getPreviousInstituteAddress", "getPreviousInstituteCode", "getPreviousInstituteName", "getPreviousStdRoll", "getRegId", "getRelation", "getRoadNo", "getSectionName", "getShiftName", "getSpecialDisease", "getStudentDOB", "getStudentEmail", "getStudentGender", "getStudentId", "getStudentMobile", "getStudentName", "getStudentReligion", "getStudentRoll", "getTcNo", "getThana", "getTimePeriod", "getVillage", "getWeight", "setAcademicYear", "", "setAddressType", "setAdmissionCategory", "setAdmissionDate", "setAdmissionYear", "setBirthCertificateNo", "setBloodGroup", "setBoard", "setClassConfigId", "setClassName", "setCountry", "setCustomStudentId", "setDistrict", "setDivision", "setExGrade", "setExPoint", "setExSession", "setExamName", "setExgroup", "setFatherName", "setFathersNID", "setGroupName", "setGuardianEducation", "setGuardianEmail", "setGuardianMobile", "setGuardianName", "setHeight", "setHouseNo", "setIdentificationId", "(Ljava/lang/Integer;)V", "setImageName", "setImagePath", "setInstituteAddress", "setInstituteEmail", "setInstituteId", "setInstituteMobile", "setInstituteName", "setInstitutePhone", "setLastEducation", "setLogoName", "setLogoPath", "setMonthlyIncome", "setMotherName", "setMothersNID", "setNationality", "setOccupation", "setPassingYear", "setPostOffice", "setPostalCode", "setPresentDistrict", "setPresentDivision", "setPresentHouseNo", "setPresentPostOffice", "setPresentPostalCode", "setPresentRoadNo", "setPresentThana", "setPresentVillage", "setPreviousInstituteAddress", "setPreviousInstituteCode", "setPreviousInstituteName", "setPreviousStdRoll", "setRegId", "setRelation", "setRoadNo", "setSectionName", "setShiftName", "setSpecialDisease", "setStudentDOB", "setStudentEmail", "setStudentGender", "setStudentId", "setStudentMobile", "setStudentName", "setStudentReligion", "setStudentRoll", "setTcNo", "setThana", "setTimePeriod", "setVillage", "setWeight", "app_debug"})
public final class StudentPortalProfile {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "logoName")
    private java.lang.String logoName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "fatherName")
    private java.lang.String fatherName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "country")
    private java.lang.String country;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "occupation")
    private java.lang.String occupation;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "lastEducation")
    private java.lang.String lastEducation;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "imagePath")
    private java.lang.String imagePath;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "postalCode")
    private java.lang.String postalCode;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "studentDOB")
    private java.lang.String studentDOB;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "birthCertificateNo")
    private java.lang.Object birthCertificateNo;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "studentRoll")
    private java.lang.Integer studentRoll;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "mothersNID")
    private java.lang.String mothersNID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "relation")
    private java.lang.String relation;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "division")
    private java.lang.String division;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "shiftName")
    private java.lang.String shiftName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "bloodGroup")
    private java.lang.String bloodGroup;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "academicYear")
    private java.lang.String academicYear;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "admissionYear")
    private java.lang.String admissionYear;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "houseNo")
    private java.lang.String houseNo;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "previousInstituteCode")
    private java.lang.String previousInstituteCode;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteMobile")
    private java.lang.String instituteMobile;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "customStudentId")
    private java.lang.String customStudentId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "identificationId")
    private java.lang.Integer identificationId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "postOffice")
    private java.lang.String postOffice;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "height")
    private java.lang.Object height;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "presentPostalCode")
    private java.lang.String presentPostalCode;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "admissionDate")
    private java.lang.String admissionDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "imageName")
    private java.lang.String imageName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "tcNo")
    private java.lang.Object tcNo;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "addressType")
    private java.lang.String addressType;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "logoPath")
    private java.lang.String logoPath;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "examName")
    private java.lang.String examName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "motherName")
    private java.lang.String motherName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "presentHouseNo")
    private java.lang.String presentHouseNo;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "weight")
    private java.lang.Object weight;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "presentThana")
    private java.lang.String presentThana;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "exPoint")
    private java.lang.String exPoint;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "presentDivision")
    private java.lang.String presentDivision;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "passingYear")
    private java.lang.String passingYear;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "groupName")
    private java.lang.String groupName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "nationality")
    private java.lang.String nationality;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "studentName")
    private java.lang.String studentName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "district")
    private java.lang.String district;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "presentVillage")
    private java.lang.String presentVillage;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "timePeriod")
    private java.lang.String timePeriod;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteId")
    private java.lang.String instituteId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "regId")
    private java.lang.String regId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "guardianEmail")
    private java.lang.String guardianEmail;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "presentPostOffice")
    private java.lang.String presentPostOffice;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "presentDistrict")
    private java.lang.String presentDistrict;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "className")
    private java.lang.String className;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteAddress")
    private java.lang.String instituteAddress;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteName")
    private java.lang.String instituteName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "sectionName")
    private java.lang.String sectionName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "studentId")
    private java.lang.String studentId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "studentEmail")
    private java.lang.String studentEmail;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "studentGender")
    private java.lang.String studentGender;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "classConfigId")
    private java.lang.String classConfigId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "roadNo")
    private java.lang.String roadNo;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "guardianName")
    private java.lang.String guardianName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "studentReligion")
    private java.lang.String studentReligion;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "exgroup")
    private java.lang.String exgroup;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "institutePhone")
    private java.lang.String institutePhone;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "village")
    private java.lang.String village;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "exGrade")
    private java.lang.String exGrade;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "guardianEducation")
    private java.lang.String guardianEducation;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "monthlyIncome")
    private java.lang.String monthlyIncome;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "previousStdRoll")
    private java.lang.String previousStdRoll;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "specialDisease")
    private java.lang.Object specialDisease;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "fathersNID")
    private java.lang.String fathersNID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "guardianMobile")
    private java.lang.String guardianMobile;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "admissionCategory")
    private java.lang.String admissionCategory;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "studentMobile")
    private java.lang.String studentMobile;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "previousInstituteAddress")
    private java.lang.String previousInstituteAddress;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "presentRoadNo")
    private java.lang.String presentRoadNo;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "instituteEmail")
    private java.lang.String instituteEmail;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "thana")
    private java.lang.String thana;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "exSession")
    private java.lang.String exSession;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "board")
    private java.lang.String board;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "previousInstituteName")
    private java.lang.String previousInstituteName;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLogoName() {
        return null;
    }
    
    public final void setLogoName(@org.jetbrains.annotations.Nullable()
    java.lang.String logoName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFatherName() {
        return null;
    }
    
    public final void setFatherName(@org.jetbrains.annotations.Nullable()
    java.lang.String fatherName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCountry() {
        return null;
    }
    
    public final void setCountry(@org.jetbrains.annotations.Nullable()
    java.lang.String country) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getOccupation() {
        return null;
    }
    
    public final void setOccupation(@org.jetbrains.annotations.Nullable()
    java.lang.String occupation) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLastEducation() {
        return null;
    }
    
    public final void setLastEducation(@org.jetbrains.annotations.Nullable()
    java.lang.String lastEducation) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getImagePath() {
        return null;
    }
    
    public final void setImagePath(@org.jetbrains.annotations.Nullable()
    java.lang.String imagePath) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPostalCode() {
        return null;
    }
    
    public final void setPostalCode(@org.jetbrains.annotations.Nullable()
    java.lang.String postalCode) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getStudentDOB() {
        return null;
    }
    
    public final void setStudentDOB(@org.jetbrains.annotations.Nullable()
    java.lang.String studentDOB) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getBirthCertificateNo() {
        return null;
    }
    
    public final void setBirthCertificateNo(@org.jetbrains.annotations.Nullable()
    java.lang.Object birthCertificateNo) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getStudentRoll() {
        return null;
    }
    
    public final void setStudentRoll(@org.jetbrains.annotations.Nullable()
    java.lang.Integer studentRoll) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMothersNID() {
        return null;
    }
    
    public final void setMothersNID(@org.jetbrains.annotations.Nullable()
    java.lang.String mothersNID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRelation() {
        return null;
    }
    
    public final void setRelation(@org.jetbrains.annotations.Nullable()
    java.lang.String relation) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDivision() {
        return null;
    }
    
    public final void setDivision(@org.jetbrains.annotations.Nullable()
    java.lang.String division) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getShiftName() {
        return null;
    }
    
    public final void setShiftName(@org.jetbrains.annotations.Nullable()
    java.lang.String shiftName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBloodGroup() {
        return null;
    }
    
    public final void setBloodGroup(@org.jetbrains.annotations.Nullable()
    java.lang.String bloodGroup) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAcademicYear() {
        return null;
    }
    
    public final void setAcademicYear(@org.jetbrains.annotations.Nullable()
    java.lang.String academicYear) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAdmissionYear() {
        return null;
    }
    
    public final void setAdmissionYear(@org.jetbrains.annotations.Nullable()
    java.lang.String admissionYear) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getHouseNo() {
        return null;
    }
    
    public final void setHouseNo(@org.jetbrains.annotations.Nullable()
    java.lang.String houseNo) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPreviousInstituteCode() {
        return null;
    }
    
    public final void setPreviousInstituteCode(@org.jetbrains.annotations.Nullable()
    java.lang.String previousInstituteCode) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteMobile() {
        return null;
    }
    
    public final void setInstituteMobile(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteMobile) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCustomStudentId() {
        return null;
    }
    
    public final void setCustomStudentId(@org.jetbrains.annotations.Nullable()
    java.lang.String customStudentId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getIdentificationId() {
        return null;
    }
    
    public final void setIdentificationId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer identificationId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPostOffice() {
        return null;
    }
    
    public final void setPostOffice(@org.jetbrains.annotations.Nullable()
    java.lang.String postOffice) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getHeight() {
        return null;
    }
    
    public final void setHeight(@org.jetbrains.annotations.Nullable()
    java.lang.Object height) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPresentPostalCode() {
        return null;
    }
    
    public final void setPresentPostalCode(@org.jetbrains.annotations.Nullable()
    java.lang.String presentPostalCode) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAdmissionDate() {
        return null;
    }
    
    public final void setAdmissionDate(@org.jetbrains.annotations.Nullable()
    java.lang.String admissionDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getImageName() {
        return null;
    }
    
    public final void setImageName(@org.jetbrains.annotations.Nullable()
    java.lang.String imageName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getTcNo() {
        return null;
    }
    
    public final void setTcNo(@org.jetbrains.annotations.Nullable()
    java.lang.Object tcNo) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAddressType() {
        return null;
    }
    
    public final void setAddressType(@org.jetbrains.annotations.Nullable()
    java.lang.String addressType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLogoPath() {
        return null;
    }
    
    public final void setLogoPath(@org.jetbrains.annotations.Nullable()
    java.lang.String logoPath) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getExamName() {
        return null;
    }
    
    public final void setExamName(@org.jetbrains.annotations.Nullable()
    java.lang.String examName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMotherName() {
        return null;
    }
    
    public final void setMotherName(@org.jetbrains.annotations.Nullable()
    java.lang.String motherName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPresentHouseNo() {
        return null;
    }
    
    public final void setPresentHouseNo(@org.jetbrains.annotations.Nullable()
    java.lang.String presentHouseNo) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getWeight() {
        return null;
    }
    
    public final void setWeight(@org.jetbrains.annotations.Nullable()
    java.lang.Object weight) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPresentThana() {
        return null;
    }
    
    public final void setPresentThana(@org.jetbrains.annotations.Nullable()
    java.lang.String presentThana) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getExPoint() {
        return null;
    }
    
    public final void setExPoint(@org.jetbrains.annotations.Nullable()
    java.lang.String exPoint) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPresentDivision() {
        return null;
    }
    
    public final void setPresentDivision(@org.jetbrains.annotations.Nullable()
    java.lang.String presentDivision) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPassingYear() {
        return null;
    }
    
    public final void setPassingYear(@org.jetbrains.annotations.Nullable()
    java.lang.String passingYear) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getGroupName() {
        return null;
    }
    
    public final void setGroupName(@org.jetbrains.annotations.Nullable()
    java.lang.String groupName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getNationality() {
        return null;
    }
    
    public final void setNationality(@org.jetbrains.annotations.Nullable()
    java.lang.String nationality) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getStudentName() {
        return null;
    }
    
    public final void setStudentName(@org.jetbrains.annotations.Nullable()
    java.lang.String studentName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDistrict() {
        return null;
    }
    
    public final void setDistrict(@org.jetbrains.annotations.Nullable()
    java.lang.String district) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPresentVillage() {
        return null;
    }
    
    public final void setPresentVillage(@org.jetbrains.annotations.Nullable()
    java.lang.String presentVillage) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTimePeriod() {
        return null;
    }
    
    public final void setTimePeriod(@org.jetbrains.annotations.Nullable()
    java.lang.String timePeriod) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteId() {
        return null;
    }
    
    public final void setInstituteId(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRegId() {
        return null;
    }
    
    public final void setRegId(@org.jetbrains.annotations.Nullable()
    java.lang.String regId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getGuardianEmail() {
        return null;
    }
    
    public final void setGuardianEmail(@org.jetbrains.annotations.Nullable()
    java.lang.String guardianEmail) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPresentPostOffice() {
        return null;
    }
    
    public final void setPresentPostOffice(@org.jetbrains.annotations.Nullable()
    java.lang.String presentPostOffice) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPresentDistrict() {
        return null;
    }
    
    public final void setPresentDistrict(@org.jetbrains.annotations.Nullable()
    java.lang.String presentDistrict) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getClassName() {
        return null;
    }
    
    public final void setClassName(@org.jetbrains.annotations.Nullable()
    java.lang.String className) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteAddress() {
        return null;
    }
    
    public final void setInstituteAddress(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteAddress) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteName() {
        return null;
    }
    
    public final void setInstituteName(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSectionName() {
        return null;
    }
    
    public final void setSectionName(@org.jetbrains.annotations.Nullable()
    java.lang.String sectionName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getStudentId() {
        return null;
    }
    
    public final void setStudentId(@org.jetbrains.annotations.Nullable()
    java.lang.String studentId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getStudentEmail() {
        return null;
    }
    
    public final void setStudentEmail(@org.jetbrains.annotations.Nullable()
    java.lang.String studentEmail) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getStudentGender() {
        return null;
    }
    
    public final void setStudentGender(@org.jetbrains.annotations.Nullable()
    java.lang.String studentGender) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getClassConfigId() {
        return null;
    }
    
    public final void setClassConfigId(@org.jetbrains.annotations.Nullable()
    java.lang.String classConfigId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRoadNo() {
        return null;
    }
    
    public final void setRoadNo(@org.jetbrains.annotations.Nullable()
    java.lang.String roadNo) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getGuardianName() {
        return null;
    }
    
    public final void setGuardianName(@org.jetbrains.annotations.Nullable()
    java.lang.String guardianName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getStudentReligion() {
        return null;
    }
    
    public final void setStudentReligion(@org.jetbrains.annotations.Nullable()
    java.lang.String studentReligion) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getExgroup() {
        return null;
    }
    
    public final void setExgroup(@org.jetbrains.annotations.Nullable()
    java.lang.String exgroup) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstitutePhone() {
        return null;
    }
    
    public final void setInstitutePhone(@org.jetbrains.annotations.Nullable()
    java.lang.String institutePhone) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getVillage() {
        return null;
    }
    
    public final void setVillage(@org.jetbrains.annotations.Nullable()
    java.lang.String village) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getExGrade() {
        return null;
    }
    
    public final void setExGrade(@org.jetbrains.annotations.Nullable()
    java.lang.String exGrade) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getGuardianEducation() {
        return null;
    }
    
    public final void setGuardianEducation(@org.jetbrains.annotations.Nullable()
    java.lang.String guardianEducation) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMonthlyIncome() {
        return null;
    }
    
    public final void setMonthlyIncome(@org.jetbrains.annotations.Nullable()
    java.lang.String monthlyIncome) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPreviousStdRoll() {
        return null;
    }
    
    public final void setPreviousStdRoll(@org.jetbrains.annotations.Nullable()
    java.lang.String previousStdRoll) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getSpecialDisease() {
        return null;
    }
    
    public final void setSpecialDisease(@org.jetbrains.annotations.Nullable()
    java.lang.Object specialDisease) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFathersNID() {
        return null;
    }
    
    public final void setFathersNID(@org.jetbrains.annotations.Nullable()
    java.lang.String fathersNID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getGuardianMobile() {
        return null;
    }
    
    public final void setGuardianMobile(@org.jetbrains.annotations.Nullable()
    java.lang.String guardianMobile) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAdmissionCategory() {
        return null;
    }
    
    public final void setAdmissionCategory(@org.jetbrains.annotations.Nullable()
    java.lang.String admissionCategory) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getStudentMobile() {
        return null;
    }
    
    public final void setStudentMobile(@org.jetbrains.annotations.Nullable()
    java.lang.String studentMobile) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPreviousInstituteAddress() {
        return null;
    }
    
    public final void setPreviousInstituteAddress(@org.jetbrains.annotations.Nullable()
    java.lang.String previousInstituteAddress) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPresentRoadNo() {
        return null;
    }
    
    public final void setPresentRoadNo(@org.jetbrains.annotations.Nullable()
    java.lang.String presentRoadNo) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getInstituteEmail() {
        return null;
    }
    
    public final void setInstituteEmail(@org.jetbrains.annotations.Nullable()
    java.lang.String instituteEmail) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getThana() {
        return null;
    }
    
    public final void setThana(@org.jetbrains.annotations.Nullable()
    java.lang.String thana) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getExSession() {
        return null;
    }
    
    public final void setExSession(@org.jetbrains.annotations.Nullable()
    java.lang.String exSession) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBoard() {
        return null;
    }
    
    public final void setBoard(@org.jetbrains.annotations.Nullable()
    java.lang.String board) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPreviousInstituteName() {
        return null;
    }
    
    public final void setPreviousInstituteName(@org.jetbrains.annotations.Nullable()
    java.lang.String previousInstituteName) {
    }
    
    public StudentPortalProfile() {
        super();
    }
}