package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001:\u0001\u000fB\u001b\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R \u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR \u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u0010"}, d2 = {"Lcom/netizen/netiworld/model/MessageType1;", "", "productRoleAssignID", "", "productInfoDTO", "Lcom/netizen/netiworld/model/MessageType1$ProductInfoDTO;", "(Ljava/lang/String;Lcom/netizen/netiworld/model/MessageType1$ProductInfoDTO;)V", "getProductInfoDTO", "()Lcom/netizen/netiworld/model/MessageType1$ProductInfoDTO;", "setProductInfoDTO", "(Lcom/netizen/netiworld/model/MessageType1$ProductInfoDTO;)V", "getProductRoleAssignID", "()Ljava/lang/String;", "setProductRoleAssignID", "(Ljava/lang/String;)V", "ProductInfoDTO", "app_debug"})
public final class MessageType1 {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productRoleAssignID")
    private java.lang.String productRoleAssignID;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "productInfoDTO")
    private com.netizen.netiworld.model.MessageType1.ProductInfoDTO productInfoDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getProductRoleAssignID() {
        return null;
    }
    
    public final void setProductRoleAssignID(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.MessageType1.ProductInfoDTO getProductInfoDTO() {
        return null;
    }
    
    public final void setProductInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.MessageType1.ProductInfoDTO p0) {
    }
    
    public MessageType1(@org.jetbrains.annotations.Nullable()
    java.lang.String productRoleAssignID, @org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.MessageType1.ProductInfoDTO productInfoDTO) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\n\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002B\u001b\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007J\r\u0010\r\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u000eJ\r\u0010\u000f\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0010J\b\u0010\u0011\u001a\u0004\u0018\u00010\u0006J\r\u0010\u0012\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u000eJ\u0015\u0010\u0013\u001a\u00020\u00142\b\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u0015J\u0015\u0010\u0016\u001a\u00020\u00142\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0017J\u0010\u0010\u0018\u001a\u00020\u00142\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0015\u0010\u0019\u001a\u00020\u00142\b\u0010\f\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\u0015R\u0016\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\nR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000bR\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\n\u00a8\u0006\u001a"}, d2 = {"Lcom/netizen/netiworld/model/MessageType1$ProductInfoDTO;", "", "()V", "productID", "", "productName", "", "(Ljava/lang/Long;Ljava/lang/String;)V", "percentVat", "", "Ljava/lang/Double;", "Ljava/lang/Long;", "salesPrice", "getPercentVat", "()Ljava/lang/Double;", "getProductID", "()Ljava/lang/Long;", "getProductName", "getSalesPrice", "setPercentVat", "", "(Ljava/lang/Double;)V", "setProductID", "(Ljava/lang/Long;)V", "setProductName", "setSalesPrice", "app_debug"})
    public static final class ProductInfoDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "productID")
        private java.lang.Long productID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "productName")
        private java.lang.String productName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "salesPrice")
        private java.lang.Double salesPrice;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "percentVat")
        private java.lang.Double percentVat;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Long getProductID() {
            return null;
        }
        
        public final void setProductID(@org.jetbrains.annotations.Nullable()
        java.lang.Long productID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getProductName() {
            return null;
        }
        
        public final void setProductName(@org.jetbrains.annotations.Nullable()
        java.lang.String productName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Double getSalesPrice() {
            return null;
        }
        
        public final void setSalesPrice(@org.jetbrains.annotations.Nullable()
        java.lang.Double salesPrice) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Double getPercentVat() {
            return null;
        }
        
        public final void setPercentVat(@org.jetbrains.annotations.Nullable()
        java.lang.Double percentVat) {
        }
        
        public ProductInfoDTO() {
            super();
        }
        
        public ProductInfoDTO(@org.jetbrains.annotations.Nullable()
        java.lang.Long productID, @org.jetbrains.annotations.Nullable()
        java.lang.String productName) {
            super();
        }
    }
}