package com.netizen.netiworld.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u0006\u00a8\u0006\t"}, d2 = {"Lcom/netizen/netiworld/utils/JWTUtils;", "", "()V", "decoded", "", "JWTEncoded", "", "getJson", "strEncoded", "app_debug"})
public final class JWTUtils {
    public static final com.netizen.netiworld.utils.JWTUtils INSTANCE = null;
    
    public final void decoded(@org.jetbrains.annotations.NotNull()
    java.lang.String JWTEncoded) throws java.lang.Exception {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getJson(@org.jetbrains.annotations.NotNull()
    java.lang.String strEncoded) throws java.io.UnsupportedEncodingException {
        return null;
    }
    
    private JWTUtils() {
        super();
    }
}