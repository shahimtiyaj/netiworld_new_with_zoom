package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u0010\n\u001a\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/netizen/netiworld/model/AreaModel;", "", "()V", "categoryName", "", "coreCategoryID", "getCategoryName", "getCoreCategoryID", "setCategoryName", "", "setCoreCategoryID", "app_debug"})
public final class AreaModel {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "coreCategoryID")
    private java.lang.String coreCategoryID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "categoryName")
    private java.lang.String categoryName;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCoreCategoryID() {
        return null;
    }
    
    public final void setCoreCategoryID(@org.jetbrains.annotations.NotNull()
    java.lang.String coreCategoryID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCategoryName() {
        return null;
    }
    
    public final void setCategoryName(@org.jetbrains.annotations.NotNull()
    java.lang.String categoryName) {
    }
    
    public AreaModel() {
        super();
    }
}