package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b$\n\u0002\u0010\u0002\n\u0002\b \b\u0007\u0018\u00002\u00020\u0001:\u0001fB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\'\u001a\u0004\u0018\u00010\u0004J\b\u0010(\u001a\u0004\u0018\u00010\u0004J\b\u0010)\u001a\u0004\u0018\u00010\u0004J\b\u0010*\u001a\u0004\u0018\u00010\u0004J\b\u0010+\u001a\u0004\u0018\u00010\u0004J\b\u0010,\u001a\u0004\u0018\u00010\u0004J\b\u0010-\u001a\u0004\u0018\u00010\u0004J\b\u0010.\u001a\u0004\u0018\u00010\u0004J\b\u0010/\u001a\u0004\u0018\u00010\u0004J\b\u00100\u001a\u0004\u0018\u00010\u0004J\b\u00101\u001a\u0004\u0018\u00010\u0004J\b\u00102\u001a\u0004\u0018\u00010\u0010J\b\u00103\u001a\u0004\u0018\u00010\u0004J\b\u00104\u001a\u0004\u0018\u00010\u0004J\b\u00105\u001a\u0004\u0018\u00010\u0010J\b\u00106\u001a\u0004\u0018\u00010\u0010J\r\u00107\u001a\u0004\u0018\u00010\u0016\u00a2\u0006\u0002\u00108J\r\u00109\u001a\u0004\u0018\u00010\u0016\u00a2\u0006\u0002\u00108J\b\u0010:\u001a\u0004\u0018\u00010\u0010J\b\u0010;\u001a\u0004\u0018\u00010\u0004J\b\u0010<\u001a\u0004\u0018\u00010\u0004J\b\u0010=\u001a\u0004\u0018\u00010\u0004J\r\u0010>\u001a\u0004\u0018\u00010\u001e\u00a2\u0006\u0002\u0010?J\b\u0010@\u001a\u0004\u0018\u00010\u0004J\b\u0010A\u001a\u0004\u0018\u00010\"J\b\u0010B\u001a\u0004\u0018\u00010\u0004J\r\u0010C\u001a\u0004\u0018\u00010\u001e\u00a2\u0006\u0002\u0010?J\b\u0010D\u001a\u0004\u0018\u00010\u0004J\b\u0010E\u001a\u0004\u0018\u00010\u0010J\u0010\u0010F\u001a\u00020G2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010H\u001a\u00020G2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010I\u001a\u00020G2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0010\u0010J\u001a\u00020G2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004J\u0010\u0010K\u001a\u00020G2\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010L\u001a\u00020G2\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\u0010\u0010M\u001a\u00020G2\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\u0010\u0010N\u001a\u00020G2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010O\u001a\u00020G2\b\u0010\f\u001a\u0004\u0018\u00010\u0004J\u0010\u0010P\u001a\u00020G2\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\u0010\u0010Q\u001a\u00020G2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\u0010\u0010R\u001a\u00020G2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010J\u0010\u0010S\u001a\u00020G2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\u0010\u0010T\u001a\u00020G2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\u0010\u0010U\u001a\u00020G2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0010J\u0010\u0010V\u001a\u00020G2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0010J\u0015\u0010W\u001a\u00020G2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u00a2\u0006\u0002\u0010XJ\u0015\u0010Y\u001a\u00020G2\b\u0010\u0018\u001a\u0004\u0018\u00010\u0016\u00a2\u0006\u0002\u0010XJ\u0010\u0010Z\u001a\u00020G2\b\u0010\u0019\u001a\u0004\u0018\u00010\u0010J\u0010\u0010[\u001a\u00020G2\b\u0010\u001a\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\\\u001a\u00020G2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010]\u001a\u00020G2\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\u0015\u0010^\u001a\u00020G2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001e\u00a2\u0006\u0002\u0010_J\u0010\u0010`\u001a\u00020G2\b\u0010 \u001a\u0004\u0018\u00010\u0004J\u0010\u0010a\u001a\u00020G2\b\u0010!\u001a\u0004\u0018\u00010\"J\u0010\u0010b\u001a\u00020G2\b\u0010#\u001a\u0004\u0018\u00010\u0004J\u0015\u0010c\u001a\u00020G2\b\u0010$\u001a\u0004\u0018\u00010\u001e\u00a2\u0006\u0002\u0010_J\u0010\u0010d\u001a\u00020G2\b\u0010%\u001a\u0004\u0018\u00010\u0004J\u0010\u0010e\u001a\u00020G2\b\u0010&\u001a\u0004\u0018\u00010\u0010R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0015\u001a\u0004\u0018\u00010\u00168\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0017R\u0016\u0010\u0018\u001a\u0004\u0018\u00010\u00168\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0017R\u0014\u0010\u0019\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001a\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001c\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001d\u001a\u0004\u0018\u00010\u001e8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u001fR\u0014\u0010 \u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010!\u001a\u0004\u0018\u00010\"8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010#\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010$\u001a\u0004\u0018\u00010\u001e8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u001fR\u0014\u0010%\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010&\u001a\u0004\u0018\u00010\u00108\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006g"}, d2 = {"Lcom/netizen/netiworld/model/ProfileInformation;", "Ljava/io/Serializable;", "()V", "age", "", "area", "basicEmail", "basicMobile", "bloodGroup", "customNetiID", "dateOfBirth", "district", "division", "fullName", "gender", "hobby", "", "imageName", "imagePath", "leadCustomNetiID", "leadName", "netiID", "", "Ljava/lang/Integer;", "numOfPoint", "password", "registerFrom", "registrationDate", "religion", "smsBalance", "", "Ljava/lang/Double;", "upazilla", "userDetailsInfoResponseDTO", "Lcom/netizen/netiworld/model/ProfileInformation$UserDetailsInfoResponseDTO;", "userStatus", "userWalletBalance", "username", "validationStatus", "getAge", "getArea", "getBasicEmail", "getBasicMobile", "getBloodGroup", "getCustomNetiID", "getDateOfBirth", "getDistrict", "getDivision", "getFullName", "getGender", "getHobby", "getImageName", "getImagePath", "getLeadCustomNetiID", "getLeadName", "getNetiID", "()Ljava/lang/Integer;", "getNumOfPoint", "getPassword", "getRegisterFrom", "getRegistrationDate", "getReligion", "getSmsBalance", "()Ljava/lang/Double;", "getUpazilla", "getUserDetailsInfoResponseDTO", "getUserStatus", "getUserWalletBalance", "getUsername", "getValidationStatus", "setAge", "", "setArea", "setBasicEmail", "setBasicMobile", "setBloodGroup", "setCustomNetiID", "setDateOfBirth", "setDistrict", "setDivision", "setFullName", "setGender", "setHobby", "setImageName", "setImagePath", "setLeadCustomNetiID", "setLeadName", "setNetiID", "(Ljava/lang/Integer;)V", "setNumOfPoint", "setPassword", "setRegisterFrom", "setRegistrationDate", "setReligion", "setSmsBalance", "(Ljava/lang/Double;)V", "setUpazilla", "setUserDetailsInfoResponseDTO", "setUserStatus", "setUserWalletBalance", "setUsername", "setValidationStatus", "UserDetailsInfoResponseDTO", "app_debug"})
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
public final class ProfileInformation implements java.io.Serializable {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "netiID")
    private java.lang.Integer netiID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "customNetiID")
    private java.lang.String customNetiID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "fullName")
    private java.lang.String fullName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "hobby")
    private java.lang.Object hobby;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "gender")
    private java.lang.String gender;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "religion")
    private java.lang.String religion;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "dateOfBirth")
    private java.lang.String dateOfBirth;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "bloodGroup")
    private java.lang.String bloodGroup;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "age")
    private java.lang.String age;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "basicMobile")
    private java.lang.String basicMobile;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "basicEmail")
    private java.lang.String basicEmail;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "validationStatus")
    private java.lang.Object validationStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "registrationDate")
    private java.lang.String registrationDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userWalletBalance")
    private java.lang.Double userWalletBalance;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "smsBalance")
    private java.lang.Double smsBalance;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "imageName")
    private java.lang.String imageName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "imagePath")
    private java.lang.String imagePath;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "leadName")
    private java.lang.Object leadName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "leadCustomNetiID")
    private java.lang.Object leadCustomNetiID;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "area")
    private java.lang.String area;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "upazilla")
    private java.lang.String upazilla;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "district")
    private java.lang.String district;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "division")
    private java.lang.String division;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userDetailsInfoResponseDTO")
    private com.netizen.netiworld.model.ProfileInformation.UserDetailsInfoResponseDTO userDetailsInfoResponseDTO;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "username")
    private java.lang.String username;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "userStatus")
    private java.lang.String userStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "password")
    private java.lang.Object password;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "numOfPoint")
    private java.lang.Integer numOfPoint;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "registerFrom")
    private java.lang.String registerFrom;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getNetiID() {
        return null;
    }
    
    public final void setNetiID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer netiID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCustomNetiID() {
        return null;
    }
    
    public final void setCustomNetiID(@org.jetbrains.annotations.Nullable()
    java.lang.String customNetiID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFullName() {
        return null;
    }
    
    public final void setFullName(@org.jetbrains.annotations.Nullable()
    java.lang.String fullName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getHobby() {
        return null;
    }
    
    public final void setHobby(@org.jetbrains.annotations.Nullable()
    java.lang.Object hobby) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getGender() {
        return null;
    }
    
    public final void setGender(@org.jetbrains.annotations.Nullable()
    java.lang.String gender) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReligion() {
        return null;
    }
    
    public final void setReligion(@org.jetbrains.annotations.Nullable()
    java.lang.String religion) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDateOfBirth() {
        return null;
    }
    
    public final void setDateOfBirth(@org.jetbrains.annotations.Nullable()
    java.lang.String dateOfBirth) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBloodGroup() {
        return null;
    }
    
    public final void setBloodGroup(@org.jetbrains.annotations.Nullable()
    java.lang.String bloodGroup) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAge() {
        return null;
    }
    
    public final void setAge(@org.jetbrains.annotations.Nullable()
    java.lang.String age) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBasicMobile() {
        return null;
    }
    
    public final void setBasicMobile(@org.jetbrains.annotations.Nullable()
    java.lang.String basicMobile) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBasicEmail() {
        return null;
    }
    
    public final void setBasicEmail(@org.jetbrains.annotations.Nullable()
    java.lang.String basicEmail) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getValidationStatus() {
        return null;
    }
    
    public final void setValidationStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Object validationStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRegistrationDate() {
        return null;
    }
    
    public final void setRegistrationDate(@org.jetbrains.annotations.Nullable()
    java.lang.String registrationDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getUserWalletBalance() {
        return null;
    }
    
    public final void setUserWalletBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Double userWalletBalance) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getSmsBalance() {
        return null;
    }
    
    public final void setSmsBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Double smsBalance) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getImageName() {
        return null;
    }
    
    public final void setImageName(@org.jetbrains.annotations.Nullable()
    java.lang.String imageName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getImagePath() {
        return null;
    }
    
    public final void setImagePath(@org.jetbrains.annotations.Nullable()
    java.lang.String imagePath) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getLeadName() {
        return null;
    }
    
    public final void setLeadName(@org.jetbrains.annotations.Nullable()
    java.lang.Object leadName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getLeadCustomNetiID() {
        return null;
    }
    
    public final void setLeadCustomNetiID(@org.jetbrains.annotations.Nullable()
    java.lang.Object leadCustomNetiID) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getArea() {
        return null;
    }
    
    public final void setArea(@org.jetbrains.annotations.Nullable()
    java.lang.String area) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUpazilla() {
        return null;
    }
    
    public final void setUpazilla(@org.jetbrains.annotations.Nullable()
    java.lang.String upazilla) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDistrict() {
        return null;
    }
    
    public final void setDistrict(@org.jetbrains.annotations.Nullable()
    java.lang.String district) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDivision() {
        return null;
    }
    
    public final void setDivision(@org.jetbrains.annotations.Nullable()
    java.lang.String division) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.ProfileInformation.UserDetailsInfoResponseDTO getUserDetailsInfoResponseDTO() {
        return null;
    }
    
    public final void setUserDetailsInfoResponseDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.ProfileInformation.UserDetailsInfoResponseDTO userDetailsInfoResponseDTO) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUsername() {
        return null;
    }
    
    public final void setUsername(@org.jetbrains.annotations.Nullable()
    java.lang.String username) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUserStatus() {
        return null;
    }
    
    public final void setUserStatus(@org.jetbrains.annotations.Nullable()
    java.lang.String userStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getPassword() {
        return null;
    }
    
    public final void setPassword(@org.jetbrains.annotations.Nullable()
    java.lang.Object password) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getNumOfPoint() {
        return null;
    }
    
    public final void setNumOfPoint(@org.jetbrains.annotations.Nullable()
    java.lang.Integer numOfPoint) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRegisterFrom() {
        return null;
    }
    
    public final void setRegisterFrom(@org.jetbrains.annotations.Nullable()
    java.lang.String registerFrom) {
    }
    
    public ProfileInformation() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0013\n\u0002\u0010\u0002\n\u0002\b\u0011\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0005J\b\u0010\u0018\u001a\u0004\u0018\u00010\u0005J\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001J\b\u0010\u001a\u001a\u0004\u0018\u00010\u0005J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0005J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0005J\b\u0010\u001d\u001a\u0004\u0018\u00010\u0005J\b\u0010\u001e\u001a\u0004\u0018\u00010\u0005J\b\u0010\u001f\u001a\u0004\u0018\u00010\u0005J\b\u0010 \u001a\u0004\u0018\u00010\u0005J\b\u0010!\u001a\u0004\u0018\u00010\u0005J\b\u0010\"\u001a\u0004\u0018\u00010\u0001J\b\u0010#\u001a\u0004\u0018\u00010\u0005J\b\u0010$\u001a\u0004\u0018\u00010\u0001J\r\u0010%\u001a\u0004\u0018\u00010\u0014\u00a2\u0006\u0002\u0010&J\u0010\u0010\'\u001a\u00020(2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0001J\u0010\u0010)\u001a\u00020(2\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0010\u0010*\u001a\u00020(2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005J\u0010\u0010+\u001a\u00020(2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0001J\u0010\u0010,\u001a\u00020(2\b\u0010\b\u001a\u0004\u0018\u00010\u0005J\u0010\u0010-\u001a\u00020(2\b\u0010\t\u001a\u0004\u0018\u00010\u0005J\u0010\u0010.\u001a\u00020(2\b\u0010\n\u001a\u0004\u0018\u00010\u0005J\u0010\u0010/\u001a\u00020(2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0005J\u0010\u00100\u001a\u00020(2\b\u0010\f\u001a\u0004\u0018\u00010\u0005J\u0010\u00101\u001a\u00020(2\b\u0010\r\u001a\u0004\u0018\u00010\u0005J\u0010\u00102\u001a\u00020(2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0005J\u0010\u00103\u001a\u00020(2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0005J\u0010\u00104\u001a\u00020(2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001J\u0010\u00105\u001a\u00020(2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0005J\u0010\u00106\u001a\u00020(2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001J\u0015\u00107\u001a\u00020(2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014\u00a2\u0006\u0002\u00108R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0013\u001a\u0004\u0018\u00010\u00148\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0015\u00a8\u00069"}, d2 = {"Lcom/netizen/netiworld/model/ProfileInformation$UserDetailsInfoResponseDTO;", "", "()V", "address", "addressDetails", "", "birthCertificateNo", "columnValue", "fatherName", "latitude", "longitude", "maritalStatus", "motherName", "nationalID", "nationality", "numberOfChild", "occupation", "passportNo", "typeName", "userDetailsID", "", "Ljava/lang/Integer;", "getAddress", "getAddressDetails", "getBirthCertificateNo", "getColumnValue", "getFatherName", "getLatitude", "getLongitude", "getMaritalStatus", "getMotherName", "getNationalID", "getNationality", "getNumberOfChild", "getOccupation", "getPassportNo", "getTypeName", "getUserDetailsID", "()Ljava/lang/Integer;", "setAddress", "", "setAddressDetails", "setBirthCertificateNo", "setColumnValue", "setFatherName", "setLatitude", "setLongitude", "setMaritalStatus", "setMotherName", "setNationalID", "setNationality", "setNumberOfChild", "setOccupation", "setPassportNo", "setTypeName", "setUserDetailsID", "(Ljava/lang/Integer;)V", "app_debug"})
    public static final class UserDetailsInfoResponseDTO {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "userDetailsID")
        private java.lang.Integer userDetailsID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "nationalID")
        private java.lang.String nationalID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "fatherName")
        private java.lang.String fatherName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "motherName")
        private java.lang.String motherName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "maritalStatus")
        private java.lang.String maritalStatus;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "numberOfChild")
        private java.lang.String numberOfChild;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "nationality")
        private java.lang.String nationality;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "address")
        private java.lang.Object address;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "latitude")
        private java.lang.String latitude;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "longitude")
        private java.lang.String longitude;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "occupation")
        private java.lang.Object occupation;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "passportNo")
        private java.lang.String passportNo;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "birthCertificateNo")
        private java.lang.String birthCertificateNo;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "addressDetails")
        private java.lang.String addressDetails;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "typeName")
        private java.lang.Object typeName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "columnValue")
        private java.lang.Object columnValue;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getUserDetailsID() {
            return null;
        }
        
        public final void setUserDetailsID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer userDetailsID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getNationalID() {
            return null;
        }
        
        public final void setNationalID(@org.jetbrains.annotations.Nullable()
        java.lang.String nationalID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getFatherName() {
            return null;
        }
        
        public final void setFatherName(@org.jetbrains.annotations.Nullable()
        java.lang.String fatherName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getMotherName() {
            return null;
        }
        
        public final void setMotherName(@org.jetbrains.annotations.Nullable()
        java.lang.String motherName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getMaritalStatus() {
            return null;
        }
        
        public final void setMaritalStatus(@org.jetbrains.annotations.Nullable()
        java.lang.String maritalStatus) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getNumberOfChild() {
            return null;
        }
        
        public final void setNumberOfChild(@org.jetbrains.annotations.Nullable()
        java.lang.String numberOfChild) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getNationality() {
            return null;
        }
        
        public final void setNationality(@org.jetbrains.annotations.Nullable()
        java.lang.String nationality) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getAddress() {
            return null;
        }
        
        public final void setAddress(@org.jetbrains.annotations.Nullable()
        java.lang.Object address) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLatitude() {
            return null;
        }
        
        public final void setLatitude(@org.jetbrains.annotations.Nullable()
        java.lang.String latitude) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getLongitude() {
            return null;
        }
        
        public final void setLongitude(@org.jetbrains.annotations.Nullable()
        java.lang.String longitude) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getOccupation() {
            return null;
        }
        
        public final void setOccupation(@org.jetbrains.annotations.Nullable()
        java.lang.Object occupation) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getPassportNo() {
            return null;
        }
        
        public final void setPassportNo(@org.jetbrains.annotations.Nullable()
        java.lang.String passportNo) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getBirthCertificateNo() {
            return null;
        }
        
        public final void setBirthCertificateNo(@org.jetbrains.annotations.Nullable()
        java.lang.String birthCertificateNo) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getAddressDetails() {
            return null;
        }
        
        public final void setAddressDetails(@org.jetbrains.annotations.Nullable()
        java.lang.String addressDetails) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getTypeName() {
            return null;
        }
        
        public final void setTypeName(@org.jetbrains.annotations.Nullable()
        java.lang.Object typeName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getColumnValue() {
            return null;
        }
        
        public final void setColumnValue(@org.jetbrains.annotations.Nullable()
        java.lang.Object columnValue) {
        }
        
        public UserDetailsInfoResponseDTO() {
            super();
        }
    }
}