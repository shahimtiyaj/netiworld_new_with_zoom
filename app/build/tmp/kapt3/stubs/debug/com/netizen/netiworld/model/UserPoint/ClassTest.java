package com.netizen.netiworld.model.UserPoint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u0013B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\n\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u0004J\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001J\r\u0010\f\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\rJ\u0018\u0010\u000e\u001a\u00020\u000f2\u0010\u0010\u0003\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u0004J\u0010\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001J\u0015\u0010\u0011\u001a\u00020\u000f2\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u0012R\u001c\u0010\u0003\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\t\u00a8\u0006\u0014"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/ClassTest;", "", "()V", "item", "", "Lcom/netizen/netiworld/model/UserPoint/ClassTest$Item;", "message", "msgType", "", "Ljava/lang/Integer;", "getItem", "getMessage", "getMsgType", "()Ljava/lang/Integer;", "setItem", "", "setMessage", "setMsgType", "(Ljava/lang/Integer;)V", "Item", "app_debug"})
public final class ClassTest {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "message")
    private java.lang.Object message;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "msgType")
    private java.lang.Integer msgType;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "item")
    private java.util.List<com.netizen.netiworld.model.UserPoint.ClassTest.Item> item;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.Nullable()
    java.lang.Object message) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getMsgType() {
        return null;
    }
    
    public final void setMsgType(@org.jetbrains.annotations.Nullable()
    java.lang.Integer msgType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.netizen.netiworld.model.UserPoint.ClassTest.Item> getItem() {
        return null;
    }
    
    public final void setItem(@org.jetbrains.annotations.Nullable()
    java.util.List<com.netizen.netiworld.model.UserPoint.ClassTest.Item> item) {
    }
    
    public ClassTest() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\fJ\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000f\u001a\u00020\u00102\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0015\u0010\u0011\u001a\u00020\u00102\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0012J\u0010\u0010\u0013\u001a\u00020\u00102\b\u0010\b\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u0014\u001a\u00020\u00102\b\u0010\t\u001a\u0004\u0018\u00010\u0004R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/ClassTest$Item;", "", "()V", "className", "", "classTestConfigID", "", "Ljava/lang/Integer;", "instituteID", "testName", "getClassName", "getClassTestConfigID", "()Ljava/lang/Integer;", "getInstituteID", "getTestName", "setClassName", "", "setClassTestConfigID", "(Ljava/lang/Integer;)V", "setInstituteID", "setTestName", "app_debug"})
    public static final class Item {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "classTestConfigID")
        private java.lang.Integer classTestConfigID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "instituteID")
        private java.lang.String instituteID;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "className")
        private java.lang.String className;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "testName")
        private java.lang.String testName;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getClassTestConfigID() {
            return null;
        }
        
        public final void setClassTestConfigID(@org.jetbrains.annotations.Nullable()
        java.lang.Integer classTestConfigID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getInstituteID() {
            return null;
        }
        
        public final void setInstituteID(@org.jetbrains.annotations.Nullable()
        java.lang.String instituteID) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getClassName() {
            return null;
        }
        
        public final void setClassName(@org.jetbrains.annotations.Nullable()
        java.lang.String className) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getTestName() {
            return null;
        }
        
        public final void setTestName(@org.jetbrains.annotations.Nullable()
        java.lang.String testName) {
        }
        
        public Item() {
            super();
        }
    }
}