package com.netizen.netiworld.apiService;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00b8\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001JF\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\b\u001a\u00020\u0004H\'JF\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u000b\u001a\u00020\f2,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'JF\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000b\u001a\u00020\f2,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'J\u0018\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u0004H\'JL\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\u000b\u001a\u00020\u0013H\'JL\u0010\u0014\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00150\u00110\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\u000b\u001a\u00020\u0016H\'JL\u0010\u0017\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u00110\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\u000b\u001a\u00020\u0013H\'JH\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\n0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010\u001aH\'J$\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u001d\u001a\u0004\u0018\u00010\u0004H\'J$\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u001f\u001a\u00020\u00042\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010 H\'J\u0018\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00010\u00032\b\b\u0001\u0010\b\u001a\u00020\u0004H\'JF\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000b\u001a\u00020#2,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'JF\u0010$\u001a\b\u0012\u0004\u0012\u00020%0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010&\u001a\u00020\u0004H\'J\u001e\u0010\'\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020(0\u00110\u00032\b\b\u0001\u0010)\u001a\u00020\u0004H\'J*\u0010*\u001a\u0010\u0012\f\u0012\n\u0012\u0004\u0012\u00020+\u0018\u00010\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010,\u001a\u00020\u0004H\'J$\u0010-\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010.0\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010/\u001a\u00020\u0004H\'J(\u00100\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002010\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u00102\u001a\u00020\u0004H\'J6\u00103\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002040\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u00105\u001a\u0004\u0018\u00010\u00042\n\b\u0001\u00106\u001a\u0004\u0018\u00010\u0004H\'J*\u00107\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002010\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u00102\u001a\u0004\u0018\u00010\u0004H\'J\u001e\u00108\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002090\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u0004H\'J\u001e\u0010:\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020;0\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u0004H\'J(\u0010<\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020=0\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u00102\u001a\u00020\u0004H\'JF\u0010>\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\b\u001a\u00020\u0004H\'J\u0014\u0010?\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020@0\u00110\u0003H\'J(\u0010A\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020=0\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u00102\u001a\u00020\u0004H\'J\"\u0010B\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010C\u001a\u00020DH\'J\u001e\u0010E\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020F0\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u0004H\'J\u001e\u0010G\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020H0\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u0004H\'JV\u0010I\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020J0\u00110\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010K\u001a\u00020\u00042\b\b\u0001\u0010L\u001a\u00020\u0004H\'J\u0018\u0010M\u001a\b\u0012\u0004\u0012\u00020N0\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u0004H\'JF\u0010O\u001a\b\u0012\u0004\u0012\u00020P0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\b\u001a\u00020\u0004H\'JF\u0010Q\u001a\b\u0012\u0004\u0012\u00020R0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\b\u001a\u00020\u0004H\'JF\u0010S\u001a\b\u0012\u0004\u0012\u00020T0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\b\u001a\u00020\u0004H\'J(\u0010U\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020V0\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010W\u001a\u00020\u0004H\'J(\u0010X\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020V0\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010)\u001a\u00020\u0004H\'JS\u0010Y\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020Z0\u00110\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\n\b\u0001\u0010[\u001a\u0004\u0018\u00010\\H\'\u00a2\u0006\u0002\u0010]JN\u0010^\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020_0\u00110\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\u0004H\'JS\u0010`\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020Z0\u00110\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\n\b\u0001\u0010[\u001a\u0004\u0018\u00010\\H\'\u00a2\u0006\u0002\u0010]JL\u0010a\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020b0\u00110\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010\u000b\u001a\u00020cH\'JF\u0010d\u001a\b\u0012\u0004\u0012\u00020e0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\b\b\u0001\u0010f\u001a\u00020\u0004H\'JS\u0010g\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020h0\u00110\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\n\b\u0001\u0010i\u001a\u0004\u0018\u00010\\H\'\u00a2\u0006\u0002\u0010]J<\u0010j\u001a\b\u0012\u0004\u0012\u00020k0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'J\u001e\u0010l\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020m0\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u0004H\'J$\u0010n\u001a\b\u0012\u0004\u0012\u00020o0\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010p\u001a\u0004\u0018\u00010qH\'J\u001e\u0010r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020s0\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u0004H\'J6\u0010t\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010u\u001a\u00020\u00042\b\b\u0001\u0010v\u001a\u00020\u00042\b\b\u0001\u0010w\u001a\u00020\u0004H\'J6\u0010x\u001a\b\u0012\u0004\u0012\u00020y0\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010u\u001a\u00020\u00042\b\b\u0001\u0010v\u001a\u00020\u00042\b\b\u0001\u0010z\u001a\u00020\u0004H\'J@\u0010{\u001a\b\u0012\u0004\u0012\u00020|0\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010u\u001a\u00020\u00042\b\b\u0001\u0010v\u001a\u00020\u00042\b\b\u0001\u0010z\u001a\u00020\u00042\b\b\u0001\u0010}\u001a\u00020\u0004H\'J6\u0010~\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010u\u001a\u00020\u00042\b\b\u0001\u0010v\u001a\u00020\u00042\b\b\u0001\u0010w\u001a\u00020\u0004H\'J6\u0010\u007f\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010u\u001a\u00020\u00042\b\b\u0001\u0010v\u001a\u00020\u00042\b\b\u0001\u0010w\u001a\u00020\u0004H\'J#\u0010\u0080\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010u\u001a\u00020\u0004H\'J%\u0010\u0081\u0001\u001a\t\u0012\u0005\u0012\u00030\u0082\u00010\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\t\b\u0001\u0010\u0083\u0001\u001a\u00020\u0004H\'JC\u0010\u0084\u0001\u001a\t\u0012\u0005\u0012\u00030\u0085\u00010\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010u\u001a\u00020\u00042\b\b\u0001\u0010v\u001a\u00020\u00042\b\b\u0001\u0010z\u001a\u00020\u00042\t\b\u0001\u0010\u0086\u0001\u001a\u00020\u0004H\'J/\u0010\u0087\u0001\u001a\t\u0012\u0005\u0012\u00030\u0088\u00010\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\t\b\u0001\u0010\u0083\u0001\u001a\u00020\u00042\b\b\u0001\u0010u\u001a\u00020\u0004H\'J.\u0010\u0089\u0001\u001a\t\u0012\u0005\u0012\u00030\u008a\u00010\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010u\u001a\u00020\u00042\b\b\u0001\u0010v\u001a\u00020\u0004H\'J8\u0010\u008b\u0001\u001a\t\u0012\u0005\u0012\u00030\u008c\u00010\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010u\u001a\u00020\u00042\b\b\u0001\u0010v\u001a\u00020\u00042\b\b\u0001\u0010w\u001a\u00020\u0004H\'J8\u0010\u008d\u0001\u001a\t\u0012\u0005\u0012\u00030\u008e\u00010\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010u\u001a\u00020\u00042\b\b\u0001\u0010v\u001a\u00020\u00042\b\b\u0001\u0010w\u001a\u00020\u0004H\'J \u0010\u008f\u0001\u001a\u000f\u0012\u000b\u0012\t\u0012\u0005\u0012\u00030\u0090\u00010\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u0004H\'J*\u0010\u0091\u0001\u001a\u000f\u0012\u000b\u0012\t\u0012\u0005\u0012\u00030\u0092\u00010\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u00102\u001a\u00020\u0004H\'J\u001f\u0010\u0093\u0001\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020s0\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u0004H\'J \u0010\u0094\u0001\u001a\u000f\u0012\u000b\u0012\t\u0012\u0005\u0012\u00030\u0095\u00010\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u0004H\'J=\u0010\u0096\u0001\u001a\b\u0012\u0004\u0012\u00020D0\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'J*\u0010\u0097\u0001\u001a\u000f\u0012\u000b\u0012\t\u0012\u0005\u0012\u00030\u0098\u00010\u00110\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u00102\u001a\u00020\u0004H\'JN\u0010\u0099\u0001\u001a\u000f\u0012\u000b\u0012\t\u0012\u0005\u0012\u00030\u009a\u00010\u00110\u00032\b\b\u0001\u0010\u000b\u001a\u00020\u00162,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'JH\u0010\u009b\u0001\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\t\b\u0001\u0010\u000b\u001a\u00030\u009c\u00012,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'JH\u0010\u009d\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\t\b\u0001\u0010\u000b\u001a\u00030\u009e\u00012,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'Jm\u0010\u009f\u0001\u001a\t\u0012\u0005\u0012\u00030\u00a0\u00010\u00032-\b\u0001\u0010\u00a1\u0001\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u0007H\'J\u001a\u0010\u00a2\u0001\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\t\b\u0001\u0010\u000b\u001a\u00030\u00a3\u0001H\'J\u001b\u0010\u00a4\u0001\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\n\b\u0001\u0010\u000b\u001a\u0004\u0018\u00010\u001aH\'J%\u0010\u00a5\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00a6\u0001\u001a\u00030\u00a7\u0001H\'J%\u0010\u00a8\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00a9\u0001\u001a\u00030\u00aa\u0001H\'J%\u0010\u00ab\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00ac\u0001\u001a\u00030\u00ad\u0001H\'J%\u0010\u00ae\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00af\u0001\u001a\u00030\u00b0\u0001H\'J%\u0010\u00b1\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00b2\u0001\u001a\u00030\u00b3\u0001H\'J&\u0010\u00b4\u0001\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\t\b\u0001\u0010\u000b\u001a\u00030\u00b5\u0001H\'J%\u0010\u00b6\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00b7\u0001\u001a\u00030\u00b8\u0001H\'JH\u0010\u00b9\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032,\b\u0001\u0010\u0005\u001a&\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u00040\u0006j\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0004\u0012\u0006\u0012\u0004\u0018\u00010\u0004`\u00072\t\b\u0001\u0010\u000b\u001a\u00030\u00ba\u0001H\'J%\u0010\u00bb\u0001\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00bc\u0001\u001a\u00030\u00bd\u0001H\'J%\u0010\u00be\u0001\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00bf\u0001\u001a\u00030\u00c0\u0001H\'J%\u0010\u00c1\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00c1\u0001\u001a\u00030\u00c2\u0001H\'J%\u0010\u00c3\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00c4\u0001\u001a\u00030\u00c5\u0001H\'J#\u0010\u00c6\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\b\b\u0001\u0010C\u001a\u00020DH\'J%\u0010\u00c7\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00c7\u0001\u001a\u00030\u00c8\u0001H\'J%\u0010\u00c9\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00c9\u0001\u001a\u00030\u00ca\u0001H\'J%\u0010\u00cb\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00c9\u0001\u001a\u00030\u00a7\u0001H\'J%\u0010\u00cc\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00cc\u0001\u001a\u00030\u00cd\u0001H\'J$\u0010\u00ce\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\t\b\u0001\u0010C\u001a\u00030\u00cf\u0001H\'J%\u0010\u00d0\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00d0\u0001\u001a\u00030\u00d1\u0001H\'J%\u0010\u00d2\u0001\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u000f\u001a\u00020\u00042\n\b\u0001\u0010\u00b2\u0001\u001a\u00030\u00b3\u0001H\'J\u001a\u0010\u00d3\u0001\u001a\b\u0012\u0004\u0012\u00020 0\u00032\t\b\u0001\u0010\u00d4\u0001\u001a\u00020\u0004H\'J1\u0010\u00d5\u0001\u001a\t\u0012\u0005\u0012\u00030\u00d6\u00010\u00032\t\b\u0001\u0010\u00d7\u0001\u001a\u00020\u00042\t\b\u0001\u0010\u00d8\u0001\u001a\u00020\u00042\t\b\u0001\u0010\u00d9\u0001\u001a\u00020\u0004H\'\u00a8\u0006\u00da\u0001"}, d2 = {"Lcom/netizen/netiworld/apiService/ApiInterface;", "", "OTPVarify", "Lretrofit2/Call;", "", "headers", "Ljava/util/HashMap;", "Lkotlin/collections/HashMap;", "code", "WalletBalanceTransfer", "Ljava/lang/Void;", "obj", "Lcom/netizen/netiworld/model/WalletTransfer;", "WalletBalanceTransferFinal", "addPoint", "token", "balanceDepositReportGetData", "", "Lcom/netizen/netiworld/model/BalanceDepositReportGetData;", "Lcom/netizen/netiworld/model/BalanceReportPostData;", "balanceStatementGetData", "Lcom/netizen/netiworld/model/BalanceStatementGetData;", "Lcom/netizen/netiworld/model/BalanceDepositPostForList;", "balanceTransferGetData", "Lcom/netizen/netiworld/model/BalanceTransferGetData;", "changePassword", "Lcom/netizen/netiworld/model/ResetPassword;", "downloadTokenAttachment", "Lcom/google/gson/JsonObject;", "imageUrl", "forgotPasswordOTP", "userContactNo", "Lcom/netizen/netiworld/model/UserCheckForgotPass;", "forgotPasswordOTPVarify", "generalProductPostData", "Lcom/netizen/netiworld/model/GeneralProductPostData;", "getAccountNoInfo", "Lcom/netizen/netiworld/model/MobileBankAcNo;", "localBankCategoryID", "getArea", "Lcom/netizen/netiworld/model/AreaModel;", "parentCategoryID", "getBalanceDepositAccountInfo", "Lcom/netizen/netiworld/model/BalanceDepositAccountInfo;", "defaultCode", "getBalanceWithdrawInfo", "Lcom/netizen/netiworld/model/WithDrawInfo;", "codeCategoryDefaultCode", "getBankAccountList", "Lcom/netizen/netiworld/model/BankAccountInfo;", "typeDefaultCode", "getBankBranchList", "Lcom/netizen/netiworld/model/BankBranchInfo;", "bankId", "districtId", "getBankDistrictList", "getBankList", "Lcom/netizen/netiworld/model/BankList;", "getCertificationInfo", "Lcom/netizen/netiworld/model/CertificationInfo;", "getDataByTypeDefaultCode", "Lcom/netizen/netiworld/model/DataByTypeDefaultCode;", "getDataSearchByNetiID", "getDistrict", "Lcom/netizen/netiworld/model/DistrictModel;", "getDistrictList", "getDivisionList", "profileInformation", "Lcom/netizen/netiworld/model/ProfileInformation;", "getEducationInfo", "Lcom/netizen/netiworld/model/EducationInfo;", "getExperienceInfo", "Lcom/netizen/netiworld/model/ExperienceInfo;", "getGeneralProductReportData", "Lcom/netizen/netiworld/model/GeneralProductGetData;", "startDate", "endDate", "getHomePageInfo", "Lcom/netizen/netiworld/model/HomePageInfo;", "getMessageType", "Lcom/netizen/netiworld/model/MessageType;", "getMessageType1", "Lcom/netizen/netiworld/model/MessageType1;", "getOfferProduct", "Lcom/netizen/netiworld/model/OfferProduct;", "getProblemModuleList", "Lcom/netizen/netiworld/model/Problem;", "catDefaultCode", "getProblemTypeList", "getProductGDetailsData", "Lcom/netizen/netiworld/model/ProductGDetailsGetData;", "purchaseID", "", "(Ljava/util/HashMap;Ljava/lang/Integer;)Lretrofit2/Call;", "getProductName", "Lcom/netizen/netiworld/model/Products;", "getProductODetailsData", "getProductOfferReportData", "Lcom/netizen/netiworld/model/ProductsOfferGetData;", "Lcom/netizen/netiworld/model/ProductsOfferReportPostData;", "getProfile", "Lcom/netizen/netiworld/model/ProfileImage;", "filePath", "getPurchaseCodeData", "Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata;", "usedStatus", "getPurchasePoint", "Lcom/netizen/netiworld/model/PurchasePoint;", "getReferenceInfo", "Lcom/netizen/netiworld/model/ReferenceInfo;", "getRevenueLogData", "Lcom/netizen/netiworld/model/RevenueLogGetData;", "revenueLogPostData", "Lcom/netizen/netiworld/model/RevenueLogPostData;", "getSolvedAndPendingToken", "Lcom/netizen/netiworld/model/TokenList;", "getStudentPortalClassRoutineInfo", "instituteId", "studentId", "academicYear", "getStudentPortalClassTest", "Lcom/netizen/netiworld/model/UserPoint/ClassTest;", "year", "getStudentPortalClassTestDetails", "Lcom/netizen/netiworld/model/UserPoint/ClassTestDetails;", "classTestConfigId", "getStudentPortalClassTestInfo", "getStudentPortalExamInfo", "getStudentPortalExamRoutineSession", "getStudentPortalGlobalInfo", "Lcom/netizen/netiworld/model/UserPoint/YearByTypeId;", "typeId", "getStudentPortalInventoryDetails", "Lcom/netizen/netiworld/model/UserPoint/InventoryDetails;", "monthName", "getStudentPortalPeriod", "Lcom/netizen/netiworld/model/UserPoint/StudentPortalPeriod;", "getStudentPortalProfileInfo", "Lcom/netizen/netiworld/model/UserPoint/StudentPortalProfile;", "getStudentPortalSubjectInfo", "Lcom/netizen/netiworld/model/UserPoint/StudentPortalSubject;", "getStudentPortalUnpaidFees", "Lcom/netizen/netiworld/model/UserPoint/UnpaidFees;", "getTagList", "Lcom/netizen/netiworld/model/TagGetData;", "getTagTypeList", "Lcom/netizen/netiworld/model/TagType;", "getTokenList", "getTrainingInfo", "Lcom/netizen/netiworld/model/TrainingInfo;", "getUserProfileInfo", "getUsingPurposeList", "Lcom/netizen/netiworld/model/UsingPurpose;", "messageRechargeGetData", "Lcom/netizen/netiworld/model/BalanceMessageGetData;", "messageRechargepostData", "Lcom/netizen/netiworld/model/MessageRecharge;", "offerProductPostData", "Lcom/netizen/netiworld/model/OfferProductPost;", "postData", "Lcom/netizen/netiworld/model/PostResponse;", "params", "registrationSubmit", "Lcom/netizen/netiworld/model/RegistrationPost;", "resetPasswordAfterForgot", "saveAcademicEducationInfo", "postAcademicEducation", "Lcom/netizen/netiworld/model/PostAcademicEducation;", "saveCertificationInfo", "postCertificationInfo", "Lcom/netizen/netiworld/model/PostCertificationInfo;", "saveExperienceInfo", "postExperienceInfo", "Lcom/netizen/netiworld/model/PostExperienceInfo;", "saveReferenceInfo", "postReferenceInfo", "Lcom/netizen/netiworld/model/PostReferenceInfo;", "saveTrainingInfo", "postTrainingInfo", "Lcom/netizen/netiworld/model/PostTrainingInfo;", "submitBalanceWithdrawData", "Lcom/netizen/netiworld/model/WithdrawPostData;", "submitBankAccount", "bankAccountPostData", "Lcom/netizen/netiworld/model/BankAccountPostData;", "submitDepositPostData", "Lcom/netizen/netiworld/model/DepositPostData;", "submitNetiMail", "netiMailSubmit", "Lcom/netizen/netiworld/model/NetiMailSubmit;", "submitNewToken", "tokenSubmitPostData", "Lcom/netizen/netiworld/model/TokenSubmitPostData;", "tagAndAddBankAccount", "Lcom/netizen/netiworld/model/TagAndAddBankAccount;", "tagBankAccount", "tagPostData", "Lcom/netizen/netiworld/model/TagPostData;", "updateAddressInfo", "updateBankAccount", "Lcom/netizen/netiworld/model/UpdateBankAccount;", "updateCertificationInfo", "Lcom/netizen/netiworld/model/UpdateCertificationInfo;", "updateEducationInfo", "updateExperienceInfo", "Lcom/netizen/netiworld/model/UpdateExperienceInfo;", "updatePersonalInfo", "Lcom/netizen/netiworld/model/ProfileInformation_;", "updateReferenceInfo", "Lcom/netizen/netiworld/model/UpdateReferenceInfo;", "updateTrainingInfo", "userCheckForgotPaasword", "username", "userLogIn", "Lcom/netizen/netiworld/model/MSG;", "name", "password", "grant_type", "app_debug"})
public abstract interface ApiInterface {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "oauth/token")
    @retrofit2.http.FormUrlEncoded()
    public abstract retrofit2.Call<com.netizen.netiworld.model.PostResponse> postData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.FieldMap()
    java.util.HashMap<java.lang.String, java.lang.String> params, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/profile/details")
    public abstract retrofit2.Call<com.netizen.netiworld.model.ProfileInformation> getUserProfileInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/account/by/corebankid")
    public abstract retrofit2.Call<com.netizen.netiworld.model.MobileBankAcNo> getAccountNoInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "coreBankID")
    java.lang.String localBankCategoryID);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/category/by/type/2nd_parent_type")
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.BankAccountInfo>> getBankAccountList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "typeDefaultCode")
    java.lang.String typeDefaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/message/types/by/point")
    public abstract retrofit2.Call<com.netizen.netiworld.model.MessageType> getMessageType(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "roleID")
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/message/types/by/point")
    public abstract retrofit2.Call<com.netizen.netiworld.model.MessageType1> getMessageType1(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "roleID")
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/roles/assigned")
    public abstract retrofit2.Call<com.netizen.netiworld.model.PurchasePoint> getPurchasePoint(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/products/by/role")
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.Products>> getProductName(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "roleID")
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/balance/deposit")
    public abstract retrofit2.Call<java.lang.String> submitDepositPostData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.DepositPostData obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/message/recharge")
    public abstract retrofit2.Call<java.lang.Void> messageRechargepostData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.MessageRecharge obj, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/product/purchase")
    public abstract retrofit2.Call<java.lang.String> generalProductPostData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.GeneralProductPostData obj, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/product/offer/by/code")
    public abstract retrofit2.Call<com.netizen.netiworld.model.OfferProduct> getOfferProduct(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "code")
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/product/purchase/offer")
    public abstract retrofit2.Call<java.lang.String> offerProductPostData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.OfferProductPost obj, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/profile/by/custom_id")
    public abstract retrofit2.Call<java.lang.String> getDataSearchByNetiID(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "custom_id")
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/transfer/check/balance")
    public abstract retrofit2.Call<java.lang.Void> WalletBalanceTransfer(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.WalletTransfer obj, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/core/check-otp")
    public abstract retrofit2.Call<java.lang.String> OTPVarify(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "code")
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/balance/transfer")
    public abstract retrofit2.Call<java.lang.String> WalletBalanceTransferFinal(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.WalletTransfer obj, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/balance/requests/by/date_range")
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.BalanceDepositReportGetData>> balanceDepositReportGetData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.BalanceReportPostData obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/balance/transfer/by/date")
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.BalanceTransferGetData>> balanceTransferGetData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.BalanceReportPostData obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/message/by/date-range")
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.BalanceMessageGetData>> messageRechargeGetData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.BalanceDepositPostForList obj, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/balance/statement")
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.BalanceStatementGetData>> balanceStatementGetData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.BalanceDepositPostForList obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/product/purchases/by/date-range")
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.GeneralProductGetData>> getGeneralProductReportData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "startDate")
    java.lang.String startDate, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "endDate")
    java.lang.String endDate);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/purchase/codes/by/purchaseid")
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.ProductGDetailsGetData>> getProductGDetailsData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "purchaseID")
    java.lang.Integer purchaseID);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/product/offer/by/date-range")
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.ProductsOfferGetData>> getProductOfferReportData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.ProductsOfferReportPostData obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/purchase/codes/by/purchaseid")
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.ProductGDetailsGetData>> getProductODetailsData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "purchaseID")
    java.lang.Integer purchaseID);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/purchase/codes/by/usedstatus")
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.PurchaseCodeLogGetdata>> getPurchaseCodeData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "usedStatus")
    java.lang.Integer usedStatus);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/file/find")
    public abstract retrofit2.Call<com.netizen.netiworld.model.ProfileImage> getProfile(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "filePath")
    java.lang.String filePath);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "guest/user/register-user")
    public abstract retrofit2.Call<java.lang.Void> registrationSubmit(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.RegistrationPost obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/addresses/district")
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.DistrictModel>> getDistrict();
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/addresses/area")
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.AreaModel>> getArea(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "parentCategoryID")
    java.lang.String parentCategoryID);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/user/profile/by")
    public abstract retrofit2.Call<com.netizen.netiworld.model.UserCheckForgotPass> userCheckForgotPaasword(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "username")
    java.lang.String username);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "guest/recover/password/check_contact")
    public abstract retrofit2.Call<java.lang.Void> forgotPasswordOTP(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "userContactNo")
    java.lang.String userContactNo, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.UserCheckForgotPass obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/core/check-otp")
    public abstract retrofit2.Call<java.lang.Object> forgotPasswordOTPVarify(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "code")
    java.lang.String code);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "guest/recover/password/reset")
    public abstract retrofit2.Call<java.lang.Void> resetPasswordAfterForgot(@org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.ResetPassword obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/change/password")
    public abstract retrofit2.Call<java.lang.Void> changePassword(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.HeaderMap()
    java.util.HashMap<java.lang.String, java.lang.String> headers, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.ResetPassword obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/chat/new")
    public abstract retrofit2.Call<java.lang.Void> submitNetiMail(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.NetiMailSubmit netiMailSubmit);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "oauth/token")
    @retrofit2.http.FormUrlEncoded()
    @retrofit2.http.Headers(value = {"Authorization: Basic ZGV2Z2xhbi1jbGllbnQ6ZGV2Z2xhbi1zZWNyZXQ=", "Content-Type:application/x-www-form-urlencoded", "NZUSER:absiddik:123:password"})
    public abstract retrofit2.Call<com.netizen.netiworld.model.MSG> userLogIn(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "name")
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "password")
    java.lang.String password, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "grant_type")
    java.lang.String grant_type);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/home/dashboard/info")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.netizen.netiworld.model.HomePageInfo> getHomePageInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/core/bank/accounts/by")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.BalanceDepositAccountInfo>> getBalanceDepositAccountInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "defaultCode")
    java.lang.String defaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/tagged/bank-account/by")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.netizen.netiworld.model.WithDrawInfo> getBalanceWithdrawInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "coreCategoryDefaultCode")
    java.lang.String codeCategoryDefaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/balance/withdraw")
    public abstract retrofit2.Call<java.lang.String> submitBalanceWithdrawData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.WithdrawPostData obj);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/report/revenue/log")
    public abstract retrofit2.Call<com.netizen.netiworld.model.RevenueLogGetData> getRevenueLogData(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.RevenueLogPostData revenueLogPostData);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/category/by/typeDefaultCode")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.BankAccountInfo>> getBankDistrictList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "typeDefaultCode")
    java.lang.String typeDefaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/bank/branches/by")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.BankBranchInfo>> getBankBranchList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "bankID")
    java.lang.String bankId, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "districtID")
    java.lang.String districtId);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/bank/add-account")
    public abstract retrofit2.Call<java.lang.String> submitBankAccount(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.BankAccountPostData bankAccountPostData);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/bank/accounts/by/user")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.BankList>> getBankList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/category/by/typeDefaultCode")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.TagType>> getTagTypeList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "typeDefaultCode")
    java.lang.String typeDefaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/bank/account/taggings")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.TagGetData>> getTagList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/bank/account/tag")
    public abstract retrofit2.Call<java.lang.String> tagBankAccount(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.TagPostData tagPostData);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/tokens/modules/by")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.Problem>> getProblemModuleList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "catDefaultCode")
    java.lang.String catDefaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/tokens/types/by")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.Problem>> getProblemTypeList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "parentCategoryID")
    java.lang.String parentCategoryID);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/tokens/by/pending")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.TokenList>> getSolvedAndPendingToken(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/tokens/new")
    public abstract retrofit2.Call<java.lang.Void> submitNewToken(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.TokenSubmitPostData tokenSubmitPostData);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/tokens/by/top10")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.TokenList>> getTokenList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "guest/file/find/")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.google.gson.JsonObject> downloadTokenAttachment(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "filePath")
    java.lang.String imageUrl);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/education-infos/find")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.EducationInfo>> getEducationInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/training-infos/find")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.TrainingInfo>> getTrainingInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/certificate-infos/find")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.CertificationInfo>> getCertificationInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/experience-infos/find")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.ExperienceInfo>> getExperienceInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/reference-infos/find")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.ReferenceInfo>> getReferenceInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/category/by/typeDefaultCode")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.UsingPurpose>> getUsingPurposeList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "typeDefaultCode")
    java.lang.String typeDefaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/bank/account/add/and/tag")
    public abstract retrofit2.Call<java.lang.String> tagAndAddBankAccount(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.TagAndAddBankAccount tagAndAddBankAccount);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/category/by/typeDefaultCode")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> getDataByTypeDefaultCode(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "typeDefaultCode")
    java.lang.String typeDefaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/education-info/save")
    public abstract retrofit2.Call<java.lang.String> saveAcademicEducationInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.PostAcademicEducation postAcademicEducation);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/training-info/save")
    public abstract retrofit2.Call<java.lang.String> saveTrainingInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.PostTrainingInfo postTrainingInfo);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/certification-info/save")
    public abstract retrofit2.Call<java.lang.String> saveCertificationInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.PostCertificationInfo postCertificationInfo);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/experience-info/save")
    public abstract retrofit2.Call<java.lang.String> saveExperienceInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.PostExperienceInfo postExperienceInfo);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "user/reference-info/save")
    public abstract retrofit2.Call<java.lang.String> saveReferenceInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.PostReferenceInfo postReferenceInfo);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.PUT(value = "user/experience-info/update")
    public abstract retrofit2.Call<java.lang.String> updateExperienceInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.UpdateExperienceInfo updateExperienceInfo);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.PUT(value = "user/bank/account/tagging/udate")
    public abstract retrofit2.Call<java.lang.String> updateBankAccount(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.UpdateBankAccount updateBankAccount);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.PUT(value = "user/training-info/update")
    public abstract retrofit2.Call<java.lang.String> updateTrainingInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.PostTrainingInfo postTrainingInfo);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.PUT(value = "user/reference-info/update")
    public abstract retrofit2.Call<java.lang.String> updateReferenceInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.UpdateReferenceInfo updateReferenceInfo);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.PUT(value = "user/certification-info/update")
    public abstract retrofit2.Call<java.lang.String> updateCertificationInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.UpdateCertificationInfo updateCertificationInfo);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.PUT(value = "user/education-info/update")
    public abstract retrofit2.Call<java.lang.String> updateEducationInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.PostAcademicEducation updateCertificationInfo);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.PUT(value = "user/profile/edit/basic")
    public abstract retrofit2.Call<java.lang.String> updatePersonalInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.ProfileInformation_ profileInformation);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.PUT(value = "user/profile/edit/basic")
    public abstract retrofit2.Call<java.lang.String> updateAddressInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.ProfileInformation profileInformation);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.PUT(value = "user/profile/edit/basic")
    public abstract retrofit2.Call<java.lang.String> getDivisionList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.netizen.netiworld.model.ProfileInformation profileInformation);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/category/by/parentCatID")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.util.List<com.netizen.netiworld.model.DataByTypeDefaultCode>> getDistrictList(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "parentCategoryID")
    java.lang.String typeDefaultCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/em/user/point/assign")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.lang.String> addPoint(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/student/profile")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.netizen.netiworld.model.UserPoint.StudentPortalProfile> getStudentPortalProfileInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "instituteId")
    java.lang.String instituteId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "studentId")
    java.lang.String studentId);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/student/subjects")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.netizen.netiworld.model.UserPoint.StudentPortalSubject> getStudentPortalSubjectInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "instituteId")
    java.lang.String instituteId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "studentId")
    java.lang.String studentId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "academicYear")
    java.lang.String academicYear);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/student/class-tests")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.lang.String> getStudentPortalClassTestInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "instituteId")
    java.lang.String instituteId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "studentId")
    java.lang.String studentId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "academicYear")
    java.lang.String academicYear);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/sa/find/global-info/list/by/type-id")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.netizen.netiworld.model.UserPoint.YearByTypeId> getStudentPortalGlobalInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "typeId")
    java.lang.String typeId);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/student/exams")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.lang.String> getStudentPortalExamInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "instituteId")
    java.lang.String instituteId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "studentId")
    java.lang.String studentId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "academicYear")
    java.lang.String academicYear);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/student/class-routine")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.lang.String> getStudentPortalClassRoutineInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "instituteId")
    java.lang.String instituteId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "studentId")
    java.lang.String studentId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "academicYear")
    java.lang.String academicYear);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/student/sessions")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<java.lang.String> getStudentPortalExamRoutineSession(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "instituteId")
    java.lang.String instituteId);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/student/core-settings/by/type-id")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.netizen.netiworld.model.UserPoint.StudentPortalPeriod> getStudentPortalPeriod(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "typeId")
    java.lang.String typeId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "instituteId")
    java.lang.String instituteId);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/student/accounts/invoice/unpaid/list")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.netizen.netiworld.model.UserPoint.UnpaidFees> getStudentPortalUnpaidFees(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "instituteId")
    java.lang.String instituteId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "studentId")
    java.lang.String studentId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "academicYear")
    java.lang.String academicYear);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/student/accounts/inventory/details")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.netizen.netiworld.model.UserPoint.InventoryDetails> getStudentPortalInventoryDetails(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "instituteId")
    java.lang.String instituteId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "studentId")
    java.lang.String studentId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "year")
    java.lang.String year, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "monthName")
    java.lang.String monthName);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/student/class-tests")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.netizen.netiworld.model.UserPoint.ClassTest> getStudentPortalClassTest(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "instituteId")
    java.lang.String instituteId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "studentId")
    java.lang.String studentId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "academicYear")
    java.lang.String year);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "user/student/class-test/marks")
    @retrofit2.http.Headers(value = {"Content-Type: application/x-www-form-urlencoded"})
    public abstract retrofit2.Call<com.netizen.netiworld.model.UserPoint.ClassTestDetails> getStudentPortalClassTestDetails(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "instituteId")
    java.lang.String instituteId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "studentId")
    java.lang.String studentId, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "academicYear")
    java.lang.String year, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "classTestConfigId")
    java.lang.String classTestConfigId);
}