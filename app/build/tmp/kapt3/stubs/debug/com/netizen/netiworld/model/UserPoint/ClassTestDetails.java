package com.netizen.netiworld.model.UserPoint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u0012B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u0004\u0018\u00010\u0004J\b\u0010\n\u001a\u0004\u0018\u00010\u0001J\r\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u000e2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0001J\u0015\u0010\u0010\u001a\u00020\u000e2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0011R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\b\u00a8\u0006\u0013"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/ClassTestDetails;", "", "()V", "item", "Lcom/netizen/netiworld/model/UserPoint/ClassTestDetails$Item;", "message", "msgType", "", "Ljava/lang/Integer;", "getItem", "getMessage", "getMsgType", "()Ljava/lang/Integer;", "setItem", "", "setMessage", "setMsgType", "(Ljava/lang/Integer;)V", "Item", "app_debug"})
public final class ClassTestDetails {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "message")
    private java.lang.Object message;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "msgType")
    private java.lang.Integer msgType;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "item")
    private com.netizen.netiworld.model.UserPoint.ClassTestDetails.Item item;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getMessage() {
        return null;
    }
    
    public final void setMessage(@org.jetbrains.annotations.Nullable()
    java.lang.Object message) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getMsgType() {
        return null;
    }
    
    public final void setMsgType(@org.jetbrains.annotations.Nullable()
    java.lang.Integer msgType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.UserPoint.ClassTestDetails.Item getItem() {
        return null;
    }
    
    public final void setItem(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.UserPoint.ClassTestDetails.Item item) {
    }
    
    public ClassTestDetails() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u000f\n\u0002\u0010\u0002\n\u0002\b\r\u0018\u00002\u00020\u0001:\u0001-B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0014\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0015\u001a\u0004\u0018\u00010\u0004J\b\u0010\u0016\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0017\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u0018J\b\u0010\u0019\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\u001a\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\r\u0018\u00010\fJ\b\u0010\u001b\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\r\u0010\u001d\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\u0002\u0010\u001eJ\r\u0010\u001f\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\u0018J\u0010\u0010 \u001a\u00020!2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0010\u0010\"\u001a\u00020!2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004J\u0010\u0010#\u001a\u00020!2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004J\u0015\u0010$\u001a\u00020!2\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010%J\u0010\u0010&\u001a\u00020!2\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\u0016\u0010\'\u001a\u00020!2\u000e\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\fJ\u0010\u0010(\u001a\u00020!2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\u0010\u0010)\u001a\u00020!2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\u0015\u0010*\u001a\u00020!2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\u0002\u0010+J\u0015\u0010,\u001a\u00020!2\b\u0010\u0013\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010%R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\tR\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\r\u0018\u00010\f8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\u0004\u0018\u00010\u00118\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0012R\u0016\u0010\u0013\u001a\u0004\u0018\u00010\b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\t\u00a8\u0006."}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/ClassTestDetails$Item;", "", "()V", "classTestName", "", "customStudentId", "mobileNo", "obtainedMarks", "", "Ljava/lang/Float;", "sectionName", "stdCtExamMarks", "", "Lcom/netizen/netiworld/model/UserPoint/ClassTestDetails$Item$StdCtExamMark;", "studentId", "studentName", "studentRoll", "", "Ljava/lang/Integer;", "totalMarks", "getClassTestName", "getCustomStudentId", "getMobileNo", "getObtainedMarks", "()Ljava/lang/Float;", "getSectionName", "getStdCtExamMarks", "getStudentId", "getStudentName", "getStudentRoll", "()Ljava/lang/Integer;", "getTotalMarks", "setClassTestName", "", "setCustomStudentId", "setMobileNo", "setObtainedMarks", "(Ljava/lang/Float;)V", "setSectionName", "setStdCtExamMarks", "setStudentId", "setStudentName", "setStudentRoll", "(Ljava/lang/Integer;)V", "setTotalMarks", "StdCtExamMark", "app_debug"})
    public static final class Item {
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "customStudentId")
        private java.lang.String customStudentId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "studentId")
        private java.lang.String studentId;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "studentRoll")
        private java.lang.Integer studentRoll;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "sectionName")
        private java.lang.String sectionName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "classTestName")
        private java.lang.String classTestName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "studentName")
        private java.lang.String studentName;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "mobileNo")
        private java.lang.String mobileNo;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "obtainedMarks")
        private java.lang.Float obtainedMarks;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "totalMarks")
        private java.lang.Float totalMarks;
        @com.google.gson.annotations.Expose()
        @com.google.gson.annotations.SerializedName(value = "stdCtExamMarks")
        private java.util.List<com.netizen.netiworld.model.UserPoint.ClassTestDetails.Item.StdCtExamMark> stdCtExamMarks;
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCustomStudentId() {
            return null;
        }
        
        public final void setCustomStudentId(@org.jetbrains.annotations.Nullable()
        java.lang.String customStudentId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getStudentId() {
            return null;
        }
        
        public final void setStudentId(@org.jetbrains.annotations.Nullable()
        java.lang.String studentId) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Integer getStudentRoll() {
            return null;
        }
        
        public final void setStudentRoll(@org.jetbrains.annotations.Nullable()
        java.lang.Integer studentRoll) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getSectionName() {
            return null;
        }
        
        public final void setSectionName(@org.jetbrains.annotations.Nullable()
        java.lang.String sectionName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getClassTestName() {
            return null;
        }
        
        public final void setClassTestName(@org.jetbrains.annotations.Nullable()
        java.lang.String classTestName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getStudentName() {
            return null;
        }
        
        public final void setStudentName(@org.jetbrains.annotations.Nullable()
        java.lang.String studentName) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getMobileNo() {
            return null;
        }
        
        public final void setMobileNo(@org.jetbrains.annotations.Nullable()
        java.lang.String mobileNo) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Float getObtainedMarks() {
            return null;
        }
        
        public final void setObtainedMarks(@org.jetbrains.annotations.Nullable()
        java.lang.Float obtainedMarks) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Float getTotalMarks() {
            return null;
        }
        
        public final void setTotalMarks(@org.jetbrains.annotations.Nullable()
        java.lang.Float totalMarks) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.util.List<com.netizen.netiworld.model.UserPoint.ClassTestDetails.Item.StdCtExamMark> getStdCtExamMarks() {
            return null;
        }
        
        public final void setStdCtExamMarks(@org.jetbrains.annotations.Nullable()
        java.util.List<com.netizen.netiworld.model.UserPoint.ClassTestDetails.Item.StdCtExamMark> stdCtExamMarks) {
        }
        
        public Item() {
            super();
        }
        
        @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\r\u0010\r\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u000eJ\r\u0010\u000f\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u000eJ\r\u0010\u0010\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u000eJ\b\u0010\u0011\u001a\u0004\u0018\u00010\tJ\r\u0010\u0012\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u0013J\u0015\u0010\u0014\u001a\u00020\u00152\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0016J\u0015\u0010\u0017\u001a\u00020\u00152\b\u0010\u0006\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0016J\u0015\u0010\u0018\u001a\u00020\u00152\b\u0010\u0007\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\u0002\u0010\u0016J\u0010\u0010\u0019\u001a\u00020\u00152\b\u0010\b\u001a\u0004\u0018\u00010\tJ\u0015\u0010\u001a\u001a\u00020\u00152\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u001bR\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0005R\u0014\u0010\b\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\f\u00a8\u0006\u001c"}, d2 = {"Lcom/netizen/netiworld/model/UserPoint/ClassTestDetails$Item$StdCtExamMark;", "", "()V", "highestMarks", "", "Ljava/lang/Float;", "obtainedMark", "subjectFullMark", "subjectName", "", "viewSerial", "", "Ljava/lang/Integer;", "getHighestMarks", "()Ljava/lang/Float;", "getObtainedMark", "getSubjectFullMark", "getSubjectName", "getViewSerial", "()Ljava/lang/Integer;", "setHighestMarks", "", "(Ljava/lang/Float;)V", "setObtainedMark", "setSubjectFullMark", "setSubjectName", "setViewSerial", "(Ljava/lang/Integer;)V", "app_debug"})
        public static final class StdCtExamMark {
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "viewSerial")
            private java.lang.Integer viewSerial;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "subjectName")
            private java.lang.String subjectName;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "subjectFullMark")
            private java.lang.Float subjectFullMark;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "obtainedMark")
            private java.lang.Float obtainedMark;
            @com.google.gson.annotations.Expose()
            @com.google.gson.annotations.SerializedName(value = "highestMarks")
            private java.lang.Float highestMarks;
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Integer getViewSerial() {
                return null;
            }
            
            public final void setViewSerial(@org.jetbrains.annotations.Nullable()
            java.lang.Integer viewSerial) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.String getSubjectName() {
                return null;
            }
            
            public final void setSubjectName(@org.jetbrains.annotations.Nullable()
            java.lang.String subjectName) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Float getSubjectFullMark() {
                return null;
            }
            
            public final void setSubjectFullMark(@org.jetbrains.annotations.Nullable()
            java.lang.Float subjectFullMark) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Float getObtainedMark() {
                return null;
            }
            
            public final void setObtainedMark(@org.jetbrains.annotations.Nullable()
            java.lang.Float obtainedMark) {
            }
            
            @org.jetbrains.annotations.Nullable()
            public final java.lang.Float getHighestMarks() {
                return null;
            }
            
            public final void setHighestMarks(@org.jetbrains.annotations.Nullable()
            java.lang.Float highestMarks) {
            }
            
            public StdCtExamMark() {
                super();
            }
        }
    }
}