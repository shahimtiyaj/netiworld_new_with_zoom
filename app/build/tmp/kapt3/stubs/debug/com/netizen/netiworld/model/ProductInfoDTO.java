package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0011\b\u0016\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004R\"\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\b\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004\u00a8\u0006\t"}, d2 = {"Lcom/netizen/netiworld/model/ProductInfoDTO;", "", "productID", "", "(Ljava/lang/Integer;)V", "getProductID", "()Ljava/lang/Integer;", "setProductID", "Ljava/lang/Integer;", "app_debug"})
public final class ProductInfoDTO {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "productID")
    private java.lang.Integer productID = 0;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getProductID() {
        return null;
    }
    
    public final void setProductID(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    public ProductInfoDTO(@org.jetbrains.annotations.Nullable()
    java.lang.Integer productID) {
        super();
    }
}