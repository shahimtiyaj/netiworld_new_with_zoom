package com.netizen.netiworld.adapter.myPoint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u00132\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0013\u0014B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000e\u0010\u0005\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0006\u00a2\u0006\u0002\u0010\bJ\b\u0010\t\u001a\u00020\nH\u0016J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\nH\u0016J\u0018\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\nH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/netizen/netiworld/adapter/myPoint/PurchaseUsedCodeListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/netizen/netiworld/adapter/myPoint/PurchaseUsedCodeListAdapter$ViewHolder;", "context", "Landroid/content/Context;", "usedCodeReportsList", "", "Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata;", "(Landroid/content/Context;Ljava/util/List;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "Companion", "ViewHolder", "app_debug"})
public final class PurchaseUsedCodeListAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.netizen.netiworld.adapter.myPoint.PurchaseUsedCodeListAdapter.ViewHolder> {
    private final android.content.Context context = null;
    private final java.util.List<com.netizen.netiworld.model.PurchaseCodeLogGetdata> usedCodeReportsList = null;
    private static final java.lang.String TAG = null;
    public static final com.netizen.netiworld.adapter.myPoint.PurchaseUsedCodeListAdapter.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.netizen.netiworld.adapter.myPoint.PurchaseUsedCodeListAdapter.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.adapter.myPoint.PurchaseUsedCodeListAdapter.ViewHolder holder, int position) {
    }
    
    public PurchaseUsedCodeListAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<com.netizen.netiworld.model.PurchaseCodeLogGetdata> usedCodeReportsList) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/netizen/netiworld/adapter/myPoint/PurchaseUsedCodeListAdapter$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemBinding", "Lcom/netizen/netiworld/databinding/ReportsPurchaseUsedCodeRowBinding;", "(Lcom/netizen/netiworld/databinding/ReportsPurchaseUsedCodeRowBinding;)V", "bind", "", "unusedCodeReport", "Lcom/netizen/netiworld/model/PurchaseCodeLogGetdata;", "app_debug"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final com.netizen.netiworld.databinding.ReportsPurchaseUsedCodeRowBinding itemBinding = null;
        
        public final void bind(@org.jetbrains.annotations.Nullable()
        com.netizen.netiworld.model.PurchaseCodeLogGetdata unusedCodeReport) {
        }
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        com.netizen.netiworld.databinding.ReportsPurchaseUsedCodeRowBinding itemBinding) {
            super(null);
        }
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/netizen/netiworld/adapter/myPoint/PurchaseUsedCodeListAdapter$Companion;", "", "()V", "TAG", "", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}