package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0094\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010 \n\u0002\b\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u00062\u00020\u0007B\u0005\u00a2\u0006\u0002\u0010\bJ\b\u0010 \u001a\u00020!H\u0002J\b\u0010\"\u001a\u00020!H\u0002J\u0010\u0010#\u001a\u00020!2\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0012\u0010$\u001a\u00020!2\b\u0010%\u001a\u0004\u0018\u00010&H\u0016J&\u0010\'\u001a\u0004\u0018\u00010(2\u0006\u0010)\u001a\u00020*2\b\u0010+\u001a\u0004\u0018\u00010,2\b\u0010%\u001a\u0004\u0018\u00010&H\u0016J\u0010\u0010-\u001a\u00020!2\u0006\u0010.\u001a\u00020\u000fH\u0016J\u0010\u0010-\u001a\u00020!2\u0006\u0010/\u001a\u00020\u0011H\u0016J\u0010\u0010-\u001a\u00020!2\u0006\u00100\u001a\u00020\u0013H\u0016J\u0010\u0010-\u001a\u00020!2\u0006\u00101\u001a\u00020\u0019H\u0016J\u0010\u0010-\u001a\u00020!2\u0006\u00102\u001a\u00020\u001dH\u0016J\u0010\u0010-\u001a\u00020!2\u0006\u00103\u001a\u00020\u001fH\u0016J\u0010\u00104\u001a\u00020!2\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0016\u00105\u001a\u00020!2\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001d06H\u0002J\u0016\u00107\u001a\u00020!2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f06H\u0002J\u0016\u00108\u001a\u00020!2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u001106H\u0002J\u0016\u00109\u001a\u00020!2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u001306H\u0002J\u0010\u0010:\u001a\u00020!2\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0016\u0010;\u001a\u00020!2\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u001906H\u0002J\u0016\u0010<\u001a\u00020!2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001f06H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00190\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001d0\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001f0\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006="}, d2 = {"Lcom/netizen/netiworld/view/fragment/myPoint/startUp/myProfile/MyProfileFragment;", "Landroidx/fragment/app/Fragment;", "Lcom/netizen/netiworld/adapter/myPoint/BankInfoAdapter$OnEditClickListener;", "Lcom/netizen/netiworld/adapter/myPoint/EducationInfoAdapter$OnEditClickListener;", "Lcom/netizen/netiworld/adapter/myPoint/CertificationAdapter$OnEditClickListener;", "Lcom/netizen/netiworld/adapter/myPoint/ExperienceInfoAdapter$OnEditClickListener;", "Lcom/netizen/netiworld/adapter/myPoint/ReferenceInfoAdapter$OnEditClickListener;", "Lcom/netizen/netiworld/adapter/myPoint/TrainingInfoAdapter$OnEditClickListener;", "()V", "bankAccountViewModel", "Lcom/netizen/netiworld/viewModel/BankAccountViewModel;", "binding", "Lcom/netizen/netiworld/databinding/FragmentMyProfileBinding;", "certificationInfoList", "Ljava/util/ArrayList;", "Lcom/netizen/netiworld/model/CertificationInfo;", "educationInfoList", "Lcom/netizen/netiworld/model/EducationInfo;", "experienceInfoList", "Lcom/netizen/netiworld/model/ExperienceInfo;", "profileInformation", "Lcom/netizen/netiworld/model/ProfileInformation;", "profileViewModel", "Lcom/netizen/netiworld/viewModel/ProfileViewModel;", "referenceInfoList", "Lcom/netizen/netiworld/model/ReferenceInfo;", "spinnerPosition", "", "tagList", "Lcom/netizen/netiworld/model/TagGetData;", "trainingInfoList", "Lcom/netizen/netiworld/model/TrainingInfo;", "initObservers", "", "initViews", "onAddressInfoEditClick", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onEditClick", "certificationInfo", "educationInfo", "experienceInfo", "referenceInfo", "tagGetData", "trainingInfo", "onProfileInfoEditClick", "setBankInfo", "", "setCertificationInfo", "setEducationInfo", "setExperienceInfo", "setPersonalInformation", "setReferenceInfo", "setTrainingInfo", "app_debug"})
public final class MyProfileFragment extends androidx.fragment.app.Fragment implements com.netizen.netiworld.adapter.myPoint.BankInfoAdapter.OnEditClickListener, com.netizen.netiworld.adapter.myPoint.EducationInfoAdapter.OnEditClickListener, com.netizen.netiworld.adapter.myPoint.CertificationAdapter.OnEditClickListener, com.netizen.netiworld.adapter.myPoint.ExperienceInfoAdapter.OnEditClickListener, com.netizen.netiworld.adapter.myPoint.ReferenceInfoAdapter.OnEditClickListener, com.netizen.netiworld.adapter.myPoint.TrainingInfoAdapter.OnEditClickListener {
    private com.netizen.netiworld.databinding.FragmentMyProfileBinding binding;
    private com.netizen.netiworld.viewModel.ProfileViewModel profileViewModel;
    private com.netizen.netiworld.viewModel.BankAccountViewModel bankAccountViewModel;
    private java.util.ArrayList<com.netizen.netiworld.model.TagGetData> tagList;
    private java.util.ArrayList<com.netizen.netiworld.model.EducationInfo> educationInfoList;
    private java.util.ArrayList<com.netizen.netiworld.model.TrainingInfo> trainingInfoList;
    private java.util.ArrayList<com.netizen.netiworld.model.CertificationInfo> certificationInfoList;
    private java.util.ArrayList<com.netizen.netiworld.model.ExperienceInfo> experienceInfoList;
    private java.util.ArrayList<com.netizen.netiworld.model.ReferenceInfo> referenceInfoList;
    private int spinnerPosition = 0;
    private com.netizen.netiworld.model.ProfileInformation profileInformation;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public void onEditClick(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.TagGetData tagGetData) {
    }
    
    @java.lang.Override()
    public void onEditClick(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.EducationInfo educationInfo) {
    }
    
    @java.lang.Override()
    public void onEditClick(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.CertificationInfo certificationInfo) {
    }
    
    @java.lang.Override()
    public void onEditClick(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.ExperienceInfo experienceInfo) {
    }
    
    @java.lang.Override()
    public void onEditClick(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.TrainingInfo trainingInfo) {
    }
    
    @java.lang.Override()
    public void onEditClick(@org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.ReferenceInfo referenceInfo) {
    }
    
    private final void onProfileInfoEditClick(com.netizen.netiworld.model.ProfileInformation profileInformation) {
    }
    
    private final void onAddressInfoEditClick(com.netizen.netiworld.model.ProfileInformation profileInformation) {
    }
    
    private final void initViews() {
    }
    
    private final void initObservers() {
    }
    
    private final void setPersonalInformation(com.netizen.netiworld.model.ProfileInformation profileInformation) {
    }
    
    private final void setBankInfo(java.util.List<com.netizen.netiworld.model.TagGetData> tagList) {
    }
    
    private final void setEducationInfo(java.util.List<com.netizen.netiworld.model.EducationInfo> educationInfoList) {
    }
    
    private final void setTrainingInfo(java.util.List<com.netizen.netiworld.model.TrainingInfo> trainingInfoList) {
    }
    
    private final void setCertificationInfo(java.util.List<com.netizen.netiworld.model.CertificationInfo> certificationInfoList) {
    }
    
    private final void setExperienceInfo(java.util.List<com.netizen.netiworld.model.ExperienceInfo> experienceInfoList) {
    }
    
    private final void setReferenceInfo(java.util.List<com.netizen.netiworld.model.ReferenceInfo> referenceInfoList) {
    }
    
    public MyProfileFragment() {
        super();
    }
}