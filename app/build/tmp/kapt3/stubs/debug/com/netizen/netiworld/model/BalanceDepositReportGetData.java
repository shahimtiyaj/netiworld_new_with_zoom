package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b#\n\u0002\u0010\u0002\n\u0002\b\u0012\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004J\r\u0010\u0018\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0019J\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001J\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001J\b\u0010\u001c\u001a\u0004\u0018\u00010\u0004J\b\u0010\u001d\u001a\u0004\u0018\u00010\u0004J\r\u0010\u001e\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0019J\b\u0010\u001f\u001a\u0004\u0018\u00010\u0004J\b\u0010 \u001a\u0004\u0018\u00010\u0004J\b\u0010!\u001a\u0004\u0018\u00010\u0004J\b\u0010\"\u001a\u0004\u0018\u00010\u0004J\b\u0010#\u001a\u0004\u0018\u00010\u0004J\b\u0010$\u001a\u0004\u0018\u00010\u0004J\r\u0010%\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0019J\r\u0010&\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0019J\b\u0010\'\u001a\u0004\u0018\u00010\u0004J\b\u0010(\u001a\u0004\u0018\u00010\u0001J\u0010\u0010)\u001a\u00020*2\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004J\u0015\u0010+\u001a\u00020*2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010,J\u0010\u0010-\u001a\u00020*2\b\u0010\b\u001a\u0004\u0018\u00010\u0001J\u0010\u0010.\u001a\u00020*2\b\u0010\t\u001a\u0004\u0018\u00010\u0001J\u0010\u0010/\u001a\u00020*2\b\u0010\n\u001a\u0004\u0018\u00010\u0004J\u0010\u00100\u001a\u00020*2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0004J\u0015\u00101\u001a\u00020*2\b\u0010\f\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010,J\u0010\u00102\u001a\u00020*2\b\u0010\r\u001a\u0004\u0018\u00010\u0004J\u0010\u00103\u001a\u00020*2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0004J\u0010\u00104\u001a\u00020*2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004J\u0010\u00105\u001a\u00020*2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0004J\u0010\u00106\u001a\u00020*2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004J\u0010\u00107\u001a\u00020*2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0004J\u0015\u00108\u001a\u00020*2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010,J\u0015\u00109\u001a\u00020*2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010,J\u0010\u0010:\u001a\u00020*2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0004J\u0010\u0010;\u001a\u00020*2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\b\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\f\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\r\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0013\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0016\u0010\u0014\u001a\u0004\u0018\u00010\u00068\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0014\u0010\u0015\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0016\u001a\u0004\u0018\u00010\u00018\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006<"}, d2 = {"Lcom/netizen/netiworld/model/BalanceDepositReportGetData;", "", "()V", "accountNumber", "", "actualAmount", "", "Ljava/lang/Integer;", "approveDate", "approveNote", "attachFileName", "attachFilePath", "balanceRequestLogId", "bank", "fromWhere", "requestDate", "requestNote", "requestStatus", "requestType", "requestedAmount", "serviceChargeAmount", "transactionDate", "transactionNumber", "getAccountNumber", "getActualAmount", "()Ljava/lang/Integer;", "getApproveDate", "getApproveNote", "getAttachFileName", "getAttachFilePath", "getBalanceRequestLogId", "getBank", "getFromWhere", "getRequestDate", "getRequestNote", "getRequestStatus", "getRequestType", "getRequestedAmount", "getServiceChargeAmount", "getTransactionDate", "getTransactionNumber", "setAccountNumber", "", "setActualAmount", "(Ljava/lang/Integer;)V", "setApproveDate", "setApproveNote", "setAttachFileName", "setAttachFilePath", "setBalanceRequestLogId", "setBank", "setFromWhere", "setRequestDate", "setRequestNote", "setRequestStatus", "setRequestType", "setRequestedAmount", "setServiceChargeAmount", "setTransactionDate", "setTransactionNumber", "app_debug"})
public final class BalanceDepositReportGetData {
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "balanceRequestLogId")
    private java.lang.Integer balanceRequestLogId;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "requestStatus")
    private java.lang.String requestStatus;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "requestType")
    private java.lang.String requestType;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "requestDate")
    private java.lang.String requestDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "transactionDate")
    private java.lang.String transactionDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "requestedAmount")
    private java.lang.Integer requestedAmount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "actualAmount")
    private java.lang.Integer actualAmount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "serviceChargeAmount")
    private java.lang.Integer serviceChargeAmount;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "requestNote")
    private java.lang.String requestNote;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "approveDate")
    private java.lang.Object approveDate;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "approveNote")
    private java.lang.Object approveNote;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "attachFileName")
    private java.lang.String attachFileName;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "attachFilePath")
    private java.lang.String attachFilePath;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "transactionNumber")
    private java.lang.Object transactionNumber;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "fromWhere")
    private java.lang.String fromWhere;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "bank")
    private java.lang.String bank;
    @com.google.gson.annotations.Expose()
    @com.google.gson.annotations.SerializedName(value = "accountNumber")
    private java.lang.String accountNumber;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getBalanceRequestLogId() {
        return null;
    }
    
    public final void setBalanceRequestLogId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer balanceRequestLogId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRequestStatus() {
        return null;
    }
    
    public final void setRequestStatus(@org.jetbrains.annotations.Nullable()
    java.lang.String requestStatus) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRequestType() {
        return null;
    }
    
    public final void setRequestType(@org.jetbrains.annotations.Nullable()
    java.lang.String requestType) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRequestDate() {
        return null;
    }
    
    public final void setRequestDate(@org.jetbrains.annotations.Nullable()
    java.lang.String requestDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTransactionDate() {
        return null;
    }
    
    public final void setTransactionDate(@org.jetbrains.annotations.Nullable()
    java.lang.String transactionDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getRequestedAmount() {
        return null;
    }
    
    public final void setRequestedAmount(@org.jetbrains.annotations.Nullable()
    java.lang.Integer requestedAmount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getActualAmount() {
        return null;
    }
    
    public final void setActualAmount(@org.jetbrains.annotations.Nullable()
    java.lang.Integer actualAmount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getServiceChargeAmount() {
        return null;
    }
    
    public final void setServiceChargeAmount(@org.jetbrains.annotations.Nullable()
    java.lang.Integer serviceChargeAmount) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getRequestNote() {
        return null;
    }
    
    public final void setRequestNote(@org.jetbrains.annotations.Nullable()
    java.lang.String requestNote) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getApproveDate() {
        return null;
    }
    
    public final void setApproveDate(@org.jetbrains.annotations.Nullable()
    java.lang.Object approveDate) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getApproveNote() {
        return null;
    }
    
    public final void setApproveNote(@org.jetbrains.annotations.Nullable()
    java.lang.Object approveNote) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAttachFileName() {
        return null;
    }
    
    public final void setAttachFileName(@org.jetbrains.annotations.Nullable()
    java.lang.String attachFileName) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAttachFilePath() {
        return null;
    }
    
    public final void setAttachFilePath(@org.jetbrains.annotations.Nullable()
    java.lang.String attachFilePath) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getTransactionNumber() {
        return null;
    }
    
    public final void setTransactionNumber(@org.jetbrains.annotations.Nullable()
    java.lang.Object transactionNumber) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFromWhere() {
        return null;
    }
    
    public final void setFromWhere(@org.jetbrains.annotations.Nullable()
    java.lang.String fromWhere) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBank() {
        return null;
    }
    
    public final void setBank(@org.jetbrains.annotations.Nullable()
    java.lang.String bank) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getAccountNumber() {
        return null;
    }
    
    public final void setAccountNumber(@org.jetbrains.annotations.Nullable()
    java.lang.String accountNumber) {
    }
    
    public BalanceDepositReportGetData() {
        super();
    }
}