package com.netizen.netiworld.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\u0018\u00002\u00020\u0001B\u0007\b\u0016\u00a2\u0006\u0002\u0010\u0002B#\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tR \u0010\u0005\u001a\u0004\u0018\u00010\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR \u0010\u0007\u001a\u0004\u0018\u00010\b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\"\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0016\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015\u00a8\u0006\u0017"}, d2 = {"Lcom/netizen/netiworld/model/MessageRecharge;", "", "()V", "requestedAmount", "", "productInfoDTO", "Lcom/netizen/netiworld/model/ProductInfoDTO;", "productPurchaseLogDTO", "Lcom/netizen/netiworld/model/ProductPurchaseLogDTO;", "(Ljava/lang/Integer;Lcom/netizen/netiworld/model/ProductInfoDTO;Lcom/netizen/netiworld/model/ProductPurchaseLogDTO;)V", "getProductInfoDTO", "()Lcom/netizen/netiworld/model/ProductInfoDTO;", "setProductInfoDTO", "(Lcom/netizen/netiworld/model/ProductInfoDTO;)V", "getProductPurchaseLogDTO", "()Lcom/netizen/netiworld/model/ProductPurchaseLogDTO;", "setProductPurchaseLogDTO", "(Lcom/netizen/netiworld/model/ProductPurchaseLogDTO;)V", "getRequestedAmount", "()Ljava/lang/Integer;", "setRequestedAmount", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "app_debug"})
public final class MessageRecharge {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "quantity")
    private java.lang.Integer requestedAmount = 0;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "productInfoDTO")
    private com.netizen.netiworld.model.ProductInfoDTO productInfoDTO;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "productPurchaseLogDTO")
    private com.netizen.netiworld.model.ProductPurchaseLogDTO productPurchaseLogDTO;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getRequestedAmount() {
        return null;
    }
    
    public final void setRequestedAmount(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.ProductInfoDTO getProductInfoDTO() {
        return null;
    }
    
    public final void setProductInfoDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.ProductInfoDTO p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.netizen.netiworld.model.ProductPurchaseLogDTO getProductPurchaseLogDTO() {
        return null;
    }
    
    public final void setProductPurchaseLogDTO(@org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.ProductPurchaseLogDTO p0) {
    }
    
    public MessageRecharge() {
        super();
    }
    
    public MessageRecharge(@org.jetbrains.annotations.Nullable()
    java.lang.Integer requestedAmount, @org.jetbrains.annotations.Nullable()
    com.netizen.netiworld.model.ProductInfoDTO productInfoDTO, @org.jetbrains.annotations.NotNull()
    com.netizen.netiworld.model.ProductPurchaseLogDTO productPurchaseLogDTO) {
        super();
    }
}