package com.netizen.netiworld.repository.UserPoint;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\r\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J&\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020\u00152\u0006\u0010<\u001a\u00020\u00152\u0006\u0010=\u001a\u00020\u00152\u0006\u0010>\u001a\u00020\u0015J\u0006\u0010?\u001a\u00020:J\u001e\u0010\u0019\u001a\u00020:2\u0006\u0010;\u001a\u00020\u00152\u0006\u0010<\u001a\u00020\u00152\u0006\u0010@\u001a\u00020\u0015J\u0006\u0010\u001d\u001a\u00020:J\u0006\u0010A\u001a\u00020:J\u0006\u0010B\u001a\u00020:J\u001e\u0010C\u001a\u00020:2\u0006\u0010;\u001a\u00020\u00152\u0006\u0010<\u001a\u00020\u00152\u0006\u0010@\u001a\u00020\u0015J\u0006\u0010D\u001a\u00020:J\u0006\u0010E\u001a\u00020:J\u0006\u0010F\u001a\u00020:R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR \u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\f\"\u0004\b\r\u0010\u000eR \u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\f\"\u0004\b\u0010\u0010\u000eR \u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\f\"\u0004\b\u0012\u0010\u000eR\u001d\u0010\u0013\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00150\u00140\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\fR&\u0010\u0017\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u00140\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\f\"\u0004\b\u001a\u0010\u000eR&\u0010\u001b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001c0\u00140\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\f\"\u0004\b\u001e\u0010\u000eR&\u0010\u001f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020 0\u00140\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b!\u0010\f\"\u0004\b\"\u0010\u000eR&\u0010#\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020$0\u00140\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\f\"\u0004\b&\u0010\u000eR \u0010\'\u001a\b\u0012\u0004\u0012\u00020(0\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b)\u0010\f\"\u0004\b*\u0010\u000eR&\u0010+\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020,0\u00140\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b-\u0010\f\"\u0004\b.\u0010\u000eR&\u0010/\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002000\u00140\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\f\"\u0004\b2\u0010\u000eR&\u00103\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002040\u00140\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b5\u0010\f\"\u0004\b6\u0010\u000eR\u001d\u00107\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00150\u00140\n\u00a2\u0006\b\n\u0000\u001a\u0004\b8\u0010\f\u00a8\u0006G"}, d2 = {"Lcom/netizen/netiworld/repository/UserPoint/SPortalProfileRepository;", "", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "appPreferences", "Lcom/netizen/netiworld/utils/AppPreferences;", "getApplication", "()Landroid/app/Application;", "isStudentPortalClassTestDataFound", "Landroidx/lifecycle/MutableLiveData;", "", "()Landroidx/lifecycle/MutableLiveData;", "setStudentPortalClassTestDataFound", "(Landroidx/lifecycle/MutableLiveData;)V", "isStudentPortalFessDataFound", "setStudentPortalFessDataFound", "isStudentPortalSubjectDataFound", "setStudentPortalSubjectDataFound", "monthsList", "", "", "getMonthsList", "studentPortalClassTest", "Lcom/netizen/netiworld/model/UserPoint/ClassTest$Item;", "getStudentPortalClassTest", "setStudentPortalClassTest", "studentPortalClassTestDetails", "Lcom/netizen/netiworld/model/UserPoint/ClassTestDetails$Item$StdCtExamMark;", "getStudentPortalClassTestDetails", "setStudentPortalClassTestDetails", "studentPortalInventoryDetailsInfo", "Lcom/netizen/netiworld/model/UserPoint/InventoryDetails$Item;", "getStudentPortalInventoryDetailsInfo", "setStudentPortalInventoryDetailsInfo", "studentPortalPeriodData", "Lcom/netizen/netiworld/model/UserPoint/StudentPortalPeriod$Item;", "getStudentPortalPeriodData", "setStudentPortalPeriodData", "studentPortalProfileInfo", "Lcom/netizen/netiworld/model/UserPoint/StudentPortalProfile;", "getStudentPortalProfileInfo", "setStudentPortalProfileInfo", "studentPortalSubjectInfo", "Lcom/netizen/netiworld/model/UserPoint/StudentPortalSubject$Item;", "getStudentPortalSubjectInfo", "setStudentPortalSubjectInfo", "studentPortalUnpaidFeesInfo", "Lcom/netizen/netiworld/model/UserPoint/UnpaidFees$Item;", "getStudentPortalUnpaidFeesInfo", "setStudentPortalUnpaidFeesInfo", "studentPortalYearInfo", "Lcom/netizen/netiworld/model/UserPoint/YearByTypeId$Item;", "getStudentPortalYearInfo", "setStudentPortalYearInfo", "unpaidFeesList", "getUnpaidFeesList", "getStudentInventoryDetails", "", "instituteId", "studentId", "year", "monthName", "getStudentPortalAttendancePeriodData", "academicYear", "getStudentPortalProfile", "getStudentPortalSubjectsData", "getStudentPortalUnpaidFees", "getStudentPortalYear", "spinnerStudentPortalMonth", "spinnerStudentPortalUnpaidFees", "app_debug"})
public final class SPortalProfileRepository {
    private final com.netizen.netiworld.utils.AppPreferences appPreferences = null;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.UserPoint.StudentPortalProfile> studentPortalProfileInfo;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalSubject.Item>> studentPortalSubjectInfo;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isStudentPortalSubjectDataFound;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isStudentPortalClassTestDataFound;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalPeriod.Item>> studentPortalPeriodData;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.YearByTypeId.Item>> studentPortalYearInfo;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.ClassTest.Item>> studentPortalClassTest;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.UnpaidFees.Item>> studentPortalUnpaidFeesInfo;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.InventoryDetails.Item>> studentPortalInventoryDetailsInfo;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.ClassTestDetails.Item.StdCtExamMark>> studentPortalClassTestDetails;
    @org.jetbrains.annotations.NotNull()
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> isStudentPortalFessDataFound;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<java.lang.String>> unpaidFeesList = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<java.util.List<java.lang.String>> monthsList = null;
    @org.jetbrains.annotations.NotNull()
    private final android.app.Application application = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.UserPoint.StudentPortalProfile> getStudentPortalProfileInfo() {
        return null;
    }
    
    public final void setStudentPortalProfileInfo(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<com.netizen.netiworld.model.UserPoint.StudentPortalProfile> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalSubject.Item>> getStudentPortalSubjectInfo() {
        return null;
    }
    
    public final void setStudentPortalSubjectInfo(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalSubject.Item>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isStudentPortalSubjectDataFound() {
        return null;
    }
    
    public final void setStudentPortalSubjectDataFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isStudentPortalClassTestDataFound() {
        return null;
    }
    
    public final void setStudentPortalClassTestDataFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalPeriod.Item>> getStudentPortalPeriodData() {
        return null;
    }
    
    public final void setStudentPortalPeriodData(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.StudentPortalPeriod.Item>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.YearByTypeId.Item>> getStudentPortalYearInfo() {
        return null;
    }
    
    public final void setStudentPortalYearInfo(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.YearByTypeId.Item>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.ClassTest.Item>> getStudentPortalClassTest() {
        return null;
    }
    
    public final void setStudentPortalClassTest(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.ClassTest.Item>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.UnpaidFees.Item>> getStudentPortalUnpaidFeesInfo() {
        return null;
    }
    
    public final void setStudentPortalUnpaidFeesInfo(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.UnpaidFees.Item>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.InventoryDetails.Item>> getStudentPortalInventoryDetailsInfo() {
        return null;
    }
    
    public final void setStudentPortalInventoryDetailsInfo(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.InventoryDetails.Item>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.ClassTestDetails.Item.StdCtExamMark>> getStudentPortalClassTestDetails() {
        return null;
    }
    
    public final void setStudentPortalClassTestDetails(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.util.List<com.netizen.netiworld.model.UserPoint.ClassTestDetails.Item.StdCtExamMark>> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> isStudentPortalFessDataFound() {
        return null;
    }
    
    public final void setStudentPortalFessDataFound(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.MutableLiveData<java.lang.Boolean> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<java.lang.String>> getUnpaidFeesList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<java.lang.String>> getMonthsList() {
        return null;
    }
    
    public final void getStudentPortalProfile() {
    }
    
    public final void getStudentPortalSubjectsData() {
    }
    
    public final void getStudentPortalUnpaidFees(@org.jetbrains.annotations.NotNull()
    java.lang.String instituteId, @org.jetbrains.annotations.NotNull()
    java.lang.String studentId, @org.jetbrains.annotations.NotNull()
    java.lang.String academicYear) {
    }
    
    public final void getStudentPortalAttendancePeriodData() {
    }
    
    public final void getStudentPortalYear() {
    }
    
    public final void getStudentInventoryDetails(@org.jetbrains.annotations.NotNull()
    java.lang.String instituteId, @org.jetbrains.annotations.NotNull()
    java.lang.String studentId, @org.jetbrains.annotations.NotNull()
    java.lang.String year, @org.jetbrains.annotations.NotNull()
    java.lang.String monthName) {
    }
    
    public final void getStudentPortalClassTest(@org.jetbrains.annotations.NotNull()
    java.lang.String instituteId, @org.jetbrains.annotations.NotNull()
    java.lang.String studentId, @org.jetbrains.annotations.NotNull()
    java.lang.String academicYear) {
    }
    
    public final void getStudentPortalClassTestDetails() {
    }
    
    public final void spinnerStudentPortalUnpaidFees() {
    }
    
    public final void spinnerStudentPortalMonth() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.app.Application getApplication() {
        return null;
    }
    
    public SPortalProfileRepository(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super();
    }
}