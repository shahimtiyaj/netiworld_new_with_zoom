// Generated by data binding compiler. Do not edit!
package com.netizen.netiworld.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.netizen.netiworld.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class InstituteListItemBinding extends ViewDataBinding {
  @NonNull
  public final LinearLayout linear2;

  @NonNull
  public final TextView textViewEndTime;

  @NonNull
  public final TextView textViewPeriod;

  @NonNull
  public final TextView textViewStudentId;

  @NonNull
  public final TextView textViewSubject;

  @NonNull
  public final TextView textViewTeacherShortName;

  protected InstituteListItemBinding(Object _bindingComponent, View _root, int _localFieldCount,
      LinearLayout linear2, TextView textViewEndTime, TextView textViewPeriod,
      TextView textViewStudentId, TextView textViewSubject, TextView textViewTeacherShortName) {
    super(_bindingComponent, _root, _localFieldCount);
    this.linear2 = linear2;
    this.textViewEndTime = textViewEndTime;
    this.textViewPeriod = textViewPeriod;
    this.textViewStudentId = textViewStudentId;
    this.textViewSubject = textViewSubject;
    this.textViewTeacherShortName = textViewTeacherShortName;
  }

  @NonNull
  public static InstituteListItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.institute_list_item, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static InstituteListItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<InstituteListItemBinding>inflateInternal(inflater, R.layout.institute_list_item, root, attachToRoot, component);
  }

  @NonNull
  public static InstituteListItemBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.institute_list_item, null, false, component)
   */
  @NonNull
  @Deprecated
  public static InstituteListItemBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<InstituteListItemBinding>inflateInternal(inflater, R.layout.institute_list_item, null, false, component);
  }

  public static InstituteListItemBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static InstituteListItemBinding bind(@NonNull View view, @Nullable Object component) {
    return (InstituteListItemBinding)bind(component, view, R.layout.institute_list_item);
  }
}
