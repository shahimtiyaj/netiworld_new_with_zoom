// Generated by data binding compiler. Do not edit!
package com.netizen.netiworld.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.netizen.netiworld.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentMessageReportBinding extends ViewDataBinding {
  @NonNull
  public final RelativeLayout fragmentContainer;

  @NonNull
  public final RecyclerView recyclerViewRevenueReport;

  @NonNull
  public final TextView textViewFound;

  protected FragmentMessageReportBinding(Object _bindingComponent, View _root, int _localFieldCount,
      RelativeLayout fragmentContainer, RecyclerView recyclerViewRevenueReport,
      TextView textViewFound) {
    super(_bindingComponent, _root, _localFieldCount);
    this.fragmentContainer = fragmentContainer;
    this.recyclerViewRevenueReport = recyclerViewRevenueReport;
    this.textViewFound = textViewFound;
  }

  @NonNull
  public static FragmentMessageReportBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_message_report, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentMessageReportBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentMessageReportBinding>inflateInternal(inflater, R.layout.fragment_message_report, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentMessageReportBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_message_report, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentMessageReportBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentMessageReportBinding>inflateInternal(inflater, R.layout.fragment_message_report, null, false, component);
  }

  public static FragmentMessageReportBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentMessageReportBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentMessageReportBinding)bind(component, view, R.layout.fragment_message_report);
  }
}
