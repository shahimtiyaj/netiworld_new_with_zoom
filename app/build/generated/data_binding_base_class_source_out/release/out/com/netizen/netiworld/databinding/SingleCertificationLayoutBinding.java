// Generated by data binding compiler. Do not edit!
package com.netizen.netiworld.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.netizen.netiworld.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class SingleCertificationLayoutBinding extends ViewDataBinding {
  @NonNull
  public final ImageView imageViewQualificationEdit;

  @NonNull
  public final TextView textViewCertificationName;

  @NonNull
  public final TextView textViewDate;

  @NonNull
  public final TextView textViewDuration;

  @NonNull
  public final TextView textViewInstitute;

  @NonNull
  public final TextView textViewLocation;

  @NonNull
  public final TextView textViewQualificationNo;

  protected SingleCertificationLayoutBinding(Object _bindingComponent, View _root,
      int _localFieldCount, ImageView imageViewQualificationEdit,
      TextView textViewCertificationName, TextView textViewDate, TextView textViewDuration,
      TextView textViewInstitute, TextView textViewLocation, TextView textViewQualificationNo) {
    super(_bindingComponent, _root, _localFieldCount);
    this.imageViewQualificationEdit = imageViewQualificationEdit;
    this.textViewCertificationName = textViewCertificationName;
    this.textViewDate = textViewDate;
    this.textViewDuration = textViewDuration;
    this.textViewInstitute = textViewInstitute;
    this.textViewLocation = textViewLocation;
    this.textViewQualificationNo = textViewQualificationNo;
  }

  @NonNull
  public static SingleCertificationLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.single_certification_layout, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static SingleCertificationLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<SingleCertificationLayoutBinding>inflateInternal(inflater, R.layout.single_certification_layout, root, attachToRoot, component);
  }

  @NonNull
  public static SingleCertificationLayoutBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.single_certification_layout, null, false, component)
   */
  @NonNull
  @Deprecated
  public static SingleCertificationLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<SingleCertificationLayoutBinding>inflateInternal(inflater, R.layout.single_certification_layout, null, false, component);
  }

  public static SingleCertificationLayoutBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static SingleCertificationLayoutBinding bind(@NonNull View view,
      @Nullable Object component) {
    return (SingleCertificationLayoutBinding)bind(component, view, R.layout.single_certification_layout);
  }
}
