// Generated by data binding compiler. Do not edit!
package com.netizen.netiworld.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.netizen.netiworld.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentOfferProductBinding extends ViewDataBinding {
  @NonNull
  public final Button btnPurchase;

  @NonNull
  public final ImageButton btnSerachOfferCode;

  @NonNull
  public final TextView code;

  @NonNull
  public final TextView codeUsable;

  @NonNull
  public final TextView codeUsed;

  @NonNull
  public final TextView discountAmount;

  @NonNull
  public final TextView etDiscountPercentage;

  @NonNull
  public final TextInputEditText etOfferCode;

  @NonNull
  public final TextInputLayout etUsernameLayout;

  @NonNull
  public final RelativeLayout fragmentContainer;

  @NonNull
  public final LinearLayout linearDiscount;

  @NonNull
  public final LinearLayout linearInfo;

  @NonNull
  public final RelativeLayout lottieProgressbar;

  @NonNull
  public final TextView payableAmount;

  @NonNull
  public final TextView pname;

  @NonNull
  public final TextView productQuantity;

  @NonNull
  public final RelativeLayout relative;

  @NonNull
  public final RelativeLayout relativeOfferCode;

  @NonNull
  public final TextView textViewWalletBalance;

  @NonNull
  public final TextView totalAmount;

  @NonNull
  public final TextView txtNetiId;

  @NonNull
  public final TextView unitPrice;

  protected FragmentOfferProductBinding(Object _bindingComponent, View _root, int _localFieldCount,
      Button btnPurchase, ImageButton btnSerachOfferCode, TextView code, TextView codeUsable,
      TextView codeUsed, TextView discountAmount, TextView etDiscountPercentage,
      TextInputEditText etOfferCode, TextInputLayout etUsernameLayout,
      RelativeLayout fragmentContainer, LinearLayout linearDiscount, LinearLayout linearInfo,
      RelativeLayout lottieProgressbar, TextView payableAmount, TextView pname,
      TextView productQuantity, RelativeLayout relative, RelativeLayout relativeOfferCode,
      TextView textViewWalletBalance, TextView totalAmount, TextView txtNetiId,
      TextView unitPrice) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btnPurchase = btnPurchase;
    this.btnSerachOfferCode = btnSerachOfferCode;
    this.code = code;
    this.codeUsable = codeUsable;
    this.codeUsed = codeUsed;
    this.discountAmount = discountAmount;
    this.etDiscountPercentage = etDiscountPercentage;
    this.etOfferCode = etOfferCode;
    this.etUsernameLayout = etUsernameLayout;
    this.fragmentContainer = fragmentContainer;
    this.linearDiscount = linearDiscount;
    this.linearInfo = linearInfo;
    this.lottieProgressbar = lottieProgressbar;
    this.payableAmount = payableAmount;
    this.pname = pname;
    this.productQuantity = productQuantity;
    this.relative = relative;
    this.relativeOfferCode = relativeOfferCode;
    this.textViewWalletBalance = textViewWalletBalance;
    this.totalAmount = totalAmount;
    this.txtNetiId = txtNetiId;
    this.unitPrice = unitPrice;
  }

  @NonNull
  public static FragmentOfferProductBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_offer_product, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentOfferProductBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentOfferProductBinding>inflateInternal(inflater, R.layout.fragment_offer_product, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentOfferProductBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_offer_product, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentOfferProductBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentOfferProductBinding>inflateInternal(inflater, R.layout.fragment_offer_product, null, false, component);
  }

  public static FragmentOfferProductBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentOfferProductBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentOfferProductBinding)bind(component, view, R.layout.fragment_offer_product);
  }
}
