// Generated by data binding compiler. Do not edit!
package com.netizen.netiworld.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.netizen.netiworld.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentTokenDetailsBinding extends ViewDataBinding {
  @NonNull
  public final LinearLayout layoutAddTag;

  @NonNull
  public final RelativeLayout lottieProgressbar;

  @NonNull
  public final TextView textViewContactNo;

  @NonNull
  public final TextView textViewDownload;

  @NonNull
  public final TextView textViewProblemDetails;

  @NonNull
  public final TextView textViewProblemType;

  @NonNull
  public final RatingBar textViewRating;

  @NonNull
  public final TextView textViewSolvedDate;

  @NonNull
  public final TextView textViewStatus;

  @NonNull
  public final TextView textViewSubmitDate;

  @NonNull
  public final TextView textViewTokenId;

  protected FragmentTokenDetailsBinding(Object _bindingComponent, View _root, int _localFieldCount,
      LinearLayout layoutAddTag, RelativeLayout lottieProgressbar, TextView textViewContactNo,
      TextView textViewDownload, TextView textViewProblemDetails, TextView textViewProblemType,
      RatingBar textViewRating, TextView textViewSolvedDate, TextView textViewStatus,
      TextView textViewSubmitDate, TextView textViewTokenId) {
    super(_bindingComponent, _root, _localFieldCount);
    this.layoutAddTag = layoutAddTag;
    this.lottieProgressbar = lottieProgressbar;
    this.textViewContactNo = textViewContactNo;
    this.textViewDownload = textViewDownload;
    this.textViewProblemDetails = textViewProblemDetails;
    this.textViewProblemType = textViewProblemType;
    this.textViewRating = textViewRating;
    this.textViewSolvedDate = textViewSolvedDate;
    this.textViewStatus = textViewStatus;
    this.textViewSubmitDate = textViewSubmitDate;
    this.textViewTokenId = textViewTokenId;
  }

  @NonNull
  public static FragmentTokenDetailsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_token_details, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentTokenDetailsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentTokenDetailsBinding>inflateInternal(inflater, R.layout.fragment_token_details, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentTokenDetailsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_token_details, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentTokenDetailsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentTokenDetailsBinding>inflateInternal(inflater, R.layout.fragment_token_details, null, false, component);
  }

  public static FragmentTokenDetailsBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentTokenDetailsBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentTokenDetailsBinding)bind(component, view, R.layout.fragment_token_details);
  }
}
