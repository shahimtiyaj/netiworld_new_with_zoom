// Generated by data binding compiler. Do not edit!
package com.netizen.netiworld.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.netizen.netiworld.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentTransferBinding extends ViewDataBinding {
  @NonNull
  public final ImageButton btnSerachTrnsferNetiId;

  @NonNull
  public final TextInputEditText editTextTransferAmount;

  @NonNull
  public final TextView etMobileNo;

  @NonNull
  public final TextView etName;

  @NonNull
  public final TextInputEditText etNetiId;

  @NonNull
  public final TextInputLayout etUsernameLayout;

  @NonNull
  public final LinearLayout layoutMobile;

  @NonNull
  public final LinearLayout layoutName;

  @NonNull
  public final RelativeLayout lottieProgressbar;

  @NonNull
  public final LinearLayout mmainLayoutId;

  @NonNull
  public final FragmentTrabsferOtpBinding otpLayout;

  @NonNull
  public final RelativeLayout relativeOfferCode;

  @NonNull
  public final Button transferBtn;

  @NonNull
  public final TextInputEditText transferNote;

  protected FragmentTransferBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ImageButton btnSerachTrnsferNetiId, TextInputEditText editTextTransferAmount,
      TextView etMobileNo, TextView etName, TextInputEditText etNetiId,
      TextInputLayout etUsernameLayout, LinearLayout layoutMobile, LinearLayout layoutName,
      RelativeLayout lottieProgressbar, LinearLayout mmainLayoutId,
      FragmentTrabsferOtpBinding otpLayout, RelativeLayout relativeOfferCode, Button transferBtn,
      TextInputEditText transferNote) {
    super(_bindingComponent, _root, _localFieldCount);
    this.btnSerachTrnsferNetiId = btnSerachTrnsferNetiId;
    this.editTextTransferAmount = editTextTransferAmount;
    this.etMobileNo = etMobileNo;
    this.etName = etName;
    this.etNetiId = etNetiId;
    this.etUsernameLayout = etUsernameLayout;
    this.layoutMobile = layoutMobile;
    this.layoutName = layoutName;
    this.lottieProgressbar = lottieProgressbar;
    this.mmainLayoutId = mmainLayoutId;
    this.otpLayout = otpLayout;
    setContainedBinding(this.otpLayout);
    this.relativeOfferCode = relativeOfferCode;
    this.transferBtn = transferBtn;
    this.transferNote = transferNote;
  }

  @NonNull
  public static FragmentTransferBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_transfer, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentTransferBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentTransferBinding>inflateInternal(inflater, R.layout.fragment_transfer, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentTransferBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_transfer, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentTransferBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentTransferBinding>inflateInternal(inflater, R.layout.fragment_transfer, null, false, component);
  }

  public static FragmentTransferBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentTransferBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentTransferBinding)bind(component, view, R.layout.fragment_transfer);
  }
}
