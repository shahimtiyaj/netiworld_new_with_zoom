// Generated by data binding compiler. Do not edit!
package com.netizen.netiworld.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.netizen.netiworld.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class SingleEducationInfoLayoutBinding extends ViewDataBinding {
  @NonNull
  public final ImageView imageViewAcademicInfoEdit;

  @NonNull
  public final TextView textViewAchievement;

  @NonNull
  public final TextView textViewBoard;

  @NonNull
  public final TextView textViewDuration;

  @NonNull
  public final TextView textViewEducationNo;

  @NonNull
  public final TextView textViewExamTitle;

  @NonNull
  public final TextView textViewForeignInstitute;

  @NonNull
  public final TextView textViewInstituteName;

  @NonNull
  public final TextView textViewLevelOfEducation;

  @NonNull
  public final TextView textViewMajor;

  @NonNull
  public final TextView textViewPassingYear;

  @NonNull
  public final TextView textViewResult;

  protected SingleEducationInfoLayoutBinding(Object _bindingComponent, View _root,
      int _localFieldCount, ImageView imageViewAcademicInfoEdit, TextView textViewAchievement,
      TextView textViewBoard, TextView textViewDuration, TextView textViewEducationNo,
      TextView textViewExamTitle, TextView textViewForeignInstitute, TextView textViewInstituteName,
      TextView textViewLevelOfEducation, TextView textViewMajor, TextView textViewPassingYear,
      TextView textViewResult) {
    super(_bindingComponent, _root, _localFieldCount);
    this.imageViewAcademicInfoEdit = imageViewAcademicInfoEdit;
    this.textViewAchievement = textViewAchievement;
    this.textViewBoard = textViewBoard;
    this.textViewDuration = textViewDuration;
    this.textViewEducationNo = textViewEducationNo;
    this.textViewExamTitle = textViewExamTitle;
    this.textViewForeignInstitute = textViewForeignInstitute;
    this.textViewInstituteName = textViewInstituteName;
    this.textViewLevelOfEducation = textViewLevelOfEducation;
    this.textViewMajor = textViewMajor;
    this.textViewPassingYear = textViewPassingYear;
    this.textViewResult = textViewResult;
  }

  @NonNull
  public static SingleEducationInfoLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.single_education_info_layout, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static SingleEducationInfoLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<SingleEducationInfoLayoutBinding>inflateInternal(inflater, R.layout.single_education_info_layout, root, attachToRoot, component);
  }

  @NonNull
  public static SingleEducationInfoLayoutBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.single_education_info_layout, null, false, component)
   */
  @NonNull
  @Deprecated
  public static SingleEducationInfoLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<SingleEducationInfoLayoutBinding>inflateInternal(inflater, R.layout.single_education_info_layout, null, false, component);
  }

  public static SingleEducationInfoLayoutBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static SingleEducationInfoLayoutBinding bind(@NonNull View view,
      @Nullable Object component) {
    return (SingleEducationInfoLayoutBinding)bind(component, view, R.layout.single_education_info_layout);
  }
}
