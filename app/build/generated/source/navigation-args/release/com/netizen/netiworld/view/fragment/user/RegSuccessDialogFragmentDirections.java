package com.netizen.netiworld.view.fragment.user;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class RegSuccessDialogFragmentDirections {
  private RegSuccessDialogFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionRegSuccessDialogFragmentToSignInFragment() {
    return new ActionOnlyNavDirections(R.id.action_regSuccessDialogFragment_to_signInFragment);
  }
}
