package com.netizen.netiworld.view.fragment.user;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class FragmentForgotPasswordOneDirections {
  private FragmentForgotPasswordOneDirections() {
  }

  @NonNull
  public static NavDirections actionFragmentForgotPasswordOneToFragmentForgotPasswordTwo() {
    return new ActionOnlyNavDirections(R.id.action_fragmentForgotPasswordOne_to_fragmentForgotPasswordTwo);
  }
}
