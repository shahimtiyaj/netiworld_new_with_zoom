package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import com.netizen.netiworld.model.ReferenceInfo;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class AddReferenceInfoFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private AddReferenceInfoFragmentArgs() {
  }

  private AddReferenceInfoFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static AddReferenceInfoFragmentArgs fromBundle(@NonNull Bundle bundle) {
    AddReferenceInfoFragmentArgs __result = new AddReferenceInfoFragmentArgs();
    bundle.setClassLoader(AddReferenceInfoFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("referenceData")) {
      ReferenceInfo referenceData;
      if (Parcelable.class.isAssignableFrom(ReferenceInfo.class) || Serializable.class.isAssignableFrom(ReferenceInfo.class)) {
        referenceData = (ReferenceInfo) bundle.get("referenceData");
      } else {
        throw new UnsupportedOperationException(ReferenceInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("referenceData", referenceData);
    } else {
      throw new IllegalArgumentException("Required argument \"referenceData\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("title")) {
      String title;
      title = bundle.getString("title");
      __result.arguments.put("title", title);
    } else {
      throw new IllegalArgumentException("Required argument \"title\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public ReferenceInfo getReferenceData() {
    return (ReferenceInfo) arguments.get("referenceData");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getTitle() {
    return (String) arguments.get("title");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("referenceData")) {
      ReferenceInfo referenceData = (ReferenceInfo) arguments.get("referenceData");
      if (Parcelable.class.isAssignableFrom(ReferenceInfo.class) || referenceData == null) {
        __result.putParcelable("referenceData", Parcelable.class.cast(referenceData));
      } else if (Serializable.class.isAssignableFrom(ReferenceInfo.class)) {
        __result.putSerializable("referenceData", Serializable.class.cast(referenceData));
      } else {
        throw new UnsupportedOperationException(ReferenceInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("title")) {
      String title = (String) arguments.get("title");
      __result.putString("title", title);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    AddReferenceInfoFragmentArgs that = (AddReferenceInfoFragmentArgs) object;
    if (arguments.containsKey("referenceData") != that.arguments.containsKey("referenceData")) {
      return false;
    }
    if (getReferenceData() != null ? !getReferenceData().equals(that.getReferenceData()) : that.getReferenceData() != null) {
      return false;
    }
    if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
      return false;
    }
    if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getReferenceData() != null ? getReferenceData().hashCode() : 0);
    result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "AddReferenceInfoFragmentArgs{"
        + "referenceData=" + getReferenceData()
        + ", title=" + getTitle()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(AddReferenceInfoFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@Nullable ReferenceInfo referenceData, @Nullable String title) {
      this.arguments.put("referenceData", referenceData);
      this.arguments.put("title", title);
    }

    @NonNull
    public AddReferenceInfoFragmentArgs build() {
      AddReferenceInfoFragmentArgs result = new AddReferenceInfoFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setReferenceData(@Nullable ReferenceInfo referenceData) {
      this.arguments.put("referenceData", referenceData);
      return this;
    }

    @NonNull
    public Builder setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public ReferenceInfo getReferenceData() {
      return (ReferenceInfo) arguments.get("referenceData");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }
  }
}
