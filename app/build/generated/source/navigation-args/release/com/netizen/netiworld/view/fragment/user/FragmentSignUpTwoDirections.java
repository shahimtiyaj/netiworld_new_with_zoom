package com.netizen.netiworld.view.fragment.user;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class FragmentSignUpTwoDirections {
  private FragmentSignUpTwoDirections() {
  }

  @NonNull
  public static NavDirections actionFragmentSignUpTwoToSignInFragment() {
    return new ActionOnlyNavDirections(R.id.action_fragmentSignUpTwo_to_signInFragment);
  }
}
