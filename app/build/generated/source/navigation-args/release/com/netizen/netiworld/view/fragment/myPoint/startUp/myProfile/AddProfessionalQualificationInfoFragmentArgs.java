package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import com.netizen.netiworld.model.CertificationInfo;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class AddProfessionalQualificationInfoFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private AddProfessionalQualificationInfoFragmentArgs() {
  }

  private AddProfessionalQualificationInfoFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static AddProfessionalQualificationInfoFragmentArgs fromBundle(@NonNull Bundle bundle) {
    AddProfessionalQualificationInfoFragmentArgs __result = new AddProfessionalQualificationInfoFragmentArgs();
    bundle.setClassLoader(AddProfessionalQualificationInfoFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("certificationData")) {
      CertificationInfo certificationData;
      if (Parcelable.class.isAssignableFrom(CertificationInfo.class) || Serializable.class.isAssignableFrom(CertificationInfo.class)) {
        certificationData = (CertificationInfo) bundle.get("certificationData");
      } else {
        throw new UnsupportedOperationException(CertificationInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("certificationData", certificationData);
    } else {
      throw new IllegalArgumentException("Required argument \"certificationData\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("title")) {
      String title;
      title = bundle.getString("title");
      __result.arguments.put("title", title);
    } else {
      throw new IllegalArgumentException("Required argument \"title\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public CertificationInfo getCertificationData() {
    return (CertificationInfo) arguments.get("certificationData");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getTitle() {
    return (String) arguments.get("title");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("certificationData")) {
      CertificationInfo certificationData = (CertificationInfo) arguments.get("certificationData");
      if (Parcelable.class.isAssignableFrom(CertificationInfo.class) || certificationData == null) {
        __result.putParcelable("certificationData", Parcelable.class.cast(certificationData));
      } else if (Serializable.class.isAssignableFrom(CertificationInfo.class)) {
        __result.putSerializable("certificationData", Serializable.class.cast(certificationData));
      } else {
        throw new UnsupportedOperationException(CertificationInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("title")) {
      String title = (String) arguments.get("title");
      __result.putString("title", title);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    AddProfessionalQualificationInfoFragmentArgs that = (AddProfessionalQualificationInfoFragmentArgs) object;
    if (arguments.containsKey("certificationData") != that.arguments.containsKey("certificationData")) {
      return false;
    }
    if (getCertificationData() != null ? !getCertificationData().equals(that.getCertificationData()) : that.getCertificationData() != null) {
      return false;
    }
    if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
      return false;
    }
    if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getCertificationData() != null ? getCertificationData().hashCode() : 0);
    result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "AddProfessionalQualificationInfoFragmentArgs{"
        + "certificationData=" + getCertificationData()
        + ", title=" + getTitle()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(AddProfessionalQualificationInfoFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@Nullable CertificationInfo certificationData, @Nullable String title) {
      this.arguments.put("certificationData", certificationData);
      this.arguments.put("title", title);
    }

    @NonNull
    public AddProfessionalQualificationInfoFragmentArgs build() {
      AddProfessionalQualificationInfoFragmentArgs result = new AddProfessionalQualificationInfoFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setCertificationData(@Nullable CertificationInfo certificationData) {
      this.arguments.put("certificationData", certificationData);
      return this;
    }

    @NonNull
    public Builder setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public CertificationInfo getCertificationData() {
      return (CertificationInfo) arguments.get("certificationData");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }
  }
}
