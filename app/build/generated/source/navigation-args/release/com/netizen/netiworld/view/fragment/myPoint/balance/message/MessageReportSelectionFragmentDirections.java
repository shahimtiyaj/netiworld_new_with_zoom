package com.netizen.netiworld.view.fragment.myPoint.balance.message;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class MessageReportSelectionFragmentDirections {
  private MessageReportSelectionFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionMessageReportSelectionFragmentToReportMessageRechargeFragment(
      ) {
    return new ActionOnlyNavDirections(R.id.action_messageReportSelectionFragment_to_reportMessageRechargeFragment);
  }
}
