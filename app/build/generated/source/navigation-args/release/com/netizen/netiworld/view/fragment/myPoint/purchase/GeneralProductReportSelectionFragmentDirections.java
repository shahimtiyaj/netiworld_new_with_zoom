package com.netizen.netiworld.view.fragment.myPoint.purchase;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class GeneralProductReportSelectionFragmentDirections {
  private GeneralProductReportSelectionFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionGeneralProductReportSelectionFragmentToGeneralProductReportFragment(
      ) {
    return new ActionOnlyNavDirections(R.id.action_generalProductReportSelectionFragment_to_generalProductReportFragment);
  }
}
