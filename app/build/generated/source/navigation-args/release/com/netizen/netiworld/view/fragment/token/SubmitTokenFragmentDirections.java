package com.netizen.netiworld.view.fragment.token;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class SubmitTokenFragmentDirections {
  private SubmitTokenFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionSubmitTokenFragmentToTokenListFragment() {
    return new ActionOnlyNavDirections(R.id.action_submitTokenFragment_to_tokenListFragment);
  }

  @NonNull
  public static NavDirections actionSubmitTokenFragmentToFragmentSetting() {
    return new ActionOnlyNavDirections(R.id.action_submitTokenFragment_to_fragmentSetting);
  }
}
