package com.netizen.netiworld.view.fragment.user;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class FragmentSignInDirections {
  private FragmentSignInDirections() {
  }

  @NonNull
  public static NavDirections actionSignInFragmentToHomeFragment() {
    return new ActionOnlyNavDirections(R.id.action_signInFragment_to_homeFragment);
  }

  @NonNull
  public static NavDirections actionSignInFragmentToFragmentSignUpOne() {
    return new ActionOnlyNavDirections(R.id.action_signInFragment_to_fragmentSignUpOne);
  }

  @NonNull
  public static NavDirections actionSignInFragmentToFragmentForgotPasswordOne() {
    return new ActionOnlyNavDirections(R.id.action_signInFragment_to_fragmentForgotPasswordOne);
  }
}
