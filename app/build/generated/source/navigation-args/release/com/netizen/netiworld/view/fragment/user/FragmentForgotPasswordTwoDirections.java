package com.netizen.netiworld.view.fragment.user;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class FragmentForgotPasswordTwoDirections {
  private FragmentForgotPasswordTwoDirections() {
  }

  @NonNull
  public static NavDirections actionFragmentForgotPasswordTwoToFragmentForgotPasswordThree() {
    return new ActionOnlyNavDirections(R.id.action_fragmentForgotPasswordTwo_to_fragmentForgotPasswordThree);
  }
}
