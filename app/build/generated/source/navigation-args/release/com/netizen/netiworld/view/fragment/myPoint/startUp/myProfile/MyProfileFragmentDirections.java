package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;
import com.netizen.netiworld.model.CertificationInfo;
import com.netizen.netiworld.model.EducationInfo;
import com.netizen.netiworld.model.ExperienceInfo;
import com.netizen.netiworld.model.ProfileInformation;
import com.netizen.netiworld.model.ReferenceInfo;
import com.netizen.netiworld.model.TagGetData;
import com.netizen.netiworld.model.TrainingInfo;
import java.io.Serializable;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class MyProfileFragmentDirections {
  private MyProfileFragmentDirections() {
  }

  @NonNull
  public static ActionMyProfileFragmentToAddBankInfoFragment actionMyProfileFragmentToAddBankInfoFragment(
      @Nullable TagGetData tagData, @Nullable String title) {
    return new ActionMyProfileFragmentToAddBankInfoFragment(tagData, title);
  }

  @NonNull
  public static ActionMyProfileFragmentToAddAcademicEducationFragment actionMyProfileFragmentToAddAcademicEducationFragment(
      @Nullable EducationInfo educationData, @Nullable String title) {
    return new ActionMyProfileFragmentToAddAcademicEducationFragment(educationData, title);
  }

  @NonNull
  public static ActionMyProfileFragmentToAddTrainingInfoFragment actionMyProfileFragmentToAddTrainingInfoFragment(
      @Nullable TrainingInfo trainingInfo, @Nullable String title) {
    return new ActionMyProfileFragmentToAddTrainingInfoFragment(trainingInfo, title);
  }

  @NonNull
  public static ActionMyProfileFragmentToAddProfessionalQualificationInfoFragment actionMyProfileFragmentToAddProfessionalQualificationInfoFragment(
      @Nullable CertificationInfo certificationData, @Nullable String title) {
    return new ActionMyProfileFragmentToAddProfessionalQualificationInfoFragment(certificationData, title);
  }

  @NonNull
  public static ActionMyProfileFragmentToAddExperienceInfoFragment actionMyProfileFragmentToAddExperienceInfoFragment(
      @Nullable ExperienceInfo experienceData, @Nullable String title) {
    return new ActionMyProfileFragmentToAddExperienceInfoFragment(experienceData, title);
  }

  @NonNull
  public static ActionMyProfileFragmentToAddReferenceInfoFragment actionMyProfileFragmentToAddReferenceInfoFragment(
      @Nullable ReferenceInfo referenceData, @Nullable String title) {
    return new ActionMyProfileFragmentToAddReferenceInfoFragment(referenceData, title);
  }

  @NonNull
  public static ActionMyProfileFragmentToUpdatePersonalInfoFragment actionMyProfileFragmentToUpdatePersonalInfoFragment(
      @Nullable ProfileInformation personalData, @Nullable String title) {
    return new ActionMyProfileFragmentToUpdatePersonalInfoFragment(personalData, title);
  }

  @NonNull
  public static ActionMyProfileFragmentToUpdateAddressInfoFragment actionMyProfileFragmentToUpdateAddressInfoFragment(
      @Nullable ProfileInformation addressData, @Nullable String title) {
    return new ActionMyProfileFragmentToUpdateAddressInfoFragment(addressData, title);
  }

  @NonNull
  public static NavDirections actionMyProfileFragmentToUpdateUserInfoFragment() {
    return new ActionOnlyNavDirections(R.id.action_myProfileFragment_to_updateUserInfoFragment);
  }

  public static class ActionMyProfileFragmentToAddBankInfoFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionMyProfileFragmentToAddBankInfoFragment(@Nullable TagGetData tagData,
        @Nullable String title) {
      this.arguments.put("tagData", tagData);
      this.arguments.put("title", title);
    }

    @NonNull
    public ActionMyProfileFragmentToAddBankInfoFragment setTagData(@Nullable TagGetData tagData) {
      this.arguments.put("tagData", tagData);
      return this;
    }

    @NonNull
    public ActionMyProfileFragmentToAddBankInfoFragment setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("tagData")) {
        TagGetData tagData = (TagGetData) arguments.get("tagData");
        if (Parcelable.class.isAssignableFrom(TagGetData.class) || tagData == null) {
          __result.putParcelable("tagData", Parcelable.class.cast(tagData));
        } else if (Serializable.class.isAssignableFrom(TagGetData.class)) {
          __result.putSerializable("tagData", Serializable.class.cast(tagData));
        } else {
          throw new UnsupportedOperationException(TagGetData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("title")) {
        String title = (String) arguments.get("title");
        __result.putString("title", title);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_myProfileFragment_to_addBankInfoFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public TagGetData getTagData() {
      return (TagGetData) arguments.get("tagData");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionMyProfileFragmentToAddBankInfoFragment that = (ActionMyProfileFragmentToAddBankInfoFragment) object;
      if (arguments.containsKey("tagData") != that.arguments.containsKey("tagData")) {
        return false;
      }
      if (getTagData() != null ? !getTagData().equals(that.getTagData()) : that.getTagData() != null) {
        return false;
      }
      if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
        return false;
      }
      if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getTagData() != null ? getTagData().hashCode() : 0);
      result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionMyProfileFragmentToAddBankInfoFragment(actionId=" + getActionId() + "){"
          + "tagData=" + getTagData()
          + ", title=" + getTitle()
          + "}";
    }
  }

  public static class ActionMyProfileFragmentToAddAcademicEducationFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionMyProfileFragmentToAddAcademicEducationFragment(
        @Nullable EducationInfo educationData, @Nullable String title) {
      this.arguments.put("educationData", educationData);
      this.arguments.put("title", title);
    }

    @NonNull
    public ActionMyProfileFragmentToAddAcademicEducationFragment setEducationData(
        @Nullable EducationInfo educationData) {
      this.arguments.put("educationData", educationData);
      return this;
    }

    @NonNull
    public ActionMyProfileFragmentToAddAcademicEducationFragment setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("educationData")) {
        EducationInfo educationData = (EducationInfo) arguments.get("educationData");
        if (Parcelable.class.isAssignableFrom(EducationInfo.class) || educationData == null) {
          __result.putParcelable("educationData", Parcelable.class.cast(educationData));
        } else if (Serializable.class.isAssignableFrom(EducationInfo.class)) {
          __result.putSerializable("educationData", Serializable.class.cast(educationData));
        } else {
          throw new UnsupportedOperationException(EducationInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("title")) {
        String title = (String) arguments.get("title");
        __result.putString("title", title);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_myProfileFragment_to_addAcademicEducationFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public EducationInfo getEducationData() {
      return (EducationInfo) arguments.get("educationData");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionMyProfileFragmentToAddAcademicEducationFragment that = (ActionMyProfileFragmentToAddAcademicEducationFragment) object;
      if (arguments.containsKey("educationData") != that.arguments.containsKey("educationData")) {
        return false;
      }
      if (getEducationData() != null ? !getEducationData().equals(that.getEducationData()) : that.getEducationData() != null) {
        return false;
      }
      if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
        return false;
      }
      if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getEducationData() != null ? getEducationData().hashCode() : 0);
      result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionMyProfileFragmentToAddAcademicEducationFragment(actionId=" + getActionId() + "){"
          + "educationData=" + getEducationData()
          + ", title=" + getTitle()
          + "}";
    }
  }

  public static class ActionMyProfileFragmentToAddTrainingInfoFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionMyProfileFragmentToAddTrainingInfoFragment(@Nullable TrainingInfo trainingInfo,
        @Nullable String title) {
      this.arguments.put("training_info", trainingInfo);
      this.arguments.put("title", title);
    }

    @NonNull
    public ActionMyProfileFragmentToAddTrainingInfoFragment setTrainingInfo(
        @Nullable TrainingInfo trainingInfo) {
      this.arguments.put("training_info", trainingInfo);
      return this;
    }

    @NonNull
    public ActionMyProfileFragmentToAddTrainingInfoFragment setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("training_info")) {
        TrainingInfo trainingInfo = (TrainingInfo) arguments.get("training_info");
        if (Parcelable.class.isAssignableFrom(TrainingInfo.class) || trainingInfo == null) {
          __result.putParcelable("training_info", Parcelable.class.cast(trainingInfo));
        } else if (Serializable.class.isAssignableFrom(TrainingInfo.class)) {
          __result.putSerializable("training_info", Serializable.class.cast(trainingInfo));
        } else {
          throw new UnsupportedOperationException(TrainingInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("title")) {
        String title = (String) arguments.get("title");
        __result.putString("title", title);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_myProfileFragment_to_addTrainingInfoFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public TrainingInfo getTrainingInfo() {
      return (TrainingInfo) arguments.get("training_info");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionMyProfileFragmentToAddTrainingInfoFragment that = (ActionMyProfileFragmentToAddTrainingInfoFragment) object;
      if (arguments.containsKey("training_info") != that.arguments.containsKey("training_info")) {
        return false;
      }
      if (getTrainingInfo() != null ? !getTrainingInfo().equals(that.getTrainingInfo()) : that.getTrainingInfo() != null) {
        return false;
      }
      if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
        return false;
      }
      if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getTrainingInfo() != null ? getTrainingInfo().hashCode() : 0);
      result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionMyProfileFragmentToAddTrainingInfoFragment(actionId=" + getActionId() + "){"
          + "trainingInfo=" + getTrainingInfo()
          + ", title=" + getTitle()
          + "}";
    }
  }

  public static class ActionMyProfileFragmentToAddProfessionalQualificationInfoFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionMyProfileFragmentToAddProfessionalQualificationInfoFragment(
        @Nullable CertificationInfo certificationData, @Nullable String title) {
      this.arguments.put("certificationData", certificationData);
      this.arguments.put("title", title);
    }

    @NonNull
    public ActionMyProfileFragmentToAddProfessionalQualificationInfoFragment setCertificationData(
        @Nullable CertificationInfo certificationData) {
      this.arguments.put("certificationData", certificationData);
      return this;
    }

    @NonNull
    public ActionMyProfileFragmentToAddProfessionalQualificationInfoFragment setTitle(
        @Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("certificationData")) {
        CertificationInfo certificationData = (CertificationInfo) arguments.get("certificationData");
        if (Parcelable.class.isAssignableFrom(CertificationInfo.class) || certificationData == null) {
          __result.putParcelable("certificationData", Parcelable.class.cast(certificationData));
        } else if (Serializable.class.isAssignableFrom(CertificationInfo.class)) {
          __result.putSerializable("certificationData", Serializable.class.cast(certificationData));
        } else {
          throw new UnsupportedOperationException(CertificationInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("title")) {
        String title = (String) arguments.get("title");
        __result.putString("title", title);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_myProfileFragment_to_addProfessionalQualificationInfoFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public CertificationInfo getCertificationData() {
      return (CertificationInfo) arguments.get("certificationData");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionMyProfileFragmentToAddProfessionalQualificationInfoFragment that = (ActionMyProfileFragmentToAddProfessionalQualificationInfoFragment) object;
      if (arguments.containsKey("certificationData") != that.arguments.containsKey("certificationData")) {
        return false;
      }
      if (getCertificationData() != null ? !getCertificationData().equals(that.getCertificationData()) : that.getCertificationData() != null) {
        return false;
      }
      if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
        return false;
      }
      if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getCertificationData() != null ? getCertificationData().hashCode() : 0);
      result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionMyProfileFragmentToAddProfessionalQualificationInfoFragment(actionId=" + getActionId() + "){"
          + "certificationData=" + getCertificationData()
          + ", title=" + getTitle()
          + "}";
    }
  }

  public static class ActionMyProfileFragmentToAddExperienceInfoFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionMyProfileFragmentToAddExperienceInfoFragment(
        @Nullable ExperienceInfo experienceData, @Nullable String title) {
      this.arguments.put("experienceData", experienceData);
      this.arguments.put("title", title);
    }

    @NonNull
    public ActionMyProfileFragmentToAddExperienceInfoFragment setExperienceData(
        @Nullable ExperienceInfo experienceData) {
      this.arguments.put("experienceData", experienceData);
      return this;
    }

    @NonNull
    public ActionMyProfileFragmentToAddExperienceInfoFragment setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("experienceData")) {
        ExperienceInfo experienceData = (ExperienceInfo) arguments.get("experienceData");
        if (Parcelable.class.isAssignableFrom(ExperienceInfo.class) || experienceData == null) {
          __result.putParcelable("experienceData", Parcelable.class.cast(experienceData));
        } else if (Serializable.class.isAssignableFrom(ExperienceInfo.class)) {
          __result.putSerializable("experienceData", Serializable.class.cast(experienceData));
        } else {
          throw new UnsupportedOperationException(ExperienceInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("title")) {
        String title = (String) arguments.get("title");
        __result.putString("title", title);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_myProfileFragment_to_addExperienceInfoFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public ExperienceInfo getExperienceData() {
      return (ExperienceInfo) arguments.get("experienceData");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionMyProfileFragmentToAddExperienceInfoFragment that = (ActionMyProfileFragmentToAddExperienceInfoFragment) object;
      if (arguments.containsKey("experienceData") != that.arguments.containsKey("experienceData")) {
        return false;
      }
      if (getExperienceData() != null ? !getExperienceData().equals(that.getExperienceData()) : that.getExperienceData() != null) {
        return false;
      }
      if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
        return false;
      }
      if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getExperienceData() != null ? getExperienceData().hashCode() : 0);
      result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionMyProfileFragmentToAddExperienceInfoFragment(actionId=" + getActionId() + "){"
          + "experienceData=" + getExperienceData()
          + ", title=" + getTitle()
          + "}";
    }
  }

  public static class ActionMyProfileFragmentToAddReferenceInfoFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionMyProfileFragmentToAddReferenceInfoFragment(@Nullable ReferenceInfo referenceData,
        @Nullable String title) {
      this.arguments.put("referenceData", referenceData);
      this.arguments.put("title", title);
    }

    @NonNull
    public ActionMyProfileFragmentToAddReferenceInfoFragment setReferenceData(
        @Nullable ReferenceInfo referenceData) {
      this.arguments.put("referenceData", referenceData);
      return this;
    }

    @NonNull
    public ActionMyProfileFragmentToAddReferenceInfoFragment setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("referenceData")) {
        ReferenceInfo referenceData = (ReferenceInfo) arguments.get("referenceData");
        if (Parcelable.class.isAssignableFrom(ReferenceInfo.class) || referenceData == null) {
          __result.putParcelable("referenceData", Parcelable.class.cast(referenceData));
        } else if (Serializable.class.isAssignableFrom(ReferenceInfo.class)) {
          __result.putSerializable("referenceData", Serializable.class.cast(referenceData));
        } else {
          throw new UnsupportedOperationException(ReferenceInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("title")) {
        String title = (String) arguments.get("title");
        __result.putString("title", title);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_myProfileFragment_to_addReferenceInfoFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public ReferenceInfo getReferenceData() {
      return (ReferenceInfo) arguments.get("referenceData");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionMyProfileFragmentToAddReferenceInfoFragment that = (ActionMyProfileFragmentToAddReferenceInfoFragment) object;
      if (arguments.containsKey("referenceData") != that.arguments.containsKey("referenceData")) {
        return false;
      }
      if (getReferenceData() != null ? !getReferenceData().equals(that.getReferenceData()) : that.getReferenceData() != null) {
        return false;
      }
      if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
        return false;
      }
      if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getReferenceData() != null ? getReferenceData().hashCode() : 0);
      result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionMyProfileFragmentToAddReferenceInfoFragment(actionId=" + getActionId() + "){"
          + "referenceData=" + getReferenceData()
          + ", title=" + getTitle()
          + "}";
    }
  }

  public static class ActionMyProfileFragmentToUpdatePersonalInfoFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionMyProfileFragmentToUpdatePersonalInfoFragment(
        @Nullable ProfileInformation personalData, @Nullable String title) {
      this.arguments.put("personalData", personalData);
      this.arguments.put("title", title);
    }

    @NonNull
    public ActionMyProfileFragmentToUpdatePersonalInfoFragment setPersonalData(
        @Nullable ProfileInformation personalData) {
      this.arguments.put("personalData", personalData);
      return this;
    }

    @NonNull
    public ActionMyProfileFragmentToUpdatePersonalInfoFragment setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("personalData")) {
        ProfileInformation personalData = (ProfileInformation) arguments.get("personalData");
        if (Parcelable.class.isAssignableFrom(ProfileInformation.class) || personalData == null) {
          __result.putParcelable("personalData", Parcelable.class.cast(personalData));
        } else if (Serializable.class.isAssignableFrom(ProfileInformation.class)) {
          __result.putSerializable("personalData", Serializable.class.cast(personalData));
        } else {
          throw new UnsupportedOperationException(ProfileInformation.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("title")) {
        String title = (String) arguments.get("title");
        __result.putString("title", title);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_myProfileFragment_to_updatePersonalInfoFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public ProfileInformation getPersonalData() {
      return (ProfileInformation) arguments.get("personalData");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionMyProfileFragmentToUpdatePersonalInfoFragment that = (ActionMyProfileFragmentToUpdatePersonalInfoFragment) object;
      if (arguments.containsKey("personalData") != that.arguments.containsKey("personalData")) {
        return false;
      }
      if (getPersonalData() != null ? !getPersonalData().equals(that.getPersonalData()) : that.getPersonalData() != null) {
        return false;
      }
      if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
        return false;
      }
      if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getPersonalData() != null ? getPersonalData().hashCode() : 0);
      result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionMyProfileFragmentToUpdatePersonalInfoFragment(actionId=" + getActionId() + "){"
          + "personalData=" + getPersonalData()
          + ", title=" + getTitle()
          + "}";
    }
  }

  public static class ActionMyProfileFragmentToUpdateAddressInfoFragment implements NavDirections {
    private final HashMap arguments = new HashMap();

    private ActionMyProfileFragmentToUpdateAddressInfoFragment(
        @Nullable ProfileInformation addressData, @Nullable String title) {
      this.arguments.put("addressData", addressData);
      this.arguments.put("title", title);
    }

    @NonNull
    public ActionMyProfileFragmentToUpdateAddressInfoFragment setAddressData(
        @Nullable ProfileInformation addressData) {
      this.arguments.put("addressData", addressData);
      return this;
    }

    @NonNull
    public ActionMyProfileFragmentToUpdateAddressInfoFragment setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    @NonNull
    public Bundle getArguments() {
      Bundle __result = new Bundle();
      if (arguments.containsKey("addressData")) {
        ProfileInformation addressData = (ProfileInformation) arguments.get("addressData");
        if (Parcelable.class.isAssignableFrom(ProfileInformation.class) || addressData == null) {
          __result.putParcelable("addressData", Parcelable.class.cast(addressData));
        } else if (Serializable.class.isAssignableFrom(ProfileInformation.class)) {
          __result.putSerializable("addressData", Serializable.class.cast(addressData));
        } else {
          throw new UnsupportedOperationException(ProfileInformation.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
      }
      if (arguments.containsKey("title")) {
        String title = (String) arguments.get("title");
        __result.putString("title", title);
      }
      return __result;
    }

    @Override
    public int getActionId() {
      return R.id.action_myProfileFragment_to_updateAddressInfoFragment;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public ProfileInformation getAddressData() {
      return (ProfileInformation) arguments.get("addressData");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }

    @Override
    public boolean equals(Object object) {
      if (this == object) {
          return true;
      }
      if (object == null || getClass() != object.getClass()) {
          return false;
      }
      ActionMyProfileFragmentToUpdateAddressInfoFragment that = (ActionMyProfileFragmentToUpdateAddressInfoFragment) object;
      if (arguments.containsKey("addressData") != that.arguments.containsKey("addressData")) {
        return false;
      }
      if (getAddressData() != null ? !getAddressData().equals(that.getAddressData()) : that.getAddressData() != null) {
        return false;
      }
      if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
        return false;
      }
      if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
        return false;
      }
      if (getActionId() != that.getActionId()) {
        return false;
      }
      return true;
    }

    @Override
    public int hashCode() {
      int result = 1;
      result = 31 * result + (getAddressData() != null ? getAddressData().hashCode() : 0);
      result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
      result = 31 * result + getActionId();
      return result;
    }

    @Override
    public String toString() {
      return "ActionMyProfileFragmentToUpdateAddressInfoFragment(actionId=" + getActionId() + "){"
          + "addressData=" + getAddressData()
          + ", title=" + getTitle()
          + "}";
    }
  }
}
