package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import com.netizen.netiworld.model.EducationInfo;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class AddAcademicEducationFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private AddAcademicEducationFragmentArgs() {
  }

  private AddAcademicEducationFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static AddAcademicEducationFragmentArgs fromBundle(@NonNull Bundle bundle) {
    AddAcademicEducationFragmentArgs __result = new AddAcademicEducationFragmentArgs();
    bundle.setClassLoader(AddAcademicEducationFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("educationData")) {
      EducationInfo educationData;
      if (Parcelable.class.isAssignableFrom(EducationInfo.class) || Serializable.class.isAssignableFrom(EducationInfo.class)) {
        educationData = (EducationInfo) bundle.get("educationData");
      } else {
        throw new UnsupportedOperationException(EducationInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("educationData", educationData);
    } else {
      throw new IllegalArgumentException("Required argument \"educationData\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("title")) {
      String title;
      title = bundle.getString("title");
      __result.arguments.put("title", title);
    } else {
      throw new IllegalArgumentException("Required argument \"title\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public EducationInfo getEducationData() {
    return (EducationInfo) arguments.get("educationData");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getTitle() {
    return (String) arguments.get("title");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("educationData")) {
      EducationInfo educationData = (EducationInfo) arguments.get("educationData");
      if (Parcelable.class.isAssignableFrom(EducationInfo.class) || educationData == null) {
        __result.putParcelable("educationData", Parcelable.class.cast(educationData));
      } else if (Serializable.class.isAssignableFrom(EducationInfo.class)) {
        __result.putSerializable("educationData", Serializable.class.cast(educationData));
      } else {
        throw new UnsupportedOperationException(EducationInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("title")) {
      String title = (String) arguments.get("title");
      __result.putString("title", title);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    AddAcademicEducationFragmentArgs that = (AddAcademicEducationFragmentArgs) object;
    if (arguments.containsKey("educationData") != that.arguments.containsKey("educationData")) {
      return false;
    }
    if (getEducationData() != null ? !getEducationData().equals(that.getEducationData()) : that.getEducationData() != null) {
      return false;
    }
    if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
      return false;
    }
    if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getEducationData() != null ? getEducationData().hashCode() : 0);
    result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "AddAcademicEducationFragmentArgs{"
        + "educationData=" + getEducationData()
        + ", title=" + getTitle()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(AddAcademicEducationFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@Nullable EducationInfo educationData, @Nullable String title) {
      this.arguments.put("educationData", educationData);
      this.arguments.put("title", title);
    }

    @NonNull
    public AddAcademicEducationFragmentArgs build() {
      AddAcademicEducationFragmentArgs result = new AddAcademicEducationFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setEducationData(@Nullable EducationInfo educationData) {
      this.arguments.put("educationData", educationData);
      return this;
    }

    @NonNull
    public Builder setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public EducationInfo getEducationData() {
      return (EducationInfo) arguments.get("educationData");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }
  }
}
