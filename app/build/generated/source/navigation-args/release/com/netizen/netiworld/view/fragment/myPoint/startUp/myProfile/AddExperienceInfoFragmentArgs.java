package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import com.netizen.netiworld.model.ExperienceInfo;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class AddExperienceInfoFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private AddExperienceInfoFragmentArgs() {
  }

  private AddExperienceInfoFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static AddExperienceInfoFragmentArgs fromBundle(@NonNull Bundle bundle) {
    AddExperienceInfoFragmentArgs __result = new AddExperienceInfoFragmentArgs();
    bundle.setClassLoader(AddExperienceInfoFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("experienceData")) {
      ExperienceInfo experienceData;
      if (Parcelable.class.isAssignableFrom(ExperienceInfo.class) || Serializable.class.isAssignableFrom(ExperienceInfo.class)) {
        experienceData = (ExperienceInfo) bundle.get("experienceData");
      } else {
        throw new UnsupportedOperationException(ExperienceInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("experienceData", experienceData);
    } else {
      throw new IllegalArgumentException("Required argument \"experienceData\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("title")) {
      String title;
      title = bundle.getString("title");
      __result.arguments.put("title", title);
    } else {
      throw new IllegalArgumentException("Required argument \"title\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public ExperienceInfo getExperienceData() {
    return (ExperienceInfo) arguments.get("experienceData");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getTitle() {
    return (String) arguments.get("title");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("experienceData")) {
      ExperienceInfo experienceData = (ExperienceInfo) arguments.get("experienceData");
      if (Parcelable.class.isAssignableFrom(ExperienceInfo.class) || experienceData == null) {
        __result.putParcelable("experienceData", Parcelable.class.cast(experienceData));
      } else if (Serializable.class.isAssignableFrom(ExperienceInfo.class)) {
        __result.putSerializable("experienceData", Serializable.class.cast(experienceData));
      } else {
        throw new UnsupportedOperationException(ExperienceInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("title")) {
      String title = (String) arguments.get("title");
      __result.putString("title", title);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    AddExperienceInfoFragmentArgs that = (AddExperienceInfoFragmentArgs) object;
    if (arguments.containsKey("experienceData") != that.arguments.containsKey("experienceData")) {
      return false;
    }
    if (getExperienceData() != null ? !getExperienceData().equals(that.getExperienceData()) : that.getExperienceData() != null) {
      return false;
    }
    if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
      return false;
    }
    if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getExperienceData() != null ? getExperienceData().hashCode() : 0);
    result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "AddExperienceInfoFragmentArgs{"
        + "experienceData=" + getExperienceData()
        + ", title=" + getTitle()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(AddExperienceInfoFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@Nullable ExperienceInfo experienceData, @Nullable String title) {
      this.arguments.put("experienceData", experienceData);
      this.arguments.put("title", title);
    }

    @NonNull
    public AddExperienceInfoFragmentArgs build() {
      AddExperienceInfoFragmentArgs result = new AddExperienceInfoFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setExperienceData(@Nullable ExperienceInfo experienceData) {
      this.arguments.put("experienceData", experienceData);
      return this;
    }

    @NonNull
    public Builder setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public ExperienceInfo getExperienceData() {
      return (ExperienceInfo) arguments.get("experienceData");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }
  }
}
