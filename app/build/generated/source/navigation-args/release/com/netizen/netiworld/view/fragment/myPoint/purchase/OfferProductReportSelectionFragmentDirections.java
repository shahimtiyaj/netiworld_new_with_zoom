package com.netizen.netiworld.view.fragment.myPoint.purchase;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class OfferProductReportSelectionFragmentDirections {
  private OfferProductReportSelectionFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionOfferProductReportSelectionFragmentToOfferProductReportFragment(
      ) {
    return new ActionOnlyNavDirections(R.id.action_offerProductReportSelectionFragment_to_offerProductReportFragment);
  }
}
