package com.netizen.netiworld.view.fragment.myPoint.balance.wallet;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class BalanceStatementReportSelectionFragmentDirections {
  private BalanceStatementReportSelectionFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionStatementReportSelectionFragmentToBalanceStatementReportFragment(
      ) {
    return new ActionOnlyNavDirections(R.id.action_statementReportSelectionFragment_to_balanceStatementReportFragment);
  }
}
