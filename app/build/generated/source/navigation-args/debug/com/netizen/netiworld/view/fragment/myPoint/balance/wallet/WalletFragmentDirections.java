package com.netizen.netiworld.view.fragment.myPoint.balance.wallet;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class WalletFragmentDirections {
  private WalletFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionWalletFragmentToFragmentSetting() {
    return new ActionOnlyNavDirections(R.id.action_walletFragment_to_fragmentSetting);
  }
}
