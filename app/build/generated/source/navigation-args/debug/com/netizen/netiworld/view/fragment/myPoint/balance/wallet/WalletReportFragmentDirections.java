package com.netizen.netiworld.view.fragment.myPoint.balance.wallet;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class WalletReportFragmentDirections {
  private WalletReportFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionWalletReportFragmentToDepositReportFragment() {
    return new ActionOnlyNavDirections(R.id.action_walletReportFragment_to_depositReportFragment);
  }

  @NonNull
  public static NavDirections actionWalletReportFragmentToTransferReportFragment() {
    return new ActionOnlyNavDirections(R.id.action_walletReportFragment_to_transferReportFragment);
  }

  @NonNull
  public static NavDirections actionWalletReportFragmentToWithdrawReportFragment() {
    return new ActionOnlyNavDirections(R.id.action_walletReportFragment_to_withdrawReportFragment);
  }
}
