package com.netizen.netiworld.view.fragment.userPoint;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class ExamRoutineFragmentDirections {
  private ExamRoutineFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionExamRoutineFragmentToClassRoutineFragment() {
    return new ActionOnlyNavDirections(R.id.action_examRoutineFragment_to_classRoutineFragment);
  }
}
