package com.netizen.netiworld.view.fragment;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class FragmentSettingDirections {
  private FragmentSettingDirections() {
  }

  @NonNull
  public static NavDirections actionFragmentSettingToFragmentProfile() {
    return new ActionOnlyNavDirections(R.id.action_fragmentSetting_to_fragmentProfile);
  }

  @NonNull
  public static NavDirections actionFragmentSettingToSubmitTokenFragment() {
    return new ActionOnlyNavDirections(R.id.action_fragmentSetting_to_submitTokenFragment);
  }

  @NonNull
  public static NavDirections actionFragmentSettingToFragmentChangePassward() {
    return new ActionOnlyNavDirections(R.id.action_fragmentSetting_to_fragmentChangePassward);
  }
}
