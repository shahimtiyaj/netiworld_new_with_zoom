package com.netizen.netiworld.view.fragment.myPoint.balance.message;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class FragmentMessageRechargeDirections {
  private FragmentMessageRechargeDirections() {
  }

  @NonNull
  public static NavDirections actionFragmentMessageRechargeToFragmentSetting() {
    return new ActionOnlyNavDirections(R.id.action_fragmentMessageRecharge_to_fragmentSetting);
  }
}
