package com.netizen.netiworld.view.fragment.myPoint.purchase;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class FragmentOfferProductDirections {
  private FragmentOfferProductDirections() {
  }

  @NonNull
  public static NavDirections actionFragmentOfferProductToFragmentSetting() {
    return new ActionOnlyNavDirections(R.id.action_fragmentOfferProduct_to_fragmentSetting);
  }
}
