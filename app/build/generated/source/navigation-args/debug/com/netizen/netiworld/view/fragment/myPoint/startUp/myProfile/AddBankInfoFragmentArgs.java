package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import com.netizen.netiworld.model.TagGetData;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class AddBankInfoFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private AddBankInfoFragmentArgs() {
  }

  private AddBankInfoFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static AddBankInfoFragmentArgs fromBundle(@NonNull Bundle bundle) {
    AddBankInfoFragmentArgs __result = new AddBankInfoFragmentArgs();
    bundle.setClassLoader(AddBankInfoFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("tagData")) {
      TagGetData tagData;
      if (Parcelable.class.isAssignableFrom(TagGetData.class) || Serializable.class.isAssignableFrom(TagGetData.class)) {
        tagData = (TagGetData) bundle.get("tagData");
      } else {
        throw new UnsupportedOperationException(TagGetData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("tagData", tagData);
    } else {
      throw new IllegalArgumentException("Required argument \"tagData\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("title")) {
      String title;
      title = bundle.getString("title");
      __result.arguments.put("title", title);
    } else {
      throw new IllegalArgumentException("Required argument \"title\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public TagGetData getTagData() {
    return (TagGetData) arguments.get("tagData");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getTitle() {
    return (String) arguments.get("title");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("tagData")) {
      TagGetData tagData = (TagGetData) arguments.get("tagData");
      if (Parcelable.class.isAssignableFrom(TagGetData.class) || tagData == null) {
        __result.putParcelable("tagData", Parcelable.class.cast(tagData));
      } else if (Serializable.class.isAssignableFrom(TagGetData.class)) {
        __result.putSerializable("tagData", Serializable.class.cast(tagData));
      } else {
        throw new UnsupportedOperationException(TagGetData.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("title")) {
      String title = (String) arguments.get("title");
      __result.putString("title", title);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    AddBankInfoFragmentArgs that = (AddBankInfoFragmentArgs) object;
    if (arguments.containsKey("tagData") != that.arguments.containsKey("tagData")) {
      return false;
    }
    if (getTagData() != null ? !getTagData().equals(that.getTagData()) : that.getTagData() != null) {
      return false;
    }
    if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
      return false;
    }
    if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getTagData() != null ? getTagData().hashCode() : 0);
    result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "AddBankInfoFragmentArgs{"
        + "tagData=" + getTagData()
        + ", title=" + getTitle()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(AddBankInfoFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@Nullable TagGetData tagData, @Nullable String title) {
      this.arguments.put("tagData", tagData);
      this.arguments.put("title", title);
    }

    @NonNull
    public AddBankInfoFragmentArgs build() {
      AddBankInfoFragmentArgs result = new AddBankInfoFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setTagData(@Nullable TagGetData tagData) {
      this.arguments.put("tagData", tagData);
      return this;
    }

    @NonNull
    public Builder setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public TagGetData getTagData() {
      return (TagGetData) arguments.get("tagData");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }
  }
}
