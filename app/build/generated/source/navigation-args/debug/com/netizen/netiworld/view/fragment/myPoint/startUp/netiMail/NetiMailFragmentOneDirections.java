package com.netizen.netiworld.view.fragment.myPoint.startUp.netiMail;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class NetiMailFragmentOneDirections {
  private NetiMailFragmentOneDirections() {
  }

  @NonNull
  public static NavDirections actionNetiMailFragmentOneToNetiMailFragmentTwo() {
    return new ActionOnlyNavDirections(R.id.action_netiMailFragmentOne_to_netiMailFragmentTwo);
  }
}
