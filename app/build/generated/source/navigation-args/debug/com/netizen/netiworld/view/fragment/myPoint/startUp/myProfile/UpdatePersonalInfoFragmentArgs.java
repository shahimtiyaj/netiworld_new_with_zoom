package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import com.netizen.netiworld.model.ProfileInformation;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class UpdatePersonalInfoFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private UpdatePersonalInfoFragmentArgs() {
  }

  private UpdatePersonalInfoFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static UpdatePersonalInfoFragmentArgs fromBundle(@NonNull Bundle bundle) {
    UpdatePersonalInfoFragmentArgs __result = new UpdatePersonalInfoFragmentArgs();
    bundle.setClassLoader(UpdatePersonalInfoFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("personalData")) {
      ProfileInformation personalData;
      if (Parcelable.class.isAssignableFrom(ProfileInformation.class) || Serializable.class.isAssignableFrom(ProfileInformation.class)) {
        personalData = (ProfileInformation) bundle.get("personalData");
      } else {
        throw new UnsupportedOperationException(ProfileInformation.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("personalData", personalData);
    } else {
      throw new IllegalArgumentException("Required argument \"personalData\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("title")) {
      String title;
      title = bundle.getString("title");
      __result.arguments.put("title", title);
    } else {
      throw new IllegalArgumentException("Required argument \"title\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public ProfileInformation getPersonalData() {
    return (ProfileInformation) arguments.get("personalData");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getTitle() {
    return (String) arguments.get("title");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("personalData")) {
      ProfileInformation personalData = (ProfileInformation) arguments.get("personalData");
      if (Parcelable.class.isAssignableFrom(ProfileInformation.class) || personalData == null) {
        __result.putParcelable("personalData", Parcelable.class.cast(personalData));
      } else if (Serializable.class.isAssignableFrom(ProfileInformation.class)) {
        __result.putSerializable("personalData", Serializable.class.cast(personalData));
      } else {
        throw new UnsupportedOperationException(ProfileInformation.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("title")) {
      String title = (String) arguments.get("title");
      __result.putString("title", title);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    UpdatePersonalInfoFragmentArgs that = (UpdatePersonalInfoFragmentArgs) object;
    if (arguments.containsKey("personalData") != that.arguments.containsKey("personalData")) {
      return false;
    }
    if (getPersonalData() != null ? !getPersonalData().equals(that.getPersonalData()) : that.getPersonalData() != null) {
      return false;
    }
    if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
      return false;
    }
    if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getPersonalData() != null ? getPersonalData().hashCode() : 0);
    result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "UpdatePersonalInfoFragmentArgs{"
        + "personalData=" + getPersonalData()
        + ", title=" + getTitle()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(UpdatePersonalInfoFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@Nullable ProfileInformation personalData, @Nullable String title) {
      this.arguments.put("personalData", personalData);
      this.arguments.put("title", title);
    }

    @NonNull
    public UpdatePersonalInfoFragmentArgs build() {
      UpdatePersonalInfoFragmentArgs result = new UpdatePersonalInfoFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setPersonalData(@Nullable ProfileInformation personalData) {
      this.arguments.put("personalData", personalData);
      return this;
    }

    @NonNull
    public Builder setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public ProfileInformation getPersonalData() {
      return (ProfileInformation) arguments.get("personalData");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }
  }
}
