package com.netizen.netiworld.view.fragment.myPoint.balance.wallet;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class RevenueReportSelectionFragmentDirections {
  private RevenueReportSelectionFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionRevenueReportSelectionFragmentToRevenueLogReportFragment() {
    return new ActionOnlyNavDirections(R.id.action_revenueReportSelectionFragment_to_revenueLogReportFragment);
  }
}
