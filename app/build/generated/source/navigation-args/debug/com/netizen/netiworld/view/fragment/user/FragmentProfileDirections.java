package com.netizen.netiworld.view.fragment.user;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class FragmentProfileDirections {
  private FragmentProfileDirections() {
  }

  @NonNull
  public static NavDirections actionFragmentProfileToFragmentSetting() {
    return new ActionOnlyNavDirections(R.id.action_fragmentProfile_to_fragmentSetting);
  }
}
