package com.netizen.netiworld.view.fragment.myPoint.bankAccount;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class BankAccountFragmentDirections {
  private BankAccountFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionBankFragmentToBankListFragment() {
    return new ActionOnlyNavDirections(R.id.action_bankFragment_to_bankListFragment);
  }
}
