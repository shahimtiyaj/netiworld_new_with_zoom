package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import com.netizen.netiworld.model.ProfileInformation;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class UpdateAddressInfoFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private UpdateAddressInfoFragmentArgs() {
  }

  private UpdateAddressInfoFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static UpdateAddressInfoFragmentArgs fromBundle(@NonNull Bundle bundle) {
    UpdateAddressInfoFragmentArgs __result = new UpdateAddressInfoFragmentArgs();
    bundle.setClassLoader(UpdateAddressInfoFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("addressData")) {
      ProfileInformation addressData;
      if (Parcelable.class.isAssignableFrom(ProfileInformation.class) || Serializable.class.isAssignableFrom(ProfileInformation.class)) {
        addressData = (ProfileInformation) bundle.get("addressData");
      } else {
        throw new UnsupportedOperationException(ProfileInformation.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("addressData", addressData);
    } else {
      throw new IllegalArgumentException("Required argument \"addressData\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("title")) {
      String title;
      title = bundle.getString("title");
      __result.arguments.put("title", title);
    } else {
      throw new IllegalArgumentException("Required argument \"title\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public ProfileInformation getAddressData() {
    return (ProfileInformation) arguments.get("addressData");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getTitle() {
    return (String) arguments.get("title");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("addressData")) {
      ProfileInformation addressData = (ProfileInformation) arguments.get("addressData");
      if (Parcelable.class.isAssignableFrom(ProfileInformation.class) || addressData == null) {
        __result.putParcelable("addressData", Parcelable.class.cast(addressData));
      } else if (Serializable.class.isAssignableFrom(ProfileInformation.class)) {
        __result.putSerializable("addressData", Serializable.class.cast(addressData));
      } else {
        throw new UnsupportedOperationException(ProfileInformation.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("title")) {
      String title = (String) arguments.get("title");
      __result.putString("title", title);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    UpdateAddressInfoFragmentArgs that = (UpdateAddressInfoFragmentArgs) object;
    if (arguments.containsKey("addressData") != that.arguments.containsKey("addressData")) {
      return false;
    }
    if (getAddressData() != null ? !getAddressData().equals(that.getAddressData()) : that.getAddressData() != null) {
      return false;
    }
    if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
      return false;
    }
    if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getAddressData() != null ? getAddressData().hashCode() : 0);
    result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "UpdateAddressInfoFragmentArgs{"
        + "addressData=" + getAddressData()
        + ", title=" + getTitle()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(UpdateAddressInfoFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@Nullable ProfileInformation addressData, @Nullable String title) {
      this.arguments.put("addressData", addressData);
      this.arguments.put("title", title);
    }

    @NonNull
    public UpdateAddressInfoFragmentArgs build() {
      UpdateAddressInfoFragmentArgs result = new UpdateAddressInfoFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setAddressData(@Nullable ProfileInformation addressData) {
      this.arguments.put("addressData", addressData);
      return this;
    }

    @NonNull
    public Builder setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public ProfileInformation getAddressData() {
      return (ProfileInformation) arguments.get("addressData");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }
  }
}
