package com.netizen.netiworld.view.fragment;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class HomeFragmentDirections {
  private HomeFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionHomeFragmentToFragmentSetting() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_fragmentSetting);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToWalletFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_walletFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToFragmentMessageRecharge() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_fragmentMessageRecharge);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToPurchaseGeneralProductFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_purchaseGeneralProductFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToFragmentOfferProduct() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_fragmentOfferProduct);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToRevenueReportSelectionFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_revenueReportSelectionFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToGeneralProductReportSelectionFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_generalProductReportSelectionFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToStatementReportSelectionFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_statementReportSelectionFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToMessageReportSelectionFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_messageReportSelectionFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToTabUsedUnusedCodeFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_tabUsedUnusedCodeFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToOfferProductReportSelectionFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_offerProductReportSelectionFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToWalletReportFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_walletReportFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToBankFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_bankFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToSignInFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_signInFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToInitAuthSDKActivity() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_initAuthSDKActivity);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToMyProfileFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_myProfileFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToNetiMailFragmentOne() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_netiMailFragmentOne);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToSubmitTokenFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_submitTokenFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToAddPointFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_addPointFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToStudentProfileFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_studentProfileFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToStudentAttendanceFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_studentAttendanceFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToSubjectListFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_subjectListFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToClassTestFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_classTestFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToInventoryInfoFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_inventoryInfoFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToClassRoutineFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_classRoutineFragment);
  }

  @NonNull
  public static NavDirections actionHomeFragmentToFeesInfoFragment() {
    return new ActionOnlyNavDirections(R.id.action_homeFragment_to_feesInfoFragment);
  }
}
