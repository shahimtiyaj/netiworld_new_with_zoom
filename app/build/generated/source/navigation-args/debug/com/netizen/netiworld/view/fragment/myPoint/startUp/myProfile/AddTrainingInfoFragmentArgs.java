package com.netizen.netiworld.view.fragment.myPoint.startUp.myProfile;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavArgs;
import com.netizen.netiworld.model.TrainingInfo;
import java.io.Serializable;
import java.lang.IllegalArgumentException;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;

public class AddTrainingInfoFragmentArgs implements NavArgs {
  private final HashMap arguments = new HashMap();

  private AddTrainingInfoFragmentArgs() {
  }

  private AddTrainingInfoFragmentArgs(HashMap argumentsMap) {
    this.arguments.putAll(argumentsMap);
  }

  @NonNull
  @SuppressWarnings("unchecked")
  public static AddTrainingInfoFragmentArgs fromBundle(@NonNull Bundle bundle) {
    AddTrainingInfoFragmentArgs __result = new AddTrainingInfoFragmentArgs();
    bundle.setClassLoader(AddTrainingInfoFragmentArgs.class.getClassLoader());
    if (bundle.containsKey("training_info")) {
      TrainingInfo trainingInfo;
      if (Parcelable.class.isAssignableFrom(TrainingInfo.class) || Serializable.class.isAssignableFrom(TrainingInfo.class)) {
        trainingInfo = (TrainingInfo) bundle.get("training_info");
      } else {
        throw new UnsupportedOperationException(TrainingInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
      __result.arguments.put("training_info", trainingInfo);
    } else {
      throw new IllegalArgumentException("Required argument \"training_info\" is missing and does not have an android:defaultValue");
    }
    if (bundle.containsKey("title")) {
      String title;
      title = bundle.getString("title");
      __result.arguments.put("title", title);
    } else {
      throw new IllegalArgumentException("Required argument \"title\" is missing and does not have an android:defaultValue");
    }
    return __result;
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public TrainingInfo getTrainingInfo() {
    return (TrainingInfo) arguments.get("training_info");
  }

  @SuppressWarnings("unchecked")
  @Nullable
  public String getTitle() {
    return (String) arguments.get("title");
  }

  @SuppressWarnings("unchecked")
  @NonNull
  public Bundle toBundle() {
    Bundle __result = new Bundle();
    if (arguments.containsKey("training_info")) {
      TrainingInfo trainingInfo = (TrainingInfo) arguments.get("training_info");
      if (Parcelable.class.isAssignableFrom(TrainingInfo.class) || trainingInfo == null) {
        __result.putParcelable("training_info", Parcelable.class.cast(trainingInfo));
      } else if (Serializable.class.isAssignableFrom(TrainingInfo.class)) {
        __result.putSerializable("training_info", Serializable.class.cast(trainingInfo));
      } else {
        throw new UnsupportedOperationException(TrainingInfo.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
      }
    }
    if (arguments.containsKey("title")) {
      String title = (String) arguments.get("title");
      __result.putString("title", title);
    }
    return __result;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
        return true;
    }
    if (object == null || getClass() != object.getClass()) {
        return false;
    }
    AddTrainingInfoFragmentArgs that = (AddTrainingInfoFragmentArgs) object;
    if (arguments.containsKey("training_info") != that.arguments.containsKey("training_info")) {
      return false;
    }
    if (getTrainingInfo() != null ? !getTrainingInfo().equals(that.getTrainingInfo()) : that.getTrainingInfo() != null) {
      return false;
    }
    if (arguments.containsKey("title") != that.arguments.containsKey("title")) {
      return false;
    }
    if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    int result = 1;
    result = 31 * result + (getTrainingInfo() != null ? getTrainingInfo().hashCode() : 0);
    result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "AddTrainingInfoFragmentArgs{"
        + "trainingInfo=" + getTrainingInfo()
        + ", title=" + getTitle()
        + "}";
  }

  public static class Builder {
    private final HashMap arguments = new HashMap();

    public Builder(AddTrainingInfoFragmentArgs original) {
      this.arguments.putAll(original.arguments);
    }

    public Builder(@Nullable TrainingInfo trainingInfo, @Nullable String title) {
      this.arguments.put("training_info", trainingInfo);
      this.arguments.put("title", title);
    }

    @NonNull
    public AddTrainingInfoFragmentArgs build() {
      AddTrainingInfoFragmentArgs result = new AddTrainingInfoFragmentArgs(arguments);
      return result;
    }

    @NonNull
    public Builder setTrainingInfo(@Nullable TrainingInfo trainingInfo) {
      this.arguments.put("training_info", trainingInfo);
      return this;
    }

    @NonNull
    public Builder setTitle(@Nullable String title) {
      this.arguments.put("title", title);
      return this;
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public TrainingInfo getTrainingInfo() {
      return (TrainingInfo) arguments.get("training_info");
    }

    @SuppressWarnings("unchecked")
    @Nullable
    public String getTitle() {
      return (String) arguments.get("title");
    }
  }
}
