package com.netizen.netiworld.view.fragment.userPoint;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import com.netizen.netiworld.R;

public class ClassRoutineFragmentDirections {
  private ClassRoutineFragmentDirections() {
  }

  @NonNull
  public static NavDirections actionClassRoutineFragmentToExamRoutineFragment() {
    return new ActionOnlyNavDirections(R.id.action_classRoutineFragment_to_examRoutineFragment);
  }
}
