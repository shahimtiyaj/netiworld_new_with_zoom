package com.netizen.netiworld.databinding;
import com.netizen.netiworld.R;
import com.netizen.netiworld.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentUpdatePersonalInfoBindingImpl extends FragmentUpdatePersonalInfoBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.fragment_container, 1);
        sViewsWithIds.put(R.id.edit_text_father_name, 2);
        sViewsWithIds.put(R.id.edit_text_mother_name, 3);
        sViewsWithIds.put(R.id.edit_text_phone_no, 4);
        sViewsWithIds.put(R.id.edit_text_date_of_birth, 5);
        sViewsWithIds.put(R.id.rdGroup, 6);
        sViewsWithIds.put(R.id.rdbMale, 7);
        sViewsWithIds.put(R.id.rdbFemale, 8);
        sViewsWithIds.put(R.id.rdbOthers, 9);
        sViewsWithIds.put(R.id.layout_blood_group, 10);
        sViewsWithIds.put(R.id.edit_text_blood_group, 11);
        sViewsWithIds.put(R.id.rdMaritalGroup, 12);
        sViewsWithIds.put(R.id.rdbMarried, 13);
        sViewsWithIds.put(R.id.rdbUmaried, 14);
        sViewsWithIds.put(R.id.layout_number_of_child, 15);
        sViewsWithIds.put(R.id.edit_text_number_of_child, 16);
        sViewsWithIds.put(R.id.layout_nationality, 17);
        sViewsWithIds.put(R.id.edit_text_nationality, 18);
        sViewsWithIds.put(R.id.layout_national_id, 19);
        sViewsWithIds.put(R.id.edit_text_national_id, 20);
        sViewsWithIds.put(R.id.layout_passport_no, 21);
        sViewsWithIds.put(R.id.edit_text_passport_no, 22);
        sViewsWithIds.put(R.id.layout_birth_certification_no, 23);
        sViewsWithIds.put(R.id.edit_text_birth_certification_no, 24);
        sViewsWithIds.put(R.id.button_save, 25);
        sViewsWithIds.put(R.id.lottie_progressbar, 26);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentUpdatePersonalInfoBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 27, sIncludes, sViewsWithIds));
    }
    private FragmentUpdatePersonalInfoBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.Button) bindings[25]
            , (com.google.android.material.textfield.TextInputEditText) bindings[24]
            , (com.google.android.material.textfield.TextInputEditText) bindings[11]
            , (com.google.android.material.textfield.TextInputEditText) bindings[5]
            , (com.google.android.material.textfield.TextInputEditText) bindings[2]
            , (com.google.android.material.textfield.TextInputEditText) bindings[3]
            , (com.google.android.material.textfield.TextInputEditText) bindings[20]
            , (com.google.android.material.textfield.TextInputEditText) bindings[18]
            , (com.google.android.material.textfield.TextInputEditText) bindings[16]
            , (com.google.android.material.textfield.TextInputEditText) bindings[22]
            , (com.google.android.material.textfield.TextInputEditText) bindings[4]
            , (android.widget.RelativeLayout) bindings[1]
            , (com.google.android.material.textfield.TextInputLayout) bindings[23]
            , (com.google.android.material.textfield.TextInputLayout) bindings[10]
            , (com.google.android.material.textfield.TextInputLayout) bindings[19]
            , (com.google.android.material.textfield.TextInputLayout) bindings[17]
            , (com.google.android.material.textfield.TextInputLayout) bindings[15]
            , (com.google.android.material.textfield.TextInputLayout) bindings[21]
            , (android.widget.RelativeLayout) bindings[26]
            , (android.widget.RadioGroup) bindings[6]
            , (android.widget.RadioGroup) bindings[12]
            , (android.widget.RadioButton) bindings[8]
            , (android.widget.RadioButton) bindings[7]
            , (android.widget.RadioButton) bindings[13]
            , (android.widget.RadioButton) bindings[9]
            , (android.widget.RadioButton) bindings[14]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}