package com.netizen.netiworld.databinding;
import com.netizen.netiworld.R;
import com.netizen.netiworld.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentProfileLayoutBindingImpl extends FragmentProfileLayoutBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.fragment_container, 1);
        sViewsWithIds.put(R.id.relative, 2);
        sViewsWithIds.put(R.id.profile_pic_id, 3);
        sViewsWithIds.put(R.id.txt_profile_name, 4);
        sViewsWithIds.put(R.id.txt_neti_id, 5);
        sViewsWithIds.put(R.id.relative_personal_info, 6);
        sViewsWithIds.put(R.id.personal_info, 7);
        sViewsWithIds.put(R.id.relative1, 8);
        sViewsWithIds.put(R.id.layout_neti, 9);
        sViewsWithIds.put(R.id.neti_id, 10);
        sViewsWithIds.put(R.id.layout_mobile, 11);
        sViewsWithIds.put(R.id.mobile_no, 12);
        sViewsWithIds.put(R.id.layout_email, 13);
        sViewsWithIds.put(R.id.email_address, 14);
        sViewsWithIds.put(R.id.layout_gender, 15);
        sViewsWithIds.put(R.id.gender, 16);
        sViewsWithIds.put(R.id.layout_religion, 17);
        sViewsWithIds.put(R.id.religion, 18);
        sViewsWithIds.put(R.id.layout_dob, 19);
        sViewsWithIds.put(R.id.dateofbirth, 20);
        sViewsWithIds.put(R.id.layout_age, 21);
        sViewsWithIds.put(R.id.age, 22);
        sViewsWithIds.put(R.id.status, 23);
        sViewsWithIds.put(R.id.userActiveStatus, 24);
        sViewsWithIds.put(R.id.relative_address_info, 25);
        sViewsWithIds.put(R.id.address_info, 26);
        sViewsWithIds.put(R.id.relative2, 27);
        sViewsWithIds.put(R.id.layout_village, 28);
        sViewsWithIds.put(R.id.village, 29);
        sViewsWithIds.put(R.id.layout_upozilla, 30);
        sViewsWithIds.put(R.id.upozilla, 31);
        sViewsWithIds.put(R.id.layout_district, 32);
        sViewsWithIds.put(R.id.district, 33);
        sViewsWithIds.put(R.id.layout_division, 34);
        sViewsWithIds.put(R.id.division, 35);
        sViewsWithIds.put(R.id.relative_footer, 36);
        sViewsWithIds.put(R.id.profile_complete_ness, 37);
        sViewsWithIds.put(R.id.progress_bar_1, 38);
        sViewsWithIds.put(R.id.profile_txt, 39);
        sViewsWithIds.put(R.id.lottie_progressbar, 40);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentProfileLayoutBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 41, sIncludes, sViewsWithIds));
    }
    private FragmentProfileLayoutBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[26]
            , (android.widget.TextView) bindings[22]
            , (android.widget.TextView) bindings[20]
            , (android.widget.TextView) bindings[33]
            , (android.widget.TextView) bindings[35]
            , (android.widget.TextView) bindings[14]
            , (android.widget.RelativeLayout) bindings[1]
            , (android.widget.TextView) bindings[16]
            , (android.widget.LinearLayout) bindings[21]
            , (android.widget.LinearLayout) bindings[32]
            , (android.widget.LinearLayout) bindings[34]
            , (android.widget.LinearLayout) bindings[19]
            , (android.widget.LinearLayout) bindings[13]
            , (android.widget.LinearLayout) bindings[15]
            , (android.widget.LinearLayout) bindings[11]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.LinearLayout) bindings[17]
            , (android.widget.LinearLayout) bindings[30]
            , (android.widget.LinearLayout) bindings[28]
            , (android.widget.RelativeLayout) bindings[40]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[37]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[3]
            , (android.widget.TextView) bindings[39]
            , (com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar) bindings[38]
            , (android.widget.RelativeLayout) bindings[2]
            , (android.widget.RelativeLayout) bindings[8]
            , (android.widget.RelativeLayout) bindings[27]
            , (android.widget.RelativeLayout) bindings[25]
            , (android.widget.RelativeLayout) bindings[36]
            , (android.widget.RelativeLayout) bindings[6]
            , (android.widget.TextView) bindings[18]
            , (android.widget.TextView) bindings[23]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[31]
            , (android.widget.TextView) bindings[24]
            , (android.widget.TextView) bindings[29]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}