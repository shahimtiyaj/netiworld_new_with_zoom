package com.netizen.netiworld.databinding;
import com.netizen.netiworld.R;
import com.netizen.netiworld.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ReportsRecylerviewListBindingImpl extends ReportsRecylerviewListBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(15);
        sIncludes.setIncludes(1, 
            new String[] {"toolbar"},
            new int[] {2},
            new int[] {com.netizen.netiworld.R.layout.toolbar});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.relative1, 3);
        sViewsWithIds.put(R.id.spinner_remarks_1, 4);
        sViewsWithIds.put(R.id.linear0, 5);
        sViewsWithIds.put(R.id.l, 6);
        sViewsWithIds.put(R.id.hr_total_found_txt, 7);
        sViewsWithIds.put(R.id.total_deposit_balance_logs, 8);
        sViewsWithIds.put(R.id.view0, 9);
        sViewsWithIds.put(R.id.relative2, 10);
        sViewsWithIds.put(R.id.line1, 11);
        sViewsWithIds.put(R.id.view1, 12);
        sViewsWithIds.put(R.id.wallet_balance_message_log_list, 13);
        sViewsWithIds.put(R.id.progress_id, 14);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ReportsRecylerviewListBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private ReportsRecylerviewListBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (com.google.android.material.appbar.AppBarLayout) bindings[1]
            , (android.widget.TextView) bindings[7]
            , (android.widget.LinearLayout) bindings[6]
            , (android.widget.LinearLayout) bindings[11]
            , (android.widget.LinearLayout) bindings[5]
            , (android.widget.ProgressBar) bindings[14]
            , (android.widget.LinearLayout) bindings[3]
            , (android.widget.RelativeLayout) bindings[10]
            , (android.widget.LinearLayout) bindings[4]
            , (com.netizen.netiworld.databinding.ToolbarBinding) bindings[2]
            , (android.widget.TextView) bindings[8]
            , (android.view.View) bindings[9]
            , (android.view.View) bindings[12]
            , (androidx.recyclerview.widget.RecyclerView) bindings[13]
            );
        this.appbar.setTag(null);
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        toolbar.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (toolbar.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        toolbar.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeToolbar((com.netizen.netiworld.databinding.ToolbarBinding) object, fieldId);
        }
        return false;
    }
    private boolean onChangeToolbar(com.netizen.netiworld.databinding.ToolbarBinding Toolbar, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
        executeBindingsOn(toolbar);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): toolbar
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}