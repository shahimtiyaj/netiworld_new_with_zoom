package com.netizen.netiworld.databinding;
import com.netizen.netiworld.R;
import com.netizen.netiworld.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentDepositBindingImpl extends FragmentDepositBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.edit_text_account_number, 2);
        sViewsWithIds.put(R.id.edit_text_deposit_amount, 3);
        sViewsWithIds.put(R.id.edit_text_deposit_date, 4);
        sViewsWithIds.put(R.id.mobile_banking_input_layout, 5);
        sViewsWithIds.put(R.id.edit_text_mobile_banking_number, 6);
        sViewsWithIds.put(R.id.transaction_input_layout, 7);
        sViewsWithIds.put(R.id.edit_text_transaction_id, 8);
        sViewsWithIds.put(R.id.branch_input_layout, 9);
        sViewsWithIds.put(R.id.edit_text_branch, 10);
        sViewsWithIds.put(R.id.layout_deposit_type, 11);
        sViewsWithIds.put(R.id.radio_group_deposit, 12);
        sViewsWithIds.put(R.id.radio_button_cash, 13);
        sViewsWithIds.put(R.id.radio_button_cheque, 14);
        sViewsWithIds.put(R.id.note_input_layout, 15);
        sViewsWithIds.put(R.id.edit_text_note, 16);
        sViewsWithIds.put(R.id.layout_image, 17);
        sViewsWithIds.put(R.id.edit_text_attachment, 18);
        sViewsWithIds.put(R.id.button_save, 19);
        sViewsWithIds.put(R.id.lottie_progressbar, 20);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentDepositBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 21, sIncludes, sViewsWithIds));
    }
    private FragmentDepositBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.textfield.TextInputLayout) bindings[9]
            , (android.widget.Button) bindings[19]
            , (com.google.android.material.textfield.TextInputEditText) bindings[2]
            , (com.google.android.material.textfield.TextInputEditText) bindings[18]
            , (com.google.android.material.textfield.TextInputEditText) bindings[10]
            , (com.google.android.material.textfield.TextInputEditText) bindings[3]
            , (com.google.android.material.textfield.TextInputEditText) bindings[4]
            , (com.google.android.material.textfield.TextInputEditText) bindings[6]
            , (com.google.android.material.textfield.TextInputEditText) bindings[16]
            , (com.google.android.material.textfield.TextInputEditText) bindings[8]
            , (android.widget.LinearLayout) bindings[11]
            , (com.google.android.material.textfield.TextInputLayout) bindings[17]
            , (android.widget.RelativeLayout) bindings[20]
            , (com.google.android.material.textfield.TextInputLayout) bindings[5]
            , (com.google.android.material.textfield.TextInputLayout) bindings[15]
            , (android.widget.RadioButton) bindings[13]
            , (android.widget.RadioButton) bindings[14]
            , (android.widget.RadioGroup) bindings[12]
            , (com.facebook.shimmer.ShimmerFrameLayout) bindings[1]
            , (com.google.android.material.textfield.TextInputLayout) bindings[7]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.shimmerAccount.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}