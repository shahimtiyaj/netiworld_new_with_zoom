package com.netizen.netiworld.databinding;
import com.netizen.netiworld.R;
import com.netizen.netiworld.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMyPointBindingImpl extends FragmentMyPointBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.scroll_main_view, 1);
        sViewsWithIds.put(R.id.image_view_user, 2);
        sViewsWithIds.put(R.id.text_view_user_name, 3);
        sViewsWithIds.put(R.id.text_view_neti_id, 4);
        sViewsWithIds.put(R.id.text_view_start_up, 5);
        sViewsWithIds.put(R.id.text_view_my_profile, 6);
        sViewsWithIds.put(R.id.text_view_add_point, 7);
        sViewsWithIds.put(R.id.text_view_neti_mail, 8);
        sViewsWithIds.put(R.id.text_view_support_token, 9);
        sViewsWithIds.put(R.id.text_view_balance, 10);
        sViewsWithIds.put(R.id.text_view_wallet, 11);
        sViewsWithIds.put(R.id.text_view_message, 12);
        sViewsWithIds.put(R.id.text_view_purchase, 13);
        sViewsWithIds.put(R.id.text_view_general_product, 14);
        sViewsWithIds.put(R.id.text_view_offer_product, 15);
        sViewsWithIds.put(R.id.text_view_sale, 16);
        sViewsWithIds.put(R.id.text_view_sale_product, 17);
        sViewsWithIds.put(R.id.text_view_bank, 18);
        sViewsWithIds.put(R.id.text_view_balance_report, 19);
        sViewsWithIds.put(R.id.text_view_wallet_report, 20);
        sViewsWithIds.put(R.id.text_view_statement_report, 21);
        sViewsWithIds.put(R.id.text_view_message_report, 22);
        sViewsWithIds.put(R.id.text_view_revenue_report, 23);
        sViewsWithIds.put(R.id.text_view_purchase_report, 24);
        sViewsWithIds.put(R.id.text_view_general_product_report, 25);
        sViewsWithIds.put(R.id.text_view_offer_product_report, 26);
        sViewsWithIds.put(R.id.text_view_code_log_report, 27);
        sViewsWithIds.put(R.id.text_view_purchase_code_report, 28);
        sViewsWithIds.put(R.id.text_view_used_code, 29);
        sViewsWithIds.put(R.id.text_view_unused_code, 30);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMyPointBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 31, sIncludes, sViewsWithIds));
    }
    private FragmentMyPointBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[2]
            , (android.widget.RelativeLayout) bindings[0]
            , (android.widget.ScrollView) bindings[1]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[19]
            , (android.widget.TextView) bindings[18]
            , (android.widget.TextView) bindings[27]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[25]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[22]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[26]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[28]
            , (android.widget.TextView) bindings[24]
            , (android.widget.TextView) bindings[23]
            , (android.widget.TextView) bindings[16]
            , (android.widget.TextView) bindings[17]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[21]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[30]
            , (android.widget.TextView) bindings[29]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[20]
            );
        this.layoutParentView.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}