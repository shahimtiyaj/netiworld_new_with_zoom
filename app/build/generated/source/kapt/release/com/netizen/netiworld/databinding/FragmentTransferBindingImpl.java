package com.netizen.netiworld.databinding;
import com.netizen.netiworld.R;
import com.netizen.netiworld.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentTransferBindingImpl extends FragmentTransferBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(15);
        sIncludes.setIncludes(0, 
            new String[] {"fragment_trabsfer_otp"},
            new int[] {1},
            new int[] {com.netizen.netiworld.R.layout.fragment_trabsfer_otp});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.mmain_layout_id, 2);
        sViewsWithIds.put(R.id.relative_offer_code, 3);
        sViewsWithIds.put(R.id.etUsernameLayout, 4);
        sViewsWithIds.put(R.id.et_neti_Id, 5);
        sViewsWithIds.put(R.id.btn_serach_trnsferNetiId, 6);
        sViewsWithIds.put(R.id.layout_name, 7);
        sViewsWithIds.put(R.id.et_name, 8);
        sViewsWithIds.put(R.id.layout_mobile, 9);
        sViewsWithIds.put(R.id.et_mobile_no, 10);
        sViewsWithIds.put(R.id.edit_text_transfer_amount, 11);
        sViewsWithIds.put(R.id.transfer_note, 12);
        sViewsWithIds.put(R.id.transferBtn, 13);
        sViewsWithIds.put(R.id.lottie_progressbar, 14);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentTransferBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private FragmentTransferBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (android.widget.ImageButton) bindings[6]
            , (com.google.android.material.textfield.TextInputEditText) bindings[11]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[8]
            , (com.google.android.material.textfield.TextInputEditText) bindings[5]
            , (com.google.android.material.textfield.TextInputLayout) bindings[4]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.LinearLayout) bindings[7]
            , (android.widget.RelativeLayout) bindings[14]
            , (android.widget.LinearLayout) bindings[2]
            , (com.netizen.netiworld.databinding.FragmentTrabsferOtpBinding) bindings[1]
            , (android.widget.RelativeLayout) bindings[3]
            , (android.widget.Button) bindings[13]
            , (com.google.android.material.textfield.TextInputEditText) bindings[12]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        otpLayout.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (otpLayout.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        otpLayout.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeOtpLayout((com.netizen.netiworld.databinding.FragmentTrabsferOtpBinding) object, fieldId);
        }
        return false;
    }
    private boolean onChangeOtpLayout(com.netizen.netiworld.databinding.FragmentTrabsferOtpBinding OtpLayout, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
        executeBindingsOn(otpLayout);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): otpLayout
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}