package com.netizen.netiworld.databinding;
import com.netizen.netiworld.R;
import com.netizen.netiworld.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentSettingBindingImpl extends FragmentSettingBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.appbar, 1);
        sViewsWithIds.put(R.id.toolbar, 2);
        sViewsWithIds.put(R.id.back_btn, 3);
        sViewsWithIds.put(R.id.relative, 4);
        sViewsWithIds.put(R.id.setting_id, 5);
        sViewsWithIds.put(R.id.profile_pic_id, 6);
        sViewsWithIds.put(R.id.text_view_message_number, 7);
        sViewsWithIds.put(R.id.txt_profile_name, 8);
        sViewsWithIds.put(R.id.txt_neti_id, 9);
        sViewsWithIds.put(R.id.relative_personal_info, 10);
        sViewsWithIds.put(R.id.personal_info, 11);
        sViewsWithIds.put(R.id.relative1, 12);
        sViewsWithIds.put(R.id.profile, 13);
        sViewsWithIds.put(R.id.relative2, 14);
        sViewsWithIds.put(R.id.update_profile, 15);
        sViewsWithIds.put(R.id.relative3, 16);
        sViewsWithIds.put(R.id.change_password, 17);
        sViewsWithIds.put(R.id.relative4, 18);
        sViewsWithIds.put(R.id.token, 19);
        sViewsWithIds.put(R.id.relative_address_info, 20);
        sViewsWithIds.put(R.id.logout, 21);
        sViewsWithIds.put(R.id.lottie_progressbar, 22);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentSettingBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 23, sIncludes, sViewsWithIds));
    }
    private FragmentSettingBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.appbar.AppBarLayout) bindings[1]
            , (android.widget.ImageView) bindings[3]
            , (android.widget.TextView) bindings[17]
            , (android.widget.TextView) bindings[21]
            , (android.widget.RelativeLayout) bindings[22]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[13]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[6]
            , (android.widget.RelativeLayout) bindings[4]
            , (android.widget.RelativeLayout) bindings[12]
            , (android.widget.RelativeLayout) bindings[14]
            , (android.widget.RelativeLayout) bindings[16]
            , (android.widget.RelativeLayout) bindings[18]
            , (android.widget.RelativeLayout) bindings[20]
            , (android.widget.RelativeLayout) bindings[10]
            , (android.widget.TextView) bindings[5]
            , (android.widget.ImageView) bindings[7]
            , (android.widget.TextView) bindings[19]
            , (androidx.appcompat.widget.Toolbar) bindings[2]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[15]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}