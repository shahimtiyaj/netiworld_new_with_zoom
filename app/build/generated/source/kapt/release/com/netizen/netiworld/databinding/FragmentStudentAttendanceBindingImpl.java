package com.netizen.netiworld.databinding;
import com.netizen.netiworld.R;
import com.netizen.netiworld.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentStudentAttendanceBindingImpl extends FragmentStudentAttendanceBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(16);
        sIncludes.setIncludes(1, 
            new String[] {"layout_student_portal_common_header"},
            new int[] {3},
            new int[] {com.netizen.netiworld.R.layout.layout_student_portal_common_header});
        sIncludes.setIncludes(2, 
            new String[] {"attendance_calculation"},
            new int[] {4},
            new int[] {com.netizen.netiworld.R.layout.attendance_calculation});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.fragment_container, 5);
        sViewsWithIds.put(R.id.txt_view_std_attendance, 6);
        sViewsWithIds.put(R.id.layout_relative_atd, 7);
        sViewsWithIds.put(R.id.button_academic_year, 8);
        sViewsWithIds.put(R.id.edit_text_academic_year, 9);
        sViewsWithIds.put(R.id.edit_text_year, 10);
        sViewsWithIds.put(R.id.edit_text_month, 11);
        sViewsWithIds.put(R.id.edit_text_period, 12);
        sViewsWithIds.put(R.id.relative_academic, 13);
        sViewsWithIds.put(R.id.button_search, 14);
        sViewsWithIds.put(R.id.lottie_progressbar, 15);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.RelativeLayout mboundView1;
    @NonNull
    private final android.widget.LinearLayout mboundView2;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentStudentAttendanceBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 16, sIncludes, sViewsWithIds));
    }
    private FragmentStudentAttendanceBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (android.widget.Button) bindings[8]
            , (android.widget.Button) bindings[14]
            , (com.google.android.material.textfield.TextInputEditText) bindings[9]
            , (com.google.android.material.textfield.TextInputEditText) bindings[11]
            , (com.google.android.material.textfield.TextInputEditText) bindings[12]
            , (com.google.android.material.textfield.TextInputEditText) bindings[10]
            , (android.widget.RelativeLayout) bindings[5]
            , (com.netizen.netiworld.databinding.AttendanceCalculationBinding) bindings[4]
            , (android.widget.RelativeLayout) bindings[7]
            , (com.netizen.netiworld.databinding.LayoutStudentPortalCommonHeaderBinding) bindings[3]
            , (android.widget.RelativeLayout) bindings[15]
            , (android.widget.RelativeLayout) bindings[13]
            , (android.widget.TextView) bindings[6]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.RelativeLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.LinearLayout) bindings[2];
        this.mboundView2.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        layoutStudentInfo.invalidateAll();
        layoutAtdCalculation.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (layoutStudentInfo.hasPendingBindings()) {
            return true;
        }
        if (layoutAtdCalculation.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        layoutStudentInfo.setLifecycleOwner(lifecycleOwner);
        layoutAtdCalculation.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeLayoutStudentInfo((com.netizen.netiworld.databinding.LayoutStudentPortalCommonHeaderBinding) object, fieldId);
            case 1 :
                return onChangeLayoutAtdCalculation((com.netizen.netiworld.databinding.AttendanceCalculationBinding) object, fieldId);
        }
        return false;
    }
    private boolean onChangeLayoutStudentInfo(com.netizen.netiworld.databinding.LayoutStudentPortalCommonHeaderBinding LayoutStudentInfo, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeLayoutAtdCalculation(com.netizen.netiworld.databinding.AttendanceCalculationBinding LayoutAtdCalculation, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
        executeBindingsOn(layoutStudentInfo);
        executeBindingsOn(layoutAtdCalculation);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): layoutStudentInfo
        flag 1 (0x2L): layoutAtdCalculation
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}