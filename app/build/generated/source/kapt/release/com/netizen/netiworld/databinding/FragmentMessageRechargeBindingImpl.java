package com.netizen.netiworld.databinding;
import com.netizen.netiworld.R;
import com.netizen.netiworld.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentMessageRechargeBindingImpl extends FragmentMessageRechargeBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(17);
        sIncludes.setIncludes(1, 
            new String[] {"message_recharge_price_layout"},
            new int[] {2},
            new int[] {com.netizen.netiworld.R.layout.message_recharge_price_layout});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.fragment_container, 3);
        sViewsWithIds.put(R.id.card_lin0, 4);
        sViewsWithIds.put(R.id.textViewWalletBalance, 5);
        sViewsWithIds.put(R.id.textSmsBalance, 6);
        sViewsWithIds.put(R.id.textViewMessageBalance, 7);
        sViewsWithIds.put(R.id.textBillPayment, 8);
        sViewsWithIds.put(R.id.layoutInput, 9);
        sViewsWithIds.put(R.id.etUsernameLayout, 10);
        sViewsWithIds.put(R.id.spinner_purchase_point, 11);
        sViewsWithIds.put(R.id.etPasswordLayout, 12);
        sViewsWithIds.put(R.id.spinner_message_type, 13);
        sViewsWithIds.put(R.id.et_message_quantity, 14);
        sViewsWithIds.put(R.id.btnMessageRecharge, 15);
        sViewsWithIds.put(R.id.lottie_progressbar, 16);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentMessageRechargeBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 17, sIncludes, sViewsWithIds));
    }
    private FragmentMessageRechargeBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (android.widget.Button) bindings[15]
            , (android.widget.LinearLayout) bindings[4]
            , (com.google.android.material.textfield.TextInputEditText) bindings[14]
            , (com.google.android.material.textfield.TextInputLayout) bindings[12]
            , (com.google.android.material.textfield.TextInputLayout) bindings[10]
            , (android.widget.RelativeLayout) bindings[3]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.RelativeLayout) bindings[16]
            , (com.netizen.netiworld.databinding.MessageRechargePriceLayoutBinding) bindings[2]
            , (android.widget.RelativeLayout) bindings[1]
            , (com.google.android.material.textfield.TextInputEditText) bindings[13]
            , (com.google.android.material.textfield.TextInputEditText) bindings[11]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[5]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.relative.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        messageLayputPoint.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (messageLayputPoint.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        messageLayputPoint.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeMessageLayputPoint((com.netizen.netiworld.databinding.MessageRechargePriceLayoutBinding) object, fieldId);
        }
        return false;
    }
    private boolean onChangeMessageLayputPoint(com.netizen.netiworld.databinding.MessageRechargePriceLayoutBinding MessageLayputPoint, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
        executeBindingsOn(messageLayputPoint);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): messageLayputPoint
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}