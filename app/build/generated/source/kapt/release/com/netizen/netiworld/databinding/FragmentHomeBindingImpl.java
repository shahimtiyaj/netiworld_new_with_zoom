package com.netizen.netiworld.databinding;
import com.netizen.netiworld.R;
import com.netizen.netiworld.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentHomeBindingImpl extends FragmentHomeBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(18);
        sIncludes.setIncludes(1, 
            new String[] {"custom_notification_layout"},
            new int[] {4},
            new int[] {com.netizen.netiworld.R.layout.custom_notification_layout});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.layout_all_points, 3);
        sViewsWithIds.put(R.id.app_bar, 5);
        sViewsWithIds.put(R.id.toolbar, 6);
        sViewsWithIds.put(R.id.navigation_id, 7);
        sViewsWithIds.put(R.id.image_view_setting, 8);
        sViewsWithIds.put(R.id.image_view_user, 9);
        sViewsWithIds.put(R.id.text_view_user_name, 10);
        sViewsWithIds.put(R.id.text_view_neti_id, 11);
        sViewsWithIds.put(R.id.text_view_mobile_no, 12);
        sViewsWithIds.put(R.id.text_view_email, 13);
        sViewsWithIds.put(R.id.text_view_wallet_balance, 14);
        sViewsWithIds.put(R.id.text_view_message_balance, 15);
        sViewsWithIds.put(R.id.layout_footer, 16);
        sViewsWithIds.put(R.id.lottie_progressbar, 17);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.RelativeLayout mboundView1;
    @NonNull
    private final android.widget.LinearLayout mboundView2;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentHomeBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 18, sIncludes, sViewsWithIds));
    }
    private FragmentHomeBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (com.google.android.material.appbar.AppBarLayout) bindings[5]
            , (com.netizen.netiworld.databinding.CustomNotificationLayoutBinding) bindings[4]
            , (android.widget.ImageView) bindings[8]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[9]
            , (android.view.View) bindings[3]
            , (android.widget.ImageView) bindings[16]
            , (android.widget.RelativeLayout) bindings[17]
            , (android.widget.ImageView) bindings[7]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[14]
            , (androidx.appcompat.widget.Toolbar) bindings[6]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.RelativeLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView2 = (android.widget.LinearLayout) bindings[2];
        this.mboundView2.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        customNotification.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (customNotification.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        customNotification.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeCustomNotification((com.netizen.netiworld.databinding.CustomNotificationLayoutBinding) object, fieldId);
        }
        return false;
    }
    private boolean onChangeCustomNotification(com.netizen.netiworld.databinding.CustomNotificationLayoutBinding CustomNotification, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
        executeBindingsOn(customNotification);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): customNotification
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}