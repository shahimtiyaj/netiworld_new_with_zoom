package com.netizen.netiworld.databinding;
import com.netizen.netiworld.R;
import com.netizen.netiworld.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentClassRoutineBindingImpl extends FragmentClassRoutineBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(11);
        sIncludes.setIncludes(1, 
            new String[] {"layout_student_portal_common_header"},
            new int[] {3},
            new int[] {com.netizen.netiworld.R.layout.layout_student_portal_common_header});
        sIncludes.setIncludes(2, 
            new String[] {"week_name_layout"},
            new int[] {4},
            new int[] {com.netizen.netiworld.R.layout.week_name_layout});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.fragment_container, 5);
        sViewsWithIds.put(R.id.relative_tab_view, 6);
        sViewsWithIds.put(R.id.button_class_test, 7);
        sViewsWithIds.put(R.id.button_semester_exam, 8);
        sViewsWithIds.put(R.id.linear3, 9);
        sViewsWithIds.put(R.id.lottie_progressbar, 10);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    @NonNull
    private final android.widget.RelativeLayout mboundView2;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentClassRoutineBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 11, sIncludes, sViewsWithIds));
    }
    private FragmentClassRoutineBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 2
            , (android.widget.Button) bindings[7]
            , (android.widget.Button) bindings[8]
            , (android.widget.RelativeLayout) bindings[5]
            , (com.netizen.netiworld.databinding.LayoutStudentPortalCommonHeaderBinding) bindings[3]
            , (com.netizen.netiworld.databinding.WeekNameLayoutBinding) bindings[4]
            , (android.widget.LinearLayout) bindings[9]
            , (android.widget.RelativeLayout) bindings[10]
            , (android.widget.RelativeLayout) bindings[1]
            , (android.widget.LinearLayout) bindings[6]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView2 = (android.widget.RelativeLayout) bindings[2];
        this.mboundView2.setTag(null);
        this.relativeStudentInfo.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        layoutStudentInfo.invalidateAll();
        layoutWeekCalendar.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (layoutStudentInfo.hasPendingBindings()) {
            return true;
        }
        if (layoutWeekCalendar.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        layoutStudentInfo.setLifecycleOwner(lifecycleOwner);
        layoutWeekCalendar.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeLayoutStudentInfo((com.netizen.netiworld.databinding.LayoutStudentPortalCommonHeaderBinding) object, fieldId);
            case 1 :
                return onChangeLayoutWeekCalendar((com.netizen.netiworld.databinding.WeekNameLayoutBinding) object, fieldId);
        }
        return false;
    }
    private boolean onChangeLayoutStudentInfo(com.netizen.netiworld.databinding.LayoutStudentPortalCommonHeaderBinding LayoutStudentInfo, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }
    private boolean onChangeLayoutWeekCalendar(com.netizen.netiworld.databinding.WeekNameLayoutBinding LayoutWeekCalendar, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
        executeBindingsOn(layoutStudentInfo);
        executeBindingsOn(layoutWeekCalendar);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): layoutStudentInfo
        flag 1 (0x2L): layoutWeekCalendar
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}