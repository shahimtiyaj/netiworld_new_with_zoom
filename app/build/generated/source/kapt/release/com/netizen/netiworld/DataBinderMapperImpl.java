package com.netizen.netiworld;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.netizen.netiworld.databinding.ActivityMainBindingImpl;
import com.netizen.netiworld.databinding.AttendanceCalculationBindingImpl;
import com.netizen.netiworld.databinding.AttendanceListItemBindingImpl;
import com.netizen.netiworld.databinding.BalanceTransferSucessfullLayoutBindingImpl;
import com.netizen.netiworld.databinding.ClassRoutineItemListBindingImpl;
import com.netizen.netiworld.databinding.ClassTestListItemBindingImpl;
import com.netizen.netiworld.databinding.CustomNotificationLayoutBindingImpl;
import com.netizen.netiworld.databinding.ExamRoutineItemListBindingImpl;
import com.netizen.netiworld.databinding.FeesInfoPaidItemBindingImpl;
import com.netizen.netiworld.databinding.FragmentAddAcademicEducationBindingImpl;
import com.netizen.netiworld.databinding.FragmentAddBankInfoBindingImpl;
import com.netizen.netiworld.databinding.FragmentAddExperienceInfoBindingImpl;
import com.netizen.netiworld.databinding.FragmentAddPointBindingImpl;
import com.netizen.netiworld.databinding.FragmentAddProfessionalQualificationInfoBindingImpl;
import com.netizen.netiworld.databinding.FragmentAddReferenceInfoBindingImpl;
import com.netizen.netiworld.databinding.FragmentAddTrainingInfoBindingImpl;
import com.netizen.netiworld.databinding.FragmentBalanceStatementReportBindingImpl;
import com.netizen.netiworld.databinding.FragmentBankAccountBindingImpl;
import com.netizen.netiworld.databinding.FragmentBankListBindingImpl;
import com.netizen.netiworld.databinding.FragmentChangePasswordBindingImpl;
import com.netizen.netiworld.databinding.FragmentClassRoutineBindingImpl;
import com.netizen.netiworld.databinding.FragmentClassTestBindingImpl;
import com.netizen.netiworld.databinding.FragmentDepositBindingImpl;
import com.netizen.netiworld.databinding.FragmentDepositReportBindingImpl;
import com.netizen.netiworld.databinding.FragmentDepositReportSelectionBindingImpl;
import com.netizen.netiworld.databinding.FragmentExamRoutineBindingImpl;
import com.netizen.netiworld.databinding.FragmentFeesInfoBindingImpl;
import com.netizen.netiworld.databinding.FragmentForgotPasswordFourBindingImpl;
import com.netizen.netiworld.databinding.FragmentForgotPasswordOneBindingImpl;
import com.netizen.netiworld.databinding.FragmentForgotPasswordThreeBindingImpl;
import com.netizen.netiworld.databinding.FragmentForgotPasswordTwoBindingImpl;
import com.netizen.netiworld.databinding.FragmentGeneralProductReportBindingImpl;
import com.netizen.netiworld.databinding.FragmentGeneralProductReportSelectionBindingImpl;
import com.netizen.netiworld.databinding.FragmentHomeBindingImpl;
import com.netizen.netiworld.databinding.FragmentHomeBindingSw600dpImpl;
import com.netizen.netiworld.databinding.FragmentInstituteChangeDialogBindingImpl;
import com.netizen.netiworld.databinding.FragmentInventoryInfoBindingImpl;
import com.netizen.netiworld.databinding.FragmentLogOutDialogBindingImpl;
import com.netizen.netiworld.databinding.FragmentMessageRechargeBindingImpl;
import com.netizen.netiworld.databinding.FragmentMessageReportBindingImpl;
import com.netizen.netiworld.databinding.FragmentMyPointBindingImpl;
import com.netizen.netiworld.databinding.FragmentMyProfileBindingImpl;
import com.netizen.netiworld.databinding.FragmentNetiMailOneBindingImpl;
import com.netizen.netiworld.databinding.FragmentNetiMailTwoBindingImpl;
import com.netizen.netiworld.databinding.FragmentOfferProductBindingImpl;
import com.netizen.netiworld.databinding.FragmentOfferProductReportBindingImpl;
import com.netizen.netiworld.databinding.FragmentOfferProductReportSelectionBindingImpl;
import com.netizen.netiworld.databinding.FragmentProfileLayoutBindingImpl;
import com.netizen.netiworld.databinding.FragmentPurchaseCodeLogBindingImpl;
import com.netizen.netiworld.databinding.FragmentPurchaseGeneralProductBindingImpl;
import com.netizen.netiworld.databinding.FragmentRevenueLogReportBindingImpl;
import com.netizen.netiworld.databinding.FragmentRevenueReportSelectionBindingImpl;
import com.netizen.netiworld.databinding.FragmentSearchSpinnerBindingImpl;
import com.netizen.netiworld.databinding.FragmentSemesterExamBindingImpl;
import com.netizen.netiworld.databinding.FragmentSettingBindingImpl;
import com.netizen.netiworld.databinding.FragmentSignInBindingImpl;
import com.netizen.netiworld.databinding.FragmentSignUpOneBindingImpl;
import com.netizen.netiworld.databinding.FragmentSignUpTwoBindingImpl;
import com.netizen.netiworld.databinding.FragmentStudentAttendanceBindingImpl;
import com.netizen.netiworld.databinding.FragmentStudentProfileBindingImpl;
import com.netizen.netiworld.databinding.FragmentSubjectListBindingImpl;
import com.netizen.netiworld.databinding.FragmentSubmitTokenBindingImpl;
import com.netizen.netiworld.databinding.FragmentTabBindingImpl;
import com.netizen.netiworld.databinding.FragmentTagDialogBindingImpl;
import com.netizen.netiworld.databinding.FragmentTokenDetailsBindingImpl;
import com.netizen.netiworld.databinding.FragmentTokenListBindingImpl;
import com.netizen.netiworld.databinding.FragmentToolBarBindingImpl;
import com.netizen.netiworld.databinding.FragmentTrabsferOtpBindingImpl;
import com.netizen.netiworld.databinding.FragmentTransferBindingImpl;
import com.netizen.netiworld.databinding.FragmentTransferReportBindingImpl;
import com.netizen.netiworld.databinding.FragmentTransferReportSelectionBindingImpl;
import com.netizen.netiworld.databinding.FragmentUnusedPurchaseReportBindingImpl;
import com.netizen.netiworld.databinding.FragmentUpdateAddressInfoBindingImpl;
import com.netizen.netiworld.databinding.FragmentUpdatePersonalInfoBindingImpl;
import com.netizen.netiworld.databinding.FragmentUpdateUserInfoBindingImpl;
import com.netizen.netiworld.databinding.FragmentUserPointBindingImpl;
import com.netizen.netiworld.databinding.FragmentWalletBindingImpl;
import com.netizen.netiworld.databinding.FragmentWalletReportBindingImpl;
import com.netizen.netiworld.databinding.FragmentWithdrawBindingImpl;
import com.netizen.netiworld.databinding.FragmentWithdrawReportBindingImpl;
import com.netizen.netiworld.databinding.FragmentWithdrawReportSelectionBindingImpl;
import com.netizen.netiworld.databinding.InstituteListItemBindingImpl;
import com.netizen.netiworld.databinding.InventoryListItemBindingImpl;
import com.netizen.netiworld.databinding.LayoutStudentPortalCommonHeaderBindingImpl;
import com.netizen.netiworld.databinding.LoginInvalidBindingImpl;
import com.netizen.netiworld.databinding.MessageRechargePriceLayoutBindingImpl;
import com.netizen.netiworld.databinding.RegistrationSucessLayoutBindingImpl;
import com.netizen.netiworld.databinding.ReportsBalanceStatementRowBindingImpl;
import com.netizen.netiworld.databinding.ReportsPurchaseProductOfferRowBindingImpl;
import com.netizen.netiworld.databinding.ReportsPurchaseUnusedCodeRowBindingImpl;
import com.netizen.netiworld.databinding.ReportsPurchaseUsedCodeRowBindingImpl;
import com.netizen.netiworld.databinding.ReportsRecylerviewListBindingImpl;
import com.netizen.netiworld.databinding.ReportsWalletBalanceTransferRowBindingImpl;
import com.netizen.netiworld.databinding.ReportsWalletMessageRechargeRowBindingImpl;
import com.netizen.netiworld.databinding.ResetPasswordSucessLayoutBindingImpl;
import com.netizen.netiworld.databinding.SingleBankInfoLayoutBindingImpl;
import com.netizen.netiworld.databinding.SingleBankListLayoutBindingImpl;
import com.netizen.netiworld.databinding.SingleCertificationLayoutBindingImpl;
import com.netizen.netiworld.databinding.SingleCommonReportLayoutBindingImpl;
import com.netizen.netiworld.databinding.SingleEducationInfoLayoutBindingImpl;
import com.netizen.netiworld.databinding.SingleExperienceDetailsLayoutBindingImpl;
import com.netizen.netiworld.databinding.SingleMessageLogReportLayoutBindingImpl;
import com.netizen.netiworld.databinding.SingleReferenceInfoLayoutBindingImpl;
import com.netizen.netiworld.databinding.SingleRevenueReportLayoutBindingImpl;
import com.netizen.netiworld.databinding.SingleSpinnerLayoutBindingImpl;
import com.netizen.netiworld.databinding.SingleTokenLayoutBindingImpl;
import com.netizen.netiworld.databinding.SingleTrainingInfoLayoutBindingImpl;
import com.netizen.netiworld.databinding.SubjectListItemBindingImpl;
import com.netizen.netiworld.databinding.Test1BindingImpl;
import com.netizen.netiworld.databinding.TestBindingImpl;
import com.netizen.netiworld.databinding.ToolbarBindingImpl;
import com.netizen.netiworld.databinding.UserRegistrationLayoutTwoBindingImpl;
import com.netizen.netiworld.databinding.WeekNameLayoutBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYMAIN = 1;

  private static final int LAYOUT_ATTENDANCECALCULATION = 2;

  private static final int LAYOUT_ATTENDANCELISTITEM = 3;

  private static final int LAYOUT_BALANCETRANSFERSUCESSFULLLAYOUT = 4;

  private static final int LAYOUT_CLASSROUTINEITEMLIST = 5;

  private static final int LAYOUT_CLASSTESTLISTITEM = 6;

  private static final int LAYOUT_CUSTOMNOTIFICATIONLAYOUT = 7;

  private static final int LAYOUT_EXAMROUTINEITEMLIST = 8;

  private static final int LAYOUT_FEESINFOPAIDITEM = 9;

  private static final int LAYOUT_FRAGMENTADDACADEMICEDUCATION = 10;

  private static final int LAYOUT_FRAGMENTADDBANKINFO = 11;

  private static final int LAYOUT_FRAGMENTADDEXPERIENCEINFO = 12;

  private static final int LAYOUT_FRAGMENTADDPOINT = 13;

  private static final int LAYOUT_FRAGMENTADDPROFESSIONALQUALIFICATIONINFO = 14;

  private static final int LAYOUT_FRAGMENTADDREFERENCEINFO = 15;

  private static final int LAYOUT_FRAGMENTADDTRAININGINFO = 16;

  private static final int LAYOUT_FRAGMENTBALANCESTATEMENTREPORT = 17;

  private static final int LAYOUT_FRAGMENTBANKACCOUNT = 18;

  private static final int LAYOUT_FRAGMENTBANKLIST = 19;

  private static final int LAYOUT_FRAGMENTCHANGEPASSWORD = 20;

  private static final int LAYOUT_FRAGMENTCLASSROUTINE = 21;

  private static final int LAYOUT_FRAGMENTCLASSTEST = 22;

  private static final int LAYOUT_FRAGMENTDEPOSIT = 23;

  private static final int LAYOUT_FRAGMENTDEPOSITREPORT = 24;

  private static final int LAYOUT_FRAGMENTDEPOSITREPORTSELECTION = 25;

  private static final int LAYOUT_FRAGMENTEXAMROUTINE = 26;

  private static final int LAYOUT_FRAGMENTFEESINFO = 27;

  private static final int LAYOUT_FRAGMENTFORGOTPASSWORDFOUR = 28;

  private static final int LAYOUT_FRAGMENTFORGOTPASSWORDONE = 29;

  private static final int LAYOUT_FRAGMENTFORGOTPASSWORDTHREE = 30;

  private static final int LAYOUT_FRAGMENTFORGOTPASSWORDTWO = 31;

  private static final int LAYOUT_FRAGMENTGENERALPRODUCTREPORT = 32;

  private static final int LAYOUT_FRAGMENTGENERALPRODUCTREPORTSELECTION = 33;

  private static final int LAYOUT_FRAGMENTHOME = 34;

  private static final int LAYOUT_FRAGMENTINSTITUTECHANGEDIALOG = 35;

  private static final int LAYOUT_FRAGMENTINVENTORYINFO = 36;

  private static final int LAYOUT_FRAGMENTLOGOUTDIALOG = 37;

  private static final int LAYOUT_FRAGMENTMESSAGERECHARGE = 38;

  private static final int LAYOUT_FRAGMENTMESSAGEREPORT = 39;

  private static final int LAYOUT_FRAGMENTMYPOINT = 40;

  private static final int LAYOUT_FRAGMENTMYPROFILE = 41;

  private static final int LAYOUT_FRAGMENTNETIMAILONE = 42;

  private static final int LAYOUT_FRAGMENTNETIMAILTWO = 43;

  private static final int LAYOUT_FRAGMENTOFFERPRODUCT = 44;

  private static final int LAYOUT_FRAGMENTOFFERPRODUCTREPORT = 45;

  private static final int LAYOUT_FRAGMENTOFFERPRODUCTREPORTSELECTION = 46;

  private static final int LAYOUT_FRAGMENTPROFILELAYOUT = 47;

  private static final int LAYOUT_FRAGMENTPURCHASECODELOG = 48;

  private static final int LAYOUT_FRAGMENTPURCHASEGENERALPRODUCT = 49;

  private static final int LAYOUT_FRAGMENTREVENUELOGREPORT = 50;

  private static final int LAYOUT_FRAGMENTREVENUEREPORTSELECTION = 51;

  private static final int LAYOUT_FRAGMENTSEARCHSPINNER = 52;

  private static final int LAYOUT_FRAGMENTSEMESTEREXAM = 53;

  private static final int LAYOUT_FRAGMENTSETTING = 54;

  private static final int LAYOUT_FRAGMENTSIGNIN = 55;

  private static final int LAYOUT_FRAGMENTSIGNUPONE = 56;

  private static final int LAYOUT_FRAGMENTSIGNUPTWO = 57;

  private static final int LAYOUT_FRAGMENTSTUDENTATTENDANCE = 58;

  private static final int LAYOUT_FRAGMENTSTUDENTPROFILE = 59;

  private static final int LAYOUT_FRAGMENTSUBJECTLIST = 60;

  private static final int LAYOUT_FRAGMENTSUBMITTOKEN = 61;

  private static final int LAYOUT_FRAGMENTTAB = 62;

  private static final int LAYOUT_FRAGMENTTAGDIALOG = 63;

  private static final int LAYOUT_FRAGMENTTOKENDETAILS = 64;

  private static final int LAYOUT_FRAGMENTTOKENLIST = 65;

  private static final int LAYOUT_FRAGMENTTOOLBAR = 66;

  private static final int LAYOUT_FRAGMENTTRABSFEROTP = 67;

  private static final int LAYOUT_FRAGMENTTRANSFER = 68;

  private static final int LAYOUT_FRAGMENTTRANSFERREPORT = 69;

  private static final int LAYOUT_FRAGMENTTRANSFERREPORTSELECTION = 70;

  private static final int LAYOUT_FRAGMENTUNUSEDPURCHASEREPORT = 71;

  private static final int LAYOUT_FRAGMENTUPDATEADDRESSINFO = 72;

  private static final int LAYOUT_FRAGMENTUPDATEPERSONALINFO = 73;

  private static final int LAYOUT_FRAGMENTUPDATEUSERINFO = 74;

  private static final int LAYOUT_FRAGMENTUSERPOINT = 75;

  private static final int LAYOUT_FRAGMENTWALLET = 76;

  private static final int LAYOUT_FRAGMENTWALLETREPORT = 77;

  private static final int LAYOUT_FRAGMENTWITHDRAW = 78;

  private static final int LAYOUT_FRAGMENTWITHDRAWREPORT = 79;

  private static final int LAYOUT_FRAGMENTWITHDRAWREPORTSELECTION = 80;

  private static final int LAYOUT_INSTITUTELISTITEM = 81;

  private static final int LAYOUT_INVENTORYLISTITEM = 82;

  private static final int LAYOUT_LAYOUTSTUDENTPORTALCOMMONHEADER = 83;

  private static final int LAYOUT_LOGININVALID = 84;

  private static final int LAYOUT_MESSAGERECHARGEPRICELAYOUT = 85;

  private static final int LAYOUT_REGISTRATIONSUCESSLAYOUT = 86;

  private static final int LAYOUT_REPORTSBALANCESTATEMENTROW = 87;

  private static final int LAYOUT_REPORTSPURCHASEPRODUCTOFFERROW = 88;

  private static final int LAYOUT_REPORTSPURCHASEUNUSEDCODEROW = 89;

  private static final int LAYOUT_REPORTSPURCHASEUSEDCODEROW = 90;

  private static final int LAYOUT_REPORTSRECYLERVIEWLIST = 91;

  private static final int LAYOUT_REPORTSWALLETBALANCETRANSFERROW = 92;

  private static final int LAYOUT_REPORTSWALLETMESSAGERECHARGEROW = 93;

  private static final int LAYOUT_RESETPASSWORDSUCESSLAYOUT = 94;

  private static final int LAYOUT_SINGLEBANKINFOLAYOUT = 95;

  private static final int LAYOUT_SINGLEBANKLISTLAYOUT = 96;

  private static final int LAYOUT_SINGLECERTIFICATIONLAYOUT = 97;

  private static final int LAYOUT_SINGLECOMMONREPORTLAYOUT = 98;

  private static final int LAYOUT_SINGLEEDUCATIONINFOLAYOUT = 99;

  private static final int LAYOUT_SINGLEEXPERIENCEDETAILSLAYOUT = 100;

  private static final int LAYOUT_SINGLEMESSAGELOGREPORTLAYOUT = 101;

  private static final int LAYOUT_SINGLEREFERENCEINFOLAYOUT = 102;

  private static final int LAYOUT_SINGLEREVENUEREPORTLAYOUT = 103;

  private static final int LAYOUT_SINGLESPINNERLAYOUT = 104;

  private static final int LAYOUT_SINGLETOKENLAYOUT = 105;

  private static final int LAYOUT_SINGLETRAININGINFOLAYOUT = 106;

  private static final int LAYOUT_SUBJECTLISTITEM = 107;

  private static final int LAYOUT_TEST = 108;

  private static final int LAYOUT_TEST1 = 109;

  private static final int LAYOUT_TOOLBAR = 110;

  private static final int LAYOUT_USERREGISTRATIONLAYOUTTWO = 111;

  private static final int LAYOUT_WEEKNAMELAYOUT = 112;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(112);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.attendance_calculation, LAYOUT_ATTENDANCECALCULATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.attendance_list_item, LAYOUT_ATTENDANCELISTITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.balance_transfer_sucessfull_layout, LAYOUT_BALANCETRANSFERSUCESSFULLLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.class_routine_item_list, LAYOUT_CLASSROUTINEITEMLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.class_test_list_item, LAYOUT_CLASSTESTLISTITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.custom_notification_layout, LAYOUT_CUSTOMNOTIFICATIONLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.exam_routine_item_list, LAYOUT_EXAMROUTINEITEMLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fees_info_paid_item, LAYOUT_FEESINFOPAIDITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_add_academic_education, LAYOUT_FRAGMENTADDACADEMICEDUCATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_add_bank_info, LAYOUT_FRAGMENTADDBANKINFO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_add_experience_info, LAYOUT_FRAGMENTADDEXPERIENCEINFO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_add_point, LAYOUT_FRAGMENTADDPOINT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_add_professional_qualification_info, LAYOUT_FRAGMENTADDPROFESSIONALQUALIFICATIONINFO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_add_reference_info, LAYOUT_FRAGMENTADDREFERENCEINFO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_add_training_info, LAYOUT_FRAGMENTADDTRAININGINFO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_balance_statement_report, LAYOUT_FRAGMENTBALANCESTATEMENTREPORT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_bank_account, LAYOUT_FRAGMENTBANKACCOUNT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_bank_list, LAYOUT_FRAGMENTBANKLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_change_password, LAYOUT_FRAGMENTCHANGEPASSWORD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_class_routine, LAYOUT_FRAGMENTCLASSROUTINE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_class_test, LAYOUT_FRAGMENTCLASSTEST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_deposit, LAYOUT_FRAGMENTDEPOSIT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_deposit_report, LAYOUT_FRAGMENTDEPOSITREPORT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_deposit_report_selection, LAYOUT_FRAGMENTDEPOSITREPORTSELECTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_exam_routine, LAYOUT_FRAGMENTEXAMROUTINE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_fees_info, LAYOUT_FRAGMENTFEESINFO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_forgot_password_four, LAYOUT_FRAGMENTFORGOTPASSWORDFOUR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_forgot_password_one, LAYOUT_FRAGMENTFORGOTPASSWORDONE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_forgot_password_three, LAYOUT_FRAGMENTFORGOTPASSWORDTHREE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_forgot_password_two, LAYOUT_FRAGMENTFORGOTPASSWORDTWO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_general_product_report, LAYOUT_FRAGMENTGENERALPRODUCTREPORT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_general_product_report_selection, LAYOUT_FRAGMENTGENERALPRODUCTREPORTSELECTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_home, LAYOUT_FRAGMENTHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_institute_change_dialog, LAYOUT_FRAGMENTINSTITUTECHANGEDIALOG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_inventory_info, LAYOUT_FRAGMENTINVENTORYINFO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_log_out_dialog, LAYOUT_FRAGMENTLOGOUTDIALOG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_message_recharge, LAYOUT_FRAGMENTMESSAGERECHARGE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_message_report, LAYOUT_FRAGMENTMESSAGEREPORT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_my_point, LAYOUT_FRAGMENTMYPOINT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_my_profile, LAYOUT_FRAGMENTMYPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_neti_mail_one, LAYOUT_FRAGMENTNETIMAILONE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_neti_mail_two, LAYOUT_FRAGMENTNETIMAILTWO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_offer_product, LAYOUT_FRAGMENTOFFERPRODUCT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_offer_product_report, LAYOUT_FRAGMENTOFFERPRODUCTREPORT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_offer_product_report_selection, LAYOUT_FRAGMENTOFFERPRODUCTREPORTSELECTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_profile_layout, LAYOUT_FRAGMENTPROFILELAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_purchase_code_log, LAYOUT_FRAGMENTPURCHASECODELOG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_purchase_general_product, LAYOUT_FRAGMENTPURCHASEGENERALPRODUCT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_revenue_log_report, LAYOUT_FRAGMENTREVENUELOGREPORT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_revenue_report_selection, LAYOUT_FRAGMENTREVENUEREPORTSELECTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_search_spinner, LAYOUT_FRAGMENTSEARCHSPINNER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_semester_exam, LAYOUT_FRAGMENTSEMESTEREXAM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_setting, LAYOUT_FRAGMENTSETTING);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_sign_in, LAYOUT_FRAGMENTSIGNIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_sign_up_one, LAYOUT_FRAGMENTSIGNUPONE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_sign_up_two, LAYOUT_FRAGMENTSIGNUPTWO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_student_attendance, LAYOUT_FRAGMENTSTUDENTATTENDANCE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_student_profile, LAYOUT_FRAGMENTSTUDENTPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_subject_list, LAYOUT_FRAGMENTSUBJECTLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_submit_token, LAYOUT_FRAGMENTSUBMITTOKEN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_tab, LAYOUT_FRAGMENTTAB);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_tag_dialog, LAYOUT_FRAGMENTTAGDIALOG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_token_details, LAYOUT_FRAGMENTTOKENDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_token_list, LAYOUT_FRAGMENTTOKENLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_tool_bar, LAYOUT_FRAGMENTTOOLBAR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_trabsfer_otp, LAYOUT_FRAGMENTTRABSFEROTP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_transfer, LAYOUT_FRAGMENTTRANSFER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_transfer_report, LAYOUT_FRAGMENTTRANSFERREPORT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_transfer_report_selection, LAYOUT_FRAGMENTTRANSFERREPORTSELECTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_unused_purchase_report, LAYOUT_FRAGMENTUNUSEDPURCHASEREPORT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_update_address_info, LAYOUT_FRAGMENTUPDATEADDRESSINFO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_update_personal_info, LAYOUT_FRAGMENTUPDATEPERSONALINFO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_update_user_info, LAYOUT_FRAGMENTUPDATEUSERINFO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_user_point, LAYOUT_FRAGMENTUSERPOINT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_wallet, LAYOUT_FRAGMENTWALLET);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_wallet_report, LAYOUT_FRAGMENTWALLETREPORT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_withdraw, LAYOUT_FRAGMENTWITHDRAW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_withdraw_report, LAYOUT_FRAGMENTWITHDRAWREPORT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.fragment_withdraw_report_selection, LAYOUT_FRAGMENTWITHDRAWREPORTSELECTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.institute_list_item, LAYOUT_INSTITUTELISTITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.inventory_list_item, LAYOUT_INVENTORYLISTITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.layout_student_portal_common_header, LAYOUT_LAYOUTSTUDENTPORTALCOMMONHEADER);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.login_invalid, LAYOUT_LOGININVALID);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.message_recharge_price_layout, LAYOUT_MESSAGERECHARGEPRICELAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.registration_sucess_layout, LAYOUT_REGISTRATIONSUCESSLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.reports_balance_statement_row, LAYOUT_REPORTSBALANCESTATEMENTROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.reports_purchase_product_offer_row, LAYOUT_REPORTSPURCHASEPRODUCTOFFERROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.reports_purchase_unused_code_row, LAYOUT_REPORTSPURCHASEUNUSEDCODEROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.reports_purchase_used_code_row, LAYOUT_REPORTSPURCHASEUSEDCODEROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.reports_recylerview_list, LAYOUT_REPORTSRECYLERVIEWLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.reports_wallet_balance_transfer_row, LAYOUT_REPORTSWALLETBALANCETRANSFERROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.reports_wallet_message_recharge_row, LAYOUT_REPORTSWALLETMESSAGERECHARGEROW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.reset_password_sucess_layout, LAYOUT_RESETPASSWORDSUCESSLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.single_bank_info_layout, LAYOUT_SINGLEBANKINFOLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.single_bank_list_layout, LAYOUT_SINGLEBANKLISTLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.single_certification_layout, LAYOUT_SINGLECERTIFICATIONLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.single_common_report_layout, LAYOUT_SINGLECOMMONREPORTLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.single_education_info_layout, LAYOUT_SINGLEEDUCATIONINFOLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.single_experience_details_layout, LAYOUT_SINGLEEXPERIENCEDETAILSLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.single_message_log_report_layout, LAYOUT_SINGLEMESSAGELOGREPORTLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.single_reference_info_layout, LAYOUT_SINGLEREFERENCEINFOLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.single_revenue_report_layout, LAYOUT_SINGLEREVENUEREPORTLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.single_spinner_layout, LAYOUT_SINGLESPINNERLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.single_token_layout, LAYOUT_SINGLETOKENLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.single_training_info_layout, LAYOUT_SINGLETRAININGINFOLAYOUT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.subject_list_item, LAYOUT_SUBJECTLISTITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.test, LAYOUT_TEST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.test1, LAYOUT_TEST1);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.toolbar, LAYOUT_TOOLBAR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.user_registration_layout_two, LAYOUT_USERREGISTRATIONLAYOUTTWO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.netizen.netiworld.R.layout.week_name_layout, LAYOUT_WEEKNAMELAYOUT);
  }

  private final ViewDataBinding internalGetViewDataBinding0(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_ACTIVITYMAIN: {
        if ("layout/activity_main_0".equals(tag)) {
          return new ActivityMainBindingImpl(component, new View[]{view});
        }
        throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
      }
      case  LAYOUT_ATTENDANCECALCULATION: {
        if ("layout/attendance_calculation_0".equals(tag)) {
          return new AttendanceCalculationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for attendance_calculation is invalid. Received: " + tag);
      }
      case  LAYOUT_ATTENDANCELISTITEM: {
        if ("layout/attendance_list_item_0".equals(tag)) {
          return new AttendanceListItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for attendance_list_item is invalid. Received: " + tag);
      }
      case  LAYOUT_BALANCETRANSFERSUCESSFULLLAYOUT: {
        if ("layout/balance_transfer_sucessfull_layout_0".equals(tag)) {
          return new BalanceTransferSucessfullLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for balance_transfer_sucessfull_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_CLASSROUTINEITEMLIST: {
        if ("layout/class_routine_item_list_0".equals(tag)) {
          return new ClassRoutineItemListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for class_routine_item_list is invalid. Received: " + tag);
      }
      case  LAYOUT_CLASSTESTLISTITEM: {
        if ("layout/class_test_list_item_0".equals(tag)) {
          return new ClassTestListItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for class_test_list_item is invalid. Received: " + tag);
      }
      case  LAYOUT_CUSTOMNOTIFICATIONLAYOUT: {
        if ("layout/custom_notification_layout_0".equals(tag)) {
          return new CustomNotificationLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for custom_notification_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_EXAMROUTINEITEMLIST: {
        if ("layout/exam_routine_item_list_0".equals(tag)) {
          return new ExamRoutineItemListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for exam_routine_item_list is invalid. Received: " + tag);
      }
      case  LAYOUT_FEESINFOPAIDITEM: {
        if ("layout/fees_info_paid_item_0".equals(tag)) {
          return new FeesInfoPaidItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fees_info_paid_item is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDACADEMICEDUCATION: {
        if ("layout/fragment_add_academic_education_0".equals(tag)) {
          return new FragmentAddAcademicEducationBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_academic_education is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDBANKINFO: {
        if ("layout/fragment_add_bank_info_0".equals(tag)) {
          return new FragmentAddBankInfoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_bank_info is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDEXPERIENCEINFO: {
        if ("layout/fragment_add_experience_info_0".equals(tag)) {
          return new FragmentAddExperienceInfoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_experience_info is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDPOINT: {
        if ("layout/fragment_add_point_0".equals(tag)) {
          return new FragmentAddPointBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_point is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDPROFESSIONALQUALIFICATIONINFO: {
        if ("layout/fragment_add_professional_qualification_info_0".equals(tag)) {
          return new FragmentAddProfessionalQualificationInfoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_professional_qualification_info is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDREFERENCEINFO: {
        if ("layout/fragment_add_reference_info_0".equals(tag)) {
          return new FragmentAddReferenceInfoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_reference_info is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDTRAININGINFO: {
        if ("layout/fragment_add_training_info_0".equals(tag)) {
          return new FragmentAddTrainingInfoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_training_info is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTBALANCESTATEMENTREPORT: {
        if ("layout/fragment_balance_statement_report_0".equals(tag)) {
          return new FragmentBalanceStatementReportBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_balance_statement_report is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTBANKACCOUNT: {
        if ("layout/fragment_bank_account_0".equals(tag)) {
          return new FragmentBankAccountBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_bank_account is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTBANKLIST: {
        if ("layout/fragment_bank_list_0".equals(tag)) {
          return new FragmentBankListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_bank_list is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCHANGEPASSWORD: {
        if ("layout/fragment_change_password_0".equals(tag)) {
          return new FragmentChangePasswordBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_change_password is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCLASSROUTINE: {
        if ("layout/fragment_class_routine_0".equals(tag)) {
          return new FragmentClassRoutineBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_class_routine is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTCLASSTEST: {
        if ("layout/fragment_class_test_0".equals(tag)) {
          return new FragmentClassTestBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_class_test is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDEPOSIT: {
        if ("layout/fragment_deposit_0".equals(tag)) {
          return new FragmentDepositBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_deposit is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDEPOSITREPORT: {
        if ("layout/fragment_deposit_report_0".equals(tag)) {
          return new FragmentDepositReportBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_deposit_report is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDEPOSITREPORTSELECTION: {
        if ("layout/fragment_deposit_report_selection_0".equals(tag)) {
          return new FragmentDepositReportSelectionBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_deposit_report_selection is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTEXAMROUTINE: {
        if ("layout/fragment_exam_routine_0".equals(tag)) {
          return new FragmentExamRoutineBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_exam_routine is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFEESINFO: {
        if ("layout/fragment_fees_info_0".equals(tag)) {
          return new FragmentFeesInfoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_fees_info is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFORGOTPASSWORDFOUR: {
        if ("layout/fragment_forgot_password_four_0".equals(tag)) {
          return new FragmentForgotPasswordFourBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_forgot_password_four is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFORGOTPASSWORDONE: {
        if ("layout/fragment_forgot_password_one_0".equals(tag)) {
          return new FragmentForgotPasswordOneBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_forgot_password_one is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFORGOTPASSWORDTHREE: {
        if ("layout/fragment_forgot_password_three_0".equals(tag)) {
          return new FragmentForgotPasswordThreeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_forgot_password_three is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFORGOTPASSWORDTWO: {
        if ("layout/fragment_forgot_password_two_0".equals(tag)) {
          return new FragmentForgotPasswordTwoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_forgot_password_two is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTGENERALPRODUCTREPORT: {
        if ("layout/fragment_general_product_report_0".equals(tag)) {
          return new FragmentGeneralProductReportBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_general_product_report is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTGENERALPRODUCTREPORTSELECTION: {
        if ("layout/fragment_general_product_report_selection_0".equals(tag)) {
          return new FragmentGeneralProductReportSelectionBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_general_product_report_selection is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTHOME: {
        if ("layout/fragment_home_0".equals(tag)) {
          return new FragmentHomeBindingImpl(component, view);
        }
        if ("layout-sw600dp/fragment_home_0".equals(tag)) {
          return new FragmentHomeBindingSw600dpImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_home is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTINSTITUTECHANGEDIALOG: {
        if ("layout/fragment_institute_change_dialog_0".equals(tag)) {
          return new FragmentInstituteChangeDialogBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_institute_change_dialog is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTINVENTORYINFO: {
        if ("layout/fragment_inventory_info_0".equals(tag)) {
          return new FragmentInventoryInfoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_inventory_info is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTLOGOUTDIALOG: {
        if ("layout/fragment_log_out_dialog_0".equals(tag)) {
          return new FragmentLogOutDialogBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_log_out_dialog is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMESSAGERECHARGE: {
        if ("layout/fragment_message_recharge_0".equals(tag)) {
          return new FragmentMessageRechargeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_message_recharge is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMESSAGEREPORT: {
        if ("layout/fragment_message_report_0".equals(tag)) {
          return new FragmentMessageReportBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_message_report is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMYPOINT: {
        if ("layout/fragment_my_point_0".equals(tag)) {
          return new FragmentMyPointBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_my_point is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMYPROFILE: {
        if ("layout/fragment_my_profile_0".equals(tag)) {
          return new FragmentMyProfileBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_my_profile is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTNETIMAILONE: {
        if ("layout/fragment_neti_mail_one_0".equals(tag)) {
          return new FragmentNetiMailOneBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_neti_mail_one is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTNETIMAILTWO: {
        if ("layout/fragment_neti_mail_two_0".equals(tag)) {
          return new FragmentNetiMailTwoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_neti_mail_two is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTOFFERPRODUCT: {
        if ("layout/fragment_offer_product_0".equals(tag)) {
          return new FragmentOfferProductBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_offer_product is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTOFFERPRODUCTREPORT: {
        if ("layout/fragment_offer_product_report_0".equals(tag)) {
          return new FragmentOfferProductReportBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_offer_product_report is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTOFFERPRODUCTREPORTSELECTION: {
        if ("layout/fragment_offer_product_report_selection_0".equals(tag)) {
          return new FragmentOfferProductReportSelectionBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_offer_product_report_selection is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPROFILELAYOUT: {
        if ("layout/fragment_profile_layout_0".equals(tag)) {
          return new FragmentProfileLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_profile_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPURCHASECODELOG: {
        if ("layout/fragment_purchase_code_log_0".equals(tag)) {
          return new FragmentPurchaseCodeLogBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_purchase_code_log is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPURCHASEGENERALPRODUCT: {
        if ("layout/fragment_purchase_general_product_0".equals(tag)) {
          return new FragmentPurchaseGeneralProductBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_purchase_general_product is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTREVENUELOGREPORT: {
        if ("layout/fragment_revenue_log_report_0".equals(tag)) {
          return new FragmentRevenueLogReportBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_revenue_log_report is invalid. Received: " + tag);
      }
    }
    return null;
  }

  private final ViewDataBinding internalGetViewDataBinding1(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_FRAGMENTREVENUEREPORTSELECTION: {
        if ("layout/fragment_revenue_report_selection_0".equals(tag)) {
          return new FragmentRevenueReportSelectionBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_revenue_report_selection is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSEARCHSPINNER: {
        if ("layout/fragment_search_spinner_0".equals(tag)) {
          return new FragmentSearchSpinnerBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_search_spinner is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSEMESTEREXAM: {
        if ("layout/fragment_semester_exam_0".equals(tag)) {
          return new FragmentSemesterExamBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_semester_exam is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSETTING: {
        if ("layout/fragment_setting_0".equals(tag)) {
          return new FragmentSettingBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_setting is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSIGNIN: {
        if ("layout/fragment_sign_in_0".equals(tag)) {
          return new FragmentSignInBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_sign_in is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSIGNUPONE: {
        if ("layout/fragment_sign_up_one_0".equals(tag)) {
          return new FragmentSignUpOneBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_sign_up_one is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSIGNUPTWO: {
        if ("layout/fragment_sign_up_two_0".equals(tag)) {
          return new FragmentSignUpTwoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_sign_up_two is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSTUDENTATTENDANCE: {
        if ("layout/fragment_student_attendance_0".equals(tag)) {
          return new FragmentStudentAttendanceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_student_attendance is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSTUDENTPROFILE: {
        if ("layout/fragment_student_profile_0".equals(tag)) {
          return new FragmentStudentProfileBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_student_profile is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSUBJECTLIST: {
        if ("layout/fragment_subject_list_0".equals(tag)) {
          return new FragmentSubjectListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_subject_list is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSUBMITTOKEN: {
        if ("layout/fragment_submit_token_0".equals(tag)) {
          return new FragmentSubmitTokenBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_submit_token is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTAB: {
        if ("layout/fragment_tab_0".equals(tag)) {
          return new FragmentTabBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_tab is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTAGDIALOG: {
        if ("layout/fragment_tag_dialog_0".equals(tag)) {
          return new FragmentTagDialogBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_tag_dialog is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTOKENDETAILS: {
        if ("layout/fragment_token_details_0".equals(tag)) {
          return new FragmentTokenDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_token_details is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTOKENLIST: {
        if ("layout/fragment_token_list_0".equals(tag)) {
          return new FragmentTokenListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_token_list is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTOOLBAR: {
        if ("layout/fragment_tool_bar_0".equals(tag)) {
          return new FragmentToolBarBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_tool_bar is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTRABSFEROTP: {
        if ("layout/fragment_trabsfer_otp_0".equals(tag)) {
          return new FragmentTrabsferOtpBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_trabsfer_otp is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTRANSFER: {
        if ("layout/fragment_transfer_0".equals(tag)) {
          return new FragmentTransferBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_transfer is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTRANSFERREPORT: {
        if ("layout/fragment_transfer_report_0".equals(tag)) {
          return new FragmentTransferReportBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_transfer_report is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTTRANSFERREPORTSELECTION: {
        if ("layout/fragment_transfer_report_selection_0".equals(tag)) {
          return new FragmentTransferReportSelectionBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_transfer_report_selection is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTUNUSEDPURCHASEREPORT: {
        if ("layout/fragment_unused_purchase_report_0".equals(tag)) {
          return new FragmentUnusedPurchaseReportBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_unused_purchase_report is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTUPDATEADDRESSINFO: {
        if ("layout/fragment_update_address_info_0".equals(tag)) {
          return new FragmentUpdateAddressInfoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_update_address_info is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTUPDATEPERSONALINFO: {
        if ("layout/fragment_update_personal_info_0".equals(tag)) {
          return new FragmentUpdatePersonalInfoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_update_personal_info is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTUPDATEUSERINFO: {
        if ("layout/fragment_update_user_info_0".equals(tag)) {
          return new FragmentUpdateUserInfoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_update_user_info is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTUSERPOINT: {
        if ("layout/fragment_user_point_0".equals(tag)) {
          return new FragmentUserPointBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_user_point is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTWALLET: {
        if ("layout/fragment_wallet_0".equals(tag)) {
          return new FragmentWalletBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_wallet is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTWALLETREPORT: {
        if ("layout/fragment_wallet_report_0".equals(tag)) {
          return new FragmentWalletReportBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_wallet_report is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTWITHDRAW: {
        if ("layout/fragment_withdraw_0".equals(tag)) {
          return new FragmentWithdrawBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_withdraw is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTWITHDRAWREPORT: {
        if ("layout/fragment_withdraw_report_0".equals(tag)) {
          return new FragmentWithdrawReportBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_withdraw_report is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTWITHDRAWREPORTSELECTION: {
        if ("layout/fragment_withdraw_report_selection_0".equals(tag)) {
          return new FragmentWithdrawReportSelectionBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_withdraw_report_selection is invalid. Received: " + tag);
      }
      case  LAYOUT_INSTITUTELISTITEM: {
        if ("layout/institute_list_item_0".equals(tag)) {
          return new InstituteListItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for institute_list_item is invalid. Received: " + tag);
      }
      case  LAYOUT_INVENTORYLISTITEM: {
        if ("layout/inventory_list_item_0".equals(tag)) {
          return new InventoryListItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for inventory_list_item is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTSTUDENTPORTALCOMMONHEADER: {
        if ("layout/layout_student_portal_common_header_0".equals(tag)) {
          return new LayoutStudentPortalCommonHeaderBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_student_portal_common_header is invalid. Received: " + tag);
      }
      case  LAYOUT_LOGININVALID: {
        if ("layout/login_invalid_0".equals(tag)) {
          return new LoginInvalidBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for login_invalid is invalid. Received: " + tag);
      }
      case  LAYOUT_MESSAGERECHARGEPRICELAYOUT: {
        if ("layout/message_recharge_price_layout_0".equals(tag)) {
          return new MessageRechargePriceLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for message_recharge_price_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_REGISTRATIONSUCESSLAYOUT: {
        if ("layout/registration_sucess_layout_0".equals(tag)) {
          return new RegistrationSucessLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for registration_sucess_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_REPORTSBALANCESTATEMENTROW: {
        if ("layout/reports_balance_statement_row_0".equals(tag)) {
          return new ReportsBalanceStatementRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for reports_balance_statement_row is invalid. Received: " + tag);
      }
      case  LAYOUT_REPORTSPURCHASEPRODUCTOFFERROW: {
        if ("layout/reports_purchase_product_offer_row_0".equals(tag)) {
          return new ReportsPurchaseProductOfferRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for reports_purchase_product_offer_row is invalid. Received: " + tag);
      }
      case  LAYOUT_REPORTSPURCHASEUNUSEDCODEROW: {
        if ("layout/reports_purchase_unused_code_row_0".equals(tag)) {
          return new ReportsPurchaseUnusedCodeRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for reports_purchase_unused_code_row is invalid. Received: " + tag);
      }
      case  LAYOUT_REPORTSPURCHASEUSEDCODEROW: {
        if ("layout/reports_purchase_used_code_row_0".equals(tag)) {
          return new ReportsPurchaseUsedCodeRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for reports_purchase_used_code_row is invalid. Received: " + tag);
      }
      case  LAYOUT_REPORTSRECYLERVIEWLIST: {
        if ("layout/reports_recylerview_list_0".equals(tag)) {
          return new ReportsRecylerviewListBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for reports_recylerview_list is invalid. Received: " + tag);
      }
      case  LAYOUT_REPORTSWALLETBALANCETRANSFERROW: {
        if ("layout/reports_wallet_balance_transfer_row_0".equals(tag)) {
          return new ReportsWalletBalanceTransferRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for reports_wallet_balance_transfer_row is invalid. Received: " + tag);
      }
      case  LAYOUT_REPORTSWALLETMESSAGERECHARGEROW: {
        if ("layout/reports_wallet_message_recharge_row_0".equals(tag)) {
          return new ReportsWalletMessageRechargeRowBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for reports_wallet_message_recharge_row is invalid. Received: " + tag);
      }
      case  LAYOUT_RESETPASSWORDSUCESSLAYOUT: {
        if ("layout/reset_password_sucess_layout_0".equals(tag)) {
          return new ResetPasswordSucessLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for reset_password_sucess_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLEBANKINFOLAYOUT: {
        if ("layout/single_bank_info_layout_0".equals(tag)) {
          return new SingleBankInfoLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_bank_info_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLEBANKLISTLAYOUT: {
        if ("layout/single_bank_list_layout_0".equals(tag)) {
          return new SingleBankListLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_bank_list_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLECERTIFICATIONLAYOUT: {
        if ("layout/single_certification_layout_0".equals(tag)) {
          return new SingleCertificationLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_certification_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLECOMMONREPORTLAYOUT: {
        if ("layout/single_common_report_layout_0".equals(tag)) {
          return new SingleCommonReportLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_common_report_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLEEDUCATIONINFOLAYOUT: {
        if ("layout/single_education_info_layout_0".equals(tag)) {
          return new SingleEducationInfoLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_education_info_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLEEXPERIENCEDETAILSLAYOUT: {
        if ("layout/single_experience_details_layout_0".equals(tag)) {
          return new SingleExperienceDetailsLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_experience_details_layout is invalid. Received: " + tag);
      }
    }
    return null;
  }

  private final ViewDataBinding internalGetViewDataBinding2(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_SINGLEMESSAGELOGREPORTLAYOUT: {
        if ("layout/single_message_log_report_layout_0".equals(tag)) {
          return new SingleMessageLogReportLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_message_log_report_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLEREFERENCEINFOLAYOUT: {
        if ("layout/single_reference_info_layout_0".equals(tag)) {
          return new SingleReferenceInfoLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_reference_info_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLEREVENUEREPORTLAYOUT: {
        if ("layout/single_revenue_report_layout_0".equals(tag)) {
          return new SingleRevenueReportLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_revenue_report_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLESPINNERLAYOUT: {
        if ("layout/single_spinner_layout_0".equals(tag)) {
          return new SingleSpinnerLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_spinner_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLETOKENLAYOUT: {
        if ("layout/single_token_layout_0".equals(tag)) {
          return new SingleTokenLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_token_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SINGLETRAININGINFOLAYOUT: {
        if ("layout/single_training_info_layout_0".equals(tag)) {
          return new SingleTrainingInfoLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for single_training_info_layout is invalid. Received: " + tag);
      }
      case  LAYOUT_SUBJECTLISTITEM: {
        if ("layout/subject_list_item_0".equals(tag)) {
          return new SubjectListItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for subject_list_item is invalid. Received: " + tag);
      }
      case  LAYOUT_TEST: {
        if ("layout/test_0".equals(tag)) {
          return new TestBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for test is invalid. Received: " + tag);
      }
      case  LAYOUT_TEST1: {
        if ("layout/test1_0".equals(tag)) {
          return new Test1BindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for test1 is invalid. Received: " + tag);
      }
      case  LAYOUT_TOOLBAR: {
        if ("layout/toolbar_0".equals(tag)) {
          return new ToolbarBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for toolbar is invalid. Received: " + tag);
      }
      case  LAYOUT_USERREGISTRATIONLAYOUTTWO: {
        if ("layout/user_registration_layout_two_0".equals(tag)) {
          return new UserRegistrationLayoutTwoBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for user_registration_layout_two is invalid. Received: " + tag);
      }
      case  LAYOUT_WEEKNAMELAYOUT: {
        if ("layout/week_name_layout_0".equals(tag)) {
          return new WeekNameLayoutBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for week_name_layout is invalid. Received: " + tag);
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      // find which method will have it. -1 is necessary becausefirst id starts with 1;
      int methodIndex = (localizedLayoutId - 1) / 50;
      switch(methodIndex) {
        case 0: {
          return internalGetViewDataBinding0(component, view, localizedLayoutId, tag);
        }
        case 1: {
          return internalGetViewDataBinding1(component, view, localizedLayoutId, tag);
        }
        case 2: {
          return internalGetViewDataBinding2(component, view, localizedLayoutId, tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case LAYOUT_ACTIVITYMAIN: {
          if("layout/activity_main_0".equals(tag)) {
            return new ActivityMainBindingImpl(component, views);
          }
          throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(1);

    static {
      sKeys.put(0, "_all");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(113);

    static {
      sKeys.put("layout/activity_main_0", com.netizen.netiworld.R.layout.activity_main);
      sKeys.put("layout/attendance_calculation_0", com.netizen.netiworld.R.layout.attendance_calculation);
      sKeys.put("layout/attendance_list_item_0", com.netizen.netiworld.R.layout.attendance_list_item);
      sKeys.put("layout/balance_transfer_sucessfull_layout_0", com.netizen.netiworld.R.layout.balance_transfer_sucessfull_layout);
      sKeys.put("layout/class_routine_item_list_0", com.netizen.netiworld.R.layout.class_routine_item_list);
      sKeys.put("layout/class_test_list_item_0", com.netizen.netiworld.R.layout.class_test_list_item);
      sKeys.put("layout/custom_notification_layout_0", com.netizen.netiworld.R.layout.custom_notification_layout);
      sKeys.put("layout/exam_routine_item_list_0", com.netizen.netiworld.R.layout.exam_routine_item_list);
      sKeys.put("layout/fees_info_paid_item_0", com.netizen.netiworld.R.layout.fees_info_paid_item);
      sKeys.put("layout/fragment_add_academic_education_0", com.netizen.netiworld.R.layout.fragment_add_academic_education);
      sKeys.put("layout/fragment_add_bank_info_0", com.netizen.netiworld.R.layout.fragment_add_bank_info);
      sKeys.put("layout/fragment_add_experience_info_0", com.netizen.netiworld.R.layout.fragment_add_experience_info);
      sKeys.put("layout/fragment_add_point_0", com.netizen.netiworld.R.layout.fragment_add_point);
      sKeys.put("layout/fragment_add_professional_qualification_info_0", com.netizen.netiworld.R.layout.fragment_add_professional_qualification_info);
      sKeys.put("layout/fragment_add_reference_info_0", com.netizen.netiworld.R.layout.fragment_add_reference_info);
      sKeys.put("layout/fragment_add_training_info_0", com.netizen.netiworld.R.layout.fragment_add_training_info);
      sKeys.put("layout/fragment_balance_statement_report_0", com.netizen.netiworld.R.layout.fragment_balance_statement_report);
      sKeys.put("layout/fragment_bank_account_0", com.netizen.netiworld.R.layout.fragment_bank_account);
      sKeys.put("layout/fragment_bank_list_0", com.netizen.netiworld.R.layout.fragment_bank_list);
      sKeys.put("layout/fragment_change_password_0", com.netizen.netiworld.R.layout.fragment_change_password);
      sKeys.put("layout/fragment_class_routine_0", com.netizen.netiworld.R.layout.fragment_class_routine);
      sKeys.put("layout/fragment_class_test_0", com.netizen.netiworld.R.layout.fragment_class_test);
      sKeys.put("layout/fragment_deposit_0", com.netizen.netiworld.R.layout.fragment_deposit);
      sKeys.put("layout/fragment_deposit_report_0", com.netizen.netiworld.R.layout.fragment_deposit_report);
      sKeys.put("layout/fragment_deposit_report_selection_0", com.netizen.netiworld.R.layout.fragment_deposit_report_selection);
      sKeys.put("layout/fragment_exam_routine_0", com.netizen.netiworld.R.layout.fragment_exam_routine);
      sKeys.put("layout/fragment_fees_info_0", com.netizen.netiworld.R.layout.fragment_fees_info);
      sKeys.put("layout/fragment_forgot_password_four_0", com.netizen.netiworld.R.layout.fragment_forgot_password_four);
      sKeys.put("layout/fragment_forgot_password_one_0", com.netizen.netiworld.R.layout.fragment_forgot_password_one);
      sKeys.put("layout/fragment_forgot_password_three_0", com.netizen.netiworld.R.layout.fragment_forgot_password_three);
      sKeys.put("layout/fragment_forgot_password_two_0", com.netizen.netiworld.R.layout.fragment_forgot_password_two);
      sKeys.put("layout/fragment_general_product_report_0", com.netizen.netiworld.R.layout.fragment_general_product_report);
      sKeys.put("layout/fragment_general_product_report_selection_0", com.netizen.netiworld.R.layout.fragment_general_product_report_selection);
      sKeys.put("layout/fragment_home_0", com.netizen.netiworld.R.layout.fragment_home);
      sKeys.put("layout-sw600dp/fragment_home_0", com.netizen.netiworld.R.layout.fragment_home);
      sKeys.put("layout/fragment_institute_change_dialog_0", com.netizen.netiworld.R.layout.fragment_institute_change_dialog);
      sKeys.put("layout/fragment_inventory_info_0", com.netizen.netiworld.R.layout.fragment_inventory_info);
      sKeys.put("layout/fragment_log_out_dialog_0", com.netizen.netiworld.R.layout.fragment_log_out_dialog);
      sKeys.put("layout/fragment_message_recharge_0", com.netizen.netiworld.R.layout.fragment_message_recharge);
      sKeys.put("layout/fragment_message_report_0", com.netizen.netiworld.R.layout.fragment_message_report);
      sKeys.put("layout/fragment_my_point_0", com.netizen.netiworld.R.layout.fragment_my_point);
      sKeys.put("layout/fragment_my_profile_0", com.netizen.netiworld.R.layout.fragment_my_profile);
      sKeys.put("layout/fragment_neti_mail_one_0", com.netizen.netiworld.R.layout.fragment_neti_mail_one);
      sKeys.put("layout/fragment_neti_mail_two_0", com.netizen.netiworld.R.layout.fragment_neti_mail_two);
      sKeys.put("layout/fragment_offer_product_0", com.netizen.netiworld.R.layout.fragment_offer_product);
      sKeys.put("layout/fragment_offer_product_report_0", com.netizen.netiworld.R.layout.fragment_offer_product_report);
      sKeys.put("layout/fragment_offer_product_report_selection_0", com.netizen.netiworld.R.layout.fragment_offer_product_report_selection);
      sKeys.put("layout/fragment_profile_layout_0", com.netizen.netiworld.R.layout.fragment_profile_layout);
      sKeys.put("layout/fragment_purchase_code_log_0", com.netizen.netiworld.R.layout.fragment_purchase_code_log);
      sKeys.put("layout/fragment_purchase_general_product_0", com.netizen.netiworld.R.layout.fragment_purchase_general_product);
      sKeys.put("layout/fragment_revenue_log_report_0", com.netizen.netiworld.R.layout.fragment_revenue_log_report);
      sKeys.put("layout/fragment_revenue_report_selection_0", com.netizen.netiworld.R.layout.fragment_revenue_report_selection);
      sKeys.put("layout/fragment_search_spinner_0", com.netizen.netiworld.R.layout.fragment_search_spinner);
      sKeys.put("layout/fragment_semester_exam_0", com.netizen.netiworld.R.layout.fragment_semester_exam);
      sKeys.put("layout/fragment_setting_0", com.netizen.netiworld.R.layout.fragment_setting);
      sKeys.put("layout/fragment_sign_in_0", com.netizen.netiworld.R.layout.fragment_sign_in);
      sKeys.put("layout/fragment_sign_up_one_0", com.netizen.netiworld.R.layout.fragment_sign_up_one);
      sKeys.put("layout/fragment_sign_up_two_0", com.netizen.netiworld.R.layout.fragment_sign_up_two);
      sKeys.put("layout/fragment_student_attendance_0", com.netizen.netiworld.R.layout.fragment_student_attendance);
      sKeys.put("layout/fragment_student_profile_0", com.netizen.netiworld.R.layout.fragment_student_profile);
      sKeys.put("layout/fragment_subject_list_0", com.netizen.netiworld.R.layout.fragment_subject_list);
      sKeys.put("layout/fragment_submit_token_0", com.netizen.netiworld.R.layout.fragment_submit_token);
      sKeys.put("layout/fragment_tab_0", com.netizen.netiworld.R.layout.fragment_tab);
      sKeys.put("layout/fragment_tag_dialog_0", com.netizen.netiworld.R.layout.fragment_tag_dialog);
      sKeys.put("layout/fragment_token_details_0", com.netizen.netiworld.R.layout.fragment_token_details);
      sKeys.put("layout/fragment_token_list_0", com.netizen.netiworld.R.layout.fragment_token_list);
      sKeys.put("layout/fragment_tool_bar_0", com.netizen.netiworld.R.layout.fragment_tool_bar);
      sKeys.put("layout/fragment_trabsfer_otp_0", com.netizen.netiworld.R.layout.fragment_trabsfer_otp);
      sKeys.put("layout/fragment_transfer_0", com.netizen.netiworld.R.layout.fragment_transfer);
      sKeys.put("layout/fragment_transfer_report_0", com.netizen.netiworld.R.layout.fragment_transfer_report);
      sKeys.put("layout/fragment_transfer_report_selection_0", com.netizen.netiworld.R.layout.fragment_transfer_report_selection);
      sKeys.put("layout/fragment_unused_purchase_report_0", com.netizen.netiworld.R.layout.fragment_unused_purchase_report);
      sKeys.put("layout/fragment_update_address_info_0", com.netizen.netiworld.R.layout.fragment_update_address_info);
      sKeys.put("layout/fragment_update_personal_info_0", com.netizen.netiworld.R.layout.fragment_update_personal_info);
      sKeys.put("layout/fragment_update_user_info_0", com.netizen.netiworld.R.layout.fragment_update_user_info);
      sKeys.put("layout/fragment_user_point_0", com.netizen.netiworld.R.layout.fragment_user_point);
      sKeys.put("layout/fragment_wallet_0", com.netizen.netiworld.R.layout.fragment_wallet);
      sKeys.put("layout/fragment_wallet_report_0", com.netizen.netiworld.R.layout.fragment_wallet_report);
      sKeys.put("layout/fragment_withdraw_0", com.netizen.netiworld.R.layout.fragment_withdraw);
      sKeys.put("layout/fragment_withdraw_report_0", com.netizen.netiworld.R.layout.fragment_withdraw_report);
      sKeys.put("layout/fragment_withdraw_report_selection_0", com.netizen.netiworld.R.layout.fragment_withdraw_report_selection);
      sKeys.put("layout/institute_list_item_0", com.netizen.netiworld.R.layout.institute_list_item);
      sKeys.put("layout/inventory_list_item_0", com.netizen.netiworld.R.layout.inventory_list_item);
      sKeys.put("layout/layout_student_portal_common_header_0", com.netizen.netiworld.R.layout.layout_student_portal_common_header);
      sKeys.put("layout/login_invalid_0", com.netizen.netiworld.R.layout.login_invalid);
      sKeys.put("layout/message_recharge_price_layout_0", com.netizen.netiworld.R.layout.message_recharge_price_layout);
      sKeys.put("layout/registration_sucess_layout_0", com.netizen.netiworld.R.layout.registration_sucess_layout);
      sKeys.put("layout/reports_balance_statement_row_0", com.netizen.netiworld.R.layout.reports_balance_statement_row);
      sKeys.put("layout/reports_purchase_product_offer_row_0", com.netizen.netiworld.R.layout.reports_purchase_product_offer_row);
      sKeys.put("layout/reports_purchase_unused_code_row_0", com.netizen.netiworld.R.layout.reports_purchase_unused_code_row);
      sKeys.put("layout/reports_purchase_used_code_row_0", com.netizen.netiworld.R.layout.reports_purchase_used_code_row);
      sKeys.put("layout/reports_recylerview_list_0", com.netizen.netiworld.R.layout.reports_recylerview_list);
      sKeys.put("layout/reports_wallet_balance_transfer_row_0", com.netizen.netiworld.R.layout.reports_wallet_balance_transfer_row);
      sKeys.put("layout/reports_wallet_message_recharge_row_0", com.netizen.netiworld.R.layout.reports_wallet_message_recharge_row);
      sKeys.put("layout/reset_password_sucess_layout_0", com.netizen.netiworld.R.layout.reset_password_sucess_layout);
      sKeys.put("layout/single_bank_info_layout_0", com.netizen.netiworld.R.layout.single_bank_info_layout);
      sKeys.put("layout/single_bank_list_layout_0", com.netizen.netiworld.R.layout.single_bank_list_layout);
      sKeys.put("layout/single_certification_layout_0", com.netizen.netiworld.R.layout.single_certification_layout);
      sKeys.put("layout/single_common_report_layout_0", com.netizen.netiworld.R.layout.single_common_report_layout);
      sKeys.put("layout/single_education_info_layout_0", com.netizen.netiworld.R.layout.single_education_info_layout);
      sKeys.put("layout/single_experience_details_layout_0", com.netizen.netiworld.R.layout.single_experience_details_layout);
      sKeys.put("layout/single_message_log_report_layout_0", com.netizen.netiworld.R.layout.single_message_log_report_layout);
      sKeys.put("layout/single_reference_info_layout_0", com.netizen.netiworld.R.layout.single_reference_info_layout);
      sKeys.put("layout/single_revenue_report_layout_0", com.netizen.netiworld.R.layout.single_revenue_report_layout);
      sKeys.put("layout/single_spinner_layout_0", com.netizen.netiworld.R.layout.single_spinner_layout);
      sKeys.put("layout/single_token_layout_0", com.netizen.netiworld.R.layout.single_token_layout);
      sKeys.put("layout/single_training_info_layout_0", com.netizen.netiworld.R.layout.single_training_info_layout);
      sKeys.put("layout/subject_list_item_0", com.netizen.netiworld.R.layout.subject_list_item);
      sKeys.put("layout/test_0", com.netizen.netiworld.R.layout.test);
      sKeys.put("layout/test1_0", com.netizen.netiworld.R.layout.test1);
      sKeys.put("layout/toolbar_0", com.netizen.netiworld.R.layout.toolbar);
      sKeys.put("layout/user_registration_layout_two_0", com.netizen.netiworld.R.layout.user_registration_layout_two);
      sKeys.put("layout/week_name_layout_0", com.netizen.netiworld.R.layout.week_name_layout);
    }
  }
}
