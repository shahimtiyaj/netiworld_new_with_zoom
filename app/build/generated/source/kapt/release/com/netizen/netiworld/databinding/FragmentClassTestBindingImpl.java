package com.netizen.netiworld.databinding;
import com.netizen.netiworld.R;
import com.netizen.netiworld.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentClassTestBindingImpl extends FragmentClassTestBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = new androidx.databinding.ViewDataBinding.IncludedLayouts(18);
        sIncludes.setIncludes(1, 
            new String[] {"layout_student_portal_common_header"},
            new int[] {2},
            new int[] {com.netizen.netiworld.R.layout.layout_student_portal_common_header});
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.fragment_container, 3);
        sViewsWithIds.put(R.id.txt_view_std_attendance, 4);
        sViewsWithIds.put(R.id.layout_relative_atd, 5);
        sViewsWithIds.put(R.id.button_academic_year, 6);
        sViewsWithIds.put(R.id.relative_tab_view, 7);
        sViewsWithIds.put(R.id.button_class_test, 8);
        sViewsWithIds.put(R.id.button_semester_exam, 9);
        sViewsWithIds.put(R.id.edit_text_academic_year, 10);
        sViewsWithIds.put(R.id.input_layout_class_test, 11);
        sViewsWithIds.put(R.id.edit_text_class_test, 12);
        sViewsWithIds.put(R.id.input_layout_semester_exam, 13);
        sViewsWithIds.put(R.id.edit_text_class_exam, 14);
        sViewsWithIds.put(R.id.relative_academic, 15);
        sViewsWithIds.put(R.id.button_search, 16);
        sViewsWithIds.put(R.id.lottie_progressbar, 17);
    }
    // views
    @NonNull
    private final android.widget.RelativeLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentClassTestBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 18, sIncludes, sViewsWithIds));
    }
    private FragmentClassTestBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (android.widget.Button) bindings[6]
            , (android.widget.Button) bindings[8]
            , (android.widget.Button) bindings[16]
            , (android.widget.Button) bindings[9]
            , (com.google.android.material.textfield.TextInputEditText) bindings[10]
            , (com.google.android.material.textfield.TextInputEditText) bindings[14]
            , (com.google.android.material.textfield.TextInputEditText) bindings[12]
            , (android.widget.RelativeLayout) bindings[3]
            , (com.google.android.material.textfield.TextInputLayout) bindings[11]
            , (com.google.android.material.textfield.TextInputLayout) bindings[13]
            , (android.widget.RelativeLayout) bindings[5]
            , (com.netizen.netiworld.databinding.LayoutStudentPortalCommonHeaderBinding) bindings[2]
            , (android.widget.RelativeLayout) bindings[17]
            , (android.widget.RelativeLayout) bindings[15]
            , (android.widget.RelativeLayout) bindings[1]
            , (android.widget.LinearLayout) bindings[7]
            , (android.widget.TextView) bindings[4]
            );
        this.mboundView0 = (android.widget.RelativeLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.relativeLayoutStdInfo.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        layoutStudentInfo.invalidateAll();
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        if (layoutStudentInfo.hasPendingBindings()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    public void setLifecycleOwner(@Nullable androidx.lifecycle.LifecycleOwner lifecycleOwner) {
        super.setLifecycleOwner(lifecycleOwner);
        layoutStudentInfo.setLifecycleOwner(lifecycleOwner);
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeLayoutStudentInfo((com.netizen.netiworld.databinding.LayoutStudentPortalCommonHeaderBinding) object, fieldId);
        }
        return false;
    }
    private boolean onChangeLayoutStudentInfo(com.netizen.netiworld.databinding.LayoutStudentPortalCommonHeaderBinding LayoutStudentInfo, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
        executeBindingsOn(layoutStudentInfo);
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): layoutStudentInfo
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}