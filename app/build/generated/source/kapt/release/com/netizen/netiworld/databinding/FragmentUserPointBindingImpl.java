package com.netizen.netiworld.databinding;
import com.netizen.netiworld.R;
import com.netizen.netiworld.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentUserPointBindingImpl extends FragmentUserPointBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.scroll_main_view, 1);
        sViewsWithIds.put(R.id.image_view_user, 2);
        sViewsWithIds.put(R.id.text_view_user_name, 3);
        sViewsWithIds.put(R.id.text_view_neti_id, 4);
        sViewsWithIds.put(R.id.text_view_setting, 5);
        sViewsWithIds.put(R.id.text_view_add_portal, 6);
        sViewsWithIds.put(R.id.text_view_student_portal, 7);
        sViewsWithIds.put(R.id.text_view_s_institute_profile, 8);
        sViewsWithIds.put(R.id.text_view_profile, 9);
        sViewsWithIds.put(R.id.text_view_s_attendance_info, 10);
        sViewsWithIds.put(R.id.text_view_s_subject_info, 11);
        sViewsWithIds.put(R.id.text_view_s_rountine_info, 12);
        sViewsWithIds.put(R.id.text_view_s_exam_info, 13);
        sViewsWithIds.put(R.id.text_view_s_fees_info, 14);
        sViewsWithIds.put(R.id.text_view_s_inventory_info, 15);
        sViewsWithIds.put(R.id.text_view_guardian_portal, 16);
        sViewsWithIds.put(R.id.text_view_g_institute_profile, 17);
        sViewsWithIds.put(R.id.text_view_g_profile, 18);
        sViewsWithIds.put(R.id.text_view_g_attendance_info, 19);
        sViewsWithIds.put(R.id.text_view_g_subject_info, 20);
        sViewsWithIds.put(R.id.text_view_g_rountine_info, 21);
        sViewsWithIds.put(R.id.text_view_g_exam_info, 22);
        sViewsWithIds.put(R.id.text_view_g_fees_info, 23);
        sViewsWithIds.put(R.id.text_view_g_inventory_info, 24);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentUserPointBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 25, sIncludes, sViewsWithIds));
    }
    private FragmentUserPointBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[2]
            , (android.widget.RelativeLayout) bindings[0]
            , (android.widget.ScrollView) bindings[1]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[19]
            , (android.widget.TextView) bindings[22]
            , (android.widget.TextView) bindings[23]
            , (android.widget.TextView) bindings[17]
            , (android.widget.TextView) bindings[24]
            , (android.widget.TextView) bindings[18]
            , (android.widget.TextView) bindings[21]
            , (android.widget.TextView) bindings[20]
            , (android.widget.TextView) bindings[16]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[3]
            );
        this.layoutParentView.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}